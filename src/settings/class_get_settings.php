<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_get_settings extends class_uri_file_path_operations {
	public $allowed_get_keys;
	public $check_numeric;
	public $check_content;
	public $hide_post;

	protected $db;

	public function __construct() {
		parent::__construct();
		$this->allowed_get_keys = array(
			"section_id",
			"content_id",
			"page",
			"text",
			"no_count",
			"file",
			"uploaddir",
			"target",
			"element",
			"form_submit_id",
			"hint",
			"back",
			"uploaddir_orig",
			"theme",
			"news_page"
		);
		$this->check_numeric    = array( "section_id", "content_id", "page", "no_count", "back" );
		$this->check_content    = array(
			"uploaddir",
			"uploaddir_orig",
			"file",
			"type",
			"gender",
			"birth_day",
			"birth_month",
			"birth_year",
			"account_picture_source",
			"theme"
		);
		$this->hide_post        = array(
			"pass",
			"password",
			"password_check",
			"password1",
			"password2",
			"register_password",
			"register_password1",
			"register_password2",
			"password_current"
		);
		$this->db               = class_database::getInstance();
	}

	public function check_content( $key, $content ) {
		$abc = array(
			"all",
			"A",
			"B",
			"C",
			"D",
			"E",
			"F",
			"G",
			"H",
			"I",
			"J",
			"K",
			"L",
			"M",
			"N",
			"O",
			"P",
			"Q",
			"R",
			"S",
			"T",
			"U",
			"V",
			"W",
			"X",
			"Y",
			"Z",
			"Ä",
			"Ö",
			"Ü",
			"OE",
			"UE",
			"AE"
		);
		if ( in_array( $key, $this->check_content ) && ! is_array( $content ) ) {
			if ( $key == "file" ) {
				$sql = "SELECT `id` FROM `" . TP . "file_download` WHERE `filename` LIKE BINARY '$content'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				if ( $result->num_rows == 0 ) {
					return false;
				} else {
					return true;
				}
			} elseif ( $key == "theme" ) {
				$layouts = $this->get_dir_content( $GLOBALS['layout_d'] );
				$found   = false;
				foreach ( $layouts as $value ) {
					if ( strcasecmp( $value, $content ) === 0 ) {
						$found = true;
						break;
					}
				}

				return $found;
			} elseif ( $key == "type" ) {
				if ( $content == 1 || $content == 2 ) {
					return true;
				} else {
					return false;
				}
			} elseif ( $key == "gender" ) {
				if ( $content === 0 || $content == 1 || $content == 2 ) {
					return true;
				} else {
					return false;
				}
			} elseif ( $key == "birth_day" ) {
				if ( $content >= - 1 && $content <= 31 ) {
					return true;
				} else {
					return false;
				}
			} elseif ( $key == "birth_month" ) {
				if ( $content >= - 1 && $content <= 13 ) {
					return true;
				} else {
					return false;
				}
			} elseif ( $key == "birth_year" ) {
				if ( $content >= - 1 && $content <= 3000 ) {
					return true;
				} else {
					return false;
				}
			} elseif ( $key == "uploaddir" || $key == "uploaddir_orig" ) {
				$result = strpos( $content, ".." );
				if ( $result !== false ) {
					return false;
				}
				$result = strpos( $content, UPLOADDIR );
				if ( $result === false || $result != 0 ) {
					return false;
				}

				return true;
			} elseif ( $key == "account_picture_source" ) {
				$result = strpos( $content, ".." );
				if ( $result !== false ) {
					return false;
				}
				/*
				$result = strpos($content, UPLOADDIR);
				if($result === false || $result != 0)
				{
					return false;
				}*/
				$result = $this->check_special_filetype( $content, "picture" );

				return $result;
			} else {
				return false;
			}
			/*
			if($key=="select")
			{
				if(in_array($content, $abc)) return TRUE;
				else return FALSE;
			}
			if($key=="season")
			{
				if($content == "s") return TRUE;
				if($content == "w") return TRUE;
				return FALSE;
			}
			if($key=="show")
			{
				if(is_numeric($content) || ($content=="all") || ($content=="archive"))
					return TRUE;
				else
					return FALSE;
			}
			if($key=="order")
			{
				if($content == "date" || $content == "number")
					return TRUE;
				else
					return FALSE;

			}
			*/
		} else {
			return true;
		}
	}
}