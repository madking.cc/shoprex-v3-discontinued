<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_page_settings extends class_uri_file_path_operations {
	public $allowed_output_files;
	public $images_upload_dirs;
	public $delete_dirs;
	public $allowed_ip_ranges;
	public $getpost_parameter_to_keep;


	public function __construct() {
		parent::__construct();
		$this->allowed_output_files      = array( "php" );
		$this->images_upload_dirs        = array( UPLOADDIR );
		$this->delete_dirs               = array(
			DOWNLOADDIR . "delete/",
			UPLOADDIR . "delete/",
			UPLOADDIR . "orig/delete/"
		);
		$this->allowed_ip_ranges         = array( "0.0.0.0", "255.255.255.255" );
		$this->load_extended_classes     = false;
		$this->getpost_parameter_to_keep = array( "no_count", "theme" );


	}

}