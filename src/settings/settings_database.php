<?php defined( 'SECURITY_CHECK' ) or die;

define( "TP", "sr_" );

if ( isset( $GLOBALS['page_is_online'] ) && $GLOBALS['page_is_online'] ) {
	define( "DB_HOST", "localhost" );
	define( "DB_TABLE", "" );
	define( "DB_PORT", "" ); // Optional
	if ( isset( $GLOBALS['is_admin'] ) && $GLOBALS['is_admin'] ) {
		define( "DB_USER", "" );
		define( "DB_PASS", "" );
	} else {
		define( "DB_USER", "" );
		define( "DB_PASS", "" );
	}
} else {
	define( "DB_HOST", "localhost" );
	define( "DB_TABLE", "" );
	define( "DB_PORT", "" ); // Optional
	if ( isset( $use_admin_db_login ) && $use_admin_db_login ) {
		define( "DB_USER", "" );
		define( "DB_PASS", "" );
	} else {
		define( "DB_USER", "" );
		define( "DB_PASS", "" );
	}
}