<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_layout_settings extends class_root {
	public $button_types;
	public $button_sizes;

	public function __construct() {
		$this->button_types = array(
			"btn-default" => $this->txt( 'STANDARD' ),
			"btn-primary" => $this->txt( 'PRIMARY' ),
			"btn-success" => $this->txt( 'SUCCESS' ),
			"btn-info"    => $this->txt( 'INFO' ),
			"btn-warning" => $this->txt( 'WARNING' ),
			"btn-danger"  => $this->txt( 'DANGER' ),
			"btn-link"    => $this->txt( 'SIMPLE_LINK' )
		);
		$this->button_sizes = array(
			"dummy" => $this->txt( 'STANDARD' ),
			"btn-lg" => $this->txt( 'BIG' ),
			"btn-sm" => $this->txt( 'SMALL' ),
			"btn-xs" => $this->txt( 'VERY_SMALL' )
		);

	}

}