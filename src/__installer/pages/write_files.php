<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

if ( session_status() !== PHP_SESSION_ACTIVE ) {
	session_start();
}

function get_session_parameter( $post_var_name ) {
	if ( isset( $_SESSION[ $post_var_name ] ) ) {
		return $_SESSION[ $post_var_name ];
	} else {
		return null;
	}
}

$db_name            = get_session_parameter( "db_name" );
$db_pass            = get_session_parameter( "db_pass" );
$db_database_server = get_session_parameter( "db_database_server" );
if ( is_null( $db_database_server ) ) {
	$db_database_server = "localhost";
}
$db_database_port = get_session_parameter( "db_database_port" );
$db_database_name = get_session_parameter( "db_database_name" );
$db_admin_name    = get_session_parameter( "db_admin_name" );
$db_admin_pass    = get_session_parameter( "db_admin_pass" );
$db_table_prefix  = get_session_parameter( "db_table_prefix" );
if ( is_null( $db_table_prefix ) ) {
	$db_table_prefix = "sr_";
}
$timezone = get_session_parameter( "timezone" );


switch ( $lang ) {
	case "en":
		$lang_parameter               = "?lang=en";
		$lang_parameter               = "";
		$txt_error_file_open_to_write = "Can't open file for write.";
		$txt_error_file_write         = "Can't write file.";
		$txt_error                    = "Error!";
		$txt_file                     = "File: ";
		$admin_subtitle               = "Write Files";
		$txt_yes                      = "Yes";
		$txt_no                       = "No";
		$txt_reload_page              = "Fix the problem in your filesystem and reload this page.";
		break;
	case "de":
	default:
		$lang_parameter               = "";
		$txt_error_file_open_to_write = "Kann Datei nicht zum schreiben öffnen.";
		$txt_error_file_write         = "Kann nicht in Datei schreiben.";
		$txt_error                    = "Fehler!";
		$txt_file                     = "Datei: ";
		$admin_subtitle               = "Schreibe Dateien";
		$txt_yes                      = "Ja";
		$txt_no                       = "Nein";
		$txt_reload_page              = "Bitte beseitigen Sie den Fehler im Dateisystem und laden dann diese Seite neu.";
		break;
}

function write_file( $file, $content ) {
	global $txt_error_file_open_to_write;
	global $txt_error_file_write;

	$file_handle = fopen( DIRROOT . $file, "w" );

	if ( $file_handle === false ) {
		return $txt_error_file_open_to_write;
	} else {
		$status = fwrite( $file_handle, $content );
		fclose( $file_handle );
		if ( $status === false ) {
			return $txt_error_file_write;
		} else {
			return false;
		}
	}
}

$error = false;
require_once( DIRINSTALL . "setting_files/htaccess_root.php" );
$result = write_file( ".htaccess", $htaccess_root );
if ( $result ) {
	$error = true;
	$content .= "<p>" . $txt_error . "</p>\n";
	$content .= "<p>" . $result . "</p>\n";
	$content .= "<p>" . $txt_file . " '" . DIRROOT . ".htaccess'</p>\n";
}
require_once( DIRINSTALL . "setting_files/htaccess_admin.php" );
$result = write_file( "__admin/.htaccess", $htaccess_admin );
if ( $result ) {
	$error = true;
	$content .= "<p>" . $txt_error . "</p>\n";
	$content .= "<p>" . $result . "</p>\n";
	$content .= "<p>" . $txt_file . " '" . DIRROOT . "__admin/.htaccess'</p>\n";
}
require_once( DIRINSTALL . "setting_files/settings_database.php" );
$result = write_file( "settings/settings_database.php", $settings_database );
if ( $result ) {
	$error = true;
	$content .= "<p>" . $txt_error . "</p>\n";
	$content .= "<p>" . $result . "</p>\n";
	$content .= "<p>" . $txt_file . " '" . DIRROOT . "settings/settings_database.php'</p>\n";
}
require_once( DIRINSTALL . "setting_files/settings_website.ini.php" );
$result = write_file( "settings/settings_website.ini", $settings_website_ini );
if ( $result ) {
	$error = true;
	$content .= "<p>" . $txt_error . "</p>\n";
	$content .= "<p>" . $result . "</p>\n";
	$content .= "<p>" . $txt_file . " '" . DIRROOT . "settings/settings_website.ini.php'</p>\n";
}


if ( $_SESSION["use_example_data"] ) {
	copy( DIRINSTALL . "example_data/download/www-shoprex-logo.png", DIRROOT . "download/www-shoprex-logo.png" );
}

function delete_recursive( $dir, $use_dirroot = false ) {
	if ( $use_dirroot ) {
		$dir = DIRROOT . $dir;
	}

	if ( is_dir( $dir ) ) {
		$objects = scandir( $dir );
		foreach ( $objects as $object ) {
			if ( $object != "." && $object != ".." ) {
				if ( filetype( $dir . "/" . $object ) == "dir" ) {
					delete_recursive( $dir . "/" . $object, false );
				} else {
					unlink( $dir . "/" . $object );
				}
			}
		}
		reset( $objects );
		rmdir( $dir );
	}
}

if ( ! $error ) {
	$GLOBALS['body_footer'] .= "<script>
                parent.location.href = '" . $loc->httpauto_web_root . "__admin/';
            </script>\n";

	$do_delete_recursive = true;
} else {
	$content .= "<p>" . $txt_reload_page . "</p>";
}




