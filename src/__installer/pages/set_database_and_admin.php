<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

if ( session_status() !== PHP_SESSION_ACTIVE ) {
	session_start();
}

function get_session_parameter( $post_var_name ) {
	if ( isset( $_SESSION[ $post_var_name ] ) ) {
		return $_SESSION[ $post_var_name ];
	} else {
		return null;
	}
}

$db_name            = get_session_parameter( "db_name" );
$db_pass            = get_session_parameter( "db_pass" );
$db_database_server = get_session_parameter( "db_database_server" );
if ( is_null( $db_database_server ) ) {
	$db_database_server = "localhost";
}
$db_database_port = get_session_parameter( "db_database_port" );
$db_database_name = get_session_parameter( "db_database_name" );
$db_admin_name    = get_session_parameter( "db_admin_name" );
$db_admin_pass    = get_session_parameter( "db_admin_pass" );
$db_table_prefix  = get_session_parameter( "db_table_prefix" );
if ( is_null( $db_table_prefix ) ) {
	$db_table_prefix = "sr_";
}
$inst_admin_name  = get_session_parameter( "inst_admin_name" );
$inst_admin_pass  = get_session_parameter( "inst_admin_pass" );
$inst_admin_email = get_session_parameter( "inst_admin_email" );
$db_drop_tables   = get_session_parameter( "db_drop_tables" );
if ( is_null( $db_drop_tables ) ) {
	$db_drop_tables = true;
}
$use_example_data = get_session_parameter( "use_example_data" );
if ( is_null( $use_example_data ) ) {
	$use_example_data = true;
}
$timezone = get_session_parameter( "timezone" );

if ( isset( $_GET['error'] ) ) {
	$error = $_GET['error'];
} else {
	$error = null;
}
if ( isset( $_GET['account'] ) ) {
	$account = $_GET['account'];
} else {
	$account = null;
}

if ( $account == "admin" ) {
	$user_name = $db_admin_name;
} else {
	$user_name = $db_name;
}


$txt_error_db = "";
switch ( $error ) {
	case "1044":
		switch ( $lang ) {
			case "en":
				$txt_error_db = "Message: User '" . $user_name . "' has no rights to access database '$db_database_name'";
				break;
			case "de":
			default:
				$txt_error_db = "Nachricht: User '" . $user_name . "' hat keine Rechte für den Zugriff auf die Datenbank '$db_database_name'";
				break;
		}
		break;
	case "1045":
		switch ( $lang ) {
			case "en":
				$txt_error_db = "Message: Database access denied for user '$user_name'";
				break;
			case "de":
			default:
				$txt_error_db = "Nachricht: Datenbank Zugriff verweigert für user '$user_name'";
				break;
		}
		break;
	case "2003":
		switch ( $lang ) {
			case "en":
				$txt_error_db = "Message: Cannot connect to server: '$db_database_server'";
				break;
			case "de":
			default:
				$txt_error_db = "Nachricht: Kann nicht zum Server verbinden: '$db_database_server'";
				break;
		}
		break;
	case "2005":
		switch ( $lang ) {
			case "en":
				$txt_error_db = "Message: Database server not found: '$db_database_server'";
				break;
			case "de":
			default:
				$txt_error_db = "Nachricht: Datenbank Server nicht gefunden: '$db_database_server'";
				break;
		}
		break;
	default:
		if ( is_numeric( $error ) && error > 0 ) {
			switch ( $lang ) {
				case "en":
					$txt_error_db = "Message: Database Error Code '$error'.";
					break;
				case "de":
				default:
					$txt_error_db = "Nachricht: Datenbank Fehler Nummer '$error'.";
					break;
			}

			if ( $error < 2000 ) {
				$error_link = "http://dev.mysql.com/doc/refman/5.6/en/error-messages-server.html";
			} else {
				$error_link = "http://dev.mysql.com/doc/refman/5.6/en/error-messages-client.html";
			}
			switch ( $lang ) {
				case "en":
					$txt_error_db .= " More Informations at: <a href='$error_link' target='_blank'>$error_link</a>";
					break;
				case "de":
				default:
					$txt_error_db .= " Mehr Informationen unter: <a href='$error_link' target='_blank'>$error_link</a>";
					break;
			}
		}
		break;
}


switch ( $lang ) {
	case "en":
		$lang_parameter                         = "?lang=en";
		$txt_database_settings                  = "<h3>Database Settings:</h3><hr>\n";
		$txt_database_account_name              = "Database Account Name:";
		$txt_missing_database_account_name      = "Database Account Name missing!";
		$txt_database_account_password          = "Database Account Password:";
		$txt_database_server                    = "Database Server:";
		$txt_missing_database_server            = "Database Server missing!";
		$txt_database_port                      = "Database Port:";
		$txt_database_name                      = "Database Name:";
		$txt_missing_database_name              = "Database Name missing!";
		$txt_optional_admin_database_account    = "Optional, database account with extended rights for admin area:";
		$txt_database_admin_account_name        = "Database Admin Account Name:";
		$txt_database_admin_account_password    = "Database Admin Account Password:";
		$txt_database_administrator_settings    = "Administrator Login:";
		$txt_hint_account_info_for_admin_area   = "Admin Account Data Login for Website:";
		$txt_website_admin_account_name         = "Website Admin Account Name:";
		$txt_missing_website_admin_account_name = "Website Admin Account Name missing!";
		$txt_website_admin_account_password     = "Website Admin Account Password:";
		$txt_other                              = "Other:";
		$txt_fill_database                      = "Fill Database with sample data:";
		$txt_hint_recommended                   = "(Recommended)";
		$txt_usually_localhost                  = "(Usually: localhost)";
		$txt_optional                           = "(Optional)";
		$txt_table_prefix                       = "Table prefix:";
		$txt_website_admin_email                = "Administrator E-Mail:";
		$txt_timezone                           = "Timezone:";
		$txt_delete_existing_tables             = "Delete existing tables:";
		$txt_delete_tables_hint                 = "(Only tables deleted with the same name from an old installation.)";
		$txt_yes                                = "Yes";
		$txt_no                                 = "No";
		break;
	case "de":
	default:
		$lang_parameter                         = "";
		$txt_database_settings                  = "<h3>Datenbankverbindung:</h3><hr>\n";
		$txt_database_account_name              = "Datenbank Account Name:";
		$txt_missing_database_account_name      = "Datenbank Account Name fehlt!";
		$txt_database_account_password          = "Datenbank Account Passwort:";
		$txt_database_server                    = "Datenbank Server:";
		$txt_missing_database_server            = "Datenbank Server fehlt!";
		$txt_database_port                      = "Datenbank Port:";
		$txt_database_name                      = "Datenbank Name:";
		$txt_missing_database_name              = "Datenbank Name fehlt!";
		$txt_optional_admin_database_account    = "Optional, ein Datenbank Account mit erweiterten Rechten für den Admin Bereich:";
		$txt_database_admin_account_name        = "Datenbank Admin Account Name:";
		$txt_database_admin_account_password    = "Datenbank Admin Account Passwort:";
		$txt_database_administrator_settings    = "Administrator Login:";
		$txt_hint_account_info_for_admin_area   = "Account Daten für den Login im Admin Bereich:";
		$txt_website_admin_account_name         = "Webseiten Admin Account Name:";
		$txt_missing_website_admin_account_name = "Webseiten Admin Account Name fehlt!";
		$txt_website_admin_account_password     = "Webseiten Admin Account Passwort:";
		$txt_other                              = "Weiteres:";
		$txt_fill_database                      = "Fülle Datenbank mit Beispieldaten:";
		$txt_hint_recommended                   = "(Empfohlen)";
		$txt_usually_localhost                  = "(In der Regel: localhost)";
		$txt_optional                           = "(Optional)";
		$txt_table_prefix                       = "Tabellen Prefix:";
		$txt_website_admin_email                = "Administrator E-Mail:";
		$txt_timezone                           = "Zeitzone:";
		$txt_delete_existing_tables             = "Lösche ggf. vorhandene Tabellen: ";
		$txt_delete_tables_hint                 = "(Es werden nur Tabellen mit selben Namen von einer alten Installation gelöscht.)";
		$txt_yes                                = "Ja";
		$txt_no                                 = "Nein";
		break;
}


$GLOBALS["body_footer"] .= "
<script>

function check_submit(formID)
{
    var result = true;

    if(formID.db_name.value == '')
    {
        result = false;
        $('#missing_database_account_name').show();   
    }
    else 
        $('#missing_database_account_name').hide(); 
    if(formID.db_database_server.value == '')
    {
        result = false;
        $('#missing_database_server').show();   
    }
    else 
        $('#missing_database_server').hide();     
    if(formID.db_database_name.value == '')
    {
        result = false;
        $('#missing_database_name').show();   
    }
    else 
        $('#missing_database_name').hide();     
    if(formID.admin_name.value == '')
    {
        result = false;
        $('#missing_website_admin_account_name').show();   
    }
    else 
        $('#missing_website_admin_account_name').hide(); 
    return result;
}

</script>
";


$content .= "<form action='/" . $loc->web_root . "fill_database.php" . $lang_parameter . "' method='POST' onSubmit='return check_submit(this)'>";

if ( ! empty( $txt_error_db ) ) {
	$content .= "<p><span class='text-error'>" . $txt_error_db . "</span></p>";
}

$content .= "<h3>" . $txt_database_settings . "</h3>\n";
$content .= "<p><span class='fixed-width'>" . $txt_database_account_name . "</span><input name='db_name' value='$db_name' type='text' class='input-db-text'></p>";
$content .= "<p id='missing_database_account_name' style='display: none'><span class='text-error'>" . $txt_missing_database_account_name . "</span></p>";
$content .= "<p><span class='fixed-width'>" . $txt_database_account_password . "</span><input name='db_pass' value='$db_pass' type='text' class='input-db-text'></p>";
$content .= "<p><span class='fixed-width'>" . $txt_database_server . "</span><input name='db_database_server' value='$db_database_server' type='text' class='input-db-text'> " . $txt_usually_localhost . "</p>";
$content .= "<p id='missing_database_server' style='display: none'><span class='text-error'>" . $txt_missing_database_server . "</span></p>";
$content .= "<p><span class='fixed-width'>" . $txt_database_port . "</span><input name='db_database_port' value='$db_database_port' type='text' class='input-db-text'> " . $txt_optional . "</p>";
$content .= "<p><span class='fixed-width'>" . $txt_database_name . "</span><input name='db_database_name' value='$db_database_name' type='text' class='input-db-text'></p>";
$content .= "<p id='missing_database_name' style='display: none'><span class='text-error'>" . $txt_missing_database_name . "</span></p>";
$content .= "<p><span class='fixed-width'>" . $txt_table_prefix . "</span><input name='db_table_prefix' value='$db_table_prefix' type='text' class='input-db-text'></p>";
$content .= "<p><b>" . $txt_optional_admin_database_account . "</b></p>";
$content .= "<p><span class='fixed-width'>" . $txt_database_admin_account_name . "</span><input name='db_admin_name' value='$db_admin_name' type='text' class='input-db-text'></p>";
$content .= "<p><span class='fixed-width'>" . $txt_database_admin_account_password . "</span><input name='db_admin_pass' value='$db_admin_pass' type='text' class='input-db-text'></p>";

$content .= "<hr><h3>" . $txt_database_administrator_settings . "</h3>\n";
$content .= "<p><b>" . $txt_hint_account_info_for_admin_area . "</b></p>";
$content .= "<p><span class='fixed-width'>" . $txt_website_admin_account_name . "</span><input name='inst_admin_name' value='$inst_admin_name' type='text' class='input-db-text'></p>";
$content .= "<p id='missing_website_admin_account_name' style='display: none'><span class='text-error'>" . $txt_missing_website_admin_account_name . "</span></p>";
$content .= "<p><span class='fixed-width'>" . $txt_website_admin_account_password . "</span><input name='inst_admin_pass' value='$inst_admin_pass' type='text' class='input-db-text'></p>";
$content .= "<p><span class='fixed-width'>" . $txt_website_admin_email . "</span><input name='inst_admin_email' value='$inst_admin_email' type='text' class='input-db-text'></p>";

$content .= "<hr><h3>" . $txt_other . "</h3>\n";
$content .= "<p><span class='fixed-width'>" . $txt_delete_existing_tables . "</span><select name='db_drop_tables'><option value='1'";
if ( $db_drop_tables ) {
	$content .= " selected=selected";
}
$content .= ">$txt_yes $txt_hint_recommended</option><option value='0'";
if ( ! $db_drop_tables ) {
	$content .= " selected=selected";
}
$content .= ">$txt_no</option></select>\n";
$content .= "</p>";
$content .= "<p>" . $txt_delete_tables_hint . "</p>";
$content .= "<p><span class='fixed-width'>" . $txt_fill_database . "</span><select name='use_example_data'><option value='1'";
if ( $use_example_data ) {
	$content .= " selected=selected";
}
$content .= ">$txt_yes $txt_hint_recommended</option><option value='0'";
if ( ! $use_example_data ) {
	$content .= " selected=selected";
}
$content .= ">$txt_no</option></select>\n";
$content .= "<p><span class='fixed-width'>" . $txt_timezone . "</span><select name='timezone'>" . select_timezone_options( $timezone ) . "</select></p>";


switch ( $lang ) {
	case "en":
		$admin_subtitle = "Informations";
		$content .= "<hr><p>The next page can be empty for the next few minutes and several times to reload. Please do not interrupt this process.</p>";
		$content .= "<div class='row'><div class='col-sm-6'><p class=''><a class='link-button' href='./check_environment.php?lang=en'>Back</a></p></div><div class='col-sm-6'><p class='text-right'><input class='link-button' type='submit' value='Fill Database'></p></div></div>";
		break;
	case "de":
	default:

		$admin_subtitle = "Informationen";
		$content .= "<hr><p>Die nächste Seite kann für die nächsten Minuten leer sein und sich mehrmals neu laden. Bitte unterbrechen Sie diesen Prozess nicht.</p>";
		$content .= "<div class='row'><div class='col-sm-6'><p class=''><a class='link-button' href='./check_environment.php'>Zurück</a></p></div><div class='col-sm-6'><p class='text-right'><input class='link-button' type='submit' value='Datenbank füllen'></p></div></div>";
		break;
}

$content .= "</form>";
