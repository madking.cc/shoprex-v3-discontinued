<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


$licence_text        = file_get_contents( DIRROOT . "LICENSE.TXT" );
$external_code_text  = file_get_contents( DIRROOT . "EXTERNAL_CODE.TXT" );
$licence_text_header = " Content Management and OnlineShop Software \"shoprex\"
 Copyright (C) {2013-" . @date( "Y" ) . "}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de
";


switch ( $lang ) {
	case "en":
		$content .= "
<p>Before you proceed you have to accept the license and terms:</p>
<textarea id='terms'>\n";
		$admin_subtitle = "License";
		break;
	case "de":
	default:
		$content .= "
<p>Bevor Sie mit der Installation beginnen können, müssen Sie den Lizenzbestimmungen zustimmen:</p>
<textarea id='terms'>\n";
		$admin_subtitle = "Lizenz";
		break;
}

$content .= $licence_text_header . "\r\n\r\n\r\n" . $licence_text . "\r\n\r\n\r\n" . $external_code_text . "
</textarea>
";

switch ( $lang ) {
	case "en":
		$content .= "<p class='text-right'><a class='link-button' href='./check_environment.php?lang=en'>I accept</a></p>";
		break;
	case "de":
	default:
		$content .= "<p class='text-right'><a class='link-button' href='./check_environment.php'>Ich akzeptiere</a></p>";
		break;
}