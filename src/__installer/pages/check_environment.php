<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$php_version_id_tested        = 50445;
$mysql_version_id_tested      = 5547;
$mysql_version_id_tested_long = "5.5.47";

//PHP
$php_version = phpversion();
if ( ! defined( 'PHP_VERSION_ID' ) ) {
	$version = explode( '.', PHP_VERSION );

	define( 'PHP_VERSION_ID', ( $version[0] * 10000 + $version[1] * 100 + $version[2] ) );
}
if ( PHP_VERSION_ID >= $php_version_id_tested ) {
	$php_version_ok = true;
} else {
	$php_version_ok = false;
}

function return_memory_limit() {
	$val  = ini_get( 'memory_limit' );
	$val  = trim( $val ); // Leerzeichen entfernen
	$last = strtolower( $val[ strlen( $val ) - 1 ] ); // Typ

	switch ( $last ) // Typ
	{
		case 'g': // Gigabyte
			$val /= 1024;
			break;
		case 'm': // Megabyte
			$val *= 1; // In Zahl umwandeln
			break;
		case 'k': // Kilobyte
			$val *= 1024;
			break;
	}

	return $val;
}


$memory_limit = return_memory_limit();

if ( $memory_limit >= 64 || $memory_limit == 0 || $memory_limit == - 1 ) {
	$memory_limit_ok = true;
} else {
	$memory_limit_ok = false;
}

//MYSQL
ob_start();
phpinfo( INFO_MODULES );
$info = ob_get_contents();
ob_end_clean();
$info = stristr( $info, 'Client API version' );
preg_match( '/[1-9].[0-9].[1-9][0-9]/', $info, $match );
$mysql_version = $match[0];

$mysql_version_id = str_replace( ".", "", $mysql_version );
if ( $mysql_version_id >= $mysql_version_id_tested ) {
	$mysql_version_ok = true;
} else {
	$mysql_version_ok = false;
}

//ZIP
if ( class_exists( "ZipArchive" ) ) {
	$zip_available = true;
} else {
	$zip_available = false;
}

if ( CRYPT_BLOWFISH != 1 ) {
	$bcrypt_available = false;
} else {
	$bcrypt_available = true;
}

//Writeable Directorys
$check_write_access_dirs         = array( "download", "files", "layout", "log", "plugins", "settings" );
$check_write_access_dirs_result  = array( true, true, true, true, true, true );
$check_write_access_files        = array( ".htaccess", "__admin/.htaccess", "robots.txt", "sitemap.xml" );
$check_write_access_files_result = array( true, true, true, true );

function check_subdirs_writeable( $dir ) {
	global $loc;


	$sub_dirs = $loc->get_sub_dirs( $dir );

	if ( is_array( $sub_dirs ) && sizeof( $sub_dirs ) > 0 ) {
		foreach ( $sub_dirs as $sub_dir ) {
			if ( ! is_writable( DIRROOT . $dir . "/" . $sub_dir ) ) {
				return false;
				break;
			} else {
				$files = $loc->get_dir_content( $dir . "/" . $sub_dir );
				if ( is_array( $files ) && sizeof( $files ) > 0 ) {
					foreach ( $files as $file ) {

						if ( strpos( $sub_dir, "/" ) == ( strlen( $sub_dir ) - 1 ) ) {
							$slash = "";
						} else {
							$slash = "/";
						}
						if ( strpos( $dir, "/" ) == ( strlen( $dir ) - 1 ) ) {
							$slash2 = "";
						} else {
							$slash2 = "/";
						}

						if ( ! is_writable( DIRROOT . $dir . $slash2 . $sub_dir . $slash . $file ) ) {
							return false;
							break;
						}
					}
				}

				if ( ! check_subdirs_writeable( $dir . "/" . $sub_dir ) ) {
					return false;
					break;
				}

			}
		}
	}

	return true;
}


foreach ( $check_write_access_dirs as $key => $dir ) {
	if ( ! is_writable( DIRROOT . $dir ) ) {
		$check_write_access_dirs_result[ $key ] = false;
		continue;
	} else {
		$files = $loc->get_dir_content( $dir );
		foreach ( $files as $file ) {

			if ( strpos( $dir, "/" ) == ( strlen( $dir ) - 1 ) ) {
				$slash = "";
			} else {
				$slash = "/";
			}

			if ( ! is_writable( DIRROOT . $dir . $slash . $file ) ) {
				$check_write_access_dirs_result[ $key ] = false;
				break;
			}
		}
		if ( ! $check_write_access_dirs_result[ $key ] ) {
			continue;
		}


		$sub_dirs = $loc->get_sub_dirs( $dir );

		foreach ( $sub_dirs as $sub_dir ) {
			if ( ! is_writable( DIRROOT . $dir . "/" . $sub_dir ) ) {
				$check_write_access_dirs_result[ $key ] = false;
				break;
			} else {

				$files = $loc->get_dir_content( $dir . "/" . $sub_dir );

				if ( is_array( $files ) && sizeof( $files ) > 0 ) {
					foreach ( $files as $file ) {
						if ( strpos( $dir, "/" ) == ( strlen( $dir ) - 1 ) ) {
							$slash = "";
						} else {
							$slash = "/";
						}
						if ( strpos( $sub_dir, "/" ) == ( strlen( $sub_dir ) - 1 ) ) {
							$slash2 = "";
						} else {
							$slash2 = "/";
						}

						if ( ! is_writable( DIRROOT . $dir . $slash . $sub_dir . $slash2 . $file ) ) {
							$check_write_access_dirs_result[ $key ] = false;
							break;
						}
					}
				}
				if ( ! $check_write_access_dirs_result[ $key ] ) {
					break;
				}


				if ( ! check_subdirs_writeable( $dir . "/" . $sub_dir ) ) {
					$check_write_access_dirs_result[ $key ] = false;
					break;
				}
			}
		}
	}
}

foreach ( $check_write_access_files as $key => $file ) {
	if ( ! is_writable( DIRROOT . $file ) ) {
		$check_write_access_files_result[ $key ] = false;
		continue;
	}
}


switch ( $lang ) {
	case "en":
		$content .= "<h3>Environment Status</h3><hr>\n";
		$admin_subtitle                      = "Environment";
		$txt_php_version                     = "PHP Version:";
		$txt_php_version_error               = "<br />This software was developed under the version id " . $php_version_id_tested . ". A lower ID might work, but not tested. It has to be PHP 5.";
		$txt_mysql_version                   = "MySQL Version:";
		$txt_mysql_version_error             = "<br />This software was developed under the version " . $mysql_version_id_tested_long . ". A lower version might work, but not testet. It has to be MySQL 5 Database.";
		$txt_zip_functionality               = "ZIP Functionality:";
		$txt_zip_error                       = "<br />With out the PHP Class ZipArchive the log files will not be compressed. This software is still functional.";
		$txt_bcrypt                          = "Bcrypt Blowfish Support";
		$txt_bcrypt_error                    = "<br />This lack of encryption functionality no passwords can be generated, stored and checked.";
		$txt_header_writeable_dirs_and_files = "Writeable directories with their subdirectories and files:";
		$txt_writeable_files                 = "Writeable files:";
		$txt_min_memory                      = "At least. 64 megabytes of memory limit:";
		$txt_min_memory_error                = "<br />You probably do not have enough memory available to populate the database.";
		break;
	case "de":
	default:
		$content .= "<h3>Status zur Umgebung</h3><hr>\n";
		$admin_subtitle                      = "Umgebung";
		$txt_php_version                     = "PHP Version:";
		$txt_php_version_error               = "<br />Die Software wurde unter der Versions ID " . $php_version_id_tested . " geschrieben. Eine niedrigere PHP5 Version wird vermutlich funktionieren, ist aber nicht getestet.";
		$txt_mysql_version                   = "MySQL Version:";
		$txt_mysql_version_error             = "<br />Die Software wurde unter der Versions " . $mysql_version_id_tested_long . " geschrieben. Eine niedrigere mySQL5 Version wird vermutlich funktionieren, ist aber nicht getestet.";
		$txt_zip_functionality               = "ZIP Funktionalität:";
		$txt_zip_error                       = "<br />Durch die fehlende Zip Funktionalität (PHP Klasse ZipArchive) werden die Log Files nicht mehr gepackt, die Software ist trotzdem funktionsfähig.";
		$txt_bcrypt                          = "Bcrypt Blowfish Unterstützung";
		$txt_bcrypt_error                    = "<br />Durch diese fehlende Verschlüsselungsfunktionalität können keine Passworter generiert, gespeichert und geprüft werden.";
		$txt_header_writeable_dirs_and_files = "Beschreibbare Verzeichnisse mit ihren Unterverzeichnissen und Dateien:";
		$txt_writeable_files                 = "Beschreibbare Dateien:";
		$txt_min_memory                      = "Mind. 64 Megabyte Speicher Limit:";
		$txt_min_memory_error                = "<br />Sie haben wahrscheinlich nicht genügend Speicher verfügbar, um die Datenbank zu füllen.";
		break;
}


$content .= "<div class='row'><div class='col-sm-12'>

<p><b>$txt_php_version</b> $php_version (" . PHP_VERSION_ID . ") " . get_status_icon( $php_version_ok );
if ( ! $php_version_ok ) {
	$content .= $txt_php_version_error;
}
$content .= "</p>
<p><b>$txt_mysql_version</b> " . $mysql_version . " " . get_status_icon( $mysql_version_ok );
if ( ! $php_version_ok ) {
	$content .= $txt_mysql_version_error;
}
$content .= "</p>
<p><b>$txt_min_memory</b> " . get_status_icon( $memory_limit_ok );
if ( ! $memory_limit_ok ) {
	$content .= $txt_min_memory_error;
}
$content .= "</p>
<p><b>$txt_zip_functionality</b> " . get_status_icon( $zip_available );
if ( ! $zip_available ) {
	$content .= $txt_zip_error;
}
$content .= "</p>
<p><b>$txt_bcrypt</b> " . get_status_icon( $bcrypt_available );
if ( ! $bcrypt_available ) {
	$content .= $txt_bcrypt_error;
}
$content .= "</p>

<hr>
<p><b>$txt_header_writeable_dirs_and_files</b></p>\n";

foreach ( $check_write_access_dirs as $key => $dir ) {
	$content .= "<p>/" . $loc->web_root . $dir . ": " . get_status_icon( $check_write_access_dirs_result[ $key ] ) . "</p>\n";

}

$content .= "
<hr>
<p><b>$txt_writeable_files</b></p>\n";

foreach ( $check_write_access_files as $key => $file ) {
	$content .= "<p>/" . $loc->web_root . $file . ": " . get_status_icon( $check_write_access_files_result[ $key ] ) . "</p>\n";

}

$content .= "
</div></div>";


switch ( $lang ) {
	case "en":
		$content .= "<div class='row'><div class='col-sm-6'><p class=''><a class='link-button' href='./?lang=en'>Back</a></p></div><div class='col-sm-6'><p class='text-right'><a class='link-button' href='./set_database_and_admin.php?lang=en'>Next</a></p></div></div>";
		break;
	case "de":
	default:
		$content .= "<div class='row'><div class='col-sm-6'><p class=''><a class='link-button' href='./'>Zurück</a></p></div><div class='col-sm-6'><p class='text-right'><a class='link-button' href='./set_database_and_admin.php'>Weiter</a></p></div></div>";
		break;
}