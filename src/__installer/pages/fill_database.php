<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

if ( session_status() !== PHP_SESSION_ACTIVE ) {
	session_start();
}

$error         = false;
$disable_flags = true;

$max_execution_time = ini_get( 'max_execution_time' );
if ( is_numeric( $max_execution_time ) && $max_execution_time > 29 ) {
	$time_max = intval( $max_execution_time * 0.7524 );
} else {
	$time_max = 0;
}
$time_start = time();

define( "ARRAY_PAIRS", true );


$db_files = array(
	0  => array( "db_drop_tables.php" => 0 ),
	1  => array( "db_structure.php" => 0 ),
	2  => array(
		"db_data_root.php" => array(
			0 => array( "accounts_admin" => 0 ),
			1 => array( "country_list" => 0 ),
			2 => array( "language_admin" => 0 ),
			3 => array( "language_admin_settings" => 0 ),
			4 => array( "language_frontend" => 0 ),
			5 => array( "language_settings" => 0 ),
			6 => array( "settings" => 0 ),
			7 => array( "settings_by_language" => 0 )
		)
	),
	3  => array(
		"db_data_geoip_part1.php" => array(
			0 => array( "big_geoip" => 0 )
		)
	),
	4  => array(
		"db_data_geoip_part2.php" => array(
			0 => array( "big_geoip" => 0 )
		)
	),
	5  => array(
		"db_data_geoip_part3.php" => array(
			0 => array( "big_geoip" => 0 )
		)
	),
	6  => array(
		"db_data_geoip_part4.php" => array(
			0 => array( "big_geoip" => 0 )
		)
	),
	7  => array(
		"db_data_geoip_part5.php" => array(
			0 => array( "big_geoip" => 0 )
		)
	),
	8  => array(
		"db_data_geoip_part6.php" => array(
			0 => array( "big_geoip" => 0 )
		)
	),
	9  => array(
		"db_data_geoip_part7.php" => array(
			0 => array( "big_geoip" => 0 )
		)
	),
	10 => array(
		"db_data_geoip_part8.php" => array(
			0 => array( "big_geoip" => 0 )
		)
	),
	11 => array(
		"db_data_example.php" => array(
			0  => array( "blog_content" => 0 ),
			1  => array( "blog_link" => 0 ),
			2  => array( "blog_section" => 0 ),
			3  => array( "carousel" => 0 ),
			4  => array( "carousel_content" => 0 ),
			5  => array( "file_download" => 0 ),
			6  => array( "menus" => 0 ),
			7  => array( "news" => 0 ),
			8  => array( "news_content" => 0 ),
			9  => array( "pages" => 0 ),
			10 => array( "text" => 0 )
		)
	)
);

$txt_db_files = array(
	0  => "Lösche ggf. vorhandene Tabellen",
	1  => "Lege Struktur an",
	2  => "Fülle mit Daten",
	3  => "Fülle GeoIP Tabelle",
	11 => "Fülle mit Beispieldaten"
);


function get_db_file( $file, $variable = null ) {
	global $lang;

	$db_array = array();
	if ( is_file( DIRINSTALL . "db_data/" . $file ) ) {
		$file_string = file_get_contents( DIRINSTALL . "db_data/" . $file );
		eval( $file_string );
		if ( ! is_null( $variable ) ) {
			return $db_array[ $variable ];
		} else {
			return $db_array;
		}
	} else {
		return false;
	}
}

function write_database( $db, &$sql_array, $row_start = 0, $table = null ) {
	global $time_start;
	global $time_max;

	$size_sql_array = sizeof( $sql_array );

	if ( $row_start < $size_sql_array ) {
		if ( is_null( $table ) ) {
			for ( $i = $row_start; $i < $size_sql_array; $i ++ ) {
				$result = $db->query( $sql_array[ $i ], __FILE__ . ":" . __LINE__ );

				if ( ! $result ) {
					return false;
				}
				$time_now   = time();
				$time_taken = $time_now - $time_start;
				if ( $time_max > 0 && $time_taken >= $time_max ) {
					return $i;
				}

			}
		} else {
			for ( $i = $row_start; $i < $size_sql_array; $i ++ ) {
				$keys   = array();
				$values = array();
				foreach ( $sql_array[ $i ] as $row_name => $data ) {
					$keys[]   = $row_name;
					$values[] = $db->escape( $data );
				}

				$result = $db->ins( __FILE__ . ":" . __LINE__, $_SESSION["db_table_prefix"] . $table, $keys, $values );
				if ( ! $result ) {
					return false;
				}
				$time_now   = time();
				$time_taken = $time_now - $time_start;
				if ( $time_max > 0 && $time_taken >= $time_max ) {
					return $i;
				}

			}

		}
	}
	if ( $db->id->connect_errno ) {
		return false;
	} else {
		return true;
	}

}

function set_session_parameter( $post_var_name ) {
	if ( isset( $_POST[ $post_var_name ] ) ) {
		$_SESSION[ $post_var_name ] = $_POST[ $post_var_name ];
	}
// todo: checkbox
}

set_session_parameter( "db_name" );
set_session_parameter( "db_pass" );
set_session_parameter( "db_database_server" );
if ( ! isset( $_SESSION["db_database_server"] ) ) {
	$_SESSION["db_database_server"] = "localhost";
}
set_session_parameter( "db_database_port" );
set_session_parameter( "db_database_name" );
set_session_parameter( "db_admin_name" );
set_session_parameter( "db_admin_pass" );
set_session_parameter( "db_table_prefix" );
set_session_parameter( "inst_admin_name" );
set_session_parameter( "inst_admin_pass" );
set_session_parameter( "inst_admin_email" );
set_session_parameter( "db_drop_tables" );
if ( ! isset( $_SESSION["db_drop_tables"] ) ) {
	$_SESSION["db_drop_tables"] = false;
}
set_session_parameter( "use_example_data" );
if ( ! isset( $_SESSION["use_example_data"] ) ) {
	$_SESSION["use_example_data"] = false;
}
set_session_parameter( "timezone" );

switch ( $lang ) {
	case "en":
		$lang_parameter = "&lang=en";
		$txt_finished   = "Database import finished!";
		$txt_db_error   = "There was a Database Error:";
		$txt_table      = "Table:";
		$txt_db_file    = "Database File:";
		$admin_subtitle = "Fill Database";
		break;
	case "de":
	default:
		$lang_parameter = "";
		$txt_finished   = "Datenbank Import Fertig!";
		$txt_db_error   = "Es gab einen Datenbank Fehler:";
		$txt_table      = "Tabelle:";
		$txt_db_file    = "Datenbank Datei:";
		$admin_subtitle = "Fülle Datenbank";
		break;
}

$db = new class_database( $_SESSION["db_database_server"], $_SESSION["db_name"], $_SESSION["db_pass"], $_SESSION["db_database_name"], $_SESSION["db_database_port"] );
if ( $db->id->connect_errno ) {
	$GLOBALS['body_footer'] .= "<script>
            parent.location.href = '" . $loc->httpauto_web_root . "set_database_and_admin.php?error=" . $db->id->connect_errno . "&account=normal" . $lang_parameter . "';
        </script>\n";

	$content .= $db->id->connect_errno;
}
if ( isset( $_SESSION["db_admin_name"] ) && ! empty( $_SESSION["db_admin_name"] ) ) {
	unset( $db );
	$db = new class_database( $_SESSION["db_database_server"], $_SESSION["db_admin_name"], $_SESSION["db_admin_pass"], $_SESSION["db_database_name"], $_SESSION["db_database_port"] );
	if ( $db->id->connect_errno ) {
		$GLOBALS['body_footer'] .= "<script>
            parent.location.href = '" . $loc->httpauto_web_root . "set_database_and_admin.php?error=" . $db->id->connect_errno . "&account=admin" . $lang_parameter . "';
        </script>\n";
	}
}

if ( ! $db->id->connect_errno ) {

	if ( isset( $_GET['file'] ) ) {
		$db_file_start = intval( $_GET['file'] );
	} else {
		if ( $_SESSION["db_drop_tables"] ) {
			$db_file_start = 0;
		} else {
			$db_file_start = 1;
		}
	}

	$db_file_max = sizeof( $db_files ) - 1;
	if ( ! $_SESSION["use_example_data"] && $db_file_start == 11 ) {
		$db_file_max -= 1;
	}


	if ( isset( $_GET['row'] ) ) {
		$db_row = intval( $_GET['row'] );
	} else {
		$db_row = 0;
	}

	if ( isset( $_GET['sub'] ) ) {
		$db_sub = intval( $_GET['sub'] );
	} else {
		$db_sub = 0;
	}

	if ( is_numeric( $db_file_start ) ) {
		for ( $i = $db_file_start; $i <= $db_file_max; $i ++ ) {
			foreach ( $db_files[ $i ] as $db_file => $db_array_counter ) {

				if ( is_array( $db_array_counter ) ) {
					$table_array_size = sizeof( $db_array_counter );
					for ( $j = $db_sub; $j < $table_array_size; $j ++ ) {
						foreach ( $db_array_counter[ $j ] as $db_table => $db_table_counter ) {
							$db_array = get_db_file( $db_file, $db_table );
							$result   = write_database( $db, $db_array, $db_row, $db_table );

							if ( $result === false ) {
								$error = true;
								$content .= "<p>" . $txt_db_error . "</p><p>" . $db->id->error . "</p><p>" . $txt_table . " '" . $_SESSION["db_table_prefix"] . $db_table . "'";
								break 4;
							} elseif ( $result === true ) {
								continue;
							} elseif ( $result >= 0 ) {
								$GLOBALS['body_footer'] .= "<script>
            parent.location.href = '" . $loc->httpauto_web_root . "fill_database.php?file=" . $i . "&sub=$j&row=" . ( $result + 1 ) . $lang_parameter . "';
        </script>\n";
								break 4;
							}

						}
					}
				} else {
					$db_array = get_db_file( $db_file );
					$result   = write_database( $db, $db_array, $db_row );
					if ( $result === false ) {
						$error = true;
						$content .= "<p>" . $txt_db_error . "</p><p>" . $db->id->error . "</p><p>" . $txt_db_file . " '" . $db_file . "'";
						break 2;
					} elseif ( $result === true ) {
						continue;
					} elseif ( $result >= 0 ) {

						$GLOBALS['body_footer'] .= "<script>
            parent.location.href = '" . $loc->httpauto_web_root . "fill_database.php?file=" . $i . "&sub=$j&row=" . ( $result + 1 ) . $lang_parameter . "';
        </script>\n";
						break 2;
					}
				}
				$GLOBALS['body_footer'] .= "<script>
            parent.location.href = '" . $loc->httpauto_web_root . "fill_database.php?file=" . ( $i + 1 ) . $lang_parameter . "';
        </script>\n";
				break 2;
			}


		}

	}

	if ( ! $error && ( $db_file_start > $db_file_max ) ) {
		$content .= "<p>$txt_finished</p>\n";

		switch ( $lang ) {
			case "en":
				$content .= "<hr><p>Next, the files are written and deleted the '__installer' directory. You are then automatically routed to the admin area.</p>";
				$content .= "<div class='row'><div class='col-sm-6'><p class=''><a class='link-button' href='./set_database_and_admin.php?lang=en'>Back</a></p></div><div class='col-sm-6'><p class='text-right'><a class='link-button' href='./write_files.php?lang=en'>Next</a></p></div></div>";
				break;
			case "de":
			default:
				$content .= "<hr><p>Im nächsten Schritt werden die Dateien geschrieben und das '__installer' Verzeichnis gelöscht. Sie werden danach automatisch in den Adminbereich geleitet.</p>";
				$content .= "<div class='row'><div class='col-sm-6'><p class=''><a class='link-button' href='./set_database_and_admin.php'>Zurück</a></p></div><div class='col-sm-6'><p class='text-right'><a class='link-button' href='./write_files.php'>Weiter</a></p></div></div>";
				break;
		}

	}
}