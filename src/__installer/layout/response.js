$(document).ready(function () {
    $("img:not(:has('.nr'),:has('img-not-responsive'),:has('table img'))").addClass("img-responsive");
    $("table:not(:has('.nr'))").addClass("table").wrap("<div class='table-responsive'></div>");

});