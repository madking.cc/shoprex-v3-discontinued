<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$this_dir_root = realpath( __DIR__ ) . "/";
if ( ! isset( $GLOBALS['head_top'] ) ) {
	$GLOBALS['head_top'] = "";
}
if ( ! isset( $GLOBALS['head_bottom'] ) ) {
	$GLOBALS['head_bottom'] = "";
}
if ( ! isset( $GLOBALS['custom_css'] ) ) {
	$GLOBALS['custom_css'] = "";
}
if ( ! isset( $header_code ) ) {
	$header_code = "";
}
if ( ! isset( $admin_title ) ) {
	$admin_title = $GLOBALS['page_title'];
}
if ( ! isset( $admin_subtitle ) ) {
	if ( ! isset( $GLOBALS['admin_subtitle'] ) ) {
		$admin_subtitle = $GLOBALS['subtitle'];
	} else {
		$admin_subtitle = $GLOBALS['admin_subtitle'];
	}
}

if ( ! isset( $admin_subtitle ) || empty( $admin_subtitle ) || ! isset( $admin_title ) || empty( $admin_title ) ) {
	$separator = "";
} else {
	$separator = " - ";
}
if ( ! isset( $admin_body_class ) ) {
	$admin_body_class = "";
}

$header_code .= "<!DOCTYPE html>
<html lang=\"" . $lang . "\">
    <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
" . $GLOBALS['head_top'] . "
    <title>" . $admin_subtitle . $separator . $admin_title . "</title>

    <meta name=\"robots\"
        content=\"noindex, nofollow\" />\n";

if ( isset( $GLOBALS['head'] ) && ! empty( $GLOBALS['head'] ) ) {
	foreach ( $GLOBALS['head'] as $value ) {
		$header_code .= $value;
	}
}

if ( ! isset( $disable_flags ) ) {
	$disable_flags = false;
}

$header_code .= $GLOBALS["head_bottom"];
$header_code .= "    <link rel=\"stylesheet\"
          href=\"/" . $loc->web_root . "__installer/layout/style.css\" media=\"all\" />\n";

$header_code .= $GLOBALS["custom_css"] . "    </head>

    <body class=\"" . $admin_body_class . "\" style=''>
    <div class='container'>
        <div class='row'>
            <div class='col-sm-12'>
    <div class='content_wrapper'>
    <div class='flags_div'>\n";
if ( ! $disable_flags ) {
	$header_code .= "<ul class='flags'>
 <li><a href=\"" . $loc->httpauto_web_root . $loc->current_file . "\" title=\"Seite in Deutsch\" class=\"link\"><img class='nr' src=\"" . $loc->web_root . "__installer/flags/flag_239.png\"  alt=\"\" class=\"img\"></a></li>
 <li><a href=\"" . $loc->httpauto_web_root . $loc->current_file . "?lang=en\" title=\"Page in English\" class=\"link\"><img class='nr' src=\"" . $loc->web_root . "__installer/flags/flag_017.png\"  alt=\"\" class=\"img\"></a></li>
</ul>\n";
}
$header_code .= "    </div>
    \n";
