<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_root {
	protected $log;

	public function escape( $text ) {
		return htmlentities( $text, ENT_QUOTES, "UTF-8" );
	}

	public function get_time() {
		return @date( "Y-m-d H:i:s" );
	}

	public function text2html( $text ) {
		$text = str_replace( "  ", "&nbsp;&nbsp;", $text );
		$text = str_replace( "\r\n", "<br />", $text );

		return $text;
	}

	public function start_session() {
		if ( session_status() !== PHP_SESSION_ACTIVE ) {
			session_start();
		}
	}

	public function cast_amp( $string ) {
		return str_replace( "&", "&amp;", $string );
	}


	public function get_user_agent( $escape = true ) {
		if ( isset( $_SERVER['HTTP_USER_AGENT'] ) && ! empty( $_SERVER['HTTP_USER_AGENT'] ) ) {
			if ( $escape ) {
				return $this->escape( $_SERVER['HTTP_USER_AGENT'] );
			} else {
				return $_SERVER['HTTP_USER_AGENT'];
			}
		} else {
			return null;
		}
	}

	public function get_referer( $escape = true ) {
		if ( isset( $_SERVER['HTTP_REFERER'] ) && ! empty( $_SERVER['HTTP_REFERER'] ) ) {
			if ( $escape ) {
				return $this->escape( $_SERVER['HTTP_REFERER'] );
			} else {
				return $_SERVER['HTTP_REFERER'];
			}
		} else {
			return null;
		}
	}

	public function get_user_ip( $escape = true, $masked = true, $encoded = false ) {
		// IP Adresse des Besuchers

		if ( $encoded ) {
			if ( ! isset( $_SERVER['REMOTE_ADDR'] ) || empty( $_SERVER['REMOTE_ADDR'] ) ) {
				return null;
			} else {
				return md5( ( $_SERVER['REMOTE_ADDR'] ) . "SALT:" . IP_SALT );
			}
		} else {

			if ( $masked ) {
				$ip = $this->mask_ip( $_SERVER['REMOTE_ADDR'] );
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}

			if ( $escape ) {
				return $this->escape( $ip );
			} else {
				return $ip;
			}
		}
	}

	public function mask_ip( $ip ) {
		if ( ! isset( $ip ) || empty( $ip ) ) {
			return false;
		} else {
			$type = $this->get_ip_type( $ip );
			switch ( $type ) {
				case "ipv4":
					$last_dot  = strrpos( $ip, "." );
					$masked_ip = substr( $ip, 0, $last_dot );
					$masked_ip .= ".***";

					return $masked_ip;
				case "ipv6":
					return $ip;  // TODO: Mask ipv6
					break;
				default:
					return false;
			}
		}
	}

	public function get_ip_type( $ip ) {
		if ( ip2long( $ip ) !== false ) {
			return "ipv4";
		} elseif ( preg_match( '/^[0-9a-fA-F:]+$/', $ip ) && @inet_pton( $ip ) ) {
			return "ipv6";
		} else {
			return false;
		}
	}

	// Thanks to http://stackoverflow.com/questions/6768793/get-the-full-url-in-php
	protected function url_origin( $s, $use_forwarded_host = false ) {
		$ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' ) ? true : false;
		$sp       = strtolower( $s['SERVER_PROTOCOL'] );
		$protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
		$port     = $s['SERVER_PORT'];
		$port     = ( ( ! $ssl && $port == '80' ) || ( $ssl && $port == '443' ) ) ? '' : ':' . $port;
		$host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
		$host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;

		return $protocol . '://' . $host;
	}

	// Thanks to http://stackoverflow.com/questions/6768793/get-the-full-url-in-php
	public function full_url( $s, $use_forwarded_host = false ) {
		return $this->url_origin( $s, $use_forwarded_host ) . $s['REQUEST_URI'];
	}

	public function get_complete_uri( $escape = true ) {

		if ( $escape ) {
			return $this->escape( $this->full_url( $_SERVER ) );
		} else {
			return $this->full_url( $_SERVER );
		}
	}

	public function get_server_name( $escape = true ) {
		if ( ! isset( $_SERVER['SERVER_NAME'] ) || empty( $_SERVER['SERVER_NAME'] ) ) {
			return null;
		}

		if ( $escape ) {
			return $this->escape( $_SERVER["SERVER_NAME"] );
		} else {
			return $_SERVER["SERVER_NAME"];
		}
	}


	public function remove_injection( $string ) {
		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			return $string;
		}

		$string = str_replace( "'", "", $string );
		$string = str_replace( "\"", "", $string );
		$string = str_replace( "`", "", $string );

		return $string;
	}


	public function get_uri( $escape = true ) {
		if ( ! isset( $_SERVER['REQUEST_URI'] ) || empty( $_SERVER['REQUEST_URI'] ) ) {
			return null;
		}

		$server_uri = $this->remove_first_slash( $_SERVER['REQUEST_URI'] );

		if ( $escape ) {
			return $this->escape( $server_uri );
		} else {
			return $server_uri;
		}
	}

	public function check_first_string_match( $needle, $heystack ) {
		if ( strpos( $heystack, $needle ) === 0 ) {
			return true;
		} else {
			return false;
		}
	}

	public function delete_first_string_match( $remove, $string ) {

		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			return $string;
		}

		$remove_length = strlen( $remove );
		$string_length = strlen( $string );
		if ( $remove_length == 0 ) {
			return $string;
		} elseif ( $remove_length == $string_length ) {
			return "";
		} else {
			if ( strpos( $string, $remove ) === 0 ) {
				$result = substr( $string, $remove_length );
				//var_dump($remove_length, $string, $result);
				if ( $result === false ) {
					return "";
				} else {
					return $result;
				}
			} else {
				return $string;
			}
		}
	}

	public function remove_sid( $string ) {
		if ( ! isset( $string ) || empty( $string ) ) {
			return $string;
		} else {
			$string = preg_replace( '/&amp;sid=.+(&amp;)/i', '$1', $string );
			$string = preg_replace( '/&sid=.+(&)/i', '$1', $string );

			$string = preg_replace( '/&amp;sid=.+/i', '', $string );
			$string = preg_replace( '/&sid=.+/i', '', $string );

			$string = preg_replace( '/\?sid=.+?&amp;/i', '\?', $string );
			$string = preg_replace( '/\?sid=.+?&/i', '\?', $string );

			$string = preg_replace( '/\?sid=.+/i', '', $string );
			$string = preg_replace( '/\?sid=.+/i', '', $string );

			return $string;
		}
	}

	public function get_uri_filetype( $file ) {
		if ( is_null( $file ) || ( empty( $file ) && $file != "0" ) ) {
			return $file;
		}

		$pos_point   = strrpos( $file, "." );
		$length_file = strlen( $file );

		if ( $pos_point === false ) {
			return "";
		} elseif ( ( $pos_point == ( $length_file - 1 ) ) && $pos_point > 0 ) {
			return "";
		} elseif ( $pos_point === 0 && $length_file > 1 ) {
			return substr( $file, 1 );
		} elseif ( ( $pos_point < ( $length_file - 1 ) ) && $pos_point > 0 ) {
			return substr( $file, $pos_point + 1 );
		} else {
			return "";
		}
	}

	public function get_uri_filename( $file ) {
		if ( is_null( $file ) || ( empty( $file ) && $file != "0" ) ) {
			return $file;
		}

		$pos_point   = strrpos( $file, "." );
		$length_file = strlen( $file );

		if ( $pos_point === false ) {
			return $file;
		} elseif ( ( $pos_point == ( $length_file - 1 ) ) && $pos_point > 0 ) {
			return substr( $file, 0, - 1 );
		} elseif ( $pos_point === 0 ) {
			return "";
		} elseif ( ( $pos_point < ( $length_file - 1 ) ) && $pos_point > 0 ) {
			return substr( $file, 0, $pos_point );
		} else {
			return "";
		}
	}

	public function get_parameter_line( $uri ) {
		if ( is_null( $uri ) || ( empty( $uri ) && $uri != "0" ) ) {
			return $uri;
		}

		$pos = strpos( $uri, "?" );
		if ( $pos === false ) {
			return "";
		} else {
			return substr( $uri, $pos + 1 );
		}
	}

	public function get_dir( $url, $no_file = false ) {
		if ( is_null( $url ) || ( empty( $url ) && $url != "0" ) ) {
			return "";
		}

		$pos = strpos( $url, "?" );
		if ( ! ( $pos === false ) ) {
			if ( $pos === 0 ) {
				return "";
			} else {

				$url = substr( $url, 0, $pos + 1 );
			}
		}

		$lastpos = strrpos( $url, "/" );
		$length  = strlen( $url );

		if ( $lastpos === false ) {
			return "";
		} elseif ( $lastpos === 0 ) {
			return "";
		} elseif ( $lastpos == ( $length - 1 ) ) {
			return $this->remove_first_slash( $url );
		} else {
			return $this->remove_first_slash( substr( $url, 0, $lastpos + 1 ) );
		}
	}

	public function remove_first_slash( $string ) {
		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			return $string;
		}

		if ( strpos( $string, "/" ) === 0 ) {
			return substr( $string, 1 );
		} else {
			return $string;
		}
	}

	public function get_preview_status() {
		if ( defined( 'PREVIEW_MODE' ) ) {
			return PREVIEW_MODE;
		} else {
			return false;
		}
	}

	public function txt( $const ) {
		$not_defined    = "Text not defined";
		$const_original = $const;
		$const          = trim( $const );
		$const          = str_replace( " ", "_", $const );
		$const_upper    = strtoupper( $const );
		$error_txt      = "Constant '" . $const_upper . "' and 'AL_" . $const_upper . "' not defined. (Orignal Constant: '$const_original'.";

		if ( $GLOBALS['is_admin'] ) {
			if ( defined( "AL_" . $const_upper ) ) {
				return constant( "AL_" . $const_upper );
			} elseif ( defined( $const_upper ) ) {
				return constant( $const_upper );
			}
		} else {
			if ( defined( $const_upper ) ) {
				return constant( $const_upper );
			} elseif ( defined( "AL_" . $const_upper ) ) {
				return constant( "AL_" . $const_upper );
			}
		}
		if ( method_exists( $this->log, 'error' ) ) {
			$this->log->error( "lang", __FILE__ . ":" . __LINE__, $error_txt );
		} else {
			$this->log( __FILE__ . ":" . __LINE__, "error", "lang", $error_txt );
		}

		return $not_defined;
	}

	public function ip2str( $ip = "" ) {
		if ( empty( $ip ) ) {
			$ip = $this->get_ipv4();
		}

		$ip = preg_replace( "/(\d{1,3})\.?/e", 'sprintf("%03d", \1)', $ip );

		return (string) $ip;
	}

	public function get_ipv4( $escape = true ) {
		if ( $escape ) {
			$addr = $this->escape( $_SERVER['REMOTE_ADDR'] );
		} else {
			$addr = $_SERVER['REMOTE_ADDR'];
		}

		return $addr;
	}

	public function unique_id( $big = true ) {
		if ( $big ) {
			return uniqid( md5( rand() ), true );
		} else {
			return uniqid( md5( rand() ), false );
		}
	}

	public function check_name( $name, $no_numbers = false ) {
		if ( ! $no_numbers ) {
			if ( ! preg_match( '/^[0-9a-zßüäö[:space:]]+$/i', $name ) ) {
				return false;
			}
		} else {
			if ( ! preg_match( '/^[a-zßüäö[:space:]]+$/i', $name ) ) {
				return false;
			}
		}
	}

	public function check_email( $email ) {

		if ( ! preg_match( '/^([a-z0-9]+([-_\.]?[a-z0-9])+)@[a-z0-9äöü]+([-_\.]?[a-z0-9äöü])+\.[a-z]{2,4}$/i', $email ) ) {
			return false;
		} else {
			return true;
		}
	}

	public function delete_redundant_spaces( $string ) {
		$tmp = preg_replace( "#[ ]{2,}#", " ", $string );

		return $tmp;
	}

	public function event( $type, $location, $text, $escape = true ) {
		if ( ! is_array( $GLOBALS["early_event"] ) ) {
			$GLOBALS["early_event"] = array();
		}
		$GLOBALS["early_event"][] = array( "type" => $type, "location" => $location, "text" => $text );
	}

	// Füge Fehlerereignis hinzu

	public function error( $type, $location, $text, $escape = true ) {
		if ( ! is_array( $GLOBALS["early_error"] ) ) {
			$GLOBALS["early_error"] = array();
		}
		$GLOBALS["early_error"][] = array( "type" => $type, "location" => $location, "text" => $text );
	}

	// Logge Anderes

	public function notice( $type, $location, $text, $escape = true ) {
		if ( ! isset( $GLOBALS["early_notice"] ) || ! is_array( $GLOBALS["early_notice"] ) ) {
			$GLOBALS["early_notice"] = array();
		}
		var_dump( $type, $location, $text );
		$GLOBALS["early_notice"][] = array( "type" => $type, "location" => $location, "text" => $text );
	}

	// Logge Mail
	public function mail( $type, $location, $text, $escape = true ) {
		if ( ! is_array( $GLOBALS["early_mail"] ) ) {
			$GLOBALS["early_mail"] = array();
		}
		$GLOBALS["early_mail"][] = array( "type" => $type, "location" => $location, "text" => $text );
	}
}