<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_location extends class_uri_file_path_operations {
	protected static $_instance = null;

	public $rewrite_mode;
	public $rewrite_mode_using;
	public $dir_root;
	public $web_root;
	public $uri;
	public $uri_untouched;
	public $uri_inside;     // Without Webroot and lang subdir (but with file)
	public $uri_clean;      // and without Parameter
	public $is_https;
	public $server_name;
	public $httpauto;
	public $httpauto_root;      // With Domain
	public $httpauto_web_root;
	public $path_array;
	public $current_file;
	public $current_filename;
	public $current_filetype;
	public $path;
	public $parameter_line;
	public $parameter_line_casted;
	public $parameter_array;
	public $current_dir;
	public $current;
	public $not_found;


	public function __construct() {
		parent::__construct();

		$this->dir_root      = DIRROOT;
		$this->server_name   = $this->remove_not_allowed_chars_in_uri( $this->get_server_name( NOT_ESCAPED ) ) . "/";
		$this->uri_untouched = urldecode( $this->get_uri( NOT_ESCAPED ) );
		$this->uri           = $this->remove_not_allowed_chars_in_uri( $this->uri_untouched );

		if ( isset( $_SERVER['HTTPS'] ) && ( $_SERVER['HTTPS'] == '1' || strtolower( $_SERVER['HTTPS'] ) == 'on' ) ) {
			$this->is_https = true;
			$this->httpauto = "https://";
		} else {
			$this->is_https = false;
			$this->httpauto = "http://";
		}

		// Security
		if ( strcasecmp( $this->uri, $this->uri_untouched ) != 0 ) {
			$this->log->notice( "security", __FILE__ . ":" . __LINE__, "Reload, Uri Violation. Original: '" . $this->uri_untouched . "', Fixed: '" . $this->uri . "'" );
			session_write_close();
			header( 'Location: ' . $this->httpauto . $this->server_name . $this->uri );
			die();
		}


		$this->httpauto_root = $this->httpauto . $this->server_name;

		if ( strrpos( $this->uri, "/" ) === false ) {
			$this->web_root   = "";
			$this->uri_inside = "/";
		} else {
			$last_slash_pos   = strrpos( $this->uri, "/" );
			$this->web_root   = substr( $this->uri, 0, $last_slash_pos + 1 );
			$this->uri_inside = $this->delete_first_string_match( $this->web_root, $this->uri );
		}

		$this->httpauto_web_root = $this->httpauto . $this->server_name . $this->web_root;

		$this->uri_inside = $this->delete_first_string_match( $this->web_root, $this->uri );

		$this->extract_informations_from_uri_inside();
		// $this->current_file
		// $this->current_filename
		// $this->current_filetype
		// $this->uri_clean
		// $this->path
		// $this->parameter_line
		// $this->parameter_line_casted
		// $this->current_dir
		// $this->current


		// Check Rewrite
		$this->rewrite_mode = false;
		if ( array_key_exists( 'HTTP_MOD_REWRITE', $_SERVER ) ) {
			if ( strtolower( $_SERVER['HTTP_MOD_REWRITE'] ) == "on" ) {
				$this->rewrite_mode = true;
			}
		} elseif ( array_key_exists( 'REDIRECT_HTTP_MOD_REWRITE', $_SERVER ) ) {
			if ( strtolower( $_SERVER['REDIRECT_HTTP_MOD_REWRITE'] ) == "on" ) {
				$this->rewrite_mode = true;
			}
		}
	}


	public static function getInstance() {
		if ( null === self::$_instance ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	protected function __clone() {
	}

	protected function extract_informations_from_uri_inside() {
		// Speichere Pfad in einem Array (path_array) und als String in (path)
		$this->uri_clean   = $this->uri_inside;
		$pos_question_mark = strpos( $this->uri_clean, "?" );
		if ( ! ( $pos_question_mark === false ) ) {
			$this->uri_clean = substr( $this->uri_clean, 0, $pos_question_mark );
		}
		$i                = 0;
		$this->path_array = array();
		$found            = false;
		$uri_tmp          = $this->uri_clean;
		while ( ! ( ( $pos_slash = strpos( $uri_tmp, "/" ) ) === false ) ) // Gehe Verzeichnisse durch
		{
			if ( $pos_slash === 0 ) {
				$uri_tmp = substr( $uri_tmp, $pos_slash + 1 );
				continue;
			}
			$found                  = true;
			$this->path_array[ $i ] = substr( $uri_tmp, 0, $pos_slash ); // Speichere Verzeichnis
			$uri_tmp                = str_replace( $this->path_array[ $i ] . "/", "", $uri_tmp ); // Entferne Verzeichnis
			$i ++; // Zähler erhöhen
		}
		// End
		// Falls ein Dateiname noch übrig
		if ( strlen( $uri_tmp ) > 0 || ! $found ) {
			$this->current_file = $uri_tmp;
		} else {
			$this->current_file = "";
		}
		$this->path = "";
		foreach ( $this->path_array as $subdir ) {
			$this->path .= $subdir . "/";
		}

		$this->current_file     = $this->remove_not_allowed_file_chars( $this->current_file );
		$this->current_file     = $this->check_for_not_allowed_filenames( $this->current_file, __FILE__ . ":" . __LINE__ );
		$this->current_filetype = $this->get_uri_filetype( $this->current_file );
		$this->current_filetype = $this->check_for_not_allowed_filenames( $this->current_filetype, __FILE__ . ":" . __LINE__ );
		$this->current_filename = $this->get_uri_filename( $this->current_file );
		$this->current_filename = $this->check_for_not_allowed_filenames( $this->current_filename, __FILE__ . ":" . __LINE__ );

		$this->parameter_line        = $this->get_parameter_line( $this->uri_inside );
		$this->parameter_line_casted = $this->cast_amp( $this->parameter_line );
		parse_str( $this->parameter_line, $this->parameter_array );
		$this->current_dir = $this->get_dir( $this->uri_inside );
		$this->current     = $this->current_dir . $this->current_file;

	}


	public function get_complete_url() {
		return $this->httpauto_root . $this->uri;
	}

	public function compare_path_with_current( $path ) {
		if ( strcasecmp( $path, $this->current ) === 0 ) {
			return true;
		} elseif ( empty( $path ) && empty( $this->current ) ) {
			return true;
		} else {
			if ( strpos( $this->current, "index.php" ) === false ) {
				return false;
			} else {
				// TODO: last index.php
				$index_path = str_replace( "/index.php", "", $this->current );
				if ( strcasecmp( $path, $index_path ) === 0 ) {
					return true;
				} else {
					$index_path = str_replace( "index.php", "", $this->current );
					if ( strcasecmp( $path, $index_path ) === 0 ) {
						return true;
					} else {
						return false;
					}
				}
			}
		}
	}
}