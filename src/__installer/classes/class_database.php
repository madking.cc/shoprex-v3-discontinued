<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


class class_database extends class_root {
	protected static $_instance = null;

	public $id;
	protected $error;
	protected $db_server;
	protected $db_user;
	protected $db_pass;
	protected $db_table;
	protected $db_port;


	public function __construct( $db_server = "", $db_user = "", $db_pass = "", $db_table = "", $db_port = "", $char_coding = "utf8" ) {
		if ( ( ! isset( $db_port ) || empty( $db_port ) ) ) {
			$db_port = "3306";
		}

		@$this->id = new mysqli( $db_server, $db_user, $db_pass, $db_table, $db_port );
		$this->db_server = $db_server;
		$this->db_user   = $db_user;
		$this->db_pass   = $db_pass;
		$this->db_table  = $db_table;
		$this->db_port   = $db_port;

		$this->id->set_charset( $char_coding );
		$this->id->query( "SET NAMES $char_coding" );
		$this->id->query( "SET CHARACTER SET $char_coding" );
	}

	public function __destruct() {
		// Datenbank Verbindung schliessen
		if ( ! $this->id->connect_errno ) {
			$this->id->close();
		}
	}

	public function get_handler() {
		// Gebe den Handler zur Datenbank Verbindung zurück
		return $this->id;
	}

	public function get_affected_rows() {
		// Gebe die Anzahl der betroffenen Zeilen nach dem letzten Datenbank Befehl zurück
		return $this->id->affected_rows;
	}


	// Datenbank Execute

	public function get_insert_id() {
		// Gebe die id-Nummer des vorher ausgeführten INSERT Befehls zurück
		return $this->id->insert_id;
	}

	public function get_error() {
		// Gebe Fehler zurück
		return $this->id->error;
	}

	public function close() {
		// Schliesse Datenbank Verbindung
		$this->id->close();
	}

	public function escape( $string ) {
		// Escape eine Zeichensatzkette für einen Datenbank Eintrag oder Update
		return $this->id->escape_string( $string );
	}

	public function upd( $location, $table, $keys, $values, $where = "", $limit = "" ) {
		if ( ! $this->check_pair( $keys, $values, $location ) ) {
			return false;
		}

		$keys   = $this->fit_keys( $keys );
		$values = $this->fit_values( $values );

		$set = "SET ";
		foreach ( $keys as $key => $value ) {
			$set .= $keys[ $key ] . " = " . $values[ $key ] . ", ";
		}
		$set = substr( $set, 0, - 2 );

		$where_sql = "";
		if ( ! empty( $where ) ) {
			$where_sql = "WHERE " . $where;
		}

		$limit_sql = "";
		if ( ! empty( $limit ) ) {
			$limit_sql = "LIMIT " . $limit;
		}

		$sql    = "UPDATE `" . $table . "` $set $where_sql $limit_sql;";
		$result = $this->query( $sql, $location );

		return $result;
	}

	protected function check_pair( $keys, $values, $location = "" ) {
		if ( ! is_array( $keys ) || ! is_array( $values ) ) {
			$this->log->error( "sql", $location, "Keys or Values for update are not a array" );
			$this->error = new class_throw_error( $location . " Keys or Values for update are not a array" );

			return false;
		} elseif ( sizeof( $keys ) != sizeof( $values ) ) {
			$this->log->error( "sql", $location, "Keys and Values sizeof mismatch. Keys: " . sizeof( $keys ) . ", values: " . sizeof( $values ) );
			$this->error = new class_throw_error( "sql: " . $location . " Keys and Values sizeof mismatch. Keys: " . sizeof( $keys ) . ", values: " . sizeof( $values ) );

			return false;
		} else {
			return true;
		}
	}

	protected function fit_keys( $keys ) {
		foreach ( $keys as $key => $value ) {
			$keys[ $key ] = "`" . $value . "`";
		}

		return $keys;
	}

	protected function fit_values( $values ) {
		foreach ( $values as $key => $value ) {
			$values[ $key ] = $this->fit_value( $value );
		}

		return $values;
	}

	public function fit_value( $var ) {
		$null_result = strcasecmp( $var, "NULL" );
		$now_result  = strcasecmp( $var, "NOW()" );
		if ( ! isset( $var ) || $var === "" || is_null( $var ) ) {
			return "NULL";
		}
		if ( $now_result === 0 || $null_result === 0 ) {
			return $var;
		} elseif ( $var === false ) {
			return "'0'";
		} elseif ( $var === true ) {
			return "'1'";
		} else {
			return "'" . $var . "'";
		}
	}

	public function query( $sql, $location = "Unknown" ) {
		$result = $this->id->query( $sql );
		// Falls Fehler:
		if ( ! $result ) {
			// Loge Fehler
			//$this->log->error("sql", $location, "SQL Query Error: " . $this->id->error . ", SQL: " . $sql);
			// Gebe Befehl an die Error-Klasse
			//$this->error = new class_throw_error("sql: " . $location . " SQL Query Error: " . $this->id->error . ", SQL: " . $sql);
		}

		return $result;
	}

	public function ins( $location, $table, $keys, $values, $limit = "" ) {
		if ( ! $this->check_pair( $keys, $values, $location ) ) {
			return false;
		}

		$keys   = $this->fit_keys( $keys );
		$values = $this->fit_values( $values );

		$ins = "(";

		foreach ( $keys as $key => $value ) {
			$ins .= $keys[ $key ] . ", ";
		}
		$ins = substr( $ins, 0, - 2 );
		$ins .= ") VALUES (";

		foreach ( $values as $key => $value ) {
			$ins .= $values[ $key ] . ", ";
		}
		$ins = substr( $ins, 0, - 2 );
		$ins .= ")";

		$limit_sql = "";
		if ( ! empty( $limit ) ) {
			$limit_sql = "LIMIT " . $limit;
		}

		$sql = "INSERT INTO `" . $table . "` $ins $limit_sql;";

		$result = $this->query( $sql, $location );
		if ( ! $result ) {
			return false;
		}

		return $result;
	}

	public function prepare( $sql, $location = "Unknown" ) {
		$stmt = $this->id->prepare( $sql );
		if ( ! $stmt ) {
			//$this->log->error("sql", $location, "Cannot prepare SQL Query, Error: " . $this->id->error . ", SQL: " . $sql);
			//$this->error = new class_throw_error("sql: " . $location . " Cannot prepare SQL Query, Error: " . $this->id->error . ", SQL: " . $sql);
		}

		return $stmt;
	}

	// Datenbank Prepare (mysqli)

	public function execute( $stmt, $location = "Unknown" ) {
		if ( ! $stmt->execute() ) {
			//$this->log->error("sql", $location, "Cannot execute SQL Query, Error: " . $this->id->error);
			//$this->error = new class_throw_error("sql: " . $location . " Cannot execute SQL Query, Error: " . $this->id->error);

			return false;
		}

		return true;
	}
}
