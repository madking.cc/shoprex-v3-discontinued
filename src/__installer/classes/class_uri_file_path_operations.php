<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


class class_uri_file_path_operations extends class_root {
	public $not_allowed_filenames;
	public $not_allowed_path_chars;
	public $not_allowed_file_chars;

	public function __construct() {
		$this->not_allowed_filenames  = array(
			"con",
			"prn",
			"aux",
			"nul",
			"com1",
			"com2",
			"com3",
			"com4",
			"com5",
			"com6",
			"com7",
			"com8",
			"com9",
			"lpt1",
			"lpt2",
			"lpt3",
			"lpt4",
			"lpt5",
			"lpt6",
			"lpt7",
			"lpt8",
			"lpt9",
			"php://output",
			"php://input",
			"php://stdin",
			"php://stdout",
			"php://stderr",
			".htaccess",
			".htpasswd"
		);
		$this->not_allowed_file_chars = array( "<", ">", "\?", "\"", ":", "\|", "\\\\", "/", "\*" );
		$this->not_allowed_path_chars = array( "<", ">", "\|", "\*" );
	}


	public function open_file( $file, $mode = "a", $use_dirroot = true ) {
		if ( $use_dirroot ) {
			$file = DIRROOT . $file;
		}
		// Prüfe, ob Datei vorhanden
		if ( ! file_exists( $file ) ) {
			$GLOBALS["early_notice"][] = array(
				"type"     => "file",
				"location" => __FILE__ . ":" . __LINE__,
				"text"     => "File $file not existend, try to create."
			);
		}
		// Öffne Datei zum schreiben, erstelle wenn nicht vorhanden
		@$handler = fopen( $file, $mode );
		// Falls Fehler
		if ( $handler === false ) {
			$GLOBALS["early_error"][] = array(
				"type"     => "file",
				"location" => __FILE__ . ":" . __LINE__,
				"text"     => " Cannot open file $file for write."
			);
		}

		// Gebe Dateizeiger zurück
		return $handler;
	}

	public function close_file( $handler ) {
		if ( $handler !== false ) {
			fclose( $handler );
		}
	}


	public function get_filename( $string ) {
		return pathinfo( $string, PATHINFO_FILENAME );
	}

	public function get_filetype( $string ) {
		return pathinfo( $string, PATHINFO_EXTENSION );
	}

	public function get_filename_and_type( $string ) {
		$filename = $this->get_filename( $string );
		$filetype = $this->get_filetype( $string );
		if ( isset( $filetype ) && ! empty( $filetype ) ) {
			return $filename . "." . $filetype;
		} else {
			return $filename;
		}
	}

	public function get_path( $string ) {
		return pathinfo( $string, PATHINFO_DIRNAME ) . "/";
	}

	public function translate_special_characters( $string, $filename = false, $underlineonly = false, $untouch_slahses = false ) {
		$replace = array(
			'ä' => 'ae',
			'ö' => 'oe',
			'ü' => 'ue',
			'ß' => 'ss',
			'à' => 'a',
			'á' => 'a',
			'â' => 'a',
			'ã' => 'a',
			'å' => 'a',
			'æ' => 'ae',
			'ç' => 'c',
			'è' => 'e',
			'é' => 'e',
			'ê' => 'e',
			'ë' => 'e',
			'ì' => 'i',
			'í' => 'i',
			'î' => 'i',
			'ï' => 'i',
			'ð' => 'o',
			'ñ' => 'n',
			'ò' => 'o',
			'ó' => 'o',
			'ô' => 'o',
			'õ' => 'o',
			'ø' => 'o',
			'ù' => 'u',
			'ú' => 'u',
			'û' => 'u',
			'ý' => 'y',
			'þ' => 'b',
			'ÿ' => 'y'
		);
		if ( $underlineonly ) {
			$line = "_";
		} else {
			$line = "-";
		}

		$replace[' '] = $line;
		if ( ! $untouch_slahses ) {
			$replace['\\'] = $line;
			$replace['/']  = $line;
		}
		$replace['['] = $line;
		$replace[']'] = $line;
		$replace['('] = $line;
		$replace[')'] = $line;
		$replace[','] = $line;
		$replace[':'] = $line;
		$replace['"'] = $line;
		$replace["'"] = $line;

		if ( ! $filename ) {
			$replace["."] = $line;
		}
		$string = strtr( mb_strtolower( $string, 'UTF-8' ), $replace );

		return $string;
	}

	protected function create_filename( $name, $type, $dir_root, $private_start_number = "" ) {

		// Falls Dateiname nach bestimmten Schema bereits existiert
		if ( empty( $private_start_number ) ) {
			$spacer = "";
		} else {
			$spacer = "_";
		}

		//$tmp = $dir_root . $name . $spacer . $private_start_number . "." . $type;
		//var_dump($tmp);

		if ( file_exists( $dir_root . $name . $spacer . $private_start_number . "." . $type ) ) {
			// Erhöhe den Zähler und prüfe nochmal
			if ( empty( $private_start_number ) ) {
				$private_start_number = 0;
			}
			$newname = $this->create_filename( $name, $type, $dir_root, ( $private_start_number + 1 ) );

			// (2) Wegen der Rekursion, gebe gefundenen Namen zurück
			return $newname;
		} else {
			// (1) Gebe den neuen gefundenen Namen an die aufgerufene Funktion zurück
			return $dir_root . $name . $spacer . $private_start_number . "." . $type;
		}
	}

	public function check_for_not_allowed_filenames( $string, $location = "" ) {
		if ( empty( $location ) ) {
			$location = __FILE__ . ":" . __LINE__;
		}

		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			$GLOBALS["early_notice"][] = $location . ": Check for not allowed filename, String is empty";

			return false;
		}

		if ( in_array( strtolower( $string ), $this->not_allowed_filenames ) ) {
			$GLOBALS["early_notice"][] = $location . ": Check for not allowed filename, String is not allowed: '" . $this->escape( $string ) . "'";

			return false;
		} else {
			return $string;
		}
	}

	public function remove_not_allowed_file_chars( $string, $location = "" ) {
		if ( empty( $location ) ) {
			$location = __FILE__ . ":" . __LINE__;
		}

		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			$GLOBALS["early_notice"][] = $location . ": Remove not allowed file chars, String is empty";

			return $string;
		}

		$not_allowed_chars = implode( "|", $this->not_allowed_file_chars );
		$string            = preg_replace( '`(' . $not_allowed_chars . ')`s', "", $string );

		return $string;
	}

	public function check_for_not_allowed_file_chars( $string, $location = "" ) {
		if ( empty( $location ) ) {
			$location = __FILE__ . ":" . __LINE__;
		}

		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			$GLOBALS["early_notice"][] = __FILE__ . ":" . __LINE__ . ": Check for not allowed filename, String is empty";

			return $string;
		}

		$not_allowed_chars = implode( "|", $this->not_allowed_file_chars );
		$result            = preg_match( '`(' . $not_allowed_chars . ')`s', $string );

		return ! $result;
	}

	public function remove_not_allowed_chars_in_path( $string, $remove_dir_backs = true, $location = "" ) {
		if ( empty( $location ) ) {
			$location = __FILE__ . ":" . __LINE__;
		}

		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			$GLOBALS["early_notice"][] = $location . ": Remove not allowed path chars, String is empty";

			return $string;
		}

		$string = $this->delete_redundant_path_chars( $string, $location );
		if ( $remove_dir_backs ) {
			$string = $this->remove_dir_backs( $string, $location );
		}
		$string = $this->remove_injection( $string, $location );

		$not_allowed_chars = implode( "|", $this->not_allowed_path_chars );
		$string            = preg_replace( '`(' . $not_allowed_chars . ')`s', "", $string );

		return $string;
	}

	public function delete_redundant_path_chars( $string, $location = "" ) {
		if ( empty( $location ) ) {
			$location = __FILE__ . ":" . __LINE__;
		}

		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			$GLOBALS["early_notice"][] = $location . ": Remove redudant path chars, String is empty";

			return $string;
		}

		$string = str_replace( " ", "", $string );
		$string = preg_replace( "#[/]{2,}#", "/", $string );
		$string = preg_replace( "#[\\\]{2,}#", "\\", $string );
		$string = str_replace( "\\", "/", $string );

		return $string;
	}

	public function remove_dir_backs( $string, $location = "" ) {
		if ( empty( $location ) ) {
			$location = __FILE__ . ":" . __LINE__;
		}

		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			$GLOBALS["early_notice"][] = $location . ": Remove dir backs, String is empty";

			return $string;
		}

		$string = str_replace( "../", "", $string );
		$string = str_replace( "/..", "", $string );
		$string = str_replace( "..\\", "", $string );
		$string = str_replace( "\\..", "", $string );
		$string = str_replace( "..", "", $string );

		return $string;
	}

	public function remove_injection( $string, $location = "" ) {
		if ( empty( $location ) ) {
			$location = __FILE__ . ":" . __LINE__;
		}

		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			$GLOBALS["early_notice"][] = $location . ": Remove injections, String is empty";

			return $string;
		}

		$string = str_replace( "'", "", $string );
		$string = str_replace( "\"", "", $string );
		$string = str_replace( "`", "", $string );

		return $string;
	}

	public function remove_not_allowed_chars_in_uri( $string, $remove_dir_backs = true ) {
		if ( is_null( $string ) || ( empty( $string ) && $string != "0" ) ) {
			return $string;
		}

		$string = $this->delete_redundant_path_chars( $string );
		if ( $remove_dir_backs ) {
			$string = $this->remove_dir_backs( $string );
		}
		$string = $this->remove_injection( $string );

		$not_allowed_chars = implode( "|", $this->not_allowed_path_chars );
		$string            = preg_replace( '`(' . $not_allowed_chars . ')`s', "", $string );

		return $string;
	}

	public function check_special_filetype( $file, $type = "picture" ) {
		$filetype = $this->get_filetype( $file );
		$filetype = strtolower( $filetype );

		switch ( $type ) {
			case "picture":
				if ( in_array( $filetype, $this->allowed_picture_types ) ) {

					$result = $this->check_for_not_allowed_filenames( $file );
					if ( empty( $result ) ) {
						return false;
					}
					$result = $this->check_for_not_allowed_file_chars( $file );

					return $result;
				} else {
					return false;
				}
				break;
			case "file":
				$result = $this->check_for_not_allowed_filenames( $file );
				if ( empty( $result ) ) {
					return false;
				}
				$result = $this->check_for_not_allowed_file_chars( $file );

				return $result;
				break;
			default:
				return false;
				break;
		}
	}


	public function get_file_content( $file, $use_dirroot = true ) {
		if ( $use_dirroot ) {
			$file = DIRROOT . $file;
		}
		if ( function_exists( 'file_get_contents' ) ) {
			return file_get_contents( $file );
		} else {
			$str = "";
			foreach ( file( $file ) as $contentline ) {
				$str .= $contentline;
			}

			return $str;
		}
	}

	public function get_filecreatedate( $path, $format_time = true, $use_webroot = false, $use_dirroot = true ) {
		if ( $use_webroot ) {
			$path = WEBROOT . $path;
		}
		if ( $use_dirroot ) {
			$path = DIRROOT . $path;
		}
		if ( ! is_file( $path ) ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't find file for datetime: '$path'" );

			return false;
		} else {
			$createtime = filectime( $path );
			if ( $format_time ) {
				$createtime = @date( $GLOBALS['default_datetime_format']['datetime'], $createtime );
			}

			return $createtime;
		}
	}


	public function get_files( $path, $type = "", $skip_special_files = true, $use_webroot = false, $use_dirroot = true ) {

		if ( $use_webroot ) {
			$path = WEBROOT . $path;
		}
		if ( $use_dirroot ) {
			$path = DIRROOT . $path;
		}
		$files = scandir( $path );

		if ( ! $files ) {
			return false;
		}
		foreach ( $files as $key => $value ) {
			if ( $value == "." || $value == ".." ) {
				unset( $files[ $key ] );
			} elseif ( is_dir( $path . $value ) ) {
				unset( $files[ $key ] );
			} elseif ( $skip_special_files && in_array( $value, $this->special_files ) ) {
				unset( $files[ $key ] );
			} elseif ( ! empty( $type ) ) {
				$array = pathinfo( $path . $value );
				if ( strtolower( $array['extension'] ) != $type ) {
					unset( $files[ $key ] );
				}
			}
		}

		return $files;
	}

	public function get_dirsize( $path, $size = 0, $use_webroot = false, $use_dirroot = true ) {
		if ( $use_webroot ) {
			$path = WEBROOT . $path;
		} elseif ( $use_dirroot ) {
			$path = DIRROOT . $path;
		}

		$files = scandir( $path );

		if ( ! $files ) {
			return $size;
		}
		foreach ( $files as $value ) {
			if ( $value == "." || $value == ".." ) {
				continue;
			} elseif ( is_dir( $path . $value ) ) {
				$size += $this->get_dirsize( $path . "/" . $value, $size, NO_WEBROOT, NO_DIRROOT );

			} else {
				$size += ( filesize( $path . "/" . $value ) / 1024 / 1024 );
			}
		}

		return $size;
	}


	public function copy_file( $source, $dest, $source_file = "", $dest_file = "", $use_dirroot = true ) {
		if ( $use_dirroot ) {
			$source = DIRROOT . $source;
			$dest   = DIRROOT . $dest;
		}
		$result = copy( $source . $source_file, $dest . $dest_file );

		return $result;
	}

	public function move_file( $source, $dest, $source_file = "", $dest_file = "", $use_dirroot = true ) {
		if ( $use_dirroot ) {
			$source = DIRROOT . $source;
			$dest   = DIRROOT . $dest;
		}
		$result = rename( $source . $source_file, $dest . $dest_file );

		return $result;
	}


	public function get_sub_dirs( $path, $use_dirroot = true ) {
		if ( $use_dirroot ) {
			$path = DIRROOT . $path;
		}

		if ( strpos( $path, "/" ) == ( strlen( $path ) - 1 ) ) {
			$slash = "";
		} else {
			$slash = "/";
		}


		//if(is_dir($path))
		$dirs = scandir( $path );
		//else
		//    return -1;

		foreach ( $dirs as $key => $value ) {
			if ( $value == "." || $value == ".." ) {
				unset( $dirs[ $key ] );
				continue;
			}
			if ( $use_dirroot ) {
				$check = $path . $slash . $value;
			} else {
				$check = DIRROOT . $path . $slash . $value;
			}

			if ( is_file( $check ) ) {
				unset( $dirs[ $key ] );
			}
		}

		return $dirs;
	}


	public function get_dir_structure( $start_dir ) {
		$dir_ar    = array();
		$dir_ar[0] = $this->get_dir_content( $start_dir );

		foreach ( $dir_ar[0] as $key => $dir ) {
			// TODO: Finish
		}
	}

	public function get_dir_content( $path = "", $use_dirroot = true, $dont_select = array() ) {
		if ( $use_dirroot ) {
			$path = DIRROOT . $path;
		}

		if ( strpos( $path, "/" ) == ( strlen( $path ) - 1 ) ) {
			$slash = "";
		} else {
			$slash = "/";
		}

		if ( is_dir( $path ) ) {
			$dirs = scandir( $path );
		} else {
			return false;
		}


		if ( ! $dirs ) {
			return false;
		}
		foreach ( $dirs as $key => $value ) {
			if ( $value == "." || $value == ".." ) {
				unset( $dirs[ $key ] );
				continue;
			}
			if ( $use_dirroot ) {
				$check = $path . $slash . $value;
			} else {
				$check = DIRROOT . $path . $slash . $value;
			}


			if ( is_dir( $check ) ) {
				unset( $dirs[ $key ] );
			} elseif ( ! empty( $dont_select ) ) {
				if ( in_array( $value, $dont_select ) ) {
					unset( $dirs[ $key ] );
				}
			}
		}

		return $dirs;
	}


	public function parse_str( $string ) {
		$string = str_replace( "&amp;", "&", $string );
		parse_str( $string, $string_ar );
		foreach ( $string_ar as $key => $value ) {
			$string_ar[ $key ] = str_replace( "&", "&amp;", $value );
		}

		return $string_ar;
	}
}