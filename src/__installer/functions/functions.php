<?php

function init_array( &$array, $number ) {
	if ( ! isset( $array[ $number ] ) ) {
		$array[ $number ] = "";
	}
}

function get_status_icon( $bool ) {
	global $loc;

	if ( $bool ) {
		$img = "true.png";
	} else {
		$img = "false.png";
	}

	return "<img class='nr' src='/" . $loc->web_root . "__installer/layout/img/" . $img . "'>";
}

function select_timezone_options( $timezone_selected = "" ) {
	$content              = "";
	$timezone_identifiers = DateTimeZone::listIdentifiers();

	foreach ( $timezone_identifiers as $timezone ) {
		$content .= "<option";
		if ( ! empty( $timezone_selected ) ) {
			if ( strcmp( $timezone, $timezone_selected ) === 0 ) {
				$content .= " selected=selected";
			}
		} else {

			if ( @date_default_timezone_get() ) {
				$default_timezone = @date_default_timezone_get();
			} elseif ( ini_get( 'date.timezone' ) ) {
				$default_timezone = ini_get( 'date.timezone' );
			} else {
				$default_timezone = null;
			}

			if ( isset( $default_timezone ) || ! empty( $default_timezone ) ) {
				if ( strcmp( $timezone, $default_timezone ) === 0 ) {
					$content .= " selected=selected";
				}
			}
		}
		$content .= ">" . $timezone . "</option>";
	}

	return $content;
}