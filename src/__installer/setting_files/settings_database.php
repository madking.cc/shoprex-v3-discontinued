<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

if ( isset( $_SESSION["db_admin_name"] ) && ! empty( $_SESSION["db_admin_name"] ) ) {
	$db_admin_name = $_SESSION["db_admin_name"];
	$db_admin_pass = $_SESSION["db_admin_pass"];
} else {
	$db_admin_name = $_SESSION["db_name"];
	$db_admin_pass = $_SESSION["db_pass"];
}

$settings_database = "<?php defined('SECURITY_CHECK') or die;
 
define(\"TP\", \"" . $_SESSION["db_table_prefix"] . "\");

if (isset(\$GLOBALS['page_is_online']) && \$GLOBALS['page_is_online']) {
    define(\"DB_HOST\", \"" . $_SESSION["db_database_server"] . "\");
    define(\"DB_TABLE\", \"" . $_SESSION["db_database_name"] . "\");
    define(\"DB_PORT\", \"" . $_SESSION["db_database_port"] . "\"); // Optional
    if (isset(\$GLOBALS['is_admin']) && \$GLOBALS['is_admin']) {
        define(\"DB_USER\", \"" . $db_admin_name . "\");
        define(\"DB_PASS\", \"" . $db_admin_pass . "\");
    } else {
        define(\"DB_USER\", \"" . $_SESSION["db_name"] . "\");
        define(\"DB_PASS\", \"" . $_SESSION["db_pass"] . "\");
    }
} else {
    define(\"DB_HOST\", \"localhost\");
    define(\"DB_TABLE\", \"\");
    define(\"DB_PORT\", \"\"); // Optional
    if (isset(\$use_admin_db_login) && \$use_admin_db_login) {
        define(\"DB_USER\", \"\");
        define(\"DB_PASS\", \"\");
    } else {
        define(\"DB_USER\", \"\");
        define(\"DB_PASS\", \"\");
    }
}";

