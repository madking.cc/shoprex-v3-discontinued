<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

if ( ! isset( $_SESSION["admin_email"] ) ) {
	$_SESSION["admin_email"] = "";
}

$settings_website_ini = "[date_default_timezone_set]
date_default_timezone_set = \"" . $_SESSION["timezone"] . "\"

[constants]
ADMIN_MAIL = \"" . $_SESSION["admin_email"] . "\"
MAIL_FROM = \"" . $_SESSION["admin_email"] . "\"
MAIL_FROM_NAME = \"Mailbot\"
SEND_BCC = 0
MAIL_BCC = \"\"
MAIL_BCC_NAME = \"\"
WEBROOT = \"" . $loc->web_root . "\"
FILE_SIZE_FOR_ARCHIVE_IN_MB = \"2\"
ADMINDIR = \"__admin/\"
UPLOADDIR = \"files/\"
DOWNLOADDIR = \"download/\"
NO_COUNT_CODE = \"" . rand( 100000, 999999 ) . "\"
ONLY_HTTPS = 0
ENABLE_TAB_IN_TEXTAREAS = 1
WRITE_LOG = 1
WRITE_LOG_NOTICE = 1
STOP_ON_ERROR = 1
    WAIT_TIME_FOR_REVISIT = 86400
    HIDE_THIS_VARS_IN_LOG = \"*pass*|token\"
    LOG_ADMIN_PREFIX = \"admin_\"
    PREVIEW_MODE = 0
    PREVIEW_MODE_RESET_DB = 1
    PREVIEW_MODE_RESET_DB_TIME = \"30\"
    IP_SALT = \"" . uniqid() . "\"

[global_variables]
maintenance_style = \"sad/\"
body_class = \"\"
page_title = \"Shoprex\"
page_subtitle = \"\"
page_title_divider = \" - \"
page_description = \"\"
page_keywords = \"\"
default_datetime_format[date] = \"d.m.Y\"
default_datetime_format[datetime] = \"d.m.Y H:i\"
default_datetime_format[datetime_seconds] = \"d.m.Y H:i:s\"
default_datetime_format[month_day] = \"d.m\"
default_datetime_format[year] = \"Y\"
default_datetime_format[time] = \"H:i\"
default_datetime_format[time_seconds] = \"H:i:s\"
default_datetime_format[hour] = \"H\"
default_datetime_format[month_year] = \"m.Y\"
    page_is_online = 1";