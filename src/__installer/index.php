<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

error_reporting( E_ALL );
ini_set( 'track_errors', '1' );
ini_set( 'display_errors', '1' );
define( "ESCAPED", 1 );
define( "NOT_ESCAPED", 0 );
if ( ! isset( $GLOBALS["body_footer"] ) ) {
	$GLOBALS["body_footer"] = "";
}

require_once( DIRINSTALL . "functions/functions.php" );
require_once( DIRINSTALL . "classes/class_root.php" );
require_once( DIRINSTALL . "classes/class_uri_file_path_operations.php" );
require_once( DIRINSTALL . "classes/class_location.php" );
require_once( DIRINSTALL . "classes/class_database.php" );
require_once( DIRINSTALL . "classes/class_crypt.php" );

$loc = new class_location();


init_array( $GLOBALS['foot'], 20 );
$GLOBALS['foot'][20] .= "      <script src=\"/" . $loc->web_root . "__installer/external/jquery/jquery.min.js\"></script>\n";
$GLOBALS['foot'][20] .= "      <script src=\"/" . $loc->web_root . "__installer/external/jquery/ui/jquery-ui.min.js\"></script>\n";
$GLOBALS['head_top'] = "      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n";
init_array( $GLOBALS['head'], 15 );
$GLOBALS['head'][15] .= "      <link href=\"/" . $loc->web_root . "__installer/external/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"all\" />\n";
init_array( $GLOBALS['foot'], 0 );
$GLOBALS['foot'][0] .= "       <!--[if lt IE 9]>\n";
$GLOBALS['foot'][0] .= "           <script src=\"/" . $loc->web_root . "__installer/external/bootstrap/assets/js/html5shiv.min.js\"></script>\n";
//$GLOBALS['foot'][0] .= "           <script src=\"/" . $loc->web_root . $GLOBALS['external_d'] . "bootstrap/assets/js/html5shiv-printshiv".$min.".js\"></script>\n";
$GLOBALS['foot'][0] .= "           <script src=\"/" . $loc->web_root . "__installer/external/bootstrap/assets/js/respond.min.js\"></script>\n";
$GLOBALS['foot'][0] .= "       <![endif]-->\n";
init_array( $GLOBALS['foot'], 30 );
$GLOBALS['foot'][30] .= "      <script src=\"/" . $loc->web_root . "__installer/external/bootstrap/js/bootstrap.min.js\"></script>\n";
if ( is_file( DIRROOT . "__installer/layout/bootstrap.js" ) ) {
	init_array( $GLOBALS['foot'], 100 );
	$GLOBALS['foot'][100] .= "    <script src=\"/" . $loc->web_root . "__installer/layout/bootstrap.js\"></script>\n";
}
if ( is_file( DIRROOT . "__installer/layout/response.js" ) ) {
	init_array( $GLOBALS['foot'], 100 );
	$GLOBALS['foot'][100] .= "    <script src=\"/" . $loc->web_root . "__installer/layout/response.js\"></script>\n";
}

switch ( $loc->current_file ) {
	case "write_files.php":
		$page = "write_files.php";
		break;
	case "check_environment.php":
		$page = "check_environment.php";
		break;
	case "set_database_and_admin.php":
		$page = "set_database_and_admin.php";
		break;
	case "fill_database.php":
		$page = "fill_database.php";
		break;
	case "index.php":
	default:
		$page = "start.php";
		break;
}

if ( isset( $_GET['lang'] ) ) {
	$lang = htmlspecialchars( $_GET['lang'], ENT_NOQUOTES );
} else {
	$lang = "de";
}

if ( ! isset( $content ) ) {
	$content = "";
}
switch ( $lang ) {
	case "en":
		$content .= "<h1>Installationtool Shoprex</h1><hr>\n";
		$admin_title = "Shoprex Installation";
		break;
	case "de":
	default:
		$content .= "<h1>Installationsroutine Shoprex</h1><hr>\n";
		$admin_title = "Shoprex Installation";
		break;
}


require_once( DIRINSTALL . "pages/" . $page );

require_once( DIRINSTALL . "layout/header.php" );
require_once( DIRINSTALL . "layout/footer.php" );
echo $header_code . $content . $footer_code;

if ( isset( $do_delete_recursive ) && $do_delete_recursive ) {
	session_destroy();
	delete_recursive( DIRINSTALL );
}