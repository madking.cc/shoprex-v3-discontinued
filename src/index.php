<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or define( "SECURITY_CHECK", "OK" );

if ( session_status() !== PHP_SESSION_ACTIVE ) {
	session_start();
}

define( "DIRROOT", realpath( __DIR__ ) . "/" );
define( "DIRINSTALL", DIRROOT . "__installer/" );
define( "DIRINCLUDE", DIRROOT . "__include/" );

if ( ! is_dir( DIRINSTALL ) ) {
	require_once( DIRINCLUDE . "init.php" );
	require_once( DIRINCLUDE . "get_content.php" );
	require_once( DIRINCLUDE . "do_output.php" );
	require_once( DIRINCLUDE . "terminate.php" );
} else {
	if ( ! isset( $installer_file ) || empty( $installer_file ) ) {
		$installer_file = "index.php";
	}
	/*
		if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == '1' || strtolower($_SERVER['HTTPS']) == 'on')) {
			$httpauto = "https://";
		} else {
			$httpauto = "http://";
		}
		$host = htmlspecialchars( "{$_SERVER['HTTP_HOST']}", ENT_QUOTES, 'UTF-8' );
		$url =  htmlspecialchars( "{$_SERVER['REQUEST_URI']}", ENT_QUOTES, 'UTF-8' );
		$url_length = strlen($url);
		$dirroot_length = strlen(DIRROOT);
		$strrpos_find = $dirroot_length - $url_length;
		if(strrpos(DIRROOT, $url) !== $strrpos_find)
			$webroot = "/";
		else
		{
			$webroot = $url;
		}
		$test = $httpauto.$host.$webroot;
		var_dump($strrpos_find, DIRROOT, $url, $host, $test);
	*/

	require_once( DIRINSTALL . $installer_file );
}
