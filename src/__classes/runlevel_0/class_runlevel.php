<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_runlevel {
	protected static $_instance = null;

	public $level;
	public $history;
	public $level_map;

	public function __construct() {
		$this->level          = "999";
		$this->history        = array();
		$this->history[]      = 0;
		$this->level_map      = array();
		$this->level_map[0]   = "Basic Funktions";
		$this->level_map[1]   = "Database, Log and Mail enabled";
		$this->level_map[2]   = "Status of Environment up";
		$this->level_map[3]   = "Full Class support";
		$this->level_map[4]   = "Admin Modus";
		$this->level_map[666] = "System End";
		$this->level_map[999] = "System Start";
	}

	public static function getInstance() {
		if ( null === self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;
	}

	protected function __clone() {
	}

	public function set_level( $integer ) {
		$this->level     = $integer;
		$this->history[] = $integer;
	}

	public function raise_level() {
		$this->level ++;
		$this->history[] = $this->level;
	}

	public function lower_level() {
		$this->level --;
		if ( $this->level < 0 ) {
			$this->level = 0;
		}
		$this->history[] = $this->level;
	}

	public function get_level() {
		return $this->level;
	}

}