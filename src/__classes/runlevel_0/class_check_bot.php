<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$this_dir_root = realpath( __DIR__ ) . "/";
require_once( $this_dir_root . "inc.crawlers.php" );

class class_check_bot extends class_root {
	public $bot_check_done;
	public $visitor_is_bot;
	protected $first_bot_search;

	public function __construct() {
		$this->first_bot_search = array( "/bot", "bot/", "memorybot", "robot", "crawler", "archive", "spider" );
	}

	public function check_bot() {
		if ( $this->bot_check_done ) {
			return $this->visitor_is_bot;
		} else {
			$user_agent = $this->get_user_agent( NOT_ESCAPED );
			if ( ! isset( $user_agent ) || empty( $user_agent ) ) {
				$this->visitor_is_bot = false;
				$this->bot_check_done = true;

				return false;
			}

			$user_agent_lower = strtolower( $user_agent );


			foreach ( $this->first_bot_search as $bot ) {
				if ( strpos( $user_agent_lower, $bot ) !== false ) {
					$this->bot_check_done = true;
					$this->visitor_is_bot = true;

					return true;
				}
			}

			if ( ! is_array( $GLOBALS['crawlers'] ) || empty( $GLOBALS['crawlers'] ) ) {
				$this->visitor_is_bot = false;
				$this->bot_check_done = true;

				return false;
			} else {
				$result = false;
				foreach ( $GLOBALS['crawlers'] as $crawler ) {
					if ( strcasecmp( $user_agent_lower, $crawler ) === 0 ) {
						$result = true;
						break;
					}
					if ( strpos( $crawler, " http" ) !== false ) {
						$crawler = str_replace( ' http', ' +http', $crawler );
						if ( strcasecmp( $user_agent_lower, $crawler ) === 0 ) {
							$result = true;
							break;
						}
					}
				}

				$this->bot_check_done = true;
				$this->visitor_is_bot = $result;

				return $result;
			}
		}
	}

}