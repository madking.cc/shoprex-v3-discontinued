<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_file_soft_lock {
	public $already_locked;
	protected $file;
	protected $file_lock;

	public function __construct( $file, $do_dirroot = true ) {
		if ( ! $do_dirroot ) {
			$this->file = $file;
		} else {
			$this->file = DIRROOT . $file;
		}

		$this->file_lock      = $file . ".lock";
		$this->already_locked = false;

		if ( file_exists( $this->file_lock ) ) {
			$this->already_locked = true;
		} else {
			touch( $this->file_lock );
		}
	}

	public function __destruct() {
		$this->close();
	}

	public function close() {
		if ( ! $this->already_locked ) {
			if ( file_exists( $this->file_lock ) ) {
				unlink( $this->file_lock );
			}
		}
	}
}