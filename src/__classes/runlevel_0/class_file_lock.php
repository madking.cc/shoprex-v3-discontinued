<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_file_lock {
	public $is_locked;
	protected $handle;

	public function __construct( &$handle ) {
		$this->handle = &$handle;
		if ( ! ( $handle === false ) ) {
			$this->is_locked = flock( $this->handle, LOCK_EX );
		} else {
			$this->is_locked = false;
		}
	}

	public function for_read( &$new_handle ) {
		$this->close();
		$this->handle = &$new_handle;
		if ( ! ( $new_handle === false ) ) {
			$this->is_locked = flock( $this->handle, LOCK_SH );
		} else {
			$this->is_locked = false;
		}
	}

	public function close() {
		if ( $this->is_locked && is_resource( $this->handle ) ) {
			flock( $this->handle, LOCK_UN );
		}
	}

	public function for_write( &$new_handle ) {
		$this->close();
		$this->handle = &$new_handle;
		if ( ! ( $new_handle === false ) ) {
			$this->is_locked = flock( $this->handle, LOCK_EX );
		} else {
			$this->is_locked = false;
		}
	}

	public function __destruct() {
		$this->close();
	}
}