<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


class class_load_external {
	protected static $_instance = null;

	public $load_external_code;
	public $load_jquery;
	public $load_cookie;
	public $load_bootstrap;
	public $load_sticky;
	public $load_ta_enhanced;
	public $load_colorbox;
	public $load_timepicker;
	public $load_colorpicker;
	public $load_rangy;
	public $load_codehl;
	public $load_js_tbl;
	public $load_response;
	public $load_ta_highlighter;
	public $load_carousel;

	public function __construct() {
		$this->load_external_code  = true;
		$this->load_jquery         = true;
		$this->load_cookie         = true;
		$this->load_bootstrap      = true;
		$this->load_sticky         = true;
		$this->load_ta_enhanced    = false;
		$this->load_colorbox       = false;
		$this->load_timepicker     = false;
		$this->load_colorpicker    = false;
		$this->load_rangy          = false;
		$this->load_codehl         = false;
		$this->load_js_tbl         = false;
		$this->load_response       = true;
		$this->load_ta_highlighter = false;
		$this->load_carousel       = false;
	}

	public static function getInstance() {
		if ( null === self::$_instance ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	protected function __clone() {
	}
}