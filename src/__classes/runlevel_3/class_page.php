<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

require_once( DIRSETTINGSCUSTOM . "class_page_settings.php" );

class class_page extends class_page_settings {
	protected $loc;
	protected $lang;
	protected $visitor;

	public $is_admin;
	public $is_visitor;
	protected $lt;
	protected $back_count;
	protected $pu;

	protected $log;
	protected $get;

	protected $paste_textarea_counter;

	public function __construct() {
		parent::__construct();
		$this->log  = class_logging::getInstance();
		$this->db   = class_database::getInstance();
		$this->mail = new class_mail();
		//$this->crypt = new class_crypt();
		//$this->img = new class_images($this);
		// Geht nicht: $this->l = new class_layout();
		$this->visitor = class_visitor::getInstance();
		$this->loc     = class_location::getInstance();
		$this->lang    = class_language::getInstance();
		$this->get     = new class_get();

		$this->is_admin               = $GLOBALS['is_admin'];
		$this->date_format            = $GLOBALS['default_datetime_format'];
		$this->paste_textarea_counter = 0;
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}

	public function get( $var, $default = null, $do_escape = true, $do_htmlspecialchars = false, $use_session = true ) {
		return $this->get->get( $var, $default, $do_escape, $do_htmlspecialchars, $use_session );
	}

	public function keep_session_alive() {
		if ( session_status() !== PHP_SESSION_ACTIVE ) {
			session_start();
		}

		$maxlifetime = ini_get( "session.gc_maxlifetime" );
		$maxlifetime = intval( ( $maxlifetime / 100 ) * 90 );
		if ( $maxlifetime > 0 ) {
			$GLOBALS['foot_bottom'] .= "<script>
    function keepMeAlive() {
        $.ajax(
        {
            url : \"" . $this->loc->httpauto_web_root . "__functions/ajax/ajax_session.php?\"+new Date().getTime(),
            type: \"POST\",
            dataType: \"html\"
        });
    }
    window.setInterval(keepMeAlive, " . ( $maxlifetime * 1000 ) . ");
    </script>\n";
		}
	}

	public function preview_parse( $string ) {
		if ( $this->get_preview_status() ) {
			/*
			$found = strpos($string, "script");
			if($found !== false)
			{
				$this->log->error("Cross-Scripting", __FILE__.":".__LINE__, "String: '$string'");
			}
			*/
			$string = str_replace( "script", "scr1pt", $string );
		}

		return $string;
	}


	public function check_access() {

		$size = sizeof( $this->allowed_ip_ranges );
		if ( $size == 0 ) {
			return true;
		} elseif ( $size % 2 != 0 ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong size of 'allowed_ip_ranges': '$size'" );

			return false;
		} else {
			$allowed = false;
			for ( $i = 0; $i < $size; $i += 2 ) {
				if ( $this->allowed_ip_ranges[ $i ] <= $this->ip2str( $this->get_ipv4() ) AND $this->ip2str( $this->get_ipv4() ) <= $this->allowed_ip_ranges[ $i + 1 ]
				) {
					$allowed = true;
					break;
				}
			}

			return $allowed;
		}
	}


	public function makeDownload( $file, $use_dirroot = true ) {

		$filename = $this->get_filename_and_type( $file );
		if ( $use_dirroot ) {
			$file = DIRROOT . $file;
		}
		$size = filesize( $file );

		header( "Content-Disposition: attachment; filename=" . urlencode( $filename ) );
		header( "Content-Type: application/octet-stream" );
		header( "Content-Description: File Transfer" );
		header( "Content-Length: $size" );
		flush();

		readfile( $file );

		return true;
	}

	public function load_location( $location = "", $dont_modify = false ) {
		if ( ! $dont_modify ) {
			if ( $this->is_admin ) {
				//var_dump($start, $start, $start, $start, $location, $this->loc->web_admin_root, $this->loc->http_domain);
				$location = $this->loc->httpauto_web_admin_root . $location;
			} else {
				$location = $this->loc->httpauto_web_root . $location;
			}
		}

		if ( ! headers_sent() ) {
			session_write_close();
			header( "Location: " . $location );
			die();
		} else {
			$this->log->error( "php", __FILE__ . __LINE__, "Headers already sent! Can't load page." );
			new class_throw_error( "php: " . __FILE__ . __LINE__ . " Headers already sent! Can't load page." );

			return false;
		}
	}


	public function init_array( &$array, $number ) {
		if ( ! isset( $array[ $number ] ) ) {
			$array[ $number ] = "";
		}
	}

	public function aasort( &$array, $key ) {
		$sorter = array();
		$ret    = array();
		reset( $array );
		foreach ( $array as $ii => $va ) {
			$sorter[ $ii ] = $va[ $key ];
		}
		asort( $sorter );
		foreach ( $sorter as $ii => $va ) {
			$ret[ $ii ] = $array[ $ii ];
		}
		$array = $ret;
	}


	public function get_random_email() {

		$tlds = array( "com", "net", "gov", "org", "edu", "biz", "info" );
		$char = "0123456789abcdefghijklmnopqrstuvwxyz";

		$ulen = mt_rand( 5, 10 );
		$dlen = mt_rand( 7, 17 );

		$a = "";

		for ( $i = 1; $i <= $ulen; $i ++ ) {
			$a .= substr( $char, mt_rand( 0, strlen( $char ) ), 1 );
		}

		$a .= "@";

		for ( $i = 1; $i <= $dlen; $i ++ ) {
			$a .= substr( $char, mt_rand( 0, strlen( $char ) ), 1 );
		}

		$a .= ".";

		$a .= $tlds[ mt_rand( 0, ( sizeof( $tlds ) - 1 ) ) ];

		return $a;
	}

	public function get_random_name() {
		$char = "0123456789abcdefghijklmnopqrstuvwxyz";

		$ulen = mt_rand( 5, 10 );
		$a    = "";

		for ( $i = 1; $i <= $ulen; $i ++ ) {
			$a .= substr( $char, mt_rand( 0, strlen( $char ) ), 1 );
		}

		return $a;
	}


	function array_split( $array ) {

		$array_left            = array();
		$array_right           = array();
		$array_result          = array();
		$array_result['left']  = array();
		$array_result['right'] = array();
		$size_array            = sizeof( $array );
		if ( $size_array <= 1 ) {
			$array_left = $array;
		} else {
			$half_size_array = ceil( $size_array / 2 );

			for ( $i = 0; $i < $half_size_array; $i ++ ) {
				$tmp                       = $this->array_kshift( $array );
				$array_left[ key( $tmp ) ] = $tmp[ key( $tmp ) ];
			}
			$array_right = $array;
		}
		$array_result['left']  = $array_left;
		$array_result['right'] = $array_right;

		return $array_result;
	}

	public function array_kshift( &$arr ) {
		list( $k ) = array_keys( $arr );
		$r = array( $k => $arr[ $k ] );
		unset( $arr[ $k ] );

		return $r;
	}


	public function fix_special_html_chars( $text ) {
		return preg_replace( "/([&])(\S+)/s", "&#38;$2", $text );
	}

	public function paste_codemirror( $editor_id, $text, $js_mode = false, $colorbox_close = true, $called = "top" ) {
		$class_external                      = class_load_external::getInstance();
		$class_external->load_ta_highlighter = true;

		if ( $colorbox_close ) {
			$colorbox_close = $called . ".$.fn.colorbox.close();";
		} else {
			$colorbox_close = "";
		}

		if ( ! $js_mode ) {

			$text = $this->fix_var_for_js( $text );

			$GLOBALS["body_footer"] .= "<script>
    $(function()
    {
        var data" . $this->paste_textarea_counter . " = \"$text\";
        var taHighlight" . $this->paste_textarea_counter . " = $('#" . $editor_id . "', " . $called . ".document);
        var cm" . $this->paste_textarea_counter . " = taHighlight" . $this->paste_textarea_counter . ".next('.CodeMirror')[0].CodeMirror;
        var doc" . $this->paste_textarea_counter . " = cm" . $this->paste_textarea_counter . ".getDoc();
        var cursor" . $this->paste_textarea_counter . " = doc" . $this->paste_textarea_counter . ".getCursor();
        doc" . $this->paste_textarea_counter . ".replaceRange(data" . $this->paste_textarea_counter . ", cursor" . $this->paste_textarea_counter . ");
        $colorbox_close

    });
    </script>";
		} else {
			return "
        var data" . $this->paste_textarea_counter . " = $text;
        var taHighlight" . $this->paste_textarea_counter . " = $('#" . $editor_id . "', " . $called . ".document);
        var cm" . $this->paste_textarea_counter . " = taHighlight" . $this->paste_textarea_counter . ".next('.CodeMirror')[0].CodeMirror;
        var doc" . $this->paste_textarea_counter . " = cm" . $this->paste_textarea_counter . ".getDoc();
        var cursor" . $this->paste_textarea_counter . " = doc" . $this->paste_textarea_counter . ".getCursor();
        doc" . $this->paste_textarea_counter . ".replaceRange(data" . $this->paste_textarea_counter . ", cursor" . $this->paste_textarea_counter . ");
            ";
		}

		$this->paste_textarea_counter ++;
	}

	public function paste_rangy( $editor_id, $text, $js_mode = false, $colorbox_close = true, $called = "top", $trigger_change = true ) {
		$class_external             = class_load_external::getInstance();
		$class_external->load_rangy = true;

		$change_called = "";

		if ( $colorbox_close ) {
			$colorbox_close = $called . ".$.fn.colorbox.close();";
		} else {
			$colorbox_close = "";
		}

		if ( ! $js_mode ) {

			$text = $this->fix_var_for_js( $text );

			$GLOBALS["body_footer"] .= "<script>
    $(function()
    {
        var textBox" . $this->paste_textarea_counter . " = $(\"#" . $editor_id . "\", " . $called . ".document);
        textBox" . $this->paste_textarea_counter . ".focus();
        var sel" . $this->paste_textarea_counter . " = textBox" . $this->paste_textarea_counter . ".getSelection();
        textBox" . $this->paste_textarea_counter . ".insertText(\"$text\", sel" . $this->paste_textarea_counter . ".end, \"collapseToEnd\");
        $change_called
        $colorbox_close

    });
    </script>";
		} else {
			return "
        var textBox" . $this->paste_textarea_counter . " = $(\"#" . $editor_id . "\", " . $called . ".document);
        textBox" . $this->paste_textarea_counter . ".focus();
        var sel" . $this->paste_textarea_counter . " = textBox" . $this->paste_textarea_counter . ".getSelection();
        textBox" . $this->paste_textarea_counter . ".insertText($text, sel" . $this->paste_textarea_counter . ".end, \"collapseToEnd\");
        $change_called
            ";
		}

		$this->paste_textarea_counter ++;
	}

	public function paste_tinymce( $editor_id, $text, $js_mode = false, $colorbox_close = true, $called = "top", $trigger_change = "change_called" ) {
		$class_external                   = class_load_external::getInstance();
		$class_external->load_ta_enhanced = true;

		$change_called = "";

		if ( $colorbox_close ) {
			$colorbox_close = $called . ".$.fn.colorbox.close();";
		} else {
			$colorbox_close = "";
		}


		if ( ! $js_mode ) {

			$text = $this->fix_var_for_js( $text );

			$GLOBALS["body_footer"] .= "<script>
    $(function()
    {
        var editor" . $this->paste_textarea_counter . " = window." . $called . ".tinyMCE.get('$editor_id');
        editor" . $this->paste_textarea_counter . ".selection.setContent(\"" . $text . "\");
        $change_called
        $colorbox_close


    });
    </script>";
		} else {
			return "
        var editor" . $this->paste_textarea_counter . " = window." . $called . ".tinyMCE.get('$editor_id');
        editor" . $this->paste_textarea_counter . ".selection.setContent($text);
        $change_called
            ";
		}

		$this->paste_textarea_counter ++;
	}


	public function paste_element( $text, $target, $element = 0, $form_submit_id = "", $as_img = false, $picture_filename = "" ) {

		$text             = $this->fix_var_for_js( $text, REPLACE_SINGLE_QUOTES );
		$picture_filename = $this->fix_var_for_js( $picture_filename, REPLACE_SINGLE_QUOTES );

		if ( ! $as_img ) {
			$GLOBALS["body_footer"] .= "<script>
                        window.parent.document.getElementsByName('" . $target . "')[" . $element . "].value = '$text';\n";
		} else {
			$GLOBALS["body_footer"] .= "<script>
                        window.parent.document.getElementsByName('" . $target . "')[" . $element . "].src = '$text';
                        window.parent.document.getElementsByName('source_" . $target . "')[" . $element . "].value = '$picture_filename';\n";
		}

		$GLOBALS["body_footer"] .= "parent.$.fn.colorbox.close();
                    </script>\n";
		if ( ! empty( $form_submit_id ) ) {
			$GLOBALS["body_footer"] .= "<script>parent.$( \"#$form_submit_id\" ).submit();</script>\n";
		}
	}


	public function fix_parameter( $parameter, $cast_amp = true, $has_protocol_prefix = false, $mask_last_anker = true ) {
		$add_anker = false;
		if ( ! empty( $parameter ) && $parameter !== false ) {

			if ( ! $mask_last_anker ) {
				$anker_pos = strrpos( $parameter, "#" );
				$amp_pos   = strrpos( $parameter, "&" );

				if ( $anker_pos !== false ) {
					if ( $amp_pos === false || $anker_pos > $amp_pos ) {
						$anker     = substr( $parameter, $anker_pos );
						$parameter = substr( $parameter, 0, $anker_pos );
						$add_anker = true;
					}
				}
			}

			if ( $cast_amp ) {
				$amp = "&amp;";
			} else {
				$amp = "&";
			}

			$amp_size = strlen( $amp );

			$tmp3 = "";
			$tmp  = explode( "&", $parameter );

			foreach ( $tmp as $value ) {
				$tmp2 = explode( "=", $value );
				$i    = 0;
				foreach ( $tmp2 as $value2 ) {
					$i ++;
					if ( $i == 1 ) {
						$tmp3 .= $value2 . "=";
						continue;
					} else {
						$value2 = str_replace( "\\\"", "\"", $value2 );
						$value2 = str_replace( "\'", "'", $value2 );
						$tmp3 .= urlencode( $value2 ) . $amp;
						$i = 0;
					}
				}
			}
			if ( ! empty( $amp ) ) {
				$tmp3 = substr( $tmp3, 0, - $amp_size );
			}
			$parameter = "?" . $tmp3;
		}

		if ( ! $has_protocol_prefix ) {

			foreach ( $this->getpost_parameter_to_keep AS $value ) {
				if ( isset( $_GET[ $value ] ) || isset( $_POST[ $value ] ) ) {
					$class_get = new class_get();
					$tmp       = $class_get->get( $value );
					$tmp       = $value . "=" . $tmp;

					if ( ! empty( $parameter ) ) {
						$parameter .= $amp . $tmp;
					} else {
						$parameter = "?" . $tmp;
					}
				}
			}
		}

		if ( $add_anker ) {
			$parameter .= $anker;
		}

		return $parameter;
	}

	public function fix_var_for_js( $string, $replace_type = REPLACE_DOUBLE_QUOTES ) {
		$string = str_replace( "\n", "\\n", $string );
		$string = str_replace( "\r", "\\r", $string );

		if ( $replace_type == REPLACE_DOUBLE_QUOTES ) {
			return str_replace( '"', '\"', $string );
		} elseif ( $replace_type == REPLACE_SINGLE_QUOTES ) {
			return str_replace( "'", "\'", $string );
		} else {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong replace type: '$replace_type'" );

			return $string;
		}
	}

	public function fix_html_attributes( $string, $replace_type = REPLACE_DOUBLE_QUOTES ) {
		if ( $replace_type == REPLACE_DOUBLE_QUOTES ) {
			return str_replace( "\"", "'", $string );
		} elseif ( $replace_type == REPLACE_SINGLE_QUOTES ) {
			return str_replace( "'", "\"", $string );
		} else {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong replace type: '$replace_type'" );

			return $string;
		}
	}

	public function fix_link( $string, $cast_amp = true, $has_protocol_prefix = false ) {

		if ( $cast_amp ) {
			$amp    = "&amp;";
			$string = $this->cast_amp( $string );
		} else {
			$amp = "&";
		}
		$amp_length = strlen( $amp );

		$parameter_to_add = "";
		if ( ! $has_protocol_prefix ) {

			foreach ( $this->getpost_parameter_to_keep AS $value ) {
				if ( isset( $_GET[ $value ] ) || isset( $_POST[ $value ] ) ) {
					$tmp = $this->get( $value );
					$tmp = $value . "=" . $tmp;

					$parameter_to_add .= $tmp . $amp;
				}
			}
		}
		if ( ! empty( $parameter_to_add ) ) {
			$parameter_to_add = substr( $parameter_to_add, 0, - $amp_length );
		} else {
			return $string;
		}

		if ( is_null( $string ) || empty( $string ) ) {
			return "?" . $parameter_to_add;
		} else {
			if ( strpos( $string, "?" ) === false ) {
				return $string . "?" . $parameter_to_add;
			} else {
				return $string . $amp . $parameter_to_add;
			}
		}
	}

	public function mail_admin( $location, $message, $subject = "", $masked_ip = true ) {
		if ( is_null( $this->visitor->country ) ) {
			$country = "k.A.";
		} else {
			$country_class = new class_country();
			$country       = $country_class->get_country_name( $this->visitor->country );
		}

		if ( $masked_ip ) {
			$ip = $this->get_user_ip();
		} else {
			$ip = $this->get_user_ip( ESCAPED, NOT_MASKED );
		}
		$referer = $this->get_referer();
		if ( empty( $referer ) ) {
			$referer = DISPLAY_EMPTY_VALUE;
		}

		$user_agent = $this->get_user_agent();
		if ( empty( $user_agent ) ) {
			$user_agent = DISPLAY_EMPTY_VALUE;
		}
		$class_checkbot = new class_check_bot();
		$is_bot         = $this->lang->answer( $class_checkbot->check_bot() );
		$is_human       = $this->lang->answer( $this->visitor->is_human );
		$is_phone       = $this->lang->answer( $this->visitor->is_phone );
		$is_table       = $this->lang->answer( $this->visitor->is_tablet );
		$address        = $this->loc->get_complete_url();
		/*
		$platform = $this->visitor->platform;
		$platform_version = $this->visitor->platform_version;
		$browser = $this->visitor->browser;
		$browser_version = $this->visitor->browser_version;
*/

		$td_style = " style='border-bottom: 1px solid #999'";

		$message_with_infos = "
<table style='border: 1px solid #000' cellspacing='0' cellpadding='6'>
<tr><td" . $td_style . ">Message:</td><td" . $td_style . ">$message</td></tr>
<tr><td>Address:</td><td>$address</td></tr>
<tr><td" . $td_style . ">Referer:</td><td" . $td_style . ">$referer</td></tr>
<tr><td>Mensch:</td><td>$is_human</td></tr>
<tr><td" . $td_style . ">Bot:</td><td" . $td_style . ">$is_bot</td></tr>
<tr><td>Land:</td><td>$country</td></tr>
<tr><td>IP:</td><td>$ip</td></tr>
<tr><td" . $td_style . ">User Agent:</td><td" . $td_style . ">$user_agent</td></tr>
<tr><td>Phone:</td><td>$is_phone</td></tr>
<tr><td>Tablet:</td><td>$is_table</td></tr>
</table>
";
		/*
		 * <tr><td>Platform:</td><td>$platform</td></tr>
		<tr><td>Version:</td><td>$platform_version</td></tr>
		<tr><td>Browser:</td><td>$browser</td></tr>
		<tr><td>Version:</td><td>$browser_version</td></tr>
		 */

		$class_mail = new class_mail();
		$class_mail->send_short_admin( $location, $message_with_infos, $subject );
	}


	public function get_content( $file, $use_dir_root = true ) {
		if ( $use_dir_root ) {
			$file2       = DIRROOT . $file;
			$path_prefix = DIRROOT;
		} else {
			$file2       = $file;
			$path_prefix = "";
		}

		$file_content = array();
		$size         = false;
		if ( file_exists( $file2 ) ) {
			$size        = @filesize( $file2 );
			$file_exists = true;
			$handle      = fopen( $file2, "rb" );
			while ( ! feof( $handle ) ) {
				$file_content[] = fgets( $handle );
			}
			fclose( $handle );
		} else {
			$file_exists = false;
		}

		return array( "file"        => $file2,
		              "path_prefix" => $path_prefix,
		              "size"        => $size,
		              "exists"      => $file_exists,
		              "content"     => $file_content
		);
	}

	public function get_file_array( $file, $use_dir_root = true ) {
		if ( $use_dir_root ) {
			$file = DIRROOT . $file;
		}

		$handle = fopen( $file, "r" );

		if ( $handle === false ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't open file for reading: '$file'" );

			return false;
		} else {
			$zeilen = array();
			while ( ! feof( $handle ) ) {
				$zeilen[] = fgets( $handle );
			}
			fclose( $handle );

			return $zeilen;
		}
	}

	// Thanks to http://stackoverflow.com/questions/3338123/how-do-i-recursively-delete-a-directory-and-its-entire-contents-files-sub-dir
	public function delete_recursive( $dir, $use_webroot = true, $use_dirroot = true ) {
		if ( $use_webroot ) {
			$dir = WEBROOT . $dir;
		}

		if ( $use_dirroot ) {
			$dir = DIRROOT . $dir;
		}

		if ( is_dir( $dir ) ) {
			$objects = scandir( $dir );
			foreach ( $objects as $object ) {
				if ( $object != "." && $object != ".." ) {
					if ( filetype( $dir . "/" . $object ) == "dir" ) {
						$this->delete_recursive( $dir . "/" . $object, NO_WEBROOT, NO_DIRROOT );
					} else {
						unlink( $dir . "/" . $object );
					}
				}
			}
			reset( $objects );
			rmdir( $dir );
		}
	}

	public function copy_recursive( $source, $destination, $use_webroot = true, $use_dirroot = true ) {
		if ( $use_webroot ) {
			$source      = WEBROOT . $source;
			$destination = WEBROOT . $destination;
		}

		if ( $use_dirroot ) {
			$source      = DIRROOT . $source;
			$destination = DIRROOT . $destination;
		}
		$result = mkdir( $destination, 0775 );
		if ( $result === false ) {
			return $result;
		}
		$result = $this->recurse_copy( $source, $destination );

		return $result;
	}

	// Thanks to http://php.net/manual/de/function.copy.php#91010
	protected function recurse_copy( $src, $dst, $i = 0 ) {
		$dir = opendir( $src );
		@mkdir( $dst, 0775 );
		while ( false !== ( $file = readdir( $dir ) ) ) {
			if ( ( $file != '.' ) && ( $file != '..' ) ) {
				if ( is_dir( $src . '/' . $file ) ) {
					$this->recurse_copy( $src . '/' . $file, $dst . '/' . $file, ++ $i );
				} else {
					copy( $src . '/' . $file, $dst . '/' . $file );
					++ $i;
				}
			}
		}
		closedir( $dir );

		return $i;
	}

	public function strlen_language_all( $table, $row, $id ) {
		$chars = "";
		foreach ( $this->lang->languages as $key => $array ) {
			$chars .= "<p><nobr>";
			if ( $this->lang->count_of_languages > 1 ) {
				$chars .= $array["text"] . ": ";
			}

			$sql_chars    = "SELECT `" . $row . $array['number'] . "` FROM `" . TP . $table . "` WHERE `id` = '" . $id . "'";
			$result_chars = $this->db->query( $sql_chars, __FILE__ . ":" . __LINE__ );
			$row_chars    = $result_chars->fetch_assoc();
			$chars .= strlen( $row_chars[ $row . $array['number'] ] ) . "</nobr></p>";
		}

		return $chars;
	}

	public function get_language_text( $lang_number ) {
		if ( ! is_null( $lang_number ) && ! empty( $lang_number ) ) {
			$sql = "SELECT `language` FROM `" . TP . "language_settings` WHERE `active` = '1' AND `number` LIKE '$lang_number'";
		} else {
			$sql = "SELECT `language` FROM `" . TP . "language_settings` WHERE `active` = '1' AND `number` IS NULL";
		}

		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows == 1 ) {
			$row = $result->fetch_assoc();

			return $row['language'];
		} else {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Language Text not found, lang_number: '$lang_number', num_rows: '" . $result->num_rows . "', sql: '" . $sql . "'" );

			return $this->txt( "not_found" );
		}
	}


	public function get_lhint() {
		if ( $this->lang->count_of_languages > 1 ) {
			return " *";
		} else {
			return "";
		}
	}


	public function get_session( $get_parameter, $default_value, $session_variable, $reset, $do_escape = true, $do_htmlspecialchars = false, $use_session = true ) {
		if ( $reset ) {
			if ( isset( $_SESSION[ $session_variable ] ) ) {
				unset( $_SESSION[ $session_variable ] );
			}
		}

		if ( ! isset( $_SESSION[ $session_variable ] ) ) {
			$_SESSION[ $session_variable ] = $default_value;
			$default_get_value             = "";
		} else {
			$default_get_value = $_SESSION[ $session_variable ];
		}

		$result = $this->get( $get_parameter, $default_get_value, $do_escape, $do_htmlspecialchars, $use_session );

		$_SESSION[ $session_variable ] = $result;

		return $result;
	}

	public function get_singletext( $name ) {
		if ( empty( $name ) || ! is_numeric( $name ) || $name <= 0 ) {
			$sql = "SELECT * FROM `" . TP . "text_single` WHERE `name` LIKE '$name' AND `deleted` = '0' AND `active` = '1' ORDER BY `changed` LIMIT 0,1";
		} else {
			$sql = "SELECT * FROM `" . TP . "text_single` WHERE `id` = '$name' AND `deleted` = '0' AND `active` = '1' ORDER BY `changed` LIMIT 0,1";
		}

		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 0 ) {
			$row = $result->fetch_assoc();
			if ( ! empty( $row[ 'text' . $this->lang->lnbr ] ) ) {
				$result = $row[ 'text' . $this->lang->lnbr ];
			} elseif ( ! empty( $row[ 'text' . $this->lang->lnbrm ] ) ) {
				$result = $row[ 'text' . $this->lang->lnbrm ];
			} else {
				$this->log->notice( "language", __FILE__ . ":" . __LINE__, "Content for single text is empty. Key: '$name', sql: '$sql'" );
				$result = "";
			}
		} else {
			$result = "";
			$this->log->notice( "parse", __FILE__ . ":" . __LINE__, "Text not found for: '" . $name . "', sql: '$sql'" );
		}

		return $result;
	}
}

