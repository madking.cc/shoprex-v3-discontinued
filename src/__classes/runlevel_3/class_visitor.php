<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

require_once( DIRROOT . "__external/load_mobile_detect.php" );
require_once( DIRSETTINGSCUSTOM . "class_visitor_settings.php" );

class class_visitor extends class_visitor_settings {
	protected static $_instance = null;

	public $ip;
	public $referer;
	public $browser;
	public $browser_ar;
	public $user_agent;
	public $page;
	public $country;
	public $is_phone;
	public $is_tablet;
	public $platform;
	public $platform_version;
	public $browser_version;
	public $browser_bits;
	public $platform_bits;
	public $language;
	public $system_id;
	public $visited;
	public $is_human;
	public $is_bot;
	public $is_browser_compatible;
	public $is_mobile;

	protected $p;
	protected $loc;

	public $visitor_type;
	public $store;
	public $screen_width;
	public $screen_height;
	public $monitor_width;
	public $monitor_height;
	public $monitor_avail_width;
	public $monitor_avail_height;
	public $no_count;

	protected $get;
	protected $check_bot;
	protected $lang;
	protected $db;
	protected $log;

	public function __construct() {
		parent::__construct();

		$this->get       = new class_get();
		$this->check_bot = new class_check_bot();
		$this->loc       = class_location::getInstance();
		$this->lang      = class_language::getInstance();
		$this->db        = class_database::getInstance();
		$this->log       = class_logging::getInstance();

		$this->set_variables();


		if ( $this->store ) {
			$this->store_visitor_unique(); // Also generates System ID
			$this->store();
		} else {
			$this->store_visitor_unique( DONT_UPDATE ); // Also generates System ID
		}
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public static function getInstance() {
		if ( null === self::$_instance ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	protected function __clone() {
	}

	protected function set_variables() {
		if ( isset( $GLOBALS['do_visitor_store'] ) ) {
			$this->store = $GLOBALS['do_visitor_store'];
		} else {
			$this->store = true;
		}

		$preview_mode = PREVIEW_MODE;
		if ( ! $preview_mode ) {
			$this->no_count = $this->get->get( 'no_count', false );
			if ( $this->no_count !== false ) {
				$this->no_count = $this->set_var( $this->no_count, NOT_NUMERIC );
			} else {
				$ajax_no_count = $this->get->get( "ajax_no_count", false );
				if ( $ajax_no_count !== false ) {
					$this->no_count = $this->set_var( $ajax_no_count, NOT_NUMERIC );
				} else {
					$this->no_count == false;
				}
			}

		} else {
			$this->no_count = false;
		}

		$this->system_id            = null;
		$this->visited              = false;
		$this->ip                   = $this->get_user_ip();
		$this->screen_width         = $this->set_var( $this->get->get( 'window_width', 0 ) );
		$this->screen_height        = $this->set_var( $this->get->get( 'window_height', 0 ) );
		$this->monitor_width        = $this->set_var( $this->get->get( 'monitor_width', 0 ) );
		$this->monitor_height       = $this->set_var( $this->get->get( 'monitor_height', 0 ) );
		$this->monitor_avail_width  = $this->set_var( $this->get->get( 'monitor_avail_width', 0 ) );
		$this->monitor_avail_height = $this->set_var( $this->get->get( 'monitor_avail_height', 0 ) );
		$this->user_agent           = $this->get_user_agent();

		$referer = $this->get->get( "ajax_referer" );
		if ( empty( $referer ) && isset( $_SERVER['HTTP_REFERER'] ) ) {
			$this->referer = $this->set_var( $_SERVER['HTTP_REFERER'], NOT_NUMERIC );
		} else {
			$this->referer = $this->set_var( $referer, NOT_NUMERIC );
		}

		$this->is_browser_compatible = true;
		$this->set_mobile();

		if ( $this->get->is_set( "ajax_page" ) ) {
			$page       = $this->get->get( "ajax_page", "index.php" );
			$this->page = $this->set_var( $page, NOT_NUMERIC );
		} else {
			$this->page = $this->loc->uri;
		}


		//$loc = class_location::getInstance();
		//$this->log->error("idefix", __FILE__.":".__LINE__, "uri: '".$loc->uri."', Page: '".$this->page."'");
		$this->set_browser();
		$this->set_country();
		$this->set_language();

		$this->is_bot   = $this->check_bot->check_bot();
		$this->is_human = ! $this->is_bot;
		$this->set_visitor_type( $this->is_human );

	}

	protected function set_mobile() {
		$detect = new Mobile_Detect;

		$this->is_phone  = false;
		$this->is_tablet = false;
		$this->is_mobile = false;

		// Phone
		if ( $detect->isMobile() && ! $detect->isTablet() ) {
			$this->is_phone  = true;
			$this->is_mobile = true;
		} // Tablet
		elseif ( $detect->isMobile() && $detect->isTablet() ) {
			$this->is_tablet = true;
			$this->is_mobile = true;
		}
	}

	protected function set_browser() {
		$this->browser_ar       = false;
		$this->platform         = false;
		$this->platform_version = false;
		$this->browser          = false;
		$this->browser_version  = false;
		$this->browser_bits     = false;
		$this->platform_bits    = false;

		if ( $this->browscap_active ) {
			$ini_memory_limit = ini_get( 'memory_limit' );
			ini_set( 'memory_limit', '-1' );

			$browscap_class = realpath( __DIR__ ) . "/inc/Browscap.php";
			$cache_dir      = realpath( __DIR__ ) . "/../_cache/browscap";

			// Loads the class
			require $browscap_class;

			// Disable in Browscap.php: namespace phpbrowscap;

			// Create a new Browscap object (loads or creates the cache)
			$bc = new Browscap( $cache_dir );

			$bc->lowercase = true;
			$browser       = $bc->getBrowser();
			ini_set( 'memory_limit', $ini_memory_limit );


			if ( isset( $browser->platform ) ) {
				$this->platform = $browser->platform;
			}
			if ( isset( $browser->platform_version ) ) {
				$this->platform_version = $browser->platform_version;
			}
			if ( isset( $browser->browser ) ) {
				$this->browser = $browser->browser;
			}
			if ( isset( $browser->version ) ) {
				$this->browser_version = $browser->version;
			}
			if ( isset( $browser->browser_bits ) ) {
				$this->browser_bits = $browser->browser_bits;
			}
			if ( isset( $browser->platform_bits ) ) {
				$this->platform_bits = $browser->platform_bits;
			}

			$this->browser_ar                     = array();
			$this->browser_ar['platform']         = $this->platform;
			$this->browser_ar['platform_version'] = $this->platform_version;
			$this->browser_ar['browser']          = $this->browser;
			$this->browser_ar['version']          = $this->version;
			$this->browser_ar['browser_bits']     = $this->browser_bits;
			$this->browser_ar['platform_bits']    = $this->platform_bits;
		}
	}

	public function set_country( $ip = "" ) {
		if ( ! isset( $ip ) || empty( $ip ) ) {
			$ip = $this->get_user_ip( NOT_ESCAPED, NOT_MASKED, NOT_ENCODED );
		}

		$type = $this->get_ip_type( $ip );

		if ( $type != false ) {
			$ip_bin     = inet_pton( $ip );
			$ip_bin_sql = $this->db->id->real_escape_string( "0x" . bin2hex( $ip_bin ) );

			$sql    = "SELECT `country` FROM `" . TP . "big_geoip` WHERE `type` = '$type' AND `start` <= '$ip_bin_sql' ORDER BY `start` DESC LIMIT 1";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row           = $result->fetch_assoc();
				$this->country = $row['country'];

				return true;
			} else {
				$this->log->notice( "geo-ip", __FILE__ . ":" . __LINE__, "Can't find Country for ip: '" . htmlspecialchars( $ip ) . "'" );
				$this->country = false;

				return false;
			}
		} else {
			$this->country = false;

			return false;
		}
	}

	protected function set_language() {
		$language_ajax = $this->get->get( 'ajax_lang', false );
		$found         = false;
		if ( $language_ajax !== false ) {
			if ( is_array( $this->lang->languages ) ) {
				foreach ( $this->lang->languages as $array ) {

					if ( empty( $language_ajax ) && empty( $array['number'] ) ) {
						$found = true;
						break;
					} elseif ( strcasecmp( $array['number'], $language_ajax ) === 0 ) {
						$found = true;
						break;
					}
				}
			}
		}
		if ( ! $found ) {
			if ( isset( $this->lang->lnbr ) ) {
				$this->language = $this->lang->lnbr;
			} else {
				$this->language = $this->lang->lnbrm;
			}
		} else {
			$this->language = $this->set_var( $language_ajax, NOT_NUMERIC );
		}


	}

	protected function set_visitor_type( $visitor_type ) {
		if ( $visitor_type ) {
			$this->visitor_type = 1;
		} else {
			$this->visitor_type = 2;
		}
	}

	protected function set_var( $var, $output_num = true, $escape = true ) {
		if ( $output_num ) {
			return intval( $var );
		} else {
			if ( $var == "null" || $var == "undefined" ) {
				return "";
			} else {
				if ( $escape ) {
					return $this->db->escape( $var );
				} else {
					return $var;
				}
			}
		}
	}


	public function store() {
		if ( ! $this->check_no_count() && $this->store ) {
			$this->store_browser();
			$this->store_referer();
			$result = $this->store_visit();
			if ( $result ) {
				$this->store_by_date( "visit" );
				$this->store_by_language( "visit" );
			}
			$this->store_hit();
			$this->store_by_date( "hit" );
			$this->store_by_language( "hit" );
			$this->store_page_views();
			$this->store_page_hits();
			if ( ! $this->is_browser_compatible ) {
				$this->store_not_compatible();
			}
			if ( $this->visited == false ) {
				$this->visited = $this->visited_by_ip();
			}
			$this->store_window_size();
			$this->store_monitor_size();
		}
	}

	public function check_no_count() {
		if ( $this->no_count !== false ) {
			if ( $this->no_count == NO_COUNT_CODE ) {
				return true;
			}
		}

		return false;
	}


















	//
	// Informationen setzen
	//

	protected function store_visitor_unique( $do_update = true ) {
		$country        = $this->country;
		$country_search = $this->db->search( "country", $country, NO_PART_SEARCH, BINARY );

		$platform        = $this->platform;
		$platform_search = $this->db->search( "platform", $platform, NO_PART_SEARCH, BINARY );

		$platform_version        = $this->platform_version;
		$platform_version_search = $this->db->search( "platform_version", $platform_version, NO_PART_SEARCH, BINARY );

		$browser        = $this->browser;
		$browser_search = $this->db->search( "browser", $browser, NO_PART_SEARCH, BINARY );

		$browser_version        = $this->browser_version;
		$browser_version_search = $this->db->search( "browser_version", $browser_version, NO_PART_SEARCH, BINARY );

		$phone        = $this->is_phone;
		$phone_search = $this->db->search( "phone", $phone, NO_PART_SEARCH, BINARY );

		$tablet       = $this->is_tablet;
		$table_search = $this->db->search( "tablet", $tablet, NO_PART_SEARCH, BINARY );

		$ip        = $this->get_user_ip( NOT_ESCAPED, NOT_MASKED, ENCODED );
		$ip_search = $this->db->search( "ip", $ip, NO_PART_SEARCH, BINARY );

		$user_agent        = $this->user_agent;
		$user_agent_search = $this->db->search( "user_agent", $user_agent, NO_PART_SEARCH, BINARY );

		$type        = $this->visitor_type;
		$type_search = $this->db->search( "type", $type, NO_PART_SEARCH, BINARY );
		/*
		$monitor_width = $this->monitor_width;
		$monitor_width_search = $this->db->search("monitor_width", $monitor_width, NO_PART_SEARCH, BINARY);
		$monitor_height = $this->monitor_height;
		$monitor_height_search = $this->db->search("monitor_height", $monitor_height, NO_PART_SEARCH, BINARY);
		*/

		$sql    = "SELECT `id` FROM `" . TP . "visitor_unique` WHERE
             " . $type_search . " AND
             " . $country_search . " AND
             " . $platform_search . " AND
             " . $platform_version_search . " AND
             " . $browser_search . " AND
             " . $browser_version_search . " AND
             " . $phone_search . " AND
             " . $table_search . " AND
             " . $ip_search . " AND
             " . $user_agent_search;
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows == 0 ) {
			$this->db->ins( __FILE__ . ":" . __LINE__, "visitor_unique",
				array(
					"created",
					"type",
					"country",
					"platform",
					"platform_version",
					"browser",
					"browser_version",
					"phone",
					"tablet",
					"ip",
					"user_agent"
				),
				array(
					"NOW()",
					$type,
					$country,
					$platform,
					$platform_version,
					$browser,
					$browser_version,
					$phone,
					$tablet,
					$ip,
					$user_agent
				) );
			$this->system_id = $this->db->get_insert_id();
		} else {
			$row             = $result->fetch_assoc();
			$this->system_id = $row['id'];
			if ( $do_update ) {
				$sql = "UPDATE `" . TP . "visitor_unique` SET `count` = (`count` + 1) WHERE `id` = '" . $row['id'] . "';";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			}
		}
	}


	protected function store_browser() {
		$browser = $this->get_browser();
		if ( empty( $browser ) ) {
			$browser = array();
		}
		if ( ! isset( $browser['platform'] ) || empty( $browser['platform'] ) ) {
			$browser['platform_search'] = "IS NULL";
			$browser['platform']        = "NULL";
		} else {
			$browser['platform_search'] = "LIKE '" . $browser['platform'] . "'";
		}

		if ( ! isset( $browser['platform_version'] ) || empty( $browser['platform_version'] ) ) {
			$browser['platform_version_search'] = "IS NULL";
			$browser['platform_version']        = "NULL";
		} else {
			$browser['platform_version_search'] = "LIKE '" . $browser['platform_version'] . "'";
		}

		if ( ! isset( $browser['browser'] ) || empty( $browser['browser'] ) ) {
			$browser['browser_search'] = "IS NULL";
			$browser['browser']        = "NULL";
		} else {
			$browser['browser_search'] = "LIKE '" . $browser['browser'] . "'";
		}

		if ( ! isset( $browser['version'] ) || empty( $browser['version'] ) ) {
			$browser['version_search'] = "IS NULL";
			$browser['version']        = "NULL";
		} else {
			$browser['version_search'] = "LIKE '" . $browser['version'] . "'";
		}

		$phone  = 0;
		$tablet = 0;
		if ( $this->is_phone ) {
			$phone = 1;
		}
		if ( $this->is_tablet ) {
			$tablet = 1;
		}

		$sql    = "SELECT `id` FROM `" . TP . "statistics_by_system` WHERE
            `platform` " . $browser['platform_search'] . " AND
            `platform_version` " . $browser['platform_version_search'] . " AND
            `browser` " . $browser['browser_search'] . " AND
            `version` " . $browser['version_search'] . " AND
            `type` = '" . $this->visitor_type . "' AND
            `system_id` = '" . $this->system_id . "' AND
            `phone` = '" . $phone . "' AND
            `tablet` = '" . $tablet . "'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows == 0 ) {
			$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_by_system", array(
				"type",
				"system_id",
				"platform",
				"platform_version",
				"browser",
				"version",
				"phone",
				"tablet",
				"created"
			), array(
				$this->visitor_type,
				$this->system_id,
				$browser['platform'],
				$browser['platform_version'],
				$browser['browser'],
				$browser['version'],
				$phone,
				$tablet,
				"NOW()"
			) );
		} else {

			$row = $result->fetch_assoc();
			$sql = "UPDATE `" . TP . "statistics_by_system` SET `visitors` = (`visitors` + 1) WHERE `id` ='" . $row['id'] . "'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		}
	}

	public function get_browser() {
		if ( empty( $this->browser_ar ) || ! isset( $this->browser_ar ) ) {
			return false;
		} else {
			return $this->browser_ar;
		}
	}

	public function get_referer_outside_domain( $referer ) {
		if ( $this->check_first_string_match( $this->loc->httpauto_root, $referer ) ) {
			return false;
		} elseif ( $this->check_first_string_match( $this->loc->httpauto_web_root, $referer ) ) {
			return false;
		} else {
			return $referer;
		}
	}


	protected function store_referer() {
		$referer = $this->get_referer_visitor();
		if ( ! isset( $referer ) || empty( $referer ) ) {
			return false;
		} else {
			$referer = $this->get_referer_outside_domain( $referer );
			if ( empty( $referer ) ) {
				return false;
			}
		}

		$sql    = "SELECT `id` FROM `" . TP . "statistics_referer` WHERE `referer` LIKE '$referer' AND `type` = '" . $this->visitor_type . "'  AND `system_id` = '" . $this->system_id . "'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			$row = $result->fetch_assoc();

			$sql = "UPDATE `" . TP . "statistics_referer` SET `count` = (`count` + 1) WHERE `id`='" . $row['id'] . "'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {

			$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_referer", array(
				"type",
				"system_id",
				"referer",
				"created"
			), array( $this->visitor_type, $this->system_id, $referer, "NOW()" ) );
		}
	}

	public function get_referer_visitor() {
		if ( ! isset( $this->referer ) || empty( $this->referer ) ) {
			return false;
		} else {
			return $this->referer;
		}
	}


	protected function store_window_size() {
		if ( ! empty( $this->screen_width ) && ! empty( $this->screen_height ) ) {
			if ( $this->screen_width > $this->screen_height ) {
				$x = $this->screen_width;
				$y = $this->screen_height;
			} else {
				$y = $this->screen_width;
				$x = $this->screen_height;
			}

			$sql    = "SELECT `id` FROM `" . TP . "statistics_by_windowsize` WHERE
                `width` = '" . $x . "' AND
                `height` = '" . $y . "' AND
                `type` = '" . $this->visitor_type . "'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 0 ) {
				$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_by_windowsize", array(
					"type",
					"width",
					"height",
					"created"
				),
					array( $this->visitor_type, $x, $y, "NOW()" ) );
			} else {
				$row = $result->fetch_assoc();
				$sql = "UPDATE `" . TP . "statistics_by_windowsize` SET `hits` = (`hits` + 1) WHERE id ='" . $row['id'] . "'";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			}
		}
	}


	protected function store_monitor_size() {
		if ( ! empty( $this->monitor_width ) && ! empty( $this->monitor_height ) ) {
			if ( $this->monitor_width > $this->monitor_height ) {
				$x = $this->monitor_width;
				$y = $this->monitor_height;
			} else {
				$y = $this->monitor_width;
				$x = $this->monitor_height;
			}

			$sql    = "SELECT `id` FROM `" . TP . "statistics_by_resolution` WHERE
                `width` = '" . $x . "' AND
                `height` = '" . $y . "' AND
                `type` = '" . $this->visitor_type . "'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 0 ) {
				$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_by_resolution", array(
					"type",
					"width",
					"height",
					"created"
				),
					array( $this->visitor_type, $x, $y, "NOW()" ) );
			} else {
				$row = $result->fetch_assoc();
				$sql = "UPDATE `" . TP . "statistics_by_resolution` SET `hits` = (`hits` + 1) WHERE id ='" . $row['id'] . "'";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			}
		}


		if ( ! empty( $this->monitor_avail_width ) && ! empty( $this->monitor_avail_height ) ) {
			if ( $this->monitor_avail_width > $this->monitor_avail_height ) {
				$x = $this->monitor_avail_width;
				$y = $this->monitor_avail_height;
			} else {
				$y = $this->monitor_avail_width;
				$x = $this->monitor_avail_height;
			}

			$sql    = "SELECT `id` FROM `" . TP . "statistics_by_resolution_avail` WHERE
                `width` = '" . $x . "' AND
                `height` = '" . $y . "' AND
                `type` = '" . $this->visitor_type . "'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 0 ) {
				$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_by_resolution_avail", array(
					"type",
					"width",
					"height",
					"created"
				),
					array( $this->visitor_type, $x, $y, "NOW()" ) );
			} else {
				$row = $result->fetch_assoc();
				$sql = "UPDATE `" . TP . "statistics_by_resolution_avail` SET `hits` = (`hits` + 1) WHERE id ='" . $row['id'] . "'";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			}
		}
	}





	//
	// Holen von Informationen
	//


	protected function escape_str( $string ) {
		return $this->db->escape( $string );
	}

	protected function store_visit() {

		$ip = $this->get_user_ip( NOT_ESCAPED, NOT_MASKED, ENCODED );
		if ( ! isset( $ip ) || empty( $ip ) ) {
			$this->visited = false;

			return false;
		}

		if ( ! isset( $this->country ) || empty( $this->country ) ) {
			$search  = " AND `country` is NULL";
			$country = "NULL";
		} else {
			$search  = " AND `country` LIKE '" . $this->country . "'";
			$country = $this->country;
		}

		$sql    = "SELECT `id`, `created`, `changed` FROM `" . TP . "statistics_count` WHERE `ip` LIKE '$ip' AND `type` = '" . $this->visitor_type . "' " . $search;
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			$row = $result->fetch_assoc();

			if ( $row['changed'] == "0000-00-00 00:00:00" ) {
				$changed = strtotime( $row['created'] );
			} else {
				$changed = strtotime( $row['changed'] );
			}
			$now  = time();
			$wait = 60 * 60 * 24; // 1 tag
			// Speicher nur Besucher, nicht jeden Aufruf
			if ( ( $changed + $wait ) <= $now ) {
				$sql = "UPDATE `" . TP . "statistics_count` SET `count` = (`count` + 1) WHERE `id`='" . $row['id'] . "'";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				$this->visited = false;

				return true;
			}
		} else {

			$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_count", array(
				"type",
				"ip",
				"created",
				"country"
			), array( $this->visitor_type, $ip, "NOW()", $country ) );
			$this->visited = false;

			return true;
		}
		$this->visited = true;

		return false; // Hat nichts in der DB gemacht
	}

	protected function store_by_date( $type ) {
		$date_now = date( "Y-m-d" );

		// DB Vorbereiten

		$sql    = "SELECT `id` FROM `" . TP . "statistics_by_date` WHERE `date` = '$date_now' AND `country` IS NULL AND `type` = '" . $this->visitor_type . "'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows == 0 ) {
			$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_by_date", array(
				"type",
				"date",
				"created",
				"country"
			), array( $this->visitor_type, $date_now, "NOW()", "NULL" ) );
		}

		if ( isset( $this->country ) && ! empty( $this->country ) ) {
			$sql    = "SELECT `id` FROM `" . TP . "statistics_by_date` WHERE `date` = '$date_now' AND `country` LIKE '" . $this->country . "' AND `type` = '" . $this->visitor_type . "'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 0 ) {
				$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_by_date", array(
					"type",
					"date",
					"created",
					"country"
				), array( $this->visitor_type, $date_now, "NOW()", $this->country ) );
			}
		}

		// DB eintragen
		if ( $type == "hit" ) {
			$sql = "UPDATE `" . TP . "statistics_by_date` SET `hits` = (`hits` + 1) WHERE `date` = '$date_now' AND `country` IS NULL AND `type` = '" . $this->visitor_type . "'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( isset( $this->country ) && ! empty( $this->country ) ) {

				$sql = "UPDATE `" . TP . "statistics_by_date` SET `hits` = (`hits` + 1) WHERE `date` = '$date_now' AND `country` LIKE '" . $this->country . "' AND `type` = '" . $this->visitor_type . "'";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			}
		} elseif ( $type == "visit" ) {
			$sql = "UPDATE `" . TP . "statistics_by_date` SET `visits` = (`visits` + 1) WHERE `date` = '$date_now' AND `country` IS NULL AND `type` = '" . $this->visitor_type . "'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( isset( $this->country ) && ! empty( $this->country ) ) {

				$sql = "UPDATE `" . TP . "statistics_by_date` SET `visits` = (`visits` + 1) WHERE `date` = '$date_now' AND `country` LIKE '" . $this->country . "' AND `type` = '" . $this->visitor_type . "'";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			}
		}
	}

	protected function store_by_language( $type ) {
		$date_now = date( "Y-m-d" );

		$language_search = $country_search = $this->db->search( "lang", $this->language, NO_PART_SEARCH, BINARY, EMPTY_VAL_IS_NULL );

		if ( $type == "hit" ) {
			// DB Vorbereiten

			$sql    = "SELECT `id` FROM `" . TP . "statistics_by_language` WHERE `date` = '$date_now' AND $language_search AND `country` IS NULL AND `type` = '" . $this->visitor_type . "'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 0 ) {
				$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_by_language", array(
					"type",
					"date",
					"created",
					"country",
					"lang"
				), array( $this->visitor_type, $date_now, "NOW()", "NULL", $this->language ) );
			}

			if ( isset( $this->country ) && ! empty( $this->country ) ) {
				$sql    = "SELECT `id` FROM `" . TP . "statistics_by_language` WHERE `date` = '$date_now' AND $language_search AND `country` LIKE '" . $this->country . "' AND `type` = '" . $this->visitor_type . "'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				if ( $result->num_rows == 0 ) {
					$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_by_language", array(
						"type",
						"date",
						"created",
						"country",
						"lang"
					), array( $this->visitor_type, $date_now, "NOW()", $this->country, $this->language ) );
				}
			}

			// DB eintragen

			$sql = "UPDATE `" . TP . "statistics_by_language` SET `hits` = (`hits` + 1) WHERE `date` = '$date_now' AND $language_search AND `country` IS NULL AND `type` = '" . $this->visitor_type . "'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( isset( $this->country ) && ! empty( $this->country ) ) {

				$sql = "UPDATE `" . TP . "statistics_by_language` SET `hits` = (`hits` + 1) WHERE `date` = '$date_now' AND $language_search AND `country` LIKE '" . $this->country . "' AND `type` = '" . $this->visitor_type . "'";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			}
		} elseif ( $type == "visit" ) {
			// DB Vorbereiten

			$sql    = "SELECT `id` FROM `" . TP . "statistics_by_language` WHERE `date` = '$date_now' AND $language_search AND `country` IS NULL AND `type` = '" . $this->visitor_type . "'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 0 ) {
				$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_by_language", array(
					"type",
					"date",
					"created",
					"country",
					"lang"
				), array( $this->visitor_type, $date_now, "NOW()", "NULL", $this->language ) );
			}

			if ( isset( $this->country ) && ! empty( $this->country ) ) {
				$sql    = "SELECT `id` FROM `" . TP . "statistics_by_language` WHERE `date` = '$date_now' AND $language_search AND `country` LIKE '" . $this->country . "' AND `type` = '" . $this->visitor_type . "'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				if ( $result->num_rows == 0 ) {
					$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_by_language", array(
						"type",
						"date",
						"created",
						"country",
						"lang"
					), array( $this->visitor_type, $date_now, "NOW()", $this->country, $this->language ) );
				}
			}

			// DB eintragen

			$sql = "UPDATE `" . TP . "statistics_by_language` SET `visits` = (`visits` + 1) WHERE `date` = '$date_now' AND $language_search AND `country` IS NULL AND `type` = '" . $this->visitor_type . "'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( isset( $this->country ) && ! empty( $this->country ) ) {

				$sql = "UPDATE `" . TP . "statistics_by_language` SET `visits` = (`visits` + 1) WHERE `date` = '$date_now' AND $language_search AND `country` LIKE '" . $this->country . "' AND `type` = '" . $this->visitor_type . "'";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			}
		}
	}





	//
	// Abfragen
	//

	protected function store_hit() {

		$ip = $this->get_user_ip( NOT_ESCAPED, NOT_MASKED, ENCODED );
		if ( ! isset( $ip ) || empty( $ip ) ) {
			return false;
		}

		if ( is_null( $this->country ) ) {
			$search = " AND `country` is NULL";
		} else {
			$search = " AND `country` LIKE '" . $this->country . "'";
		}

		$sql    = "SELECT `id` FROM `" . TP . "statistics_count` WHERE `ip` LIKE '$ip' AND `type` = '" . $this->visitor_type . "'" . $search;
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			$row = $result->fetch_assoc();

			$sql = "UPDATE `" . TP . "statistics_count` SET `hits` = (`hits` + 1) WHERE `id`='" . $row['id'] . "'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			return true;
		} else {
			$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_count", array(
				"type",
				"ip",
				"created",
				"country",
				"hits",
				"count"
			), array( $this->visitor_type, $ip, "NOW()", $this->country, 1, 0 ) );

			return true;
		}
	}











	//
	// Datenbank Funktionen
	//

	protected function store_page_views() {

		$sql    = "SELECT `id` FROM `" . TP . "statistics_hits` WHERE  `type` = '" . $this->visitor_type . "' ORDER BY `id` DESC LIMIT 1";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows == 0 ) {
			$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_hits", array(
				"type",
				"hits",
				"created"
			), array( $this->visitor_type, 0, "NOW()" ) );
		}

		$sql = "UPDATE `" . TP . "statistics_hits` SET `hits` = (`hits` + 1) WHERE `type` = '" . $this->visitor_type . "' ORDER BY `id` DESC LIMIT 1";
		$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
	}

	protected function store_page_hits() {

		$page = $this->get_page();
		if ( empty( $page ) || ! isset( $page ) ) {
			return false;
		}

		$page = $this->remove_sid( $page );

		$sql    = "SELECT `id` FROM `" . TP . "statistics_page` WHERE `page` LIKE '$page' AND `type` = '" . $this->visitor_type . "';";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			$row = $result->fetch_assoc();
			$sql = "UPDATE `" . TP . "statistics_page` SET `count` = (`count` + 1) WHERE `id`='" . $row['id'] . "'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_page", array(
				"type",
				"page",
				"created"
			), array( $this->visitor_type, $page, "NOW()" ) );
		}

		$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_view_log", array(
			"type",
			"page",
			"by_system_id",
			"country",
			"ip",
			"created"
		), array( $this->visitor_type, $page, $this->system_id, $this->country, $this->get_user_ip(), "NOW()" ) );
	}








	//
	// Datenbank Funktionen
	//

	public function get_page() {
		if ( empty( $this->page ) || ! isset( $this->page ) ) {
			return false;
		} else {
			return $this->page;
		}
	}


	protected function store_not_compatible() {

		// Eintrag in die Datenbank
		$sql    = "SELECT `id` FROM `" . TP . "statistics_not_compatible` WHERE `type` = '" . $this->visitor_type . "' AND  `system_id` = '" . $this->system_id . "' LIMIT 1";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			$row = $result->fetch_assoc();

			$sql = "UPDATE `" . TP . "statistics_not_compatible` SET `count` = (`count` + 1) WHERE `id`='" . $row['id'] . "'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_not_compatible", array(
				"type",
				"system_id",
				"created"
			), array( $this->visitor_type, $this->system_id, "NOW()" ) );
		}
	}

	public function visited_by_ip() {
		$ip = $this->get_user_ip( NOT_ESCAPED, NOT_MASKED, ENCODED );
		if ( ! isset( $ip ) || $ip === false ) {
			return false;
		}

		if ( $this->country === false ) {
			$search = " AND `country` is NULL";
		} else {
			$search = " AND `country` LIKE '" . $this->country . "'";
		}

		$sql    = "SELECT `id`, `created`, `changed` FROM `" . TP . "statistics_count` WHERE `ip` LIKE '$ip' AND `type` = '" . $this->visitor_type . "'" . $search;
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			$row = $result->fetch_assoc();

			if ( $row['changed'] == "0000-00-00 00:00:00" ) {
				$changed = strtotime( $row['created'] );
			} else {
				$changed = strtotime( $row['changed'] );
			}
			$now  = time();
			$wait = WAIT_TIME_FOR_REVISIT;

			if ( ( $changed + $wait ) > $now ) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function visited_by_cookie() {
		// Prüfe anhand von Cookie, ob der Besucher schonmal da war
		// Funktioniert nur beim 2. Aufruf, wenn zum 1. mal das Cookie gesetzt wird!!!
		setcookie( "visited", "Yes", time() + 60 * 60 * 24 * 30 ); // 30 Tage
		if ( isset( $_COOKIE["visited"] ) ) {
			return true;
		} else {
			return false;
		}
	}


	public function store_not_found( $link = "" ) {

		if ( empty( $link ) || ! $this->store ) {
			return false;
		}

		$link = $this->remove_sid( $link );
		if ( is_null( $link ) || empty( $link ) ) {
			$link = "Error with SID remove: " . __FILE__ . ":" . __LINE__;
		}

		$referer = $this->get_referer_visitor();

		if ( ! isset( $referer ) || empty( $referer ) ) {
			$referer_search = "`referer` IS NULL";
			$referer        = "NULL";
		} else {
			$referer_search = "`referer` LIKE '$referer'";
		}

		if ( ! isset( $this->system_id ) || empty( $this->system_id ) ) {
			$system_id_search = "`system_id` IS NULL";
		} else {
			$system_id_search = "`system_id` LIKE '$this->system_id'";
		}

		// Eintrag in die Datenbank
		$sql    = "SELECT `id` FROM `" . TP . "statistics_not_found` WHERE `link` LIKE '$link' AND $referer_search AND $system_id_search AND `type` ='" . $this->visitor_type . "' LIMIT 1";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			$row = $result->fetch_assoc();

			$sql = "UPDATE `" . TP . "statistics_not_found` SET `count` = (`count` + 1) WHERE `id`='" . $row['id'] . "'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$this->db->ins( __FILE__ . ":" . __LINE__, "statistics_not_found", array(
				"type",
				"system_id",
				"link",
				"created",
				"referer"
			), array( $this->visitor_type, $this->system_id, $link, "NOW()", $referer ) );
		}

		return true;
	}

}
