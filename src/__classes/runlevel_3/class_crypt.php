<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

// $hash = $bcrypt->hash('password');
// $isGood = $bcrypt->verify('password', $hash);

// Originally by Andrew Moore
// Src: http://stackoverflow.com/questions/4795385/how-do-you-use-bcrypt-for-hashing-passwords-in-php/6337021#6337021
//
// Heavily modified by Robert Kosek, from data at php.net/crypt

class class_crypt {
	private $rounds;
	private $prefix;
	private $salt_prefix;

	public function __construct( $prefix = '', $rounds = 12 ) {
		if ( CRYPT_BLOWFISH != 1 ) {
			new class_throw_error( "security: " . __FILE__ . ":" . __LINE__ . " bcrypt not supported in this installation. See http://php.net/crypt" );
		}

		$this->rounds = $rounds;
		$this->prefix = $prefix;

		// Determine if this version of PHP has the Bcrypt fix
		$this->salt_prefix = version_compare( PHP_VERSION, '5.3.7' ) >= 0 ? '$2y' : '$2a';
	}

	public function hash( $input ) {
		$hash = crypt( $input, $this->getSalt() );

		if ( strlen( $hash ) > 13 ) {
			return $hash;
		}

		return false;
	}

	private function getSalt() {
		// the base64 function uses +'s and ending ='s; translate the first, and cut out the latter
		return sprintf( '%s$%02d$%s', $this->salt_prefix, $this->rounds, substr( strtr( base64_encode( $this->getBytes() ), '+', '.' ), 0, 22 ) );
	}

	private function getBytes() {
		$bytes = '';

		if ( function_exists( 'openssl_random_pseudo_bytes' ) && ( strtoupper( substr( PHP_OS, 0, 3 ) ) !== 'WIN' ) ) { // OpenSSL slow on Win
			$bytes = openssl_random_pseudo_bytes( 18 );
		}

		if ( $bytes === '' && is_readable( '/dev/urandom' ) && ( $hRand = @fopen( '/dev/urandom', 'rb' ) ) !== false ) {
			$bytes = fread( $hRand, 18 );
			fclose( $hRand );
		}

		if ( $bytes === '' ) {
			$key = uniqid( $this->prefix, true );

			// 12 rounds of HMAC must be reproduced / created verbatim, no known shortcuts.
			// Changed the hash algorithm from salsa20, which has been removed from PHP 5.4.
			for ( $i = 0; $i < 12; $i ++ ) {
				$bytes = hash_hmac( 'snefru256', microtime() . $bytes, $key, true );
				usleep( 10 );
			}
		}

		return $bytes;
	}

	public function verify( $input, $existingHash ) {
		$hash = crypt( $input, $existingHash );

		return $hash === $existingHash;
	}
}