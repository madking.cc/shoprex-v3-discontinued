<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_parse_content extends class_uri_file_path_operations {
	protected static $_instance = null;

	public $load_external_code;

	protected $lang;
	protected $log;
	protected $p;
	protected $l;
	protected $loc;
	protected $db;

	protected $tags;
	protected $functions;
	protected $functions_counter;
	protected $i;


	public static function getInstance() {
		if ( null === self::$_instance ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	protected function __clone() {
	}

	public function __construct() {

		parent::__construct();

		$this->load_external_code = class_load_external::getInstance();
		$this->lang               = class_language::getInstance();
		$this->log                = class_logging::getInstance();

		$this->p = new class_page();

		$this->l   = new class_layout();
		$this->loc = class_location::getInstance();
		$this->db  = class_database::getInstance();

		$this->functions         = array();
		$this->functions_counter = 0;
		$this->i                 = 1;
		$this->tags              = 'displayunicode|showunicode|textarea|page_link|single|size|color|center|alertd|alert|quote|url|img|code|text|file|menu|info|duc|suc|sf|b|t|m|i|s|f';
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public function add_function( $function ) {
		$this->functions[] = $function;
		$this->functions_counter += 1;
	}

	public function parse( $string ) {
		$string = $this->parse_plugins( $string );
		$string = $this->parse_default( $string );
		$string = $this->parse_plugins( $string );
		$string = $this->parse_default( $string );

		return $string;
	}

	protected function parse_plugins( $string ) {
		for ( $i = 1; $this->functions_counter > 0, $i <= $this->functions_counter; $i ++ ) {
			$string = $this->functions[$i - 1]( $string );
		}

		return $string;
	}

	protected function parse_default( $string ) {

		$found = false;

		while ( preg_match_all( '`\[\b(' . $this->tags . ')\b=?(.*?)\](.+?)\[/\1\]`s', $string, $matches ) ) {
			foreach ( $matches[0] as $key => $match ) {
				list( $tag, $param, $innertext ) = array(
					$matches[1][ $key ],
					$matches[2][ $key ],
					$matches[3][ $key ]
				);
				$replacement = "";
				$found       = true;
				switch ( $tag ) {
					case 'page_link':
						$page_id = $param;
						$sql     = "SELECT * FROM `" . TP . "pages` WHERE `id` = '$page_id' AND `deleted` = '0' AND `active` = '1' LIMIT 0,1";
						$result  = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						if ( $result->num_rows != 1 ) {
							$replacement = PAGE_NOT_FOUND;
							$this->log->error( "parse", __FILE__ . ":" . __LINE__, "Page not found for id: '$page_id', sql: '$sql'" );
						} else {
							$row = $result->fetch_assoc();
							//var_dump($innertext);
							$link_parameter = $this->parse_str( $innertext );
							//var_dump($link_parameter);
							$link = $this->p->fix_link( $this->loc->httpauto_web_root . $this->lang->ldir . $row[ 'path' . $this->lang->lnbr ] );

							if ( ! isset( $link_parameter['text'] ) || is_null( $link_parameter['text'] ) || ( empty( $link_parameter['text'] ) && $link_parameter != "0" ) ) {
								$text = $row[ 'path' . $this->lang->lnbr ];
							} else {
								$text = $link_parameter['text'];
							}

							switch ( $link_parameter['role'] ) {
								case "button":
									$replacement = "<a href='" . $link . "' role='button' class='" . $link_parameter['class'] . "'>" . $text . "</a>";
									break;
								default:
									$replacement = "<a href='" . $link . "' class='" . $link_parameter['class'] . "'>" . $text . "</a>";
									break;
							}
						}

						break;


					case 'textarea':
						$replacement = "<textarea" . $innertext . "</textarea>";

						break;
					case 'showunicode':
					case 'suc':
						$replacement = "&#38;#" . $innertext . ";";

						break;
					case 'displayunicode':
					case 'duc':
						$replacement = "&#" . $innertext . ";";

						break;
					case 'b':
						$replacement = "<strong>$innertext</strong>";

						break;
					case 'size':
						$replacement = "<span style=\"font-size: $param;\">$innertext</span>";

						break;
					case 'color':
						$replacement = "<span style=\"color: $param;\">$innertext</span>";

						break;
					case 'center':
						$replacement = "<div class=\"centered\">$innertext</div>";

						break;
					case 'quote':
						$replacement = "<blockquote>$innertext</blockquote>" . $param ? "<cite>$param</cite>" : '';

						break;

					case 'alertd':
						// type: success, info, warning, danger
						if ( empty( $param ) ) {
							$param = "info";
						}
						$replacement = $this->l->alert_text_dismiss( $param, $innertext );

						break;
					case 'alert':
						// type: success, info, warning, danger
						if ( empty( $param ) ) {
							$param = "info";
						}
						$replacement = $this->l->alert_text( $param, $innertext );

						break;

					case 'url':
						$replacement = '<a href="' . ( $param ? $param : $innertext ) . "\">$innertext</a>";

						break;
					case 'img':
						list( $width, $height ) = preg_split( '`[Xx]`', $param );
						$replacement = "<img src=\"$innertext\" " . ( is_numeric( $width ) ? "width=\"$width\" " : '' ) . ( is_numeric( $height ) ? "height=\"$height\" " : '' ) . '/>';

						break;

					case 'code':
						if ( empty( $param ) ) {
							$param = "php";
						}
						$replacement                           = str_replace( "<", "&#60;", $innertext );
						$replacement                           = "<pre class=\"brush: $param\">" . $replacement . "</pre>";
						$this->load_external_code->load_codehl = true;

						break;

					case 's':
					case 'single':
						$key = $innertext;
						if ( empty( $key ) || ! is_numeric( $key ) || $key <= 0 ) {
							$sql = "SELECT * FROM `" . TP . "text_single` WHERE `name` LIKE '$key' AND `deleted` = '0' AND `active` = '1' ORDER BY `changed` LIMIT 0,1";
						} else {
							$sql = "SELECT * FROM `" . TP . "text_single` WHERE `id` = '$key' AND `deleted` = '0' AND `active` = '1' ORDER BY `changed` LIMIT 0,1";
						}

						$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						if ( $result->num_rows != 0 ) {
							$row = $result->fetch_assoc();
							if ( ! empty( $row[ 'text' . $this->lang->lnbr ] ) ) {
								$replacement = $row[ 'text' . $this->lang->lnbr ];
							} elseif ( ! empty( $row[ 'text' . $this->lang->lnbrm ] ) ) {
								$replacement = $row[ 'text' . $this->lang->lnbrm ];
							} else {
								$this->log->error( "language", __FILE__ . ":" . __LINE__, "Content for single text is empty. Key: '$key', sql: '$sql'" );
								$replacement = "";
							}
						} else {
							$replacement = "";
							$this->log->notice( "parse", __FILE__ . ":" . __LINE__, "Text not found for: '" . $key . "', sql: '$sql'" );
						}

						break;

					case 'f':
					case 'file':
						$key         = $innertext;
						$replacement = $this->l->file_link( $key, $param );

						break;

					case 't':
					case 'text':
						$key = $innertext;
						if ( empty( $key ) || ! is_numeric( $key ) || $key <= 0 ) {
							$sql = "SELECT * FROM `" . TP . "text` WHERE `name` LIKE '$key' AND `deleted` = '0' AND `active` = '1'  ORDER BY `changed` LIMIT 0,1";
						} else {
							$sql = "SELECT * FROM `" . TP . "text` WHERE `id` = '$key' AND `deleted` = '0' AND `active` = '1'  ORDER BY `changed`  LIMIT 0,1";
						}

						$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						if ( $result->num_rows != 0 ) {
							$row         = $result->fetch_assoc();
							$replacement = "";

							if ( CONTENT_TEXT_SHOW_HEADER ) {
								if ( ! empty( $row[ 'header' . $this->lang->lnbr ] ) ) {
									$replacement .= "<H1>" . $row[ 'header' . $this->lang->lnbr ] . "</H1>\n";
								} elseif ( ! empty( $row[ 'header' . $this->lang->lmbr ] ) ) {
									$replacement .= "<H1>" . $row[ 'header' . $this->lang->lnbrm ] . "</H1>\n";
								} else {
									$this->log->error( "language", __FILE__ . ":" . __LINE__, "Header for text is empty. Key: '$key', sql: '$sql'" );
								}
							}
							if ( CONTENT_TEXT_SHOW_DATE && ( ! empty( $row['datetime'] ) && $row['datetime'] != "0000-00-00 00:00:00" ) ) {
								$date = $this->p->sqldatetime( $row['datetime'] );
								$replacement .= "<p><strong>" . $date . "</strong></p>\n";
							}

							if ( ! empty( $row[ 'text' . $this->lang->lnbr ] ) ) {
								$replacement .= $row[ 'text' . $this->lang->lnbr ];
							} elseif ( ! empty( $row[ 'text' . $this->lang->lnbrm ] ) ) {
								$replacement .= $row[ 'text' . $this->lang->lnbrm ];
							} else {
								$this->log->notice( "language", __FILE__ . ":" . __LINE__, "Content for text is empty. Key: '$key', sql: '$sql'" );
							}

							if ( ! is_null( $row[ 'js' . $this->lang->lnbr ] ) ) {
								$GLOBALS["body_footer"] .= $row[ 'js' . $this->lang->lnbr ];
							} elseif ( ! is_null( $row[ 'js' . $this->lang->lnbrm ] ) ) {
								$GLOBALS["body_footer"] .= $row[ 'js' . $this->lang->lnbrm ];
							}
							if ( ! is_null( $row[ 'css' . $this->lang->lnbr ] ) ) {
								$GLOBALS["custom_css"] .= $row[ 'css' . $this->lang->lnbr ];
							} elseif ( ! is_null( $row[ 'css' . $this->lang->lnbrm ] ) ) {
								$GLOBALS["custom_css"] .= $row[ 'css' . $this->lang->lnbrm ];
							}
						} else {
							$replacement = "";
							$this->log->notice( "parse", __FILE__ . ":" . __LINE__, "Text not found for: '" . $key . "', sql: '$sql'" );
						}

						break;

					case 'm':
					case 'menu':
						$name        = $innertext;
						$replacement = $this->l->show_menu( $name );

						break;

					case 'i':
					case 'info':
						$info_row                = array();
						$class_date_time         = new class_date_time();
						$info_row['datetime']    = $class_date_time->get_timedate();
						$info_row['time']        = $class_date_time->get_timedate( "time_only" );
						$info_row['date']        = $class_date_time->get_timedate( "date_only" );
						$info_row['year']        = $class_date_time->get_timedate( "year_only" );
						$info_row['ip']          = htmlentities( $_SERVER['REMOTE_ADDR'], ENT_QUOTES );
						$info_row['ipsrv']       = htmlentities( $_SERVER['SERVER_ADDR'], ENT_QUOTES );
						$info_row['admindir']    = ADMINDIR;
						$info_row['domain']      = $this->loc->server_name;
						$info_row['http_domain'] = $this->loc->httpauto_root;
						$info_row['admin_mail']  = ADMIN_MAIL;
						$info_row['http_web']    = $this->loc->httpauto_web_root;

						$pushval = $innertext;

						foreach ( $info_row as $fkey => $value ) {
							if ( $fkey == $pushval ) {
								$replacement = $value;
								break;
							}
						}

						break;
					default:
						$this->log->error( "parse", __FILE__ . ":" . __LINE__, "Tag '$tag' not found." );

						break;
				}
				$string = str_replace( $match, $replacement, $string );
			}

		}

		if ( $found ) {

			$string = $this->parse( $string );
		}


		return $string;
	}

}