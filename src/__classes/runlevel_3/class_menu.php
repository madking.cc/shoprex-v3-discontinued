<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

define( "MENU_TYPE_NAV", 1 );
define( "MENU_TYPE_NAV_SUB", 10 );
define( "MENU_TYPE_NAVBAR", 2 );
define( "MENU_TYPE_NAVBAR_SIDE", 20 );
define( "MENU_TYPE_NAVBAR_SUB", 25 );
define( "MENU_TYPE_DROPDOWN", 3 );
define( "MENU_TYPE_ULLI", 5 );

define( "MENU_DB_OPTIONS_NAME", "name" );
define( "MENU_DB_OPTIONS_TYPE", "id_type" );
define( "MENU_DB_OPTIONS_NAV_TYPE", "id_option0" );
define( "MENU_DB_OPTIONS_NAV_DIRECTION_TYPE", "id_option2" );
define( "MENU_DB_OPTIONS_NAV_JUSTIFIED", "id_option1" );
define( "MENU_DB_OPTIONS_NAVBAR_TYPE", "id_option0" );
define( "MENU_DB_OPTIONS_NAVBAR_1", "id_option1" );
define( "MENU_DB_OPTIONS_NAVBAR_2", "id_option2" );
define( "MENU_DB_OPTIONS_BRAND", "content" );
define( "MENU_DB_OPTIONS_DROPDOWN_RIGHT", "id_option0" );
define( "MENU_DB_OPTIONS_CSS_ID", "css_id" );
define( "MENU_DB_OPTIONS_CSS_CLASS", "css_class" );
define( "MENU_DB_OPTIONS_TEXT", "content" );
define( "MENU_DB_OPTIONS_WITH_MENU", "with_menu" );

define( "MENU_DB_ID", "id" );
define( "MENU_DB_SUB_ID", "to_id" );
define( "MENU_DB_LINK_TO_PAGE", "content" );
define( "MENU_DB_LINK", "content" );
define( "MENU_DB_LINK_ULLI", "name" );
define( "MENU_DB_CSS_STATUS", "content_option0" );
define( "MENU_DB_OPEN_NEW_WINDOW", "content_option1" );
define( "MENU_DB_TEXT_COLOR_UNIFORM", "content_option2" );
define( "MENU_DB_BUTTON_TYPE", "content_option3" );
define( "MENU_DB_BUTTON_SIZE", "content_option4" );
define( "MENU_DB_CSS_STYLE", "id_option0" );
define( "MENU_DB_SIDE", "id_option0" );
define( "MENU_DB_DIRECTION", "id_option2" );
define( "MENU_DB_INVERTED", "id_option2" );
define( "MENU_DB_JUSTIFIED", "id_option1" );
define( "MENU_DB_MOBILE_TOGGLE", "id_option1" );
define( "MENU_DB_CSS_ID_NAME", "css_id" );
define( "MENU_DB_CSS_CLASS_STRING", "css_class" );
define( "MENU_DB_TYPE_OF_MENU_POINT", "content_type" );
define( "MENU_DB_ALIGN_RIGHT", "id_option0" );
define( "MENU_DB_LINK_TEXT", "name" );
define( "MENU_DB_BUTTON_TEXT", "content" );
define( "MENU_DB_DROPDOWN_TEXT", "content" );
define( "MENU_DB_FREE_TEXT", "content" );
define( "MENU_DB_FORM_TEXT", "content" );
define( "MENU_DB_HEADER_TEXT", "content" );
define( "MENU_DIVIDER_TEXT", "----------" );

define( "MENU_NAV_MENU_POINT_IS_LINK", "0" );
define( "MENU_NAV_MENU_POINT_IS_DROPDOWN", "1" );
define( "MENU_NAV_MENU_POINT_IS_FREE_TEXT", "2" );
define( "MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE", "3" );

define( "MENU_NAV_SUB_MENU_POINT_IS_DIVIDER", "4" );
define( "MENU_NAV_SUB_MENU_POINT_IS_LINK", MENU_NAV_MENU_POINT_IS_LINK );
define( "MENU_NAV_SUB_MENU_POINT_IS_FREE_TEXT", MENU_NAV_MENU_POINT_IS_FREE_TEXT );
define( "MENU_NAV_SUB_MENU_POINT_IS_LINK_TO_PAGE", MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE );

define( "NAVBAR_TYPE_DEFAULT", 0 );
define( "NAVBAR_TYPE_FIXED_TOP", 1 );
define( "NAVBAR_TYPE_FIXED_BOTTOM", 2 );
define( "NAVBAR_TYPE_STATIC_TOP", 3 );
define( "NAVBAR_SIDE_MENU_POINT_IS_LINK", "0" );
define( "NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN", "1" );
define( "NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT", "2" );
define( "NAVBAR_SIDE_MENU_POINT_IS_BUTTON", "3" );
define( "NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS", "4" );
define( "NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE", "5" );

define( "NAVBAR_SUB_POINT_IS_LINK", NAVBAR_SIDE_MENU_POINT_IS_LINK );
define( "NAVBAR_SUB_POINT_IS_FREE_TEXT", NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT );
define( "NAVBAR_SUB_POINT_IS_LINK_TO_PAGE", NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE );
define( "NAVBAR_SUB_POINT_IS_DIVIDER", "6" );

define( "DROPDOWN_MENU_POINT_IS_LINK", "0" );
define( "DROPDOWN_MENU_POINT_IS_FREE_TEXT", "1" );
define( "DROPDOWN_MENU_POINT_IS_DIVIDER", "2" );
define( "DROPDOWN_MENU_POINT_IS_HEADER", "3" );
define( "DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE", "4" );

define( "ULLI_MENU_POINT_IS_LINK", "0" );
define( "ULLI_MENU_POINT_IS_FREE_TEXT", "1" );
define( "ULLI_MENU_POINT_IS_DIVIDER", "2" );
define( "ULLI_MENU_POINT_IS_DROPDOWN", "3" );
define( "ULLI_MENU_POINT_IS_LINK_TO_PAGE", "4" );

define( "MENU_STATUS_CLASS_ACTIVE", " active" );
define( "MENU_STATUS_CLASS_INACTIVE", " inactive" );

class class_menu extends class_root {
	protected $p;
	protected $loc;
	protected $db;
	protected $log;
	protected $l;
	protected $lang;

	public $content;
	protected $navbar_counter = 0;

	public function __construct( $name = false, $preview = false ) {


		$this->l    = new class_layout();
		$this->p    = new class_page();
		$this->loc  = class_location::getInstance();
		$this->db   = class_database::getInstance();
		$this->log  = class_logging::getInstance();
		$this->lang = class_language::getInstance();


		if ( ! empty( $name ) ) {
			$this->content = $this->show_menu( $name, $preview );
		}
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public function show_menu( $name, $preview = false ) {
		if ( empty( $name ) ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Menu '$name' not found" );

			return "<p>" . $this->txt( 'NO_MENUNAME_GIVEN' ) . "</p>";
		}

		if ( is_numeric( $name ) ) {
			// By ID:
			$name = intval( $name );
			if ( $name <= 0 ) {
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Menu ID Format not correct: '$name'" );

				return "<p>" . $this->txt( 'WRONG_ID_FOR_MENU_COLON' ) . " '" . $name . "'</p>";
			}

			$sql    = "SELECT * FROM `" . TP . "menus` WHERE `id`='" . $name . "'  AND `deleted` = '0' AND `active` = '1' ORDER BY `pos`";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			// By Name:
			$sql    = "SELECT * FROM `" . TP . "menus` WHERE `name` LIKE BINARY '" . $name . "'  AND `deleted` = '0' AND `active` = '1' ORDER BY `pos`";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		}

		if ( $result->num_rows == 0 ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Menu '$name' not found" );

			return "<p>" . $this->txt( 'MENU_NOT_FOUND_PART01' ) . " '$name' " . $this->txt( 'MENU_NOT_FOUND_PART02' ) . "</p>";
		}

		if ( $result->num_rows > 1 ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Multiple Menu Results for '$name': '" . $result->num_rows . "'" );

			return "<p>" . $this->txt( 'NO_MAINMENU_COLON' ) . " '$name'</p>";
		}

		$row = $result->fetch_assoc();

		switch ( $row['id_type'] ) {
			case MENU_TYPE_NAV:
				return $this->generate_nav( $row );
				break;
			case MENU_TYPE_NAVBAR:
				return $this->generate_navbar( $row, $preview );
				break;
			case MENU_TYPE_DROPDOWN:
				return $this->generate_dropdown( $row );
				break;
			case MENU_TYPE_ULLI:
				return $this->generate_ulli( $row );
				break;
			default:
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong Menutype: '" . $row['id_type'] . "'" );

				return "<p>" . $this->txt( 'MENUTYPE_NOT_FOUND_PART01' ) . " " . $row['id_type'] . " " . $this->txt( 'MENUTYPE_NOT_FOUND_PART02' ) . "</p>";
				break;
		}
	}

	protected function generate_nav( $main_row ) {
		$menu_type = MENU_TYPE_NAV;
		$content   = "";
		$sql       = "SELECT * FROM `" . TP . "menus` WHERE `to_id`='" . $main_row['id'] . "' AND `deleted` = '0' AND `active` = '1' ORDER BY `pos`";
		$result    = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			$ul_css_disabled = $this->get_css_disabled( $menu_type, $main_row, "ul" );

			switch ( $main_row[ MENU_DB_CSS_STYLE ] ) {
				case "0":
					$ul_css_type = " nav-tabs";
					break;
				case "1":
					$ul_css_type = " nav-pills";
					break;
				default:
					$ul_css_type = "";
					break;
			}
			switch ( $main_row[ MENU_DB_DIRECTION ] ) {
				case "1":
					$ul_css_direction_type = " nav-stacked";
					break;
				default:
					$ul_css_direction_type = "";
					break;
			}

			$ul_css_justified = "";
			if ( ! ( $main_row[ MENU_DB_CSS_STYLE ] == 1 && $main_row[ MENU_DB_DIRECTION ] == 1 ) && $main_row[ MENU_DB_JUSTIFIED ] == 1 ) {
				$ul_css_justified = " nav-justified";
			}

			$ul_id = $this->get_id_name( $main_row );

			$ul_class = $ul_css_type . $ul_css_direction_type . $ul_css_justified . $ul_css_disabled;

			$content .= "<ul $ul_id class='nav$ul_class " . $main_row[ MENU_DB_CSS_CLASS_STRING ] . "'>";

			while ( $row = $result->fetch_assoc() ) {
				$li_css_disabled = $this->get_css_disabled( $menu_type, $row, "li" );
				$li_css_dropdown = $this->get_css_dropdown( $menu_type, $row );

				$content_lang_number = $this->get_content_lang_number( $row );
				$name_lang_number    = $this->get_name_lang_number( $row );

				if ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == MENU_NAV_MENU_POINT_IS_DROPDOWN ) {
					$content_status_class = $this->get_dropdownmenu_status_class( $row['id'], $content_lang_number, 1 );
				} else {
					$content_status_class = $this->get_menu_status_class( $row, $content_lang_number );
				}

				$li_id    = $this->get_id_name( $row );
				$li_class = $li_css_disabled . $li_css_dropdown;
				$content .= "<li $li_id class='" . $row[ MENU_DB_CSS_CLASS_STRING ] . "$li_class'>";

				switch ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] ) {
					// Link
					case MENU_NAV_MENU_POINT_IS_LINK:
						$content .= $this->get_link( $row, $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ] );
						break;
					// Dropdown
					case MENU_NAV_MENU_POINT_IS_DROPDOWN:
						$content .= $this->get_dropdown( $menu_type, $row['id'], $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class );
						break;
					// Free Text
					case MENU_NAV_MENU_POINT_IS_FREE_TEXT:
						$content .= $this->get_free_text( $row[ 'content' . $content_lang_number ] );
						break;
					// Link to Page
					case MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE:
						$content .= $this->get_link_to_page( $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ] );
						break;
					default:
						$content .= $this->get_not_found( $row[ MENU_DB_TYPE_OF_MENU_POINT ] );
						break;
				}
				$content .= "</li>\n";
			}
			$content .= "</ul>\n";
		}

		return $content;
	}

	protected function get_css_disabled( $row_type, $row, $element_type = "" ) {
		switch ( $row_type ) {
			case MENU_TYPE_NAV:

				switch ( $element_type ) {
					case "ul":
						if ( $row[ MENU_DB_CSS_STATUS ] ) {
							return " disabled";
						} else {
							return "";
						}
						break;
					case "li":
						if ( $row[ MENU_DB_CSS_STATUS ] && (
								$row[ MENU_DB_TYPE_OF_MENU_POINT ] == MENU_NAV_MENU_POINT_IS_LINK ||
								$row[ MENU_DB_TYPE_OF_MENU_POINT ] == MENU_NAV_MENU_POINT_IS_DROPDOWN ||
								$row[ MENU_DB_TYPE_OF_MENU_POINT ] == MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE )
						) {
							return " disabled";
						} else {
							return "";
						}
						break;
					default:
						$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong Elemnttype: '" . $element_type . "'" );

						return "";
						break;
				}
				break;

			case MENU_TYPE_NAV_SUB:
				if ( $row[ MENU_DB_CSS_STATUS ] && ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == 0 || $row[ MENU_DB_TYPE_OF_MENU_POINT ] == 3 ) ) {
					return " disabled";
				} else {
					return "";
				}
				break;

			case MENU_TYPE_NAVBAR:
				if ( $row[ MENU_DB_CSS_STATUS ] ) {
					return " disabled";
				} else {
					return "";
				}
				break;

			case MENU_TYPE_NAVBAR_SIDE:
				if ( ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS ) && (
						$row[ MENU_DB_CSS_STATUS ] && (
							$row[ MENU_DB_TYPE_OF_MENU_POINT ] == NAVBAR_SIDE_MENU_POINT_IS_LINK ||
							$row[ MENU_DB_TYPE_OF_MENU_POINT ] == NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN ||
							$row[ MENU_DB_TYPE_OF_MENU_POINT ] == NAVBAR_SIDE_MENU_POINT_IS_BUTTON ||
							$row[ MENU_DB_TYPE_OF_MENU_POINT ] == NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE ) )
				) {
					return " disabled";
				} else {
					return "";
				}
				break;

			case MENU_TYPE_NAVBAR_SUB:
				if ( $row[ MENU_DB_CSS_STATUS ] && ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == NAVBAR_SUB_POINT_IS_LINK || $row[ MENU_DB_TYPE_OF_MENU_POINT ] == NAVBAR_SUB_POINT_IS_LINK_TO_PAGE ) ) {
					return " disabled";
				} else {
					return "";
				}
				break;

			case MENU_TYPE_DROPDOWN:
				switch ( $element_type ) {
					case "ul":
						if ( $row[ MENU_DB_CSS_STATUS ] ) {
							return " disabled";
						} else {
							return "";
						}
						break;
					case "li":
						if ( $row[ MENU_DB_CSS_STATUS ] && ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == DROPDOWN_MENU_POINT_IS_LINK || $row[ MENU_DB_TYPE_OF_MENU_POINT ] == DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE ) ) {
							return " disabled";
						} else {
							return "";
						}
						break;
				}

			case MENU_TYPE_ULLI:
				switch ( $element_type ) {
					case "ul":
						if ( $row[ MENU_DB_CSS_STATUS ] ) {
							return " disabled";
						} else {
							return "";
						}
						break;
					case "li":
						if ( $row[ MENU_DB_CSS_STATUS ] && ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == ULLI_MENU_POINT_IS_LINK || $row[ MENU_DB_TYPE_OF_MENU_POINT ] == ULLI_MENU_POINT_IS_DROPDOWN ||
						                                     $row[ MENU_DB_TYPE_OF_MENU_POINT ] == ULLI_MENU_POINT_IS_LINK_TO_PAGE )
						) {
							return " disabled";
						} else {
							return "";
						}
						break;
				}

			default:
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong Menutype: '" . $row_type . "'" );

				return "";
				break;
		}
	}

	protected function get_id_name( $row ) {
		if ( ! empty( $row[ MENU_DB_CSS_ID_NAME ] ) ) {
			return "id='" . $row[ MENU_DB_CSS_ID_NAME ] . "'";
		} else {
			return "";
		}
	}

	protected function get_css_dropdown( $row_type, $row, $element_type = "" ) {
		switch ( $row_type ) {
			case MENU_TYPE_NAV:
				if ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == MENU_NAV_MENU_POINT_IS_DROPDOWN ) {
					return " dropdown";
				} else {
					return "";
				}
				break;

			case MENU_TYPE_NAVBAR_SIDE:
				if ( ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS ) && ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN ) ) {
					return " dropdown";
				} else {
					return "";
				}
				break;

			default:
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong Menutype: '" . $row_type . "'" );

				return "";
				break;
		}
	}

	protected function get_content_lang_number( $row ) {
		if ( is_null( $row[ 'content' . $this->lang->lnbr ] ) ) {
			return $this->lang->lnbrm;
		} else {
			return $this->lang->lnbr;
		}
	}

	protected function get_name_lang_number( $row ) {
		if ( is_null( $row[ 'name' . $this->lang->lnbr ] ) ) {
			return $this->lang->lnbrm;
		} else {
			return $this->lang->lnbr;
		}
	}

	//////////////////////////

	protected function get_dropdownmenu_status_class( $id, $content_lang_number, $dropdown_number, $iftrue = MENU_STATUS_CLASS_ACTIVE, $iffalse = MENU_STATUS_CLASS_INACTIVE ) {
		if ( is_null( $id ) || empty( $id ) || $id < 1 ) {
			return $iffalse;
		} else {
			$sql    = "SELECT * FROM `" . TP . "menus` WHERE `to_id`='" . $id . "' AND `deleted` = '0' AND `active` = '1' ORDER BY `pos`";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows > 0 ) {
				while ( $row = $result->fetch_assoc() ) {

					$result_status = $this->get_menu_status_class( $row, $content_lang_number, true, false );
					if ( $result_status ) {
						return $iftrue;
					} elseif ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == $dropdown_number ) {
						$result_status = $this->get_dropdownmenu_status_class( $row['id'], $content_lang_number, $dropdown_number, true, false );

						if ( $result_status ) {
							return $iftrue;
						}
					}
				}
			} else {
				return $iffalse;
			}

			return $iffalse;
		}
	}

	protected function get_menu_status_class( $row, $content_lang_number, $iftrue = MENU_STATUS_CLASS_ACTIVE, $iffalse = MENU_STATUS_CLASS_INACTIVE ) {
		if ( is_null( $row ) ) {
			return $iffalse;
		} else {
			$match = false;

			if ( is_null( $row[ 'content' . $content_lang_number ] ) ) {
				if ( is_null( $row[ 'content' . $this->lang->lnbrm ] ) ) {
					return $iffalse;
				} else {
					$search = $row[ 'content' . $this->lang->lnbrm ];
				}
			} else {
				if ( $row['id_type'] == MENU_TYPE_DROPDOWN ) {
					if ( ! empty( $row[ 'name' . $content_lang_number ] ) ) {
						$search = $row[ 'name' . $content_lang_number ];
					} elseif ( ! empty( $row[ 'name' . $this->lang->lnbrm ] ) ) {
						$search = $row[ 'name' . $this->lang->lnbrm ];
					}
				}

				if ( ! isset( $search ) ) {
					$search = $row[ 'content' . $content_lang_number ];
				}
			}

			if ( is_numeric( $search ) ) {
				$page_path = $this->get_path_from_page( $search, __FILE__ . ":" . __LINE__ );
				$match     = $this->loc->compare_path_with_current( $page_path );
				if ( $match ) {
					return $iftrue;
				}
			} elseif ( ! is_null( $search ) ) {
				$match = $this->loc->compare_path_with_current( $search );

				if ( $match ) {
					return $iftrue;
				}
			}

			if ( ! $match ) {
				$match = $this->has_with_menu( $row['id'] );
				if ( $match ) {
					return $iftrue;
				} else {
					return $iffalse;
				}
			}
		}
	}

	public function get_path_from_page( $id, $loc = "unknown" ) {
		if ( ! is_numeric( $id ) || is_null( $id ) || empty( $id ) || $id < 1 ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Page id is not correct: '$id', loc: '$loc'" );

			return false;
		} else {
			$sql    = "SELECT * FROM `" . TP . "pages` WHERE `deleted` = '0' AND `active` = '1' AND `id` = '$id' LIMIT 1";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();

				return $row[ 'path' . $this->lang->lnbr ];
			} else {
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "Page for id: '$id' not found, num_rows: '" . $result->num_rows . "', sql: '$sql', source: '$loc" );

				return false;
			}
		}
	}

	protected function has_with_menu( $main_id ) {
		// If a menu is connected to a another menu link
		$menu_id_from_path = $this->get_menu_id_from_path();

		if ( ! is_array( $menu_id_from_path ) || sizeof( $menu_id_from_path ) == 0 ) {
			return false;
		}

		$search    = "";
		$tmp_id_ar = array();

		foreach ( $menu_id_from_path as $value_id ) {
			$tmp_id = $this->get_main_menu_id( $value_id );
			if ( $tmp_id > 0 ) {
				if ( in_array( $tmp_id, $tmp_id_ar ) ) {
					continue;
				}
				$tmp_id_ar[] = $tmp_id;
				$search .= "`id` = '" . $tmp_id . "' OR ";
			}
		}

		if ( empty( $search ) ) {
			return false;
		}

		$search = substr( $search, 0, - 4 );
		$search = "(" . $search . ")";

		$sql    = "SELECT * FROM `" . TP . "menus` WHERE `with_menu`='" . $main_id . "' AND $search AND `deleted` = '0' AND `active` = '1' LIMIT 1";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows == 1 ) {
			return true;
		} else {
			return false;
		}
	}

	public function get_menu_id_from_path() {
		$path   = array();
		$path[] = $this->loc->current;
		if ( strpos( $this->loc->current, "/index.php" ) !== false ) {
			$path[] = str_replace( "/index.php", "", $this->loc->current );
		} elseif ( strpos( $this->loc->current, "index.php" ) !== false ) {
			$path[] = str_replace( "index.php", "", $this->loc->current );
		}

		if ( sizeof( $path ) == 0 ) {
			return array();
		}

		$sql    = "SELECT * FROM `" . TP . "menus` WHERE `content` IS NOT NULL AND `active` = '1' AND `deleted` = '0'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		// TODO: Untermenüpunkte von einem deaktivierten Menü sind trotzdem aktiv!

		$menu_pages_path = array();
		while ( $row = $result->fetch_assoc() ) {
			if ( ! empty( $row[ MENU_DB_SUB_ID ] ) ) {
				$main_menu_id_present = $this->get_main_menu_id( $row[ MENU_DB_SUB_ID ] );
				if ( $main_menu_id_present === false ) {
					continue;
				}
			}

			if ( is_numeric( $row[ MENU_DB_LINK_TO_PAGE ] ) ) {
				$row[ MENU_DB_LINK_TO_PAGE ] = $this->get_path_from_page( $row[ MENU_DB_LINK_TO_PAGE ], __FILE__ . ":" . __LINE__ );
			}
			if ( $row[ MENU_DB_LINK_TO_PAGE ] !== false ) {
				$menu_pages_path[ $row[ MENU_DB_ID ] ] = $row[ MENU_DB_LINK_TO_PAGE ];
			}
		}

		$menu_matches = array();

		foreach ( $menu_pages_path as $menu_id => $menu_path ) {
			if ( in_array( $menu_path, $path ) ) {
				$menu_matches[] = $menu_id;
			}
		}

		return $menu_matches;
	}

	public function get_main_menu_id( $id ) {
		// Recursive:
		if ( $id == 0 ) {
			return false;
		}

		$sql    = "SELECT * FROM `" . TP . "menus` WHERE `id`='" . $id . "'  AND `deleted` = '0' AND `active` = '1'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows != 1 ) {
			return false;
		} else {
			$row = $result->fetch_assoc();
		}

		if ( empty( $row[ MENU_DB_SUB_ID ] ) ) {
			return $row[ MENU_DB_ID ];
		} else {
			return $this->get_main_menu_id( $row[ MENU_DB_SUB_ID ] );
		}
	}

	protected function get_link( $row, $content_txt, $name = "", $content_status_class = "", $do_target_blank = false, $free_text = "" ) {
		$content = "";
		if ( $do_target_blank ) {
			$target_blank = " target='_blank'";
		} else {
			$target_blank = "";
		}
		if ( empty( $name ) ) {
			$name = $content_txt;
		}

		$content .= $this->l->link( $name, $content_txt, "", $target_blank . " " . $free_text, "", "link" . $content_status_class );
		$this->add_menu_to_lang_path( $content_status_class, $row, "content" );

		return $content;
	}

	protected function add_menu_to_lang_path( $status_class, $row, $col_name ) {
		if ( strcasecmp( $status_class, MENU_STATUS_CLASS_ACTIVE ) === 0 ) {
			if ( $this->lang->count_of_languages > 1 ) {
				foreach ( $this->lang->languages as $lang_key => $lang_array ) {

					if ( is_null( $row[ $col_name . $lang_array['number'] ] ) ) {
						continue;
					}
					$this->lang->lang_path[ LANG_PATH_PRIORITY_MENU_POINT ][ $lang_key ] = $lang_array['subdir'] . $row[ $col_name . $lang_array['number'] ];
				}
			}
		}
	}

	protected function get_dropdown( $menu_type, $menu_sub_id, $content_txt, $name = "", $content_status_class = "" ) {
		$content = "";
		if ( empty( $name ) ) {
			$name = $content_txt;
		}

		$content .= $this->l->link( $name . " <span class='caret'></span>", "#", "", "data-toggle='dropdown'", "", "dropdown-toggle" . $content_status_class ) . "
                        <ul class='dropdown-menu'>";

		switch ( $menu_type ) {
			case MENU_TYPE_NAV:
				$content .= $this->get_nav_subs( $menu_sub_id );
				break;
			case MENU_TYPE_NAVBAR_SIDE:
				$content .= $this->get_navbar_subs( $menu_sub_id );
				break;
			default:
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong Menutype: '" . $menu_type . "'" );
				break;
		}
		$content .= "</ul>\n";

		return $content;
	}

	protected function get_nav_subs( $id ) {
		$menu_type = MENU_TYPE_NAV_SUB;

		$content = "";
		$sql     = "SELECT * FROM `" . TP . "menus` WHERE `to_id`='" . $id . "' AND `deleted` = '0' AND `active` = '1' ORDER BY `pos`";
		$result  = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			while ( $row = $result->fetch_assoc() ) {

				$css_disabled = $this->get_css_disabled( $menu_type, $row, "li" );
				$css_divider  = $this->get_css_divider( $menu_type, $row );

				$content_lang_number = $this->get_content_lang_number( $row );
				$name_lang_number    = $this->get_name_lang_number( $row );

				$content_status_class = $this->get_menu_status_class( $row, $content_lang_number );

				$li_id    = $this->get_id_name( $row );
				$li_class = $css_disabled . $css_divider;

				$content .= "<li $li_id class='" . $row['css_class'] . "$li_class'>";

				switch ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] ) {

					// Link
					case MENU_NAV_SUB_MENU_POINT_IS_LINK:
						$content .= $this->get_link( $row, $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ] );
						break;
					// Free Text
					case MENU_NAV_SUB_MENU_POINT_IS_FREE_TEXT:
						$content .= $this->get_free_text( $row[ 'content' . $content_lang_number ] );
						break;
					// Divider
					case MENU_NAV_SUB_MENU_POINT_IS_DIVIDER:
						// Nothing
						break;
					// Link to Page
					case MENU_NAV_SUB_MENU_POINT_IS_LINK_TO_PAGE:
						$content .= $this->get_link_to_page( $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ] );
						break;
					default:
						$content .= $this->get_not_found( $row[ MENU_DB_TYPE_OF_MENU_POINT ] );
						break;
				}
				$content .= "</li>\n";
			}
		}

		return $content;
	}

	protected function get_css_divider( $menu_type, $row ) {
		switch ( $menu_type ) {
			case MENU_TYPE_NAV_SUB:
				if ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == MENU_NAV_SUB_MENU_POINT_IS_DIVIDER ) {
					return " divider";
				} else {
					return "";
				}
				break;
			case MENU_TYPE_NAVBAR_SUB:
				if ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == NAVBAR_SUB_POINT_IS_DIVIDER ) {
					return " divider";
				} else {
					return "";
				}
				break;
			case MENU_TYPE_ULLI:
				if ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == ULLI_MENU_POINT_IS_DIVIDER ) {
					return " divider";
				} else {
					return "";
				}
				break;
			default:
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong Menutype: '" . $menu_type . "'" );

				return "";
				break;
		}
	}

	protected function get_free_text( $string ) {
		return $string;
	}

	protected function get_link_to_page( $page_id, $name = "", $content_status_class = "", $do_target_blank = false, $free_text = "" ) {
		$content = "";
		if ( is_numeric( $page_id ) ) {
			if ( $do_target_blank ) {
				$target_blank = " target='_blank'";
			} else {
				$target_blank = "";
			}

			$sql          = "SELECT * FROM `" . TP . "pages` WHERE `deleted`='0' AND `active` = '1' AND `id`='" . $page_id . "'";
			$result_pages = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result_pages->num_rows == 1 ) {
				$row_page = $result_pages->fetch_assoc();
				if ( empty( $name ) ) {
					$name = $row_page['name'];
				}

				$content .= $this->l->link( $name, $row_page[ 'path' . $this->lang->lnbr ], "", $target_blank . " " . $free_text, "", "link" . $content_status_class );
				$this->add_menu_to_lang_path( $content_status_class, $row_page, "path" );
			} else {
				$content .= $this->txt( 'INVALID' );
			}
		} else {
			$content .= $this->txt( 'INVALID' );
		}

		return $content;
	}

	protected function get_not_found( $type ) {
		return $this->txt( 'TYPE_NOT_FOUND_PART01' ) . " '" . $type . "' " . $this->txt( 'TYPE_NOT_FOUND_PART02' );
	}

	protected function get_navbar_subs( $id ) {
		$content   = "";
		$menu_type = MENU_TYPE_NAVBAR_SUB;
		$sql       = "SELECT * FROM `" . TP . "menus` WHERE `to_id`='" . $id . "' AND `deleted` = '0' AND `active` = '1' ORDER BY `pos`";
		$result    = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				$css_disabled = $this->get_css_disabled( $menu_type, $row );
				$css_divider  = $this->get_css_divider( $menu_type, $row );

				$li_id    = $this->get_id_name( $row );
				$li_class = $css_disabled . $css_divider;
				$content .= "<li $li_id class='" . $row['css_class'] . "$li_class'>";

				$content_lang_number = $this->get_content_lang_number( $row );
				$name_lang_number    = $this->get_name_lang_number( $row );

				$content_status_class = $this->get_menu_status_class( $row, $content_lang_number );

				switch ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] ) {
					// Link
					case NAVBAR_SUB_POINT_IS_LINK:
						$content .= $this->get_link( $row, $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ] );
						break;
					// Free Text
					case NAVBAR_SUB_POINT_IS_FREE_TEXT:
						$content .= "<p class='navbar-text'>" . $row[ 'content' . $content_lang_number ] . "</p>";
						break;
					// Divider
					case NAVBAR_SUB_POINT_IS_DIVIDER:
						break;
					// Link to Page
					case NAVBAR_SUB_POINT_IS_LINK_TO_PAGE:
						$content .= $this->get_link_to_page( $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ] );
						break;
					default:
						$content .= $this->get_not_found( $row[ MENU_DB_TYPE_OF_MENU_POINT ] );
						break;
				}
				$content .= "</li>\n";
			}
		}

		return $content;
	}

	protected function generate_navbar( $main_row, $preview = false ) {
		$menu_type = MENU_TYPE_NAVBAR;
		$content   = "";
		$sql       = "SELECT * FROM `" . TP . "menus` WHERE `to_id`='" . $main_row['id'] . "' AND `deleted` = '0' AND `active` = '1' ORDER BY `pos`";
		$result    = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {

			$css_disabled = $this->get_css_disabled( $menu_type, $main_row );

			$css_type = " navbar-default";
			if ( ! $preview ) {
				switch ( $main_row[ MENU_DB_CSS_STYLE ] ) {
					case NAVBAR_TYPE_DEFAULT:
						break;
					case NAVBAR_TYPE_FIXED_TOP:
						$css_type = "  navbar-fixed-top";
						$GLOBALS["custom_css"] .= "<style>
    body { padding-top: 70px; }
</style>\n";

						break;
					case NAVBAR_TYPE_FIXED_BOTTOM:
						$css_type = "  navbar-fixed-bottom";
						$GLOBALS["custom_css"] .= "<style>
    body { padding-bottom: 70px; }
</style>\n";
						break;
					case NAVBAR_TYPE_STATIC_TOP:
						$css_type = "  navbar-static-top";
						break;
					default:
						$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong CSS Type: '" . $main_row[ MENU_DB_CSS_STYLE ] . "'" );
						break;
				}
			}

			if ( $main_row[ MENU_DB_MOBILE_TOGGLE ] ) {
				$mobile_toggle = "
    <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#navbar-collapse-" . $main_row['id'] . "'>
        <span class='sr-only'>Toggle navigation</span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
    </button>
    ";
			} else {
				$mobile_toggle = "";
			}

			$content_lang_number = $this->get_content_lang_number( $main_row );

			if ( $main_row[ 'content' . $content_lang_number ] ) {
				$brand = "
    <a class='navbar-brand' href='#'>Brand</a>
";
			} else {
				$brand = "";
			}

			if ( ! empty( $mobile_toggle ) || ! empty( $brand ) ) {
				$navbar_header = "
    <div class='navbar-header'>
    $mobile_toggle
    $brand
    </div>
    ";
			} else {
				$navbar_header = "";
			}

			if ( $main_row[ MENU_DB_INVERTED ] ) {
				$inverted = " navbar-inverse";
			} else {
				$inverted = "";
			}

			$nav_id    = $this->get_id_name( $main_row );
			$nav_class = $css_disabled . $css_type . $inverted;

			$content .= "<nav $nav_id class='navbar $nav_class " . $main_row['css_class'] . "'>
                " . $navbar_header . "
<div class='collapse navbar-collapse' id='navbar-collapse-" . $main_row['id'] . "'>\n";

			$content .= $this->get_navbar_side( $main_row['id'], " AND `" . MENU_DB_SIDE . "` = '0' ", "navbar-left" );
			$content .= $this->get_navbar_side( $main_row['id'], " AND `" . MENU_DB_SIDE . "` = '1' ", "navbar-right" );

			$content .= "</div></nav>\n";
		}
		$this->navbar_counter ++;

		return $content;
	}

	protected function get_navbar_side( $id, $where, $css_direction, $order = "" ) {
		$content   = "";
		$menu_type = MENU_TYPE_NAVBAR_SIDE;
		$sql       = "SELECT * FROM `" . TP . "menus` WHERE `to_id`='" . $id . "' $where AND `deleted` = '0' AND `active` = '1' ORDER BY `pos` $order";
		$result    = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		$ul_open = false;
		while ( $row = $result->fetch_assoc() ) {
			if ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS &&
			     $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_BUTTON &&
			     $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT
			) {
				if ( ! $ul_open ) {
					$content .= "<ul class='nav navbar-nav $css_direction'>\n";
				}
				$ul_open = true;
			}

			if ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS &&
			     $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_BUTTON &&
			     $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT
			) {
				$css_disabled = $this->get_css_disabled( $menu_type, $row );
				$css_dropdown = $this->get_css_dropdown( $menu_type, $row );

				$li_id    = $this->get_id_name( $row );
				$li_class = $css_disabled . $css_dropdown;
				$content .= "<li $li_id class='" . $row['css_class'] . "$li_class'>";
			}

			$content_lang_number = $this->get_content_lang_number( $row );
			$name_lang_number    = $this->get_name_lang_number( $row );

			if ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] == NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN ) {
				$content_status_class = $this->get_dropdownmenu_status_class( $row['id'], $content_lang_number, 1 );
			} else {
				$content_status_class = $this->get_menu_status_class( $row, $content_lang_number );
			}

			$target_blank = "";
			switch ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] ) {

				// Link
				case NAVBAR_SIDE_MENU_POINT_IS_LINK:
					$content .= $this->get_link( $row, $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ] );
					break;
				// Dropdown
				case NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN:
					$content .= $this->get_dropdown( $menu_type, $row['id'], $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class );
					break;
				// Free Text
				case NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT:
					$content .= $this->if_ul_open( $ul_open );
					$content .= "<p class='navbar-text $css_direction'>" . $row[ 'content' . $content_lang_number ] . "</p>";
					break;
				// Button
				case NAVBAR_SIDE_MENU_POINT_IS_BUTTON:
					$content .= $this->if_ul_open( $ul_open );
					$button_class = $css_direction . $content_status_class;
					$content .= "<button type='button' class='$button_class btn " . $row[ MENU_DB_BUTTON_TYPE ] . " " . $row[ MENU_DB_BUTTON_SIZE ] . " navbar-btn'>" .
					            $row[ 'content' . $content_lang_number ] . "</button>";
					break;
				// Free Text with form
				case NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS:
					$content .= $this->if_ul_open( $ul_open );
					$content .= $row[ 'content' . $content_lang_number ];
					break;
				// Link to Page
				case NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE:
					$content .= $this->get_link_to_page( $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ] );
					break;
				default:
					$content .= $this->get_not_found( $row[ MENU_DB_TYPE_OF_MENU_POINT ] );
					break;
			}
			if ( $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS &&
			     $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_BUTTON &&
			     $row[ MENU_DB_TYPE_OF_MENU_POINT ] != NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT
			) {
				$content .= "</li>\n";
			}
		}
		if ( $ul_open ) {
			$content .= "</ul>\n";
			//$ul_open = false;
		}

		return $content;
	}

	protected function if_ul_open( &$ul_status ) {
		if ( $ul_status ) {
			$ul_status = false;

			return "</ul>\n";
		} else {
			return "";
		}
	}

	protected function generate_dropdown( $main_row ) {
		$content   = "";
		$menu_type = MENU_TYPE_DROPDOWN;

		$sql    = "SELECT * FROM `" . TP . "menus` WHERE `to_id`='" . $main_row['id'] . "' AND `deleted` = '0' AND `active` = '1' ORDER BY `pos`";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			$css_disabled = $this->get_css_disabled( $menu_type, $main_row, "ul" );

			if ( $main_row[ MENU_DB_ALIGN_RIGHT ] ) {
				$align_right = " pull-right";
			} else {
				$align_right = "";
			}

			$ul_id    = $this->get_id_name( $main_row );
			$ul_class = $css_disabled . $align_right;

			$content .= "<div class='dropdown' style=''>
<button class='btn dropdown-toggle' type='button' data-toggle='dropdown'>
" . $main_row['content'] . "
<span class='caret'></span>
</button>
<ul $ul_id class='dropdown-menu " . $main_row['css_class'] . "$ul_class' role='menu'>";

			while ( $row = $result->fetch_assoc() ) {
				$css_disabled = $this->get_css_disabled( $menu_type, $row, "li" );
				$li_id        = $this->get_id_name( $row );

				$content_lang_number = $this->get_content_lang_number( $row );
				$name_lang_number    = $this->get_name_lang_number( $row );

				$content_status_class = $this->get_menu_status_class( $row, $content_lang_number );

				$link_entry_default    = "role='menuitem' tabindex='-1'";
				$li_additional_default = "role='presentation'";
				switch ( $row['content_type'] ) {

					// Link
					case DROPDOWN_MENU_POINT_IS_LINK:
						$content .= "<li $li_additional_default class='" . $row['css_class'] . "$css_disabled' $li_id>";
						$content .= $this->get_link( $row, $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ], $link_entry_default );
						$content .= "</li>\n";
						break;
					// Free Text
					case DROPDOWN_MENU_POINT_IS_FREE_TEXT:
						$content .= "<li $li_additional_default class='" . $row['css_class'] . "' $li_id>" . $row[ 'content' . $content_lang_number ] . "</li>\n";
						break;
					// Divider
					case DROPDOWN_MENU_POINT_IS_DIVIDER:
						$content .= "<li $li_additional_default class='" . $row['css_class'] . " divider' $li_id></li>\n";
						break;
					// Header
					case DROPDOWN_MENU_POINT_IS_HEADER:
						$content .= "<li $li_additional_default class='" . $row['css_class'] . " dropdown-header' $li_id>" . $row[ 'content' . $content_lang_number ] . "</li>\n";
						break;
					// Link to Page
					case DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE:

						$content .= "<li $li_additional_default class='" . $row['css_class'] . "$css_disabled' $li_id>";
						$content .= $this->get_link_to_page( $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ], $link_entry_default );
						$content .= "</li>\n";
						break;
					default:
						$content .= "<li>";
						$content .= $this->get_not_found( $row[ MENU_DB_TYPE_OF_MENU_POINT ] );
						$content .= "</li>\n";
						break;
				}
			}
			$content .= "</ul>\n</div>\n";
		}

		return $content;
	}

	protected function generate_ulli( $main_row, $content = "" ) {
		$menu_type = MENU_TYPE_ULLI;
		$sql       = "SELECT * FROM `" . TP . "menus` WHERE `to_id`='" . $main_row['id'] . "' AND `deleted` = '0' AND `active` = '1' ORDER BY `pos`";
		$result    = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {

			$ul_css_disabled = $this->get_css_disabled( $menu_type, $main_row, "ul" );
			$ul_id           = $this->get_id_name( $main_row );
			$ul_class        = $ul_css_disabled;

			$content .= "<ul $ul_id class='" . $main_row['css_class'] . "$ul_class'>";

			while ( $row = $result->fetch_assoc() ) {

				$li_css_disabled = $this->get_css_disabled( $menu_type, $row, "li" );
				$css_divider     = $this->get_css_divider( $menu_type, $row );

				$content_lang_number = $this->get_content_lang_number( $row );
				$name_lang_number    = $this->get_name_lang_number( $row );

				if ( $row['content_type'] == ULLI_MENU_POINT_IS_DROPDOWN ) {
					$content_status_class = $this->get_menu_status_class( $row, $content_lang_number );
					if ( $content_status_class == MENU_STATUS_CLASS_INACTIVE ) {
						$content_status_class = $this->get_dropdownmenu_status_class( $row['id'], $content_lang_number, ULLI_MENU_POINT_IS_DROPDOWN );
					}
				} else {
					$content_status_class = $this->get_menu_status_class( $row, $content_lang_number );
				}

				$li_id    = $this->get_id_name( $row );
				$li_class = $content_status_class . $li_css_disabled . $css_divider;
				$content .= "<li $li_id class='" . $row['css_class'] . "$li_class'>";

				switch ( $row['content_type'] ) {

					// Link
					case ULLI_MENU_POINT_IS_LINK:
						$content .= $this->get_link( $row, $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ] );
						break;
					// Free Text
					case ULLI_MENU_POINT_IS_FREE_TEXT:
						$content .= $row[ 'content' . $content_lang_number ];
						break;
					// Divider
					case ULLI_MENU_POINT_IS_DIVIDER:
						break;
					// Dropdown
					case ULLI_MENU_POINT_IS_DROPDOWN:
						if ( ! empty( $row[ 'name' . $name_lang_number ] ) ) {
							if ( $row[ 'name' . $name_lang_number ] == "index.php" ) {
								$link = "";
							} else {
								$link = $row[ 'name' . $name_lang_number ];
							}

							$content .= $this->l->link( $row[ 'content' . $content_lang_number ], $link, "", "", "", "link" . $content_status_class );
						} else {
							$content .= $row[ 'content' . $content_lang_number ];
						}
						break;
					// Link to Page
					case ULLI_MENU_POINT_IS_LINK_TO_PAGE:
						$content .= $this->get_link_to_page( $row[ 'content' . $content_lang_number ], $row[ 'name' . $name_lang_number ], $content_status_class, $row[ MENU_DB_OPEN_NEW_WINDOW ] );
						break;
					default:
						$content .= $this->txt( 'TYPE_NOT_FOUND_PART01' ) . " '" . $row['content_type'] . "' " . $this->txt( 'TYPE_NOT_FOUND_PART02' );
						break;
				}
				$content = $this->generate_ulli( $row, $content );
				$content .= "</li>\n";
			}
			$content .= "</ul>\n";
		}

		return $content;
	}


	public function display_menu_type( $id_type ) {
		switch ( $id_type ) {
			case MENU_TYPE_NAV:
				$type = "NAV";
				break;
			case MENU_TYPE_NAVBAR:
				$type = "NAVBAR";
				break;
			case MENU_TYPE_DROPDOWN:
				$type = "DROPDOWN";
				break;
			case MENU_TYPE_ULLI:
				$type = "ULLI";
				break;
			default:
				$type = $this->txt( 'UNKNOWN' );
				break;
		}

		return $type;
	}

	public function get_root_menu_options( $select_name, $row = false, $fix_selection = false, $layout = "tr" ) {
		$content = "";

		$select_id = $this->p->unique_id( NOT_BIG );
		$freetext  = "id='$select_id'";

		if ( ! is_array( $row ) ) {
			if ( $row === false ) {
				$match = MENU_TYPE_NAV;
			} else {
				$match = $row;
			}
			$options_nav_type           = 0;
			$options_nav_direction_type = 0;
			$options_nav_justified      = 0;
			$options_navbar_type        = 0;
			$options_navbar_1           = 0;
			$options_navbar_2           = 0;
			$options_brand              = "";
			$options_dropdown_right     = 0;
			$options_css_id             = "";
			$options_css_class          = "";
			$content_text               = "";
			$with_menu                  = false;
			$id                         = false;
			$name                       = "";
		} else {
			$match                      = $row[ MENU_DB_OPTIONS_TYPE ];
			$options_nav_type           = $row[ MENU_DB_OPTIONS_NAV_TYPE ];
			$options_nav_direction_type = $row[ MENU_DB_OPTIONS_NAV_DIRECTION_TYPE ];
			$options_nav_justified      = $row[ MENU_DB_OPTIONS_NAV_JUSTIFIED ];
			$options_navbar_type        = $row[ MENU_DB_OPTIONS_NAVBAR_TYPE ];
			$options_navbar_1           = $row[ MENU_DB_OPTIONS_NAVBAR_1 ];
			$options_navbar_2           = $row[ MENU_DB_OPTIONS_NAVBAR_2 ];
			$options_brand              = $row[ MENU_DB_OPTIONS_BRAND ];
			$options_dropdown_right     = $row[ MENU_DB_OPTIONS_DROPDOWN_RIGHT ];
			$options_css_id             = $row[ MENU_DB_OPTIONS_CSS_ID ];
			$options_css_class          = $row[ MENU_DB_OPTIONS_CSS_CLASS ];
			$content_text               = $row[ MENU_DB_OPTIONS_TEXT ];;
			$with_menu = $row[ MENU_DB_OPTIONS_WITH_MENU ];
			$id        = $row['id'];
			$name      = $row[ MENU_DB_OPTIONS_NAME ];
		}


		switch ( $layout ) {
			case "tr":
			default:
				$open_tag_header             = "<tr><th>";
				$close_tag_header            = "</th></tr>";
				$open_tag                    = "<tr><td>";
				$close_tag                   = "</td></tr>";
				$connect_tag_header          = "</th><td>";
				$connect_tag                 = "</td><td>";
				$open_tag_menu_type_nav      = "<tr id='menu_type_" . MENU_TYPE_NAV . "'><th>";
				$open_tag_menu_type_navbar   = "<tr id='menu_type_" . MENU_TYPE_NAVBAR . "'><th>";
				$open_tag_menu_type_dropdown = "<tr id='menu_type_" . MENU_TYPE_DROPDOWN . "'><th>";
				break;
		}

		if ( $fix_selection ) {
			$type = $this->display_menu_type( $match );
			$content .= $open_tag_header . $this->txt( 'TBL_TYPE' ) . $connect_tag_header . $type . $close_tag;

			$GLOBALS["body_footer"] .= "<script>

    $( function() {
        change_options( $match );
    });

</script>
    ";

		} else {
			$content .= $open_tag_header . $this->txt( 'TBL_TYPE' ) . $connect_tag_header . "\n";
			$content .= $this->l->select( $select_name, $freetext ) . "
         <option value='" . MENU_TYPE_NAV . "'";
			$match == MENU_TYPE_NAV ? $content .= " selected='selected'" : "";
			$content .= ">NAV</option>
         <option value='" . MENU_TYPE_NAVBAR . "'";
			$match == MENU_TYPE_NAVBAR ? $content .= " selected='selected'" : "";
			$content .= ">NAVBAR</option>
         <option value='" . MENU_TYPE_DROPDOWN . "'";
			$match == MENU_TYPE_DROPDOWN ? $content .= " selected='selected'" : "";
			$content .= ">DROPDOWN</option>
         <option value='" . MENU_TYPE_ULLI . "'";
			$match == MENU_TYPE_ULLI ? $content .= " selected='selected'" : "";
			$content .= ">ULLI</option>
         </select>\n";
			$content .= $close_tag . "\n";

			$GLOBALS["body_footer"] .= "<script>

    $( function() {
        change_options( $( \"#$select_id option:selected\" ).val() );
        $(\"#$select_id\").change(function() {
            change_options( $( \"#$select_id option:selected\" ).val() );
        })

    });

</script>
    ";
		}

		$this->p->init_array( $GLOBALS['foot'], 31 );
		$GLOBALS['foot'][31] .= "<script>

    function change_options(id)
    {
        $(\"#menu_type_" . MENU_TYPE_NAV . "\").hide();
        $(\"#menu_type_" . MENU_TYPE_NAVBAR . "\").hide();
        $(\"#menu_type_" . MENU_TYPE_DROPDOWN . "\").hide();
        if(id != " . MENU_TYPE_ULLI . ")
        {
            $(\"#menu_type_\" + id).show();
        }
    }

</script>
    ";

		$content .= $open_tag_header . $this->txt( 'TBL_NAME' ) . $connect_tag_header . $this->l->text( "name", $name ) . $close_tag . "\n";

		// NAV
		$content .=
			$open_tag_menu_type_nav . $this->txt( 'TBL_OPTIONS' ) . $connect_tag_header . $this->l->radio( "options_nav_type", "2", $options_nav_type ) . " " . $this->txt( 'SCRATCH' ) . "<br />
         " . $this->l->radio( "options_nav_type", "0", $options_nav_type ) . " " . $this->txt( 'AS_TABS' ) . "<br />
         " . $this->l->radio( "options_nav_type", "1", $options_nav_type ) . " " . $this->txt( 'AS_PILLS' ) . "<br /><br />
         " . $this->l->radio( "options_nav_direction_type", "0", $options_nav_direction_type ) . " " . $this->txt( 'HORIZONTAL' ) . "<br />
         " . $this->l->radio( "options_nav_direction_type", "1", $options_nav_direction_type ) . " " . $this->txt( 'VERTICAL' ) . "<br /><br />
         " . $this->l->checkbox( "options_nav_justified", "1", $options_nav_justified ) . " " . $this->txt( 'JUSTIFIED' ) . " (" . $this->txt( 'OVER_ENTIRE_WIDTH' ) . ")$close_tag\n" .

			// NAVBAR
			$open_tag_menu_type_navbar . $this->txt( 'TBL_OPTIONS' ) . $connect_tag_header . $this->l->radio( "options_navbar_type", "0", $options_navbar_type ) . " " . $this->txt( 'NORMAL' ) . "<br />
         " . $this->l->radio( "options_navbar_type", "1", $options_navbar_type ) . " " . $this->txt( 'FIXED_TOP' ) . "<br />
         " . $this->l->radio( "options_navbar_type", "2", $options_navbar_type ) . " " . $this->txt( 'FIXED_BELOW' ) . "<br />
         " . $this->l->radio( "options_navbar_type", "3", $options_navbar_type ) . " " . $this->txt( 'TOP_STATIC' ) . "<br />
         " . $this->l->checkbox( "options_navbar_1", "1", $options_navbar_1 ) . " " . $this->txt( 'FOR_MOBILE_DEVICES' ) . "<br />
         " . $this->l->checkbox( "options_navbar_2", "1", $options_navbar_2 ) . " " . $this->txt( 'INVERTED_COLOR' ) . "<br /><br />" .
			$this->txt( 'TBL_BRAND_TEXT' ) . " " . $this->l->text( "brand", $options_brand ) . "$close_tag\n" .

			// DROPDOWN
			$open_tag_menu_type_dropdown . $this->txt( 'TBL_OPTIONS' ) . $connect_tag_header . $this->l->checkbox( "options_dropdown_right", "1", $options_dropdown_right ) . " " . $this->txt( 'FLUSH_RIGHT' ) . "
         <br /><br />" . $this->txt( 'TBL_DISPLAY_TEXT' ) . " " . $this->l->text( "content_text", $content_text ) . $close_tag;

		// ALL

		$content .= $open_tag_header . $this->txt( 'TBL_CONNECTED_MENU' ) . $connect_tag_header . $this->l->select( "with_menu" ) . "<option value='NULL'";
		if ( is_null( $row['with_menu'] ) ) {
			$content .= " selected=selected";
		}
		$content .= ">" . $this->txt( 'UNRELATED' ) . "</option>\n";
		$menus_array          = $this->get_menus();
		$menus_select_options = $this->get_menu_select_options( $menus_array, 0, $with_menu, $id );
		$content .= $menus_select_options;

		$content .= "</select>" . $close_tag . "

        $open_tag_header" . $this->txt( 'TBL_CSS_ID' ) . "</th><td>" . $this->l->text( "options_css_id", $options_css_id ) . "$close_tag
        $open_tag_header" . $this->txt( 'TBL_CSS_CLASS' ) . "</th><td>" . $this->l->text( "options_css_class", $options_css_class ) . "$close_tag
            ";

		return $content;
	}


	public function get_menus( $to_id = 0, &$menu_array = null ) {
		if ( $to_id == 0 ) {
			$sql = "SELECT * FROM `" . TP . "menus` WHERE (`to_id`='0' OR `to_id` IS NULL) AND `deleted` = '0' ORDER BY POS";
		} else {
			$sql = "SELECT * FROM `" . TP . "menus` WHERE (`to_id`='$to_id') AND `deleted` = '0' ORDER BY POS";
		}
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			//$menu_array[] = array();
			while ( $row = $result->fetch_assoc() ) {
				$active = " (" . $this->txt( 'INACTIVE' ) . ")";
				if ( $row['active'] ) {
					$active = " (" . $this->txt( 'ACTIVE' ) . ")";
				}
				$menu_array[ $row['to_id'] ][ $row['id'] ] = $row['name'] . $active;
				$this->get_menus( $row['id'], $menu_array );
			}
		}

		return $menu_array;
	}

	public function get_menu_select_options( &$array, $key = 0, $selected_id = 0, $exclude_id = 0, $step = 0 ) {
		$content = "";
		if ( isset( $array[ $key ] ) ) {
			foreach ( $array[ $key ] as $id => $name ) {
				$spaces = str_repeat( "&#9658;", $step );
				if ( $exclude_id != $id ) {
					$selected = "";
					if ( $selected_id == $id ) {
						$selected = " selected=selected";
					}
					$content .= "<option value='" . $id . "'" . $selected . ">$spaces$name</option>\n";
					$content .= $this->get_menu_select_options( $array, $id, $selected_id, $exclude_id, ++ $step );
					-- $step;
				}
			}
		}

		return $content;
	}

	function get_content_type( $id_type, $row2, $display_case = "full" ) {
		$content = "";

		$page_for_content = "content_menus_content.php";

		switch ( $id_type ) {
			// NAV
			case MENU_TYPE_NAV:
				switch ( $row2[ MENU_DB_TYPE_OF_MENU_POINT ] ) {
					// Link
					case MENU_NAV_MENU_POINT_IS_LINK:
						if ( $display_case == "full" ) {
							$content .= $this->l->link( $row2[ MENU_DB_LINK_TEXT ], $row2[ MENU_DB_LINK_TO_PAGE ], "", "target='_blank'" ) . " &#8594; " . htmlspecialchars( $row2[ MENU_DB_LINK_TO_PAGE ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'LINK' );
						}
						break;
					// Dropdown
					case MENU_NAV_MENU_POINT_IS_DROPDOWN:
						if ( $display_case == "full" ) {
							$content .= $this->l->link_button( htmlspecialchars( $row2[ MENU_DB_DROPDOWN_TEXT ], ENT_QUOTES ), $page_for_content, "id=" . $row2['id'] );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'DROPDOWN_LINK' );
						}
						break;
					// Custom Text
					case MENU_NAV_MENU_POINT_IS_FREE_TEXT:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_FREE_TEXT ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'FREE_TEXT' );
						}
						break;
					// Link to Page
					case MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_LINK_TEXT ], ENT_QUOTES ) . " &#8594; " . $this->get_page_text( $row2[ MENU_DB_LINK_TO_PAGE ] );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'LINK_TO_PAGE' );
						}
						break;
					// Divider
					case MENU_NAV_SUB_MENU_POINT_IS_DIVIDER:
						if ( $display_case == "full" ) {
							$content .= MENU_DIVIDER_TEXT;
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'DIVIDER' );
						}
						break;
				}
				break;
			// NAVBAR
			case MENU_TYPE_NAVBAR:
				switch ( $row2[ MENU_DB_TYPE_OF_MENU_POINT ] ) {
					// Link
					case NAVBAR_SIDE_MENU_POINT_IS_LINK:
						if ( $display_case == "full" ) {
							$content .= $this->l->link( $row2[ MENU_DB_LINK_TEXT ], $row2[ MENU_DB_LINK_TO_PAGE ], "", "target='_blank'" ) . " &#8594; " . htmlspecialchars( $row2[ MENU_DB_LINK_TO_PAGE ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'LINK' );
						}
						break;
					// Dropdown
					case NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN:
						if ( $display_case == "full" ) {
							$content .= $this->l->link_button( htmlspecialchars( $row2[ MENU_DB_DROPDOWN_TEXT ], ENT_QUOTES ), $page_for_content, "id=" . $row2['id'] );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'DROPDOWN_LINK' );
						}
						break;
					// Custom Text
					case NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_FREE_TEXT ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'FREE_TEXT' );
						}
						break;
					// Button
					case NAVBAR_SIDE_MENU_POINT_IS_BUTTON:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_BUTTON_TEXT ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'BUTTON' );
						}
						break;
					// Custom Text with forms
					case NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_FORM_TEXT ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'TEXT_WITH_FORM' );
						}
						break;
					// Link to Page
					case NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_LINK_TEXT ], ENT_QUOTES ) . " &#8594; " . $this->get_page_text( $row2[ MENU_DB_LINK_TO_PAGE ] );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'LINK_TO_PAGE' );
						}
						break;
				}
				break;

			// Dropdowns
			case MENU_TYPE_DROPDOWN:
				switch ( $row2[ MENU_DB_TYPE_OF_MENU_POINT ] ) {
					// Link
					case DROPDOWN_MENU_POINT_IS_LINK:
						if ( $display_case == "full" ) {
							$content .= $this->l->link( $row2[ MENU_DB_LINK_TEXT ], $row2[ MENU_DB_LINK_TO_PAGE ], "", "target='_blank'" ) . " &#8594; " . htmlspecialchars( $row2[ MENU_DB_LINK_TO_PAGE ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'LINK' );
						}
						break;
					// Free Text
					case DROPDOWN_MENU_POINT_IS_FREE_TEXT:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_FREE_TEXT ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'FREE_TEXT' );
						}
						break;
					// Divider
					case DROPDOWN_MENU_POINT_IS_DIVIDER:
						if ( $display_case == "full" ) {
							$content .= MENU_DIVIDER_TEXT;
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'DIVIDER' );
						}
						break;
					// Header
					case DROPDOWN_MENU_POINT_IS_HEADER:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_HEADER_TEXT ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'HEADER' );
						}
						break;
					// Link to Page
					case DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_LINK_TEXT ], ENT_QUOTES ) . " &#8594; " . $this->get_page_text( $row2[ MENU_DB_LINK_TO_PAGE ] );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'LINK_TO_PAGE' );
						}
						break;
				}
				break;

			// ULLI
			case MENU_TYPE_ULLI:
				switch ( $row2[ MENU_DB_TYPE_OF_MENU_POINT ] ) {
					// Link
					case DROPDOWN_MENU_POINT_IS_LINK:
						if ( $display_case == "full" ) {
							$content .= $this->l->link( $row2[ MENU_DB_LINK_TEXT ], $row2[ MENU_DB_LINK_TO_PAGE ], "", "target='_blank'" ) . " &#8594; " . htmlspecialchars( $row2[ MENU_DB_LINK_TO_PAGE ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'LINK' );
						}
						break;
					// Free Text
					case DROPDOWN_MENU_POINT_IS_FREE_TEXT:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_FREE_TEXT ], ENT_QUOTES );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'FREE_TEXT' );
						}
						break;
					// Divider
					case DROPDOWN_MENU_POINT_IS_DIVIDER:
						if ( $display_case == "full" ) {
							$content .= MENU_DIVIDER_TEXT;
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'DIVIDER' );
						}
						break;
					// Dropdown
					case DROPDOWN_MENU_POINT_IS_HEADER:
						if ( $display_case == "full" ) {
							$content .= $this->l->link_button( htmlspecialchars( $row2[ MENU_DB_HEADER_TEXT ], ENT_QUOTES ), $page_for_content, "id=" . $row2['id'] );
						} elseif ( $display_case == "text" ) {
							$content .= $this->txt( 'HEADER' );
						}
						break;
					// Link to Page
					case DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $display_case == "full" ) {
							$content .= htmlspecialchars( $row2[ MENU_DB_LINK_TEXT ], ENT_QUOTES ) . " &#8594; " . $this->get_page_text( $row2[ MENU_DB_LINK_TO_PAGE ] );
						} elseif ( $display_case == "type" ) {
							$content .= $this->txt( 'LINK_TO_PAGE' );
						}
						break;
				}
				break;

			default:
				$content .= $this->txt( 'UNKNOWN_SEP' ) . " ";
				break;
		}

		return $content;
	}

	function get_menupoint_status_text( $id_type, $row2 ) {
		$content = "";

		switch ( $id_type ) {
			case MENU_TYPE_NAV:
				switch ( $row2[ MENU_DB_TYPE_OF_MENU_POINT ] ) {
					case MENU_NAV_MENU_POINT_IS_LINK:
					case MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $row2[ MENU_DB_CSS_STATUS ] ) {
							$content .= $this->txt( 'DISABLED_SEP' ) . " ";
						}
						if ( $row2[ MENU_DB_OPEN_NEW_WINDOW ] ) {
							$content .= $this->txt( 'NEW_WINDOW_SEP' ) . " ";
						}
						break;
					case MENU_NAV_MENU_POINT_IS_DROPDOWN:
						if ( $row2[ MENU_DB_CSS_STATUS ] ) {
							$content .= $this->txt( 'DISABLED_SEP' ) . " ";
						}
						break;
					default:
						break;
				}

				break;

			case MENU_TYPE_NAVBAR:
				switch ( $row2[ MENU_DB_TYPE_OF_MENU_POINT ] ) {
					case NAVBAR_SIDE_MENU_POINT_IS_LINK:
					case NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $row2[ MENU_DB_CSS_STATUS ] ) {
							$content .= $this->txt( 'DISABLED_SEP' ) . " ";
						}
						if ( $row2[ MENU_DB_OPEN_NEW_WINDOW ] ) {
							$content .= $this->txt( 'NEW_WINDOW_SEP' ) . " ";
						}
						break;
					case NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN:
					case NAVBAR_SIDE_MENU_POINT_IS_BUTTON:
						if ( $row2[ MENU_DB_CSS_STATUS ] ) {
							$content .= $this->txt( 'DISABLED_SEP' ) . " ";
						}
						break;
					case NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT:
						if ( $row2[ MENU_DB_TEXT_COLOR_UNIFORM ] ) {
							$content .= $this->txt( 'TEXT_COLOR_UNIFORM_SEP' ) . " ";
						}
						break;
					default:
						break;
				}
				break;

			case MENU_TYPE_DROPDOWN:
				switch ( $row2[ MENU_DB_TYPE_OF_MENU_POINT ] ) {
					case DROPDOWN_MENU_POINT_IS_LINK:
					case DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $row2[ MENU_DB_CSS_STATUS ] ) {
							$content .= $this->txt( 'DISABLED_SEP' ) . " ";
						}
						if ( $row2[ MENU_DB_OPEN_NEW_WINDOW ] ) {
							$content .= $this->txt( 'NEW_WINDOW_SEP' ) . " ";
						}
						break;
					default:
						break;
				}
				break;

			case MENU_TYPE_ULLI:
				switch ( $row2[ MENU_DB_TYPE_OF_MENU_POINT ] ) {
					case ULLI_MENU_POINT_IS_LINK:
					case ULLI_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $row2[ MENU_DB_CSS_STATUS ] ) {
							$content .= $this->txt( 'DISABLED_SEP' ) . " ";
						}
						if ( $row2[ MENU_DB_OPEN_NEW_WINDOW ] ) {
							$content .= $this->txt( 'NEW_WINDOW_SEP' ) . " ";
						}
						break;
					case ULLI_MENU_POINT_IS_DROPDOWN:
						if ( $row2[ MENU_DB_CSS_STATUS ] ) {
							$content .= $this->txt( 'DISABLED_SEP' ) . " ";
						}
						break;
					default:
						break;
				}
				break;

			default:
				$content .= $this->txt( 'UNKNOWN_SEP' ) . " ";
				break;
		}

		return $content;
	}

	public function get_page_text( $id ) {
		$content = "";

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql          = "SELECT * FROM `" . TP . "pages` WHERE `deleted`='0' AND `id`='$id'";
			$result_pages = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result_pages->num_rows == 1 ) {
				$row = $result_pages->fetch_assoc();
				$row['active'] ? $active = $this->txt( 'ACTIVE' ) : $active = $this->txt( 'INACTIVE' );
				$content .= $this->l->link( $row['name'], ADMINDIR . "content_pages_edit.php?id=" . $id ) . " ($active)";
			} else {
				$content .= $this->txt( 'LISTING_NO_LONGER_VALID' ) . "\n";
			}
		} else {
			$content .= $this->txt( 'NOT_LINKED' );
		}

		return $content;
	}

}