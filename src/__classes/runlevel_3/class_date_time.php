<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_date_time extends class_root {

	protected $log;

	public function __construct() {
		$this->log = class_logging::getInstance();
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public function sqldatetime( $date, $direction = "in" ) {
		if ( $date == 0 ) {
			$tmp = "";
		} else {
			if ( strlen( $date ) == 4 ) {
				$date = date( 'd' ) . "." . date( 'm' ) . "." . $date . " " . date( 'H' ) . ":" . $date( 'i' );
			}

			if ( ! is_numeric( $date ) ) {
				$time = strtotime( $date );
			} else {
				$time = $date;
			}

			$tmp = "";
			if ( $time != - 1 ) {
				if ( $direction == "in" ) {
					$tmp .= date( $GLOBALS['default_datetime_format']['datetime'], $time );
				} else {
					$tmp .= date( "Y-m-d H:i:s", $time );
				}
			} else {
				$tmp = $this->txt( "Cannot translate datetime" );
				$this->log->error( "GET", __FILE__ . ":" . __LINE__, "Cannot translate datetime: " . $date . " Direction: " . $direction );
			}
		}

		return $tmp;
	}


	function get_month( $month, $short = false ) {
		if ( ! $short ) {
			return strftime( "%B", mktime( 0, 0, 0, $month, 1, 1 ) );
		} else {
			return strftime( "%b", mktime( 0, 0, 0, $month, 1, 1 ) );
		}
	}

	public function get_last_day( $month, $year ) {
		$month ++;
		if ( $month == 13 ) {
			$month = 1;
			$year ++;
		}
		$d = new DateTime( $year . "-" . $month . "-01" );
		$d->modify( '-1 day' );
		$last_day = $d->format( 'd' );

		return $last_day;
	}

	public function get_timedate( $all = true ) {
		if ( $all === true ) {
			return date( $GLOBALS['default_datetime_format']['datetime'] );
		} else {
			switch ( $all ) {
				case "year_only":
					return date( $GLOBALS['default_datetime_format']['year'] );
					break;
				case "date_only":
					return date( $GLOBALS['default_datetime_format']['date'] );
					break;
				case "time_only":
					return date( $GLOBALS['default_datetime_format']['time'] );
					break;
				default:
					return "error";
					break;
			}
		}
	}

	public function set_birthdate( $day, $month, $year ) {
		$set_birth_day = true;
		if ( $day <= 0 ) {
			$set_birth_day = false;
		}
		$set_birth_month = true;
		if ( $month <= 0 ) {
			$set_birth_month = false;
		}
		$set_birth_year = true;
		if ( $year <= 0 ) {
			$set_birth_year = false;
		}

		if ( ! ( $set_birth_day && $set_birth_month && $set_birth_year ) ) {
			$birth = "NULL";
		} else {
			if ( ! $set_birth_day ) {
				$day = "1";
			}
			if ( ! $set_birth_month ) {
				$month = "1";
			}
			if ( ! $set_birth_year ) {
				$year = "1899";
			}

			$birth = $day . "." . $month . "." . $year;
			$birth = $this->sqldate( $birth, "out" );
		}

		return $birth;
	}

	public function sqldate( $date, $direction = "in" ) {
		if ( $date == 0 ) {
			$tmp = "0";
		} else {
			if ( strlen( $date ) == 4 ) {
				$date = date( 'd' ) . "." . date( 'm' ) . "." . $date;
			}

			$time = strtotime( $date );

			$tmp = "";
			if ( $time != - 1 ) {
				if ( $direction == "in" ) {
					$tmp .= date( $GLOBALS['default_datetime_format']['date'], $time );
				} else {
					$tmp .= date( "Y-m-d", $time );
				}
			} else {
				$tmp = "Cannot translate date";
				$this->log->error( "GET", __FILE__ . ":" . __LINE__, "Cannot translate date: " . $date . " Direction: " . $direction );
			}
		}

		return $tmp;
	}

	public function get_birthdate( $birth, $isset_day, $isset_month, $isset_year ) {
		$birth = strtotime( $birth );
		if ( $isset_day ) {
			$day = date( "d", $birth );
		} else {
			$day = "--";
		}
		if ( $isset_month ) {
			$month = date( "m", $birth );
		} else {
			$month = "--";
		}
		if ( $isset_year ) {
			$year = date( "Y", $birth );
		} else {
			$year = "----";
		}

		$birth = $day . "." . $month . "." . $year;

		return $birth;
	}


}