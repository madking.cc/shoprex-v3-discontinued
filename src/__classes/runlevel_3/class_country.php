<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


class class_country extends class_root {
	protected $db;
	protected $log;
	protected $lang;

	public function __construct() {
		$this->db   = class_database::getInstance();
		$this->log  = class_logging::getInstance();
		$this->lang = class_language::getInstance();
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public function get_country_name( $iso ) {
		if ( is_null( $iso ) || empty( $iso ) ) {
			return $this->txt( "NOT_SPECIFIED" );
		} elseif ( strcasecmp( $iso, $this->txt( "ALL" ) ) === 0 ) {
			return $this->txt( "ALL" );
		} else {
			$iso = $this->db->escape( $iso );
			if ( strlen( $iso ) != 2 ) {
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong iso length, iso: '$iso'" );

				return $this->txt( "ERROR" );
			} else {
				$sql    = "SELECT * FROM `" . TP . "country_list` WHERE UPPER(`iso-2`) LIKE UPPER('" . $iso . "') LIMIT 1";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				if ( $result->num_rows == 1 ) {
					$row = $result->fetch_assoc();

					return $row[ 'name' . $this->lang->lnbr ];
				} else {
					$this->log->notice( "php", __FILE__ . ":" . __LINE__, "Can't find match, sql: '$sql'" );

					return $this->txt( "NOT_FOUND" );
				}
			}
		}
	}
}