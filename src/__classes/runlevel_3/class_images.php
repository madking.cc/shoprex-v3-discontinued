<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_images extends class_uri_file_path_operations {
	protected $loc;
	protected $log;
	protected $p;
	protected $l;
	protected $db;

	// Init
	protected $init_array;
	public $insert_type;
	public $target;
	public $form_id;
	public $do_submit;
	public $do_resize;
	public $keep_ratio;
	public $autoresize;
	public $auto_max_x;
	public $auto_max_y;
	public $uploaddir;
	public $is_sub_window;
	public $hint;
	public $do;
	public $do_back;
	public $keep_original_picture;
	public $uploadsubdir_orig;
	public $one_step;
	public $element_number;
	public $element_is_image;
	public $store_in_db;

	public $preview_prefix;
	protected $resized_picture;
	public $error;
	public $error_text;
	protected $is_admin;

	protected $html_picture_link_type;
	protected $picture_alt;
	protected $crop_left;
	protected $crop_right;
	protected $crop_top;
	protected $crop_bottom;
	protected $x_size;
	protected $y_size;
	protected $x_size_new;
	protected $y_size_new;
	protected $file_name_checked;
	protected $file_name_original_checked;
	protected $proportional;
	protected $html_image_center;
	protected $html_image_responsive;
	protected $init_array_saved;

	public function __construct() {
		parent::__construct();
		$this->loc = class_location::getInstance();
		$this->log = class_logging::getInstance();
		$this->p   = new class_page();
		$this->l   = new class_layout();
		$this->db  = class_database::getInstance();

		$this->is_admin = $GLOBALS["is_admin"];

		$this->error                      = false;
		$this->error_text                 = null;
		$this->html_picture_link_type     = 0;
		$this->picture_alt                = "";
		$this->crop_left                  = 0;
		$this->crop_right                 = 0;
		$this->crop_top                   = 0;
		$this->crop_bottom                = 0;
		$this->x_size                     = null;
		$this->y_size                     = null;
		$this->x_size_new                 = null;
		$this->y_size_new                 = null;
		$this->file_name_checked          = "";
		$this->file_name_original_checked = "";
		$this->proportional               = 1;
		$this->resized_picture            = false;
		$this->preview_prefix             = "preview_";
		$this->html_image_center          = false;
		$this->html_image_responsive      = true;
		$this->init_array_saved           = array();

		// insert_types:
		// 0 = Simple Textarea
		// 1 = TinyMCE Textarea
		// 2 = Codemirror Textarea
		// 10 = Input Text
		// 11 = Get Array
		// 99 = Only Upload

		$this->init_array = array(
			"insert_type"           => null,
			"target"                => null,
			"form_id"               => null,
			"do_submit"             => false,
			"do_resize"             => true,
			"keep_ratio"            => true,
			"autoresize"            => false,
			"auto_max_x"            => null,
			"auto_max_y"            => null,
			"uploaddir"             => UPLOADDIR,
			"is_sub_window"         => true,
			"hint"                  => null,
			"do"                    => null,
			"do_back"               => "back",
			"keep_original_picture" => true,
			"uploadsubdir_orig"     => "orig/",
			"one_step"              => false,
			"element_number"        => 0,
			"element_is_image"      => true,
			"store_in_db"           => true
		);

		$this->init( $this->init_array );

	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public function email_img_create( $email, $file, $path, $use_dirroot = true ) {
		if ( $use_dirroot ) {
			$path = DIRROOT . $path;
		}
		$strdimensions = imagettfbbox( 10.5, 0.0, $this->loc->dir_root . 'arial.ttf', $email );
		$width         = $strdimensions[2] - $strdimensions[0];
		$height        = $strdimensions[1] - $strdimensions[7];
		$image         = @imagecreate( $width + 2, $height + 4 );
		$white         = imagecolorallocate( $image, 255, 255, 255 );
		$black         = imagecolorallocate( $image, 0, 0, 0 ); //background
		imagecolortransparent( $image, $white );
		imagettftext( $image, 10.5, 0.0, 0, $height - 2, $black, DIRROOT . 'arial.ttf', $email );
		imagepng( $image, $path . $file . '.png' );
		//var_dump($path, $file);
		imagedestroy( $image );
	}

	public function resize( $file_source, $dir_source, $new_size_x, $new_size_y, $keep_ratio = true, $new_file_prefix = null, $direct_call = true, $dir_destination = null, $file_destination = null, $use_dirroot = true ) {
		$content = "";

		if ( $use_dirroot ) {
			$dir_source = DIRROOT . $dir_source;
		}

		if ( is_null( $dir_destination ) ) {
			$dir_destination = $dir_source;
		} elseif ( $use_dirroot ) {
			$dir_destination = DIRROOT . $dir_destination;
		}
		if ( is_null( $file_destination ) ) {
			$file_destination = $file_source;
		}

		if ( ! is_file( $dir_source . $file_source ) ) {
			$error = $this->txt( 'PICTURE_FILE_NOT_FOUND' );
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Picture File not found: " . $dir_source . $file_source );

			return $error;
		}

		list( $image_width, $image_height, $picture_type, $image_attr ) = getimagesize( $dir_source . $file_source );
		$picture_type = $this->get_image_type( $picture_type );
		if ( $new_size_x != $image_width || $new_size_y != $image_height ) {
			if ( $picture_type == "jpeg" ) {
				$image = imagecreatefromjpeg( $dir_source . $file_source );
			} elseif ( $picture_type == "gif" ) {
				$image = imagecreatefromgif( $dir_source . $file_source );
			} elseif ( $picture_type == "png" ) {
				$image = imagecreatefrompng( $dir_source . $file_source );
			}
			$image_b = $image_width;
			$image_h = $image_height;

			if ( $keep_ratio ) {
				if ( $image_b > $image_h ) {
					$ratio = $image_b / $image_h;

					$new_b = $new_size_x;
					$new_h = round( $new_b / $ratio );
				} else {
					$ratio = $image_h / $image_b;

					$new_h = $new_size_y;
					$new_b = round( $new_h / $ratio );
				}

				if ( $new_h > $new_size_y ) {
					$ratio = $image_h / $image_b;

					$new_h = $new_size_y;
					$new_b = round( $new_h / $ratio );
				}
				if ( $new_b > $new_size_x ) {
					$ratio = $image_b / $image_h;

					$new_b = $new_size_x;
					$new_h = round( $new_b / $ratio );
				}
			} else {
				$new_h = $new_size_y;
				$new_b = $new_size_x;
			}
			if ( ( $picture_type == "jpeg" ) || ( $picture_type == "png" ) ) {
				$image_new = imagecreatetruecolor( $new_b, $new_h );
			} elseif ( $picture_type == "gif" ) {
				$image_new = imagecreate( $new_b, $new_h );
			} else {
				$image_new = false;
			}
			if ( ! $image_new ) {
				$error = $this->txt( 'ERROR_CREATION_PLACEHOLDER_NEW_IMAGE' );
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "In the creation of an image placeholder occured an error." );

				return $error;
			}
			// 1 = Zielbild
			// 2 = Quellbild
			// 3 = X-Coo vom Zielbild
			// 4 = Y-Coo vom Zielbild
			// 5 = X-Coo vom Quellbild
			// 6 = Y-Coo vom Quellbild
			// Zielbild Breite
			// Zielbild Höhe
			// Quellbild Breite
			// Quellbild Höhe

			$noerror_new = imagecopyresampled( $image_new, $image, 0, 0, 0, 0, $new_b, $new_h, $image_b, $image_h );
			if ( ! $noerror_new ) {
				$error = $this->txt( 'ERROR_COPY_PICTURE_TO_NEW_SIZE' );
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Cannot copy picture in new size." );

				return $error;
			}

			if ( $picture_type == "jpeg" ) {
				$noerror_new = imagejpeg( $image_new, $dir_destination . $new_file_prefix . $file_destination, 100 );
			} elseif ( $picture_type == "gif" ) {
				$noerror_new = imagegif( $image_new, $dir_destination . $new_file_prefix . $file_destination );
			} elseif ( $picture_type == "png" ) {
				$noerror_new = imagepng( $image_new, $dir_destination . $new_file_prefix . $file_destination );
			}

			imagedestroy( $image );

			if ( ! $noerror_new ) {
				$error = $this->txt( 'ERROR_WHILE_SAVING_NEW_PICTURE' );
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Cannot save new picture. Destination: " . $dir_destination . $new_file_prefix . $file_destination );

				return $error;
			}
		}

		return "";
	}

	public function resize_pictures( $path, $new_x, $new_y, $use_dirroot = true ) {
		$content = "";

		if ( empty( $path ) || empty( $new_x ) || empty( $new_y ) || ! is_numeric( $new_x ) || ! is_numeric( $new_y ) || $new_x < 0 || $new_y < 0 ) {
			$content .= $this->l->alert_text_dismiss( "danger", $this->txt( "Can not change picture in size" ) );
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "One or more wrong values. path: '$path', new_x: '$new_x', new_y: '$new_y'" );

			return $content;
		} else {

			if ( $use_dirroot ) {
				$dirroot = DIRROOT;
			} else {
				$dirroot = "";
			}

			$sql       = "SELECT * FROM `" . TP . "images_resize_helper` WHERE `uploaddir` LIKE BINARY '$path' AND `deleted` = '0'";
			$result_db = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			while ( $row = $result_db->fetch_assoc() ) {
				if ( is_file( $dirroot . $path . $row['filename_copy'] ) ) {
					@$result = unlink( $dirroot . $path . $row['filename_copy'] );
					if ( $result == false ) {
						$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't delete file '" . $dirroot . $path . $row['filename_copy'] . "'" );
					}
				}

				$result = $this->resize( $row['filename_original'], $dirroot . $path . "orig/", $new_x, $new_y, KEEP_RATIO, NO_FILE_PREFIX, DIRECT_CALL, $dirroot . $path, $row['filename_copy'], ! $use_dirroot );
				if ( ! empty( $result ) ) {
					return $result;
				}

				$result = $this->crop( $row['filename_copy'], $row['crop_top'], $row['crop_bottom'], $row['crop_left'], $row['crop_right'], DIRECT_CALL, $dirroot . $path, ! $use_dirroot );
				if ( ! empty( $result ) ) {
					return $result;
				}
			}

			return "";
		}
	}


	public function crop( $picture, $top, $bottom, $left, $right, $direct_call = true, $uploaddir = "", $use_dirroot = true ) {
		$content = "";

		if ( $top == 0 && $bottom == 0 && $left == 0 && $right == 0 ) {
			return "";
		}

		if ( empty( $uploaddir ) ) {
			$uploaddir = $this->uploaddir;
		}

		if ( $use_dirroot ) {
			$uploaddir = DIRROOT . $uploaddir;
		}


		if ( ! is_file( $uploaddir . $picture ) ) {
			$error = $this->txt( 'PICTURE_FILE_NOT_FOUND' );

			if ( ! $direct_call ) {
				$content .= $this->l->alert_text( "danger", $error, DO_BACK, $this->get_parameter_failure() );
			} else {
				$content .= $this->l->alert_text_dismiss( "danger", $error );
			}

			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Picture File not found: " . $uploaddir . $picture );

			$this->error = true;

			return $content;
		}

		list( $image_width, $image_height, $picture_type, $image_attr ) = getimagesize( $uploaddir . $picture );

		$picture_type = $this->p->get_image_type( $picture_type );

		if ( $picture_type == "jpeg" ) {
			$image = imagecreatefromjpeg( $uploaddir . $picture );
		} elseif ( $picture_type == "gif" ) {
			$image = imagecreatefromgif( $uploaddir . $picture );
		} elseif ( $picture_type == "png" ) {
			$image = imagecreatefrompng( $uploaddir . $picture );
		} else {
			$error = $this->txt( 'WRONG_PICTURE_TYPE' );

			if ( ! $direct_call ) {
				$content .= $this->l->alert_text( "danger", $error, DO_BACK, $this->get_parameter_failure() );
			} else {
				$content .= $this->l->alert_text_dismiss( "danger", $error );
			}

			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong picture-type: '$picture_type'" );

			$this->error = true;

			return $content;
		}

		$new_b = $image_width - $left - $right;
		$new_h = $image_height - $top - $bottom;

		//$this->log->event("idefix", "", "image_width $image_width, image_height_o $image_height, left $left, right $right,
		//new_b $new_b, top $top, bottom $bottom, new_h $new_h");

		$image_new = false;
		if ( ( $picture_type == "jpeg" ) || ( $picture_type == "png" ) ) {
			$image_new = imagecreatetruecolor( $new_b, $new_h );
		}
		if ( $picture_type == "gif" ) {
			$image_new = imagecreate( $new_b, $new_h );
		}

		// 1 = Zielbild
		// 2 = Quellbild
		// 3 = X-Coo vom Zielbild
		// 4 = Y-Coo vom Zielbild
		// 5 = X-Coo vom Quellbild
		// 6 = Y-Coo vom Quellbild
		// Zielbild Breite
		// Zielbild Höhe
		// Quellbild Breite
		// Quellbild Höhe

		$noerror_new = imagecopyresampled( $image_new, $image, 0, 0, $left, $top, $new_b, $new_h, $new_b, $new_h );

		if ( ! $noerror_new ) {
			$error = $this->txt( 'ERROR_COPY_PICTURE_TO_NEW_SIZE' );
			if ( ! $direct_call ) {
				$content .= $this->l->alert_text( "danger", $error, DO_BACK, $this->get_parameter_failure() );
			} else {
				$content .= $this->l->alert_text_dismiss( "danger", $error );
			}
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Cannot copy picture in new size." );

			$this->error = true;

			return $content;
		}

		if ( $picture_type == "jpeg" ) {
			$noerror_new = imagejpeg( $image_new, $uploaddir . $picture, 100 );
		} elseif ( $picture_type == "gif" ) {
			$noerror_new = imagegif( $image_new, $uploaddir . $picture );
		} elseif ( $picture_type == "png" ) {
			$noerror_new = imagepng( $image_new, $uploaddir . $picture );
		}

		imagedestroy( $image );

		if ( ! $noerror_new ) {
			$error = $this->txt( 'ERROR_WHILE_SAVING_NEW_PICTURE' );
			if ( ! $direct_call ) {
				$content .= $this->l->alert_text( "danger", $error, DO_BACK, $this->get_parameter_failure() );
			} else {
				$content .= $this->l->alert_text_dismiss( "danger", $error );
			}
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Cannot save new picture. Destination: " . $uploaddir . $picture );

			$this->error = true;

			return $content;
		}

		$this->resized_picture = true;

		return "";
	}

	public function delete_in_db( $filename_copy, $upload_dir ) {

		if ( empty( $filename_copy ) || empty( $upload_dir ) ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "No filename '$filename_copy' or directory '$upload_dir' given" );

			return false;
		}

		$upload_dir = urldecode( $upload_dir );

		$sql    = "SELECT * FROM `" . TP . "images_resize_helper` WHERE `deleted` = '0' AND `filename_copy` LIKE BINARY '$filename_copy' AND `uploaddir` LIKE BINARY '$upload_dir'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows == 0 ) {
			$this->log->notice( "image", __FILE__ . ":" . __LINE__, "No entries for deletion found. Filename_copy: '$filename_copy', uploaddir: '$upload_dir'" );
		} else {
			while ( $row = $result->fetch_assoc() ) {
				$this->db->upd( __FILE__ . ":" . __LINE__, "images_resize_helper", array( "deleted" ), array( true ), "`id` = '" . $row['id'] . "'" );
			}
		}

		return true;

	}

	public function delete_in_filesystem( $filename_copy, $upload_dir, $filename_orig, $use_dirroot = true ) {
		if ( $use_dirroot ) {
			$dirroot = DIRROOT;
		} else {
			$dirroot = "";
		}

		$upload_subdir_original = "orig/";

		$result = true;
		if ( is_file( $dirroot . $upload_dir . $filename_copy ) ) {
			$result = $this->move_file_with_auto_renaming( $upload_dir . $filename_copy, $upload_dir . "delete/" . $filename_copy );
		}
		if ( $result === false ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file into delete folder: '" . $dirroot . $upload_dir . $filename_copy . "'" );
		} elseif ( $result == - 1 ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file into delete folder, cause of failure in renaming to destination: '" . $dirroot . $upload_dir . $filename_copy . "'" );
		} else {
			$this->log->event( "file", __FILE__ . ":" . __LINE__, "Uploaded Picture moved into delete folde. Name: '" . $dirroot . $upload_dir . $filename_copy . "'" );
		}

		$result = true;
		if ( is_file( $dirroot . $upload_dir . $this->preview_prefix . $filename_copy ) ) {
			$result = $this->move_file_with_auto_renaming( $upload_dir . $this->preview_prefix . $filename_copy, $upload_dir . "delete/" . $this->preview_prefix . $filename_copy );
		}
		if ( $result === false ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file into delete folder: '" . $dirroot . $upload_dir . $this->preview_prefix . $filename_copy . "'" );
		} elseif ( $result == - 1 ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file into delete folder, cause of failure in renaming to destination: '" . $dirroot . $upload_dir . $this->preview_prefix . $filename_copy . "'" );
		} else {
			$this->log->event( "file", __FILE__ . ":" . __LINE__, "Uploaded Picture moved into delete folde. Name: '" . $dirroot . $upload_dir . $this->preview_prefix . $filename_copy . "'" );
		}

		if ( empty( $filename_orig ) ) {
			$dbresult = $this->get_dbresutlt_filename_orig_from_filename_copy( $filename_copy, $upload_dir );
			while ( $row = $dbresult->fetch_assoc() ) {
				$result = true;
				if ( is_file( $dirroot . $upload_dir . $upload_subdir_original . $row['filename_original'] ) ) {
					$result = $this->move_file_with_auto_renaming( $upload_dir . $upload_subdir_original . $row['filename_original'], $upload_dir . $upload_subdir_original . "delete/" . $row['filename_original'] );
				}
				if ( $result === false ) {
					$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file into delete folder: '" . $dirroot . $upload_dir . $upload_subdir_original . $row['filename_original'] . "'" );
				} elseif ( $result == - 1 ) {
					$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file into delete folder, cause of failure in renaming to destination: '" . $dirroot . $upload_dir . $upload_subdir_original . $row['filename_original'] . "'" );
				} else {
					$this->log->event( "file", __FILE__ . ":" . __LINE__, "Uploaded Picture moved into delete folde. Name: '" . $dirroot . $upload_dir . $upload_subdir_original . $row['filename_original'] . "'" );
				}
			}
		} else {
			$result = true;
			if ( is_file( $dirroot . $upload_dir . $upload_subdir_original . $filename_orig ) ) {
				$result = $this->move_file_with_auto_renaming( $upload_dir . $upload_subdir_original . $filename_orig, $upload_dir . $upload_subdir_original . "delete/" . $filename_orig );
			}
			if ( $result === false ) {
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file into delete folder: '" . $dirroot . $upload_dir . $upload_subdir_original . $filename_orig . "'" );
			} elseif ( $result == - 1 ) {
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file into delete folder, cause of failure in renaming to destination: '" . $dirroot . $upload_dir . $upload_subdir_original . $filename_orig . "'" );
			} else {
				$this->log->event( "file", __FILE__ . ":" . __LINE__, "Uploaded Picture moved into delete folde. Name: '" . $dirroot . $upload_dir . $upload_subdir_original . $filename_orig . "'" );
			}
		}
	}

	public function delete_in_db_and_filesystem( $filename_copy, $upload_dir, $filename_orig = "", $use_dirroot = true ) {

		if ( empty( $filename_copy ) || empty( $upload_dir ) ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "No filename '$filename_copy' or directory '$upload_dir' given" );

			return false;
		}

		$this->delete_in_filesystem( $filename_copy, $upload_dir, $filename_orig, $use_dirroot );
		$this->delete_in_db( $filename_copy, $upload_dir );

		return true;
	}

	public function get_dbresutlt_filename_orig_from_filename_copy( $filename_copy, $upload_dir ) {
		$sql    = "SELECT * FROM `" . TP . "images_resize_helper` WHERE `deleted` = '0' AND `filename_copy` LIKE BINARY '$filename_copy' AND `uploaddir` LIKE BINARY '$upload_dir'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		return $result;
	}

	public function rename_in_db( $filename_copy, $upload_dir, $new_filename_copy ) {
		$sql    = "SELECT * FROM `" . TP . "images_resize_helper` WHERE `deleted` = '0' AND `filename_copy` LIKE BINARY '$filename_copy' AND `uploaddir` LIKE BINARY '$upload_dir'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		while ( $row = $result->fetch_assoc() ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "images_resize_helper", array( "filename_copy" ), array( $new_filename_copy ), "`id` = '" . $row['id'] . "'" );
		}
	}

	public function close_colorbox() {
		if ( $this->is_sub_window ) {

			$GLOBALS['body_footer'] .= "<script>
            parent.$.fn.colorbox.close();
        </script>";
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	protected function crop_with_optional_preview( $uploaddir, $file_name, $x_size_new, $y_size_new, $crop_top = 0, $crop_right = 0, $crop_bottom = 0, $crop_left = 0, $preview_prefix = "", $proportional = true, $use_dirroot = true ) {
		if ( $use_dirroot ) {
			$uploaddir = DIRROOT . $uploaddir;
		}

		// Schneide (prefix, wenn prefix leer, wird das normale Bild genommen)
		if ( ( $crop_top > 0 || $crop_bottom > 0 || $crop_left > 0 || $crop_right > 0 ) ) {
			// Hole Höhe und Breite von (prefix) Bild
			list( $x_size_original, $y_size_original, $picture_type, $image_attr ) = $this->get_picture_stats( $uploaddir . $preview_prefix . $file_name, ! $use_dirroot );

			// Wenn die original Bildgröße später geändert werden soll
			if ( $x_size_original != $x_size_new || $y_size_original != $y_size_new ) {
				// Bestimme Ratio von Crop, da das Bild erst danach in der Größe geändert wird
				$crop_ratio_y = $y_size_original / $y_size_new;
				$crop_ratio_x = $x_size_original / $x_size_new;

				$crop_left_new  = round( $crop_ratio_x * $crop_left );
				$crop_right_new = round( $crop_ratio_x * $crop_right );

				$crop_top_new    = round( $crop_ratio_y * $crop_top );
				$crop_bottom_new = round( $crop_ratio_y * $crop_bottom );

				$result = $this->crop( $preview_prefix . $file_name, $crop_top_new, $crop_bottom_new, $crop_left_new, $crop_right_new, NO_DIRECT_CALL, $uploaddir, ! $use_dirroot );
				if ( ! empty( $result ) && $this->error ) {
					return $result;
				}

				// Wenn es ein prefix gibt und die spätere Größenänderung Proportinal ist, Schneide normales Bild zu
				// (Das normale Bild hat ja durch das Preview Bild die volle Größe und wird auch nicht in der Größe geändert)
				if ( ! empty( $preview_prefix ) && $proportional ) {
					$result = $this->crop( $file_name, $crop_top_new, $crop_bottom_new, $crop_left_new, $crop_right_new, NO_DIRECT_CALL, $uploaddir, ! $use_dirroot );
					if ( ! empty( $result ) && $this->error ) {
						return $result;
					}
				}
			} // Wenn Bildgröße später nicht geändert wird
			else {
				// Schneide (prefix) Bild zu
				$result = $this->crop( $preview_prefix . $file_name, $crop_top, $crop_bottom, $crop_left, $crop_right, NO_DIRECT_CALL, $uploaddir, ! $use_dirroot );
				if ( ! empty( $result ) && $this->error ) {
					return $result;
				}

				// Wenn es ein prefix gibt und die spätere Größenänderung Proportinal ist, Schneide normales Bild zu
				// (Das normale Bild hat ja durch das Preview Bild die volle Größe und wird auch nicht in der Größe geändert)
				if ( ! empty( $preview_prefix ) && $proportional ) {
					$result = $this->crop( $file_name, $crop_top, $crop_bottom, $crop_left, $crop_right, NO_DIRECT_CALL, $uploaddir, ! $use_dirroot );
					if ( ! empty( $result ) && $this->error ) {
						return $result;
					}
				}
			}
		}

		return "";
	}

	protected function check_form_transfer_ok( $error_code, $name ) {

		if ( $error_code != 0 ) {
			$this->error      = true;
			$this->error_text = $this->l->alert_text( "danger", $this->file_upload_error( $error_code, $name ), DO_BACK, $this->get_parameter_failure() );
		}
	}

	protected function check_if_picture( $name ) {
		$result = $this->p->check_special_filetype( $name, "picture" );
		if ( $result == false ) {
			$this->error = true;

			$error            = $this->txt( 'ONLY_THIS_FORMATS_OF_PICTURES_ARE_ALLOWED_AS_UPLOAD' );
			$this->error_text = $this->l->alert_text( "danger", $error, DO_BACK, $this->get_parameter_failure() );
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Not allowed filetype. Filename: '" . $name . "'" );

			return false;
		} else {
			return true;
		}
	}

	protected function move_uploaded_file( $tmp, $destination, $use_dirroot = true ) {
		if ( $use_dirroot ) {
			$dirroot = DIRROOT;
		} else {
			$dirroot = "";
		}

		if ( ! move_uploaded_file( $tmp, $dirroot . $destination ) ) {

			$this->error = true;

			$error = $this->txt( 'ERROR_WITH_FILE_UPLOAD_TOO_BIG' ) . "<br/>
                " . $this->txt( 'SOURCE_PATH' ) . " " . $tmp . "<br />
                " . $this->txt( 'DESTINATION_PATH' ) . " " . DIRROOT . $destination;

			$this->error_text = $this->l->alert_text( "danger", $error, DO_BACK, $this->get_parameter_failure() );
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file to his location. Sourcepath: " . $tmp . ", destinationpath: " . DIRROOT . $destination );
		}
	}

	protected function get_picture_stats( $path, $use_dirroot = true ) {
		if ( $use_dirroot ) {
			$dirroot = DIRROOT;
		} else {
			$dirroot = "";
		}

		return getimagesize( $dirroot . $path );
	}

	protected function check_given_data() {
		$content = "";

		if ( empty( $this->uploaddir ) ) {
			if ( $this->is_sub_window ) {
				$GLOBALS['body_footer'] .= "
                <script>
                function window_close()
                {
                    parent.$.fn.colorbox.close();
                }
                </script>\n";

				$content .= $this->l->alert_text( "danger", $this->txt( 'NO_UPLOAD_DIR' ) . " " . $this->l->button( $this->txt( 'CLOSE_WINDOW' ), "onclick='window_close();'" ) . "" );
			} else {
				$content .= $this->l->alert_text( "danger", $this->txt( 'NO_UPLOAD_DIR' ) );
			}
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "Missing Name of upload directory" );
			$this->error      = true;
			$this->error_text = $content;

			return;
		}

		if ( empty( $this->target ) ) {
			if ( $this->is_sub_window ) {
				$GLOBALS['body_footer'] .= "
                <script>
                function window_close()
                {
                    parent.$.fn.colorbox.close();
                }
                </script>\n";

				$content .= $this->l->alert_text( "danger", $this->txt( 'NO_DATA_DESTINATION' ) . " " . $this->l->button( $this->txt( 'CLOSE_WINDOW' ), "onclick='window_close();'" ) . "" );
			} else {
				$content .= $this->l->alert_text( "danger", $this->txt( 'NO_DATA_DESTINATION' ) );
			}
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "No destination given" );
			$this->error      = true;
			$this->error_text = $content;

			return;
		}
	}

	protected function get_var_for_html( $var ) {
		if ( $var === false ) {
			return 0;
		}
		if ( $var === true ) {
			return 1;
		}
		if ( $var === 0 ) {
			return 0;
		}
		if ( $var === "0" ) {
			return 0;
		}
		if ( ! isset( $var ) ) {
			return "";
		}
		if ( empty( $var ) ) {
			return "";
		}

		return $var;
	}

	protected function html_picture_link_enabled() {
		if ( $this->insert_type == EDITOR_SIMPLE || $this->insert_type == EDITOR_ENHANCED || $this->insert_type == EDITOR_CODE ) {
			return true;
		} else {
			return false;
		}
	}

	protected function store_in_db() {
		if ( ! $this->store_in_db ) {
			return true;
		}


		if ( ! isset( $this->file_name_original_checked ) || empty( $this->file_name_original_checked ) || ! isset( $this->file_name_checked ) || empty( $this->file_name_checked ) ) {
			return false;
		}


		$result = $this->db->ins( __FILE__ . ":" . __LINE__, "images_resize_helper", array(
			"filename_copy",
			"filename_original",
			"uploaddir",
			"crop_top",
			"crop_bottom",
			"crop_left",
			"crop_right",
			"created"
		),
			array(
				$this->file_name_checked,
				$this->file_name_original_checked,
				$this->uploaddir,
				$this->crop_top,
				$this->crop_bottom,
				$this->crop_left,
				$this->crop_right,
				"NOW()"
			) );

		return $result;

	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////

	protected function set_from_post() {
		$this->init( array( "insert_type" => $this->p->get( "insert_type", $this->insert_type ) ) );
		$this->init( array( "target" => $this->p->get( "target", $this->target ) ) );
		$this->init( array( "form_id" => $this->p->get( "form_id", $this->form_id ) ) );
		$this->init( array( "do_submit" => $this->p->get( "do_submit", $this->do_submit ) ) );
		$this->init( array( "do_resize" => $this->p->get( "do_resize", $this->do_resize ) ) );
		$this->init( array( "keep_ratio" => $this->p->get( "keep_ratio", $this->keep_ratio ) ) );
		$this->init( array( "autoresize" => $this->p->get( "autoresize", $this->autoresize ) ) );
		$this->init( array( "auto_max_x" => $this->p->get( "auto_max_x", $this->auto_max_x ) ) );
		$this->init( array( "auto_max_y" => $this->p->get( "auto_max_y", $this->auto_max_y ) ) );
		$this->init( array( "uploaddir" => $this->p->get( "uploaddir", $this->uploaddir ) ) );
		$this->init( array( "is_sub_window" => $this->p->get( "is_sub_window", $this->is_sub_window ) ) );
		$this->init( array( "hint" => $this->p->get( "hint", $this->hint ) ) );
		$this->init( array( "do" => $this->p->get( "do", $this->do ) ) );
		$this->init( array( "do_back" => $this->p->get( "do_back", $this->do_back ) ) );
		$this->init( array( "keep_original_picture" => $this->p->get( "keep_original_picture", $this->keep_original_picture ) ) );
		$this->init( array( "uploadsubdir_orig" => $this->p->get( "uploadsubdir_orig", $this->uploadsubdir_orig ) ) );
		$this->init( array( "one_step" => $this->p->get( "one_step", $this->one_step ) ) );
		$this->init( array( "element_number" => $this->p->get( "element_number", $this->element_number ) ) );
		$this->init( array( "element_is_image" => $this->p->get( "element_is_image", $this->element_is_image ) ) );
		$this->init( array( "store_in_db" => $this->p->get( "store_in_db", $this->store_in_db ) ) );

		$this->html_picture_link_type     = $this->p->get( "html_picture_link_type", $this->html_picture_link_type );
		$this->picture_alt                = $this->p->get( "picture_alt", $this->picture_alt );
		$this->crop_left                  = $this->p->get( "crop_left", $this->crop_left );
		$this->crop_right                 = $this->p->get( "crop_right", $this->crop_right );
		$this->crop_top                   = $this->p->get( "crop_top", $this->crop_top );
		$this->crop_bottom                = $this->p->get( "crop_bottom", $this->crop_bottom );
		$this->x_size                     = $this->p->get( "x_size", $this->x_size );
		$this->y_size                     = $this->p->get( "y_size", $this->y_size );
		$this->x_size_new                 = $this->p->get( "x_size_new", $this->x_size_new );
		$this->y_size_new                 = $this->p->get( "y_size_new", $this->y_size_new );
		$this->file_name_checked          = $this->p->get( "file_name_checked", $this->file_name_checked );
		$this->file_name_original_checked = $this->p->get( "file_name_original_checked", $this->file_name_original_checked );
		$this->proportional               = $this->p->get( "proportional", $this->proportional );
		$this->html_image_center          = $this->p->get( "html_image_center", $this->html_image_center );
		$this->html_image_responsive      = $this->p->get( "html_image_responsive", $this->html_image_responsive );

		$this->init( $this->init_array_saved );
	}

	protected function get_input_hidden_upload() {
		$content = "";
		$content .= $this->get_input_hidden_edit();
		$content .= $this->l->hidden( "html_image_responsive", $this->get_var_for_html( $this->html_image_responsive ) );
		$content .= $this->l->hidden( "html_image_center", $this->get_var_for_html( $this->html_image_center ) );

		return $content;
	}


	protected function get_input_hidden_edit() {
		$content = "";
		$content .= $this->l->hidden( "insert_type", $this->get_var_for_html( $this->insert_type ) );
		$content .= $this->l->hidden( "target", $this->get_var_for_html( $this->target ) );
		$content .= $this->l->hidden( "form_id", $this->get_var_for_html( $this->form_id ) );
		$content .= $this->l->hidden( "do_submit", $this->get_var_for_html( $this->do_submit ) );
		$content .= $this->l->hidden( "do_resize", $this->get_var_for_html( $this->do_resize ) );
		$content .= $this->l->hidden( "keep_ratio", $this->get_var_for_html( $this->keep_ratio ) );
		$content .= $this->l->hidden( "autoresize", $this->get_var_for_html( $this->autoresize ) );
		$content .= $this->l->hidden( "auto_max_x", $this->get_var_for_html( $this->auto_max_x ) );
		$content .= $this->l->hidden( "auto_max_y", $this->get_var_for_html( $this->auto_max_y ) );
		$content .= $this->l->hidden( "uploaddir", $this->get_var_for_html( $this->uploaddir ) );
		$content .= $this->l->hidden( "is_sub_window", $this->get_var_for_html( $this->is_sub_window ) );
		$content .= $this->l->hidden( "hint", $this->get_var_for_html( $this->hint ) );
		$content .= $this->l->hidden( "do", $this->get_var_for_html( $this->do ) );
		$content .= $this->l->hidden( "do_back", $this->get_var_for_html( $this->do_back ) );
		$content .= $this->l->hidden( "keep_original_picture", $this->get_var_for_html( $this->keep_original_picture ) );
		$content .= $this->l->hidden( "uploadsubdir_orig", $this->get_var_for_html( $this->uploadsubdir_orig ) );
		$content .= $this->l->hidden( "one_step", $this->get_var_for_html( $this->one_step ) );
		$content .= $this->l->hidden( "element_number", $this->get_var_for_html( $this->element_number ) );
		$content .= $this->l->hidden( "element_is_image", $this->get_var_for_html( $this->element_is_image ) );
		$content .= $this->l->hidden( "store_in_db", $this->get_var_for_html( $this->store_in_db ) );

		return $content;
	}

	protected function get_parameter_fixed() {
		return "insert_type=" . $this->get_var_for_html( $this->insert_type ) . "&
        target=" . $this->get_var_for_html( $this->target ) . "&
        form_id=" . $this->get_var_for_html( $this->form_id ) . "&
        do_submit=" . $this->get_var_for_html( $this->do_submit ) . "&
        do_resize=" . $this->get_var_for_html( $this->do_resize ) . "&
        keep_ratio=" . $this->get_var_for_html( $this->keep_ratio ) . "&
        autoresize=" . $this->get_var_for_html( $this->autoresize ) . "&
        auto_max_x=" . $this->get_var_for_html( $this->auto_max_x ) . "&
        auto_max_y=" . $this->get_var_for_html( $this->auto_max_y ) . "&
        uploaddir=" . $this->get_var_for_html( $this->uploaddir ) . "&
        is_sub_window=" . $this->get_var_for_html( $this->is_sub_window ) . "&
        hint=" . $this->get_var_for_html( $this->hint ) . "&
        keep_original_picture=" . $this->get_var_for_html( $this->keep_original_picture ) . "&
        uploadsubidr_orig=" . $this->get_var_for_html( $this->uploadsubdir_orig ) . "&
        one_step=" . $this->get_var_for_html( $this->one_step ) . "&
        element_number=" . $this->get_var_for_html( $this->element_number ) . "&
        element_is_image=" . $this->get_var_for_html( $this->element_is_image ) . "&
        store_in_db=" . $this->get_var_for_html( $this->store_in_db ) . "&
        html_image_center=" . $this->get_var_for_html( $this->html_image_center ) . "&
        html_image_responsive=" . $this->get_var_for_html( $this->html_image_responsive );
	}

	protected function get_parameter_failure() {
		return $this->get_parameter_fixed() . "&do=" . $this->get_var_for_html( $this->do_back ) . "&
        do_back=" . $this->get_var_for_html( $this->do_back );
	}

	protected function get_parameter() {
		return $this->get_parameter_fixed() . "&do=" . $this->get_var_for_html( $this->do ) . "&
        do_back=" . $this->get_var_for_html( $this->do_back );
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function init( $array, $main_init = false ) {
		if ( $main_init ) {
			$this->init_array_saved = $array;
		}

		foreach ( $array as $key => $value ) {
			if ( array_key_exists( $key, $this->init_array ) ) {
				$this->$key = $value;
			} else {
				$error = $this->txt( "INIT_PARAMETER_DOES_NOT_EXIST" );
				$this->log->error( "image", __FILE__ . ":" . __LINE__, $error . " key: '$key', value: '$value'" );
			}
		}

	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function show_upload_page() {
		$content = "";

		$this->set_from_post();

		$this->check_given_data();
		if ( $this->error ) {
			return $this->error_text;
		}

		$content .= $this->l->form_admin_file();
		$content .= $this->get_input_hidden_upload();
		$content .= "
    <h3>" . $this->txt( 'PICTURE_UPLOAD' ) . "</h3>
    " . $this->l->table() . "
    <tr>
        <td>" . $this->txt( 'TBL_SELECT_THE_FILE' ) . "
        </td>
        <td>" . $this->l->file( "file_to_upload", "300" ) . "
        </td>
    </tr>
    <tr>
        <td colspan=\"2\"><p>" . $this->txt( 'MAXIMUM_FILE_SIZE' ) . " " . $this->p->get_max_upload() . " " . $this->txt( 'MEGABYTE' ) . "</p>
        <p>" . $this->txt( 'ALLOWED_FORMATS_PICTURE' ) . "</p>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>" . $this->l->submit( $this->txt( 'UPLOAD' ) ) . "
        </td>
    </tr>
    </table>
    </form>";

		return $content;
	}

	public function show_edit_page() {

		$this->set_from_post();
		$content = "";

		$this->check_given_data();
		if ( $this->error ) {
			return $this->error_text;
		}

		// Überprüfe, ob das Bild vom Formular richtig übertragen wurde:
		if ( isset( $_FILES['file_to_upload'] ) && ! empty( $_FILES['file_to_upload']['name'] ) ) {

			$this->check_form_transfer_ok( $_FILES['file_to_upload']['error'], $_FILES['file_to_upload']['name'] );
			if ( $this->error ) {
				return $this->error_text;
			}

			$this->check_if_picture( $_FILES['file_to_upload']['name'] );
			if ( $this->error ) {
				return $this->error_text;
			}

			// Bestimme die Formatierung des Dateinamens:
			$file_to_upload_name     = $_FILES['file_to_upload']['name'];
			$file_to_upload_tmp_name = $_FILES['file_to_upload']['tmp_name'];

			$this->file_name_original_checked = $this->p->check_filename( $this->uploaddir . $this->uploadsubdir_orig . $file_to_upload_name );
			if ( $this->file_name_original_checked == false ) {
				$this->error_text = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $file_to_upload_name . "''";
				$this->error      = true;

				return $this->error_text;
			}

			$this->file_name_checked = $this->p->check_filename( $this->uploaddir . $file_to_upload_name );
			if ( $this->file_name_checked == false ) {
				$this->error_text = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                " . AL_FILENAME_NOT_ALLOWED . ": '" . $file_to_upload_name . "''";
				$this->error      = true;

				return $this->error_text;
			}


			$file_and_path_original_checked = $this->uploaddir . $this->uploadsubdir_orig . $this->file_name_original_checked;
			$file_and_path_checked          = $this->uploaddir . $this->file_name_checked;

			// Die Original Datei hochladen:
			$this->move_uploaded_file( $file_to_upload_tmp_name, $file_and_path_original_checked );
			if ( $this->error ) {
				return $this->error_text;
			}

			list( $image_width, $image_height, $image_type, $image_attr ) = $this->get_picture_stats( $file_and_path_original_checked );

			if ( ! $this->one_step ) {
				$GLOBALS['body_footer'] .= "<script>";

				$new_b = false;
				$new_h = false;

				if ( ! $this->autoresize ) {
					// No Autoresize, ratio optional:
					$GLOBALS['body_footer'] .= "
                                                        var xSizeOriginal = $image_width;
                                                        var ySizeOriginal = $image_height;
                                               ";
				} else {
					// Autoresize, with keep ratio:
					if ( $image_width > $image_height ) {
						$ratio = $image_width / $image_height;

						$new_b = $this->auto_max_x;
						$new_h = round( $new_b / $ratio );
					} else {
						$ratio = $image_height / $image_width;

						$new_h = $this->auto_max_y;
						$new_b = round( $new_h / $ratio );
					}

					if ( $new_h > $this->auto_max_y ) {
						$ratio = $image_height / $image_width;

						$new_h = $this->auto_max_y;
						$new_b = round( $new_h / $ratio );
					} elseif ( $new_b > $this->auto_max_x ) {
						$ratio = $image_width / $image_height;

						$new_b = $this->auto_max_x;
						$new_h = round( $new_b / $ratio );
					}

					$GLOBALS['body_footer'] .= "
                                                        var xSizeOriginal = " . $new_b . ";
                                                        var ySizeOriginal = " . $new_h . ";
                                                ";
				}

				$GLOBALS['body_footer'] .= "                            var imgPreview = document.getElementById('imgPreview');";

				// Javascript Size Change
				$GLOBALS['body_footer'] .= "
                                                        function changeX(formID)
                                                        {
                                                                if(formID.proportional.checked)
                                                                {
                                                                    var ratio = xSizeOriginal / ySizeOriginal;
                                                                    var yNewSize    = Math.round(formID.x_size.value / ratio);
                                                                    formID.y_size.value = yNewSize;
                                                                    formID.y_size_new.value = yNewSize - formID.crop_top.value - formID.crop_bottom.value;
                                                                    imgPreview.style.height = yNewSize + 'px';
                                                                }
                                                                else
                                                                {
                                                                    imgPreview.style.height = formID.y_size.value + 'px';
                                                                }
                                                                imgPreview.style.width = formID.x_size.value + 'px';
                                                                formID.x_size_new.value = formID.x_size.value - formID.crop_left.value - formID.crop_right.value;
                                                        }

                                                        function changeY(formID)
                                                        {
                                                                if(formID.proportional.checked)
                                                                {
                                                                    var ratio = ySizeOriginal / xSizeOriginal;
                                                                    var xNewSize    = Math.round(formID.y_size.value / ratio);
                                                                    formID.x_size.value = xNewSize;
                                                                    formID.x_size_new.value = xNewSize - formID.crop_left.value - formID.crop_right.value;
                                                                    imgPreview.style.width = xNewSize + 'px';
                                                                }
                                                                else
                                                                {
                                                                    imgPreview.style.width = formID.x_size.value + 'px';
                                                                }
                                                               imgPreview.style.height = formID.y_size.value + 'px';
                                                               formID.y_size_new.value = formID.y_size.value - formID.crop_top.value - formID.crop_bottom.value;
                                                        }
                                                        ";
				// Javascript CROP
				$GLOBALS['body_footer'] .= "
                                                        function changeTop(formID)
                                                        {
                                                                formID.y_size_new.value = formID.y_size.value - formID.crop_top.value - formID.crop_bottom.value;
                                                                document.getElementById('crop_top').style.height = formID.crop_top.value + 'px';
                                                        }

                                                        function changeBottom(formID)
                                                        {
                                                                formID.y_size_new.value = formID.y_size.value - formID.crop_top.value - formID.crop_bottom.value;
                                                                document.getElementById('crop_bottom').style.height = formID.crop_bottom.value + 'px';
                                                        }

                                                        function changeLeft(formID)
                                                        {
                                                                formID.x_size_new.value = formID.x_size.value - formID.crop_left.value - formID.crop_right.value;
                                                                document.getElementById('crop_left').style.width = formID.crop_left.value + 'px';
                                                        }

                                                        function changeRight(formID)
                                                        {
                                                                formID.x_size_new.value = formID.x_size.value - formID.crop_left.value - formID.crop_right.value;
                                                                document.getElementById('crop_right').style.width = formID.crop_right.value + 'px';
                                                        }
                                                        ";
				// Javascript Counter
				$GLOBALS['body_footer'] .= "
                                                        function changeValueUp(elementID, step)
                                                        {
                                                                var value;
                                                                value = parseInt(elementID.value);
                                                                elementID.value = value + step;
                                                                elementID.onkeyup();
                                                        }
                                                        function changeValueDown(elementID, step)
                                                        {
                                                                var value;
                                                                value = parseInt(elementID.value);
                                                                if(step > value)
                                                                    step = value;
                                                                if(value==0) return;
                                                                elementID.value = value - step;
                                                                elementID.onkeyup();
                                                        }

                                                        formID = document.forms[0];
                                                        ";

				$GLOBALS['body_footer'] .= "
                                                        changeX(formID);
                                                        changeY(formID);
                                                        ";
				$GLOBALS['body_footer'] .= "
                                                        changeTop(formID);
                                                        changeBottom(formID);
                                                        changeLeft(formID);
                                                        changeRight(formID);
                                                        </script>\n";

				if ( $this->html_picture_link_enabled() ) {
					$using = "<tr><td>" . $this->p->txt( 'TBL_TYPE' ) . "</td><td>" . $this->l->select( "html_picture_link_type" ) . "
                                                                <option value=\"0\"";
					$using .= ">" . $this->p->txt( 'DEFAULT_PICTURE' ) . "</option>
                                                                <option value=\"1\"";
					$using .= ">" . $this->p->txt( 'PREVIEW_PICTURE' ) . "</option>
                                                                </select></td></tr>\n";

					$alt = "    <tr>
                                                        <td>Alternativer Text:</td>
                                                        <td>" . $this->l->text( "picture_alt", "", "400" ) . " (Optional)
                                                        </td>
                                                        </tr>\n";
				} else {
					$using = "";
					$alt   = "";
				}

				$content .= $this->l->form_admin() . $this->l->keep_form_ids() . $this->get_input_hidden_edit();

				$content .= $this->l->hidden( "file_name_checked", $this->file_name_checked );
				$content .= $this->l->hidden( "file_name_original_checked", $this->file_name_original_checked );

				$input_width = 65;
				$content .= "<div class='row'><div class='col-sm-5'>" . $this->l->table() . $using;

				if ( ! empty( $this->hint ) ) {
					$content .= "<tr><td>" . $this->txt( 'TBL_HINT' ) . "</td><td>" . $this->hint . "</td></tr>";
				}

				if ( $this->html_picture_link_enabled() ) {
					$content .= "<tr><td>" . $this->txt( 'TBL_PROPERTIES' ) . "</td><td>" . $this->l->checkbox( "html_image_center", 1, $this->html_image_center ) . " " . $this->txt( 'CENTER_IMAGE_HORIZONT' ) . "
                " . $this->l->checkbox( "html_image_responsive", 1, $this->html_image_responsive ) . " " . $this->txt( 'PICTURE_IS_RESPONSIVE' ) . " </td></tr>";
				} else {
					$content .= $this->l->hidden( "html_image_center", 0 ) . $this->l->hidden( "html_image_responsive", 0 );
				}

				if ( ! $this->autoresize ) {
					// Resize
					$content .= "   <tr>
                                                                <td class='v-align'>" . $this->txt( 'TBL_SIZE' ) . "
                                                                </td>
                                                                <td class='v-align'><nobr>" .
					            $this->l->button( "--", "onclick=\"changeValueDown(this.form.x_size, 10)\"" ) .
					            $this->l->button( "-", "onclick=\"changeValueDown(this.form.x_size, 1)\"" ) . " " .
					            $this->l->text( "x_size", $image_width, $input_width, "onkeyup=\"changeX(this.form)\"" ) . " " .
					            $this->l->button( "+", "onclick=\"changeValueUp(this.form.x_size, 1)\"" ) .
					            $this->l->button( "++", "onclick=\"changeValueUp(this.form.x_size, 10)\"" ) . " " . $this->txt( 'WIDTH' ) . "</nobr><br /><nobr>" .
					            $this->l->button( "--", "onclick=\"changeValueDown(this.form.y_size, 10)\"" ) .
					            $this->l->button( "-", "onclick=\"changeValueDown(this.form.y_size, 1)\"" ) . " " .
					            $this->l->text( "y_size", $image_height, $input_width, "onkeyup=\"changeY(this.form)\"" ) . " " .
					            $this->l->button( "+", "onclick=\"changeValueUp(this.form.y_size, 1)\"" ) .
					            $this->l->button( "++", "onclick=\"changeValueUp(this.form.y_size, 10)\"" ) . " " . $this->txt( 'HEIGHT_IN_PIXELS' ) . "</nobr>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                               <td class='v-align'>" . $this->txt( 'PROPORTIONAL_QUE' ) . "</td>
                                                               <td class='v-align'>" . $this->l->checkbox( "proportional", "1", true, 0, "", false ) . " </td>
                                                            </tr>
                                                            ";
				} else {
					$content .= $this->l->checkbox( "proportional", "1", true, 0, "style='display:none;'", false );
					$content .= $this->l->hidden( "x_size", $new_b ) . $this->l->hidden( "y_size", $new_h );
				}
				$content .= "
                                                        <tr>
                                                        <td class='v-align'>" . $this->txt( 'TBL_CUT' );
				// Crop
				$content .= "</td>
                                                        <td class='v-align'><table style='border: none'>
                                                            <tr><td class='v-align'><nobr>" .
				            $this->l->button( "--", "onclick=\"changeValueDown(this.form.crop_top, 10)\"" ) .
				            $this->l->button( "-", "onclick=\"changeValueDown(this.form.crop_top, 1)\"" ) . " " .
				            $this->l->text( "crop_top", 0, $input_width, "onkeyup=\"changeTop(this.form)\"" ) . " " .
				            $this->l->button( "+", "onclick=\"changeValueUp(this.form.crop_top, 1)\"" ) .
				            $this->l->button( "++", "onclick=\"changeValueUp(this.form.crop_top, 10)\"" ) . " " . $this->txt( 'PIXEL' ) . " " . $this->txt( "FROM_TOP" ) . "</nobr></td></tr>
                                                            <tr><td class='v-align'><nobr>" .
				            $this->l->button( "--", "onclick=\"changeValueDown(this.form.crop_right, 10)\"" ) .
				            $this->l->button( "-", "onclick=\"changeValueDown(this.form.crop_right, 1)\"" ) . " " .
				            $this->l->text( "crop_right", 0, $input_width, "onkeyup=\"changeRight(this.form)\"" ) . " " .
				            $this->l->button( "+", "onclick=\"changeValueUp(this.form.crop_right, 1)\"" ) .
				            $this->l->button( "++", "onclick=\"changeValueUp(this.form.crop_right, 10)\"" ) . " " . $this->txt( 'PIXEL' ) . " " . $this->txt( "FROM_RIGHT" ) . "</nobr></td></tr>
                                                            <tr><td class='v-align'><nobr>" .
				            $this->l->button( "--", "onclick=\"changeValueDown(this.form.crop_bottom, 10)\"" ) .
				            $this->l->button( "-", "onclick=\"changeValueDown(this.form.crop_bottom, 1)\"" ) . " " .
				            $this->l->text( "crop_bottom", 0, $input_width, "onkeyup=\"changeBottom(this.form)\"" ) . " " .
				            $this->l->button( "+", "onclick=\"changeValueUp(this.form.crop_bottom, 1)\"" ) .
				            $this->l->button( "++", "onclick=\"changeValueUp(this.form.crop_bottom, 10)\"" ) . " " . $this->txt( 'PIXEL' ) . " " . $this->txt( "FROM_BOTTOM" ) . "</nobr></td></tr>
                                                            <tr><td class='v-align'><nobr>" .
				            $this->l->button( "--", "onclick=\"changeValueDown(this.form.crop_left, 10)\"" ) .
				            $this->l->button( "-", "onclick=\"changeValueDown(this.form.crop_left, 1)\"" ) . " " .
				            $this->l->text( "crop_left", 0, $input_width, "onkeyup=\"changeLeft(this.form)\"" ) . " " .
				            $this->l->button( "+", "onclick=\"changeValueUp(this.form.crop_left, 1)\"" ) .
				            $this->l->button( "++", "onclick=\"changeValueUp(this.form.crop_left, 10)\"" ) . " " . $this->txt( 'PIXEL' ) . " " . $this->txt( "FROM_LEFT" ) . "</nobr></td></tr>


                                                            </table>\n";
				// New Size Readonly
				if ( ! $this->autoresize ) {
					$content .= "
                                                            <tr>
                                                                <td class='v-align'>" . $this->txt( 'TBL_NEW_SIZE' ) . "</td>
                                                                <td class='v-align'><nobr>" .
					            $this->l->text( "x_size_new", $image_width, $input_width, "readonly=\"readonly\"" ) . " " . $this->txt( 'WIDTH' ) . " " .
					            $this->l->text( "y_size_new", $image_height, $input_width, "readonly=\"readonly\"" ) . "</nobr><br /><nobr>" .
					            $this->txt( 'HEIGHT_IN_PIXELS' ) . "</nobr></td>
                                                            </tr>";
				} else {
					$content .= $this->l->hidden( "x_size_new", 0 ) . $this->l->hidden( "y_size_new", 0 );
				}
				$content .= "
                                                        $alt
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td class='v-align'>" .
				            $this->l->submit( $this->txt( 'COMPLETE_EDITING' ) ) . " " . $this->l->button( $this->txt( 'BACK' ), "onclick='back(this.form)'" ) . "
                                                            </td>
                                                        </tr>
                                                        </table>
                                                        </div>
                                                        </form>
                                                        <div class='col-sm-7'>
                                                        <h3>" . $this->txt( 'PREVIEW_COLON' ) . "</h3>
                                                        <div id='placeholder' style='position:relative; display:inline-block;'>
                                                        <div id='crop_top' style='position: absolute; top:0; left:0; height: 0; width:100%; background:#000000 url(\"/" . $this->loc->web_root . ADMINDIR . $GLOBALS['layout_d'] . "img/pic_crop.jpg\");'></div>
                                                        <div id='crop_bottom' style='position: absolute; bottom:0; left:0; height: 0; width:100%; background:#000000 url(\"/" . $this->loc->web_root . ADMINDIR . $GLOBALS['layout_d'] . "img/pic_crop.jpg\");'></div>
                                                        <div id='crop_left' style='position: absolute; top:0; left:0; height: 100%; width:0; background:#000000 url(\"/" . $this->loc->web_root . ADMINDIR . $GLOBALS['layout_d'] . "img/pic_crop.jpg\");'></div>
                                                        <div id='crop_right' style='position: absolute; top:0; right:0; height: 100%; width:0; background:#000000 url(\"/" . $this->loc->web_root . ADMINDIR . $GLOBALS['layout_d'] . "img/pic_crop.jpg\");'></div>
                                                        <img src=\"/" . $this->loc->web_root . $file_and_path_original_checked . "\" id=\"imgPreview\">

                                                        </div></div></div>
                                                        ";

				$GLOBALS['body_footer'] .= "
                                                        <script>
                                                        function back(formID)
                                                        {
                                                            formID.do.value='back';
                                                            formID.submit();
                                                        }
                                                        </script>
                                                        ";

				return $content;
			} else {
				// One Step

				// Autoresize
				// If resized it will copy from orignal to destination
				if ( $this->autoresize ) {
					$result = $this->resize( $this->file_name_original_checked, $this->uploaddir . $this->uploadsubdir_orig, $this->auto_max_x, $this->auto_max_y, $this->keep_ratio, NO_PREFIX, SINGLE_CALL, $this->uploaddir, $this->file_name_checked );
					if ( $this->error ) {
						return $result;
					}
				}

				if ( ! $this->resized_picture ) {
					$result = $this->p->copy_file( $file_and_path_original_checked, $file_and_path_checked );
					if ( ! $result ) {
						$this->error      = true;
						$this->error_text = $this->l->alert_text( "danger", $this->txt( 'ERROR_CANT_COPY_FILE' ), DO_BACK, $this->get_parameter_failure() );
						$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't copy file from '" . DIRROOT . $file_and_path_original_checked . "' to '" . DIRROOT . $file_and_path_checked . "'" );

						return $this->error_text;
					}
				}

				if ( ! $this->keep_original_picture && is_file( DIRROOT . $file_and_path_original_checked ) ) {
					$result = unlink( DIRROOT . $file_and_path_original_checked );
					if ( $result === false ) {
						$this->log->notice( "file", __FILE__ . ":" . __LINE__, "Can't delete file: '" . $file_and_path_original_checked . "'" );
					}
				}


				$result = $this->insert_code( $this->file_name_checked, $this->uploaddir );

				if ( $result === false ) {
					return $this->error_text;
				}

				if ( ! $this->error ) {
					//$this->close_colorbox();
				}

				return $result;
			}
		} else {
			$this->error = true;

			$error            = $this->txt( 'ERROR_FILE_NOT_CORRECT_TRANSFERED_NO_NAME_QUE' );
			$this->error_text = $this->l->alert_text( "danger", $error, DO_BACK, $this->get_parameter_failure() );
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "No filename given" );

			return $this->error_text;
		}
	}


	public function save() {
		$content = "";

		if ( $this->one_step ) {
			$this->close_colorbox();

			return true;
		}

		$this->set_from_post();

		$prefix = "";
		if ( $this->html_picture_link_type == 1 ) {
			$prefix = $this->preview_prefix;
		}

		if ( $this->autoresize ) {
			$this->x_size = $this->auto_max_x;
			$this->y_size = $this->auto_max_y;
		}

		if ( empty( $this->uploaddir ) ) {
			if ( $this->is_sub_window ) {
				$GLOBALS['body_footer'] .= "<script>function window_close()
            {
                parent.$.fn.colorbox.close();
            }
            </script>\n";

				$content .= $this->l->alert_text( "danger", $this->txt( 'NO_UPLOAD_DIR' ) . " " . $this->l->button( $this->txt( 'CLOSE_WINDOW' ), "onclick='window_close();'" ) . "" );
			} else {
				$content .= $this->l->alert_text( "danger", $this->txt( 'NO_UPLOAD_DIR' ) );
			}

			$this->log->error( "value", __FILE__ . ":" . __LINE__, "Missing Name of upload directory" );

			return $content;
		}

		if ( ! is_numeric( $this->x_size ) || ! is_numeric( $this->y_size ) ) {
			$error = $this->txt( 'PIXELS_NOT_NUMERIC' );
			$content .= $this->l->alert_text( "danger", $error, DO_BACK, $this->get_parameter_failure() );
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "Value not numeric, x_size: '" . $this->x_size . "', y_size: '" . $this->y_size . "'" );

			return $content;
		}
		if ( ! is_numeric( $this->crop_top ) || ! is_numeric( $this->crop_bottom ) || ! is_numeric( $this->crop_left ) || ! is_numeric( $this->crop_right ) ) {

			$error = $this->txt( 'PIXELS_NOT_NUMERIC' );
			$content .= $this->l->alert_text( "danger", $error, DO_BACK, $this->get_parameter_failure() );
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "Value not numeric" );

			return $content;
		}
		$x_size_new = $this->x_size - $this->crop_left - $this->crop_right;
		$y_size_new = $this->y_size - $this->crop_top - $this->crop_bottom;

		$file_and_path_original_checked    = $this->uploaddir . $this->uploadsubdir_orig . $this->file_name_original_checked;
		$file_and_path_checked             = $this->uploaddir . $this->file_name_checked;
		$file_and_path_checked_with_prefix = $this->uploaddir . $this->preview_prefix . $this->file_name_checked;

		if ( $this->html_picture_link_type == 1 ) {
			$result = $this->p->copy_file( $file_and_path_original_checked, $file_and_path_checked_with_prefix );
			if ( ! $result ) {
				$this->error      = true;
				$this->error_text = $this->l->alert_text( "danger", $this->txt( 'ERROR_CANT_COPY_FILE' ), DO_BACK, $this->get_parameter_failure() );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can,t copy file from '" . DIRROOT . $this->uploaddir . $this->uploadsubdir_orig . $this->file_name_original_checked . "' to '" . DIRROOT . $this->uploaddir . $prefix . $this->file_name_checked . "'" );

				return $this->error_text;
			}
		}
		$result = $this->p->copy_file( $file_and_path_original_checked, $file_and_path_checked );
		if ( ! $result ) {
			$this->error      = true;
			$this->error_text = $this->l->alert_text( "danger", $this->txt( 'ERROR_CANT_COPY_FILE' ), DO_BACK, $this->get_parameter_failure() );
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can,t copy file from '" . DIRROOT . $this->uploaddir . $this->uploadsubdir_orig . $this->file_name_original_checked . "' to '" . DIRROOT . $this->uploaddir . $prefix . $this->file_name_checked . "'" );

			return $this->error_text;
		}
		if ( ! $this->keep_original_picture && is_file( DIRROOT . $file_and_path_original_checked ) ) {
			$result_unlink = unlink( DIRROOT . $file_and_path_original_checked );
			if ( $result_unlink === false ) {
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cann't delete file: '" . DIRROOT . $file_and_path_original_checked . "'" );
			}
		}

		// CROP
		$result = $this->crop_with_optional_preview( $this->uploaddir, $this->file_name_checked, $this->x_size, $this->y_size, $this->crop_top, $this->crop_right, $this->crop_bottom, $this->crop_left, $prefix, $this->proportional );
		if ( ! empty( $result ) ) {
			return $result;
		}

		//var_dump($this->uploaddir, $this->file_name_checked, $this->x_size, $this->y_size, $this->crop_top, $this->crop_right, $this->crop_bottom, $this->crop_left, $prefix, $this->proportional);
		//die();

		// RESIZE
		$result = $this->resize( $prefix . $this->file_name_checked, $this->uploaddir, $x_size_new, $y_size_new, $this->keep_ratio, NO_PREFIX, SINGLE_CALL );
		if ( ! empty( $result ) ) {
			return $result;
		}

		$this->log->event( "file", __FILE__ . ":" . __LINE__, "Picture Upload finished. Name: " . $this->uploaddir . $this->file_name_checked );


		$result = $this->insert_code( $this->file_name_checked, $this->uploaddir );

		if ( $result === false ) {
			if ( is_file( DIRROOT . $file_and_path_original_checked ) ) {
				$result_unlink = unlink( DIRROOT . $file_and_path_original_checked );
				if ( $result_unlink === false ) {
					$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cann't delete file: '" . DIRROOT . $file_and_path_original_checked . "'" );
				}
			}
			if ( is_file( DIRROOT . $file_and_path_checked ) ) {
				$result_unlink = unlink( DIRROOT . $file_and_path_checked );
				if ( $result_unlink === false ) {
					$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cann't delete file: '" . DIRROOT . $file_and_path_checked . "'" );
				}
			}
			if ( is_file( DIRROOT . $file_and_path_checked_with_prefix ) ) {
				$result_unlink = unlink( DIRROOT . $file_and_path_checked_with_prefix );
				if ( $result_unlink === false ) {
					$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cann't delete file: '" . DIRROOT . $file_and_path_checked_with_prefix . "'" );
				}
			}

			return $this->error_text;
		} else {
			$this->store_in_db();
		}

		if ( ! $this->error ) {
			//$this->close_colorbox();
			return true;
		}

		return $result;
	}


	protected function insert_code( $picture, $dir ) {

		$additional_css_class = "";
		$additional_style     = "";
		if ( ! $this->html_image_responsive ) {
			$additional_css_class .= " img-not-responsive";
			$additional_style = "";
		}
		if ( $this->html_image_center ) {
			$additional_css_class .= " img-center";
			$additional_style = "";
		}

		switch ( $this->insert_type ) {
			// EDITOR SIMPLE
			case 0:
				if ( ! isset( $this->html_picture_link_type ) || $this->html_picture_link_type == 0 || empty( $this->html_picture_link_type )
				) {
					$text = "<img src=\"/" . $this->loc->web_root . $dir . $picture . "\" alt=\"" . $this->picture_alt . "\" style=\"$additional_style\"  class=\"img$additional_css_class\">";
				} else {
					$text = "<a href=\"/" . $this->loc->web_root . $dir . $picture . "\" target=\"_blank\"><img src=\"/" . $this->loc->web_root . $dir . $this->preview_prefix . $picture . "\" alt=\"" . $this->picture_alt . "\" style=\"$additional_style\"  class=\"img$additional_css_class\"></a>";
				}
				$this->p->paste_rangy( $this->target, $text );

				return true;
				break;
			// EDITOR ENHANCED
			case 1:
				if ( ! isset( $this->html_picture_link_type ) || $this->html_picture_link_type == 0 || empty( $this->html_picture_link_type )
				) {
					$text = "<img src=\"/" . $this->loc->web_root . $dir . $picture . "\" alt=\"" . $this->picture_alt . "\" style=\"$additional_style\" class=\"img$additional_css_class\">";
				} else {
					$text = "<a href=\"/" . $this->loc->web_root . $dir . $picture . "\" target=\"_blank\"><img src=\"/" . $this->loc->web_root . $dir . $this->preview_prefix . $picture . "\" alt=\"" . $this->picture_alt . "\" style=\"$additional_style\" class=\"img$additional_css_class\"></a>";
				}
				$this->p->paste_tinymce( $this->target, $text, NO_JS_MODE, DO_COLORBOX_CLOSE, "parent" );

				return true;
				break;
			// EDITOR CODE
			case 2:
				if ( ! isset( $this->html_picture_link_type ) || $this->html_picture_link_type == 0 || empty( $this->html_picture_link_type )
				) {
					$text = "<img src=\"/" . $this->loc->web_root . $dir . $picture . "\" alt=\"" . $this->picture_alt . "\" style=\"$additional_style\" class=\"img$additional_css_class\">";
				} else {
					$text = "<a href=\"/" . $this->loc->web_root . $dir . $picture . "\" target=\"_blank\"><img src=\"/" . $this->loc->web_root . $dir . $this->preview_prefix . $picture . "\" alt=\"" . $this->picture_alt . "\" style=\"$additional_style\" class=\"img$additional_css_class\"></a>";
				}
				$this->p->paste_codemirror( $this->target, $text );

				return true;
				break;
				break;
			// INPUT TEXT
			case 10:
				$this->p->paste_element( "/" . $this->loc->web_root . $dir . $picture, $this->target, $this->element_number, $this->form_id, $this->element_is_image, $picture );

				return true;
				break;
			// GET ARRAY
			case 11:
				$tmp              = array();
				$tmp['file']      = $picture;
				$tmp['uploaddir'] = $dir;
				$tmp['web_root']  = "/" . $this->loc->web_root;

				return $tmp;
				break;
			// UPLOAD ONLY
			case 99:
				return true;
				break;
			default:
				$this->error      = true;
				$this->error_text = $this->l->alert_text_dismiss( "danger", $this->txt( 'ERROR_WRONG_INSERT_TYPE' ) );
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong insert type: '" . $this->insert_type . "'" );

				return false;
		}
	}


}