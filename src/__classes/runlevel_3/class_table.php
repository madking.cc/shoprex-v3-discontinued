<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


class class_table extends class_layout {
	protected $log;
	protected $p;
	protected $l;

	protected $js_tbl_header = array();
	protected $js_tbl_code = array();

	protected $load_external;

	public function __construct() {
		parent::__construct();
		$this->log = class_logging::getInstance();

		$this->p = new class_page();

		$this->load_external              = class_load_external::getInstance();
		$this->load_external->load_js_tbl = true;

	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public function tbl_init() {


		$num_args = func_num_args();

		$content = "";

		if ( $num_args > 0 ) {

			$id = $this->p->unique_id( false );
			$content .= $this->table( "id='$id'" );
			$arg_list = func_get_args();

			if ( is_array( $arg_list[0] ) && $num_args == 1 ) {
				$arg_list = $arg_list[0];
				$num_args = sizeof( $arg_list );
			}

			$content .= "\n<thead><tr><th>Pos.:</th>\n";
			for ( $i = 0; $i < $num_args; $i ++ ) {
				if ( is_array( $arg_list[ $i ] ) ) {
					$array_length = sizeof( $arg_list[ $i ] );
					switch ( $array_length ) {
						case "2":
							if ( $arg_list[ $i ][1] == "nosort" ) {
								$content .= "<th>" . $arg_list[ $i ][0] . "</th>";
							} else {
								$content .= "<th><a id='tbl_head_sort_" . $i . "_" . $id . "' href='javascript:change_sort($i, \"$id\");'>" . $arg_list[ $i ][0] . "</a></th>";
							}
							break;
					}
				} else {
					$content .= "<th><a  id='tbl_head_sort_" . $i . "_" . $id . "' href='javascript:change_sort($i, \"$id\");'>" . $arg_list[ $i ] . "</a></th>";
				}
			}
			$content .= "</tr></thead>\n";

			$content .= "<tbody><tr><td colspan='" . ( $num_args + 2 ) . "'></td></tr></tbody>";

			$this->js_tbl_header[ $id ] = $content;

			$GLOBALS['foot_bottom'] .= "<script>
    window['arg_size_" . $id . "'] = " . $num_args . ";
</script>\n";

			return $id;
		} else {
			$this->p->log->error( "php", __FILE__ . ":" . __LINE__, "js_tbl_init function needs at least 1 argument" );

			return $content;
		}
	}

	public function tbl_load() {
		$num_args = func_num_args();

		$content = "";

		if ( $num_args > 0 ) {
			$arg_list = func_get_args();

			if ( is_array( $arg_list[0] ) && $num_args == 1 ) {
				$arg_list = $arg_list[0];
				$num_args = sizeof( $arg_list );
			}

			$id = $arg_list[0];

			$content .= " var sub_array = [];\n";
			for ( $i = 1; $i < $num_args; $i ++ ) {
				$arg1 = "";
				$arg2 = "";
				if ( is_array( $arg_list[ $i ] ) ) {
					$array_length = sizeof( $arg_list[ $i ] );
					switch ( $array_length ) {
						case "2":
							$value = $arg_list[ $i ][0];
							$arg1  = $arg_list[ $i ][1];
							break;
						case "3":
							$value = $arg_list[ $i ][0];
							$arg1  = $arg_list[ $i ][1];
							$arg2  = $arg_list[ $i ][2];
							break;
					}
				} else {
					$value = $arg_list[ $i ];
				}

				$value_out = $this->escape_single_quotation_marks( $value );
				$content .= $this->remove_line_breaks( " sub_array.push('" . $value_out . "');" ) . "\n";
				$arg1_out = $this->escape_single_quotation_marks( $arg1 );
				$content .= $this->remove_line_breaks( " sub_array.push('" . $arg1_out . "');" ) . "\n";
				if ( isset( $array_length ) && $array_length == 3 ) {
					$arg2 = "<nobr>" . $arg2 . "</br>";
				}
				$arg2_out = $this->escape_single_quotation_marks( $arg2 );
				$content .= $this->remove_line_breaks( " sub_array.push('" . $arg2_out . "');" ) . "\n";
			}

			$content .= " window['array_" . $id . "'].push(sub_array);\n";
			if ( ! isset( $this->js_tbl_code[ $id ] ) ) {
				$this->js_tbl_code[ $id ] = "";
			}
			$this->js_tbl_code[ $id ] .= $content . "\n";
		} else {
			$this->p->log->error( "php", __FILE__ . ":" . __LINE__, "js_tbl_load function needs at least 2 arguments" );

			return $content;
		}
	}

	public function tbl_out( $id, $init_sort = 0, $init_direction = 0 ) {
		if ( ! array_key_exists( $id, $this->js_tbl_code ) ) {
			$content = "<p>" . $this->txt( 'NO_DATA_AVAILABLE' ) . "</p>\n";

			return $content;
		}
		$class_external              = class_load_external::getInstance();
		$class_external->load_js_tbl = true;
		$class_external->load_cookie = true;

		$init_js_array = "window['array_" . $id . "'] = [];\n";

		$GLOBALS["body_footer"] .= "<script>
$(document).ready(function () {
$init_js_array
" . $this->js_tbl_code[ $id ] . "
var initSort = $init_sort;
var initDirection = $init_direction;

if($.cookie('initSort') != \"undefined\")
    initSort = $.cookie('initSort');
if($.cookie('initDirection') != \"undefined\")
    initDirection = $.cookie('initDirection');
update_tbl('" . $id . "', initSort, initDirection);
window['sort_initSort_" . $id . "'] = initDirection;

});


</script>\n";

		$content = "";
		$content .= $this->js_tbl_header[ $id ];
		$content .= "</table>\n";

		return $content;
	}
}