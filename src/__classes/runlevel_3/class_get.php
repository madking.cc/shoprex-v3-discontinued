<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

require_once( DIRSETTINGSCUSTOM . "class_get_settings.php" );

class class_get extends class_get_settings {
	protected $db;
	protected $log;

	public function __construct() {
		parent::__construct();
		$this->db  = class_database::getInstance();
		$this->log = class_logging::getInstance();
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	// Hole Parameter per GET oder POST
	public function get( $var, $default = null, $do_escape = true, $do_htmlspecialchars = false, $use_session = true ) {
		$GET = $_GET;

		if ( $use_session ) {
			$this->start_session();
			if ( isset( $_SESSION['INPUT_POST'] ) ) {
				$POST = $_SESSION['INPUT_POST'];
			} else {
				$POST = $_POST;
			}
		} else {
			$POST = $_POST;
		}

		// Für die Log Klasse:
		if ( $default === null ) {
			$show_value = "NULL";
		} elseif ( $default == 0 && ( strlen( $default ) == 1 ) ) {
			$show_value = "0";
		} elseif ( $default === false ) {
			$show_value = "FALSE";
		} elseif ( $default === true ) {
			$show_value = "TRUE";
		} else {
			$show_value = $default;
		}

		// Bei Übermittling mit Post:
		if ( isset( $POST[ $var ] ) ) {

			// Wenn Post nicht leer:
			if ( ! empty( $POST[ $var ] ) ) {

				if ( $GLOBALS['is_admin'] || $this->check_numeric( $var, $POST[ $var ] ) ) {
					if ( $GLOBALS['is_admin'] || $this->check_content( $var, $POST[ $var ] ) ) {
						if ( $do_escape ) {
							if ( ! is_array( $POST[ $var ] ) ) {
								if ( $do_htmlspecialchars ) {
									$value = htmlspecialchars( $this->db->id->real_escape_string( $POST[ $var ] ), ENT_NOQUOTES );

									/*
									$value_log = $value;
									if (in_array($var, $this->hide_post)) {
										$value_log = "(hidden)";
									}
									$this->log->all("POST", __FILE__ . ":" . __LINE__, $var . " => " . $value_log, false);
									*/

									return $value;
								} else {
									$value = $this->db->id->real_escape_string( $POST[ $var ] );

									/*
									$value_log = $value;
									if (in_array($var, $this->hide_post)) {
										$value_log = "(hidden)";
									}
									$this->log->all("POST", __FILE__ . ":" . __LINE__, $var . " => " . $value_log, false);
									*/

									return $value;
								}
							} else {
								$tmp = "Array $var: ";
								foreach ( $POST[ $var ] as $key => $value ) {
									if ( in_array( $var, $this->hide_post ) ) {
										$value = "(hidden)";
									}
									$tmp .= "key: $key => value: $value, ";
								}
								$tmp = substr( $tmp, 0, - 2 );
								$this->log->error( "POST", __FILE__ . ":" . __LINE__, "Post variable is array and cannot escaped. " . $tmp );
								new class_throw_error( "POST: " . __FILE__ . ":" . __LINE__ . " Post variable is array and cannot escaped. " . $tmp );
							}
						} else {
							if ( $do_htmlspecialchars ) {
								$value = htmlspecialchars( $POST[ $var ], ENT_NOQUOTES );

								/*
								$value_log = $value;
								if (in_array($var, $this->hide_post)) {
									$value_log = "(hidden)";
								}
								$this->log->all("POST", __FILE__ . ":" . __LINE__, $var . " => " . $value_log, false);
								*/

								return $value;
							} else {
								/*
								if (is_array($POST[$var])) {
									$tmp = "Array $var: ";
									foreach ($POST[$var] as $key => $value) {
										if (in_array($var, $this->hide_post)) {
											$value = "(hidden)";
										}
										$tmp .= "key: $key => value: $value, ";
									}
									$tmp = substr($tmp, 0, -2);
									$this->log->all("POST", __FILE__ . ":" . __LINE__, $tmp);
								} else {
									$value_log = $POST[$var];
									if (in_array($var, $this->hide_post)) {
										$value_log = "(hidden)";
									}
									$this->log->all("POST", __FILE__ . ":" . __LINE__, $var . " => " . $value_log);
								}
								*/
								$value = $POST[ $var ];

								return $value;
							}
						}
					} else {
						// Wenn Inhalt nicht erlaubt
						$value = $POST[ $var ];
						if ( in_array( $var, $this->hide_post ) ) {
							$value = "(hidden)";
						}
						$this->log->notice( "POST", __FILE__ . ":" . __LINE__, "Content not allowed: " . $var . " => " . $value . "" );

						//$this->error->throw_error("POST", __FILE__ . ":" . __LINE__, "Content not allowed: " . $var . " => " . $value . "");
						return null;
					}
				} else {
					// Wenn Inhalt nicht numerisch
					$value = $POST[ $var ];
					if ( in_array( $var, $this->hide_post ) ) {
						$value = "(hidden)";
					}
					$this->log->notice( "POST", __FILE__ . ":" . __LINE__, "Content not numeric. " . $var . " => " . $value . "" );

					//$this->error->throw_error("POST", __FILE__ . ":" . __LINE__, "Content not numeric. " . $var . " => " . $value . "");
					return null;
				}
			} // Wenn Post leer:
			else {
				// Überprüfen auf den Wert "0":
				if ( $POST[ $var ] == 0 && ( strlen( $POST[ $var ] ) == 1 ) ) {
					//$this->log->all("POST", __FILE__ . ":" . __LINE__, $var . " => 0");
					return "0"; // Gebe 0 zurück
				} // Alles andere:
				else {
					//$this->log->all("POST", __FILE__ . ":" . __LINE__, "Empty Content. " . $var . " => " . $show_value);
					return $default; // Gebe default zurück
				}
			}
		} // Wenn per get übermittelt wurde:
		elseif ( isset( $GET[ $var ] ) ) {
			// Überprüfung auf erlaubte Variablennamen bei GET:
			if ( $GLOBALS['is_admin'] || $this->check_key( $var ) ) {
				if ( $GLOBALS['is_admin'] || $this->check_numeric( $var, $GET[ $var ] ) ) {
					if ( $GLOBALS['is_admin'] || $this->check_content( $var, $GET[ $var ] ) ) {
						// Wenn der Inhalt nicht leer ist:
						if ( ! empty( $GET[ $var ] ) ) {
							if ( $do_escape ) {
								if ( $do_htmlspecialchars ) {
									$value = htmlspecialchars( $this->db->id->real_escape_string( $GET[ $var ] ), ENT_NOQUOTES );

									/*
									$value_log = $value;
									if (in_array($var, $this->hide_post)) {
										$value_log = "(hidden)";
									}
									$this->log->all("GET", __FILE__ . ":" . __LINE__, $var . " => " . $value_log, false);
									*/

									return $value;
								} else {
									$value = $this->db->id->real_escape_string( $GET[ $var ] );

									/*
									$value_log = $value;
									if (in_array($var, $this->hide_post)) {
										$value_log = "(hidden)";
									}
									$this->log->all("GET", __FILE__ . ":" . __LINE__, $var . " => " . $value_log, false);
									*/

									return $value;
								}
							} else {
								if ( $do_htmlspecialchars ) {
									$value = htmlspecialchars( $GET[ $var ], ENT_NOQUOTES );

									/*
									$value_log = $value;
									if (in_array($var, $this->hide_post)) {
										$value_log = "(hidden)";
									}
									$this->log->all("GET", __FILE__ . ":" . __LINE__, $var . " => " . $value_log, false);
									*/

									return $value;
								} else {
									$value = $GET[ $var ];

									/*
									$value_log = $value;
									if (in_array($var, $this->hide_post)) {
										$value_log = "(hidden)";
									}
									$this->log->all("GET", __FILE__ . ":" . __LINE__, $var . " => " . $value_log);
									*/

									return $value;
								}
							}
						} // Wenn der Inhalt leer ist:
						// Überprüfung auf die Zahl "0":
						elseif ( $GET[ $var ] == 0 && ( strlen( $GET[ $var ] ) == 1 ) ) {
							//$this->log->all("GET", __FILE__ . ":" . __LINE__, $var . " => 0");
							return 0;
						} // Sonst gebe default zurück:
						else {
							//$this->log->all("GET", __FILE__ . ":" . __LINE__, "Empty Content. " . $var . " => " . $show_value);
							return $default;
						}
					} else {
						// Wenn Inhalt nicht erlaubt
						$value     = $GET[ $var ];
						$value_log = $value;
						if ( in_array( $var, $this->hide_post ) ) {
							$value_log = "(hidden)";
						}
						$this->log->notice( "GET", __FILE__ . ":" . __LINE__, "Content not allowed: " . $var . " => " . $value_log . "" );

						//$this->error->throw_error("GET", __FILE__ . ":" . __LINE__, "Content not allowed: " . $var . " => " . $value_log . "");
						return null;
					}
				} else {
					// Wenn Inhalt nicht numerisch
					$value     = $GET[ $var ];
					$value_log = $value;
					if ( in_array( $var, $this->hide_post ) ) {
						$value_log = "(hidden)";
					}
					$this->log->notice( "GET", __FILE__ . ":" . __LINE__, "Content not numeric: " . $var . " => " . $value_log . "" );

					//$this->error->throw_error("GET", __FILE__ . ":" . __LINE__, "Content not numeric: " . $var . " => " . $value_log . "");
					return null;
				}
			} // Wenn verbotene Variablennamen benutzt wurden:
			else {
				$value     = $GET[ $var ];
				$value_log = $value;
				if ( in_array( $var, $this->hide_post ) ) {
					$value_log = "(hidden)";
				}
				$this->log->notice( "GET", __FILE__ . ":" . __LINE__, "Keyname not allowed: " . $var . " => " . $value_log . "" );

				//$this->error->throw_error("GET", __FILE__ . ":" . __LINE__, "Keyname not allowed: " . $var . " => " . $value_log . "");
				return null;
			}
		}
		// Wenn keine Post oder Get Variable existiert, gebe den angegebenen default Wert zurück:
		////$this->log->all("POST/GET", __FILE__ . ":" . __LINE__, "Not found. " . $var . " => " . $show_value);
		return $default;
	}

	protected function check_numeric( $key, $content ) {
		if ( in_array( $key, $this->check_numeric ) && ! is_array( $content ) ) {
			$tmp = trim( $content );
			if ( is_numeric( $content ) || empty( $tmp ) ) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	protected function check_key( $var ) {
		// Prüfe für GET, ob Schlüsselname auch erlaubt
		if ( in_array( $var, $this->allowed_get_keys ) ) {
			return true;
		} else {
			return false;
		}
	}

	public function store_post() {
		$this->start_session();

		$_SESSION['INPUT_POST'] = $_POST;
		//$_SESSION['INPUT_GET'] = $_GET;
	}

	public function remove_post( $var = "" ) {
		if ( ! empty( $var ) ) {
			if ( isset( $_SESSION['INPUT_POST'][ $var ] ) ) {
				unset( $_SESSION['INPUT_POST'][ $var ] );
			} elseif ( isset( $_SESSION['INPUT_GET'][ $var ] ) ) {
				unset( $_SESSION['INPUT_GET'][ $var ] );
			}
		} else {
			if ( isset( $_SESSION['INPUT_POST'] ) ) {
				unset( $_SESSION['INPUT_POST'] );
			}
			if ( isset( $_SESSION['INPUT_GET'] ) ) {
				unset( $_SESSION['INPUT_GET'] );
			}
		}
	}

	public function is_set( $var ) {
		if ( isset( $_POST[ $var ] ) ) {
			return true;
		}
		if ( isset( $_GET[ $var ] ) ) {
			return true;
		}

		return false;
	}
}