<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

require_once( DIRSETTINGSCUSTOM . "class_layout_settings.php" );

// Layout Klasse

class class_layout extends class_layout_settings {
	protected $p;
	protected $log;
	protected $db;
	protected $lang;
	protected $loc;


	public $button_reload_counter = 0;
	public $js_tab_function_enabled = 0;
	protected $js_tab_function;
	public $enable_js_tab_function = ENABLE_TAB_IN_TEXTAREAS;
	protected $edit_button_by_lang_counter = array();

	protected $input_datetime_id_counter = 0;
	protected $input_date_id_counter = 0;
	protected $input_color_id_counter = 0;
	protected $checkbox_counter = 0;
	protected $media_ta_counter = 0;
	protected $custom_upload_picture_pages = array( "account_picture_upload.php" );
	protected $ta_char_counter_count = 0;
	protected $text_char_counter_count = 0;
	protected $confirm_id = array();

	protected $slideshow_counter = 0;
	protected $paste_button_counter = 0;

	protected $paste_select_counter = 0;

	protected $close_button_counter = 0;

	public function __construct() {
		$this->p    = new class_page();
		$this->log  = class_logging::getInstance();
		$this->db   = class_database::getInstance();
		$this->lang = class_language::getInstance();
		$this->loc  = class_location::getInstance();

		$this->paste_button_counter = $this->unique_id( NOT_BIG );
		$this->paste_select_counter = $this->unique_id( NOT_BIG );
		$this->close_button_counter = $this->unique_id( NOT_BIG );

		parent::__construct();


		if ( defined( "TITLE" . $this->lang->lnbr ) ) {
			$GLOBALS['page_title'] = constant( "TITLE" . $this->lang->lnbr );
		} elseif ( defined( "TITLE" . $this->lang->lnbrm ) ) {
			$GLOBALS['page_title'] = constant( "TITLE" . $this->lang->lnbrm );
		} elseif ( defined( "TITLE" ) ) {
			$GLOBALS['page_title'] = constant( "TITLE" );
		} elseif ( ! isset( $GLOBALS['page_title'] ) ) {
			$GLOBALS['page_title'] = "";
		}


		if ( defined( "DESCRIPTION" . $this->lang->lnbr ) ) {
			$GLOBALS['description'] = constant( "DESCRIPTION" . $this->lang->lnbr );
		} elseif ( defined( "DESCRIPTION" . $this->lang->lnbrm ) ) {
			$GLOBALS['description'] = constant( "DESCRIPTION" . $this->lang->lnbrm );
		} elseif ( defined( "DESCRIPTION" ) ) {
			$GLOBALS['description'] = constant( "DESCRIPTION" );
		} elseif ( ! isset( $GLOBALS['description'] ) ) {
			$GLOBALS['description'] = "";
		}


		if ( defined( "KEYWORDS" . $this->lang->lnbr ) ) {
			$GLOBALS['keywords'] = constant( "KEYWORDS" . $this->lang->lnbr );
		} elseif ( defined( "KEYWORDS" . $this->lang->lnbrm ) ) {
			$GLOBALS['keywords'] = constant( "KEYWORDS" . $this->lang->lnbrm );
		} elseif ( defined( "KEYWORDS" ) ) {
			$GLOBALS['keywords'] = constant( "KEYWORDS" );
		} elseif ( ! ! isset( $GLOBALS['keywords'] ) ) {
			if ( ! isset( $GLOBALS['subtitle'] ) || empty( $GLOBALS['subtitle'] ) ) {
				if ( defined( "SUBTITLE" . $this->lang->lnbr ) ) {
					$GLOBALS['subtitle'] = constant( "SUBTITLE" . $this->lang->lnbr );
				} elseif ( defined( "SUBTITLE" . $this->lang->lnbrm ) ) {
					$GLOBALS['subtitle'] = constant( "SUBTITLE" . $this->lang->lnbrm );
				} elseif ( defined( "SUBTITLE" ) ) {
					$GLOBALS['subtitle'] = constant( "SUBTITLE" );
				} else {
					$GLOBALS['subtitle'] = "";
				}
			}
		}
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public function decode( $value ) {
		$value = str_replace( "&#34;", "\"", $value );
		$value = str_replace( "&#39;", "\'", $value );

		return $value;
	}

	public function display_status_and_reload( $text, $parameter, $autoreload = false ) {
		if ( $autoreload ) {
			$this->reload_js( "", $parameter );
		} else {
			return $text . $this->reload_button( "", $parameter, $this->txt( "RELOAD" ), "btn btn-default" );
		}
	}

	public function reload_js( $page = "", $parameter = "", $parent = false, $check_admin = true, $cast_amp = true, $has_protocol_prefix = false, $mask_last_anker = true ) {
		if ( empty( $page ) ) {
			if ( $GLOBALS['is_admin'] && $check_admin ) {
				$page .= ADMINDIR;
			}
			$page .= $this->loc->uri_clean;
		} else {
			if ( $GLOBALS['is_admin'] && $check_admin ) {
				$page = ADMINDIR . $page;
			}
		}

		if ( empty( $parameter ) && $parameter !== false ) {
			$tmp = "";
			if ( ! empty( $this->loc->parameter_line ) ) {
				$tmp = $this->p->fix_parameter( $this->loc->parameter_line, $cast_amp, $has_protocol_prefix, $mask_last_anker );
			}

			$page .= $tmp;
		} elseif ( ! empty( $parameter ) && $parameter !== false ) {
			$parameter = $this->p->fix_parameter( $parameter, $cast_amp, $has_protocol_prefix, $mask_last_anker );
			if ( ! empty( $parameter ) ) {
				$page .= $parameter;
			}
		}

		if ( $parent ) {
			$GLOBALS['body_footer'] .= "<script>
            parent.location.href = '" . $this->loc->httpauto_web_root . $page . "';
        </script>
        ";
		} else {

			$GLOBALS['body_footer'] .= "<script>
            window.location.href = '" . $this->loc->httpauto_web_root . $page . "';
        </script>
        ";
		}
	}


	public function reload_button( $page = "", $parameter = "", $text = "Neu laden", $class = "btn btn-default", $check_admin = true, $mask_last_anker = true ) {

		$content = "";

		if ( empty( $page ) ) {
			$page = $this->loc->current;
		} else {
			if ( $GLOBALS['is_admin'] && $check_admin ) {
				$page = ADMINDIR . $page;
			}
			$page = $this->loc->web_root . $page;
		}

		if ( empty( $parameter ) && $parameter !== false ) {
			$tmp = "";
			if ( ! empty( $this->loc->parameter_line ) ) {
				$tmp = $this->p->fix_parameter( $this->loc->parameter_line, NO_CAST_AMP, HAS_PROTOCOL_PREFIX, $mask_last_anker );
			}

			$page .= $tmp;
		} elseif ( ! empty( $parameter ) && $parameter !== false ) {
			$parameter = $this->p->fix_parameter( $parameter, NO_CAST_AMP, HAS_PROTOCOL_PREFIX, $mask_last_anker );
			if ( ! empty( $parameter ) ) {
				$page .= $parameter;
			}
		}

		$GLOBALS['body_footer'] .= "<script>\n" . " function button_reload_page_" . $this->button_reload_counter . "()\n" . " {\n" . "   window.location.href = '" . $this->loc->httpauto_root . $page . "';\n" . " }\n" . "</script>";
		$content .= $this->button( $text, "onclick='button_reload_page_" . $this->button_reload_counter . "();'", $class );
		$this->button_reload_counter ++;

		return $content;
	}


	public function button( $value, $free_text = "", $class = "btn btn-default" ) {
		return "<input type=\"button\" value=\"$value\" $free_text class=\"$class\" />";
	}


	public function scroll_to_anchor() {

		$anchor = $this->p->get( "anchor" );

		if ( isset( $anchor ) && ! empty( $anchor ) ) {
			$GLOBALS['body_footer'] .= "<script>
                $(function() { location.hash = \"#$anchor\"; });
            </script>
            ";
		}
	}

	public function scroll_to_id( $id ) {


		if ( ! ( is_null( $id ) || $id === "" ) ) {

			if ( $GLOBALS['is_admin'] ) {
				$top_adjust = "-100";
			} else {
				$top_adjust = "";
			}

			$GLOBALS['body_footer'] .= "
            <script>
             $(function() {
                $(\"#" . $id . "\").ready(function() {

                    $(document).scrollTop( $(\"#" . $id . "\").offset().top" . $top_adjust . ");

                });

             });
            </script>
        ";
		}
	}

	public function display_message_by_session( $session_var_name, $text, $type = "success" ) {
		if ( isset( $_SESSION[ $session_var_name ] ) ) {
			unset( $_SESSION[ $session_var_name ] );

			return $this->alert_text_dismiss( $type, $text );
		} else {
			return "";
		}
	}

	public function alert_text_dismiss( $type, $text, $freetext = "", $freetext_button = "" ) {
		// type: success, info, warning, danger

		return "<div $freetext class='alert alert-" . $type . " alert-dismissable'><button $freetext_button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><p>" . $text . "</p></div>";
	}


	public function input_max_file_size() {
		$max_size = $this->p->get_max_upload();
		$max_size = $max_size * 1024 * 1024;

		return "<input type='hidden' name='MAX_FILE_SIZE' value='$max_size' />";
	}

	public function box_alert( $text ) {
		$class_external                = class_load_external::getInstance();
		$class_external->load_colorbox = true;

		return "$.colorbox({html:\"$text\" });";
	}


	public function form( $free_text = "", $action = "", $parameter = "", $name = "", $method = "POST", $class = "form-inline", $check_admin = true, $use_webroot = true ) {
		$this->start_session();
		if ( $use_webroot ) {
			$action_suffix = $this->loc->web_root;
			if ( $check_admin ) {
				if ( $GLOBALS['is_admin'] ) {
					$action_suffix = $this->loc->web_admin_root;
				}
			}
		} else {
			$action_suffix = "";
		}

		if ( empty( $action ) ) {
			$action = $this->loc->uri_clean;
		}

		if ( empty( $parameter ) && $parameter !== false ) {
			$parameter = "";
			if ( ! empty( $this->loc->parameter_line ) ) {
				$parameter = $this->loc->parameter_line;
			}
		}
		$parameter = $this->p->fix_parameter( $parameter, NO_CAST_AMP, HAS_PROTOCOL_PREFIX );
		$action    = $action_suffix . $action . $parameter;

		$form_session_id                                        = $this->p->unique_id( NOT_BIG );
		$_SESSION["input_security_form_id"][ $form_session_id ] = true;

		$keep_form_ids = "";
		if ( false && isset( $GLOBALS['is_in_frame'] ) && $GLOBALS['is_in_frame'] ) {
			$keep_form_ids = "\n" . $this->keep_form_ids();
		}

		// Form mit UTF-8 Zeichensatz und POST Methode
		return "<form $name action=\"/" . $this->loc->web_root .
		       "input.php\" method=\"$method\" accept-charset=\"UTF-8\" class=\"$class\" $free_text >\n" .
		       "<input type=\"hidden\" name=\"input_form_action\" value=\"$action\">\n" .
		       "<input type=\"hidden\" name=\"input_form_id\" value=\"$form_session_id\">" . $keep_form_ids;
	}


	public function hidden( $name, $value, $freetext = "" ) {
		$value = $this->encode( $value );

		return "<input type=\"hidden\" name=\"$name\" value=\"$value\" $freetext />";
	}

	public function encode( $value ) {
		$value = str_replace( "\\\"", "&#34;", $value );
		$value = str_replace( "\"", "&#34;", $value );
		$value = str_replace( "\\'", "&#39;", $value );

		return $value;
	}


	public function submit( $value = "Senden", $free_text = "", $class = "btn btn-default" ) {
		return "<input type=\"submit\" value=\"$value\" $free_text class=\"$class\" />";
	}


	public function reset( $value = "Reset", $free_text = "", $class = "btn btn-default" ) {
		return "<input type=\"reset\" value=\"$value\" $free_text class=\"$class\" />";
	}


	public function password( $name, $text = "", $size = "250", $free_text = "", $max_length = "", $class = "input_text form-control" ) {
		// Ersetze " mit HTML-Code, damit kein Fehler entsteht
		$text = str_replace( "\"", "&#34;", $text );
		if ( ! empty( $size ) ) {
			$size = "width:" . $size . "px;";
		}

		return "<input type=\"password\" name=\"$name\" class=\"$class\" value=\"$text\" maxlength=\"$max_length\" $free_text style=\"$size display:inline;\" />";
	}


	public function datetime( $name, $value = "", $size = "250", $free_text = "", $max_length = "", $class = "input_text form-control" ) {
		$class_load_external                  = class_load_external::getInstance();
		$class_load_external->load_timepicker = true;

		$GLOBALS['body_footer'] .= "
<script>
    $(function() {
        $('#datetimepicker_$this->input_datetime_id_counter').datetimepicker({dateFormat: \"dd.mm.yy\", changeMonth: true, changeYear: true});
    });

</script>
";

		// Ersetze " mit HTML-Code, damit kein Fehler entsteht
		$value = str_replace( "\"", "&#34;", $value );
		if ( ! empty( $size ) ) {
			$size = "width:" . $size . "px;";
		}

		$content = "<input id=\"datetimepicker_" . $this->input_datetime_id_counter . "\" type=\"text\" name=\"$name\" class=\"$class\" value=\"$value\" maxlength=\"$max_length\" $free_text style=\"$size display:inline;\" />";
		$this->input_datetime_id_counter ++;

		return $content;
	}

	// Hidden Feld

	public function date( $name, $value = "", $size = "250", $free_text = "", $max_length = "", $class = "input_text form-control" ) {
		$class_load_external                  = class_load_external::getInstance();
		$class_load_external->load_timepicker = true;

		$GLOBALS['body_footer'] .= "
<script>
    $(function() {
        $('#datepicker_$this->input_date_id_counter').datepicker({dateFormat: \"dd.mm.yy\", changeMonth: true, changeYear: true});
    });

</script>
";

		// Ersetze " mit HTML-Code, damit kein Fehler entsteht
		$value = str_replace( "\"", "&#34;", $value );
		if ( ! empty( $size ) ) {
			$size = "width:" . $size . "px;";
		}

		$content = "<input id=\"datepicker_" . $this->input_date_id_counter . "\" type=\"text\" name=\"$name\" class=\"$class\" value=\"$value\" maxlength=\"$max_length\" $free_text style=\"$size display:inline;\" />";
		$this->input_date_id_counter ++;

		return $content;
	}

	// Back Button

	public function color( $name, $color_init = "", $size = "25", $free_text = "", $class = "input_text form-control" ) {
		$class_external = class_load_external::getInstance();


		$class_external->load_colorpicker = true;

		$GLOBALS['body_footer'] .= "
<script>

$(function() {

    $('#colorpicker_" . $this->input_color_id_counter . "').spectrum({
        flat: false,
        showInput: true,
        allowEmpty: true,
        className: \"full-spectrum\",
        showInitial: true,
        showPalette: true,
        showSelectionPalette: true,
        maxPaletteSize: 10,
        preferredFormat: \"hex\",
        localStorageKey: \"spectrum.colors\",
        move: function (color) {
    },
        show: function () {

    },
        beforeShow: function () {

    },
        hide: function (color) {
    },

        palette: [
        [\"rgb(0, 0, 0)\", \"rgb(67, 67, 67)\", \"rgb(102, 102, 102)\", /*\"rgb(153, 153, 153)\",\"rgb(183, 183, 183)\",*/
            \"rgb(204, 204, 204)\", \"rgb(217, 217, 217)\", /*\"rgb(239, 239, 239)\", \"rgb(243, 243, 243)\",*/ \"rgb(255, 255, 255)\"],
        [\"rgb(152, 0, 0)\", \"rgb(255, 0, 0)\", \"rgb(255, 153, 0)\", \"rgb(255, 255, 0)\", \"rgb(0, 255, 0)\",
            \"rgb(0, 255, 255)\", \"rgb(74, 134, 232)\", \"rgb(0, 0, 255)\", \"rgb(153, 0, 255)\", \"rgb(255, 0, 255)\"],
        [\"rgb(230, 184, 175)\", \"rgb(244, 204, 204)\", \"rgb(252, 229, 205)\", \"rgb(255, 242, 204)\", \"rgb(217, 234, 211)\",
            \"rgb(208, 224, 227)\", \"rgb(201, 218, 248)\", \"rgb(207, 226, 243)\", \"rgb(217, 210, 233)\", \"rgb(234, 209, 220)\",
            \"rgb(221, 126, 107)\", \"rgb(234, 153, 153)\", \"rgb(249, 203, 156)\", \"rgb(255, 229, 153)\", \"rgb(182, 215, 168)\",
            \"rgb(162, 196, 201)\", \"rgb(164, 194, 244)\", \"rgb(159, 197, 232)\", \"rgb(180, 167, 214)\", \"rgb(213, 166, 189)\",
            \"rgb(204, 65, 37)\", \"rgb(224, 102, 102)\", \"rgb(246, 178, 107)\", \"rgb(255, 217, 102)\", \"rgb(147, 196, 125)\",
            \"rgb(118, 165, 175)\", \"rgb(109, 158, 235)\", \"rgb(111, 168, 220)\", \"rgb(142, 124, 195)\", \"rgb(194, 123, 160)\",
            \"rgb(166, 28, 0)\", \"rgb(204, 0, 0)\", \"rgb(230, 145, 56)\", \"rgb(241, 194, 50)\", \"rgb(106, 168, 79)\",
            \"rgb(69, 129, 142)\", \"rgb(60, 120, 216)\", \"rgb(61, 133, 198)\", \"rgb(103, 78, 167)\", \"rgb(166, 77, 121)\",
            /*\"rgb(133, 32, 12)\", \"rgb(153, 0, 0)\", \"rgb(180, 95, 6)\", \"rgb(191, 144, 0)\", \"rgb(56, 118, 29)\",
            \"rgb(19, 79, 92)\", \"rgb(17, 85, 204)\", \"rgb(11, 83, 148)\", \"rgb(53, 28, 117)\", \"rgb(116, 27, 71)\",*/
            \"rgb(91, 15, 0)\", \"rgb(102, 0, 0)\", \"rgb(120, 63, 4)\", \"rgb(127, 96, 0)\", \"rgb(39, 78, 19)\",
            \"rgb(12, 52, 61)\", \"rgb(28, 69, 135)\", \"rgb(7, 55, 99)\", \"rgb(32, 18, 77)\", \"rgb(76, 17, 48)\"]
    ]
    });
});
</script>
";
		if ( ! empty( $size ) ) {
			$size = "width:" . $size . "px;";
		}
		$content = "<input id=\"colorpicker_" . $this->input_color_id_counter . "\" name=\"$name\" class=\"$class\" $free_text style=\"$size display:inline;\" value=\"$color_init\" />";
		$this->input_color_id_counter ++;

		return $content;
	}


	public function file( $name, $size = "", $free_text = "", $class = "input_file btn btn-default" ) {
		if ( ! empty( $size ) ) {
			$size = "width:" . $size . "px;";
		}

		return "<input type=\"file\" name=\"$name\" $free_text style=\"" . $size . "\" class=\"$class\" />";
	}


	public function select_yesno( $name, $match = "-1", $width = "85", $freetext = "", $submit_on_change = false, $style = "", $class = "select form-control" ) {
		$content = "";
		$content .= $this->select( $name, $freetext, $width, $submit_on_change, $style, $class );
		$content .= "<option value='1'";
		if ( $match == "1" ) {
			$content .= " selected=selected";
		}
		$content .= ">" . $this->txt( 'YES' ) . "</option>\n";
		$content .= "<option value='0'";
		if ( $match == "0" ) {
			$content .= " selected=selected";
		}
		$content .= ">" . $this->txt( 'NO' ) . "</option>\n";
		$content .= "</select>\n";

		return $content;
	}

	public function select( $name, $freetext = "", $width = "250", $on_change = "", $style = "", $class = "select form-control" ) {
		if ( ! empty( $width ) ) {
			$width = "width:" . $width . "px !important;";
		}
		if ( ! empty( $width ) || ! empty( $style ) ) {
			$style = "style='" . $style . $width . "'";
		}
		if ( $on_change === true ) {
			$on_change = "this.form.submit();";
		}

		if ( ! empty( $on_change ) ) {
			$on_change = "onchange='$on_change'";
		}

		return "<select class=\"$class\" name=\"$name\" $style $freetext $on_change id=\"$name\">\n";
	}

	public function select_load( $name = "", $freetext = "", $width = "", $on_change = "select_load_page(this);", $style = "", $class = "select form-control input-sm hide_first_option" ) {


		if ( ! empty( $width ) ) {
			$width = "width:" . $width . "px;";
		}
		if ( ! empty( $width ) || ! empty( $style ) ) {
			$style = "style='" . $style . $width . "'";
		}

		$GLOBALS['foot_bottom'] .= "
<script>

function select_load_page(elementID)
{
    var page = elementID.options[elementID.selectedIndex].value;
    window.location.href = '" . $this->loc->httpauto_root . $this->loc->web_root . "' + page;
}

</script>

";

		return $this->select( $name, $freetext, $width, $on_change, $style, $class );
	}

	public function select_day( $name = "", $head = "", $match = "", $freetext = "", $width = "", $on_change = "", $style = "", $class = "select form-control" ) {
		$content = "";

		if ( ! empty( $head ) ) {
			$class .= "  hide_first_option";
		}
		if ( ! empty( $width ) ) {
			$width = "width:" . $width . "px;";
		}
		if ( ! empty( $width ) || ! empty( $style ) ) {
			$style = "style='" . $style . $width . "'";
		}

		$content .= $this->select( $name, $freetext, $width, $on_change, $style, $class );

		if ( ! empty( $head ) ) {
			$content .= "<option value='-1'>$head</option>\n";
		}

		//$content .= "<option value='0'>---</option>\n";

		for ( $i = 1; $i <= 31; $i ++ ) {
			$content .= "<option style='text-align:right' value='$i'";
			if ( $match == $i ) {
				$content .= " selected='selected'";
			}
			$content .= ">$i</option>\n";
		}

		$content .= "</select>\n";

		return $content;
	}


	public function select_month( $name = "", $head = "", $match = "", $freetext = "", $width = "", $on_change = "", $style = "", $class = "select form-control" ) {
		$content = "";

		if ( ! empty( $head ) ) {
			$class .= "  hide_first_option";
		}
		if ( ! empty( $width ) ) {
			$width = "width:" . $width . "px;";
		}
		if ( ! empty( $width ) || ! empty( $style ) ) {
			$style = "style='" . $style . $width . "'";
		}

		$content .= $this->select( $name, $freetext, $width, $on_change, $style, $class );

		if ( ! empty( $head ) ) {
			$content .= "<option value='-1'>$head</option>\n";
		}

		//$content .= "<option value='0'>---</option>\n";

		for ( $i = 1; $i <= 12; $i ++ ) {
			$content .= "<option style='text-align:left' value='$i'";
			if ( $match == $i ) {
				$content .= " selected='selected'";
			}
			$content .= ">" . $this->p->get_month( $i ) . "</option>\n";
		}

		$content .= "</select>\n";

		return $content;
	}


	public function select_year( $name = "", $head = "", $match = "", $freetext = "", $width = "", $on_change = "", $style = "", $class = "select form-control" ) {
		$content = "";

		if ( ! empty( $head ) ) {
			$class .= "  hide_first_option";
		}
		if ( ! empty( $width ) ) {
			$width = "width:" . $width . "px;";
		}
		if ( ! empty( $width ) || ! empty( $style ) ) {
			$style = "style='" . $style . $width . "'";
		}

		$content .= $this->select( $name, $freetext, $width, $on_change, $style, $class );

		if ( ! empty( $head ) ) {
			$content .= "<option value='-1'>$head</option>\n";
		}

		//$content .= "<option value='0'>---</option>\n";

		$tmp         = time();
		$recent_year = date( "Y", $tmp );
		$first_year  = $recent_year - 110;

		for ( $i = $recent_year; $i >= $first_year; $i -- ) {
			$content .= "<option style='text-align:right' value='$i'";
			if ( $match == $i ) {
				$content .= " selected='selected'";
			}
			$content .= ">" . $i . "</option>\n";
		}

		$content .= "</select>\n";

		return $content;
	}


	public function checkbox( $name, $value, $checked = false, $uncheck_value = "0", $freetext = "", $submit_uncheck_value = true, $class = "checkbox" ) {
		// TODO: UNCHECK_VALUE get/post bugged


		$content = "";

		if ( $submit_uncheck_value ) {
			if ( $checked ) {
				$content .= $this->hidden( $name, $value );
			} else {
				$content .= $this->hidden( $name, $uncheck_value );
			}

			$content .= "<input type=\"checkbox\" name=\"" . $name . "_dummy\" value=\"$value\" $freetext onChange=\"checkbox_change(this, '$name', '$value', '$uncheck_value')\"";
			if ( $checked ) {
				$content .= " checked=\"checked\"";
			}
			$content .= " class=\"$class\" />\n";

			$GLOBALS['foot_bottom'] .= "<script>\n" . "function checkbox_change(element, hidden_element_name, checkedValue, uncheckedValue)\n" .
			                           "{\n" . "   if(element.checked)\n" . "   {\n" . "       document.getElementsByName(hidden_element_name)[0].value = checkedValue;\n" . "   }\n" .
			                           "   else\n" . "   {\n" . "       document.getElementsByName(hidden_element_name)[0].value = uncheckedValue;\n" . "   }\n" . "}\n" . "</script>\n";

			$this->checkbox_counter ++;
		} else {
			$content .= "<input type=\"checkbox\" name=\"" . $name . "\" value=\"$value\" $freetext";
			if ( $checked ) {
				$content .= " checked=\"checked\"";
			}
			$content .= " class=\"$class\" />";
		}

		return $content;
	}

	public function radio( $name, $value, $match = "", $checked = false, $freetext = "", $class = "radio" ) {
		$content = "<input type=\"radio\" name=\"$name\" value=\"$value\" $freetext ";

		if ( $checked || $match == $value ) {
			$content .= "checked=\"checked\"";
		}

		$content .= " class=\"$class\" />\n";

		return $content;
	}


	public function back_button( $value = "", $href = "", $parameter = "", $free_text = "", $class = "btn btn-default", $check_admin = true ) {
		if ( empty( $value ) ) {
			$value = $this->txt( 'BACK' );
		}

		if ( empty( $href ) ) {
			$target = $this->loc->httpauto_root . $this->loc->current;
		} elseif ( $GLOBALS['is_admin'] && $check_admin ) {
			$target = $this->loc->httpauto_root . $this->loc->web_root . ADMINDIR . $href;
		} else {
			$target = $this->loc->httpauto_root . $this->loc->web_root . $href;
		}

		$parameter = $this->p->fix_parameter( $parameter );

		return "<input type=\"button\" value=\"$value\" $free_text class=\"$class\" onclick=\"window.location.href='$target" . $parameter . "'\" />";
	}

	public function link_button( $text, $target, $parameter = "", $free_text = "", $class = "link_button btn btn-default", $dont_modify_target = false ) {

		if ( ! $this->check_link_target( $target ) || empty( $target ) ) {
			if ( ! $dont_modify_target ) {
				if ( empty( $target ) ) {
					$target = "/" . $this->loc->current;
				} else {
					$tmp = $this->loc->web_root;
					if ( $GLOBALS['is_admin'] ) {
						$tmp .= ADMINDIR;
					}
					$target = $tmp . $target;
				}
			}
			$parameter = $this->p->fix_parameter( $parameter, CAST_AMP, NO_PROTOCOL_PREFIX );

			return "<a href=\"/" . $target . $parameter . "\" $free_text class=\"$class\">$text</a>";
		} else {
			$parameter = $this->p->fix_parameter( $parameter, CAST_AMP, HAS_PROTOCOL_PREFIX );

			return "<a href=\"/$target$parameter\" $free_text class=\"$class\">$text</a>";
		}
	}

	protected function check_link_target( $target ) {
		if ( ( strncmp( $target, "mailto:", 7 ) == 0 ) || ( strncmp( $target, "http://", 7 ) == 0 ) || ( strncmp( $target, "https://", 8 ) == 0 ) || ( strncmp( $target, "ftp://", 6 ) == 0 ) || ( strncmp( $target, "#", 1 ) == 0 )
		) {
			return true;
		} else {
			return false;
		}
	}

	public function img( $link, $alt, $free_text = "", $class = "img" ) {
		return "<img src=\"/" . $this->loc->web_root . $link . "\" $free_text alt=\"$alt\" class=\"$class\">";
	}


	public function fix_ta( $string ) {
		$string = htmlspecialchars( $string, ENT_QUOTES );
		$string = str_replace( "\r", '', $string );
		$string = str_replace( "\n", '\n', $string );

		return $string;
	}


	public function box_js( $element, $link, $parameter, $width = "", $height = "", $check_admin = true ) {
		$class_external = class_load_external::getInstance();

		$class_external->load_colorbox = true;

		if ( empty( $width ) ) {
			$width = $GLOBALS['box_width'];
		}
		if ( empty( $height ) ) {
			$height = $GLOBALS['box_height'];
		}

		$parameter = $this->p->fix_parameter( $parameter, NO_CAST_AMP );

		if ( $GLOBALS['is_admin'] && $check_admin ) {
			$link = ADMINDIR . $link;
		}

		$GLOBALS['body_footer'] .= "<script>
$(document).ready(function() {

    $(document).on('click', '$element', function(){
        $.colorbox({href:'/" . WEBROOT . $link . $parameter . "', iframe:true, width:'$width', height:'$height' });
        });
});
</script>
";
	}

	public function ta( $name, $text = "", $width = "840", $height = "500", $freetext = "", $id = "", $class = "ta form-control" ) {


		if ( empty( $id ) ) {
			$id = "$name";
		}

		$content = "<textarea name=\"$name\" style=\"width:" . $width . "px; height:" . $height . "px;\" $freetext  class=\"$class\" id=\"$id\">$text</textarea>";

		// Bug, if a space is inserted in the beginning of a line the content will not be saved
		if ( $this->enable_js_tab_function && $GLOBALS['is_admin'] ) {
			if ( ! $this->js_tab_function_enabled ) {
				$this->js_tab_function_enabled = true;
				$GLOBALS['body_footer'] .= $this->get_javascript_tab_fix_in_input_elements();
			}

			$GLOBALS['body_footer'] .= "<script>
$(\"#" . $id . "\").allowTabChar();
</script>\n";
		}

		return $content;
	}


	public function ta_char_counter( $name, $text = "", $width = "840", $height = "500", $freetext = "", $id = "", $max_length = "", $class = "ta form-control" ) {


		if ( empty( $id ) ) {
			$id = "$name";
		}
		if ( ! empty( $max_length ) ) {
			$length_suggested = AL_TBL_MAX_SUGGESTED . " " . $max_length;
		}

		$count_display_parameter = "'$id', 'countdisplay_" . $id . $this->ta_char_counter_count . "'";

		$content = "<textarea name=\"$name\" style=\"width:" . $width . "px; height:" . $height . "px;\" $freetext  class=\"$class\" id=\"$id\"
            onKeypress=\"ta_char_count($count_display_parameter)\"
            onKeydown=\"ta_char_count($count_display_parameter)\"
            onKeyup=\"ta_char_count($count_display_parameter)\"
            onChange=\"ta_char_count($count_display_parameter)\"
            >$text</textarea>
            <p>" . AL_TBL_CHARS . " <span class='count_display' id='countdisplay_" . $id . $this->ta_char_counter_count . "'></span> $length_suggested</p>";

		// Bug, if a space is inserted in the beginning of a line the content will not be saved
		if ( $this->enable_js_tab_function && $GLOBALS['is_admin'] ) {
			if ( ! $this->js_tab_function_enabled ) {
				$this->js_tab_function_enabled = true;
				$GLOBALS['body_footer'] .= $this->get_javascript_tab_fix_in_input_elements();
			}

			$GLOBALS['body_footer'] .= "<script>
$(\"#" . $id . "\").allowTabChar();
</script>\n";
		}

		if ( $this->ta_char_counter_count == 0 ) {
			$GLOBALS['body_footer'] .= "<script>
    function ta_char_count(ta_id, count_id)
    {
        $('#'+count_id).text( $('#'+ta_id).val().length );
    }
</script>\n";
		}

		$GLOBALS['body_footer'] .= "<script>
$(function(){
   $('#countdisplay_" . $id . $this->ta_char_counter_count . "').text( $('#" . $id . "').val().length );
});
</script>\n";

		$this->ta_char_counter_count ++;

		return $content;
	}

	public function text_char_counter( $name, $text = "", $size = "250", $free_text = "", $id = "", $max_length = "", $class = "input_text form-control" ) {


		if ( empty( $id ) ) {
			$id = "$name";
		}
		if ( ! empty( $max_length ) ) {
			$length_suggested = AL_TBL_MAX_SUGGESTED . " " . $max_length;
		}
		$text = $this->encode( $text );
		if ( ! empty( $size ) ) {
			$size = "width:" . $size . "px;";
		}

		$count_display_parameter = "'$id', 'countdisplay_" . $id . $this->text_char_counter_count . "'";

		$content = "<input type=\"text\" name=\"$name\" class=\"$class\" value=\"$text\" $free_text style=\"$size display:inline;\" id=\"$id\"
            onKeypress=\"ta_char_count($count_display_parameter)\"
            onKeydown=\"ta_char_count($count_display_parameter)\"
            onKeyup=\"ta_char_count($count_display_parameter)\"
            onChange=\"ta_char_count($count_display_parameter)\" />
            <p>" . AL_TBL_CHARS . " <span class='count_display' id='countdisplay_" . $id . $this->text_char_counter_count . "'></span> $length_suggested</p>";

		if ( $this->text_char_counter_count == 0 ) {
			$GLOBALS['body_footer'] .= "<script>
    function text_char_count(ta_id, count_id)
    {
        $('#'+count_id).text( $('#'+ta_id).val().length );
    }
</script>\n";
		}

		$GLOBALS['body_footer'] .= "<script>
$(function(){
   $('#countdisplay_" . $id . $this->text_char_counter_count . "').text( $('#" . $id . "').val().length );
});
</script>\n";

		$this->text_char_counter_count ++;

		return $content;
	}


	public function box_confirm( $title, $text, $source, $form_id, $id = "dialog-confirm", $button_ok = "Ja", $button_cancel = "Nein" ) {
		$class_external                = class_load_external::getInstance();
		$class_external->load_colorbox = true;

		if ( ! in_array( $id, $this->confirm_id ) ) {
			$GLOBALS["body_header"] .= "<div id=\"$id\" title=\"$title\">
<p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin: 0 7px 20px 0;\"></span>$text</p>
</div>";
			$this->confirm_id[] = $id;
		}

		$GLOBALS["body_footer"] .= " <script>
$(function() {
    $( \"#$id\" ).css('display', 'none');
    $( document ).on( \"click\", \"$source\", function() {
    $( \"#$id\" ).dialog({
    resizable: false,
    height:200,
    modal: true,
    buttons: {
            \"$button_ok\": function() {
                $( this ).dialog( \"close\" );
                $( \"#$form_id\" ).submit();
            },
    \"$button_cancel\": function() {
                $( this ).dialog( \"close\" );
            }
    }
    });
    });
});
</script>";
	}

	public function panel( $text, $type = "default", $header = "", $freetext = "" ) {
		// $type: default, primary, success, info, warning, danger

		$content = "<div $freetext class=\"panel panel-" . $type . "\">\n";
		if ( ! empty( $header ) ) {
			$content .= "  <div class=\"panel-heading\">" . $header . "</div>\n";
		}
		$content .= "  <div class=\"panel-body\">
    " . $text . "
  </div>
</div>\n";

		return $content;
	}


	function table( $freetext = "", $class = "table table-striped table-responsive", $border = "0" ) {
		$content = "<table $freetext class=\"$class\" border=\"$border\">";

		return $content;
	}


	public function escape_single_quotation_marks( $string ) {
		$string = str_replace( "'", "\'", $string );
		$string = str_replace( "\\\\'", "\\\\\\'", $string );

		return $string;
	}

	public function remove_line_breaks( $string, $log_found = true ) {
		if ( $log_found ) {
			if ( strpos( $string, "\r" ) !== false || strpos( $string, "\n" ) !== false ) {
				$this->log->notice( "warning", __FILE__ . ":" . __LINE__, "Found Line Break in string: '$string'" );
			}
		}
		$string = str_replace( "\r", "", $string );
		$string = str_replace( "\n", "", $string );

		return $string;
	}


	public function link_sql( $text, $row, $itm, $sort, $parameter, $reverse = false, $target = "" ) {
		if ( empty( $target ) ) {
			$target = $this->loc->current_file;
		}
		if ( $GLOBALS['is_admin'] ) {
			$target = ADMINDIR . $target;
		}

		$class = "";
		if ( $itm == $row ) {
			if ( $sort == "asc" ) {
				$sort = "desc";
			} else {
				$sort = "asc";
			}
			$class = "selected";
		} else {
			if ( $reverse ) {
				$sort = "desc";
			} else {
				$sort = "asc";
			}
		}
		if ( empty( $parameter ) ) {
			return $this->link( $text, $target, "itm=$row&sort=$sort", "", "", "link " . $class );
		} else {
			return $this->link( $text, $target, "itm=$row&sort=$sort&$parameter", "", "", "link " . $class );
		}
	}

	public function link( $text, $target, $parameter = "", $free_text = "", $anker = "", $class = "link", $anker_only_for_mobile = true ) {

		if ( empty( $text ) ) {
			$text = $target;
		}
		$link_has_protocol_prefix = $this->check_link_target( $target );

		// Wenn Parameter angegeben, ein ? vorsetzen
		$parameter = $this->p->fix_parameter( $parameter, CAST_AMP, $link_has_protocol_prefix );
		if ( ! empty( $anker ) ) {
			$anker = "#" . $anker;
			//var_dump($tmp, $this->p->visitor);
			/*
            if ($this->visitor === false || (!$anker_only_for_mobile || ($anker_only_for_mobile && $this->visitor->is_mobile))
            ) {
                $anker = "#" . $anker;
            } else {
                $anker = "";
            }
            */
		}
		$parameter .= $anker;
		// Prüfe, ob ein Protokol Prefix vorhanden
		if ( $link_has_protocol_prefix ) {
			// Wenn ja
			// Erzeuge Link wie angegeben
			return "<a href=\"$target$parameter\" $free_text class=\"$class\">$text</a>";
		} else {
			// Wenn Nein
			// Füge die Anfangs Adresse der Webseite an den Anfang des Links mit ein
			return "<a href=\"/" . $this->loc->web_root . $this->lang->ldir . "$target$parameter\" $free_text class=\"$class\">$text</a>";
		}
	}

	public function get_value( $text ) {
		if ( $text === "" || is_null( $text ) ) {
			return DISPLAY_EMPTY_VALUE;
		} else {
			return $text;
		}
	}

	public function show_menu( $name, $preview = false ) {
		$m = new class_menu( $name, $preview );

		return $m->content;
	}

	public function file_link( $id, $class = null ) {
		if ( ! is_numeric( $id ) || empty( $id ) || $id < 1 ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "id not correct: '" . $id . "'" );
			$content = $this->txt( 'DOWNLOAD_NOT_FOUND' );
		} else {

			$sql    = "SELECT * FROM `" . TP . "file_download` WHERE `id` LIKE '$id' AND `active` = '1' AND `deleted` = '0'";
			$result = $this->p->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 0 ) {
				$row = $result->fetch_assoc();
				if ( is_null( $row[ 'name' . $this->lang->lnbr ] ) || empty( $row[ 'name' . $this->lang->lnbr ] ) ) {
					if ( is_null( $row[ 'name' . $this->lang->lnbrm ] ) || empty( $row[ 'name' . $this->lang->lnbrm ] ) ) {
						$row[ 'name' . $this->lang->lnbr ] = $row['filename'];
					} else {
						$row[ 'name' . $this->lang->lnbr ] = $row[ 'name' . $this->lang->lnbrm ];
					}
				}
				$content = $this->link( $row[ 'name' . $this->lang->lnbr ], "download.php", "file=" . $row['filename'], "target='_self' ", "", $class );
			} else {
				$content = $this->txt( 'DOWNLOAD_NOT_FOUND' );
			}
		}

		return $content;
	}


	public function alert_text( $type, $text, $back = false, $parameter = "", $href = "" ) {
		// type: success, info, warning, danger
		$back_txt = $this->txt( 'BACK' );
		if ( $back ) {
			if ( $back === "history" ) {
				$back = " " . $this->back_history();
			} elseif ( empty( $href ) ) {
				$back = " " . $this->back_link( $back_txt, $parameter );
			} else {
				$back = " " . $this->back_link( $back_txt, $parameter, $href );
			}
		}

		return "<div class='alert alert-" . $type . "'><p>" . $text . $back . "</p></div>";
	}

	public function back_history( $amount = "1", $type = "button", $value = "", $free_text = "", $class = "btn btn-default" ) {

		if ( empty( $value ) ) {
			$value = $this->txt( "BACK" );
		}

		$onclick = "history.back($amount);";
		if ( $type == "button" ) {
			return "<input type=\"button\" value=\"$value\" onclick=\"$onclick\" $free_text class=\"$class\" />";
		}
	}

	public function back_link( $value = "", $parameter = "", $href = "", $free_text = "", $class = "link", $check_admin = true ) {
		if ( empty( $value ) ) {
			$value = $this->txt( 'BACK' );
		}

		if ( empty( $href ) ) {
			if ( $GLOBALS['is_admin'] && $check_admin ) {
				$link = "/" . WEBROOT . ADMINDIR . $this->loc->current;
			} else {
				$link = "/" . WEBROOT . $this->loc->current;
			}

		} else {
			if ( $GLOBALS['is_admin'] && $check_admin ) {
				$link = $this->loc->httpauto_web_admin_root;
			} else {
				$link = $this->loc->httpauto_web_root;
			}
		}

		$target = $link . $href;

		$parameter = $this->p->fix_parameter( $parameter );

		return "<a href=\"$target$parameter\" $free_text class=\"$class\">$value</a>";
	}

	public function text( $name, $text = "", $size = "250", $free_text = "", $max_length = "", $class = "input_text form-control" ) {
		// Ersetze " mit HTML-Code, damit kein Fehler entsteht
		$text = $this->encode( $text );
		if ( ! empty( $size ) ) {
			$size = "width:" . $size . "px;";
		}

		return "<input type=\"text\" name=\"$name\" class=\"$class\" value=\"$text\" maxlength=\"$max_length\" $free_text style=\"$size display:inline;\" />";
	}


	public function select_timezone_options( $timezone_selected = "" ) {
		$content              = "";
		$timezone_identifiers = DateTimeZone::listIdentifiers();

		foreach ( $timezone_identifiers as $timezone ) {
			$content .= "<option";
			if ( ! empty( $timezone_selected ) ) {
				if ( strcmp( $timezone, $timezone_selected ) === 0 ) {
					$content .= " selected=selected";
				}
			} else {
				if ( isset( $GLOBALS['date_default_timezone'] ) || ! empty( $GLOBALS['date_default_timezone'] ) ) {
					if ( strcmp( $timezone, $GLOBALS['date_default_timezone'] ) === 0 ) {
						$content .= " selected=selected";
					}
				}
			}
			$content .= ">" . $timezone . "</option>";
		}

		return $content;
	}


	public function button_close( $value = "", $name = "close_button" ) {

		$content = "";

		if ( empty( $value ) ) {
			$value = $this->txt( 'CLOSE_WINDOW' );
		}

		$content .= $this->button( $value, "id='" . $name . $this->close_button_counter . "'" );

		$GLOBALS['body_footer'] .= "<script>\n" . "$('#" . $name . $this->close_button_counter . "').click(function() {\n" . "   window.close();\n" . "});\n" . "</script>\n";

		$this->close_button_counter = $this->unique_id( NOT_BIG );

		return $content;
	}

	public function mark_hits( $text, $search, $colors = false ) {
		$tmp = array();
		foreach ( $search as $s ) {
			$s = trim( $s );
			if ( $s == '' ) {
				continue;
			}
			$tmp[] = preg_quote( $s );
		}
		if ( ! $tmp ) {
			return "";
		}
		$w     = '(' . join( '|', $tmp ) . ')';
		$n     = 50;
		$r     = 5;
		$max   = 30;
		$text  = strip_tags( $text );
		$dummy = preg_match_all( "#(\b(.{0,$n}$w){1,$r}?(.+\b){0,4}?)#Usi", $text, $matches );
		if ( $dummy ) {
			$nbr    = count( $matches[1] );
			$result = '';
			for ( $i = 0; $i < $nbr; $i ++ ) {
				if ( $i == $max ) {
					break;
				}
				$result .= '... ' . $this->mark_words( $matches[1][ $i ], $search, $colors ) . ' ...<br />';
			}
		} else {
			$result = "";
		}

		return $result;
	}

	public function mark_words( $text, $words, $colors = false ) {
		if ( ! $colors ) {
			$colors = array( '#ff9999', '#ffff99', '#ff99ff', '#99ffff', '#99ff99' );
		}
		$c = 0;
		foreach ( $words as $w ) {
			$w = preg_quote( trim( $w ) );
			if ( $w == '' ) {
				continue;
			}

			$regexp      = "/($w)(?![^<]+>)/i";
			$replacement = '<b style="background-color:' . $colors[ $c ] . '">\1</b>';

			$text = preg_replace( $regexp, $replacement, $text );
			$c ++;
			if ( $c >= count( $colors ) ) {
				$c = 0;
			}
		}

		return $text;
	}


	public function add_to_current_parameter( $parameter ) {
		if ( ! empty( $this->loc->parameter_line ) ) {
			parse_str( $this->loc->parameter_line, $parameter_array );
			parse_str( $parameter, $parameter_array_add );
			$match = false;
			foreach ( $parameter_array as $var => $value ) {
				if ( isset( $parameter_array_add[ $var ] ) ) {
					$match                   = true;
					$parameter_array[ $var ] = $parameter_array_add[ $var ];
				}
			}

			if ( $match ) {
				$new_parameter_line = "";
				foreach ( $parameter_array as $var => $value ) {
					$new_parameter_line .= $var . "=" . $value . "&";
				}
				$new_parameter_line = substr( $new_parameter_line, 0, - 1 );

				return $new_parameter_line;
			} else {
				return $this->loc->parameter_line . "&" . $parameter;
			}
		} else {
			return $parameter;
		}
	}


	public function generate_preview_text( $string ) {
		$class_parse = class_parse_content::getInstance();
		$text        = $class_parse->parse( $string );
		$text        = strip_tags( $text );
		$text        = htmlspecialchars( $text, ENT_QUOTES );

		$text = $this->truncate( $text );
		$text = str_replace( "&amp;#", "&#", $text );

		return $text;
	}

	public function truncate( $string, $length = 500, $append = "&hellip;" ) {
		$string = trim( $string );

		if ( strlen( $string ) > $length ) {
			$string = wordwrap( $string, $length, "####end####" );
			$string = explode( "####end####", $string );
			$string = array_shift( $string ) . $append;
		}

		return $string;
	}

	public function form_simple( $free_text = "", $action = "", $parameter = "", $method = "POST", $class = "form-inline", $use_webroot = true ) {
		if ( $use_webroot ) {
			$webroot = "/" . WEBROOT;
			if ( $GLOBALS['is_admin'] ) {
				$webroot .= ADMINDIR;
			}
		} else {
			$webroot = "/";
		}

		// Automatischer Aufruf des Forms an die aktuelle Datei
		if ( empty( $action ) ) {
			$action = $webroot . $this->loc->uri_inside;
		} else {
			$action = $webroot . $action;
		}

		if ( empty( $parameter ) && $parameter !== false ) {
			$parameter = $this->loc->parameter_line;
		}
		$parameter_fixed = $this->p->fix_parameter( $parameter );

		$input_hidden_parameter = "";
		if ( ! empty( $parameter_fixed ) ) {
			if ( $method == "POST" ) {
				$action .= $parameter_fixed;
			} else {
				foreach ( $this->loc->parameter_array as $key => $value ) {
					$input_hidden_parameter .= $this->hidden( $key, $value );
				}
			}
		}


		// Form mit UTF-8 Zeichensatz
		return "<form $free_text action=\"" . $action . "\" method=\"$method\" accept-charset=\"UTF-8\" class=\"$class\">" . $input_hidden_parameter;
	}

	public function display_preview( $type = "box_info" ) {
		$preview_mode = false;
		if ( defined( 'PREVIEW_MODE' ) ) {
			$preview_mode = PREVIEW_MODE;
		}
		if ( $preview_mode ) {
			switch ( $type ) {
				case "box_alert":
					$GLOBALS['foot_bottom'] .= "<script>" . $this->box_alert( $this->txt( "AREA_DISABLED_PREVIEW" ) ) . "</script>\n";

					return "";
					break;
				case "box_info":
					$GLOBALS['body_header'] .= $this->alert_text_dismiss( "info", $this->txt( "AREA_DISABLED_PREVIEW" ) );

					break;
				default:
					$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong Type for display preview info: '" . $type . "'" );
					break;
			}
		}
	}

	public function get_javascript_tab_fix_in_input_elements() {
		// Thanks to: http://stackoverflow.com/questions/4379535/tab-within-a-text-area-without-changing-focus-jquery
		return "
<script>
(function($) {
    function pasteIntoInput(el, text) {
        el.focus();
        if (typeof el.selectionStart == \"number\") {
            var val = el.value;
            var selStart = el.selectionStart;
            el.value = val.slice(0, selStart) + text + val.slice(el.selectionEnd);
            el.selectionEnd = el.selectionStart = selStart + text.length;
        } else if (typeof document.selection != \"undefined\") {
            var textRange = document.selection.createRange();
            textRange.text = text;
            textRange.collapse(false);
            textRange.select();
        }

    }

    function allowTabChar(el) {
        $(el).keydown(function(e) {
            if (e.which == 9) {
                pasteIntoInput(this, \"\t\");
                e.preventDefault();
                $(el).change();
                return false;
            }
        });

        // For Opera, which only allows suppression of keypress events, not keydown
        $(el).keypress(function(e) {
            if (e.which == 9) {
                return false;
            }
        });
    }

    $.fn.allowTabChar = function() {
        if (this.jquery) {
            this.each(function() {
                if (this.nodeType == 1) {
                    var nodeName = this.nodeName.toLowerCase();
                    if (nodeName == \"textarea\" || (nodeName == \"input\" && this.type == \"text\")) {
                        allowTabChar(this);
                    }
                }
            })
        }
        return this;
    }
})(jQuery);
</script>
            ";
	}

	public function form_admin_file( $free_text = "", $action = "", $class = "form-inline", $use_webroot = true ) {
		/*
        if ($use_webroot) {
            $webroot = WEBROOT;
        } else {
            $webroot = "";
        }

        // Automatischer Aufruf des Forms an die aktuelle Datei
        if (empty($action)) {
            $action = "/" . $webroot . ADMINDIR . $this->loc->current;
        } else {
            $action = "/" . $webroot . ADMINDIR . $action;
        }
*/

		if ( $use_webroot ) {
			//$action_suffix = $this->loc->web_root;
			$action_suffix = $this->loc->web_admin_root;
		} else {
			$action_suffix = "";
		}

		if ( empty( $action ) ) {
			$action = $this->loc->uri_clean;
		}

		$action = $action_suffix . $action;

		$input_max_file_size = $this->input_max_file_size();
		$method              = "POST";
		$parameter           = "";
		$name                = "";

		if ( ! $this->get_preview_status() ) {
			return "<form action=\"/$action\" method=\"POST\" accept-charset=\"UTF-8\" $free_text class=\"$class\" enctype=\"multipart/form-data\">\n$input_max_file_size";
		} else {

			$content = "";
			$this->display_preview();
			$content .= $this->form_admin( $free_text, $action, $parameter, $name, $method, $class, $use_webroot ) . $input_max_file_size;

			return $content;
		}
	}

	public function form_admin( $free_text = "", $action = "", $parameter = "", $name = "", $method = "POST", $class = "form-inline", $use_webroot = true ) {
		// Automatischer Aufruf des Forms an die aktuelle Datei
		$form = $this->form( $free_text, $action, $parameter, $name, $method, $class, CHECK_ADMIN, $use_webroot );
		$form .= $this->hidden( "input_form_admin", "1" );

		return $form;
	}

	public function keep_form_ids() {
		return $this->hidden( "input_keep_form_ids", "3612423216325637235" );
	}


	public function form_submit_by_lang(
		$insert_breaks = true, $free_text = "", $action = "", $parameter = "", $name = "", $method = "POST",
		$class_form = "form-inline", $class_submit = "btn btn-default", $input_hidden_name = "lang"
	) {
		$content = "";

		if ( $this->lang->count_of_languages > 1 ) {
			$submit_value = $this->txt( 'EDIT_IN' ) . " ";
			$text         = $this->lang->languages[ $this->lang->lkeym ]['text'];
		} else {
			$submit_value = $this->txt( 'EDIT' );
			$text         = "";
		}

		$hidden_value = $this->lang->languages[ $this->lang->lkeym ]['number'];

		$content .= $this->form_admin( $free_text, $action, $parameter, $name, $method, $class_form );
		$content .= $this->hidden( $input_hidden_name, $hidden_value );
		$content .= $this->submit( $submit_value . $text, $free_text, $class_submit );
		$content .= "</form>";

		if ( $this->lang->count_of_languages > 1 ) {
			foreach ( $this->lang->languages as $key => $array ) {
				if ( $key == $this->lang->lkeym ) {
					continue;
				}
				if ( $insert_breaks ) {
					$content .= "<br />";
				}
				$content .= $this->form_admin( $free_text, $action, $parameter, $name, $method, $class_form );
				$text         = $this->lang->languages[ $key ]['text'];
				$hidden_value = $this->lang->languages[ $key ]['number'];
				$content .= $this->hidden( $input_hidden_name, $hidden_value );
				$content .= $this->submit( $submit_value . $text, $free_text, $class_submit );
				$content .= "</form>";
			}
		}

		return $content;
	}


	public function edit_button_by_lang( $insert_breaks = true, $id, $action = "", $do = "edit", $anchor_field_name = "anchor", $anchor_prefix = "anchor_", $do_field_name = "do", $lang_field_name = "lang", $class_submit = "btn btn-default" ) {

		$content = "";

		if ( $this->lang->count_of_languages > 1 ) {
			$submit_value = $this->txt( 'EDIT_IN' ) . " ";

			$text = $this->lang->languages[ $this->lang->lkeym ]['text'];
			if ( ! empty( $action ) ) {
				if ( strpos( $action, "&" ) === false ) {
					$action_with_lang = $action . "?lang=";
				} else {
					$action_with_lang = $action . "&lang=";
				}
			} else {
				$action_with_lang = "";
			}
		} else {
			$submit_value     = $this->txt( 'EDIT' );
			$action_with_lang = $action;
			$text             = "";
		}

		$button_free_text = "onClick='edit_entry_by_lang(this.form, \"$action_with_lang\", \"$id\");'";

		if ( ! isset( $this->edit_button_by_lang_counter[ $id ] ) ) {
			$GLOBALS['body_footer'] .= "<script>
function edit_entry_by_lang(formID, action, id)
{

    if(action != '')
        formID.action = action;

    formID." . $do_field_name . ".value = '" . $do . "';
    formID." . $anchor_field_name . ".value = '" . $anchor_prefix . "' + id;
    formID.submit();
}
</script>\n";
			$this->edit_button_by_lang_counter[ $id ] = 1;
		}

		$content .= $this->button( $submit_value . $text, $button_free_text, $class_submit );

		if ( $this->lang->count_of_languages > 1 ) {
			foreach ( $this->lang->languages as $key => $array ) {
				if ( $key == $this->lang->lkeym ) {
					continue;
				}
				if ( $insert_breaks ) {
					$content .= "<br />";
				}
				$text = $this->lang->languages[ $key ]['text'];
				if ( ! empty( $action ) ) {
					if ( strpos( $action, "&" ) === false ) {
						$action_with_lang = $action . "?lang=" . $this->lang->languages[ $key ]['number'];
					} else {
						$action_with_lang = $action . "&lang=" . $this->lang->languages[ $key ]['number'];
					}
				} else {
					$action_with_lang = "";
				}

				$button_free_text = "onClick='edit_entry_by_lang(this.form, \"$action_with_lang\", \"$id\");'";
				$content .= $this->button( $submit_value . $text, $button_free_text, $class_submit );;
			}
		}

		return $content;
	}


	public function media_ta( $name, $text = "", $uploaddir = "", $editor_type = 0, $rows = "", $freetext = "", $class = "ta ta_media", $additional_buttons = "", $file_type = null ) {

		$cb_iframe_width     = $GLOBALS["box_width"];
		$cb_iframe_height    = $GLOBALS["box_height"];
		$slave_window_width  = $GLOBALS["box_slave_width"];
		$slave_window_height = $GLOBALS["box_slave_height"];

		$class_external                = class_load_external::getInstance();
		$class_external->load_colorbox = true;
		$id                            = "id='" . $name . "'";

		$style = "";

		switch ( $editor_type ) {
			case EDITOR_SIMPLE:
				$style .= "width:100%;";
				$textarea_type = EDITOR_SIMPLE;
				$insert_type   = EDITOR_SIMPLE;

				if ( $this->enable_js_tab_function ) {
					if ( ! $this->js_tab_function_enabled ) {
						$this->js_tab_function_enabled = true;
						$GLOBALS['body_footer'] .= $this->get_javascript_tab_fix_in_input_elements();
					}

					$GLOBALS['body_footer'] .= "<script>
$(\"#" . $name . "\").allowTabChar();
</script>\n";

				}
				break;
			case EDITOR_ENHANCED:
				$class_external->load_ta_enhanced = true;
				$textarea_type                    = EDITOR_ENHANCED;
				$insert_type                      = EDITOR_ENHANCED;
				$this->init_enhanced_editor( "#" . $name );
				break;
			case EDITOR_CODE:
				$class_external->load_ta_highlighter = true;
				$textarea_type                       = EDITOR_CODE;
				$insert_type                         = EDITOR_CODE;

				$class .= " form-control";

				if ( ! is_null( $file_type ) && ! empty( $file_type ) ) {
					$GLOBALS['body_footer'] .= "<script>
$(document).ready(function() {
    var myCodeMirror_" . $this->media_ta_counter . " = CodeMirror.fromTextArea($(\"#" . $name . "\")[0], {
    lineNumbers: true,
    autoRefresh: true,
    htmlMode: true,
    selectionPointer: true,
    matchBrackets: true,\n";
					switch ( $file_type ) {

						case "js":
							$GLOBALS['body_footer'] .= "    mode: \"javascript\",\n";
							break;

						case "css":
							$GLOBALS['body_footer'] .= "    mode: \"css\",\n";
							break;

						default:
							$GLOBALS['body_footer'] .= "    mode: \"application/x-httpd-php\",\n";
							break;
					}

					$GLOBALS['body_footer'] .= "                indentUnit: 4,
    indentWithTabs: true
  });
            $('#" . $name . "').data('CodeMirrorInstance', myCodeMirror_" . $this->media_ta_counter . ");
});
        </script>\n";
				}
				break;

			default:
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong type for textarea: '$editor_type'" );

				return false;
				break;
		}

		$additional_dir = "";
		if ( $GLOBALS['is_admin'] ) {
			$additional_dir = ADMINDIR;
		}

		if ( empty( $uploaddir ) ) {
			$uploaddir = UPLOADDIR;
		}
		$uploaddir = urlencode( $uploaddir );


		if ( ! empty( $rows ) ) {
			if ( strpos( $rows, "%" ) !== false || strpos( $rows, "px" ) !== false ) {
				$style .= "height:" . $rows;
				$rows = "";
			} else {
				$rows = "rows='" . $rows . "'";
			}
		}

		$box_style = "";
		$content   = "";

		$content .= $this->button( $this->txt( 'ADD_PICTURE' ), 'id=\'open-popup-picture_' . $this->media_ta_counter . '\'', 'btn btn-default' ) . "\n";
		$content .= $this->button( $this->txt( 'ADD_FILE' ), 'id=\'open-popup-file_' . $this->media_ta_counter . '\'', 'btn btn-default' ) . "\n";
		$content .= $this->button( $this->txt( 'ADD_VIDEO' ), 'id=\'open-popup-video_' . $this->media_ta_counter . '\'', 'btn btn-default' ) . "\n";
		$content .= $this->button( $this->txt( 'ADD_AUDIO' ), 'id=\'open-popup-audio_' . $this->media_ta_counter . '\'', 'btn btn-default' ) . "\n";
		$content .= $this->button( $this->txt( 'MEDIA_MANAGER' ), 'id=\'open-popup-manager_' . $this->media_ta_counter . '\'', 'btn btn-default' ) . "\n";
		$content .= $this->button( $this->txt( 'VARIABLES' ), 'id=\'open-new-template-help_' . $this->media_ta_counter . '\'', 'btn btn-default' ) . "\n";
		$content .= $this->button( $this->txt( 'TEXT' ), 'id=\'open-new-text-help_' . $this->media_ta_counter . '\'', 'btn btn-default' ) . "\n";
		$content .= $additional_buttons;

		$content .= "<textarea name=\"$name\" $id $freetext style='$style' class=\"$class\" $rows>$text</textarea>\n";

		$GLOBALS['body_footer'] .= "<script>
$(document).ready(function() {
$('#open-popup-picture_" . $this->media_ta_counter . "').click(function() {
    $.colorbox({href:'/" . WEBROOT . $additional_dir . "upload_picture.php?uploaddir=" . $uploaddir . "&target=" . $name . "&insert_type=" . $insert_type . "', iframe:true, width:'$cb_iframe_width', height:'$cb_iframe_height' });
    });
$('#open-popup-file_" . $this->media_ta_counter . "').click(function() {
    $.colorbox({href:'/" . WEBROOT . $additional_dir . "upload_file.php?uploaddir=" . $uploaddir . "&editor=" . $name . "&textarea_type=" . $textarea_type . "', iframe:true, width:'$cb_iframe_width', height:'$cb_iframe_height' });
    });
$('#open-popup-video_" . $this->media_ta_counter . "').click(function() {
    $.colorbox({href:'/" . WEBROOT . $additional_dir . "upload_video.php?uploaddir=" . $uploaddir . "&editor=" . $name . "&textarea_type=" . $textarea_type . "', iframe:true, width:'$cb_iframe_width', height:'$cb_iframe_height' });
    });
$('#open-popup-audio_" . $this->media_ta_counter . "').click(function() {
    $.colorbox({href:'/" . WEBROOT . $additional_dir . "upload_audio.php?uploaddir=" . $uploaddir . "&editor=" . $name . "&textarea_type=" . $textarea_type . "', iframe:true, width:'$cb_iframe_width', height:'$cb_iframe_height' });
    });
$('#open-popup-manager_" . $this->media_ta_counter . "').click(function() {
    $.colorbox({href:'/" . WEBROOT . $additional_dir . "upload_manager.php?editor=" . $name . "&textarea_type=" . $textarea_type . "', iframe:true, width:'$cb_iframe_width', height:'$cb_iframe_height' });
    });
$('#open-new-template-help_" . $this->media_ta_counter . "').click(function() {
    window.open('/" . WEBROOT . $additional_dir . "insert_values.php?editor=" . $name . "&textarea_type=" . $textarea_type . "', 'Template Helper', 'width=$slave_window_width,height=$slave_window_height,scrollbars=yes,menubar=no,status=no,toolbar=no,location=no,resizeable=yes');
    });
$('#open-new-text-help_" . $this->media_ta_counter . "').click(function() {
    window.open('/" . WEBROOT . $additional_dir . "insert_content.php?editor=" . $name . "&textarea_type=" . $textarea_type . "', 'Text Helper', 'width=$slave_window_width,height=$slave_window_height,scrollbars=yes,menubar=no,status=no,toolbar=no,location=no,resizeable=yes');
    });
});
</script>
";
		$this->media_ta_counter ++;

		return $content;
	}


	protected function init_enhanced_editor( $id, $change_function_call = false, $height = "350", $width = "", $load_editor_layout = true ) {
		if ( $load_editor_layout ) {
			$editor_css  = $GLOBALS['layout_d'] . $GLOBALS['theme'] . "editor.css";
			$content_css = "content_css : \"/" . $this->loc->web_root . $editor_css . "\",\n";
		}

		if ( ! empty( $width ) ) {
			$width = "width : \"" . $width . "px\",";
		}

		if ( ! empty( $height ) ) {
			$height = "height: \"" . $height . "px\",";
		}


		$GLOBALS['foot_bottom'] .= "<script>
        tinyMCE.init({
            selector: \"textarea$id\",
            theme: \"modern\",
            language: \"de\",
                    plugins: \"advlist, anchor, autolink, charmap, code, hr, link, importcss, nonbreaking, paste, print, searchreplace, table, textcolor, visualblocks, visualchars, wordcount, contextmenu\",
                    toolbar1: \"undo, redo, |, bold, italic, underline, forecolor, backcolor, styleselect, |, table, |, alignleft, aligncenter, alignright, alignjustify, |, bullist numlist, |, outdent indent\",
                    toolbar2: \"print, searchreplace, |, visualchars, link, nonbreaking, code\",
                    database:  \"inserttable\",
                    nonbreaking_force_tab: true,
                    importcss_append: true,

            " . $content_css . "
            entity_encoding : \"raw\",
            convert_urls : false,
            $width
            $height
         });
    </script>\n";
	}


	public function media_button( $value, $target, $type = "pic", $form_submit_id = "", $hint = "", $element_number = "0", $uploaddir = "", $element_is_image = "0", $auto_resize = "0", $auto_max_x = 0, $auto_max_y = 0, $free_text = "", $class = "btn btn-default" ) {
		global $media_button_counter;
		$class_external                = class_load_external::getInstance();
		$class_external->load_colorbox = true;
		if ( empty( $uploaddir ) ) {
			$uploaddir = UPLOADDIR;
		}
		//$uploaddir = urlencode($uploaddir);

		$open_without_parameters = false;
		switch ( $type ) {
			case "pic_element_media_manager":
				$insert_type      = 10;
				$element_is_image = 1;
				$media_mod        = "upload_manager.php";
				break;
			case "select_flag":
				$insert_type = 2;
				$media_mod   = "insert_flag.php";
				break;
			case "pic_element":
				$insert_type      = 10;
				$element_is_image = 1;
			case "pic":
				$media_mod = "upload_picture.php";
				break;
			case "file":
				$media_mod = "upload_file.php";
				break;
			case "pic_one_step":
				$media_mod = "upload_picture_one_step.php";
				break;
			default:
				if ( in_array( $type, $this->custom_upload_picture_pages ) ) {
					$media_mod               = $type;
					$open_without_parameters = true;
				} else {
					$this->p->log->error( "security", __FILE__ . ":" . __LINE__, "Wrong Type: '" . $type . "'" );

					return false;
				}
		}

		if ( $GLOBALS['is_admin'] ) {
			$subdir = ADMINDIR;
		} else {
			$subdir = "";
		}

		$id = "media_button_" . ( $media_button_counter ++ );


		$resize = "";
		if ( $auto_resize ) {
			$resize = "&auto_resize=1";
		}

		if ( $open_without_parameters ) {
			$GLOBALS["body_footer"] .= "<script>
$(document).ready(function() {
    $('#$id').click(function() {
        $.colorbox({href:'/" . WEBROOT . $subdir . $media_mod . "', iframe:true, width:'" . $GLOBALS['box_width'] . "', height:'" . $GLOBALS['box_height'] . "' });
        });
});
</script>
";
		} else {
			$parameter = "uploaddir=" . $uploaddir . "&target=" . $target . "&insert_type=" . $insert_type . "&form_submit_id=" . $form_submit_id . "&element_is_image=$element_is_image&hint=" . $hint . $resize . "&element_number=$element_number&autoresize=$auto_resize&auto_max_x=$auto_max_x&auto_max_y=$auto_max_y";
			$parameter = $this->p->fix_parameter( $parameter, NO_CAST_AMP );

			$GLOBALS["body_footer"] .= "<script>
$(document).ready(function() {
    $('#$id').click(function() {
        $.colorbox({href:'/" . WEBROOT . $subdir . $media_mod . $parameter . "', iframe:true, width:'" . $GLOBALS['box_width'] . "', height:'" . $GLOBALS['box_height'] . "' });
        });
});
</script>
";
		}

		return "<input type=\"button\" value=\"$value\" id=\"$id\" $free_text class=\"$class\" />";
	}


	public function show_file_upload( $uploaddir = "" ) {
		$class_get = new class_get();


		$textarea_type = $class_get->get( "textarea_type", EDITOR_SIMPLE );
		// target = input text
		$target = $class_get->get( "target" );
		// element = input text[x]
		$element = $class_get->get( "element", 0 );
		// editor = textarea
		$editor = $class_get->get( "editor" );

		if ( empty( $uploaddir ) ) {
			$uploaddir = $this->p->get( "uploaddir" );
		}

		$parameter = "textarea_type=$textarea_type&target=$target&element=$element&editor=$editor&uploaddir=$uploaddir";
		$content   = "";

		if ( empty( $uploaddir ) ) {
			$GLOBALS['body_footer'] .= "    <script>function window_close()
        {
            parent.$.fn.colorbox.close();
        }
    </script>\n";

			$content .= $this->alert_text( "danger", $this->button( $this->txt( 'NO_UPLOAD_DIR' ) ) . " " . $this->button( $this->txt( 'CLOSE_WINDOW' ), "onclick='window_close();'" ) . "" );
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "Missing Name of upload directory" );

			return $content;
		}

		// Überprüfe, ob das Bild vom Formular richtig übertragen wurde:
		if ( isset( $_FILES['file_to_upload'] ) && ! empty( $_FILES['file_to_upload']['name'] ) ) {
			if ( $_FILES['file_to_upload']['error'] != 0 ) {
				$result = $this->p->file_upload_error( $_FILES['file_to_upload']['error'], $_FILES['file_to_upload']['name'] );
				$content .= $this->alert_text( "danger", $result, DO_BACK, $parameter );

				return $content;
			}

			// Prüfe, ob Datei:
			$result = $this->p->check_special_filetype( $_FILES['file_to_upload']['name'], "file" );
			if ( $result == false ) {
				$error = $this->txt( 'FILETYPE_NOT_ALLOWED' );
				$content .= $this->alert_text( "danger", $error, DO_BACK, $parameter );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Not allowed filetype. Filename: " . $_FILES['file_to_upload']['name'] );

				return $content;
			}

			// Bestimme die Formatierung des Dateinamens:
			$tmp               = $_FILES['file_to_upload']['name'];
			$file_name_checked = $this->p->check_filename( $uploaddir . $_FILES['file_to_upload']['name'] );

			if ( $file_name_checked == false ) {
				$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $tmp . "''";

				$content .= $this->alert_text( "danger", $error, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$tmp'" );

				return $content;
			}

			// Bestimme Zieldatei mit Pfad:
			$upload_file_destination = $this->loc->dir_root . $uploaddir . $file_name_checked;

			// Die Original Datei hochladen:
			if ( move_uploaded_file( $_FILES['file_to_upload']['tmp_name'], $upload_file_destination ) ) {
				$this->log->event( "file", __FILE__ . ":" . __LINE__, "File uploaded: " . $upload_file_destination );
			} else {
				$error = $this->txt( 'ERROR_WITH_FILE_UPLOAD_TOO_BIG' ) . "<br/>
                " . $this->txt( 'SOURCE_PATH' ) . " " . $_FILES['file_to_upload']['tmp_name'] . "<br />
                " . $this->txt( 'DESTINATION_PATH' ) . " " . $upload_file_destination;
				$content .= $this->alert_text( "danger", $error, DO_BACK, $parameter );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "File cannot be uploaded. Sourcepath: " . $_FILES['file_to_upload']['tmp_name'] . ", Destinationpath " . $upload_file_destination );

				return $content;
			}
		} else {
			$error = $this->txt( 'FILE_FORM_ERROR' );
			$content .= $this->alert_text( "danger", $error, DO_BACK, $parameter );
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "File not correct transfered from form" );

			return $content;
		}

		if ( empty( $target ) ) {
			$content .= $this->form_admin() . $this->hidden( "uploaddir", $uploaddir ) . $this->hidden( "do", "file_upload_2" ) . $this->hidden( "file", $file_name_checked ) . $this->hidden( "editor", $editor ) . $this->hidden( "textarea_type", $textarea_type ) . $this->hidden( "element", $element ) . "<h3>Datei bearbeiten</h3>
        " . $this->table() . "
        <tr>
            <td>" . $this->txt( 'TBL_FILENAME' ) . "
            </td>
            <td>" . $this->text( "file", $file_name_checked, "300", "readonly=\"readonly\"" ) . "
            </td>
        </tr>
        <tr>
            <td>" . $this->txt( 'TBL_LINK_TEXT' ) . "
            </td>
            <td>" . $this->text( "link_text", $file_name_checked, "300" ) . "
            </td>
        </tr>
        <tr>
            <td>" . $this->txt( 'TBL_TYPE_OF_OPENING' ) . "
            </td>
            <td>" . $this->select( "type_of_using" ) . "<option value=\"0\">" . $this->txt( 'OPEN_LINK_IN_SAME_WINDOW' ) . "</option><option value=\"1\">" . $this->txt( 'OPEN_LINK_IN_NEW_WINDOW' ) . "</option></select>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>" . $this->submit( $this->txt( 'ADD_FILELINK_TO_TEXT' ) ) . "
            </td>
        </tr>
        </table>
        </form>";
		} else {
			$content .= $this->save_file_upload( $file_name_checked );
		}

		return $content;
	}

	public function save_file_upload( $file = "" ) {

		$class_get = new class_get();

		$textarea_type = $class_get->get( "textarea_type" );
		if ( empty( $file ) ) {
			$file = $class_get->get( 'file', $file );
		}
		$target        = $class_get->get( 'target', null );
		$element       = $class_get->get( 'element', 0 );
		$editor        = $class_get->get( "editor" );
		$link_text     = $class_get->get( 'link_text' );
		$uploaddir     = $class_get->get( "uploaddir" );
		$type_of_using = $class_get->get( "type_of_using", 0 );

		$target_blank = "";
		if ( $type_of_using == 1 ) {
			$target_blank = " target=\"_blank\"";
		}


		switch ( $textarea_type ) {
			// EDITOR SIMPLE
			case 0:
				$text = '<a href="/' . $this->loc->web_root . $uploaddir . $file . '"' . $target_blank . '>' . $link_text . '</a>';
				$this->p->paste_rangy( $editor, $text );

				return true;
				break;
			// EDITOR ENHANCED
			case 1:
				$text = '<a href="/' . $this->loc->web_root . $uploaddir . $file . '"' . $target_blank . '>' . $link_text . '</a>';
				$this->p->paste_tinymce( $editor, $text );

				return true;
				break;
			// EDITOR CODE
			case 2:
				$text = '<a href="/' . $this->loc->web_root . $uploaddir . $file . '"' . $target_blank . '>' . $link_text . '</a>';
				$this->p->paste_codemirror( $editor, $text );

				return true;
				break;
				break;
			// INPUT TEXT
			case 10:
				$this->p->paste_element( "/" . $this->loc->web_root . $uploaddir . $file, $target, $element );

				return true;
				break;
			// GET ARRAY
			case 11:
				$tmp              = array();
				$tmp['file']      = $file;
				$tmp['text']      = $link_text;
				$tmp['uploaddir'] = $uploaddir;
				$tmp['web_root']  = "/" . $this->loc->web_root;

				return $tmp;
				break;
			// UPLOAD ONLY
			case 99:
				return true;
				break;
			default:
				$this->error      = true;
				$this->error_text = $this->alert_text_dismiss( "danger", $this->txt( 'ERROR_WRONG_INSERT_TYPE' ) );
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong insert type: '" . $textarea_type . "'" );

				return false;
		}
	}


	public function select_paste( $editor, $textarea_type, $select_id, $value = "", $trigger_change = "change_called", $name = "paste_select", $called = "opener" ) {
		$content = "";

		if ( empty( $value ) ) {
			$value = $this->txt( 'INSERT' );
		}

		$content .= $this->button( $value, "id='" . $name . $this->paste_select_counter . "'" );

		$GLOBALS['body_footer'] .= "<script>
            $('#" . $name . $this->paste_select_counter . "').click(function() {\n";
		$GLOBALS['body_footer'] .= $this->paste_textarea( $textarea_type, $editor, "$('" . $select_id . "').val()", JS_MODE, NO_COLORBOX_CLOSE, $called, $trigger_change );
		$GLOBALS['body_footer'] .= "        });
        </script>\n";

		$this->paste_select_counter = $this->unique_id( NOT_BIG );

		return $content;
	}

	public function paste_textarea( $textarea_type, $textarea_id, $text, $js_mode = false, $colorbox_close = true, $called = "top", $trigger_change = "change_called" ) {

		// called: parent (iframe) or opener (window.open) or top (Main Window)

		switch ( $textarea_type ) {
			case "0":
				return $this->p->paste_rangy( $textarea_id, $text, $js_mode, $colorbox_close, $called, $trigger_change );
				break;
			case "1":
				return $this->p->paste_tinymce( $textarea_id, $text, $js_mode, $colorbox_close, $called, $trigger_change );
				break;
			case "2":
				return $this->p->paste_codemirror( $textarea_id, $text, $js_mode, $colorbox_close, $called );
				break;
			default:
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong textarea type: '$textarea_type'" );
				$GLOBALS['body_header'] .= $this->alert_text_dismiss( "danger", $this->txt( "ERROR_WRONG_TEXTAREA_TYPE" ) );

				return "";
				break;
		}
	}

	public function button_paste( $editor, $textarea_type, $text, $trigger_change = "change_called", $value = AL_INSERT, $name = "paste_button", $called = "opener" ) {
		$content = "";

		$content .= $this->button( $value, "id='" . $name . $this->paste_button_counter . "'" );

		$GLOBALS['body_footer'] .= "<script>
            $('#" . $name . $this->paste_button_counter . "').click(function() {\n";
		$GLOBALS['body_footer'] .= $this->paste_textarea( $textarea_type, $editor, '"' . $text . '"', JS_MODE, NO_COLORBOX_CLOSE, $called, $trigger_change );
		$GLOBALS['body_footer'] .= "        });
        </script>\n";

		$this->paste_button_counter = $this->unique_id( NOT_BIG );

		return $content;
	}


	public function language_import_fields( $submit_page, $id, $active_lang_number, $parameter = "", $free_text = "" ) {
		$content = "";

		if ( $this->lang->count_of_languages > 1 ) {
			if ( empty( $parameter ) ) {
				$parameter = "id=$id&lang=$active_lang_number";
			} else {
				$parameter = "id=$id&lang=$active_lang_number&" . $parameter;
			}

			$content .= $this->form_admin( EMPTY_FREE_TEXT, $submit_page, $parameter, AUTO_NAMING, "POST", "form-inline form-box" ) . $free_text . "\n";

			$select = $this->select( "import_lang", EMPTY_FREE_TEXT, "125" ) . "\n";
			foreach ( $this->lang->languages as $lang_array ) {
				if ( strcasecmp( $active_lang_number, $lang_array['number'] ) === 0 ) {
					continue;
				}

				if ( is_null( $lang_array['number'] ) || empty( $lang_array['number'] ) ) {
					$value = "empty";
				} else {
					$value = $lang_array['number'];
				}

				$select .= "<option value='" . $value . "'>" . $lang_array['text'] . "</option>\n";
			}
			$select .= "</select> " . $this->submit( $this->txt( 'IMPORT' ) ) . "\n";

			$content .= $select . "</form>\n";
		}

		return $content;
	}

	public function language_edit_fields( $submit_page, $id, $active_lang_number, $parameter = "", $free_text = "" ) {
		$content = "";

		if ( $this->lang->count_of_languages > 1 ) {
			if ( empty( $parameter ) ) {
				$parameter = "id=$id";
			} else {
				$parameter = "id=$id&lang=$active_lang_number&" . $parameter;
			}

			// TODO: Change Method from POST to GET :(
			$content .= $this->form_admin( EMPTY_FREE_TEXT, $submit_page, $parameter, AUTO_NAMING, "POST", "form-inline form-box" ) . $free_text . "\n";

			$select = $this->select( "lang", EMPTY_FREE_TEXT, "125" ) . "\n";
			foreach ( $this->lang->languages as $lang_array ) {
				if ( strcasecmp( $active_lang_number, $lang_array['number'] ) === 0 ) {
					continue;
				}

				if ( is_null( $lang_array['number'] ) || empty( $lang_array['number'] ) ) {
					$value = "";
				} else {
					$value = $lang_array['number'];
				}

				$select .= "<option value='" . $value . "'>" . $lang_array['text'] . "</option>\n";
			}
			$select .= "</select> " . $this->submit( $this->txt( 'EDIT' ) ) . "\n";

			$content .= $select . "</form>\n";
		}

		return $content;
	}
}