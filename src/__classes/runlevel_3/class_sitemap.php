<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

require_once( DIRSETTINGSCUSTOM . "class_sitemap_settings.php" );

class class_sitemap extends class_sitemap_settings {
	protected $db;
	protected $log;
	protected $lang;
	protected $loc;
	protected $l;

	public function __construct() {
		parent::__construct();
		$this->db   = class_database::getInstance();
		$this->log  = class_logging::getInstance();
		$this->lang = class_language::getInstance();
		$this->loc  = class_location::getInstance();
		$this->l    = new class_layout();
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}

	public function update_sitemap_automatic() {
		if ( SITEMAP_AUTOMATIC ) {
			$this->start_session();
			$sitemap_content = $this->generate_sitemap_xml();

			if ( $sitemap_content['num_rows'] == 0 ) {
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "No elements for sitemap update" );
				$_SESSION['options_sitemap_updated'] = 0;
			} else {
				$_SESSION['options_sitemap_updated'] = $sitemap_content['num_rows'];
			}

			$_SESSION['options_sitemap_error'] = $sitemap_content['error'];
		}
	}

	public function generate_sitemap_xml() {
		$sitemap_content_xml = false;
		$num_rows            = 0;

		$sitemap_content['header'] = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"
  xmlns:xhtml=\"http://www.w3.org/1999/xhtml\">\n";

		$sitemap_content['footer']  = "</urlset>";
		$sitemap_content['content'] = "";

		$main_link_tmp       = array();
		$alternate_links_tmp = array();

		$link_counter = 0;
		// Index Page
		foreach ( $this->lang->languages as $lang_array ) {
			$main_link_tmp[ $link_counter ]['path']     = array( $lang_array['subdir'] => date( "Y-m-d H:i:s" ) );
			$main_link_tmp[ $link_counter ]['priority'] = "1.0";
			foreach ( $this->lang->languages as $lang_array_alternate ) {
				$alternate_links_tmp[ $link_counter ][ $lang_array_alternate['hreflang'] ] = $lang_array_alternate['subdir'];
			}
			$link_counter ++;
		}

		$sql    = "SELECT * FROM `" . TP . "pages` WHERE `deleted` = '0' AND `active` = '1' AND `in_sitemap` = '1'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				foreach ( $this->lang->languages as $lang_array ) {
					$main_link_tmp[ $link_counter ]['path']     = array( $lang_array['subdir'] . $row[ 'path' . $lang_array['number'] ] => $row['changed'] );
					$main_link_tmp[ $link_counter ]['priority'] = "0.9";
					foreach ( $this->lang->languages as $lang_array_alternate ) {
						$alternate_links_tmp[ $link_counter ][ $lang_array_alternate['hreflang'] ] = $lang_array_alternate['subdir'] . $row[ 'path' . $lang_array_alternate['number'] ];
					}
					$link_counter ++;
				}
			}
		}

		if ( isset( $GLOBALS['plugins_sitemap_add_class'] ) && sizeof( $GLOBALS['plugins_sitemap_add_class'] ) > 0 ) {
			foreach ( $GLOBALS['plugins_sitemap_add_class'] as $plugin_class ) {
				$$plugin_class = new $plugin_class;
				$$plugin_class->add_links( $link_counter, $main_link_tmp, $alternate_links_tmp );
			}
		}

		if ( sizeof( $main_link_tmp ) > 0 ) {
			$link_counter = 0;
			$this->clear_sitemap_array( $main_link_tmp, $alternate_links_tmp );

			foreach ( $main_link_tmp as $key => $array ) {
				foreach ( $array['path'] as $link => $changed ) {
					if ( ! isset( $alternate_links_tmp[ $key ] ) ) {
						$alternate = null;
					} else {
						$alternate = $alternate_links_tmp[ $key ];
					}
					$sitemap_content['content'] .= $this->sitemap_url( $link, $changed, $array['priority'], CAST_AMP, $alternate );
					$link_counter ++;
				}
			}
			$num_rows            = $link_counter;
			$sitemap_content_xml = $sitemap_content['header'] . $sitemap_content['content'] . $sitemap_content['footer'];
		}

		if ( $num_rows > 0 ) {

			$file_error = false;
			$datei      = $this->open_file( $this->sitemap_file, "w" );

			if ( $datei === false ) {
				$this->log->error( "php", __FILE__ . ":" . __LINE__, $this->sitemap_file . " konnte nicht aktualisiert werden. Konnte Datei nicht zum schreiben öffnen." );
				$file_error = $this->sitemap_file . " " . $this->txt( 'SITEMAP_ERROR_OPEN' );
			} else {
				$file_lock = new class_file_lock( $datei );
				$status    = fwrite( $datei, $sitemap_content_xml );
				$this->close_file( $datei );
				$file_lock->close();

				if ( $status === false ) {
					$this->log->error( "php", __FILE__ . ":" . __LINE__, $this->sitemap_file . " konnte nicht aktualisiert werden. Konnte nicht in Datei schreiben." );
					$file_error = $this->sitemap_file . " " . $this->txt( 'SITEMAP_ERROR_WRITING' );
				} else {

					$this->log->event( "php", __FILE__ . ":" . __LINE__, $this->sitemap_file . " " . $this->txt( 'SITEMAP_UPDATED_WITH_X_ELEMENTS_PART01' ) . " " . $num_rows . " " . $this->txt( 'SITEMAP_UPDATED_WITH_X_ELEMENTS_PART02' ) );
				}
			}
		} elseif ( is_file( DIRROOT . $this->sitemap_file ) ) {
			$result = @unlink( DIRROOT . $this->sitemap_file );
			if ( $result === false ) {
				$this->log->error( "php", __FILE__ . ":" . __LINE__, $this->sitemap_file . " konnte nicht gelöscht werden. Keine Rechte?." );
				$file_error = $this->sitemap_file . " " . $this->txt( 'SITEMAP_ERROR_DELETE' );
			}
		}

		return array( "xml" => $sitemap_content_xml, "num_rows" => $num_rows, "error" => $file_error );
	}

	protected function clear_sitemap_array( &$array, &$array_alternate ) {
		if ( is_array( $array ) && sizeof( $array ) > 1 ) {
			$size          = count( $array );
			$tmp           = $array;
			$tmp_alternate = $array_alternate;

			for ( $i = $size - 1; $i > 0; $i -- ) {
				for ( $t = $i - 1; $t >= 0; $t -- ) {
					$key_i = key( $array[ $i ]['path'] );
					$key_t = key( $array[ $t ]['path'] );

					// If same Path
					if ( strcasecmp( $key_i, $key_t ) === 0 ) {

						if ( isset( $tmp[ $i ] ) ) {
							unset( $tmp[ $i ] );
							unset( $tmp_alternate[ $i ] );
						}
					}
				}
			}
			$array           = $tmp;
			$array_alternate = $tmp_alternate;
		}

		foreach ( $array as $key => $value ) {
			foreach ( $value['path'] as $path => $changed ) {
				if ( strcasecmp( $path, "index.php" ) === 0 ) {
					unset( $tmp_alternate[ $key ] );
					unset( $array[ $key ] );
				}
			}
		}
	}

	protected function sitemap_url( $url, $lastmod, $priority = 1, $cast_amp = true, $tmp_alternate = "" ) {

		$content = "";
		$url     = $this->translate_special_characters( $url, NOT_A_FILE, DO_UNDERLINE, UNTOUCH_SLASHES );
		if ( $cast_amp ) {
			$url = $this->cast_amp( $url );
		}
		$web_root = $this->loc->web_root;

		if ( SITEMAP_HTTP_TYPE != "https" ) {
			$url = "http://" . $this->loc->server_name . $this->loc->web_root . $url;
		} else {
			$url = "https://" . $this->loc->server_name . $this->loc->web_root . $url;
		}

		$priority = number_format( $priority, 1, '.', '' );

		$lastmod      = strtotime( $lastmod );
		$lastmod_date = date( "Y-m-d", $lastmod );
		$lastmod_time = date( "H:m:i", $lastmod );

		$content .= " <url>
  <loc>$url</loc>
  <lastmod>" . $lastmod_date . "T" . $lastmod_time . "Z</lastmod>
  <priority>$priority</priority>\n";

		if ( ! is_null( $tmp_alternate ) && ! empty( $tmp_alternate ) && is_array( $tmp_alternate ) ) {
			foreach ( $tmp_alternate as $hreflang => $alternate_url ) {
				$alternate_url = $this->translate_special_characters( $alternate_url, NOT_A_FILE, DO_UNDERLINE, UNTOUCH_SLASHES );
				if ( $cast_amp ) {
					$alternate_url = $this->cast_amp( $alternate_url );
				}

				if ( SITEMAP_HTTP_TYPE != "https" ) {
					$alternate_url = "http://" . $this->loc->server_name . $this->loc->web_root . $alternate_url;
				} else {
					$alternate_url = "https://" . $this->loc->server_name . $this->loc->web_root . $alternate_url;
				}

				$content .= "    <xhtml:link rel=\"alternate\" hreflang=\"" . $hreflang . "\" href=\"" . $alternate_url . "\" />\n";
			}
		}

		$content .= " </url>\n";

		return $content;
	}

	public function display_sitemap_message() {
		$content                 = "";
		$options_sitemap_updated = - 1;
		if ( isset( $_SESSION['options_sitemap_updated'] ) ) {
			$options_sitemap_updated = $_SESSION['options_sitemap_updated'];
			unset( $_SESSION['options_sitemap_updated'] );
		}
		if ( $options_sitemap_updated > 0 && DO_SITEMAP_STATUS ) {

			$content .= $this->l->alert_text_dismiss( "success", $this->txt( "SITEMAP_UPDATED_WITH_X_ELEMENTS_PART01" ) . " " . $options_sitemap_updated . " " . $this->txt( "SITEMAP_UPDATED_WITH_X_ELEMENTS_PART02" ) );
		} elseif ( $options_sitemap_updated == 0 && DO_SITEMAP_STATUS ) {
			$content .= $this->l->alert_text_dismiss( "success", $this->txt( "ELEMENTS_FOR_SITEMAP_UPDATE" ) );
		}

		$options_sitemap_error = false;
		if ( isset( $_SESSION['options_sitemap_error'] ) ) {
			$options_sitemap_error = $_SESSION['options_sitemap_error'];
			unset( $_SESSION['options_sitemap_error'] );
		}
		if ( $options_sitemap_error ) {
			$content .= $this->l->alert_text_dismiss( "danger", $options_sitemap_error );
		}

		return $content;
	}

}