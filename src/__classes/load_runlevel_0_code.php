<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$this_dir_root = realpath( __DIR__ ) . "/";

require_once( $this_dir_root . "runlevel_0/class_load_external.php" );
require_once( $this_dir_root . "runlevel_0/class_throw_error.php" );

require_once( $this_dir_root . "runlevel_0/class_runlevel.php" );
require_once( $this_dir_root . "runlevel_0/class_root.php" );

require_once( $this_dir_root . "runlevel_0/class_uri_file_path_operations.php" );
require_once( $this_dir_root . "runlevel_0/class_file_lock.php" );
require_once( $this_dir_root . "runlevel_0/class_file_soft_lock.php" );

require_once( $this_dir_root . "runlevel_0/class_check_bot.php" );

