<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_location extends class_uri_file_path_operations {
	protected static $_instance = null;

	public $rewrite_mode;
	public $rewrite_mode_using;
	protected $lang;
	protected $log;
	protected $lang_paths;
	public $dir_root;
	public $web_root;
	public $dir_admin_root;
	public $web_admin_root;
	public $uri;
	public $uri_untouched;
	public $uri_inside;     // Without Webroot and lang subdir (but with file)
	public $uri_clean;      // and without Parameter
	public $is_https;
	public $server_name;
	public $httpauto;
	public $lang_path;
	public $httpauto_root;      // With Domain
	public $httpauto_web_root;
	public $httpauto_web_admin_root;
	public $path_array;
	public $current_file;
	public $current_filename;
	public $current_filetype;
	public $path;
	public $parameter_line;
	public $parameter_line_casted;
	public $parameter_array;
	public $current_dir;
	public $current;
	public $not_found;


	public function __construct() {
		parent::__construct();
		$this->log        = class_logging::getInstance();
		$this->lang       = class_language::getInstance();
		$this->lang_paths = $this->lang->paths;

		$this->dir_root       = DIRROOT;
		$this->web_root       = WEBROOT;
		$this->dir_admin_root = DIRROOT . ADMINDIR;
		$this->web_admin_root = WEBROOT . ADMINDIR;
		$this->server_name    = $this->remove_not_allowed_chars_in_uri( $this->get_server_name( NOT_ESCAPED ) ) . "/";
		$this->uri_untouched  = urldecode( $this->get_uri( NOT_ESCAPED ) );
		$this->uri            = $this->remove_not_allowed_chars_in_uri( $this->uri_untouched );

		if ( isset( $_SERVER['HTTPS'] ) && ( $_SERVER['HTTPS'] == '1' || strtolower( $_SERVER['HTTPS'] ) == 'on' ) ) {
			$this->is_https = true;
			$this->httpauto = "https://";
		} else {
			$this->is_https = false;
			$this->httpauto = "http://";
			if ( ONLY_HTTPS ) {
				if ( ! headers_sent() ) {
					if ( strcasecmp( $this->uri, $this->uri_untouched ) != 0 ) {
						$this->log->notice( "security", __FILE__ . ":" . __LINE__, "Https Reload and Uri Violation. Original: '" . $this->uri_untouched . "', Fixed: '" . $this->uri . "'" );
					} else {
						$this->log->notice( "notice", __FILE__ . ":" . __LINE__, "Https Reload" );
					}
					header( 'Location: https://' . $this->server_name . $this->uri );
					die();
				}
			}
		}

		// Security
		if ( strcasecmp( $this->uri, $this->uri_untouched ) != 0 ) {
			$this->log->notice( "security", __FILE__ . ":" . __LINE__, "Reload, Uri Violation. Original: '" . $this->uri_untouched . "', Fixed: '" . $this->uri . "'" );
			session_write_close();
			header( 'Location: ' . $this->httpauto . $this->server_name . $this->uri );
			die();
		}

		// Remove Web Root
		//var_dump($this->uri_inside);
		$this->uri_inside = $this->delete_first_string_match( $this->web_root, $this->uri );
		//var_dump($this->uri_inside);
		// Remove Admin Root
		if ( $GLOBALS['is_admin'] ) {
			$this->uri_inside = $this->delete_first_string_match( ADMINDIR, $this->uri_inside );
		}
		//var_dump($this->uri_inside);
		// Get first Path, set lang path and remove lang Path from uri
		$first_slash_pos = strpos( $this->uri_inside, "/" );
		if ( $first_slash_pos > 0 ) {
			$first_path = substr( $this->uri_inside, 0, $first_slash_pos + 1 );
			if ( in_array( $first_path, $this->lang_paths ) ) {
				$this->uri_inside = $this->delete_first_string_match( $first_path, $this->uri_inside );
			} else {
				$first_path = null;
			}
			$this->lang_path = $first_path;
		}

		$this->httpauto_root           = $this->httpauto . $this->server_name;
		$this->httpauto_web_root       = $this->httpauto . $this->server_name . $this->web_root;
		$this->httpauto_web_admin_root = $this->httpauto . $this->server_name . $this->web_admin_root;

		$this->extract_informations_from_uri_inside();
		// $this->path_array
		// $this->current_file
		// $this->current_filename
		// $this->current_filetype
		// $this->uri_clean
		// $this->path
		// $this->parameter_line
		// $this->parameter_line_casted
		// $this->current_dir
		// $this->current


		// Check Rewrite
		$this->rewrite_mode = false;
		if ( array_key_exists( 'HTTP_MOD_REWRITE', $_SERVER ) ) {
			if ( strtolower( $_SERVER['HTTP_MOD_REWRITE'] ) == "on" ) {
				$this->rewrite_mode = true;
			}
		} elseif ( array_key_exists( 'REDIRECT_HTTP_MOD_REWRITE', $_SERVER ) ) {
			if ( strtolower( $_SERVER['REDIRECT_HTTP_MOD_REWRITE'] ) == "on" ) {
				$this->rewrite_mode = true;
			}
		}

		//var_dump($this->db_path);
		/*
				var_dump($this->dir_root, $this->web_root, $this->web_admin_root, $this->admin_root, $this->server_name, $this->uri, $this->uri_untouched,
					$this->uri_inside, $this->http_root, $this->https_root, $this->current_file, $this->current_filetype, $this->current_filename,
					$this->parameter_line, $this->parameter_line_casted, $this->current_dir, $this->current, $this->current, $this->current,
					$this->lang, $this->rewrite_mode, $this->rewrite_mode_using, $this->https, $this->http_domain,
					$this->path, $this->end, $this->end, $this->end);
		*/
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public static function getInstance() {
		if ( null === self::$_instance ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	protected function __clone() {
	}

	protected function extract_informations_from_uri_inside() {
		// Speichere Pfad in einem Array (path_array) und als String in (path)
		$this->uri_clean   = $this->uri_inside;
		$pos_question_mark = strpos( $this->uri_clean, "?" );
		if ( ! ( $pos_question_mark === false ) ) {
			$this->uri_clean = substr( $this->uri_clean, 0, $pos_question_mark );
		}
		$i                = 0;
		$this->path_array = array();
		$found            = false;
		$uri_tmp          = $this->uri_clean;
		while ( ! ( ( $pos_slash = strpos( $uri_tmp, "/" ) ) === false ) ) // Gehe Verzeichnisse durch
		{
			if ( $pos_slash === 0 ) {
				$uri_tmp = substr( $uri_tmp, $pos_slash + 1 );
				continue;
			}
			$found                  = true;
			$this->path_array[ $i ] = substr( $uri_tmp, 0, $pos_slash ); // Speichere Verzeichnis
			$uri_tmp                = str_replace( $this->path_array[ $i ] . "/", "", $uri_tmp ); // Entferne Verzeichnis
			$i ++; // Zähler erhöhen
		}
		// End
		// Falls ein Dateiname noch übrig
		if ( strlen( $uri_tmp ) > 0 || ! $found ) {
			$this->current_file = $uri_tmp;
		} else {
			$this->current_file = "";
		}
		$this->path = "";
		foreach ( $this->path_array as $subdir ) {
			$this->path .= $subdir . "/";
		}

		$this->current_file     = $this->remove_not_allowed_file_chars( $this->current_file );
		$this->current_file     = $this->check_for_not_allowed_filenames( $this->current_file, __FILE__ . ":" . __LINE__ );
		$this->current_filetype = $this->get_uri_filetype( $this->current_file );
		$this->current_filetype = $this->check_for_not_allowed_filenames( $this->current_filetype, __FILE__ . ":" . __LINE__ );
		$this->current_filename = $this->get_uri_filename( $this->current_file );
		$this->current_filename = $this->check_for_not_allowed_filenames( $this->current_filename, __FILE__ . ":" . __LINE__ );

		$this->parameter_line        = $this->get_parameter_line( $this->uri_inside );
		$this->parameter_line_casted = $this->cast_amp( $this->parameter_line );
		parse_str( $this->parameter_line, $this->parameter_array );
		$this->current_dir = $this->get_dir( $this->uri_inside );
		$this->current     = $this->current_dir . $this->current_file;

	}

	/*
	public function correct_path_by_language($subdir)
	{
		$this->db_path = $this->delete_first_string_match($subdir, $this->db_path);
		$this->path = $this->delete_first_string_match($subdir, $this->path);
		$this->current = $this->delete_first_string_match($subdir, $this->current);
		$this->path = $this->delete_first_string_match($subdir, $this->path);
		$this->current_dir = $this->delete_first_string_match($subdir, $this->current_dir);

		if(isset($this->path_array[0]))
		{
			if(strcasecmp($subdir, $this->path_array[0]) === 0)
			{
				unset($this->path_array[0]);
				if(isset($this->path_array[1]))
				{
					$this->path_array = array_merge($this->path_array);
				}
			}
		}
	}
	*/

	public function get_complete_url() {
		return $this->httpauto_root . $this->uri;
	}

	public function compare_path_with_current( $path ) {
		if ( strcasecmp( $path, $this->current ) === 0 ) {
			return true;
		} elseif ( empty( $path ) && empty( $this->current ) ) {
			return true;
		} elseif ( strcasecmp( $path, "index.php" ) === 0 && empty( $this->current ) ) {
			return true;
		} else {
			if ( strpos( $this->current, "index.php" ) === false ) {
				return false;
			} else {
				// TODO: last index.php
				$index_path = str_replace( "/index.php", "", $this->current );
				if ( strcasecmp( $path, $index_path ) === 0 ) {
					return true;
				} else {
					$index_path = str_replace( "index.php", "", $this->current );
					if ( strcasecmp( $path, $index_path ) === 0 ) {
						return true;
					} else {
						return false;
					}
				}
			}
		}
	}
}