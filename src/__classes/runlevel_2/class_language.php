<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_language extends class_root {
	protected static $_instance = null;

	//public $main_language_number;
	public $ldir;
	public $lnbr;
	public $lnbrm;
	//public $lnbrm;
	public $lhreflang;
	public $lkey;
	public $lkeym;
	public $lhcd;
	public $lang_path;
	//public $date_format;
	public $languages;
	//public $main_language_id;
	public $multi_language_enabled;
	public $count_of_languages;

	public $count_of_languages_admin;
	public $languages_admin;

	public $lkey_admin;
	public $lnbr_admin;
	public $lhcd_admin;

	protected $log;
	protected $db;
	public $paths;
	public $is_admin;

	public function __construct() {
		// Werden durch aktuellen Seitenaufruf gesetzt:
		$this->ldir = "";       // subdir
		//$this->ldirm = "";      // subdir main
		$this->lnbr  = "";       // number
		$this->lnbrm = "";      // number main
		//$this->lnbrm = "";
		$this->lhreflang = "";   // hreflang
		//$this->hreflangm = "";  // hreflang main
		$this->lkey  = 0;        // id
		$this->lkeym = 0;       // id main
		$this->lhcd  = "";       // html code
		//$this->lhcdm = "";      // html code main
		$this->languages = array(); // all informations of each language
		$this->lang_path = array(); // for language switching

		$this->log      = class_logging::getInstance();
		$this->db       = class_database::getInstance();
		$this->paths    = array();     // all subdirs of all languages
		$this->is_admin = $GLOBALS['is_admin'];

		$this->multi_language_enabled = false;
		$this->count_of_languages     = 0;


		$sql    = "SELECT subdir FROM `" . TP . "language_settings` WHERE `active` = '1' ORDER BY `main` DESC, `pos`";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows >= 1 ) {
			$this->count_of_languages = $result->num_rows;
			while ( $row = $result->fetch_assoc() ) {
				$this->paths[] = $row['subdir'];
			}
			if ( ! in_array( $GLOBALS['fixed_lang_path'], $this->paths ) ) {
				$GLOBALS['fixed_lang_path'] = null;
			}
		}

		$this->get_language_config();
		if ( $GLOBALS['is_admin'] ) {
			$this->get_language_admin_config();
		}

	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public static function getInstance() {
		if ( null === self::$_instance ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	protected function __clone() {
	}

	public function set_lang( $loc_lang_path = null ) {
		$found_lang_id = false;
		if ( ! is_null( $GLOBALS['fixed_lang_path'] ) ) {
			$found_lang_id = $this->match_path_db( $GLOBALS['fixed_lang_path'] );
		}

		if ( $found_lang_id == false ) {
			if ( ! is_null( $loc_lang_path ) ) {
				$found_lang_id = $this->match_path_db( $loc_lang_path );
			}
		}

		if ( $found_lang_id == false ) {
			$found_lang_id = $this->get_main_language_id();
		}

		if ( $found_lang_id == false ) {
			$this->active_lang_id = null;
			$this->log->error( "lang", __FILE__ . ":" . __LINE__, "Can't find language ID. loc_lang_path: '$loc_lang_path', fixed_lang_path: '" . $GLOBALS['fixed_lang_path'] . "'" );
		} else {
			$this->active_lang_id = $found_lang_id;
		}
		$this->set_language( $this->active_lang_id );
	}

	protected function match_path_db( $lang_path ) {
		$sql    = "SELECT id FROM `" . TP . "language_settings` WHERE `active` = '1' AND `subdir` LIKE BINARY '$lang_path'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows == 1 ) {
			$row = $result->fetch_assoc();

			return $row['id'];
		} else {
			return $this->lkeym;
		}
	}

	public function get_main_language_id() {
		$sql    = "SELECT id FROM `" . TP . "language_settings` WHERE `active` = '1' ORDER BY `main` DESC LIMIT 1";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows == 1 ) {
			$row = $result->fetch_assoc();

			return $row['id'];
		} else {
			return $this->lkeym;
		}
	}


	// Language
	public function get_language_config() {
		$sql    = "SELECT * FROM `" . TP . "language_settings` WHERE `active` = '1' ORDER BY `pos`, `main` DESC";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 1 ) {
			while ( $row = $result->fetch_assoc() ) {
				$this->languages[ $row['id'] ] = array();
				$this->languages[ $row['id'] ] = array( "text"          => $row['language'],
				                                        "number"        => $row['number'],
				                                        "subdir"        => $row['subdir'],
				                                        "html_code"     => $row['html_code'],
				                                        "hreflang"      => $row['hreflang'],
				                                        "main"          => $row['main'],
				                                        "img"           => $row['img'],
				                                        "link_title"    => $row['link_title'],
				                                        "locale_time"   => $row['setlocale_time'],
				                                        "date_settings" => $row['date_settings']
				);
				if ( $row['main'] ) {
					$this->lkeym = $row['id'];
					$this->lnbrm = $row['number'];
				}
			}
			$this->multi_language_enabled = true;
			$this->count_of_languages     = $result->num_rows;
		} elseif ( $result->num_rows == 1 ) {
			$row                           = $result->fetch_assoc();
			$this->languages[ $row['id'] ] = array();
			$this->languages[ $row['id'] ] = array( "text"          => $row['language'],
			                                        "number"        => $row['number'],
			                                        "subdir"        => "",
			                                        "html_code"     => $row['html_code'],
			                                        "hreflang"      => $row['hreflang'],
			                                        "main"          => $row['main'],
			                                        "img"           => $row['img'],
			                                        "link_title"    => $row['link_title'],
			                                        "locale_time"   => $row['setlocale_time'],
			                                        "date_settings" => $row['date_settings']
			);
			$this->lkeym                   = "1";
			$this->lnbrm                   = "";
			$this->multi_language_enabled  = false;
			$this->count_of_languages      = 1;
		} else {
			$this->lkeym                  = false;
			$this->lnbrm                  = false;
			$this->multi_language_enabled = false;
			$this->count_of_languages     = 0;
		}
	}

	protected function get_language_admin_config() {

		$sql                            = "SELECT * FROM `" . TP . "language_admin_settings` WHERE `language` IS NOT NULL ORDER BY `main` ";
		$result                         = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		$this->count_of_languages_admin = $result->num_rows;
		while ( $row = $result->fetch_assoc() ) {
			$this->languages_admin[] = array( "text"          => $row['language'],
			                                  "number"        => $row['number'],
			                                  "subdir"        => $row['subdir'],
			                                  "html_code"     => $row['html_code'],
			                                  "hreflang"      => $row['hreflang'],
			                                  "main"          => $row['main'],
			                                  "img"           => $row['img'],
			                                  "link_title"    => $row['link_title'],
			                                  "locale_time"   => $row['setlocale_time'],
			                                  "date_settings" => $row['date_settings']
			);
			if ( $row['active'] ) {
				$found            = true;
				$this->lkey_admin = $row['id'];
				$this->lnbr_admin = $row['number'];
				$this->lhcd_admin = $row['html_code'];
			}
		}

		if ( ! $found ) {
			$this->lkey_admin               = false;
			$this->lnbr_admin               = false;
			$this->count_of_languages_admin = 0;
			$this->lhcd_admin               = "";
			$this->log->error( "lang", __FILE__ . ":" . __LINE__, "Can't determine active admin language. SQL: '$sql'" );
		}

	}

	public function set_language( $lang_id = null ) {
		//$lang_by_path = $this->loc->get_language();
		//var_dump($lang_by_path);
		if ( ! $GLOBALS['is_admin'] ) {
			if ( is_numeric( $lang_id ) && $lang_id > 0 && isset( $this->languages[ $lang_id ] ) && is_array( $this->languages[ $lang_id ] ) ) {
				//"text" => $row['language'],  "main" => $row['main'], "img" => $row['img'], "link_title" => $row['link_title']);
				$this->ldir      = $this->languages[ $lang_id ]['subdir'];
				$this->lnbr      = $this->languages[ $lang_id ]['number'];
				$this->lhreflang = $this->languages[ $lang_id ]['hreflang'];
				$this->lhcd      = $this->languages[ $lang_id ]['html_code'];
				$this->lkey      = $lang_id;
			} else {
				$this->set_to_main_language();
			}
		}
		if ( ! empty( $this->lkey ) && isset( $this->languages[ $this->lkey ]['date_settings'] ) && ! empty( $this->languages[ $this->lkey ]['date_settings'] ) ) {
			$GLOBALS['default_datetime_format'] = unserialize( $this->languages[ $this->lkey ]['date_settings'] );
		}
		if ( ! empty( $this->lkey ) && isset( $this->languages[ $this->lkey ]['locale_time'] ) && ! empty( $this->languages[ $this->lkey ]['locale_time'] ) ) {
			$result = setlocale( LC_TIME, $this->languages[ $this->lkey ]['locale_time'] );
			if ( $result == false ) {
				$result = setlocale( LC_TIME, "en_US.utf8" );
				if ( $result == false ) {
					$this->log->notice( "language", __FILE__ . ":" . __LINE__, "Can't set locale to: '" . $this->languages[ $this->lkey ]['locale_time'] . "'" );
				}
			}
		}
	}

	public function set_to_main_language() {
		//$this->loc->correct_path_by_language($this->languages[$this->main_language]['subdir']);
		if ( is_numeric( $this->lkeym ) && $this->lkeym > 0 && isset( $this->languages[ $this->lkeym ] ) && is_array( $this->languages[ $this->lkeym ] ) ) {
			$this->lkey = $this->lkeym;
			$this->ldir = $this->languages[ $this->lkeym ]['subdir'];
			$this->lnbr = $this->languages[ $this->lkeym ]['number'];

			$this->lhreflang = $this->languages[ $this->lkeym ]['hreflang'];
			$this->lhcd      = $this->languages[ $this->lkeym ]['html_code'];
		} else {
			$this->log->notice( "language", __FILE__ . ":" . __LINE__, "Can't set main language. lkeym = '" . $this->lkeym . "'" );
		}
	}

	public function add_to_lang_path( $path, $priority = 1, $check_multiple_languages_available = true, $lang_key = false, $lang_array = false ) {
		if ( $check_multiple_languages_available ) {
			if ( $this->count_of_languages > 1 ) {
				foreach ( $this->languages as $lang_key => $lang_array ) {
					$this->lang_path[ $priority ][ $lang_key ] = $lang_array['subdir'] . $path;
				}
			}
		} else {
			if ( ! ( $lang_key === false || $lang_array === false ) ) {
				$this->lang_path[ $priority ][ $lang_key ] = $lang_array['subdir'] . $path;
			}
		}
		ksort( $this->lang_path );
	}

	public function answer( $question ) {

		if ( empty( $question ) ) {
			return $this->txt( 'NO' );
		} else {
			return $this->txt( 'YES' );
		}
	}
}