<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


class class_sys extends class_uri_file_path_operations {
	public $p;
	public $l;
	public $country;
	public $crypt;
	public $get;
	public $menu;
	public $tbl;
	public $visitor;
	public $lang;
	public $loc;
	public $db;
	public $log;
	public $mail;
	public $runlevel;
	public $load_external;
	public $check_bot;
	public $dt;
	public $parse;

	public $img;
	public $sitemap;


	public function __construct() {
		parent::__construct();
		$this->p       = new class_page();
		$this->l       = new class_layout();
		$this->country = new class_country();
		$this->crypt   = new class_crypt();
		$this->get     = new class_get();
		$this->img     = new class_images();
		$this->menu    = new class_menu();
		//$this->sitemap = new class_sitemap();
		$this->tbl           = new class_table();
		$this->visitor       = class_visitor::getInstance();
		$this->lang          = class_language::getInstance();
		$this->loc           = class_location::getInstance();
		$this->db            = class_database::getInstance();
		$this->log           = class_logging::getInstance();
		$this->mail          = new class_mail();
		$this->runlevel      = class_runlevel::getInstance();
		$this->load_external = class_load_external::getInstance();
		$this->check_bot     = new class_check_bot();
		$this->dt            = new class_date_time();
		$this->sitemap       = new class_sitemap();
		$this->parse         = new class_parse_content();

	}
}