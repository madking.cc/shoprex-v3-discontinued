<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

require_once( DIRROOT . "__external/load_phpmailer.php" );

// Mail Klasse

class class_mail extends class_root {
	protected $message_header;
	protected $html_header;
	protected $html_footer;
	protected $text;
	protected $pmail;
	protected $log;

	public function __construct() {
		$this->pmail = new PHPMailer;
		$this->log   = class_logging::getInstance();
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public function init( $from = MAIL_FROM, $from_name = MAIL_FROM_NAME ) {
		$this->pmail->From     = $from;
		$this->pmail->FromName = $from_name;
	}

	public function address_add( $to, $to_name = "" ) {
		$this->pmail->addAddress( $to, $to_name );

		if ( SEND_BCC ) {
			$this->pmail->addBCC( MAIL_BCC, MAIL_BCC_NAME );
		}
	}

	public function file_add( $file, $name = "" ) {
		$this->pmail->addAttachment( $file, $name );
	}

	public function message_add( $message, $subject = "" ) {
		$this->pmail->isHTML( true );

		$this->pmail->Subject = utf8_decode( $subject );
		$this->pmail->Body    = utf8_decode( $message );
		$this->pmail->AltBody = utf8_decode( $this->pmail->html2text( $message, true ) );
	}

	public function send( $location = "" ) {
		if ( ! $this->pmail->send() ) {
			$this->log->mail( "error", $location, "Mail can't be send, error: " . $this->pmail->ErrorInfo );
			$result = false;
		} else {
			$this->log->mail( "success", $location, "Mail sent" );
			$result = true;
		}

		$this->pmail->ClearAttachments();
		$this->pmail->clearAllRecipients();

		return $result;
	}

	// Sende E-Mail

	public function send_short_admin( $location, $message, $subject = "" ) {
		$tmp = ADMIN_MAIL;
		if ( empty( $tmp ) ) {
			$this->log->mail( "error", $location, "Mail can't be send to admin, because no admin mail is defined" );

			return false;
		}
		// Rufe die Mail Senden Funktion auf
		$this->send_short( $location, ADMIN_MAIL, "Administrator", $message, $subject, MAIL_FROM, MAIL_FROM_NAME, NO_BCC );
	}

	// Sende E-Mail an Admin

	public function send_short( $location, $to, $to_name, $message, $subject = "", $from = MAIL_FROM, $from_name = MAIL_FROM_NAME, $send_bcc = SEND_BCC ) {
		if ( empty( $message ) ) {
			$message = "No message given";
		}

		$this->pmail->From     = $from;
		$this->pmail->FromName = $from_name;

		$this->pmail->addAddress( $to, $to_name );

		if ( $send_bcc ) {
			$this->pmail->addBCC( MAIL_BCC, MAIL_BCC_NAME );
		}

		$this->pmail->isHTML( true );

		$this->pmail->Subject = utf8_decode( $subject );
		$this->pmail->Body    = utf8_decode( $message );
		$this->pmail->AltBody = utf8_decode( $this->pmail->html2text( $message, true ) );

		if ( ! $this->pmail->send() ) {
			$this->log->mail( "error", $location, "Mail can't be send, to: " . $to . ", to name: " . $to_name . ", from: " . $from . ", name: " . $from_name . ", subject: " . $subject . ", text: " . $message . ", error: " . $this->pmail->ErrorInfo );
			$result = false;
		} else {
			$this->log->mail( "success", $location, "Mail sent, to: " . $to . ", to name: " . $to_name . ", from: " . $from . ", name: " . $from_name . ", subject: " . $subject . ", text: " . $message );
			$result = true;
		}
		$this->pmail->ClearAttachments();
		$this->pmail->clearAllRecipients();

		return $result;
	}

	public function format_text( $string ) {
		$string = str_replace( " ", '&#160;', $string );
		$string = str_replace( "\r\n", '<br />', $string );
		$string = str_replace( "\n\r", '<br />', $string );
		$string = str_replace( "\r", '<br />', $string );
		$string = str_replace( "\n", '<br />', $string );

		return $string;
	}
}