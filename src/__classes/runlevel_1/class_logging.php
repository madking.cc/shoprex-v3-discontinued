<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

// Log Klasse (als Singleton)

// push_error types: uri, value, file, sql, php
// push_* types: uri, POST, GET, log, file, sql, POST/GET

class class_logging extends class_uri_file_path_operations {
	protected static $_instance = null;

	protected $file_events;
	protected $file_errors;
	protected $file_notices;
	protected $file_mails;
	protected $logfile_suffix_bot;
	protected $logfile_type;
	protected $log_dir;
	protected $logging_enabled;
	protected $class_bot;

	protected $runlevel;

	public static function getInstance() {
		if ( null === self::$_instance ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	protected function __clone() {
	}

	public function __construct() {
		parent::__construct();

		$this->class_bot = new class_check_bot();
		$this->runlevel  = class_runlevel::getInstance();

		// todo: does not work, cause of .htaccess, never changes, cause of singleton
		if ( ! isset( $GLOBALS['logging_enabled'] ) ) {
			$GLOBALS['logging_enabled'] = true;
		}
		$this->logging_enabled = $GLOBALS['logging_enabled'];

		if ( ! isset( $GLOBALS['logging_prefix'] ) ) {
			$prefix = "";
		} else {
			$prefix = $GLOBALS['logging_prefix'];
		}

		$this->logfile_type       = ".log";
		$this->logfile_suffix_bot = "_bot";
		$this->log_dir            = $GLOBALS['log_d'];

		// Datei um Events zu loggen
		$this->file_events = $prefix . "events";
		// Datei um Fehler zu loggen
		$this->file_errors = $prefix . "errors";
		// Other events
		$this->file_notices = $prefix . "notices";
		// Datei um den Mailverkehr zu loggen
		$this->file_mails = $prefix . "mail";
		// Maximale Dateigröße bis zum archivieren
		$this->max_size = FILE_SIZE_FOR_ARCHIVE_IN_MB; // Megabyte

		// Prüfe, ob Größe erreicht und zippen der log Datei
		$this->do_archive( $this->log_dir, $this->file_events . $this->logfile_type, $this->max_size );
		$this->do_archive( $this->log_dir, $this->file_events . $this->logfile_suffix_bot . $this->logfile_type, $this->max_size );
		$this->do_archive( $this->log_dir, $this->file_notices . $this->logfile_type, $this->max_size );
		$this->do_archive( $this->log_dir, $this->file_notices . $this->logfile_suffix_bot . $this->logfile_type, $this->max_size );
		$this->do_archive( $this->log_dir, $this->file_errors . $this->logfile_type, $this->max_size );
		$this->do_archive( $this->log_dir, $this->file_errors . $this->logfile_suffix_bot . $this->logfile_type, $this->max_size );
		$this->do_archive( $this->log_dir, $this->file_mails . $this->logfile_type, $this->max_size );
		$this->do_archive( $this->log_dir, $this->file_mails . $this->logfile_suffix_bot . $this->logfile_type, $this->max_size );

		// Falls Fehler oder Meldungen in do_archive oder anderen Meldungen vor dem öffnen der Dateien aufgetaucht sind:
		if ( isset( $GLOBALS["early_event"] ) && sizeof( $GLOBALS["early_event"] ) > 0 ) {
			foreach ( $GLOBALS["early_event"] as $array ) {
				$this->event( $array["type"], $array["location"], $array["text"] );
			}
			unset( $GLOBALS["early_event"] );
		}
		if ( isset( $GLOBALS["early_error"] ) && sizeof( $GLOBALS["early_error"] ) > 0 ) {
			foreach ( $GLOBALS["early_error"] as $array ) {
				$this->error( $array["type"], $array["location"], $array["text"] );
			}
			unset( $GLOBALS["early_error"] );
		}
		if ( isset( $GLOBALS["early_notice"] ) && sizeof( $GLOBALS["early_notice"] ) > 0 ) {
			foreach ( $GLOBALS["early_notice"] as $array ) {
				$this->notice( $array["type"], $array["location"], $array["text"] );
			}
			unset( $GLOBALS["early_notice"] );
		}
	}

	protected function generate_file( $filename ) {
		if ( $this->class_bot->visitor_is_bot ) {
			$file = $this->log_dir . $filename . $this->logfile_suffix_bot . $this->logfile_type;
		} else {
			$file = $this->log_dir . $filename . $this->logfile_type;
		}

		return $file;
	}

	protected function write_data( $file, $type, $location, $text, $escape, $do_dirroot = true ) {
		if ( $do_dirroot ) {
			$file = DIRROOT . $file;
		}

		if ( ! $this->logging_enabled || ( defined( "WRITE_LOG" ) && ! WRITE_LOG ) || FILE_SIZE_FOR_ARCHIVE_IN_MB === 0 ) {
			return false;
		}

		$date = $this->get_time();
		$tmp  = array();
		$text = str_replace( array( "\r\n", "\r", "\n" ), ' ', $text );
		if ( $escape ) {
			$text = $this->escape( $text );
		}
		$tmp["type"]     = $type;
		$tmp["location"] = $location;
		$tmp["text"]     = $text;
		$tmp["date"]     = $date;
		$tmp["ip"]       = $this->get_user_ip();
		$tmp["uri"]      = $this->get_complete_uri();;
		$tmp["user_agent"] = $this->get_user_agent();
		$tmp["runlevel"]   = $this->runlevel->get_level();
		$tmp               = serialize( $tmp );

		$file_h = $this->open_file( $file, "a", NO_DIRROOT );
		if ( $file_h !== false ) {
			$file_lock = new class_file_lock( $file_h );
			fwrite( $file_h, $tmp . "\n" );
			$this->close_file( $file_h );
			$file_lock->close();
		}

		return true;
	}


	// Füge Ereignis hinzu

	public function event( $type, $location, $text, $escape = true ) {
		$file = $this->generate_file( $this->file_events );
		$this->write_data( $file, $type, $location, $text, $escape );
	}

	// Füge Fehlerereignis hinzu

	public function error( $type, $location, $text, $escape = true ) {
		$file = $this->generate_file( $this->file_errors );
		$this->write_data( $file, $type, $location, $text, $escape );
	}

	// Logge Anderes

	public function notice( $type, $location, $text, $escape = true ) {
		if ( ! defined( "WRITE_LOG_NOTICE" ) || WRITE_LOG_NOTICE ) {
			$file = $this->generate_file( $this->file_notices );
			$this->write_data( $file, $type, $location, $text, $escape );
		}
	}

	// Logge Mail
	public function mail( $type, $location, $text, $escape = true ) {
		$file = $this->generate_file( $this->file_mails );
		$this->write_data( $file, $type, $location, $text, $escape );
	}

	/*
	public function store($array)
	{
		switch($array['level'])
		{
			case "event":
				$this->event($array["type"], $array["location"], $array["text"]);
				break;
			case "notice":
				$this->notice($array["type"], $array["location"], $array["text"]);
				break;
			case "error":
				$this->error($array["type"], $array["location"], $array["text"]);
				break;
			default:
				$this->error($array["type"], $array["location"], "(Unknown Type) ".$array["text"]);
				break;

		}

	}
	*/
}
