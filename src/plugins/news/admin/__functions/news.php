<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2015}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_NEWS;

class class_content_news extends class_sys {
	public $content;

	public function __construct( $action ) {
		parent::__construct();

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );

		switch ( $action ) {
			case "news_add":
				$this->content .= $this->news_add();
				$this->content .= $this->start();
				break;
			case "search":
			case "news_edit":
				$this->content .= $this->news_edit();
				break;
			case "news_delete":
				$this->content .= $this->news_delete();
				$this->content .= $this->start();
				break;
			case "news_options":
				$this->content .= $this->news_options();
				break;
			case "news_options_update":
				$this->content .= $this->news_options_update();
				break;
			case "news_options_update_and_back":
				$this->content .= $this->news_options_update( NO_RELOAD );
				$this->content .= $this->start();
				break;
			case "news_status":
				$this->content .= $this->news_status();
				$this->content .= $this->start();
				break;

			case "content_edit":
				$this->content .= $this->content_edit();
				break;
			case "content_update":
				$content_error = $this->content_update();
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_edit();
				}
				break;
			case "content_update_no_back":
				$content_error = $this->content_update( STAY_ON_PAGE );
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
				}
				$this->content .= $this->content_edit();
				break;

			case "content_save":
				$this->content .= $this->content_save();
				$this->content .= $this->news_edit();
				break;
			case "content_delete":
				$this->content .= $this->delete_content();
				$this->content .= $this->news_edit();
				break;
			case "content_status":
				$this->content .= $this->content_status();
				$this->content .= $this->news_edit();
				break;

			case "init":
			default:
				$this->content .= $this->start();
				break;
		}


	}

	public function get_content() {
		return $this->content;
	}


	public function start() {
		$content = "";
		global $l;
		global $db;
		global $news_width;
		global $news_height;
		global $p;

		if ( isset( $_SESSION['text_id'] ) ) {
			unset( $_SESSION['text_id'] );
		}
		if ( isset( $_SESSION['text_search'] ) ) {
			unset( $_SESSION['text_search'] );
		}

		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );

		$sql    = "SELECT *, `" . TP . "news`.`active` AS `news_active` FROM `" . TP . "news` WHERE `deleted` = '0' ORDER BY `name`";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			$content .= "<h3 class='underline'>" . AL_EXISTING_BLOCK_NEWS . "</h3>\n";

			$handler = $this->tbl->tbl_init( AL_TBL_NAME, AL_TBL_HEADER, AL_TBL_NUMBER_NEWS, AL_TBL_DATE, AL_TBL_ID, AL_TBL_ACTIVE, array(
				AL_TBL_ACTION,
				"nosort"
			) );

			$entry_counter = 0;

			while ( $row = $result->fetch_assoc() ) {
				$sql2        = "SELECT `id` FROM `" . TP . "news_content` WHERE `to_id` = '" . $row['id'] . "' AND `deleted` = '0'";
				$result2     = $this->db->query( $sql2, __FILE__ . ":" . __LINE__ );
				$content_sum = $result2->num_rows;
				if ( ! isset( $content_sum ) ) {
					$content_sum = 0;
				}

				$date = $row['changed'];
				if ( $row['changed'] == "0000-00-00 00:00:00" ) {
					$date = $row['created'];
				}
				$date      = $this->dt->sqldatetime( $date );
				$timestamp = strtotime( $date );

				$parameter = "id=" . $row["id"];

				if ( $row['news_active'] == "0" ) {
					$submit_status = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				} elseif ( $row['news_active'] == "1" ) {
					$submit_status = $this->l->submit( AL_DEACTIVATE );
				}

				$active = $this->lang->answer( $row['news_active'] );

				$this->tbl->tbl_load( $handler, $row['name'], $row['header'], $content_sum, array(
					$timestamp,
					"time",
					$date
				), $row["id"], $active, $this->l->form_admin( EMPTY_FREE_TEXT, "news_content.php", $parameter ) . $this->l->submit( AL_CONTENT ) . "</form> " . $this->l->form_admin( EMPTY_FREE_TEXT, "news_options.php", $parameter ) . $this->l->submit( AL_OPTIONS ) . "</form> " . $this->l->form_admin() . $this->l->hidden( "id", $row["id"] ) . $this->l->hidden( "do", "news_status" ) . $submit_status . "</form> " . $this->l->form_admin( "id=\"form_admin_delete_news_$entry_counter\"" ) . $this->l->hidden( "do", "news_delete" ) . $this->l->hidden( "id", $row["id"] ) . $this->l->button( AL_DELETE, "id=\"button_delete_news_$entry_counter\"" ) . "</form>" );
				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_BLOCKNEWS_PART01 . " \"" . $row['name'] . "\" " . AL_DELETE_BLOCKNEWS_PART02, "#button_delete_news_$entry_counter", "form_admin_delete_news_$entry_counter", "confirm_delete_$entry_counter" );
				$entry_counter ++;
			}
			$content .= $this->tbl->tbl_out( $handler, 0 );
		}

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_BLOCKNEWS_START, "info", AL_INFORMATION );

		$content .= "<h3 class='underline'>" . AL_ADD_BLOCKNEWS . "</h3>\n";

		$new_news_name   = "";
		$new_news_header = "";
		$x_size          = $news_width;
		$y_size          = $news_height;
		$do_archive      = 1;
		$show_amount     = 0;
		$enhanced_editor = "1";

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "news_add" ) . $this->l->table() . "<tr><th>" . AL_TBL_NAME . "</th><td>" . $this->l->text( "name", $new_news_name ) . "</td></tr>
        <tr><th>" . AL_TBL_HEADER . "</th><td>" . $this->l->text( "header", $new_news_header ) . "</td></tr>
        <tr><th>" . AL_TBL_SIZE . "</th><td>" . $this->l->text( "x_size", $x_size, "100" ) . " x " . $this->l->text( "y_size", $y_size, "100" ) . " y " . AL_HINT_ALLOWED_TYPES_FOR_BLOCKNEWS_SIZE . "</td></tr>
        <tr><th>" . AL_TBL_WITH_ARCHIVE . "</th><td>" . $this->l->select_yesno( "do_archive", $do_archive ) . "</td></tr>\n" . "<tr><th>" . AL_TBL_NEWS_QUANTITY . "</th><td>" . $this->l->text( "show_amount", $show_amount ) . "</td></tr>
        <tr><th>" . AL_TBL_ENHANCED_EDITOR . "</th><td>" . $this->l->select_yesno( "enhanced_editor", $enhanced_editor ) . "</td></tr>
        <tr><td></td><td>" . $this->l->submit( AL_ADD_NEW_BLOCKNEWS ) . "</td></tr></table></form>\n";

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_BLOCKNEWS_START_ADD, "info", AL_INFORMATION );

		return $content;
	}

	public function news_add() {
		$content = "";

		$name            = $this->p->get( "name", "", NOT_ESCAPED );
		$header          = $this->p->get( "header", "" );
		$x_size          = $this->p->get( "x_size" );
		$y_size          = $this->p->get( "y_size" );
		$do_archive      = $this->p->get( "do_archive" );
		$show_amount     = $this->p->get( "show_amount", 0 );
		$enhanced_editor = $this->p->get( "enhanced_editor", 1 );

		$name = trim( $name );
		$name = $this->p->translate_special_characters( $name, NOT_A_FILE, NO_UNDERLINE );
		$name = $this->db->escape( $name );

		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME );

			return $content;
		}

		$sql    = "SELECT `id` FROM `" . TP . "news` WHERE `name` LIKE '$name' AND `deleted` = '0' ";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 0 ) {
			$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02 );

			return $content;
		}

		$this->db->ins( __FILE__ . ":" . __LINE__, "news", array(
			"name",
			"header",
			"x_size",
			"y_size",
			"do_archive",
			"show_amount",
			"created",
			"enhanced_editor"
		), array( $name, $header, $x_size, $y_size, $do_archive, $show_amount, "NOW()", $enhanced_editor ) );

		$id = $this->db->get_insert_id();

		$content .= $this->l->alert_text_dismiss( "success", AL_BLOCKNEWS_ADDED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "New newsblock saved, name: " . $name . ", Id: " . $id );

		return $content;
	}

	public function news_options() {
		$content = "";

		$GLOBALS['admin_subtitle'] = AL_NEWS_OPTIONS;

		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );

		$id = $this->p->get( "id" );

		$parameter = "id=" . $id;

		if ( ! ( is_numeric( $id ) && $id > 0 ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_BLOCKNEWS );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= start();

			return $content;
		}

		$sql    = "SELECT * FROM `" . TP . "news` WHERE `id`='$id' AND `deleted` = '0' ";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 1 ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_BLOCKNEWS );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID not found in database, id: " . $id );
			$content .= start();

			return $content;
		}

		$row = $result->fetch_assoc();

		$content .= "<h3 class='underline'>" . AL_BLOCKNEWS_OPTIONS_OF . " \"" . $row['name'] . "\"</h3>\n";

		$content .= $this->l->form_admin( "", "news_options.php", $parameter ) . $this->l->hidden( "id", $id ) . $this->l->hidden( "do", "news_options_update" ) . $this->l->hidden( "old_name", $row['name'] ) . $this->l->table() . "<tr><th>" . AL_TBL_NAME . "</th><td colspan='3'>" . $this->l->text( "name", $row['name'] ) . "</td></tr>\n";

		if ( $this->lang->count_of_languages <= 1 ) {
			$content .= "<tr><th>" . AL_TBL_HEADER . "</th><td colspan='3'>" . $this->l->text( "header", $row[ 'header' . $this->p->lang->lnbrm ] ) . "</td></tr>";
		} else {
			foreach ( $this->lang->languages as $lang_array ) {
				$content .= "<tr><th>" . AL_TITLE_IN . " " . $lang_array['text'] . ":</th><td colspan='3'>" . $this->l->text( "header" . $lang_array['number'], $row[ 'header' . $lang_array['number'] ] ) . "</td></tr>";
			}
		}

		$content .= "
        <tr><th>" . AL_TBL_SIZE . "</th><td colspan='3'>" . $this->l->text( "x_size", $row['x_size'], "100" ) . " x " . $this->l->text( "y_size", $row['y_size'], "100" ) . " y " . AL_HINT_ALLOWED_TYPES_FOR_BLOCKNEWS_SIZE . "</td></tr>
        <tr><th>" . AL_TBL_WITH_ARCHIVE . "</th><td>" . $this->l->select_yesno( "do_archive", $row['do_archive'] ) . "</td></tr>\n" . "<tr><th>" . AL_TBL_NEWS_QUANTITY . "</th><td>" . $this->l->text( "show_amount", $row['show_amount'] ) . "</td></tr>
    <tr><th>" . AL_TBL_ENHANCED_EDITOR . "</th><td colspan='3'>" . $this->l->select_yesno( "enhanced_editor", $row['enhanced_editor'] ) . "</td></tr>
    <tr><td></td><td colspan='3'>" . $this->l->submit( AL_SAVE ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='save_and_back(this.form);'" ) . " " . $this->l->reload_button( "news_options.php", "id=$id", AL_RELOAD ) . " " . $this->l->back_button( AL_BACK, "news.php" ) . "
    </td></tr></table></form>\n";

		$GLOBALS['body_footer'] .= "<script>
    function save_and_back(formID)
    {
        formID.do.value = 'news_options_update_and_back';
        formID.action = '/" . WEBROOT . ADMINDIR . "news.php';
        formID.submit();
    }
    </script>
    ";

		return $content;
	}

	public function news_options_update( $reload = true ) {
		$content = "";

		$id   = $this->p->get( "id" );
		$name = $this->p->get( "name", "", NOT_ESCAPED );

		if ( $this->lang->count_of_languages <= 1 ) {
			$header = $this->p->get( "header", "" );
		} else {
			foreach ( $this->lang->languages as $lang_array ) {
				${"header" . $lang_array['number']} = $this->p->get( "header" . $lang_array['number'] );
			}
		}

		$old_name        = $this->p->get( "old_name" );
		$x_size          = $this->p->get( "x_size" );
		$y_size          = $this->p->get( "y_size" );
		$do_archive      = $this->p->get( "do_archive" );
		$show_amount     = $this->p->get( "show_amount" );
		$enhanced_editor = $this->p->get( "enhanced_editor", 1 );

		$name = trim( $name );
		$name = $this->p->translate_special_characters( $name, NOT_A_FILE, NO_UNDERLINE );
		$name = $this->db->escape( $name );

		if ( ! ( is_numeric( $id ) && $id > 0 ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_UPDATE_BLOCKNEWS_OPTIONS );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME );

			return $content;
		}

		$sql    = "SELECT `id` FROM `" . TP . "news` WHERE `name` LIKE '$name' AND `deleted` = '0' ";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 0 && $name != $old_name ) {
			$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02 );

			return $content;
		}

		if ( $this->lang->count_of_languages <= 1 ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "news", array(
				"name",
				"header",
				"x_size",
				"y_size",
				"do_archive",
				"show_amount",
				"enhanced_editor"
			), array( $name, $header, $x_size, $y_size, $do_archive, $show_amount, $enhanced_editor ), "`id` ='$id'" );
		} else {
			$array_keys   = array( "name", "x_size", "y_size", "do_archive", "show_amount", "enhanced_editor" );
			$array_values = array( $name, $x_size, $y_size, $do_archive, $show_amount, $enhanced_editor );
			foreach ( $this->lang->languages as $lang_array ) {
				$array_keys[]   = "header" . $lang_array['number'];
				$array_values[] = ${"header" . $lang_array['number']};
			}
			$this->db->upd( __FILE__ . ":" . __LINE__, "news", $array_keys, $array_values, "`id` ='$id'" );
		}

		$this->log->event( "log", __FILE__ . ":" . __LINE__, "newsblock updated, name: " . $name . ", id: " . $id );

		$_SESSION['options_updated'] = 1;
		if ( $reload ) {
			$this->l->reload_js( "", "id=$id" );
		}

		return $content;
	}

	public function news_delete() {
		$content = "";

		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {

			$this->db->upd( __FILE__ . ":" . __LINE__, "news", array( "deleted" ), array( true ), "`id` ='$id'" );
			$this->db->upd( __FILE__ . ":" . __LINE__, "news_content", array( "deleted" ), array( true ), "`to_id` ='$id'" );
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_BLOCKNEWS );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_BLOCKNEWS_DELETED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "news deleted, id: " . $id );

		return $content;
	}

	public function news_edit() {
		$content = "";

		$GLOBALS['admin_subtitle'] = AL_NEWS_CONTENT;

		$content .= $this->l->display_message_by_session( 'content_updated', AL_NEWS_UPDATED );

		$reset_search = $this->p->get( "reset_search", 0 );
		$search       = $this->p->get_session( "search", "", "text_search", $reset_search );

		$change_editortype = $this->p->get( "change_editortype", "-1" );

		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT * FROM `" . TP . "news` WHERE `id`='" . $id . "' AND `deleted` = '0' ";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_OPEN_BLOCKNEWS );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= start();

			return $content;
		}

		if ( $result->num_rows != 1 ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_OPEN_BLOCKNEWS );
			$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
			$content .= start();

			return $content;
		}

		$row = $result->fetch_assoc();

		$news_global_enhanced_editor = $row['enhanced_editor'];

		if ( $change_editortype == "-1" ) {
			$content_to_add      = "";
			$header_to_add       = "";
			$datetime_to_add     = "";
			$archive_date_to_add = "";
		} else {
			$content_to_add      = $this->p->get( "content_to_add", "", false );
			$header_to_add       = $this->p->get( "header_to_add", "", false );
			$datetime_to_add     = $this->p->get( "datetime_to_add", "", false );
			$archive_date_to_add = $this->p->get( "archive_date_to_add", "", false );
		}

		$parameter = "id=" . $row["id"];

		$content .= $this->l->form_admin( "id='form_content_save' name='form_action'", DEFAULT_ACTION, $parameter ) . $this->l->hidden( "do", "content_save" ) . $this->l->back_button( AL_BACK, "news.php" ) . "<br /><br />" . $this->l->table() . "
        <tr><td>" . AL_TBL_HEADER . "</td><td>" . $this->l->text( "header_to_add", $header_to_add ) . "</td></tr>
        <tr><td>" . AL_TBL_CONTENT . "</td><td>";

		$rows = "";
		if ( $news_global_enhanced_editor ) {
			if ( $change_editortype == "-1" ) {
				if ( $row['enhanced_editor'] ) {
					$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
					$enhanced_editor = EDITOR_ENHANCED;
				} else {
					$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
					$rows            = "20";
					$enhanced_editor = EDITOR_SIMPLE;
				}
			} elseif ( $change_editortype ) {
				$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
				$enhanced_editor = EDITOR_ENHANCED;
			} else {
				$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
				$rows            = "20";
				$enhanced_editor = EDITOR_SIMPLE;
			}

			$content .= $this->l->hidden( "enhanced_editor", $enhanced_editor ) . $this->l->hidden( "change_editortype", "-1" );

			$action_js = "news_edit";

			$GLOBALS['body_footer'] .= "
<script>
    $(\"#change_editor_enhanced\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('1');
        $(\"form[name=form_action]\").submit();
    });
    $(\"#change_editor_simple\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('0');
        $(\"form[name=form_action]\").submit();
    });

</script>
";
		} else {
			$enhanced_editor = EDITOR_SIMPLE;
			$rows            = "20";
			$content .= $this->l->hidden( "enhanced_editor", $row['enhanced_editor'] );
		}

		$content .= $this->l->media_ta( "content_to_add", $content_to_add, UPLOADDIR, $enhanced_editor, $rows ) . "</td></tr>
        <tr><td>" . AL_TBL_DATE . "</td><td>" . $this->l->datetime( "datetime_to_add", $datetime_to_add ) . "</td></tr>
        <tr><td>" . AL_TBL_EXPIRATION_DATE . "</td><td>" . $this->l->datetime( "archive_date_to_add", $archive_date_to_add ) . "</td></tr>
        <tr><td></td><td>" . $this->l->submit( AL_ADD_NEWS ) . "</td></tr>
        </table>
        </form>\n";

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_NEWS_EDIT, "info", AL_INFORMATION );

		$search_text = "";
		if ( ! empty( $search ) ) {
			$content .= "<h3 class='underline'>" . AL_NEWS_SEARCH_RESULTS . "</h3>\n";
			$search_text = "AND (" . $this->db->search( "header", $search ) . " OR " . $this->db->search( "text", $search ) . ")";
		} else {
			$content .= "<h3 class='underline'>" . AL_NEWS . "</h3>\n";
		}

		$sql2    = "SELECT * FROM `" . TP . "news_content` WHERE `to_id` = '" . $row['id'] . "' AND `deleted` = '0' $search_text";
		$result2 = $this->db->query( $sql2, __FILE__ . ":" . __LINE__ );

		if ( ! empty( $search ) || $result2->num_rows != 0 ) {
			$content .= " " . $this->l->form_admin( "name='form_search'" ) . $this->l->text( "search", $search, "250", "id='search_text'" ) . $this->l->hidden( "do", "search" ) . " " . $this->l->hidden( "id", $id ) . " " . $this->l->submit( AL_SEARCH, "id='search'" ) . "
                <span id='message' class='error'>| " . AL_ERROR . ": " . AL_MISSING_SEARCH_TEXT . "</span>\n";
			$content .= "    </form>\n";

			if ( ! empty( $search ) ) {
				$content .= $this->l->form_admin() . $this->l->hidden( "reset_search", "1" ) . $this->l->hidden( "id", $id ) . $this->l->submit( AL_RESET );
				$content .= "    </form>\n";
			}

			$GLOBALS['body_footer'] .= "
        <script>

            $(document).ready(function () {
                $(\"#message\").hide();
            });
            $(\"form[name=form_search]\").submit(function( event ) {
                var val = $(\"input[name=search]\").val();
                if(val == '')
                {
                    $(\"#message\").fadeIn();
                    event.preventDefault();
                }
                else
                {
                    $(\"#message\").fadeOut();
                }
            });
             </script>\n";

			$handler = $this->tbl->tbl_init( AL_TBL_TEXT, AL_TBL_DATE, AL_TBL_ID, AL_TBL_HEADER, AL_TBL_ARCHIVE, array(
				AL_TBL_CHARS,
				"nosort"
			), AL_TBL_ACTIVE, array( AL_TBL_ACTION, "nosort" ) );

			if ( $result2->num_rows > 0 ) {
				$entry_counter = 0;
				while ( $row2 = $result2->fetch_assoc() ) {
					$text = $this->l->fix_ta( $row2['text'] );
					$text = $this->l->ta( "text_" . $entry_counter, $text, "500", "180", "readonly=\"readonly\"" );

					$datetime           = $this->dt->sqldatetime( $row2['datetime'] );
					$timestamp_datetime = strtotime( $row2['datetime'] );

					if ( $row2['archive_date'] != "0000-00-00 00:00:00" ) {
						$archive_date           = $this->dt->sqldatetime( $row2['archive_date'] );
						$timestamp_archive_date = strtotime( $row2['archive_date'] );
					} else {
						$archive_date           = AL_NOT_SPECIFIED_SHORT;
						$timestamp_archive_date = 0;
					}

					if ( empty( $row2['header'] ) ) {
						$header = AL_NOT_SPECIFIED_SHORT;
					} else {
						$header = $row2['header'];
					}

					$active = $this->lang->answer( $row2['active'] );

					$parameter_content = $parameter . "&id_content=" . $row2["id"];

					if ( $row2['active'] == "0" ) {
						$submit_status = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
					} elseif ( $row2['active'] == "1" ) {
						$submit_status = $this->l->submit( AL_DEACTIVATE );
					}

					$chars = $this->p->strlen_language_all( "news_content", "text", $row2['id'] );

					$this->tbl->tbl_load( $handler, $text, array(
						$timestamp_datetime,
						"time",
						$datetime
					), $row2['id'], $header, array( $timestamp_archive_date, "time", $archive_date ), $chars, $active,
						$this->l->form_submit_by_lang( INSERT_BREAKS, EMPTY_FREETEXT, "news_content_edit.php", $parameter_content ) . "<br />" .
						$this->l->form_admin( "id=\"form_admin_delete_news_$entry_counter\"" ) . $this->l->hidden( "do", "content_delete" ) . $this->l->hidden( "id_content", $row2["id"] ) . $this->l->button( AL_DELETE, "id=\"button_delete_news_$entry_counter\"" ) . "</form>" .
						$this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "id_content", $row2["id"] ) . $this->l->hidden( "do", "content_status" ) . $submit_status . "</form> " );

					$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_NEWS_PART01 . " " . $datetime . " " . AL_DELETE_NEWS_PART02, "#button_delete_news_$entry_counter", "form_admin_delete_news_$entry_counter", "confirm_delete_$entry_counter" );

					$entry_counter ++;
				}
				$content .= $this->tbl->tbl_out( $handler, 1 );
			} else {
				if ( ! empty( $search ) ) {
					$content .= "<h3>" . AL_NO_MATCHES . "</h3>";
				} else {
					$content .= "<h3>" . AL_NO_ENTRIES . "</h3>";
				}
			}
		} else {
			$content .= "<h3>" . AL_NO_ENTRIES . "</h3>";
		}

		return $content;
	}

	public function content_edit() {
		$content = "";

		$GLOBALS['admin_subtitle'] = AL_EDIT_NEWS_CONTENT;

		$content .= $this->l->display_message_by_session( 'content_updated', AL_CONTENT_UPDATED );

		$lang_number        = $this->p->get( "lang" );
		$import_lang_number = $this->p->get( "import_lang" );
		$change_editortype  = $this->p->get( "change_editortype", - 1 );

		$id    = $this->p->get( "id_content" );
		$to_id = $this->p->get( "id" );

		if ( ! ( is_numeric( $to_id ) && $to_id > 0 ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_CONTENT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "TO_ID value is empty or not numeric: " . $to_id );
			$content .= news_edit();

			return $content;
		} else {
			$sql    = "SELECT `enhanced_editor` FROM `" . TP . "news` WHERE `id`='" . $to_id . "' AND `deleted` = '0' ";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 1 ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CONTENT_NOT_FOUND );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
				$content .= news_edit();

				return $content;
			}
			$row                         = $result->fetch_assoc();
			$news_global_enhanced_editor = $row['enhanced_editor'];
		}

		if ( $change_editortype == "-1" ) {
			if ( is_numeric( $id ) && $id > 0 ) {
				$sql    = "SELECT * FROM `" . TP . "news_content` WHERE `id`='" . $id . "' AND `deleted` = '0'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				if ( $result->num_rows != 1 ) {
					$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CONTENT_NOT_FOUND );
					$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
					$content .= news_edit();

					return $content;
				} else {
					$row             = $result->fetch_assoc();
					$row['datetime'] = $this->dt->sqldatetime( $row['datetime'] );
				}
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_CONTENT );
				$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
				$content .= news_edit();

				return $content;
			}
		} else {
			$row['enhanced_editor']         = $this->p->get( "enhanced_editor", $news_global_enhanced_editor );
			$row[ 'text' . $lang_number ]   = $this->p->get( "content_to_update", "", false );
			$row[ 'header' . $lang_number ] = $this->p->get( "header_to_update", "", false );
			$row['datetime']                = $this->p->get( "datetime_to_update", "", false );
			$row['archive_date']            = $this->p->get( "archive_date_to_update", "", false );
		}

		$parameter_back    = "id=" . $to_id;
		$parameter_content = $parameter_back . "&id_content=$id";

		if ( ! is_null( $import_lang_number ) ) {
			if ( strcasecmp( $import_lang_number, "empty" ) === 0 ) {
				$import_lang_number = "";
			}
			$row[ 'text' . $lang_number ]   = $row[ 'text' . $import_lang_number ];
			$row[ 'header' . $lang_number ] = $row[ 'header' . $import_lang_number ];
		}

		$content .= $this->l->table();

		if ( $this->lang->count_of_languages > 1 ) {
			$content .= "<tr>";
			$content .= "<td>" . AL_TBL_LANGUAGE_FUNCTIONS . "</td><td>";
			$content .= $this->l->language_import_fields( "news_content_edit.php", $id, $lang_number, $parameter_content . "&lang=$lang_number" ) .
			            $this->l->language_edit_fields( "news_content_edit.php", $id, $lang_number, $parameter_content ) . "</td>
            </tr>";
		}

		$content .=

			"<tr><td></td><td>" . $this->l->form_admin( "id='form_content_save' name='form_action'", "news_content_edit.php", $parameter_back ) .
			$this->l->hidden( "id_content", $id ) . $this->l->hidden( "id", $to_id ) . $this->l->hidden( "do", "content_update" ) . $this->l->hidden( "do_no_back", "content_update_no_back" ) .
			$this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='check_submit(this.form, false)'" ) . " " .
			$this->l->reload_button( "news_content_edit.php", $parameter_content . "&lang=$lang_number", AL_RELOAD ) . " " . $this->l->back_button( AL_BACK, "news_content.php", $parameter_back ) . "
            </td></tr>";

		$content .= "<tr><td>" . AL_TBL_ID . "</td><td>" . $id . "</td></tr>\n";

		if ( $this->lang->count_of_languages > 1 ) {
			$content .= "<tr><td>" . AL_TBL_CURRENT_LANGUAGE . "</td><td><strong>" . $this->p->get_language_text( $lang_number ) . $this->l->hidden( "lang", $lang_number ) . "</strong></td></tr>\n";
		} else {
			$content .= $this->l->hidden( "lang", $this->p->lang->lnbrm );
		}

		$content .= "<tr><td>" . AL_TBL_HEADER . "</td><td>" . $this->l->text( "header_to_update", $row[ 'header' . $lang_number ] ) . "</td></tr>
        <tr><td>" . AL_TBL_CONTENT . "</td><td>";

		$rows = "";
		if ( $news_global_enhanced_editor ) {
			if ( $change_editortype == "-1" ) {
				if ( $row['enhanced_editor'] ) {
					$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
					$enhanced_editor = EDITOR_ENHANCED;
				} else {
					$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
					$rows            = "20";
					$enhanced_editor = EDITOR_SIMPLE;
				}
			} elseif ( $change_editortype ) {
				$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
				$enhanced_editor = EDITOR_ENHANCED;
			} else {
				$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
				$rows            = "20";
				$enhanced_editor = EDITOR_SIMPLE;
			}

			$content .= $this->l->hidden( "enhanced_editor", $enhanced_editor ) . $this->l->hidden( "change_editortype", "-1" );

			$action_js = "content_edit";

			$GLOBALS['body_footer'] .= "
<script>
    $(\"#change_editor_enhanced\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('1');
        $(\"form[name=form_action]\").submit();
    });
    $(\"#change_editor_simple\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('0');
        $(\"form[name=form_action]\").submit();
    });

</script>
";
		} else {
			$enhanced_editor = EDITOR_SIMPLE;
			$rows            = "20";
			$content .= $this->l->hidden( "enhanced_editor", $row['enhanced_editor'] );
		}

		if ( empty( $row['archive_date'] ) || $row['archive_date'] == "0000-00-00 00:00:00" ) {
			$row['archive_date'] = "";
		}

		$GLOBALS['body_footer'] .= "<script>
    function check_submit(formID, no_back)
    {
        if(no_back)
        {
            formID.do.value = formID.do_no_back.value;
        }
        formID.submit();
    }
    </script>
    ";

		$content .= $this->l->media_ta( "content_to_update", $row[ 'text' . $lang_number ], UPLOADDIR, $enhanced_editor, $rows ) . "</td></tr>

        <tr><td>" . AL_TBL_DATE . "</td><td>" . $this->l->datetime( "datetime_to_update", $row['datetime'] ) . "</td></tr>
        <tr><td>" . AL_TBL_EXPIRATION_DATE . "</td><td>" . $this->l->datetime( "archive_date_to_update", $row['archive_date'] ) . "</td></tr>
        <tr><td></td><td>" . $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " .
		            $this->l->button( AL_SAVE_AND_BACK, "onclick='check_submit(this.form, false)'" ) . " " .
		            $this->l->reload_button( "news_content_edit.php", $parameter_content, AL_RELOAD ) . " " .
		            $this->l->back_button( AL_BACK, "news_content.php", $parameter_back ) . "</td></tr>
        </table>
        </form>\n";

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_NEWS_EDIT, "info", AL_INFORMATION );

		return $content;
	}

	public function content_update( $stay_on_page = false ) {

		$content = "";

		$lang_number = $this->p->get( "lang" );

		$content_to_update      = $this->p->get( "content_to_update", "" );
		$header_to_update       = $this->p->get( "header_to_update", "" );
		$datetime_to_update     = $this->p->get( "datetime_to_update" );
		$archive_date_to_update = $this->p->get( "archive_date_to_update", "" );
		$id                     = $this->p->get( "id_content" );
		$to_id                  = $this->p->get( "id" );
		$enhanced_editor        = $this->p->get( "enhanced_editor", EDITOR_ENHANCED );

		if ( empty( $datetime_to_update ) ) {
			$datetime_to_update = date( "d.m.Y h:i:s" );
		}
		$datetime_to_update = $this->dt->sqldatetime( $datetime_to_update, "out" );

		if ( ! empty( $archive_date_to_update ) ) {
			$archive_date_to_update = $this->dt->sqldatetime( $archive_date_to_update, "out" );
		} else {
			$archive_date_to_update = null;
		}

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_SAVE_CONTENT, DO_BACK, NO_PARAMETER, "news_content_edit.php" );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty" );

			return $content;
		} else {

			$this->db->upd( __FILE__ . ":" . __LINE__, "news_content", array(
				"text" . $lang_number,
				"header" . $lang_number,
				"datetime",
				"archive_date",
				"enhanced_editor"
			), array(
				$content_to_update,
				$header_to_update,
				$datetime_to_update,
				$archive_date_to_update,
				$enhanced_editor
			), "`id` ='$id'" );

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content updated, id: " . $id . ", content: " . $content_to_update );

			$_SESSION['content_updated'] = 1;

			if ( ! $stay_on_page ) {
				$this->l->reload_js( "news_content.php", "id=$to_id" );
			}

			return $content;
		}
	}

	public function content_save() {

		$lang_number = $this->p->get( "lang" );

		$content = "";

		$content_add         = $this->p->get( "content_to_add", "" );
		$header_to_add       = $this->p->get( "header_to_add", "" );
		$datetime_to_add     = $this->p->get( "datetime_to_add" );
		$archive_date_to_add = $this->p->get( "archive_date_to_add", "" );
		$id                  = $this->p->get( "id" );
		$enhanced_editor     = $this->p->get( "enhanced_editor", EDITOR_ENHANCED );

		if ( empty( $datetime_to_add ) ) {
			$datetime_to_add = date( "d.m.Y h:i:s" );
		}
		$datetime_to_add = $this->dt->sqldatetime( $datetime_to_add, "out" );

		if ( ! empty( $archive_date_to_add ) ) {
			$archive_date_to_add = $this->dt->sqldatetime( $archive_date_to_add, "out" );
		} else {
			$archive_date_to_add = null;
		}

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_SAVE_CONTENT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty" );

			return $content;
		} else {
			$this->db->ins( __FILE__ . ":" . __LINE__, "news_content", array(
				"to_id",
				"text" . $lang_number,
				"header" . $lang_number,
				"datetime",
				"archive_date",
				"enhanced_editor",
				"created"
			), array(
				$id,
				$content_add,
				$header_to_add,
				$datetime_to_add,
				$archive_date_to_add,
				$enhanced_editor,
				"NOW()"
			) );

			$content .= $this->l->alert_text_dismiss( "success", AL_CONTENT_SAVED );
			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content added to news, id: " . $id . ", content: " . $content_add );

			return $content;
		}
	}

	public function delete_content() {

		$content = "";

		$id_content = $this->p->get( "id_content" );
		$id         = $this->p->get( "id" );

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_NEWS );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty" );

			return $content;
		} elseif ( empty( $id_content ) || ! is_numeric( $id_content ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_NEWS );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "id_content value is empty" );

			return $content;
		} else {
			$sql    = "SELECT `text` FROM `" . TP . "news_content` WHERE id = '$id_content' AND `deleted` = '0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 1 ) {
				$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_NEWS );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "Content ID $id_content not found" );

				return $content;
			} else {
				$this->db->upd( __FILE__ . ":" . __LINE__, "news_content", array( "deleted" ), array( true ), "`id` ='$id_content'" );

				$content .= $this->l->alert_text_dismiss( "success", AL_NEWS_DELETED );
				$this->log->event( "log", __FILE__ . ":" . __LINE__, "News deleted, id: " . $id );

				return $content;
			}
		}
	}

	public function content_status() {

		$content = "";

		$id_content = $this->p->get( "id_content" );
		$id         = $this->p->get( "id" );

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_NEWS_STATUS );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty" );

			return $content;
		} elseif ( empty( $id_content ) || ! is_numeric( $id_content ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_NEWS_STATUS );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "id_content value is empty" );

			return $content;
		} else {
			$sql    = "SELECT `active` FROM `" . TP . "news_content` WHERE `id` = '$id_content' AND `deleted` = '0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 1 ) {
				$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_NEWS_STATUS );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "Content ID $id_content not found" );

				return $content;
			} else {
				$row = $result->fetch_assoc();

				if ( $row['active'] ) {
					$this->db->upd( __FILE__ . ":" . __LINE__, "news_content", array( "active" ), array( false ), "`id` ='$id_content'" );
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content status in news changed, id: " . $id_content . ", new status: inactive" );
				} else {
					$this->db->upd( __FILE__ . ":" . __LINE__, "news_content", array( "active" ), array( true ), "`id` ='$id_content'" );
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content status in news changed, id: " . $id_content . ", new status: active" );
				}

				$content .= $this->l->alert_text_dismiss( "success", AL_NEWS_STATUS_CHANGE );

				return $content;
			}
		}
	}

	public function news_status() {
		$content = "";

		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT `active` FROM `" . TP . "news` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();
				if ( $row['active'] == 0 ) {
					$active = 1;
				} else {
					$active = 0;
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "news_content", array( "active" ), array( $active ), "`id` ='$id'" );
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_BLOCKNEWS_STATUS );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_BLOCKNEWS_STATUS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_BLOCKNEWS_STATUS_CHANGE );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "news status changed, id: " . $id );

		return $content;
	}
}

if ( ! isset( $action ) ) {
	$action = null;
}

$class_content_news = new class_content_news( $action );
$content .= $class_content_news->get_content();