<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_news extends class_root {

	public $p;
	public $loc;
	public $db;
	public $log;
	public $l;
	public $lang;
	public $content;
	public $get;
	public $datetime;

	public function __construct() {

		$this->l        = new class_layout();
		$this->p        = new class_page();
		$this->get      = new class_get();
		$this->datetime = new class_date_time();
		$this->loc      = class_location::getInstance();
		$this->db       = class_database::getInstance();
		$this->log      = class_logging::getInstance();
		$this->lang     = class_language::getInstance();

	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public function show_news( $news_name ) {
		$content = "";
		if ( ! empty( $news_name ) ) {
			$sql    = "SELECT * FROM `" . TP . "news` WHERE `name` LIKE '$news_name' AND `deleted`='0' AND `active`='1'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();

				$do_archive  = $row['do_archive'];
				$show_amount = $row['show_amount'];

				$where = "";
				if ( $do_archive ) {
					$where = "AND `archive_date` >= NOW()";
				}

				$limit = "";
				if ( $show_amount > 0 ) {
					$sql3          = "SELECT * FROM `" . TP . "news_content` WHERE `to_id` = '" . $row['id'] . "' AND `deleted`='0' AND `active`='1' $where";
					$result3       = $this->db->query( $sql3, __FILE__ . ":" . __LINE__ );
					$entries_count = $result3->num_rows;

					$news_page = $this->get->get( "news_page", 0 );
					$news_page = intval( $news_page );
					if ( $news_page < 0 ) {
						$news_page = 0;
					}
					$max_pages = ceil( $entries_count / $show_amount ) - 1;
					if ( $news_page > $max_pages ) {
						$news_page = $max_pages;
					}
					$start = $news_page * $show_amount;

					$limit = "LIMIT $start, $show_amount";

					if ( $news_page > 0 ) {
						$parameter_back = $this->l->add_to_current_parameter( "news_page=" . ( $news_page - 1 ) );
						$link_back      = $this->l->link( $this->txt( 'PAGE_ARROW_LEFT' ) . " " . $this->txt( 'PAGE_BACK' ), $this->loc->current, $parameter_back, "", "news-" . $row['id'], "link", ANKER_ALWAYS_ON );
					} else {
						$link_back = "";
					}
					if ( $news_page < $max_pages ) {
						$parameter_forward = $this->l->add_to_current_parameter( "news_page=" . ( $news_page + 1 ) );
						$link_forward      = $this->l->link( $this->txt( 'PAGE_FORWARD' ) . " " . $this->txt( 'PAGE_ARROW_RIGHT' ), $this->loc->current, $parameter_forward, "", "news-" . $row['id'], "link", ANKER_ALWAYS_ON );
					} else {
						$link_forward = "";
					}
					$page_info = $this->txt( 'PAGE' ) . " " . ( $news_page + 1 ) . " " . $this->txt( 'OF' ) . " " . ( $max_pages + 1 );
				}

				$sql     = "SELECT * FROM `" . TP . "news_content` WHERE `to_id` = '" . $row['id'] . "' AND `deleted`='0' AND `active`='1' $where ORDER BY `datetime` DESC $limit";
				$result2 = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				if ( $result2->num_rows > 0 ) {
					$content .= "<div id='news-" . $row['id'] . "'></div>\n";

					if ( ! empty( $row['header'] ) ) {
						if ( is_null( $row[ 'header' . $this->lang->lnbr ] ) ) {
							$header_lang_number = $this->lang->lnbrm;
						} else {
							$header_lang_number = $this->lang->lnbr;
						}
						$content .= "<h1 class='news-header'>" . $row[ 'header' . $header_lang_number ] . "</h1>\n";
					}

					while ( $row2 = $result2->fetch_assoc() ) {
						$content .= "<div class='news-content'>";
						if ( ! empty( $row2['header'] ) ) {
							if ( is_null( $row2[ 'header' . $this->lang->lnbr ] ) ) {
								$header_lang_number = $this->lang->lnbrm;
							} else {
								$header_lang_number = $this->lang->lnbr;
							}
							$content .= "<h3>" . $row2[ 'header' . $header_lang_number ] . "</h3>\n";
						}

						$date = $this->datetime->sqldatetime( $row2['datetime'] );
						$content .= "<p class='date'>" . $date . "</p>\n";

						if ( is_null( $row2[ 'text' . $this->lang->lnbr ] ) ) {
							$content_lang_number = $this->lang->lnbrm;
						} else {
							$content_lang_number = $this->lang->lnbr;
						}

						$content .= $row2[ 'text' . $content_lang_number ];
						$content .= "</div>\n";
					}
					if ( ! empty( $link_back ) || ! empty( $link_forward ) ) {
						$content .= "<div class='row news-navigation'><div class='text-center col-sm-4'>$link_back</div>
                            <div class='text-center col-sm-4'>$page_info</div><div class='text-center col-sm-4'>$link_forward</div></div>\n";
					}
				} else {
					$this->log->error( "news", __FILE__ . ":" . __LINE__, "Can't find Newsblock for name like '" . $news_name . "'" );
				}
			} else {
				$this->log->error( "news", __FILE__ . ":" . __LINE__, "Can't find Newsblock for name like '" . $news_name . "'" );
			}
		} else {
			$this->log->error( "news", __FILE__ . ":" . __LINE__, "Can't find Newsblock for name like '" . $news_name . "'" );
		}

		return $content;
	}


}