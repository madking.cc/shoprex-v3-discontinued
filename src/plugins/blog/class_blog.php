<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


class class_blog extends class_uri_file_path_operations {
	public $p;
	public $log;
	public $db;
	public $lang;
	public $get;
	public $l;
	public $datetime;
	public $loc;

	public $blog_anker;
	public $blog_file;

	public function __construct() {

		parent::__construct();
		$this->p        = new class_page();
		$this->get      = new class_get();
		$this->l        = new class_layout();
		$this->datetime = new class_date_time();
		$this->log      = class_logging::getInstance();
		$this->db       = class_database::getInstance();
		$this->lang     = class_language::getInstance();
		$this->loc      = class_location::getInstance();

		$this->blog_anker = "content-shop";
		$this->blog_file  = BLOG_FILE;

		// Todo: why?
		if ( ! isset( $GLOBALS['subtitle'] ) ) {
			$GLOBALS['subtitle'] = "";
		}

	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public function show_blog_menu() {
		$content = "";

		// Count of all
		if ( BLOG_SHOW_SIZEOF_SECTIONS ) {
			$sql    = "SELECT `id` FROM `" . TP . "blog_content` WHERE `active`='1' AND `deleted`='0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows > 0 ) {
				$size_all = " " . $this->txt( 'OPEN_BRACKET' ) . $result->num_rows . $this->txt( 'CLOSE_BRACKET' );
			} else {
				$size_all = " " . $this->txt( 'OPEN_BRACKET' ) . $this->txt( 'ZERO' ) . $this->txt( 'CLOSE_BRACKET' );
			}
		} else {
			$size_all = "";
		}

		// Get all sections
		$sql    = "SELECT *, (CASE WHEN `text" . $this->lang->lnbr . "` IS NOT NULL THEN `text" . $this->lang->lnbr . "` ELSE `text" . $this->lang->lnbrm . "` END) as `text_sort`
            FROM `" . TP . "blog_section` WHERE `deleted`=0 AND `active`=1 ORDER BY `pos` DESC, `text_sort`";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		$sections = array();
		$tmp      = array();

		// Get content counter to each section
		if ( $result->num_rows > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				if ( is_null( $row[ 'text' . $this->lang->lnbr ] ) ) {
					$content_lang_number = $this->lang->lnbrm;
				} else {
					$content_lang_number = $this->lang->lnbr;
				}

				$tmp['text']        = $row[ 'text' . $content_lang_number ];
				$tmp['id']          = $row['id'];
				$tmp['has_content'] = false;
				$tmp['count']       = "";
				$tmp['image']       = $row['image'];

				$sql     = "SELECT `id_content` FROM `" . TP . "blog_link` WHERE `id_section` = '" . $row['id'] . "'";
				$result2 = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				if ( $result2->num_rows > 0 ) {
					$i = 0;
					while ( $row2 = $result2->fetch_assoc() ) {
						$sql     = "SELECT `id` FROM `" . TP . "blog_content` WHERE `id` = '" . $row2['id_content'] . "' AND `active`='1' AND `deleted`='0'";
						$result3 = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						if ( $result3->num_rows > 0 ) {
							$i ++;
							$tmp['count']       = $i;
							$tmp['has_content'] = true;
						}
					}
				}
				$sections[] = $tmp;
			}
		}

		// Build menu
		if ( sizeof( $sections ) > 0 ) {
			$content .= "
        <ul class=\"blog-menu nav nav-stacked\">";

			$section_id = $this->get->get( "section_id", "0" );

			// Main menu point for all content
			$section_id == "0" ? $class = "active" : $class = "inactive";
			$content .= "<li class=''>" . $this->l->link( ALL . " " . $size_all, $this->blog_file, NO_PARAMETER, EMPTY_FREE_TEXT, $this->blog_anker, "link " . $class ) . "</li>\n";
			if ( strcasecmp( $class, "active" ) === 0 ) {
				$this->lang->add_to_lang_path( $this->blog_file, LANG_PATH_PRIORITY_BLOG );
			}

			// Single sections
			foreach ( $sections as $value ) {
				if ( $value['has_content'] ) {
					$size = "";
					if ( BLOG_SHOW_SIZEOF_SECTIONS ) {
						$size = " " . $this->txt( 'OPEN_BRACKET' ) . $value['count'] . $this->txt( 'CLOSE_BRACKET' );
					}
					$text = $this->translate_special_characters( $value['text'], IS_FILE );

					$class = "inactive";
					if ( $section_id == $value['id'] ) {
						$class = "active";

						// Do Meta

						$GLOBALS['subtitle'] .= " &#187; " . $value['text'];
						$GLOBALS['description'] = $value['text'];

					}

					if ( is_null( $value['image'] ) || empty( $value['image'] ) ) {
						$image = "";
					} else {
						$image = $this->l->img( $this->loc->web_root . "files/" . $value['image'], "Section Picture", "", "img nr image-blog-section" ) . " ";
					}


					$content .= "<li class=''>" . $this->l->link( $image . $value['text'] . $size, $this->blog_file, "text=$text&section_id=" . $value['id'], EMPTY_FREE_TEXT, $this->blog_anker, "link " . $class ) . "</li>\n";

					// If section or section with content is selected (for language path)
					if ( strcasecmp( $class, "active" ) === 0 ) {
						if ( $this->lang->count_of_languages > 1 ) {
							$content_id = $this->get->get( "content_id" );

							foreach ( $this->lang->languages as $lang_key => $lang_array ) {
								// Only section is selected
								if ( is_null( $content_id ) ) {
									$this->lang->add_to_lang_path( $this->blog_file . "?text=$text&section_id=" . $value['id'], LANG_PATH_PRIORITY_BLOG_SECTION, NO_CHECK_FOR_LANGUAGES, $lang_key, $lang_array );
								} // Section with content is selected
								else {
									$content_id     = trim( $content_id );
									$content_id     = intval( $content_id );
									$content_sql    = "SELECT * FROM `" . TP . "blog_content` WHERE `active`='1' AND `deleted`='0' AND `id`='$content_id'";
									$content_result = $this->db->query( $content_sql, __FILE__ . ":" . __LINE__ );
									if ( $content_result->num_rows == 1 ) {
										$content_row  = $content_result->fetch_assoc();
										$content_text = $this->translate_special_characters( $content_row[ 'header' . $lang_array['number'] ], true );
										$this->lang->add_to_lang_path( $this->blog_file . "?text=$content_text&content_id=$content_id&section_id=" . $value['id'], LANG_PATH_PRIORITY_BLOG_ARTICLE_WITH_SECTION, NO_CHECK_FOR_LANGUAGES, $lang_key, $lang_array );
									} else {
										$this->log->error( "php", __FILE__ . ":" . __LINE__, "Can't find content, content_id: '$content_id', num_rows: '" . $content_result->num_rows . ", sql: '$content_sql'" );;
									}
								}
							}
						}
					}
				}
			}
			$content .= "   </ul>\n";
		}

		return $content;
	}

	public function show_blog_content() {


		$content = "";

		$section_id = $this->get->get( "section_id", "0" );
		$content_id = $this->get->get( "content_id", "0" );

		if ( $content_id == 0 ) {
			// If no content is selected

			// Determine page
			$do_browse = false;
			$limit     = "";
			if ( BLOG_SIZEOF_ARTICLES != 0 ) {
				$page = $this->get->get( "page", 0 );
				if ( $page >= 0 && is_numeric( $page ) ) {
					$join  = "";
					$where = "";
					if ( $section_id > 0 ) {
						$join  = " LEFT JOIN `" . TP . "blog_link` ON (`" . TP . "blog_content`.`id` = `" . TP . "blog_link`.`id_content`)";
						$where = " AND `" . TP . "blog_link`.`id_section`='$section_id'";
					}

					$sql    = "SELECT `" . TP . "blog_content`.`id` AS `content_id` FROM `" . TP . "blog_content` $join WHERE `active`='1' AND `deleted`='0' $where";
					$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

					$articles_sum = $result->num_rows;

					if ( $articles_sum > BLOG_SIZEOF_ARTICLES ) {
						$do_browse = true;
						$max_page  = ceil( $articles_sum / BLOG_SIZEOF_ARTICLES );

						if ( $page == 0 ) {
							$limit = "LIMIT 0," . BLOG_SIZEOF_ARTICLES;
						} else {
							if ( $page > 0 && $page <= $max_page ) {
								$limit_start = $page * BLOG_SIZEOF_ARTICLES;
								if ( $limit_start > 0 ) {
									$limit = "LIMIT $limit_start," . BLOG_SIZEOF_ARTICLES;
								}
							}
						}
					} else {
						$do_browse = false;
						$max_page  = 1;
						$page      = 1;
					}
				}
			}

			// Determine preview text in section or all sections
			$join  = "";
			$where = "";
			if ( $section_id > 0 ) {
				$join  = " LEFT JOIN `" . TP . "blog_link` ON (`" . TP . "blog_content`.`id` = `" . TP . "blog_link`.`id_content`)";
				$where = " AND `" . TP . "blog_link`.`id_section`='$section_id'";
			}

			$sql    = "SELECT *, `" . TP . "blog_content`.`id` AS `content_id` FROM `" . TP . "blog_content` $join WHERE `active`='1' AND `deleted`='0' $where ORDER BY `datetime` DESC $limit";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 0 ) {
				$content .= "<p>" . $this->txt( 'UNDER_CONSTRUCTION' ) . "</p>\n";
			} else {
				// Display preview
				while ( $row = $result->fetch_assoc() ) {
					if ( is_null( $row[ 'text' . $this->lang->lnbr ] ) ) {
						$content_lang_number = $this->lang->lnbrm;
					} else {
						$content_lang_number = $this->lang->lnbr;
					}

					$text = $this->l->generate_preview_text( $row[ 'text' . $content_lang_number ] );

					if ( is_null( $row[ 'header' . $this->lang->lnbr ] ) ) {
						$header_lang_number = $this->lang->lnbrm;
					} else {
						$header_lang_number = $this->lang->lnbr;
					}

					$link_text = $this->translate_special_characters( $row[ 'header' . $header_lang_number ], true );

					$parameter = "";
					// If section is selcted
					if ( ! empty( $section_id ) ) {
						$parameter .= "section_id=" . $section_id . "&";
					}
					// If page is selected
					if ( $do_browse && $page > 0 && is_numeric( $page ) && $page < $max_page ) {
						$parameter .= "page=" . $page . "&";
					}
					// Fix parameter
					if ( ! empty( $parameter ) ) {
						$parameter = substr( $parameter, 0, - 1 );
						$parameter = "&" . $parameter;
					}

					// Display date
					$date = "";
					if ( BLOG_SHOW_DATE ) {
						$date = $this->datetime->sqldatetime( $row['datetime'] );
						$date = "<p>" . $date . "</p>";
					}

					// Display preview per $row
					$content .= "
<section>
    <header>
            <H2>" . $this->l->link( $row[ 'header' . $header_lang_number ], $this->blog_file, "text=$link_text&content_id=" . $row['content_id'] . $parameter, "", $this->blog_anker, "link link-hover2 link-underline-big" ) . "</H2>
            " . $date . "
    </header>
            
    \n";
					if ( ! is_null( $row['image'] ) && ! empty( $row['image'] ) ) {
						$content .= "	<div class=\"row\">
	    <div class=\"col-sm-4 4u\">
	    " . $this->l->link( $this->l->img( $this->loc->web_root . "files/" . $row['image'], "", "", "img img-center" ), $this->blog_file, "text=$link_text&content_id=" . $row['content_id'] . $parameter, "", $this->blog_anker, "link link-hover2" ) . "
        </div >
		<div class=\"col-sm-8 8u\" > \n";
					}
					$content .= "
            <p>" . $text . "</p>
            <footer><p>" . $this->l->link( SHOW . "...", $this->blog_file, "text=$link_text&content_id=" . $row['content_id'] . $parameter, "", $this->blog_anker, "link_button btn btn-primary" ) . "</p></footer>\n";

					if ( ! is_null( $row['image'] ) && ! empty( $row['image'] ) ) {
						$content .= "        </div>
	</div>\n";
					}
					$content .= "</section>
<hr /><div class='v-padding-bottom'></div>
            ";
					// End of while $row
				}

				// Clear parameter
				$parameter = "";
				if ( ! empty( $section_id ) ) {
					$parameter .= "section_id=" . $section_id . "&";
				}

				// Display page links and select field
				if ( $do_browse ) {
					$content .= "
            <div class='row'>
                <div class='4u col-sm-4 text-left'>";
					if ( $page > 0 && $page < $max_page ) {
						if ( ( $page - 1 ) != 0 ) {
							$back_link      = "page=" . ( $page - 1 ) . "&";
							$parameter_back = $parameter . $back_link;
						} else {
							$parameter_back = $parameter;
						}
						$parameter_back = substr( $parameter_back, 0, - 1 );
						$content .= $this->l->link( "&laquo; " . PAGE_BACK, $this->blog_file, $parameter_back, "", $this->blog_anker, "button link_button btn btn-primary" );
					}

					$content .= "
                </div>
                <div class='4u col-sm-4 text-center'>
                ";
					$content .= $this->l->form_simple( "", "", false, "GET" );
					if ( $section_id ) {
						$content .= $this->l->hidden( "section_id", $section_id );
					}
					$content .= $this->l->select( "page", "", "", true, "margin:0 auto;", "select form-control input-sm" );
					for ( $i = 0; $i < $max_page; $i ++ ) {
						$content .= "<option value='$i'";
						if ( $i == $page ) {
							$content .= " selected='selected'";
						}
						$content .= ">" . PAGE . " " . ( $i + 1 ) . " " . OF . " $max_page</option>\n";
					}
					$content .= "</select></form>";

					$content .= "
                </div>
                <div class='4u col-sm-4 text-right'>";
					if ( $page >= 0 && $page < ( $max_page - 1 ) ) {
						$next_link      = "page=" . ( $page + 1 );
						$parameter_next = $parameter . $next_link;
						$content .= $this->l->link( PAGE_FORWARD . " &raquo;", $this->blog_file, $parameter_next, "", $this->blog_anker, "button link_button btn btn-primary" );
					}
					$content .= "
                </div>
            </div>
            ";
				}
				// End of display page links and select field
			}
			// End of display previews
		} // Show content if selected
		elseif ( $content_id > 0 ) {
			$sql    = "SELECT * FROM `" . TP . "blog_content` WHERE `active`='1' AND `deleted`='0' AND `id`='$content_id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 0 ) {
				$content .= "<p>" . $this->txt( 'UNDER_CONSTRUCTION' ) . "</p>\n";
				$content .= "<p><img src='" . WEBROOT . "__media/under_construction.png' style='margin: 0 auto'></p>\n";
			} elseif ( $result->num_rows == 1 ) {
				$page = $this->get->get( "page", "-1" );

				$parameter = "";
				if ( ! empty( $section_id ) ) {
					$parameter .= "section_id=" . $section_id . "&";
				}
				if ( $page > 0 && is_numeric( $page ) ) {
					$parameter .= "page=" . $page . "&";
				}
				if ( ! empty( $parameter ) ) {
					$parameter = substr( $parameter, 0, - 1 );
				}

				$row = $result->fetch_assoc();

				if ( $this->lang->count_of_languages > 1 ) {
					foreach ( $this->lang->languages as $lang_key => $lang_array ) {
						$content_text = $this->translate_special_characters( $row[ 'header' . $lang_array['number'] ], true );
						$this->lang->add_to_lang_path( $this->blog_file . "?text=$content_text&content_id=$content_id", LANG_PATH_PRIORITY_BLOG_ARTICLE, NO_CHECK_FOR_LANGUAGES, $lang_key, $lang_array );
					}
				}

				if ( is_null( $row[ 'header' . $this->lang->lnbr ] ) ) {
					$header_lang_number = $this->lang->lnbrm;
				} else {
					$header_lang_number = $this->lang->lnbr;
				}

				$entry_subtitle = " &#187; " . $row[ 'header' . $header_lang_number ];
				$description    = $row[ 'header' . $header_lang_number ];

				$date = "";
				if ( BLOG_SHOW_DATE ) {
					$date = $this->datetime->sqldatetime( $row['datetime'] );
					$date = "<p><strong>" . $date . "</strong></p>";
				}

				if ( is_null( $row[ 'text' . $this->lang->lnbr ] ) ) {
					$content_lang_number = $this->lang->lnbrm;
				} else {
					$content_lang_number = $this->lang->lnbr;
				}

				//$content .= "<div class='disclaimer'>[t]blog_disclaimer[/t]</div>";

				$back_button = "<p>" . $this->l->link( "&laquo; " . BACK, $this->blog_file, $parameter, "", $this->blog_anker, "link_button btn btn-primary" ) . "</p>";

				// Display content
				$content .= "
            <H1>" . $row[ 'header' . $header_lang_number ] . "</H1>
            $date
            $back_button
            <hr>
            " . $row[ 'text' . $content_lang_number ] . "
            <hr>
            $back_button

            <div class='v-padding-bottom'></div>
            ";

				// Get javascript
				if ( ! is_null( $row[ 'js' . $this->lang->lnbr ] ) ) {
					if ( ! isset( $GLOBALS['body_footer'] ) ) {
						$GLOBALS['body_footer'] = "";
					}
					$GLOBALS['body_footer'] .= $row[ 'js' . $this->lang->lnbr ];
				} elseif ( ! is_null( $row[ 'js' . $this->lang->lnbrm ] ) ) {
					if ( ! isset( $GLOBALS['body_footer'] ) ) {
						$GLOBALS['body_footer'] = "";
					}
					$GLOBALS['body_footer'] .= $row[ 'js' . $this->lang->lnbrm ];
				}
				// Get css
				if ( ! is_null( $row[ 'css' . $this->lang->lnbr ] ) ) {
					$GLOBALS["custom_css"] .= $row[ 'css' . $this->lang->lnbr ];
				} elseif ( ! is_null( $row[ 'css' . $this->lang->lnbrm ] ) ) {
					$GLOBALS["custom_css"] .= $row[ 'css' . $this->lang->lnbrm ];
				}
			}
		}

		// Do Meta
		if ( isset( $entry_subtitle ) ) {
			$GLOBALS['subtitle'] .= $entry_subtitle;
			$GLOBALS['description'] = $description;
		}

		return $content;
	}

}