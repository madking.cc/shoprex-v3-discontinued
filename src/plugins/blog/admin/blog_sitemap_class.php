<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class blog_sitemap_class {
	protected $db;
	protected $lang;

	public function __construct() {
		$this->db   = class_database::getInstance();
		$this->lang = class_language::getInstance();
	}

	public function add_links( &$link_counter, &$main_link_tmp, &$alternate_links_tmp ) {

		if ( BLOG_SITEMAP_ENABLE ) {
			$sql    = "SELECT * FROM `" . TP . "blog_content` WHERE `deleted` = '0' AND `active` = '1'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows > 0 ) {
				while ( $row = $result->fetch_assoc() ) {
					foreach ( $this->lang->languages as $lang_array ) {
						$main_link_tmp[ $link_counter ]['path']     = array( $lang_array['subdir'] . BLOG_FILE . "?text=" . $row[ 'header' . $lang_array['number'] ] . "&content_id=" . $row['id'] => $row['changed'] );
						$main_link_tmp[ $link_counter ]['priority'] = "0.8";
						foreach ( $this->lang->languages as $lang_array_alternate ) {
							$alternate_links_tmp[ $link_counter ][ $lang_array_alternate['hreflang'] ] = $lang_array_alternate['subdir'] . BLOG_FILE . "?text=" . $row[ 'header' . $lang_array_alternate['number'] ] . "&content_id=" . $row['id'];
						}
						$link_counter ++;
					}
				}
			}
		}
	}
}