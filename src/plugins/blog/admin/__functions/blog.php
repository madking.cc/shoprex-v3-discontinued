<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2015}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_BLOG;

class class_content_blog extends class_sys {
	public $content;

	public function __construct( $action ) {
		parent::__construct();

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );

		switch ( $action ) {

			case "sections":
				$this->content .= $this->sections();
				break;
			case "sections_status":
				$this->content .= $this->sections_status();
				$this->content .= $this->sections();
				break;
			case "sections_delete":
				$this->content .= $this->sections_delete();
				$this->content .= $this->sections();
				break;
			case "sections_add":
				$this->content .= $this->sections_save();
				$this->content .= $this->sections();
				break;
			case "sections_edit":
				$this->content .= $this->sections_edit();
				break;
			case "sections_update":
				$this->content .= $this->sections_update();
				break;
			case "options":
				$this->content .= $this->options();
				break;
			case "options_update":
				$this->options_update();
				break;
			case "options_update_and_back":
				$this->options_update( NO_RELOAD );
				$this->content .= $this->start();
				break;
			case "content_add":
				$this->content .= $this->content_add();
				break;
			case "content_save":
				$this->content_error = $this->content_save();
				if ( ! empty( $this->content_error ) ) {
					$this->content .= $this->content_error;
					$this->content .= $this->content_add();
				}
				break;
			case "content_save_no_back":
				$this->content_error = $this->content_save( STAY_ON_PAGE );
				if ( ! empty( $this->content_error ) ) {
					$this->content .= $this->content_error;
					$this->content .= $this->content_add();
				}
				break;
			case "content_delete":
				$this->content .= $this->content_delete();
				$this->content .= $this->start();
				break;
			case "content_status":
				$this->content .= $this->content_status();
				$this->content .= $this->start();
				break;
			case "content_edit":
				$this->content .= $this->content_add( EDIT );
				break;
			case "content_update":
				$this->content_error = $this->content_update();
				if ( ! empty( $this->content_error ) ) {
					$this->content .= $this->content_error;
					$this->content .= $this->content_add( EDIT );
				}
				break;
			case "content_update_no_back":
				$this->content_error = $this->content_update( STAY_ON_PAGE );
				if ( ! empty( $this->content_error ) ) {
					$this->content .= $this->content_error;
				}
				$this->content .= $this->content_add( EDIT );
				break;
			case "show_articles_to_section":
				$GLOBALS['admin_subtitle'] = AL_BLOG_ARTICLES_TO_SECTION;
			case "search":
			case "init":
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";

		$reset_search = $this->p->get( "reset_search", 0 );
		$search       = $this->p->get_session( "search", "", "text_search", $reset_search );
		$intext       = $this->p->get_session( "intext", 0, "text_intext", $reset_search );

		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );
		$content .= $this->l->display_message_by_session( 'content_added', AL_BLOG_SAVED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_BLOG_POST_UPDATED );
		$content .= $this->l->display_message_by_session( 'section_update', AL_AREA_UPDATED );
		$content .= $this->sitemap->display_sitemap_message();

		$section_id = $this->p->get( "section_id", 0 );

		if ( $section_id > 0 ) {
			$sql    = "SELECT text FROM `" . TP . "blog_section` WHERE `deleted`=0 AND `id`='$section_id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows == 0 ) {
				$section_id = 0;
			} else {
				$row_section = $result->fetch_assoc();
			}
		}

		if ( empty( $search ) && $section_id == 0 ) {
			$content .= "<h3 class='underline'>" . AL_BLOG . "</h3>\n";
		} elseif ( $section_id == 0 ) {
			$content .= "<h3 class='underline'>" . AL_BLOG . " " . AL_SEARCH_RESULT . "</h3>\n";
		} else {
			$content .= "<h3 class='underline'>" . AL_BLOG . " " . AL_ARTICLE_TO_RANGE . " " . $row_section['text'] . "</h3>\n";
		}

		if ( $section_id == 0 ) {
			if ( empty( $search ) ) {
				$sql    = "SELECT * FROM `" . TP . "blog_content` WHERE `deleted`=0 ORDER BY `datetime` DESC";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			} else {
				$header_search = "";
				$text_search   = "";
				foreach ( $this->lang->languages as $lang_array ) {
					$header_search .= $this->db->search( "header" . $lang_array['number'], $search ) . " OR ";
					if ( $intext ) {
						$text_search .= $this->db->search( "text" . $lang_array['number'], $search ) . " OR ";
					}
				}
				if ( ! empty( $header_search ) && empty( $text_search ) ) {
					$header_search = substr( $header_search, 0, - 3 );
				}
				if ( ! empty( $text_search ) ) {
					$text_search = substr( $text_search, 0, - 3 );
				}

				$sql    = "SELECT * FROM `" . TP . "blog_content` WHERE (" . $header_search . $text_search . ") AND `deleted`=0 ORDER BY 'header'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			}

			$content .= $this->l->form_admin( EMPTY_FREE_TEXT, "blog_add.php" ) . $this->l->submit( AL_ADD_NEW_BLOG_POST ) . "</form>
            " . $this->l->form_admin( EMPTY_FREE_TEXT, "blog_sections.php" ) . $this->l->submit( AL_EDIT_RANGES ) . "</form>
            " . $this->l->form_admin( EMPTY_FREE_TEXT, "blog_options.php" ) . $this->l->submit( AL_SETTINGS ) . "</form>";

			if ( $result->num_rows > 0 || ! empty( $search ) ) {

				$content .= " " . $this->l->form_admin( "name='form_search'" ) . $this->l->text( "search", $search, "250", "id='search_text'" ) . $this->l->hidden( "do", "search" ) . " " . $this->l->submit( AL_SEARCH, "id='search'" ) . " " . $this->l->checkbox( "intext", "1", $intext, 0, "", DONT_SUBMIT_UNCHECK_VALUE ) . " " . AL_IN_TEXT . "
                <span id='message' class='error'>| " . AL_ERROR . ": " . AL_MISSING_SEARCH_TEXT . "</span>\n";
				$content .= "    </form>\n";

				if ( ! empty( $search ) ) {
					$content .= $this->l->form_admin() . $this->l->hidden( "reset_search", "1" ) . $this->l->submit( AL_RESET );
					$content .= "    </form>\n";
				}

				$GLOBALS['body_footer'] .= "
        <script>

            $(document).ready(function () {
                $(\"#message\").hide();
            });
            $(\"form[name=form_search]\").submit(function( event ) {
                var val = $(\"input[name=search]\").val();
                if(val == '')
                {
                    $(\"#message\").fadeIn();
                    event.preventDefault();
                }
                else
                {
                    $(\"#message\").fadeOut();
                }
            });
             </script>\n";
			}
		} else {
			$content .= $this->l->form_admin( EMPTY_FREE_TEXT, "blog_add.php", "section_id=" . $section_id ) .
			            $this->l->submit( AL_ADD_NEW_BLOG_POST ) . "</form> " . $this->l->form_admin( "name='form_action'" ) .
			            $this->l->back_button( AL_TO_AREA_SELECTION, "blog_sections.php" ) . " " .
			            $this->l->back_button( AL_SHOW_ALL_BLOG_ENTRIES, "blog.php" ) . "</form>\n";
			$sql    = "SELECT *, `" . TP . "blog_content`.`id` AS `blog_content_id` FROM `" . TP . "blog_content`
            LEFT JOIN `" . TP . "blog_link` ON (`" . TP . "blog_content`.`id` = `" . TP . "blog_link`.`id_content`)
            WHERE `id_section` = '" . $section_id . "' AND `" . TP . "blog_content`.`deleted` = 0";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		}

		if ( $result->num_rows != 0 ) {
			$handler = $this->tbl->tbl_init( AL_TBL_HEADER, AL_TBL_DATE, AL_TBL_CHANGED, array(
				AL_TBL_CHARS,
				"nosort"
			), AL_TBL_ACTIVE, array( AL_TBL_ACTION, "nosort" ) );

			$entry_counter = 0;
			while ( $row = $result->fetch_assoc() ) {
				if ( isset( $row['blog_content_id'] ) ) {
					$row['id'] = $row['blog_content_id'];
				}
				$date              = $this->dt->sqldatetime( $row['datetime'] );
				$changed           = $this->dt->sqldatetime( $row['changed'] );
				$timestamp         = strtotime( $date );
				$timestamp_changed = strtotime( $changed );

				$active = $this->lang->answer( $row['active'] );

				$parameter = "id=" . $row["id"];
				if ( ! empty( $section_id ) ) {
					$parameter .= "&section_id=" . $section_id;
				}

				if ( $row['active'] == "0" ) {
					$submit_status = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				} elseif ( $row['active'] == "1" ) {
					$submit_status = $this->l->submit( AL_DEACTIVATE );
				}

				$chars = $this->p->strlen_language_all( "blog_content", "text", $row['id'] );

				$this->tbl->tbl_load( $handler, $row['header'], array(
					$timestamp,
					"time",
					$date
				), array( $timestamp_changed, "time", $changed ), $chars, $active,

					$this->l->form_submit_by_lang( INSERT_BREAKS, EMPTY_FREETEXT, "blog_edit.php", $parameter ) . "<br />" .

					$this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "do", "content_status" ) . $submit_status . "</form> " .
					$this->l->form_admin( "id=\"form_admin_delete_content_$entry_counter\"" ) . $this->l->hidden( "do", "content_delete" ) . $this->l->hidden( "id", $row["id"] ) .
					$this->l->button( AL_DELETE, "id=\"button_delete_content_$entry_counter\"" ) . "</form>" );
				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_BLOG_ENTRY_PART01 . " \"" . $row['header'] . "\" " . AL_DELETE_BLOG_ENTRY_PART02, "#button_delete_content_$entry_counter", "form_admin_delete_content_$entry_counter", "confirm_delete_$entry_counter" );

				$entry_counter ++;
			}

			$content .= $this->tbl->tbl_out( $handler, 1 );

			if ( $section_id > 0 ) {
				$content .= $this->l->form_admin( "name='form_action'" ) . $this->l->back_button( AL_TO_AREA_SELECTION, "blog_sections.php" ) . " " . $this->l->back_button( AL_SHOW_ALL_BLOG_ENTRIES, "blog.php" ) . "</form><br /><br />\n";
			}
		} else {
			if ( ! empty( $search ) || $section_id > 0 ) {
				$content .= "<h3>" . AL_NO_MATCHES . "</h3>";
			} else {
				$content .= "<h3>" . AL_NO_ENTRIES . "</h3>";
			}
		}

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_BLOG_START, "info", AL_INFORMATION );

		return $content;
	}

	public function options() {
		$content = "";

		$GLOBALS['admin_subtitle'] = AL_BLOG_OPTIONS;

		$content .= "<h3 class='underline'>" . AL_BLOG_OPTIONS . "</h3>\n";

		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );

		$content .= $this->l->form_admin( EMPTY_FREE_TEXT, "blog_options.php" ) . $this->l->hidden( "do", "options_update" ) . $this->l->table() . "
    <tr><td></td><td>" . $this->l->submit( AL_SAVE ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='save_and_back(this.form);'" ) . " " . $this->l->back_button( AL_BACK, "blog.php" ) . "</td></tr>
    <tr><td>" . AL_USE_ENHANCED_EDITOR . ":</td><td>" . $this->l->select_yesno( "blog_enhanced_editor", BLOG_ENHANCED_EDITOR ) . "</td></tr>
    <tr><td>" . AL_SHOW_ARTICLE_NUMBER . ":</td><td>" . $this->l->select_yesno( "blog_show_sizeof_sections", BLOG_SHOW_SIZEOF_SECTIONS ) . "</td></tr>
    <tr><td>" . AL_SHOW_CATEGORIES . ":</td><td>" . $this->l->select_yesno( "blog_show_sections", BLOG_SHOW_SECTIONS ) . "</td></tr>
    <tr><td>" . AL_SHOW_DATE_IN_ARTICLE . ":</td><td>" . $this->l->select_yesno( "blog_show_date", BLOG_SHOW_DATE ) . "</td></tr>
    <tr><td>" . AL_ITEMS_PER_PAGE . ":</td><td>" . $this->l->text( "blog_sizeof_articles", BLOG_SIZEOF_ARTICLES, "100" ) . "</td></tr>
    <tr><td>" . AL_ENABLE_IN_SITEMAP . ":</td><td>" . $this->l->select_yesno( "blog_sitemap_enable", BLOG_SITEMAP_ENABLE ) . "</td></tr>
    <tr><td>" . AL_IMAGE_SIZE . ":</td><td>" . $this->l->text( "blog_image_size_x", BLOG_IMAGES_SIZE_X, "100" ) . " x " . $this->l->text( "blog_image_size_y", BLOG_IMAGES_SIZE_Y, "100" ) . " " . AL_PIXEL . "</td></tr>
    <tr><td>" . AL_SECTION_IMAGE_SIZE . ":</td><td>" . $this->l->text( "blog_section_image_size_x", BLOG_SECTION_IMAGES_SIZE_X, "100" ) . " x " . $this->l->text( "blog_section_image_size_y", BLOG_SECTION_IMAGES_SIZE_Y, "100" ) . " " . AL_PIXEL . "</td></tr>
    <tr><td>Blog Datei:</td><td>" . $this->l->text( "blog_file", BLOG_FILE, "100" ) . "</td></tr>
    </table></form>\n";

		$content .= $this->l->panel( AL_HELP_BLOG_OPTIONS . AL_HELP_ENHANCED_EDITOR_ADVANTAGES, "info", AL_INFORMATION );

		$GLOBALS['body_footer'] .= "<script>
    function save_and_back(formID)
    {
        formID.do.value = 'options_update_and_back';
        formID.action = '/" . WEBROOT . ADMINDIR . "blog.php';
        formID.submit();
    }
    </script>
    ";

		return $content;
	}

	public function options_update( $reload = true ) {

		$elements                               = array();
		$elements['blog_enhanced_editor']       = $this->p->get( "blog_enhanced_editor", "1" );
		$elements['blog_show_sizeof_sections']  = $this->p->get( "blog_show_sizeof_sections", "1" );
		$elements['blog_show_sections']         = $this->p->get( "blog_show_sections", "1" );
		$elements['blog_sizeof_articles']       = $this->p->get( "blog_sizeof_articles", "1" );
		$elements['blog_show_date']             = $this->p->get( "blog_show_date", "1" );
		$elements['blog_sizeof_articles']       = intval( $elements['blog_sizeof_articles'] );
		$elements['blog_sitemap_enable']        = $this->p->get( "blog_sitemap_enable", "1" );
		$elements['blog_file']                  = $this->p->get( "blog_file", "blog.php" );
		$elements['blog_images_size_x']         = $this->p->get( "blog_image_size_x", "0" );
		$elements['blog_images_size_y']         = $this->p->get( "blog_image_size_y", "0" );
		$elements['blog_section_images_size_x'] = $this->p->get( "blog_section_image_size_x", "0" );
		$elements['blog_section_images_size_y'] = $this->p->get( "blog_section_image_size_y", "0" );

		foreach ( $elements as $key => $value ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "settings", array( "value" ), array( $value ), "UPPER(`key`) LIKE UPPER('$key')" );

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Settings updated, key: " . $key . ", value: " . $value );
		}

		$_SESSION['options_updated'] = 1;

		if ( $reload ) {
			$this->l->reload_js();
		}
	}

	public function sections() {
		$content = "";

		$GLOBALS['admin_subtitle'] = AL_BLOG_AREAS;

		$content .= $this->l->display_message_by_session( 'section_update', AL_AREA_UPDATED );

		$content .= "<h3 class='underline'>" . AL_BLOG_AREAS . "</h3>\n";

		$name   = "";
		$pos    = "0";
		$active = "1";

		$content .= $this->l->form_admin( "name='form_action'" ) . $this->l->hidden( "do", "sections_add" ) . "
        " . AL_NAME . ":
        " . $this->l->text( "name", $name ) . "
        " . AL_POSITION . ":
        " . $this->l->text( "pos", $pos, "60" ) . "
        " . AL_ACTIVE . ":
        " . $this->l->select_yesno( "active", $active ) . "
        " . $this->l->button( AL_ADD, "onclick='check_submit(this.form)'" ) . "
        <span id='message'></span>
        " . $this->l->back_button( AL_SHOW_ALL_BLOG_ENTRIES, "blog.php" ) . "
        </form>\n";

		$GLOBALS['body_footer'] .= "<script>
    function check_submit(formID)
    {
        str = formID.name.value;
        if(str == '')
        {
            " . $this->l->box_alert( "<H3 class='error underline'>" . AL_ERROR . "</H3><p>" . AL_MUST_ENTER_NAME . "</p>" ) . "
        }
        else
        {
            formID.submit();
        }
    }
    </script>
    ";

		$sql    = "SELECT * FROM `" . TP . "blog_section` WHERE `deleted`=0 ORDER BY `pos` DESC, text ASC";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows != 0 ) {
			$handler = $this->tbl->tbl_init( AL_TBL_NAME, AL_TBL_ITEMS, AL_TBL_POS, AL_TBL_IMAGE, AL_TBL_ACTIVE, array(
				AL_TBL_ACTION,
				"nosort"
			), array( AL_TBL_ACTION, "nosort" ) );

			$entry_counter = 0;
			while ( $row = $result->fetch_assoc() ) {
				$sql           = "SELECT * FROM `" . TP . "blog_link`
            LEFT JOIN `" . TP . "blog_content` ON (`" . TP . "blog_content`.`id` = `" . TP . "blog_link`.`id_content`)
            WHERE `id_section` = '" . $row['id'] . "' AND `" . TP . "blog_content`.`deleted` = 0";
				$result2       = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				$article_count = $result2->num_rows;

				$active = $this->lang->answer( $row['active'] );

				$this->l->box_js( "#edit_" . $row['id'], "blog_sections_edit.php", "id=" . $row['id'] );

				$form_childs = "";
				if ( $article_count > 0 ) {
					$form_childs .= $this->l->form_admin( EMPTY_FREE_TEXT, "blog_sections_childs.php", "section_id=" . $row["id"] );
					$form_childs .= $this->l->submit( AL_SHOW_ITEM );
					$form_childs .= "</form> ";
				}

				if ( $row['active'] == "0" ) {
					$submit_active = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				} elseif ( $row['active'] == "1" ) {
					$submit_active = $this->l->submit( AL_DEACTIVATE );
				}

				if ( is_null( $row['image'] ) || empty( $row['image'] ) ) {
					$image = "---";
				} else {
					$image = $this->l->img( $this->loc->web_root . "files/" . $row['image'], "" );
				}

				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_RANGE_NAME_PART01 . " \"" . $row['text'] . "\" " . AL_DELETE_RANGE_NAME_PART02, "#button_delete_section_$entry_counter", "form_admin_delete_section_$entry_counter", "dialog-confirm-$entry_counter" );

				if ( $this->lang->count_of_languages <= 1 ) {
					$name = $row['text'];
				} else {
					$name = "";
					foreach ( $this->lang->languages as $lang_array ) {
						$name .= $lang_array['text'] . ": ";
						$name .= $row[ 'text' . $lang_array['number'] ];
						$name .= "<br /><br />";
					}
				}

				$this->tbl->tbl_load( $handler, $name, $article_count, $row['pos'], $image, $active, $this->l->form_admin() . $this->l->button( AL_EDIT, "id=\"edit_" . $row['id'] . "\"" ) . "</form> " . $form_childs, $this->l->form_admin() . $this->l->hidden( "do", "sections_status" ) . $this->l->hidden( "id", $row["id"] ) . $submit_active . "</form>" . $this->l->form_admin( "id=\"form_admin_delete_section_$entry_counter\"" ) . $this->l->hidden( "do", "sections_delete" ) . $this->l->hidden( "id", $row["id"] ) . $this->l->button( AL_DELETE, "id=\"button_delete_section_$entry_counter\"" ) . "</form>" );

				$entry_counter ++;
			}

			$content .= $this->tbl->tbl_out( $handler, 2 );

			$content .= $this->l->form_admin() . $this->l->back_button( AL_BACK, "blog.php" ) . "</form><br /><br />\n";
		}

		return $content;
	}

	public function sections_edit() {
		$content = "";

		$GLOBALS['admin_subtitle'] = AL_EDIT_BLOG_SECTIONS;

		$GLOBALS['no_menu'] = true;

		$id = $this->p->get( "id" );

		$content .= $this->l->form_admin() . $this->l->button( AL_CLOSE_WINDOW, "id='close_window'" ) . "</form><br /><br />\n";

		$GLOBALS['body_footer'] .= "<script>
    $(\"#close_window\").click(function() {
        parent.$.fn.colorbox.close();
    });
</script>\n";

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			var_dump( $id );
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_AREA );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		} else {
			$sql    = "SELECT * FROM `" . TP . "blog_section` WHERE `id` = '$id' AND `deleted`=0";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows == 0 ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_AREA );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );

				return $content;
			} else {
				$row            = $result->fetch_assoc();
				$row['created'] = $this->dt->sqldatetime( $row['created'] );
				$row['changed'] = $this->dt->sqldatetime( $row['changed'] );
			}
		}

		$content .= $this->l->form_admin( "name='form_action2'" ) . $this->l->hidden( "do", "sections_update" ) . $this->l->hidden( "id", $row['id'] ) . $this->l->table();

		if ( $this->lang->count_of_languages <= 1 ) {
			$content .= "<tr><th>" . AL_TBL_NAME . "</th><td>" . $this->l->text( "text", $row['text'] ) . "</td></tr>";
		} else {
			foreach ( $this->lang->languages as $lang_array ) {
				$content .= "<tr><th>" . AL_NAME_IN . " " . $lang_array['text'] . ":</th><td>" . $this->l->text( "text" . $lang_array['number'], $row[ 'text' . $lang_array['number'] ] ) . "</td></tr>";
			}
		}

		$no_image_path = "/" . $this->loc->web_root . "__media/no_image.png";
		if ( ! is_null( $row['image'] ) && ! empty( $row['image'] ) ) {
			$blog_image                     = "/" . $this->loc->web_root . "files/" . $row['image'];
			$source_blog_image_holder_value = $row['image'];
		} else {
			$blog_image                     = $no_image_path;
			$source_blog_image_holder_value = "";
		}

		$GLOBALS['foot_bottom'] .= "<script>
    function delete_blog_image()
    {
        document.getElementsByName('blog-image-holder')[0].src = '$no_image_path';
        document.getElementsByName('source_blog-image-holder')[0].value = '';
    }
    </script>\n";

		$content .= "
        <tr><th>" . AL_TBL_POS_LONG . "</th><td>" . $this->l->text( "pos", $row['pos'], "60" ) . "</td></tr>
        <tr><th>" . AL_TBL_ACTIVE . "</th><td>" . $this->l->select_yesno( "active", $row['active'] ) . "</td></tr>
        <tr><td>
    " . AL_TBL_PICTURE . "</td><td><div id='blog-image-holder-outside' style='width:" . BLOG_SECTION_IMAGES_SIZE_X . "px;height:" . BLOG_SECTION_IMAGES_SIZE_Y . "px;background-color:#dedede;'>
    <img src='$blog_image' alt='' name='blog-image-holder' id='blog-image-holder'></div>
    " . $this->l->hidden( "source_blog-image-holder", $source_blog_image_holder_value, "id='source_blog-image-holder'" ) . "
    " . $this->l->media_button( AL_UPLOAD_PICTURE, "blog-image-holder", "pic_element", NO_ID, NO_HINT, 0, DEFAULT_UPLOADDIR, ELEMENT_IS_IMAGE, AUTO_RESIZE, BLOG_SECTION_IMAGES_SIZE_X, BLOG_SECTION_IMAGES_SIZE_Y ) . "
    " . $this->l->button( AL_DELETE_PICTURE, "onClick='delete_blog_image()'" ) . "
    " . $this->l->media_button( AL_MEDIA_MANAGER, "blog-image-holder", "pic_element_media_manager", NO_ID, NO_HINT, 0, DEFAULT_UPLOADDIR, ELEMENT_IS_IMAGE, AUTO_RESIZE, BLOG_SECTION_IMAGES_SIZE_X, BLOG_SECTION_IMAGES_SIZE_Y ) . "
        </td></tr>
        <tr><th>" . AL_TBL_CREATED . "</th><td>" . $this->l->text( "created", $row['created'], "250", "readonly='readonly'" ) . "</td></tr>
        <tr><th>" . AL_TBL_CHANGED . "</th><td>" . $this->l->text( "changed", $row['changed'], "250", "readonly='readonly'" ) . "</td></tr>
        <tr><th></th><td>" . $this->l->button( AL_SAVE, "id='submitform'" ) . " <span id='message'></span></td></tr>
        </table></form>";

		$GLOBALS['body_footer'] .= "<script>
    $(\"#submitform\").click(function() {

            var val = $(\"input[name=text]\").val();
            if(val == '')
            {
                $(\"<span id='message' class='error'>| " . AL_ERROR . ": " . AL_ENTER_VALID_NAME . "</span>\").replaceAll(\"#message\");
            }
            else
            {
                $(\"<span id='message'></span>\").replaceAll(\"#message\");
                $(\"form[name=form_action2]\").submit();
            }
        });
</script>\n";

		return $content;
	}

	public function content_add( $edit = false ) {
		$content = "";

		if ( $edit ) {
			$GLOBALS['admin_subtitle'] = AL_EDIT_BLOG;
		} else {
			$GLOBALS['admin_subtitle'] = AL_ADD_BLOG;
		}

		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );
		$content .= $this->l->display_message_by_session( 'content_added', AL_BLOG_SAVED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_BLOG_POST_UPDATED );
		$content .= $this->sitemap->display_sitemap_message();

		if ( $edit ) {
			$content .= "<h3 class='underline'>" . AL_BLOG_EDIT . "</h3>\n";
		} else {
			$content .= "<h3 class='underline'>" . AL_BLOG_ADD . "</h3>\n";
		}

		$lang_number        = $this->p->get( "lang" );
		$import_lang_number = $this->p->get( "import_lang" );
		$change_editortype  = $this->p->get( "change_editortype", - 1 );

		$section_id = $this->p->get( "section_id", 0 );
		if ( $section_id > 0 ) {
			$back_page             = "blog_sections_childs.php";
			$section_parameter     = "section_id=" . $section_id;
			$section_parameter_add = "&" . $section_parameter;
		} else {
			$back_page             = "blog.php";
			$section_parameter     = "";
			$section_parameter_add = "";
		}

		$tmp_sections = array();

		if ( $edit && $change_editortype == "-1" ) {
			$id = $this->p->get( "id" );
			if ( empty( $id ) || ! is_numeric( $id ) ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_POST );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
				$content .= start();

				return $content;
			} else {
				$sql    = "SELECT * FROM `" . TP . "blog_content` WHERE `id` = '$id' AND `deleted`=0";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				if ( $result->num_rows == 0 ) {
					$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_POST );
					$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
					$content .= start();

					return $content;
				} else {
					$row             = $result->fetch_assoc();
					$row['datetime'] = $this->dt->sqldatetime( $row['datetime'] );
					$hash            = $this->p->unique_id( BIG );
				}
			}
		} else {
			$hash                           = $this->p->get( "hash", $this->p->unique_id( BIG ) );
			$row['to_id']                   = $this->p->get( "to_id", "0" );
			$row[ 'header' . $lang_number ] = $this->p->get( "header", "", NOT_ESCAPED );
			$row[ 'text' . $lang_number ]   = $this->p->get( "text", "", NOT_ESCAPED );
			$row[ 'js' . $lang_number ]     = $this->p->get( "js", "", NOT_ESCAPED );
			$row[ 'css' . $lang_number ]    = $this->p->get( "css", "", NOT_ESCAPED );
			$row['active']                  = $this->p->get( "active", "1" );
			$row['datetime']                = $this->p->get( "datetime", "" );
			$row['enhanced_editor']         = $this->p->get( "enhanced_editor", BLOG_ENHANCED_EDITOR );
			$id                             = $this->p->get( "id", "" );

			$sections_selected = $this->p->get( "sections", "", NOT_ESCAPED );

			if ( ! empty( $sections_selected ) ) {
				foreach ( $sections_selected as $key => $value ) {
					$sections_selected[ $key ] = htmlentities( $value, ENT_QUOTES );
				}
			}
		}

		if ( ! is_null( $import_lang_number ) ) {
			if ( strcasecmp( $import_lang_number, "empty" ) === 0 ) {
				$import_lang_number = "";
			}
			$row[ 'js' . $lang_number ]     = $row[ 'js' . $import_lang_number ];
			$row[ 'css' . $lang_number ]    = $row[ 'css' . $import_lang_number ];
			$row[ 'text' . $lang_number ]   = $row[ 'text' . $import_lang_number ];
			$row[ 'header' . $lang_number ] = $row[ 'header' . $import_lang_number ];
		}

		$new_section = $this->p->get( "new_section", "", NOT_ESCAPED );
		if ( ! empty( $new_section ) ) {
			foreach ( $new_section as $key => $value ) {
				$new_section[ $key ] = htmlentities( $value, ENT_QUOTES );
			}
		}

		$sections = array();
		$sql      = "SELECT * FROM `" . TP . "blog_section` WHERE `deleted`=0 AND `active`=1 ORDER BY `text`";
		$result2  = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result2->num_rows > 0 ) {
			while ( $row2 = $result2->fetch_assoc() ) {
				$tmp       = array();
				$tmp['id'] = $row2['id'];
				foreach ( $this->lang->languages as $lang_array ) {
					$tmp[ 'text' . $lang_array['number'] ] = $row2[ 'text' . $lang_array['number'] ];
				}

				if ( $edit && $change_editortype == "-1" ) {
					$sql     = "SELECT id FROM `" . TP . "blog_link` WHERE `id_content`='" . $row['id'] . "' AND `id_section`='" . $row2['id'] . "'";
					$result3 = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
					if ( $result3->num_rows > 0 ) {
						$tmp['match'] = true;
					} else {
						$tmp['match'] = false;
					}
				} else {
					$tmp['match'] = false;
					if ( ! empty( $sections_selected ) && in_array( $row2['id'], $sections_selected ) ) {
						$tmp['match'] = true;
					}
				}
				$sections[] = $tmp;
			}
		}

		$content .= $this->l->table() . "
    <tr>";

		if ( $edit ) {
			$content .= "<td>" . AL_LANGUAGE_FEATURES . ":</td><td>";
			$content .= $this->l->language_import_fields( "blog_edit.php", $id, $lang_number, $section_parameter ) . $this->l->language_edit_fields( "blog_edit.php", $id, $lang_number, $section_parameter ) . "</td>
        </tr>
        <tr>
        <td></td><td>
            " . $this->l->form_admin( "name='form_action'", "blog_edit.php", "id=$id" );
			$content .= $this->l->hidden( "do", "content_update" );
			$content .= $this->l->hidden( "do_no_back", "content_update_no_back" );
		} else {
			$content .= "<td></td><td>\n";
			$content .= $this->l->form_admin( "name='form_action'", "blog_add.php" );
			$content .= $this->l->hidden( "do", "content_save" );
			$content .= $this->l->hidden( "do_no_back", "content_save_no_back" );
		}

		$content .= $this->l->hidden( "id", $id ) . $this->l->hidden( "hash", $hash ) . $this->l->hidden( "section_id", $section_id ) .
		            $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " .
		            $this->l->button( AL_SAVE_AND_BACK, "onclick='check_submit(this.form, false)'" ) . " ";
		if ( $edit ) {
			$content .= $this->l->reload_button( "blog_edit.php", "id=$id&lang=$lang_number" . $section_parameter_add, AL_RELOAD ) . " ";
		} else {
			$content .= $this->l->reload_button( "blog_add.php", $section_parameter, AL_RELOAD ) . " ";
		}

		$parameter_back = $section_parameter;

		$content .= $this->l->back_button( AL_BACK, $back_page, $parameter_back ) . "</td></tr>\n";

		if ( $edit ) {
			$content .= "<tr><td>" . AL_TBL_ID . "</td><td>" . $id . "</td></tr>\n";
		}

		if ( $this->lang->count_of_languages > 1 ) {
			$content .= "<tr><td>" . AL_TBL_CURRENT_LANGUAGE . "</td><td><strong>" . $this->p->get_language_text( $lang_number ) . $this->l->hidden( "lang", $lang_number ) . "</strong></td></tr>\n";
		} else {
			$content .= $this->l->hidden( "lang", $this->p->lang->lnbrm );
		}

		$content .= "
    <tr><td>" . AL_TBL_HEADER . $this->p->get_lhint() . "</td><td>" . $this->l->text( "header", $row[ 'header' . $lang_number ], "600" ) . "</td></tr>
    <tr><td>" . AL_TBL_TEXT . $this->p->get_lhint() . "</td><td>";

		$rows = "";
		if ( BLOG_ENHANCED_EDITOR ) {
			if ( $change_editortype == "-1" ) {
				if ( $row['enhanced_editor'] ) {
					$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
					$enhanced_editor = EDITOR_ENHANCED;
				} else {
					$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
					$rows            = "20";
					$enhanced_editor = EDITOR_SIMPLE;
				}
			} elseif ( $change_editortype ) {
				$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
				$enhanced_editor = EDITOR_ENHANCED;
			} else {
				$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
				$rows            = "20";
				$enhanced_editor = EDITOR_SIMPLE;
			}

			$content .= $this->l->hidden( "enhanced_editor", $enhanced_editor ) . $this->l->hidden( "change_editortype", "-1" );

			if ( $edit ) {
				$action_js = "content_edit";
			} else {
				$action_js = "content_add";
			}

			$GLOBALS['body_footer'] .= "
<script>
    $(\"#change_editor_enhanced\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('1');
        $(\"form[name=form_action]\").submit();
    });
    $(\"#change_editor_simple\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('0');
        $(\"form[name=form_action]\").submit();
    });

</script>
";
		} else {
			$enhanced_editor = EDITOR_SIMPLE;
			$rows            = "20";
			$content .= $this->l->hidden( "enhanced_editor", $row['enhanced_editor'] );
		}

		$ta_freetext = "";
		$ta_hint     = "";
		if ( $this->l->get_preview_status() ) {
			$ta_freetext = "readonly=\"readonly\"";
			$ta_hint     = "\n<br /><p class='info'>" . AL_JS_DISABLED_PREVIEW . ".</p>\n";
		}

		$content .= $this->l->media_ta( "text", $row[ 'text' . $lang_number ], UPLOADDIR, $enhanced_editor, $rows ) . "</td></tr>
    <tr><td>" . AL_TBL_JS . $this->p->get_lhint() . "</td><td>" . $this->l->media_ta( "js", $row[ 'js' . $lang_number ], UPLOADDIR, EDITOR_CODE, "100%", $ta_freetext, "ta ta_media", NO_ADDITONAL_BUTTONS, "js" ) . $ta_hint . "</td></tr>
    <tr><td>" . AL_TBL_CSS . $this->p->get_lhint() . "</td><td>" . $this->l->media_ta( "css", $row[ 'css' . $lang_number ], UPLOADDIR, EDITOR_CODE, "100%", EMPTY_FREETEXT, "ta ta_media", NO_ADDITONAL_BUTTONS, "css" ) . "</td></tr>\n";

		$no_image_path = "/" . $this->loc->web_root . "__media/no_image.png";
		if ( ! is_null( $row['image'] ) && ! empty( $row['image'] ) ) {
			$blog_image                     = "/" . $this->loc->web_root . "files/" . $row['image'];
			$source_blog_image_holder_value = $row['image'];
		} else {
			$blog_image                     = $no_image_path;
			$source_blog_image_holder_value = "";
		}

		$content .= "    <tr><td>
    " . AL_TBL_PICTURE . "</td><td><div id='blog-image-holder-outside' style='width:" . BLOG_IMAGES_SIZE_X . "px;height:" . BLOG_IMAGES_SIZE_Y . "px;background-color:#dedede;'>
    <img src='$blog_image' alt='' name='blog-image-holder' id='blog-image-holder'></div>
    " . $this->l->hidden( "source_blog-image-holder", $source_blog_image_holder_value, "id='source_blog-image-holder'" ) . "
    " . $this->l->media_button( AL_UPLOAD_PICTURE, "blog-image-holder", "pic_element", NO_ID, NO_HINT, 0, DEFAULT_UPLOADDIR, ELEMENT_IS_IMAGE, AUTO_RESIZE, BLOG_IMAGES_SIZE_X, BLOG_IMAGES_SIZE_Y ) . "
    " . $this->l->button( AL_DELETE_PICTURE, "onClick='delete_blog_image()'" ) . "
    " . $this->l->media_button( AL_MEDIA_MANAGER, "blog-image-holder", "pic_element_media_manager", NO_ID, NO_HINT, 0, DEFAULT_UPLOADDIR, ELEMENT_IS_IMAGE, AUTO_RESIZE, BLOG_IMAGES_SIZE_X, BLOG_IMAGES_SIZE_Y ) . "
    </td></tr>\n";

		$GLOBALS['foot_bottom'] .= "<script>
    function delete_blog_image()
    {
        document.getElementsByName('blog-image-holder')[0].src = '$no_image_path';
        document.getElementsByName('source_blog-image-holder')[0].value = '';
    }
    </script>\n";

		//media_button($value, $target, $type = "pic", $form_submit_id = "", $hint = "", $element_number = "0", $uploaddir = "", $element_is_image = "0", $auto_resize = "0", $auto_max_x = 0, $auto_max_y = 0, $free_text = "", $class = "btn btn-default")

		$content .= "    <tr><td>" . AL_TBL_ACTIVE . "</td><td>" . $this->l->select_yesno( "active", $row['active'] ) . "</td></tr>
    <tr><td>" . AL_TBL_DATE . "</td><td>" . $this->l->datetime( "datetime", $row['datetime'] ) . "</td></tr>
    <tr><td>" . AL_TBL_CATEGORY . "</td><td><ul class='list-group' id='sections'>";

		foreach ( $sections as $value ) {
			if ( is_array( $tmp_sections ) && sizeof( $tmp_sections ) > 0 ) {
				if ( in_array( $value['id'], $tmp_sections ) ) {
					$value['match'] = true;
				} else {
					$value['match'] = false;
				}
			}
			$content .= "<li class='list-group-item'>" . $this->l->checkbox( "sections[]", $value['id'], $value['match'], 0, EMPTY_FREE_TEXT, DONT_SUBMIT_UNCHECK_VALUE, "" ) . " ";

			if ( $this->lang->count_of_languages <= 1 ) {
				$content .= $value['text'];
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$tmp_value = $value[ 'text' . $lang_array['number'] ];
					if ( is_null( $tmp_value ) || empty( $tmp_value ) ) {
						$tmp_value = DISPLAY_EMPTY_VALUE;
					}
					$content .= $lang_array['text'] . ": " . $tmp_value . " / ";
				}
				$content = substr( $content, 0, - 2 );
			}
			$content .= "</li>\n";
		}

		$GLOBALS['body_footer'] .= "<script>
    var id_arr = [];
</script>\n";

		if ( ! empty( $new_section ) ) {
			$i = 0;
			$GLOBALS['body_footer'] .= "<script>\n";
			foreach ( $new_section AS $value ) {
				if ( empty( $value ) ) {
					continue;
				}
				$content .= "<li class='list-group-item'>" . $this->l->text( "new_section[]", $value, "250", "onkeyup='add_new_section($i);'" ) . "</li>\n";
				$GLOBALS['body_footer'] .= "   id_arr.push($i);\n";
				$i ++;
			}
			$content .= "<li class='list-group-item'>" . $this->l->text( "new_section[]", "", "250", "onkeyup='add_new_section($i);'" ) . "</li>\n";
			$GLOBALS['body_footer'] .= "</script>\n";
		} else {
			$content .= "<li class='list-group-item'>" . $this->l->text( "new_section[]", "", "250", "onkeyup='add_new_section(0);'" ) . "</li>\n";
		}

		$content .= "</ul>
    <tr><td></td><td>" . $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " . $this->l->back_button( AL_BACK, $back_page, $parameter_back ) . "</td></tr>

    </table></form>
    ";

		$GLOBALS['body_footer'] .= "<script>

    function add_new_section(id)
    {
       if(jQuery.inArray(id, id_arr) == -1)
       {
            id_arr.push(id);
            $('<li id=\"new_section_' + id + '\" class=\"list-group-item\"><input type=\"text\" name=\"new_section[]\" class=\"input_text form-control\" onkeyup=\"add_new_section(' + (id+1) + ');\" style=\"width:250px;\" /></li>').appendTo(\"#sections\");
            $( '#new_section_' + id  ).hide();
            $( '#new_section_' + id  ).show( 'fade', 1000 );
       }
    }

    function check_submit(formID, no_back)
    {
        str = formID.header.value;
        if(str == '')
        {
            " . $this->l->box_alert( "<H3 class='error underline'>" . AL_ERROR . "</H3><p>" . AL_MUST_ENTER_TITLE . "</p>" ) . "
        }
        else
        {
            if(no_back)
            {
                formID.do.value = formID.do_no_back.value;
                formID.submit();
            }
            else
            {
                formID.submit();
            }
        }
    }
    </script>
    ";

		if ( $this->lang->count_of_languages > 1 ) {
			$language_hint = AL_HELP_LANGUAGE_HINT;
		} else {
			$language_hint = "";
		}

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_ENHANCED_EDITOR_ADVANTAGES . AL_HELP_JAVASCRIPT_TAGS_NEEDED . AL_HELP_CSS_TAGS_NEEDED . AL_HELP_TEXT_MODS . $language_hint, "info", AL_INFORMATION );

		return $content;
	}

	public function content_save( $stay_on_page = false ) {
		$content = "";

		$lang_number = $this->p->get( "lang" );

		$header          = $this->p->get( "header", "" );
		$enhanced_editor = $this->p->get( "enhanced_editor", EDITOR_ENHANCED );
		$text            = $this->p->get( "text", "" );
		$js              = $this->p->get( "js", "" );
		$css             = $this->p->get( "css", "" );
		$active          = $this->p->get( "active", 1 );
		$datetime        = $this->p->get( "datetime", "" );
		$new_section     = $this->p->get( "new_section", "", NOT_ESCAPED );
		$sections        = $this->p->get( "sections", "", NOT_ESCAPED );

		$image = $this->p->get( "source_blog-image-holder", "" );

		$section_id = $this->p->get( "section_id", 0 );
		if ( $section_id > 0 ) {
			$back_page             = "blog_sections_childs.php";
			$section_parameter     = "section_id=" . $section_id;
			$section_parameter_add = "&" . $section_parameter;
		} else {
			$back_page             = "blog.php";
			$section_parameter     = "";
			$section_parameter_add = "";
		}

		if ( ! empty( $sections ) ) {
			foreach ( $sections as $key => $value ) {
				$sections[ $key ] = htmlentities( $value, ENT_QUOTES );
			}
		}

		if ( ! empty( $new_section ) ) {
			foreach ( $new_section as $key => $value ) {
				$new_section[ $key ] = htmlentities( $value, ENT_QUOTES );
			}
		}

		if ( empty( $header ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_TITLE, HISTORY_BACK, $section_parameter, "blog_add.php" );

			return $content;
		}

		if ( empty( $datetime ) ) {
			$datetime = date( "Y-m-d H:i:s" );
		} else {
			$datetime = $this->dt->sqldatetime( $datetime, "out" );
		}

		if ( $this->l->get_preview_status() ) {
			$this->db->ins( __FILE__ . ":" . __LINE__, "blog_content", array(
				"header" . $lang_number,
				"text" . $lang_number,
				"active",
				"created",
				"datetime",
				"enhanced_editor",
				"css" . $lang_number
			), array( $header, $text, $active, "NOW()", $datetime, $enhanced_editor, $css ) );
		} else {
			$this->db->ins( __FILE__ . ":" . __LINE__, "blog_content", array(
				"header" . $lang_number,
				"text" . $lang_number,
				"active",
				"created",
				"datetime",
				"enhanced_editor",
				"css" . $lang_number,
				"js" . $lang_number,
				"image"
			), array( $header, $text, $active, "NOW()", $datetime, $enhanced_editor, $css, $js, $image ) );
		}

		$id_new_blog = $this->db->get_insert_id();
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "New blog content added, header: $header, id: " . $id_new_blog );

		if ( ! empty( $new_section ) && sizeof( $new_section ) > 0 ) {
			foreach ( $new_section as $value ) {
				if ( $value === "" ) {
					continue;
				}

				$this->db->ins( __FILE__ . ":" . __LINE__, "blog_section", array( "text", "created" ), array(
					$value,
					"NOW()"
				) );

				$id_new_section = $this->db->get_insert_id();
				$this->log->event( "log", __FILE__ . ":" . __LINE__, "New blog section added and linked to blog content, name: $value, id: " . $id_new_section );
				$this->db->ins( __FILE__ . ":" . __LINE__, "blog_link", array(
					"id_content",
					"id_section"
				), array( $id_new_blog, $id_new_section ) );
			}
		}

		if ( ! empty( $sections ) ) {
			foreach ( $sections as $value ) {
				if ( $value === "" ) {
					continue;
				}

				$this->db->ins( __FILE__ . ":" . __LINE__, "blog_link", array(
					"id_content",
					"id_section"
				), array( $id_new_blog, $value ) );
			}
		}

		$_SESSION['content_added'] = 1;
		$this->sitemap->update_sitemap_automatic();

		if ( $stay_on_page ) {
			$this->l->reload_js( "blog_edit.php", "id=$id_new_blog" . $section_parameter_add );
		} else {
			$this->l->reload_js( $back_page, $section_parameter );
		}
	}

	public function sections_save() {
		$content = "";

		$name   = $this->p->get( "name", "" );
		$pos    = $this->p->get( "pos", 0 );
		$active = $this->p->get( "active", 1 );

		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME );

			return $content;
		}

		if ( ! is_numeric( $pos ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_NUMERIC_POS );

			return $content;
		}

		$this->db->ins( __FILE__ . ":" . __LINE__, "blog_section", array(
			"text",
			"pos",
			"active",
			"created"
		), array( $name, $pos, $active, "NOW()" ) );

		$id_new_section = $this->db->get_insert_id();
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "New blog section added, name: $name, id: " . $id_new_section );

		$content .= $this->l->alert_text_dismiss( "success", AL_BLOG_SAVED );

		return $content;
	}

	public function content_update( $stay_on_page = false ) {
		$content = "";

		$lang_number = $this->p->get( "lang" );

		$id              = $this->p->get( "id" );
		$enhanced_editor = $this->p->get( "enhanced_editor", EDITOR_ENHANCED );
		$header          = $this->p->get( "header", "" );
		$text            = $this->p->get( "text", "" );
		$js              = $this->p->get( "js", "" );
		$css             = $this->p->get( "css", "" );
		$active          = $this->p->get( "active", 1 );
		$datetime        = $this->p->get( "datetime", "" );
		$new_section     = $this->p->get( "new_section", "", NOT_ESCAPED );
		$sections        = $this->p->get( "sections", "", NOT_ESCAPED );

		$image = $this->p->get( "source_blog-image-holder", "" );

		$section_id = $this->p->get( "section_id", 0 );
		if ( $section_id > 0 ) {
			$back_page             = "blog_sections_childs.php";
			$section_parameter     = "section_id=" . $section_id;
			$section_parameter_add = "&" . $section_parameter;
		} else {
			$back_page             = "blog.php";
			$section_parameter     = "";
			$section_parameter_add = "";
		}

		if ( ! empty( $sections ) ) {
			foreach ( $sections as $key => $value ) {
				$sections[ $key ] = htmlentities( $value, ENT_QUOTES );
			}
		}

		if ( ! empty( $new_section ) ) {
			foreach ( $new_section as $key => $value ) {
				$new_section[ $key ] = htmlentities( $value, ENT_QUOTES );
			}
		}

		if ( empty( $header ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_TITLE, HISTORY_BACK, $section_parameter, "blog_edit.php" );

			return $content;
		}

		if ( empty( $datetime ) ) {
			$datetime = date( "Y-m-d H:i:s" );
		} else {
			$datetime = $this->dt->sqldatetime( $datetime, "out" );
		}

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_POST );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		} else {
			if ( $this->l->get_preview_status() ) {
				$this->db->upd( __FILE__ . ":" . __LINE__, "blog_content", array(
					"header" . $lang_number,
					"text" . $lang_number,
					"active",
					"datetime",
					"enhanced_editor",
					"css" . $lang_number
				), array( $header, $text, $active, $datetime, $enhanced_editor, $css ), "`id` = '$id'" );
			} else {
				$this->db->upd( __FILE__ . ":" . __LINE__, "blog_content", array(
					"header" . $lang_number,
					"text" . $lang_number,
					"active",
					"datetime",
					"enhanced_editor",
					"css" . $lang_number,
					"js" . $lang_number,
					"image"
				), array( $header, $text, $active, $datetime, $enhanced_editor, $css, $js, $image ), "`id` = '$id'" );
			}

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Blog content changed, header: $header, id: " . $id );

			$sql = "DELETE FROM `" . TP . "blog_link` WHERE `id_content` = '$id'";
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( ! empty( $new_section ) && sizeof( $new_section ) > 0 ) {
				foreach ( $new_section as $value ) {
					if ( $value === "" ) {
						continue;
					}

					$this->db->ins( __FILE__ . ":" . __LINE__, "blog_section", array(
						"text",
						"created"
					), array( $value, "NOW()" ) );

					$id_new_section = $this->db->get_insert_id();
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "New blog section added and linked to blog content, name: $value, id: " . $id_new_section );
					$this->db->ins( __FILE__ . ":" . __LINE__, "blog_link", array(
						"id_content",
						"id_section"
					), array( $id, $id_new_section ) );
				}
			}

			if ( ! empty( $sections ) ) {
				foreach ( $sections as $value ) {
					if ( $value === "" ) {
						continue;
					}

					$this->db->ins( __FILE__ . ":" . __LINE__, "blog_link", array(
						"id_content",
						"id_section"
					), array( $id, $value ) );
				}
			}

			$_SESSION['content_updated'] = 1;
			$this->sitemap->update_sitemap_automatic();

			if ( ! $stay_on_page ) {
				$this->l->reload_js( $back_page, $section_parameter );
			}

			return $content;
		}
	}

	public function sections_update() {
		$content = "";

		$GLOBALS['no_menu'] = true;

		if ( $this->lang->count_of_languages <= 1 ) {
			$name = $this->p->get( "text", "", false );
		} else {
			foreach ( $this->lang->languages as $lang_array ) {
				${"name" . $lang_array['number']} = $this->p->get( "text" . $lang_array['number'], null );
			}
		}

		$pos    = $this->p->get( "pos", 0 );
		$active = $this->p->get( "active", 1 );
		$id     = $this->p->get( "id", "0" );
		$image  = $this->p->get( "source_blog-image-holder", "" );

		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME, HISTORY_BACK, EMPTY_PARAMETER, "blog_sections_edit.php" );

			return $content;
		}

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_AREA );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		} else {
			if ( $this->lang->count_of_languages <= 1 ) {
				$this->db->upd( __FILE__ . ":" . __LINE__, "blog_section", array(
					"text" . $this->p->lang->lnbrm,
					"pos",
					"active",
					"image"
				), array( $name, $pos, $active, $image ), "`id` = '$id'" );
			} else {
				$array_upd     = array();
				$array_upd[]   = "pos";
				$array_upd[]   = "active";
				$array_upd[]   = "image";
				$array_value   = array();
				$array_value[] = $pos;
				$array_value[] = $active;
				$array_value[] = $image;

				foreach ( $this->lang->languages as $lang_array ) {
					$array_upd[]   = "text" . $lang_array['number'];
					$array_value[] = ${"name" . $lang_array['number']};
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "blog_section", $array_upd, $array_value, "`id` = '$id'" );
			}

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Blog section changed, name: $name id: " . $id );

			$GLOBALS['body_footer'] .= "<script>
        parent.$.fn.colorbox.close();
</script>\n";
			$_SESSION['section_update'] = 1;
			$this->l->reload_js( "blog_sections.php", "", true );
		}
	}

	public function content_delete() {
		$content = "";

		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "blog_content", array( "deleted" ), array( true ), "`id` = '$id'" );
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_POST );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_DELETED_BLOG_POST );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "blog_content deleted, id: " . $id );

		$this->sitemap->update_sitemap_automatic();

		return $content;
	}

	public function sections_delete() {
		$content = "";

		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "blog_section", array( "deleted" ), array( true ), "`id` = '$id'" );
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_AREA );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_DELETED_BLOG_AREA );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "blog_section deleted, id: " . $id );

		return $content;
	}

	public function content_status() {
		$content = "";

		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT `active` FROM `" . TP . "blog_content` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();
				if ( $row['active'] == 0 ) {
					$active = 1;
				} else {
					$active = 0;
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "blog_content", array( "active" ), array( $active ), "`id` = '$id'" );
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_POST_STATUS );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_POST_STATUS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$this->sitemap->update_sitemap_automatic();

		$content .= $this->l->alert_text_dismiss( "success", AL_CHANGED_BLOG_POST_STATUS );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "blog_content status changed, id: " . $id );

		return $content;
	}

	public function sections_status() {
		$content = "";

		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT `active` FROM `" . TP . "blog_section` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();
				if ( $row['active'] == 0 ) {
					$active = 1;
				} else {
					$active = 0;
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "blog_section", array( "active" ), array( $active ), "`id` = '$id'" );
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_AREA_STATUS );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_AREA_STATUS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_CHANGED_AREA_STATUS );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "blog_section status changed, id: " . $id );

		return $content;
	}
}

if ( ! isset( $action ) ) {
	$action = null;
}

$class_content_blog = new class_content_blog( $action );
$content .= $class_content_blog->get_content();