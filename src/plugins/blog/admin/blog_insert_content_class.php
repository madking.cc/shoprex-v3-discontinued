<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class blog_insert_content_class {
	protected $db;
	protected $lang;
	protected $l;
	public $content;

	public function __construct() {
		$this->db      = class_database::getInstance();
		$this->lang    = class_language::getInstance();
		$this->l       = new class_layout();
		$this->dt      = new class_date_time();
		$this->content = "";
	}

	public function generate_content( $spaces, $editor, $textarea_type ) {

// Blog get
		$blog_select_options        = "";
		$blog_button_select_options = "";
		$sql                        = "SELECT `header`,`changed`,`id`,`active` FROM `" . TP . "blog_content` WHERE `deleted` = '0' ORDER BY `header`,`changed`";
		$result                     = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				if ( $row['active'] ) {
					$active = " " . AL_ACTIVE;
				} else {
					$active = " " . AL_INACTIVE;
				}
				$blog_button_select_options .= "<option value='[blog_link=" . $row['id'] . "]identifier=" . $row[ 'header' . $this->lang->lnbrm ] . "&text=&class=btn btn-primary&role=button[/blog_link]'>" . $row[ 'header' . $this->lang->lnbrm ] . " " . $spaces . "(" . AL_TBL_ID . " " . $row['id'] . AL_COMMA . " " . AL_TBL_CHANGED . " " . $this->dt->sqldatetime( $row['changed'] ) . AL_COMMA . " " . AL_TBL_STATUS . " " . $active . ")</option>\n";
				$blog_select_options .= "<option value='[blog_link=" . $row['id'] . "]identifier=" . $row[ 'header' . $this->lang->lnbrm ] . "&text=&class=link&role=link[/blog_link]'>" . $row[ 'header' . $this->lang->lnbrm ] . " " . $spaces . "(" . AL_TBL_ID . " " . $row['id'] . AL_COMMA . " " . AL_TBL_CHANGED . " " . $this->dt->sqldatetime( $row['changed'] ) . AL_COMMA . " " . AL_TBL_STATUS . " " . $active . ")</option>\n";
				//$blog_select_options .= "<option value='[blog_link=" . $row['id'] . "]identifier=" . $row['header' . $this->lang->lnbrm] . "&text=&class=link&role=link[/blog_link]'>" . $row['header' . $this->lang->lnbrm] . " (id:" . $row['id'] . ", changed: " . $this->dt->sqldatetime($row['changed']) . ", status: " . $active . ")</option>\n";
			}

		}
// Display Blog

		$this->content .= "
<div class='row'><div class='col-sm-12'><h4>" . AL_LINK_TO_BLOG_POST . "</h4></div></div>
<div class='row'>\n";
		if ( ! empty( $blog_select_options ) > 0 ) {

			$this->content .= "<div class='col-sm-12'>\n";

			$this->content .= $this->l->table();

			$this->content .= "<tr><td>" . $this->l->form() . $this->l->select( "blog_link_button", EMPTY_FREE_TEXT, AUTO_WIDTH ) . $blog_button_select_options . "</select> " . $this->l->select_paste( $editor, $textarea_type, "#blog_link_button", AL_INSERT_BUTTON ) . "</form></td></tr>";
			$this->content .= "<tr><td>" . $this->l->form() . $this->l->select( "blog_link", EMPTY_FREE_TEXT, AUTO_WIDTH ) . $blog_select_options . "</select> " . $this->l->select_paste( $editor, $textarea_type, "#blog_link", AL_INSERT_LINK ) . "</form></td></tr>";

			$this->content .= "</table>";

			$this->content .= "
    </div>\n";
		} else {
			$this->content .= "<div class='col-sm-12'><p>" . AL_NO_BLOGS_AVAILABLE . "</p></div>\n";
		}
		$this->content .= "
</div>
";
	}

	public function get_content() {
		return $this->content;
	}
}