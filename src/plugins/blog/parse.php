<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$parse_plugin = function ( $string ) use ( &$parse_plugin ) {
	/*
		$load_external_code = new class_load_external();
		$l = new class_layout();
	*/
	$blog  = new class_blog();
	$log   = class_logging::getInstance();
	$db    = class_database::getInstance();
	$lang  = class_language::getInstance();
	$loc   = class_location::getInstance();
	$uri   = new class_uri_file_path_operations();
	$p     = new class_page();
	$found = false;

	$tags = 'blog_link|blog';

	while ( preg_match_all( '`\[\b(' . $tags . ')\b=?(.*?)\](.+?)\[/\1\]`s', $string, $matches ) ) {
		foreach ( $matches[0] as $key => $match ) {
			list( $tag, $param, $innertext ) = array( $matches[1][ $key ], $matches[2][ $key ], $matches[3][ $key ] );
			$found       = true;
			$replacement = "";
			switch ( $tag ) {
				case 'blog':
					switch ( $innertext ) {
						case "menu":
							$replacement = $blog->show_blog_menu();
							break;
						case "content":
							$replacement = $blog->show_blog_content();
							break;
						default:
							break;
					}

					break;
				case 'blog_link':
					$blog_id = $param;
					$sql     = "SELECT * FROM `" . TP . "blog_content` WHERE `id` LIKE '$blog_id' AND `deleted` = '0' AND `active` = '1' LIMIT 0,1";
					$result  = $db->query( $sql, __FILE__ . ":" . __LINE__ );
					if ( $result->num_rows != 1 ) {
						$replacement = "Blog entry not found";
						$log->error( "parse", __FILE__ . ":" . __LINE__, "Blog not found for id: '$blog_id', sql: '$sql'" );
					} else {
						$row = $result->fetch_assoc();
						parse_str( $innertext, $link_parameter );

						if ( ! empty( $row[ 'header' . $lang->lnbr ] ) ) {
							$link_header = $uri->translate_special_characters( $row[ 'header' . $lang->lnbr ] );
							$text        = $row[ 'header' . $lang->lnbr ];
						} elseif ( ! empty( $row[ 'header' . $lang->lnbrm ] ) ) {
							$link_header = $uri->translate_special_characters( $row[ 'header' . $lang->lnbrm ] );
							$text        = $row[ 'header' . $lang->lnbrm ];
						} else {
							$link_header = "";
							$log->error( "language", __FILE__ . ":" . __LINE__, "Link header is empty. Id: '$blog_id', sql: '$sql'" );
						}

						$link = $p->fix_link( $loc->httpauto_web_root . $lang->ldir . BLOG_FILE . "?text=" . $link_header . "&content_id=" . $row['id'] );

						if ( ! ( is_null( $link_parameter['text'] ) || ( empty( $link_parameter['text'] ) && $link_parameter != "0" ) ) ) {
							$text = $link_parameter['text'];
						}

						switch ( $link_parameter['role'] ) {
							case "button":
								$replacement = "<a href='" . $link . "' role='button' class='" . $link_parameter['class'] . "'>" . $text . "</a>";
								break;
							default:
								$replacement = "<a href='" . $link . "' class='" . $link_parameter['class'] . "'>" . $text . "</a>";
								break;
						}
					}

					break;
				default:
					$this->log->error( "parse", __FILE__ . ":" . __LINE__, "Tag '$tag' not found." );

					break;
			}
			$string = str_replace( $match, $replacement, $string );
		}

	}

	if ( $found ) {

		$string = $parse_plugin( $string );
	}

	return $string;
};

$class_parse_content = class_parse_content::getInstance();
$class_parse_content->add_function( $parse_plugin );
