<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


class class_slideshow extends class_root {
	protected static $_instance = null;

	public $p;
	public $loc;
	public $db;
	public $log;
	public $l;
	public $lang;

	public $slideshow_counter_fade;

	public function __construct() {

		$this->l                      = new class_layout();
		$this->p                      = new class_page();
		$this->loc                    = class_location::getInstance();
		$this->db                     = class_database::getInstance();
		$this->log                    = class_logging::getInstance();
		$this->lang                   = class_language::getInstance();
		$this->slideshow_counter_fade = 0;
	}

	public function event( $type, $location, $text, $escape = true ) {
		$this->log->event( $type, $location, $text, $escape );
	}

	public function error( $type, $location, $text, $escape = true ) {
		$this->log->error( $type, $location, $text, $escape );
	}

	public function notice( $type, $location, $text, $escape = true ) {
		$this->log->notice( $type, $location, $text, $escape );
	}

	public function mail( $type, $location, $text, $escape = true ) {
		$this->log->mail( $type, $location, $text, $escape );
	}


	public static function getInstance() {
		if ( null === self::$_instance ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	protected function __clone() {
	}

	public function slideshow( $name ) {
		$content = "";
		$iii     = 1;

		$sql    = "SELECT * FROM `" . TP . "carousel` WHERE `name` LIKE '$name' AND `deleted` = '0' AND `active` = '1'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 0 ) {
			$row = $result->fetch_assoc();
			if ( ! ( $row['deleted'] == 1 || $row['active'] == 0 ) ) {
				$sql2    = "SELECT * FROM `" . TP . "carousel_content` WHERE `to_id` = '" . $row['id'] . "'  AND `deleted` = '0' AND `active` = 1 ORDER BY `pos`";
				$result2 = $this->db->query( $sql2, __FILE__ . ":" . __LINE__ );
				if ( $result2->num_rows != 0 ) {
					$content_db = array();
					while ( $row2 = $result2->fetch_assoc() ) {
						if ( is_null( $row2[ 'text' . $this->lang->lnbr ] ) ) {
							$content_lang_number = $this->lang->lnbrm;
						} else {
							$content_lang_number = $this->lang->lnbr;
						}
						if ( is_null( $row2[ 'caption' . $this->lang->lnbr ] ) ) {
							$caption_lang_number = $this->lang->lnbrm;
						} else {
							$caption_lang_number = $this->lang->lnbr;
						}

						$row2['text']    = $row2[ 'text' . $content_lang_number ];
						$row2['caption'] = $row2[ 'caption' . $caption_lang_number ];

						$content_db[] = $row2;
					}

					$style  = "";
					$width  = "";
					$height = "";
					if ( ! empty( $row['x_size'] ) ) {
						$width = "max-width:" . $row['x_size'] . ";";
					}
					if ( ! empty( $row['y_size'] ) ) {
						$height = "max-height:" . $row['y_size'] . ";";
					}
					if ( ! empty( $row['y_size'] ) || ! empty( $row['x_size'] ) ) {
						$style = "style='" . $width . $height . "'";
					}

					if ( $row['type'] == 1 ) {
						$content .= "<div id=\"" . $row['name'] . "\" class=\"carousel slide carousel-fade\" data-interval=\"" . $row['duration'] . "\"  data-ride=\"carousel\" $style>\n";

						if ( $this->slideshow_counter_fade == 0 ) {
							$GLOBALS['head_bottom'] .= "    <style>
        .carousel-fade .carousel-inner .item {
            opacity: 0;
            -webkit-transition-property: opacity;
            -moz-transition-property: opacity;
            -o-transition-property: opacity;
            transition-property: opacity;
        }
        .carousel-fade .carousel-inner .active {
            opacity: 1;
        }
        .carousel-fade .carousel-inner .active.left,
        .carousel-fade .carousel-inner .active.right {
            left: 0;
            opacity: 0;
            z-index: 1;
        }
        .carousel-fade .carousel-inner .next.left,
        .carousel-fade .carousel-inner .prev.right {
            opacity: 1;
        }
        .carousel-fade .carousel-control {
            z-index: 2;
        }
        html,
        body,
        .carousel,
        .carousel-inner,
        .carousel-inner .item {
            height: 100%;
        }
    </style>
    ";
						}

						if ( ! empty( $row['background_color'] ) ) {
							$GLOBALS["head_bottom"] .= "
    <style>
        #" . $row['name'] . " {
            background-color: " . $row['background_color'] . ";
        }
    </style>
    ";
						}
					}
					if ( $row['type'] == 2 ) {
						$content .= "<div id=\"" . $row['name'] . "\" class=\"carousel slide\" data-interval=\"" . $row['duration'] . "\" $style  data-ride=\"carousel\">\n";


						if ( ! empty( $row['background_color'] ) ) {
							$GLOBALS["head_bottom"] .= "    <style>\n";
							$GLOBALS["head_bottom"] .= "
    #" . $row['name'] . " {
        background-color: " . $row['background_color'] . ";
    }\n";
							$GLOBALS["head_bottom"] .= "    </style>\n";
						}

					}

					if ( $row['indicators'] ) {
						$content .= "<ol class=\"carousel-indicators\">\n";
						for ( $i = 0; $i < $result2->num_rows; $i ++ ) {
							$content .= "<li data-target=\"#" . $row['name'] . "\" data-slide-to=\"$i\"";
							if ( $i == 0 ) {
								$content .= " class=\"active\"";
							}
							$content .= "></li>\n";
						}
						$content .= "</ol>\n";
					}

					$content .= "<div class=\"carousel-inner\">\n";
					for ( $i = 0; $i < $result2->num_rows; $i ++ ) {
						$content .= "<div class=\"item";
						if ( $i == 0 ) {
							$content .= " active";
						}
						$content .= "\">\n" . $content_db[ $i ]['text'];

						if ( $row['caption'] ) {
							$content .= "<div class=\"carousel-caption\">\n" . $content_db[ $i ]['caption'] . "\n</div>\n";
						}

						$content .= "</div>\n";
					}
					$content .= "</div>\n";

					if ( $row['control'] ) {
						$content .= "
                                        <a class=\"left carousel-control\" href=\"#" . $row['name'] . "\" data-slide=\"prev\">
                                            <span class=\"icon-prev\"></span>
                                        </a>
                                        <a class=\"right carousel-control\" href=\"#" . $row['name'] . "\" data-slide=\"next\">
                                            <span class=\"icon-next\"></span>
                                        </a>
                                    ";
					}
					$content .= "</div>\n";
				} else {
					$content .= "<div style='background-color:#000000;color:#FFFFFF;text-align:center;'>
                                <p>No content found</p></div>\n";
					$this->log->error( "content", __FILE__ . ":" . __LINE__, "No content found for carousel " . $row['name'] );
				}
				if ( $row['type'] == 1 ) {
					$this->slideshow_counter_fade ++;
				}
			}
		} else {
			$content .= "<div style='background-color:#000000;color:#FFFFFF;text-align:center;'>
                        <p>Slideshow not found</p></div>\n";
			$this->log->error( "content", __FILE__ . ":" . __LINE__, "No carousel found: " . $name );
		}

		return $content;
	}
}