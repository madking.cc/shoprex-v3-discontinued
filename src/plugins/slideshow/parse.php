<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$parse_plugin = function ( $string ) use ( &$parse_plugin ) {
	/*
		$load_external_code = new class_load_external();
		$lang = class_language::getInstance();
		$log = class_logging::getInstance();
		$p = new class_page();
		$l = new class_layout();
		$loc = class_location::getInstance();
		$db = class_database::getInstance();
	*/
	$found = false;

	$tags = 'slideshow|ss';

	while ( preg_match_all( '`\[\b(' . $tags . ')\b=?(.*?)\](.+?)\[/\1\]`s', $string, $matches ) ) {
		foreach ( $matches[0] as $key => $match ) {
			list( $tag, $param, $innertext ) = array( $matches[1][ $key ], $matches[2][ $key ], $matches[3][ $key ] );
			$found       = true;
			$replacement = "";
			switch ( $tag ) {
				case 'ss':
				case 'slideshow':
					$name                          = $innertext;
					$load_external                 = class_load_external::getInstance();
					$load_external->load_carousel  = true;
					$load_external->load_bootstrap = true;

					$class_slideshow = class_slideshow::getInstance();
					$replacement     = $class_slideshow->slideshow( $name );

					break;
				default:
					$this->log->error( "parse", __FILE__ . ":" . __LINE__, "Tag '$tag' not found." );

					break;
			}
			$string = str_replace( $match, $replacement, $string );
		}

	}

	if ( $found ) {

		$string = $parse_plugin( $string );
	}


	return $string;
};

$class_parse_content = class_parse_content::getInstance();
$class_parse_content->add_function( $parse_plugin );
