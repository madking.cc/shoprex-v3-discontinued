<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2015}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_CAROUSEL;

class class_content_carousel extends class_sys {
	public $content;
	protected $types;

	public function __construct( $action ) {
		parent::__construct();

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );

		$this->types   = array();
		$this->types[] = "Fade";
		$this->types[] = "Slide";

		switch ( $action ) {
			case "carousel_add":
				$this->content .= $this->carousel_add();
				$this->content .= $this->start();
				break;
			case "carousel_edit":
				$this->content .= $this->carousel_edit();
				break;
			case "carousel_delete":
				$this->content .= $this->carousel_delete();
				$this->content .= $this->start();
				break;
			case "carousel_options":
				$this->content .= $this->carousel_options();
				break;
			case "carousel_options_update":
				$this->carousel_options_update();
				break;
			case "carousel_options_update_and_back":
				$this->carousel_options_update( NO_RELOAD );
				$this->content .= $this->start();
				break;

			case "carousel_status":
				$this->content .= $this->carousel_status();
				$this->content .= $this->start();
				break;

			case "content_edit":
				$this->content .= $this->content_edit();
				break;
			case "content_update":
				$content_error = $this->content_update();
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_edit();
				}
				break;
			case "content_update_no_back":
				$content_error = $this->content_update( STAY_ON_PAGE );
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
				}
				$this->content .= $this->content_edit();
				break;

			case "content_save":
				$this->content .= $this->content_save();
				$this->content .= $this->carousel_edit();
				break;
			case "content_delete":
				$this->content .= $this->delete_content();
				$this->content .= $this->carousel_edit();
				break;
			case "content_status":
				$this->content .= $this->content_status();
				$this->content .= $this->carousel_edit();
				break;
			case "content_move":
				$this->content .= $this->content_move();
				$this->content .= $this->carousel_edit();
				break;
			/*
			case "content_update_caption":
				$this->content .= content_update_caption();
				$this->content .= carousel_edit();
				break;
		*/
			case "search":
			case "init":
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}


	public function start() {
		$content = "";

		$reset_search = $this->p->get( "reset_search", 0 );
		$search       = $this->p->get_session( "search", "", "text_search", $reset_search );
		$intext       = $this->p->get_session( "intext", 0, "text_intext", $reset_search );

		$content .= $this->l->display_message_by_session( 'carousel_options_updated', AL_SETTINGS_SAVED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_CONTENT_UPDATED );

		if ( empty( $search ) || ! $intext ) {
			$search_text = "";
			if ( ! empty( $search ) ) {
				$search_text = "AND " . $this->db->search( "name", $search );
			}
			$sql    = "SELECT *, `" . TP . "carousel`.`active` AS `carousel_active` FROM `" . TP . "carousel` WHERE `deleted` = '0' $search_text ORDER BY `name`";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$carousel_search_match_pages = array();

			$search_text = "";
			$sql         = "SELECT *, `" . TP . "carousel`.`active` AS `carousel_active`, `" . TP . "carousel`.`id` AS `carousel_id`, `" . TP . "carousel_content`.`id` AS `content_id` FROM `" . TP . "carousel`
        INNER JOIN `" . TP . "carousel_content` ON (`" . TP . "carousel_content`.`to_id` = `" . TP . "carousel`.`id`)
        WHERE (" . $this->db->search( "text", $search ) . " $search_text) AND `" . TP . "carousel_content`.`deleted`=0 AND `" . TP . "carousel`.`deleted`=0
        ORDER BY `" . TP . "carousel`.`created` DESC, `" . TP . "carousel`.`changed` DESC, `" . TP . "carousel_content`.`created` DESC, `" . TP . "carousel_content`.`changed` DESC";
			$result      = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			$entry_counter = 0;
			while ( $row = $result->fetch_assoc() ) {
				if ( isset( $row['pos'] ) ) {
					$carousel_search_match_pages[ $entry_counter ]['pos']        = $row['pos'];
					$carousel_search_match_pages[ $entry_counter ]['id_content'] = $row['content_id'];
					$entry_counter ++;
				}
			}
			if ( sizeof( $carousel_search_match_pages ) > 0 ) {
				$this->p->aasort( $carousel_search_match_pages, "pos" );
			}
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		}

		if ( ! empty( $search ) ) {
			$content .= "<h3 class='underline'>" . AL_CAROUSEL_SEARCH_RESULTS . "</h3>\n";
		}

		if ( ! empty( $search ) || $result->num_rows > 0 ) {
			$content .= " " . $this->l->form_admin( "name='form_search'" ) . $this->l->text( "search", $search, "250", "id='search_text'" ) . $this->l->hidden( "do", "search" ) . " " . $this->l->submit( AL_SEARCH, "id='search'" ) . " " . $this->l->checkbox( "intext", "1", $intext ) . " In Seiten
                <span id='message' class='error'>| " . AL_ERROR . ": " . AL_MISSING_SEARCH_TEXT . "</span>\n";
			$content .= "    </form>\n";

			if ( ! empty( $search ) ) {
				$content .= $this->l->form_admin() . $this->l->hidden( "reset_search", "1" ) . $this->l->submit( AL_RESET );
				$content .= "    </form>\n";
			}

			$GLOBALS['body_footer'] .= "
        <script>

            $(document).ready(function () {
                $(\"#message\").hide();
            });
            $(\"form[name=form_search]\").submit(function( event ) {
                var val = $(\"input[name=search]\").val();
                if(val == '')
                {
                    $(\"#message\").fadeIn();
                    event.preventDefault();
                }
                else
                {
                    $(\"#message\").fadeOut();
                }
            });
             </script>\n";
		}

		if ( $result->num_rows > 0 ) {
			if ( empty( $search ) ) {
				$content .= "<h3 class='underline'>" . AL_EXISTING_CAROUSELS . "</h3>\n";
			}

			$handler = $this->tbl->tbl_init( AL_TBL_NAME, AL_TBL_NUMBER_OF_PAGES, AL_TBL_DATE, AL_TBL_ID, AL_TBL_ACTIVE, array(
				AL_TBL_ACTION,
				"nosort"
			) );

			$entry_counter = 0;

			$carousel_remember = array();

			while ( $row = $result->fetch_assoc() ) {
				if ( isset( $row['carousel_id'] ) ) {
					$row['id'] = $row['carousel_id'];
				}

				if ( ! in_array( $row['id'], $carousel_remember ) ) {
					$carousel_remember[] = $row['id'];
				} else {
					continue;
				}

				$sql2        = "SELECT `id` FROM `" . TP . "carousel_content` WHERE `to_id` = '" . $row['id'] . "' AND `deleted` = '0'";
				$result2     = $this->db->query( $sql2, __FILE__ . ":" . __LINE__ );
				$content_sum = $result2->num_rows;
				if ( ! isset( $content_sum ) ) {
					$content_sum = 0;
				}

				$date = $row['changed'];
				if ( $row['changed'] == "0000-00-00 00:00:00" ) {
					$date = $row['created'];
				}
				$date      = $this->dt->sqldatetime( $date );
				$timestamp = strtotime( $date );

				$parameter = "id=" . $row["id"];

				if ( $row['carousel_active'] == "0" ) {
					$submit_status = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				} elseif ( $row['carousel_active'] == "1" ) {
					$submit_status = $this->l->submit( AL_DEACTIVATE );
				}

				$active = $this->lang->answer( $row['carousel_active'] );

				// Display Pagenumbers with Link in Carousel which matches search
				if ( ! empty( $search ) && $intext ) {
					if ( sizeof( $carousel_search_match_pages ) > 0 ) {
						$matches = " (" . AL_RESULTS_FOR . ": ";
						foreach ( $carousel_search_match_pages as $value ) {
							$matches .= $this->l->link( ( $value['pos'] + 1 ), ADMINDIR . "carousel_content_edit.php", "id=" . $row['id'] . "&id_content=" . $value['id_content'] ) . ", ";
						}
						$matches = substr( $matches, 0, - 2 );
						$matches .= ")";
					}
					$content_sum .= $matches;
				}

				$this->tbl->tbl_load( $handler, $row['name'], $content_sum, array(
					$timestamp,
					"time",
					$date
				), $row['id'], $active, $this->l->form_admin( "", "carousel_content.php", $parameter ) . $this->l->submit( AL_CONTENT ) . "</form> " . $this->l->form_admin( "", "carousel_options.php", $parameter ) . $this->l->submit( AL_OPTIONS ) . "</form> " . $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "id", $row["id"] ) . $this->l->hidden( "do", "carousel_status" ) . $submit_status . "</form> " . $this->l->form_admin( "id=\"form_admin_delete_carousel_$entry_counter\"", DEFAULT_ACTION, $parameter ) . $this->l->hidden( "do", "carousel_delete" ) . $this->l->hidden( "id", $row["id"] ) . $this->l->button( AL_DELETE, "id=\"button_delete_carousel_$entry_counter\"" ) . "</form>" );
				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_CAROUSEL_PART01 . " \"" . $row['name'] . "\" " . AL_DELETE_CAROUSEL_PART02, "#button_delete_carousel_$entry_counter", "form_admin_delete_carousel_$entry_counter", "confirm_delete_$entry_counter" );
				$entry_counter ++;
			}
			$content .= $this->tbl->tbl_out( $handler, 0 );
		} else {
			if ( ! empty( $search ) ) {
				$content .= "<h3>" . AL_NO_MATCHES . "</h3>";
			} else {
				$content .= "<h3>" . AL_NO_ENTRIES . "</h3>";
			}
		}

		$content .= "<h3 class='underline'>" . AL_ADD_CAROUSEL . "</h3>\n";

		$new_carousel_name = "";
		$x_size            = "";
		$y_size            = "";
		$type              = 1;
		$indicators        = 1;
		$control           = 1;
		$caption           = 1;
		$background_color  = "";
		$duration          = "4000";
		$enhanced_editor   = "1";

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "carousel_add" ) . $this->l->table() . "<tr><th>" . AL_TBL_NAME . "</th><td colspan='3'>" . $this->l->text( "name", $new_carousel_name ) . "</td></tr><tr><th>" . AL_TBL_SIZE . "</th><td colspan='3'>" . $this->l->text( "x_size", $x_size, "100" ) . " x " . $this->l->text( "y_size", $y_size, "100" ) . " y " . AL_HINT_ALLOWED_TYPES_FOR_CAROUSEL_SIZE . "</td></tr>
        <tr><th>" . AL_TBL_TYPE . "</th><td colspan='3'>" . $this->l->select( "type" ) . "
        <option value='1'";
		if ( $type == 1 ) {
			$content .= " selected=selected";
		}
		$content .= ">" . $this->types[0] . "</option>
        <option value='2'";
		if ( $type == 2 ) {
			$content .= " selected=selected";
		}
		$content .= ">" . $this->types[1] . "</option></select></td></tr>\n" . "<tr><th>" . AL_TBL_DURATION . "</th><td colspan='3'>" . $this->l->text( "duration", $duration ) . "</td></tr>\n" . "<tr><th>" . AL_TBL_OPTIONS . "</th><td>" . $this->l->checkbox( "indicators", "1", $indicators ) . " " . AL_INDICATORS . "</td><td>" . $this->l->checkbox( "control", "1", $control ) . " " . AL_CONTROL . "</td><td>" . $this->l->checkbox( "caption", "1", $caption ) . " " . AL_CAPTIONS . "</td></tr>
        <tr><th>" . AL_TBL_BACKGROUNDCOLOR . "</th><td colspan='3'>" . $this->l->color( "background_color", $background_color ) . "</td></tr>
        <tr><th>" . AL_TBL_ENHANCED_EDITOR . "</th><td colspan='3'>" . $this->l->select_yesno( "enhanced_editor", $enhanced_editor ) . "</td></tr>
        <tr><td></td><td colspan='3'>" . $this->l->submit( AL_ADD_NEW_CAROUSEL ) . "</td></tr></table></form>\n";

		$content .= "<p> </p>

        " . $this->l->panel( AL_HELP_CAROUSEL_START, "info", AL_INFORMATION );

		return $content;
	}

	public function carousel_add() {
		$content = "";

		$name             = $this->p->get( "name", "", NOT_ESCAPED );
		$x_size           = $this->p->get( "x_size" );
		$y_size           = $this->p->get( "y_size" );
		$type             = $this->p->get( "type" );
		$indicators       = $this->p->get( "indicators", 0 );
		$control          = $this->p->get( "control", 0 );
		$caption          = $this->p->get( "caption", 0 );
		$background_color = $this->p->get( "background_color" );
		$duration         = $this->p->get( "duration" );
		$enhanced_editor  = $this->p->get( "enhanced_editor", 1 );

		$name = trim( $name );
		$name = $this->p->translate_special_characters( $name, NOT_A_FILE, NO_UNDERLINE );
		$name = $this->db->escape( $name );
		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME );

			return $content;
		}

		$sql    = "SELECT `id` FROM `" . TP . "carousel` WHERE `name` LIKE '$name' AND `deleted` = '0' ";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 0 ) {
			$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02 );

			return $content;
		}

		$this->db->ins( __FILE__ . ":" . __LINE__, "carousel", array(
			"name",
			"x_size",
			"y_size",
			"type",
			"duration",
			"indicators",
			"control",
			"caption",
			"background_color",
			"created",
			"enhanced_editor"
		), array(
			$name,
			$x_size,
			$y_size,
			$type,
			$duration,
			$indicators,
			$control,
			$caption,
			$background_color,
			"NOW()",
			$enhanced_editor
		) );

		$id = $this->db->get_insert_id();

		$content .= $this->l->alert_text_dismiss( "success", AL_CAROUSEL_ADDED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "New carousel saved, name: " . $name . ", Id: " . $id );

		return $content;
	}

	public function carousel_options() {
		$content = "";

		$GLOBALS['subtitle'] = AL_CAROUSEL_OPTIONS;

		$content .= $this->l->display_message_by_session( 'carousel_options_updated', AL_SETTINGS_SAVED );

		$id = $this->p->get( "id" );

		$parameter = "id=" . $id;

		if ( ! ( is_numeric( $id ) && $id > 0 ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_OPEN_CAROUSEL_FOR_EDIT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= $this->start();

			return $content;
		}

		$sql    = "SELECT * FROM `" . TP . "carousel` WHERE `id`='$id' AND `deleted` = '0' ";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 1 ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_OPEN_CAROUSEL_FOR_EDIT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID not found in database, id: " . $id );
			$content .= $this->start();

			return $content;
		}

		$row = $result->fetch_assoc();

		$content .= "<h3 class='underline'>" . AL_CAROUSEL_OPTIONS_FOR . " \"" . $row['name'] . "\"</h3>\n";

		$content .= $this->l->form_admin( "", "carousel_options.php", $parameter ) .
		            $this->l->hidden( "id", $id ) . $this->l->hidden( "do", "carousel_options_update" ) .
		            $this->l->hidden( "old_name", $row['name'] ) . $this->l->table() . "<tr><th>" . AL_TBL_NAME . "</th><td colspan='3'>" . $this->l->text( "name", $row['name'] ) . "</td></tr><tr><th>" . AL_TBL_SIZE . "</th><td colspan='3'>" . $this->l->text( "x_size", $row['x_size'], "100" ) . " x " . $this->l->text( "y_size", $row['y_size'], "100" ) . " y " . AL_HINT_ALLOWED_TYPES_FOR_CAROUSEL_SIZE . "</td></tr>
        <tr><th>" . AL_TBL_TYPE . "</th><td colspan='3'>" . $this->l->select( "type" ) . "
        <option value='1'";
		if ( $row['type'] == 1 ) {
			$content .= " selected=selected";
		}
		$content .= ">" . $this->types[0] . "</option>
        <option value='2'";
		if ( $row['type'] == 2 ) {
			$content .= " selected=selected";
		}
		$content .= ">" . $this->types[1] . "</option></select></td></tr>\n" . "<tr><th>" . AL_TBL_DURATION . "</th><td colspan='3'>" . $this->l->text( "duration", $row['duration'] ) . "</td></tr>\n" . "<tr><th>" . AL_TBL_OPTIONS . "</th><td>" . $this->l->checkbox( "indicators", "1", $row['indicators'] ) . " " . AL_INDICATORS . "</td><td>" . $this->l->checkbox( "control", "1", $row['control'] ) . " " . AL_CONTROL . "</td><td>" . $this->l->checkbox( "caption", "1", $row['caption'] ) . " " . AL_CAPTIONS . "</td></tr>
    <tr><th>" . AL_TBL_BACKGROUNDCOLOR . "</th><td colspan='3'>" . $this->l->color( "background_color", $row['background_color'] ) . "</td></tr>
    <tr><th>" . AL_TBL_ENHANCED_EDITOR . "</th><td colspan='3'>" . $this->l->select_yesno( "enhanced_editor", $row['enhanced_editor'] ) . "</td></tr>
    <tr><td></td><td colspan='3'>" . $this->l->submit( AL_SAVE ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='save_and_back(this.form);'" ) . " " .
		            $this->l->reload_button( "carousel_options.php", "id=$id", AL_RELOAD ) . " " . $this->l->back_button( AL_BACK, "carousel.php", $parameter ) . "
    </td></tr></table></form>\n";

		$GLOBALS['body_footer'] .= "<script>
    function save_and_back(formID)
    {
        formID.do.value = 'carousel_options_update_and_back';
        formID.action = '/" . WEBROOT . ADMINDIR . "carousel.php';
        formID.submit();
    }
    </script>
    ";

		return $content;
	}

	public function carousel_options_update( $reload = true ) {
		$content = "";

		$id               = $this->p->get( "id" );
		$name             = $this->p->get( "name", "", NOT_ESCAPED );
		$old_name         = $this->p->get( "old_name", "" );
		$x_size           = $this->p->get( "x_size" );
		$y_size           = $this->p->get( "y_size" );
		$type             = $this->p->get( "type" );
		$indicators       = $this->p->get( "indicators", 0 );
		$control          = $this->p->get( "control", 0 );
		$caption          = $this->p->get( "caption", 0 );
		$background_color = $this->p->get( "background_color" );
		$duration         = $this->p->get( "duration" );
		$enhanced_editor  = $this->p->get( "enhanced_editor", EDITOR_ENHANCED );

		$name = trim( $name );
		$name = $this->p->translate_special_characters( $name, NOT_A_FILE, NO_UNDERLINE );
		$name = $this->db->escape( $name );

		if ( ! ( is_numeric( $id ) && $id > 0 ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_SAVE_OPTIONS_FOR_CAROUSEL );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME );

			return $content;
		}

		$sql    = "SELECT `id` FROM `" . TP . "carousel` WHERE `name` LIKE '$name' AND `deleted` = '0' ";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 0 && $name != $old_name ) {
			$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02 );

			return $content;
		}

		$this->db->upd( __FILE__ . ":" . __LINE__, "carousel", array(
			"name",
			"x_size",
			"y_size",
			"type",
			"duration",
			"indicators",
			"control",
			"caption",
			"background_color",
			"enhanced_editor"
		), array(
			$name,
			$x_size,
			$y_size,
			$type,
			$duration,
			$indicators,
			$control,
			$caption,
			$background_color,
			$enhanced_editor
		), "`id` = '$id'" );

		$this->log->event( "log", __FILE__ . ":" . __LINE__, "Carousel updated, name: " . $name . ", id: " . $id );

		$_SESSION['carousel_options_updated'] = 1;
		if ( $reload ) {
			$this->l->reload_js( AUTO_PAGE_SELECT, "id=$id" );
		}

		return $content;
	}

	public function carousel_delete() {
		$content = "";

		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "carousel", array( "deleted" ), array( true ), "`id` = '$id'" );
			$this->db->upd( __FILE__ . ":" . __LINE__, "carousel_content", array( "deleted" ), array( true ), "`to_id` = '$id'" );
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_CAROUSEL );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_CAROUSEL_DELETED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "Carousel deleted, id: " . $id );

		return $content;
	}

	public function carousel_edit() {
		$content = "";

		$GLOBALS['subtitle'] = AL_CAROUSEL_CONTENT;

		$content .= $this->l->display_message_by_session( 'content_updated', AL_CONTENT_UPDATED );

		$id = $this->p->get( "id" );

		$change_editortype = $this->p->get( "change_editortype", "-1" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT * FROM `" . TP . "carousel` WHERE `id`='" . $id . "' AND `deleted` = '0' ";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_OPEN_CAROUSEL_FOR_EDIT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= $this->start();

			return $content;
		}

		if ( $result->num_rows != 1 ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_OPEN_CAROUSEL_FOR_EDIT );
			$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
			$content .= $this->start();

			return $content;
		}

		$row       = $result->fetch_assoc();
		$parameter = "id=" . $row["id"];

		$carousel_global_enhanced_editor = $row['enhanced_editor'];

		if ( $change_editortype == "-1" ) {
			$content_to_add = "";
		} else {
			$content_to_add = $this->p->get( "content_to_add", "", NOT_ESCAPED );
		}

		$content .= $this->l->form_admin( "id='form_content_save' name='form_action'", "", $parameter ) . $this->l->hidden( "do", "content_save" ) . $this->l->back_button( AL_BACK, "carousel.php" ) . "<br /><br />";

		$rows = "";
		if ( $carousel_global_enhanced_editor ) {
			if ( $change_editortype == "-1" ) {
				if ( $row['enhanced_editor'] ) {
					$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
					$enhanced_editor = EDITOR_ENHANCED;
				} else {
					$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
					$rows            = "20";
					$enhanced_editor = EDITOR_SIMPLE;
				}
			} elseif ( $change_editortype ) {
				$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
				$enhanced_editor = EDITOR_ENHANCED;
			} else {
				$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
				$rows            = "20";
				$enhanced_editor = EDITOR_SIMPLE;
			}

			$content .= $this->l->hidden( "enhanced_editor", $enhanced_editor ) . $this->l->hidden( "change_editortype", "-1" );

			$action_js = "carousel_edit";

			$GLOBALS['body_footer'] .= "
<script>
    $(\"#change_editor_enhanced\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('1');
        $(\"form[name=form_action]\").submit();
    });
    $(\"#change_editor_simple\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('0');
        $(\"form[name=form_action]\").submit();
    });

</script>
";
		} else {
			$enhanced_editor = EDITOR_SIMPLE;
			$rows            = "20";
			$content .= $this->l->hidden( "enhanced_editor", $row['enhanced_editor'] );
		}

		$content .= $this->l->media_ta( "content_to_add", $content_to_add, UPLOADDIR, $enhanced_editor, $rows ) . "<br /><br />" . $this->l->submit( AL_SAVE_NEW_CONTENT ) . "</form>\n";

		$sql2    = "SELECT * FROM `" . TP . "carousel_content` WHERE `to_id` = '" . $row['id'] . "' AND `deleted` = '0' ORDER BY `pos`";
		$result2 = $this->db->query( $sql2, __FILE__ . ":" . __LINE__ );

		if ( $result2->num_rows != 0 ) {
			if ( $result2->num_rows == 1 ) {
				$show_pos = false;
			} else {
				$show_pos = true;
			}

			$content .= "<br /><br />" . $this->l->table() . "<tr><th>" . AL_TBL_POS . "</th><th>" . AL_TBL_CONTENT . "</th><th>" . AL_TBL_DATE . "</th><th>" . AL_TBL_ID . "</th><th>" . AL_TBL_CHARS . "</th>";
			if ( $show_pos ) {
				$content .= "<th>" . AL_TBL_POS_LONG . "</th>";
			}
			$content .= "<th>" . AL_TBL_ACTIVE . "</th>";
			if ( $row['caption'] ) {
				$content .= "<th>" . AL_TBL_CAPTION . "</th>";
			}
			$content .= "<th>" . AL_TBL_ACTION . "</th></tr>\n";

			$entry_counter = 0;
			while ( $row2 = $result2->fetch_assoc() ) {
				$text = $this->l->fix_ta( $row2[ 'text' . $this->lang->lnbrm ] );
				$text = $this->l->ta( "text_" . $entry_counter, $text, "500", "180", "readonly=\"readonly\"" );

				$datetime = $this->dt->sqldatetime( $row2['changed'] );

				$content .= "<tr><td>" . ( $entry_counter + 1 ) . "</td><td>" . $text . "</td>";
				$content .= "<td>" . $datetime . "</td><td>" . $row2['id'] . "</td>";

				$chars = $this->p->strlen_language_all( "carousel_content", "text", $row2['id'] );

				$content .= "<td>$chars</td>";

				if ( $show_pos ) {
					$content .= "<td>" . $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) .
					            $this->l->hidden( "id_content", $row2["id"] ) .
					            $this->l->hidden( "do", "content_move" ) .
					            $this->l->hidden( "case", "up" ) . $this->l->submit( AL_UP ) . "</form> " .
					            $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "id_content", $row2["id"] ) .
					            $this->l->hidden( "do", "content_move" ) . $this->l->hidden( "case", "down" ) . $this->l->submit( AL_DOWN ) . "</form></td>";
				}

				$active = $this->lang->answer( $row2['active'] );
				$content .= "<td>" . $active . "</td><td>";

				if ( $row['caption'] ) {
					$caption = $this->l->fix_ta( $row2[ 'caption' . $this->lang->lnbrm ] );
					$caption = $this->l->ta( "caption_" . $entry_counter, $caption, "500", "180", "readonly=\"readonly\"" );

					$content .= $caption . "</td><td>";
				}

				$parameter_content = $parameter . "&id_content=" . $row2["id"];

				$content .= $this->l->form_submit_by_lang( INSERT_BREAKS, EMPTY_FREETEXT, "carousel_content_edit.php", $parameter_content ) . "<br />";

				$content .= $this->l->form_admin( "id='form_delete_pic_$entry_counter'", DEFAULT_ACTION, $parameter ) .
				            $this->l->hidden( "do", "content_delete" ) . $this->l->hidden( "id_content", $row2["id"] ) .
				            $this->l->button( AL_DELETE, "id='button_delete_pic_$entry_counter'" ) . "</form> " . $this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_CONTENT_QUESTION, "#button_delete_pic_$entry_counter", "form_delete_pic_$entry_counter" );

				if ( $row2["active"] ) {
					$content .= $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "id_content", $row2["id"] ) . $this->l->hidden( "do", "content_status" ) . $this->l->submit( AL_DEACTIVATE ) . "</form> ";
				} else {
					$content .= $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "id_content", $row2["id"] ) . $this->l->hidden( "do", "content_status" ) . $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" ) . "</form> ";
				}

				$content .= "
                </td>
                </tr>\n";
				$entry_counter ++;
			}

			$content .= "<tr><td colspan='";
			if ( $show_pos ) {
				$add = 1;
			} else {
				$add = 0;
			}

			if ( $row['caption'] ) {
				$content .= 9 + $add;
			} else {
				$content .= 8 + $add;
			}
			$content .= "'>" . $this->l->back_button( AL_BACK, "carousel.php" ) . "</td></tr>\n";
			$content .= "</table>";
		}

		return $content;
	}

	public function content_edit() {
		$content = "";

		$GLOBALS['subtitle'] = AL_EDIT_CAROUSEL_CONTENT;

		$id    = $this->p->get( "id_content" );
		$to_id = $this->p->get( "id" );

		$lang_number        = $this->p->get( "lang" );
		$import_lang_number = $this->p->get( "import_lang" );
		$change_editortype  = $this->p->get( "change_editortype", "-1" );

		$content .= $this->l->display_message_by_session( 'content_updated', AL_CONTENT_UPDATED );

		if ( ! ( is_numeric( $to_id ) && $to_id > 0 ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_CONTENT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "TO_ID value is empty or not numeric: " . $to_id );
			$content .= $this->carousel_edit();

			return $content;
		} else {
			$sql    = "SELECT * FROM `" . TP . "carousel` WHERE `id`='" . $to_id . "' AND `deleted` = '0' ";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 1 ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CONTENT_NOT_FOUND );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
				$content .= $this->carousel_edit();

				return $content;
			}
			$row                             = $result->fetch_assoc();
			$carousel_has_captions           = $row['caption'];
			$carousel_global_enhanced_editor = $row['enhanced_editor'];
		}

		if ( $change_editortype == "-1" ) {
			if ( is_numeric( $id ) && $id > 0 ) {
				$sql    = "SELECT * FROM `" . TP . "carousel_content` WHERE `id`='" . $id . "' AND `deleted` = '0'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				if ( $result->num_rows != 1 ) {
					$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CONTENT_NOT_FOUND );
					$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
					$content .= $this->carousel_edit();

					return $content;
				} else {
					$row = $result->fetch_assoc();
				}
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_CONTENT );
				$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
				$content .= $this->carousel_edit();

				return $content;
			}
		} else {
			$row['enhanced_editor']          = $this->p->get( "enhanced_editor", $carousel_global_enhanced_editor );
			$row[ 'text' . $lang_number ]    = $this->p->get( "content_to_update", "", NOT_ESCAPED );
			$row[ 'caption' . $lang_number ] = $this->p->get( "caption", "", NOT_ESCAPED );
		}

		if ( ! is_null( $import_lang_number ) ) {
			if ( strcasecmp( $import_lang_number, "empty" ) === 0 ) {
				$import_lang_number = "";
			}
			$row[ 'text' . $lang_number ]    = $row[ 'text' . $import_lang_number ];
			$row[ 'caption' . $lang_number ] = $row[ 'caption' . $import_lang_number ];
		}

		$parameter_back    = "id=" . $to_id;
		$parameter_content = $parameter_back . "&id_content=$id";

		if ( $this->lang->count_of_languages > 1 ) {
			$content .= $this->l->language_import_fields( "carousel_content_edit.php", $id, $lang_number, $parameter_content . "&lang=$lang_number" ) .
			            $this->l->language_edit_fields( "carousel_content_edit.php", $id, $lang_number, $parameter_content ) . "<br /><br />\n";
		}

		$content .=
			$this->l->form_admin( "id='form_content_save' name='form_action'", "carousel_content_edit.php", $parameter_back ) .
			$this->l->hidden( "id_content", $id ) .
			$this->l->hidden( "id", $to_id ) . $this->l->hidden( "do", "content_update" ) .
			$this->l->hidden( "do_no_back", "content_update_no_back" ) . $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " .
			$this->l->button( AL_SAVE_AND_BACK, "onclick='check_submit(this.form, false)'" ) . " " .
			$this->l->reload_button( "carousel_content_edit.php", $parameter_content . "&lang=$lang_number", AL_RELOAD ) . " " .
			$this->l->back_button( AL_BACK, "carousel_content.php", $parameter_back ) . "<br /><br />";

		$content .= AL_TBL_ID . " " . $id . "<br /><br />\n";

		if ( $this->lang->count_of_languages > 1 ) {
			$content .= AL_TBL_CURRENT_LANGUAGE . " <strong>" . $this->p->get_language_text( $lang_number ) . $this->l->hidden( "lang", $lang_number ) . "</strong><br /><br />\n";
		} else {
			$content .= $this->l->hidden( "lang", $this->p->lang->lnbrm );
		}

		$rows = "";
		if ( $carousel_global_enhanced_editor ) {
			if ( $change_editortype == "-1" ) {
				if ( $row['enhanced_editor'] ) {
					$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
					$enhanced_editor = EDITOR_ENHANCED;
				} else {
					$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
					$rows            = "20";
					$enhanced_editor = EDITOR_SIMPLE;
				}
			} elseif ( $change_editortype ) {
				$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
				$enhanced_editor = EDITOR_ENHANCED;
			} else {
				$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
				$rows            = "20";
				$enhanced_editor = EDITOR_SIMPLE;
			}

			$content .= $this->l->hidden( "enhanced_editor", $enhanced_editor ) . $this->l->hidden( "change_editortype", "-1" );

			$action_js = "content_edit";

			$GLOBALS['body_footer'] .= "
<script>
    $(\"#change_editor_enhanced\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('1');
        $(\"form[name=form_action]\").submit();
    });
    $(\"#change_editor_simple\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('0');
        $(\"form[name=form_action]\").submit();
    });

</script>
";
		} else {
			$enhanced_editor = EDITOR_SIMPLE;
			$rows            = "20";
			$content .= $this->l->hidden( "enhanced_editor", $row['enhanced_editor'] );
		}

		$content .= $this->l->media_ta( "content_to_update", $row[ 'text' . $lang_number ], UPLOADDIR, $enhanced_editor, $rows ) . "<br /><br />";

		if ( $carousel_has_captions ) {
			$content .= "<h2>" . AL_TBL_CAPTION . "</h2>" . $this->l->media_ta( "caption", $row[ 'caption' . $lang_number ], UPLOADDIR, $enhanced_editor, $rows );
		}

		$content .= "</td></tr><br /><br />" .
		            $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='check_submit(this.form, false)'" ) . " " .
		            $this->l->reload_button( "carousel_content_edit.php", $parameter_content . "&lang=$lang_number", AL_RELOAD ) . " " .
		            $this->l->back_button( AL_BACK, "carousel_content.php", $parameter_back ) . "</form>\n";

		$GLOBALS['body_footer'] .= "<script>
    function check_submit(formID, no_back)
    {
        if(no_back)
        {
            formID.do.value = formID.do_no_back.value;
        }
        formID.submit();
    }
    </script>
    ";

		return $content;
	}

	public function content_update( $stay_on_page = false ) {
		$content = "";

		$lang_number = $this->p->get( "lang" );

		$content_to_update = $this->p->get( "content_to_update", "" );
		$caption           = $this->p->get( "caption", null );
		$id                = $this->p->get( "id_content" );
		$to_id             = $this->p->get( "id" );
		$enhanced_editor   = $this->p->get( "enhanced_editor", EDITOR_ENHANCED );

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_SAVE_CONTENT, true, "", "carousel_content_edit.php" );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty" );

			return $content;
		} else {
			$this->db->upd( __FILE__ . ":" . __LINE__, "carousel_content", array(
				"text" . $lang_number,
				"enhanced_editor",
				"caption" . $lang_number
			), array( $content_to_update, $enhanced_editor, $caption ), "`id` = '$id'" );

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content updated, id: " . $id . ", content: " . $content_to_update );

			$_SESSION['content_updated'] = 1;

			if ( ! $stay_on_page ) {
				$this->l->reload_js( "carousel_content.php", "id=$to_id" );
			}

			return $content;
		}
	}

	public function content_save() {
		$content = "";

		$lang_number = $this->p->get( "lang" );

		$content_add     = $this->p->get( "content_to_add", "" );
		$id              = $this->p->get( "id" );
		$enhanced_editor = $this->p->get( "enhanced_editor", EDITOR_ENHANCED );
		$caption         = $this->p->get( "caption", null );

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_SAVE_CONTENT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty" );

			return $content;
		} else {

			$sql    = "SELECT `pos` FROM `" . TP . "carousel_content` WHERE `to_id` = '$id' AND `deleted` = '0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 0 ) {
				$pos = $result->num_rows;
			} else {
				$pos = 0;
			}

			$this->db->ins( __FILE__ . ":" . __LINE__, "carousel_content", array(
				"to_id",
				"text" . $lang_number,
				"caption" . $lang_number,
				"pos",
				"enhanced_editor",
				"created"
			), array( $id, $content_add, $caption, $pos, $enhanced_editor, "NOW()" ) );

			$content .= $this->l->alert_text_dismiss( "success", AL_CONTENT_SAVED );
			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content added to carousel, id: " . $id . ", content: " . $content_add );

			return $content;
		}
	}

	public function delete_content() {
		$content = "";

		$id_content = $this->p->get( "id_content" );
		$id         = $this->p->get( "id" );

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_CONTENT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty" );

			return $content;
		} elseif ( empty( $id_content ) || ! is_numeric( $id_content ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_CONTENT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "id_content value is empty" );

			return $content;
		} else {
			$sql    = "SELECT `text` FROM `" . TP . "carousel_content` WHERE id = '$id_content' AND `deleted` = '0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 1 ) {
				$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_CONTENT );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "Content ID $id_content not found" );

				return $content;
			} else {
				$this->db->upd( __FILE__ . ":" . __LINE__, "carousel_content", array( "deleted" ), array( true ), "`id` = '$id_content'" );

				$new_pos = 0;
				$sql     = "SELECT `id` FROM `" . TP . "carousel_content` WHERE `to_id` = '$id' AND `deleted` = '0' ORDER BY `pos`";
				$result  = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				if ( $result->num_rows != 0 ) {
					while ( $row = $result->fetch_assoc() ) {

						$this->db->upd( __FILE__ . ":" . __LINE__, "carousel_content", array( "pos" ), array( $new_pos ), "`id` = '" . $row['id'] . "'" );

						$new_pos ++;
					}
				}

				$content .= $this->l->alert_text_dismiss( "success", AL_CONTENT_DELETED );
				$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content in carousel deleted, id: " . $id );

				return $content;
			}
		}
	}

	public function content_status() {
		$content = "";

		$id_content = $this->p->get( "id_content" );
		$id         = $this->p->get( "id" );

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_OPEN_CAROUSEL_FOR_EDIT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty" );

			return $content;
		} elseif ( empty( $id_content ) || ! is_numeric( $id_content ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_CONTENT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "id_content value is empty" );

			return $content;
		} else {
			$sql    = "SELECT `active` FROM `" . TP . "carousel_content` WHERE `id` = '$id_content' AND `deleted` = '0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 1 ) {
				$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_CONTENT );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "Content ID $id_content not found" );

				return $content;
			} else {
				$row = $result->fetch_assoc();

				if ( $row['active'] ) {
					$this->db->upd( __FILE__ . ":" . __LINE__, "carousel_content", array( "active" ), array( false ), "`id` = '$id_content'" );

					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content status in carousel changed, id: " . $id_content . ", new status: inactive" );
				} else {
					$this->db->upd( __FILE__ . ":" . __LINE__, "carousel_content", array( "active" ), array( true ), "`id` = '$id_content'" );
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content status in carousel changed, id: " . $id_content . ", new status: active" );
				}

				$content .= $this->l->alert_text_dismiss( "success", AL_STATUS_CHANGED );

				return $content;
			}
		}
	}

	public function content_move() {
		$content = "";

		$id_content = $this->p->get( "id_content" );
		$id         = $this->p->get( "id" );
		$case       = $this->p->get( "case" );

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_OPEN_CAROUSEL_FOR_EDIT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty" );

			return $content;
		} elseif ( empty( $id_content ) || ! is_numeric( $id_content ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CONTENT_NOT_FOUND );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "id_content value is empty" );

			return $content;
		} elseif ( empty( $case ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CONTENT_NOT_MOVEABLE );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "CASE value is empty" );

			return $content;
		} else {
			$sql    = "SELECT `pos` FROM `" . TP . "carousel_content` WHERE `id` = '$id_content' AND `deleted` = '0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 1 ) {
				$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CONTENT_NOT_MOVEABLE );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "Content ID $id_content not found" );

				return $content;
			} else {
				$row     = $result->fetch_assoc();
				$old_pos = $row['pos'];

				$sql     = "SELECT `id` FROM `" . TP . "carousel_content` WHERE `to_id` = '$id' AND `deleted` = '0'";
				$result2 = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				$max_pos = $result2->num_rows - 1;

				if ( $case == "up" ) {
					if ( $old_pos == 0 ) {
						$new_pos = $max_pos;
						$sql     = "UPDATE `" . TP . "carousel_content` SET `pos` = `pos` - 1 WHERE `to_id` ='$id'";
						$this->db->query( $sql, __FILE__ . ":" . __LINE__ );

						$this->db->upd( __FILE__ . ":" . __LINE__, "carousel_content", array( "pos" ), array( $max_pos ), "`id` = '$id_content'" );

						$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content position in carousel changed, id: " . $id_content . ", old pos: $old_pos, new pos: $new_pos" );
					} else {
						$new_pos = $old_pos - 1;
						$sql     = "UPDATE `" . TP . "carousel_content` SET `pos` = `pos` + 1 WHERE `pos` = '" . $new_pos . "' AND `to_id` ='$id'";
						$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						$sql = "UPDATE `" . TP . "carousel_content` SET `pos` = `pos` - 1 WHERE `id` = '" . $id_content . "'";
						$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content position in carousel changed, id: " . $id_content . ", old pos: $old_pos, new pos: $new_pos" );
					}
				} elseif ( $case == "down" ) {
					if ( $old_pos == $max_pos ) {
						$new_pos = 0;
						$sql     = "UPDATE `" . TP . "carousel_content` SET `pos` = `pos` + 1 WHERE `to_id` ='$id'";
						$this->db->query( $sql, __FILE__ . ":" . __LINE__ );

						$this->db->upd( __FILE__ . ":" . __LINE__, "carousel_content", array( "pos" ), array( 0 ), "`id` = '$id_content'" );

						$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content position in carousel changed, id: " . $id_content . ", old pos: $old_pos, new pos: $new_pos" );
					} else {
						$new_pos = $old_pos + 1;
						$sql     = "UPDATE `" . TP . "carousel_content` SET `pos` = `pos` - 1 WHERE `pos` = '" . $new_pos . "' AND `to_id` ='$id'";
						$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						$sql = "UPDATE `" . TP . "carousel_content` SET `pos` = `pos` + 1 WHERE `id` = '" . $id_content . "'";
						$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content position in carousel changed, id: " . $id_content . ", old pos: $old_pos, new pos: $new_pos" );
					}
				} else {
					$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CONTENT_NOT_MOVEABLE );
					$this->log->error( "sql", __FILE__ . ":" . __LINE__, "CASE value is empty or wrong: " . $case );

					return $content;
				}

				$content .= $this->l->alert_text_dismiss( "success", AL_CONTENT_MOVED );

				return $content;
			}
		}
	}

	public function carousel_status() {
		$content = "";

		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT `active` FROM `" . TP . "carousel` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();
				if ( $row['active'] == 0 ) {
					$active = 1;
				} else {
					$active = 0;
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "carousel", array( "active" ), array( $active ), "`id` = '$id'" );
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_CAROUSEL_STATUS );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_CAROUSEL_STATUS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_STATUS_CHANGED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "carousel status changed, id: " . $id );

		return $content;
	}
}

if ( ! isset( $action ) ) {
	$action = null;
}

$class_content_carousel = new class_content_carousel( $action );
$content .= $class_content_carousel->get_content();