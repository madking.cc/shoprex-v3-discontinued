<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or define( "SECURITY_CHECK", "OK" );

if ( session_status() !== PHP_SESSION_ACTIVE ) {
	session_start();
}

if ( ! defined( "DONT_MODIFY" ) ) {
	define( "DONT_MODIFY", 1 );
}

$this_dir_root = realpath( __DIR__ ) . "/";
define( "DIRROOT", $this_dir_root );

$is_visitor = false;

require_once( DIRROOT . "__include/init.php" );


function error( $details = "", $location = "" ) {
	$log = class_logging::getInstance();

	if ( empty( $location ) ) {
		$location = __FILE__ . ":" . __LINE__;
	}

	$datails_log = str_replace( "<br />", "\r", $details );
	$log->error( "security", $location, $datails_log );
	if ( isset( $_POST['input_form_admin'] ) && $_POST['input_form_admin'] ) {
		$GLOBALS['is_admin'] = true;
		new class_throw_error( "form-error: " . $location . " " . $details );
	} else {
		$p = new class_page();
		$p->load_location();
	}
}

if ( isset( $_POST['input_form_action'] ) && isset( $_POST['input_form_id'] ) ) {
	$form_action = $p->get( "input_form_action", "", true, false, false );
	$form_id     = $p->get( "input_form_id", "", true, false, false );

	if ( empty( $form_id ) ) {
		error( "Input Form ID is empty after a form submit.", __FILE__ . ":" . __LINE__ );
	} elseif ( isset( $_SESSION["input_security_form_id"][ $form_id ] ) && $_SESSION["input_security_form_id"][ $form_id ] ) {
		$class_get = new class_get();
		$class_get->store_post();

		if ( ! ( isset( $_POST['input_keep_form_ids'] ) && intval( $_POST['input_keep_form_ids'] ) == "3612423216325637235" ) ) {
			unset( $_SESSION["input_security_form_id"][ $form_id ] );
		}
		$p->load_location( $loc->httpauto_root . $form_action, DONT_MODIFY );
		require_once( DIRROOT . "__include/terminate.php" );
	} else {
		error( "security_form_id " . $form_id . " not set! form_action: '" . $form_action . "'", __FILE__ . ":" . __LINE__ );

	}
} else {
	error( "Single Call of this file. form_action and form_id are not set.", __FILE__ . ":" . __LINE__ );
}

