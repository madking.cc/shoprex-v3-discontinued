<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$admin_language = ADMIN_LANGUAGE;
if ( empty( $admin_language ) ) {
	$number_search = "IS NULL";
} else {
	$number_search = "LIKE BINARY '" . ADMIN_LANGUAGE . "'";
}

$sql    = "SELECT `setlocale_time` FROM `" . TP . "language_admin_settings` WHERE `number` $number_search LIMIT 0,1";
$result = $db->query( $sql, __FILE__ . ":" . __LINE__ );
if ( isset( $row ) ) {
	unset( $row );
}
if ( $result->num_rows == 1 ) {
	$row = $result->fetch_assoc();
}

if ( ! isset( $row['setlocale_time'] ) || empty( $row['setlocale_time'] ) ) {
	$result = setlocale( LC_TIME, $row['setlocale_time'] );
	if ( $result == false ) {
		$result = setlocale( LC_TIME, "en_US.utf8" );
		if ( $result == false ) {
			$log->error( "language", __FILE__ . ":" . __LINE__, "Can't set locale to: '" . $row['setlocale_time'] . "'" );
		}
	}
}