<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

function set_subtitle( $row ) {
	$lang = class_language::getInstance();

	if ( ! is_null( $row[ 'subtitle' . $lang->lnbr ] ) ) {
		$GLOBALS['subtitle'] = $row[ 'subtitle' . $lang->lnbr ];
	} elseif ( ! is_null( $row[ 'subtitle' . $lang->lnbrm ] ) ) {
		$GLOBALS['subtitle'] = $row[ 'subtitle' . $lang->lnbrm ];
	}
}

function set_body_class( $row ) {
	if ( ! is_null( $row['class_body'] ) ) {
		$GLOBALS["body_class"] .= " " . $row['class_body'];
	}
}

function get_text( $row ) {
	$lang = class_language::getInstance();
	$log  = class_logging::getInstance();

	$l = new class_layout();
	$p = new class_page();

	$content = "";
	if ( $row['parse_php'] && ! $l->get_preview_status() ) {
		if ( ! is_null( $row[ 'text' . $lang->lnbr ] ) ) {
			$eval_result = eval( $row[ 'text' . $lang->lnbr ] );
			if ( $eval_result === false ) {
				$log->error( "eval", __FILE__ . ":" . __LINE__, "Parse Error" );
				new class_throw_error( "eval: " . __FILE__ . ":" . __LINE__ . " Parse Error" );
			} else {
				return $content;
			}
		} elseif ( ! is_null( $row[ 'text' . $lang->lnbrm ] ) ) {
			$eval_result = eval( $row[ 'text' . $lang->lnbrm ] );
			if ( $eval_result === false ) {
				$log->error( "eval", __FILE__ . ":" . __LINE__, "Parse Error" );
				new class_throw_error( "eval: " . __FILE__ . ":" . __LINE__ . " Parse Error" );
			} else {
				return $content;
			}
		}

		return "";
	} else {
		if ( ! is_null( $row[ 'text' . $lang->lnbr ] ) ) {
			return $p->preview_parse( $row[ 'text' . $lang->lnbr ] );
		} elseif ( ! is_null( $row[ 'text' . $lang->lnbrm ] ) ) {
			return $p->preview_parse( $row[ 'text' . $lang->lnbrm ] );
		}
	}
}

function set_meta( $row ) {
	$lang = class_language::getInstance();

	if ( ! empty( $row[ 'meta_description' . $lang->lnbr ] ) ) {
		$GLOBALS['description'] = ( $row[ 'meta_description' . $lang->lnbr ] );
	} elseif ( ! empty( $row[ 'meta_description' . $lang->lnbrm ] ) ) {
		$GLOBALS['description'] = ( $row[ 'meta_description' . $lang->lnbrm ] );
	}

	if ( ! empty( $row[ 'meta_keywords' . $lang->lnbr ] ) ) {
		$GLOBALS['keywords'] = ( $row[ 'meta_keywords' . $lang->lnbr ] );
	} elseif ( ! empty( $row[ 'meta_keywords' . $lang->lnbrm ] ) ) {
		$GLOBALS['keywords'] = ( $row[ 'meta_keywords' . $lang->lnbrm ] );
	}
}

function no_content() {
	return "<!--No-content-given-->";
}

function set_js( $row ) {
	$lang           = class_language::getInstance();
	$class_external = class_load_external::getInstance();

	$db_page_has_js = false;
	if ( ! is_null( $row[ 'js' . $lang->lnbr ] ) ) {
		$GLOBALS["body_footer"] .= $row[ 'js' . $lang->lnbr ];
		$db_page_has_js = true;
	} elseif ( ! is_null( $row[ 'js' . $lang->lnbrm ] ) ) {
		$GLOBALS["body_footer"] .= $row[ 'js' . $lang->lnbrm ];
		$db_page_has_js = true;
	}
	if ( $db_page_has_js ) {
		if ( strpos( $GLOBALS["body_footer"], "$.colorbox" ) !== false ) {
			$class_external->load_colorbox = true;
		}
	}
}

function set_css( $row ) {
	$lang = class_language::getInstance();
	$p    = new class_page();

	if ( ! is_null( $row[ 'css' . $lang->lnbr ] ) ) {
		$GLOBALS["custom_css"] .= $p->preview_parse( $row[ 'css' . $lang->lnbr ] );
	} elseif ( ! is_null( $row[ 'css' . $lang->lnbrm ] ) ) {
		$GLOBALS["custom_css"] .= $p->preview_parse( $row[ 'css' . $lang->lnbrm ] );
	}
}

function set_layout( $row ) {
	$lang = class_language::getInstance();

	if ( $GLOBALS['show_layout'] ) {
		$GLOBALS['show_layout'] = $row['show_layout'];
	}

	if ( ! is_null( $row[ 'sub_layout' . $lang->lnbr ] ) ) {
		$GLOBALS['sublayout'] = $row[ 'sub_layout' . $lang->lnbr ] . "/";
	} elseif ( ! is_null( $row[ 'sub_layout' . $lang->lnbrm ] ) ) {
		$GLOBALS['sublayout'] = $row[ 'sub_layout' . $lang->lnbrm ] . "/";
	}
}

function set_lang_array( $mixed ) {
	$lang = class_language::getInstance();

	if ( $lang->count_of_languages > 1 ) {
		foreach ( $lang->languages as $lang_key => $lang_array ) {
			if ( is_array( $mixed ) ) {
				$lang->lang_path[ LANG_PATH_PRIORITY_DEFAULT ][ $lang_key ] = $lang_array['subdir'] . $mixed[ 'path' . $lang_array['number'] ];
			} else {
				if ( strcasecmp( $mixed, "index.php" ) === 0 ) {
					$mixed = "";
				}
				$lang->lang_path[ LANG_PATH_PRIORITY_DEFAULT ][ $lang_key ] = $lang_array['subdir'] . $mixed;
			}
		}
	}
}


function check_password_protection( $row ) {
	$lang = class_language::getInstance();
	$p    = new class_page();

	if ( $row['password_protection'] ) {
		$login_text = "";

		if ( ! is_null( $row[ 'login_text' . $lang->lnbr ] ) ) {
			$login_text = $row[ 'login_text' . $lang->lnbr ];
		} elseif ( ! is_null( $row[ 'login_text' . $lang->lnbrm ] ) ) {
			$login_text = $row[ 'login_text' . $lang->lnbrm ];
		}
		$login_text = $p->fix_html_attributes( $login_text );

		if ( ! isset( $_SERVER['PHP_AUTH_USER'] ) ) {
			$_SERVER['PHP_AUTH_USER'] = "";
		}
		if ( ! isset( $_SERVER['PHP_AUTH_PW'] ) ) {
			$_SERVER['PHP_AUTH_PW'] = "";
		}

		if ( $_SERVER['PHP_AUTH_USER'] != $row['username'] || $_SERVER['PHP_AUTH_PW'] != $row['password'] ) {
			header( 'WWW-Authenticate: Basic realm="' . $login_text . '"' );
			header( 'HTTP/1.0 401 Unauthorized' );
			echo NOT_AUTHORIZED;
			unset( $_SERVER['PHP_AUTH_USER'] );
			unset( $_SERVER['PHP_AUTH_PW'] );
			die();
		}
		unset( $_SERVER['PHP_AUTH_USER'] );
		unset( $_SERVER['PHP_AUTH_PW'] );
	}
}