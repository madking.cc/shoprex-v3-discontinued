<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$browser_compatible = $v->is_browser_compatible;
if ( ! isset( $GLOBALS['browser_warning'] ) ) {
	$GLOBALS['browser_warning'] = "";
}

// Display Warning, if Browser is not compatible
if ( ! $browser_compatible && ! isset( $_COOKIE["alert_incompatible"] ) ) {
	$GLOBALS["foot_bottom"] .= '<script>
    $(document).ready(function () {
        $.cookie("alert_incompatible", "1", { expires: 30 });
        alert("' . BROWSER_NOT_COMPATIBLE . '");
     });
</script>
';
	$GLOBALS['browser_warning'] .= BROWSER_NOT_COMPATIBLE;
}