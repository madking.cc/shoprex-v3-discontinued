<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

// On the end of this page is the include of the other setting files

$this_dir_root = realpath( __DIR__ ) . "/";

require_once( $this_dir_root . "set_fixed_constants.php" );
require_once( $this_dir_root . "set_fixed_globals.php" );


/////////////////////////////////////////////////////////////////////////////

define( "PROCESS_SECTIONS", 1 );

function load_ini_file( $path, $process_sections = null ) {
	$ini_array = parse_ini_file( $path, $process_sections );
	if ( $ini_array === false ) {
		$GLOBALS["early_error"][] = array( "type"     => "php",
		                                   "location" => __FILE__ . ":" . __LINE__,
		                                   "text"     => "Can't parse ini file: '" . $path . "'."
		);
	}

	return $ini_array;
}

function set_variables_of_ini_array( $ini_array, $process_sections = false ) {
	if ( $process_sections ) {
		if ( isset( $ini_array["constants"] ) ) {
			foreach ( $ini_array["constants"] as $constant => $value ) {
				define( $constant, $value );
			}
		}
		if ( isset( $ini_array["global_variables"] ) ) {
			foreach ( $ini_array["global_variables"] as $name => $value ) {
				if ( ! is_array( $value ) ) {
					$GLOBALS[ $name ] = $value;
				} else {
					if ( ! isset( $GLOBALS[ $name ] ) ) {
						$GLOBALS[ $name ] = array();
					}
					foreach ( $value as $name2 => $value2 ) {
						$GLOBALS[ $name ][ $name2 ] = $value2;
					}
				}
			}
		}
	} else {
		foreach ( $ini_array as $name => $value ) {
			if ( ! defined( $name ) ) {
				define( $name, $value );
			}
			/*else
				$GLOBALS["early_error"][] = array("type" => "php", "location" => __FILE__ . ":" . __LINE__ , "text" => "Constant already defined: '\" . $name . \"'.");
			*/
		}
	}
}

/////////////////////////////////////////////////////////////////////////////


// settings_debug.ini:
$ini_array = load_ini_file( DIRSETTINGSCUSTOM . "settings_debug.ini" );
if ( isset( $ini_array["DO_DEBUG"] ) ) {
	set_variables_of_ini_array( $ini_array );
} else {

	$GLOBALS["early_error"][] = array( "type"     => "php",
	                                   "location" => __FILE__ . ":" . __LINE__,
	                                   "text"     => "Can't set DO_DEBUG. Set to false."
	);
	if ( ! defined( "DO_DEBUG" ) ) {
		define( "DO_DEBUG", 0 );
	}
}
// settings_debug.ini End

// Activate Debug Settings
if ( DO_DEBUG == true ) {
	error_reporting( E_ALL );
	ini_set( 'track_errors', '1' );
	ini_set( 'display_errors', '1' );
} else {
	error_reporting( E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED );
	ini_set( 'track_errors', '0' );
	ini_set( 'display_errors', '0' );
}

// settings_local.ini:
$ini_array = load_ini_file( DIRSETTINGSCUSTOM . "settings_website.ini", PROCESS_SECTIONS );
if ( $ini_array !== false ) {
	$GLOBALS['date_default_timezone'] = $ini_array["date_default_timezone_set"]["date_default_timezone_set"];
	date_default_timezone_set( $GLOBALS['date_default_timezone'] );
	set_variables_of_ini_array( $ini_array, PROCESS_SECTIONS );
}
// settings_local.ini End

// settings_webserver.ini:
$ini_array = load_ini_file( DIRSETTINGSCUSTOM . "settings_webserver.ini" );
if ( $ini_array !== false ) {
	set_variables_of_ini_array( $ini_array );
}
// settings_webserver.ini End

// settings_dirs.ini:
$ini_array = load_ini_file( DIRSETTINGSCUSTOM . "settings_dirs.ini", PROCESS_SECTIONS );
if ( $ini_array !== false ) {
	set_variables_of_ini_array( $ini_array, PROCESS_SECTIONS );
}
// settings_dirs.ini End

require_once( DIRSETTINGSCUSTOM . "settings_database.php" );

