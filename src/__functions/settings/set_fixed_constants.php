<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

define( "DIRSETTINGSCUSTOM", DIRROOT . "settings/" );
define( "DIRCLASSES", DIRROOT . "__classes/" );
define( "DIRFUNCTIONS", DIRROOT . "__functions/" );
define( "SYSPAGESDIR", DIRROOT . "__system_pages/" );
define( "TERMINATEFILE", DIRROOT . "__include/terminate.php" );
define( "LAYOUTDIR", DIRROOT . "layout/" );

define( "EDITOR_SIMPLE", 0 );
define( "EDITOR_ENHANCED", 1 );
define( "EDITOR_CODE", 2 );
define( "INPUT_TEXT", 10 );
define( "GET_ARRAY", 11 );
define( "UPLOAD_ONLY", 99 );

define( "DONT_MODIFY_TARGET", 1 );
define( "MODIFY_TARGET", 0 );
define( "NO_COLORBOX_CLOSE", 0 );
define( "DO_COLORBOX_CLOSE", 1 );
define( "NOT_NUMERIC", 0 );
define( "NOT_ESCAPED", 0 );
define( "ESCAPED", 1 );
define( "NOT_MASKED", 0 );
define( "MASKED", 1 );
define( "ENCODED", 1 );
define( "NOT_ENCODED", 0 );
define( "NO_BCC", 1 );
define( "BINARY", 1 );
define( "NOT_BINARY", 0 );
define( "PART_SEARCH", 1 );
define( "NO_PART_SEARCH", 0 );
define( "NO_DIRROOT", 0 );
define( "DO_DIRROOT", 1 );
define( "IS_FILE", 1 );
define( "NOT_A_FILE", 1 );
define( "DONT_SKIP_SPECIAL_FILES", 0 );
define( "SKIP_SPECIAL_FILES", 1 );
define( "DO_UNDERLINE", 0 );
define( "NO_UNDERLINE", 1 );
define( "UNTOUCH_SLASHES", 1 );
define( "TOUCH_SLASHES", 0 );
define( "CAST_AMP", 1 );
define( "NO_CAST_AMP", 0 );
define( "HAS_PROTOCOL_PREFIX", 1 );
define( "NO_PROTOCOL_PREFIX", 0 );
define( "MASK_LAST_ANKER", 1 );
define( "NO_MASK_LAST_ANKER", 0 );
define( "IS_PARENT", 1 );
define( "NOT_PARENT", 0 );
define( "NO_PARAMETER", false );
define( "EMPTY_PARAMETER", "" );
define( "NO_WEBROOT", 0 );
define( "DO_WEBROOT", 1 );
define( "CHECK_ADMIN", 1 );
define( "NO_CHECK_ADMIN", 0 );
define( "INSERT_BREAKS", 1 );
define( "NO_BREAKS", 0 );
define( "EMPTY_FREETEXT", "" );
define( "EMPTY_FREE_TEXT", "" );
define( "NO_ADDITONAL_BUTTONS", "" );
define( "DONT_SUBMIT_UNCHECK_VALUE", 0 );
define( "DISPLAY_EMPTY_VALUE", "---" );
define( "NOT_BIG", 0 );
define( "JS_MODE", 1 );
define( "NO_JS_MODE", 0 );
define( "SINGLE_CALL", 1 );
define( "MAIN_INIT", 1 );
define( "STAY_ON_PAGE", 1 );
define( "SAME_PAGE", 1 );

define( "NO_FILES", 1 );
define( "KEEP_RATIO", 1 );
define( "NO_FILE_PREFIX", "" );
define( "LANG_PATH_PRIORITY_DEFAULT", 0 );
define( "LANG_PATH_PRIORITY_MENU_POINT", 1 );
define( "LANG_PATH_PRIORITY_BLOG", 2 );
define( "LANG_PATH_PRIORITY_BLOG_SECTION", 3 );
define( "LANG_PATH_PRIORITY_BLOG_ARTICLE", 4 );
define( "LANG_PATH_PRIORITY_BLOG_ARTICLE_WITH_SECTION", 5 );
define( "LANG_PATH_PRIORITY_NOT_FOUND", 100 );
define( "ANKER_ALWAYS_ON", 0 );
define( "ANKER_ONLY_FOR_MOBILE_PAGES", 1 );
define( "NO_CHECK_FOR_LANGUAGES", 0 );
define( "AUTO_PAGE_SELECT", "" );
define( "REPLACE_SINGLE_QUOTES", 1 );
define( "REPLACE_DOUBLE_QUOTES", 2 );
define( "DEFAULT_ACTION", "" );
define( "CLEAR_PARAMETER", false );
define( "LOG_FOUND", 1 );
define( "NO_LOG_FOUND", 0 );
define( "DO_RATIO", 1 );
define( "NO_RATIO", 0 );
define( "NO_PREFIX", "" );
define( "DIRECT_CALL", 1 );
define( "NO_DIRECT_CALL", 0 );
define( "DO_BACK", 1 );
define( "NO_BACK", 0 );
define( "AUTO_ID", "" );
define( "META_DESCRIPTION_MAX_CHARS", 160 );
define( "META_KEYWORDS_MAX_CHARS", 200 );
define( "GET_UNIX_TIME", 0 );
define( "NO_ANKER", "" );
define( "DONT_REMOVE_DIR_BACKS", 0 );
define( "REMOVE_DIR_BACKS", 1 );
define( "NO_SUBMIT_ID", "" );
define( "AS_IMAGE", 1 );
define( "NOT_AS_IMAGE", 0 );
define( "EMPTY_ALT", "Na" );
define( "AUTO_WIDTH", "" );
define( "AUTO_NAMING", "" );
define( "HISTORY_BACK", "history" );
define( "NO_SELECTION", 1 );
define( "NO_NAME", "" );
define( "NO_ID", "" );
define( "MENU_PREVIEW_MODE", 1 );
define( "DO_UPDATE", 1 );
define( "NO_ENHANCED_EDITOR", 0 );
define( "FORM_INLINE", "style='display: inline;'" );
define( "EMPTY_VAL_IS_NULL", 1 );
define( "DONT_UPDATE", 0 );
define( "CB_IFRAME_WIDTH", "95%" );
define( "CB_IFRAME_HEIGHT", "95%" );
define( "SLAVE_WINDOW_WIDTH", "1000" );
define( "SLAVE_WINDOW_HEIGHT", "900" );
define( "ADMIN_FLAG_X", 22 );
define( "ADMIN_FLAG_Y", 15 );

define( "NO_HINT", "" );
define( "DEFAULT_UPLOADDIR", "" );
define( "ELEMENT_IS_IMAGE", 1 );
define( "ELEMENT_IS_NOT_IMAGE", 0 );
define( "AUTO_RESIZE", 1 );