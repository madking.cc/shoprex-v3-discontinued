<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

//$GLOBALS["time_now"] = date("Y-m-d H:i:s");
$GLOBALS["visitor_ip"]  = htmlspecialchars( $_SERVER['REMOTE_ADDR'], ENT_QUOTES );
$GLOBALS["request_uri"] = htmlentities( $_SERVER['REQUEST_URI'], ENT_QUOTES );
$GLOBALS["useragent"]   = htmlentities( $_SERVER['HTTP_USER_AGENT'], ENT_QUOTES );

if ( ! isset( $GLOBALS["head_bottom"] ) ) {
	$GLOBALS["head_bottom"] = "";
}
if ( ! isset( $GLOBALS["custom_css"] ) ) {
	$GLOBALS["custom_css"] = "";
}
if ( ! isset( $GLOBALS["body_class"] ) ) {
	$GLOBALS["body_class"] = "";
}
if ( ! isset( $GLOBALS["body_header"] ) ) {
	$GLOBALS["body_header"] = "";
}
if ( ! isset( $GLOBALS["body_footer"] ) ) {
	$GLOBALS["body_footer"] = "";
}
if ( ! isset( $GLOBALS["foot_bottom"] ) ) {
	$GLOBALS["foot_bottom"] = "";
}
if ( ! isset( $GLOBALS["content"] ) ) {
	$GLOBALS["content"] = "";
}
if ( ! isset( $GLOBALS['head'] ) || ! is_array( $GLOBALS['head'] ) ) {
	$GLOBALS['head'] = array();
}
if ( ! isset( $GLOBALS['foot'] ) || ! is_array( $GLOBALS['foot'] ) ) {
	$GLOBALS['foot'] = array();
}
if ( ! isset( $GLOBALS['head_top'] ) ) {
	$GLOBALS['head_top'] = "";
}
if ( ! isset( $GLOBALS['show_layout'] ) ) {
	$GLOBALS['show_layout'] = true;
}
if ( ! isset( $GLOBALS['sublayout'] ) ) {
	$GLOBALS['sublayout'] = "";
}
if ( ! isset( $GLOBALS['do_visitor_store'] ) ) {
	$GLOBALS['do_visitor_store'] = true;
}
if ( ! isset( $GLOBALS['load_visitor_code'] ) ) {
	$GLOBALS['load_visitor_code'] = true;
}