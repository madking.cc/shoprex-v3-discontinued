<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


// Frontend Language
$sql                = "SELECT * FROM `" . TP . "language_frontend` WHERE `active` = '1' AND `deleted` = '0'; ";
$result             = $db->query( $sql, __FILE__ . ":" . __LINE__ );
$language_constants = array();
if ( $result->num_rows > 0 ) {
	while ( $row = $result->fetch_assoc() ) {

		$language_constants[ $row['constant'] ] = $row[ 'text' . $lang->lnbr ];
	}
}

foreach ( $language_constants as $language_constants_name => $language_constants_value ) {
	if ( ! defined( strtoupper( $language_constants_name ) ) ) {
		define( strtoupper( $language_constants_name ), $language_constants_value );
	}
}

