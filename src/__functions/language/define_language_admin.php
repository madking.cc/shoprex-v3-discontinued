<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$admin_language = "";
// Admin Not logged in, to determine current Admin Language, to load language constants for user frontend
if ( ! isset( $_SESSION['admin_user'] ) || empty( $_SESSION['admin_user'] ) ) {
	$sql = "SELECT `number` FROM `" . TP . "language_admin_settings` WHERE `active` = '1' AND `language` IS NOT NULL ORDER BY `main` DESC, `pos`, `id` DESC LIMIT 0,1";

	$result = $db->query( $sql, __FILE__ . ":" . __LINE__ );
	if ( $result->num_rows > 0 ) {
		$row = $result->fetch_assoc();
		if ( ! isset( $row['number'] ) || empty( $row['number'] ) ) {
			$admin_language = "";
		} else {
			$admin_language = $row['number'];
		}
	} else {
		$admin_language = '';
	}
} elseif ( isset( $_SESSION['admin_user'] ) && ! empty( $_SESSION['admin_user'] ) ) {
	// Logged in
	$sql = "SELECT `lang_select` FROM `" . TP . "accounts_admin` WHERE `deleted` = '0' AND `name` LIKE BINARY '" . $_SESSION['admin_user'] . "' ORDER BY `id` DESC LIMIT 0,1";

	$result = $db->query( $sql, __FILE__ . ":" . __LINE__ );
	if ( $result->num_rows > 0 ) {
		$row = $result->fetch_assoc();
		if ( ! isset( $row['lang_select'] ) || empty( $row['lang_select'] ) ) {
			$admin_language = "";
		} else {
			$admin_language = $row['lang_select'];
		}
	} else {
		$admin_language = '';
	}
}

define( "ADMIN_LANGUAGE", $admin_language );