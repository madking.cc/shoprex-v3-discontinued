<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

if ( ! isset( $GLOBALS["show_layout"] ) ) {
	$GLOBALS["show_layout"] = true;
}
$subtitle               = "Downloadhelper";
$sublayout              = "download/";
$class_get              = new class_get();
$file                   = $class_get->get( "file" );
$download_error_message = "";

if ( is_null( $file ) ) {
	$log->error( "file-security", __FILE__ . ":" . __LINE__, WEBROOT . "download.php was executed without GET file parameter or file is not in download list." );

	$download_error_message .= "
    " . THE_FILE . " " . NOT_AVAILABLE_THIS_TIME . "<br />
    " . TRY_LATER . "";
} else {

	if ( PAGE_IS_OFFLINE ) {
		$download_error_message .= "
    " . SITE_OFFLINE . "<br />\n";

		$log->event( "file-download-error", __FILE__ . ":" . __LINE__, "$file can't be downloaded in offline mode" );
	} else {

		$sql    = "SELECT `id`,`count` FROM `" . TP . "file_download` WHERE `filename` LIKE BINARY '$file' AND `active` ='1' AND `deleted`='0'";
		$result = $db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows == 0 ) {
			$download_error_message .= "
    " . THE_FILE . " \"" . htmlspecialchars( $file, ENT_QUOTES ) . "\" " . NOT_AVAILABLE . "<br />";

			$log->error( "file-download-error", __FILE__ . ":" . __LINE__, "$file can't be downloaded. Deleted in Database or not active" );
			$error_msg = THE_FILE . " \"" . htmlspecialchars( $file, ENT_QUOTES ) . "\" " . NOT_AVAILABLE;
			$p->mail_admin( __FILE__ . ":" . __LINE__, $error_msg, FILE_DOWNLOAD );
		} else {
			if ( ! is_file( DIRROOT . DOWNLOADDIR . $file ) ) {
				$log->error( "file-download-error", __FILE__ . ":" . __LINE__, DOWNLOADDIR . $file . " can't be downloaded, because file isn't on filesystem" );

				$download_error_message .= "
    " . THE_FILE . " \"" . htmlspecialchars( $file, ENT_QUOTES ) . "\" " . NOT_AVAILABLE_THIS_TIME . "<br />
    " . TRY_LATER . "<br />";

				$error_msg = THE_FILE . " \"" . htmlspecialchars( $file, ENT_QUOTES ) . "\" " . NOT_AVAILABLE_THIS_TIME;
				$p->mail_admin( __FILE__ . ":" . __LINE__, $error_msg, FILE_DOWNLOAD );
			} else {
				$result_download = $p->makeDownload( DOWNLOADDIR . $file );

				if ( ! $result_download ) {
					$download_error_message .= "
    " . THE_FILE . " \"" . htmlspecialchars( $file, ENT_QUOTES ) . "\" " . CANNOT_BE_DOWNLOADED . "<br />
    " . TRY_LATER . "<br />";
					$error_msg = THE_FILE . " \"" . htmlspecialchars( $file, ENT_QUOTES ) . "\" " . CANNOT_BE_DOWNLOADED;
					$p->mail_admin( __FILE__ . ":" . __LINE__, $error_msg, FILE_DOWNLOAD );
				} else {


					if ( $v->no_count === false && $v->is_human ) {

						$row = $result->fetch_assoc();
						$sql = "UPDATE `" . TP . "file_download` SET `count` = (`count` + 1) WHERE `id`='" . $row['id'] . "'";
						$db->query( $sql, __FILE__ . ":" . __LINE__ );

						$ip      = $loc->get_user_ip();
						$referer = $loc->get_referer();

						if ( is_null( $v->country ) ) {
							$search = " AND `country` is NULL";
						} else {
							$search = " AND `country` LIKE '" . $v->country . "'";
						}

						if ( is_null( $referer ) ) {
							$referer_search = " AND `referer` is NULL";
						} else {
							$referer_search = " AND `referer` LIKE BINARY '" . $referer . "'";
						}

						$sql2    = "SELECT `id` FROM `" . TP . "statistics_download` WHERE `ip` LIKE '$ip' AND `to_id` = '" . $row['id'] . "' AND `type` = '" . $v->visitor_type . "' AND `system_id` = '" . $v->system_id . "'" . $search . $referer_search;
						$result2 = $db->query( $sql2, __FILE__ . ":" . __LINE__ );
						if ( $result2->num_rows == 0 ) {
							$db->ins( __FILE__ . ":" . __LINE__, "statistics_download", array(
								"type",
								"system_id",
								"to_id",
								"referer",
								"ip",
								"uri",
								"created",
								"country"
							), array(
								$v->visitor_type,
								$v->system_id,
								$row['id'],
								$referer,
								$ip,
								$loc->get_complete_url(),
								"NOW()",
								$v->country
							) );
							$update_id = $db->get_insert_id();
						} elseif ( $result2->num_rows == 1 ) {
							$row2      = $result2->fetch_assoc();
							$update_id = $row2['id'];

							$sql2 = "UPDATE `" . TP . "statistics_download` SET `count` = (`count` + 1) WHERE `id`='" . $update_id . "'";
							$db->query( $sql2, __FILE__ . ":" . __LINE__ );
						} else {
							if ( $result2->num_rows > 1 ) {
								$log->error( "php-sql", __FILE__ . ":" . __LINE__, "To many rows for result: '" . $result2->num_rows . "', sql: '$sql2'" );
							}
						}

						if ( is_null( $v->country ) ) {
							$country = "k.A.";
						} else {
							$class_country = new class_country();
							$country       = $class_country->get_country_name( $v->country );
						}

						$log->event( "file-download", __FILE__ . ":" . __LINE__, "$file downloaded " . ( $row['count'] + 1 ) . ". times. Country: '" . $country . "', IP: '$ip'" );
						$p->mail_admin( __FILE__ . ":" . __LINE__, "New $file download counter: " . ( $row['count'] + 1 ) . ".<br />", FILE_DOWNLOAD );
					}

					require_once( DIRROOT . "__include/terminate.php" );
					//die();
				}
			}
		}
	}
}

if ( ! empty( $download_error_message ) ) {
	new class_throw_error( $download_error_message );
}