<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or define( "SECURITY_CHECK", "OK" );

$this_dir_root = realpath( __DIR__ ) . "/";

if ( ! defined( "SITE_ERROR_MESSAGE_HEADER" ) ) {
	$site_error_message_header = "Error on Page. Administrator contacted. Try again later, please.";
} else {
	$site_error_message_header = SITE_ERROR_MESSAGE_HEADER;
}

if ( ! defined( "DETAILS_TBL" ) ) {
	$details_text = "Details: ";
} else {
	$details_text = DETAILS_TBL . " ";
}

if ( ! defined( "PAGE_NOT_FOUND_TBL" ) ) {
	$notfound_text = "Page not found: ";
} else {
	$notfound_text = PAGE_NOT_FOUND_TBL . " ";
}

$text = "<p class=\"text\">?Syntax Error</p><p class=\"text\">$site_error_message_header</p>";

if ( defined( 'DO_DEBUG' ) && DO_DEBUG ) {
	if ( isset( $error_message ) ) {
		$text .= "<p class=\"text\">" . $details_text . $error_message . "</p>\n";
	}
}

if ( ! isset( $GLOBALS['maintenance_style'] ) || empty( $GLOBALS['maintenance_style'] ) ) {
	require_once( $this_dir_root . "../settings/load_settings_local.php" );
	if ( ! isset( $GLOBALS['maintenance_style'] ) || empty( $GLOBALS['maintenance_style'] ) ) {
		$GLOBALS['maintenance_style'] = "default/";
	}
}

if ( isset( $loc->not_found ) ) {
	$text .= "<p class=\"text\">" . $notfound_text . $loc->escape( $loc->not_found ) . "</p>\n";
}

if ( isset( $loc->web_root ) ) {
	$admindir = "";

	if ( isset( $GLOBALS['is_admin'] ) && $GLOBALS['is_admin'] ) {
		$admindir = ADMINDIR;
	}

	$text .= "<p class=\"text\"><a href='/" . $loc->web_root . $admindir . "'>Klick here to load Startpage.</a></p>\n";
}
require_once( $this_dir_root . "styles/" . $GLOBALS['maintenance_style'] . "header.php" );
require_once( $this_dir_root . "styles/" . $GLOBALS['maintenance_style'] . "footer.php" );

$content = $header . $text . $footer;

echo $content;

require_once( TERMINATEFILE );