<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$server_software  = $_SERVER['SERVER_SOFTWARE'];
$server_signature = $_SERVER['SERVER_SIGNATURE'];

$header = "<html>
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<title>Error</title>
</head>
<style>
*
{
    margin: 0;
    padding: 0;
    color: #a5a5ff;

}
html, body
{
    background-color: #a5a5ff;
}
#content-box
{
    margin: 0 auto;
	width: 90%;
	min-width: 90%;
	height: 80%;
	min-height: 80%;
	margin-top: 5%;
	background-color: #4242e7;
}
.text-header, .text
{
    text-transform: uppercase;
	font-size: 26px;
}
.text-header
{
    text-align: center;
}
p
{
    line-height: 32px;
	font-family: \"Courier New\", Courier, monospace;
	font-weight: bold;
}
#cursor
{
    width: 26px;
	height: 26px;
	background-color: #a5a5ff;
}
a, a:hover, a:focus, a:visited
{
    text-decoration: none;
}
</style>
<script>

var cursorStatus = 0;
var cursorBlinkTimeSeconds = 1;
var cursorID = \"cursor\";

setInterval(cursorBlink, cursorBlinkTimeSeconds * 1000);

function cursorBlink()
{
    if(cursorStatus == 0)
    {
        document.getElementById(cursorID).style.display=\"none\";
        cursorStatus = 1;
    }
    else
    {
        document.getElementById(cursorID).style.display=\"block\";
        cursorStatus = 0;
    }

}

</script>
<body>
<div id=\"content-box\">
<p class=\"text-header\">**** SHOPREX CMS ****</p>
<p class=\"text-header\">" . $server_software . "</p>";