<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

// DB Account (for admin and frontend) needs permission to create and drop

if ( defined( 'PREVIEW_MODE' ) && defined( 'PREVIEW_MODE_RESET_DB' ) && PREVIEW_MODE && PREVIEW_MODE_RESET_DB ) {

	$sql    = "SELECT * FROM `" . TP . "preview_reset` ORDER BY `last_reset` DESC LIMIT 1;";
	$result = $db->query( $sql, __FILE__ . ":" . __LINE__ );
	if ( $result->num_rows == 0 ) {

		$orig_tbl = "orig_";

		$db_array   = array();
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "accounts_admin` LIKE `" . TP . "accounts_admin`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "blog_content` LIKE `" . TP . "blog_content`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "blog_link` LIKE `" . TP . "blog_link`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "blog_section` LIKE `" . TP . "blog_section`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "carousel` LIKE `" . TP . "carousel`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "carousel_content` LIKE `" . TP . "carousel_content`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "file_download` LIKE `" . TP . "file_download`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "language_admin` LIKE `" . TP . "language_admin`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "language_admin_settings` LIKE `" . TP . "language_admin_settings`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "language_frontend` LIKE `" . TP . "language_frontend`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "language_settings` LIKE `" . TP . "language_settings`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "menus` LIKE `" . TP . "menus`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "news` LIKE `" . TP . "news`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "news_content` LIKE `" . TP . "news_content`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "pages` LIKE `" . TP . "pages`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "scripts` LIKE `" . TP . "scripts`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "settings` LIKE `" . TP . "settings`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "settings_by_language` LIKE `" . TP . "settings_by_language`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "text` LIKE `" . TP . "text`;";
		$db_array[] = "CREATE TABLE IF NOT EXISTS `" . TP . $orig_tbl . "text_single` LIKE `" . TP . "text_single`;";

		foreach ( $db_array as $command ) {
			$db->query( $command, __FILE__ . ":" . __LINE__ );
		}

		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "accounts_admin`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "blog_content`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "blog_link`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "blog_section`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "carousel`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "carousel_content`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "file_download`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "language_frontend`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "language_settings`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "language_admin`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "language_admin_settings`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "menus`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "news`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "news_content`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "pages`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "scripts`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "settings`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "settings_by_language`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "text`", __FILE__ . ":" . __LINE__ );
		$db->query( "TRUNCATE TABLE `" . TP . $orig_tbl . "text_single`", __FILE__ . ":" . __LINE__ );

		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "accounts_admin` SELECT * FROM `" . TP . "accounts_admin`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "blog_content` SELECT * FROM `" . TP . "blog_content`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "blog_link` SELECT * FROM `" . TP . "blog_link`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "blog_section` SELECT * FROM `" . TP . "blog_section`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "carousel` SELECT * FROM `" . TP . "carousel`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "carousel_content` SELECT * FROM `" . TP . "carousel_content`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "file_download` SELECT * FROM `" . TP . "file_download`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "language_frontend` SELECT * FROM `" . TP . "language_frontend`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "language_settings` SELECT * FROM `" . TP . "language_settings`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "language_admin` SELECT * FROM `" . TP . "language_admin`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "language_admin_settings` SELECT * FROM `" . TP . "language_admin_settings`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "menus` SELECT * FROM `" . TP . "menus`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "news` SELECT * FROM `" . TP . "news`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "news_content` SELECT * FROM `" . TP . "news_content`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "pages` SELECT * FROM `" . TP . "pages`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "scripts` SELECT * FROM `" . TP . "scripts`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "settings` SELECT * FROM `" . TP . "settings`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "settings_by_language` SELECT * FROM `" . TP . "settings_by_language`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "text` SELECT * FROM `" . TP . "text`", __FILE__ . ":" . __LINE__ );
		$db->query( "INSERT DELAYED INTO `" . TP . $orig_tbl . "text_single` SELECT * FROM `" . TP . "text_single`", __FILE__ . ":" . __LINE__ );

		define( "PREVIEW_TIME_LEFT", PREVIEW_MODE_RESET_DB_TIME );

		$time_now = date( "Y-m-d H:i:s" );
		$db->ins( __FILE__ . ":" . __LINE__, "preview_reset", array( "last_reset", "ip" ), array(
			$time_now,
			$loc->get_user_ip()
		) );

	} else {

		$result    = $result->fetch_assoc();
		$last_time = $result['last_reset'];
		$last_time = strtotime( $last_time );
		$time_now  = time();

		$time_difference = $time_now - $last_time;
		$time_difference = round( $time_difference / 60 );
		$time_left       = (int) ( PREVIEW_MODE_RESET_DB_TIME - $time_difference );


		if ( $time_left <= 0 ) {

			define( "PREVIEW_TIME_LEFT", PREVIEW_MODE_RESET_DB_TIME );

			$time_now = date( "Y-m-d H:i:s" );
			$db->ins( __FILE__ . ":" . __LINE__, "preview_reset", array( "last_reset", "ip" ), array(
				$time_now,
				$loc->get_user_ip()
			) );

			$db->query( "TRUNCATE TABLE `" . TP . "accounts_admin`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "blog_content`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "blog_link`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "blog_section`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "carousel`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "carousel_content`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "file_download`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "language_frontend`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "language_settings`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "language_admin`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "language_admin_settings`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "menus`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "news`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "news_content`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "pages`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "scripts`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "settings`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "settings_by_language`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "text`", __FILE__ . ":" . __LINE__ );
			$db->query( "TRUNCATE TABLE `" . TP . "text_single`", __FILE__ . ":" . __LINE__ );

			$orig_tbl = "orig_";

			$db->query( "INSERT DELAYED INTO `" . TP . "accounts_admin` SELECT * FROM `" . TP . $orig_tbl . "accounts_admin`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "blog_content` SELECT * FROM `" . TP . $orig_tbl . "blog_content`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "blog_link` SELECT * FROM `" . TP . $orig_tbl . "blog_link`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "blog_section` SELECT * FROM `" . TP . $orig_tbl . "blog_section`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "carousel` SELECT * FROM `" . TP . $orig_tbl . "carousel`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "carousel_content` SELECT * FROM `" . TP . $orig_tbl . "carousel_content`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "file_download` SELECT * FROM `" . TP . $orig_tbl . "file_download`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "language_frontend` SELECT * FROM `" . TP . $orig_tbl . "language_frontend`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "language_settings` SELECT * FROM `" . TP . $orig_tbl . "language_settings`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "language_admin` SELECT * FROM `" . TP . $orig_tbl . "language_admin`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "language_admin_settings` SELECT * FROM `" . TP . $orig_tbl . "language_admin_settings`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "menus` SELECT * FROM `" . TP . $orig_tbl . "menus`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "news` SELECT * FROM `" . TP . $orig_tbl . "news`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "news_content` SELECT * FROM `" . TP . $orig_tbl . "news_content`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "pages` SELECT * FROM `" . TP . $orig_tbl . "pages`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "scripts` SELECT * FROM `" . TP . $orig_tbl . "scripts`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "settings` SELECT * FROM `" . TP . $orig_tbl . "settings`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "settings_by_language` SELECT * FROM `" . TP . $orig_tbl . "settings_by_language`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "text` SELECT * FROM `" . TP . $orig_tbl . "text`", __FILE__ . ":" . __LINE__ );
			$db->query( "INSERT DELAYED INTO `" . TP . "text_single` SELECT * FROM `" . TP . $orig_tbl . "text_single`", __FILE__ . ":" . __LINE__ );
		} else {
			define( "PREVIEW_TIME_LEFT", $time_left );
		}
	}
}
