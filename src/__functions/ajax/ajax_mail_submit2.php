<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$class_get = new class_get();

$name            = $class_get->get( "name", "" );
$email           = $class_get->get( "email", "" );
$subject         = $class_get->get( "subject", "" );
$text            = $class_get->get( "text", "", false );
$spam_protection = $class_get->get( "spam", "" );

if ( ! empty( $spam_protection ) && is_numeric( $spam_protection ) && ( $spam_protection > 1 && $spam_protection < 21 ) ) {
	$class_mail = new class_mail();

	if ( empty( $subject ) ) {
		$subject = "No subject given";
	}
	$subject = $loc->server_name . ": " . $subject;

	if ( empty( $text ) ) {
		$text = "No message";
	}
	if ( empty( $name ) ) {
		$name = "No name";
	}
	if ( empty( $email ) ) {
		$email = "No email";
	}

	$text = $class_mail->format_text( $text );


	$text = "Name:<br />$name<br /><br />
Email:<br />$email<br /><br />
Text:<br />$text<br />
<br />
Spam answer: " . $spam_protection;

	$result = $class_mail->send_short( __FILE__ . ":" . __LINE__, ADMIN_MAIL, "", $text, $subject, "noresponse@mail.de", $name, false );

	if ( $result ) {
		$content = "success";
	} else {
		$log->error( "mail", __FILE__ . ":" . __LINE__, "Mail error: " . $spam_protection . ". Name: " . $name . ", Email: " . $email . ", Subject: " . $subject . ", Message: " . $text );
		$content = "failure";
	}
} else {
	if ( empty( $spam_protection ) ) {
		$spam_protection = "!empty!";
	}
	$log->notice( "spam", __FILE__ . ":" . __LINE__, "Spam content. Answer to question: '" . $spam_protection . "', Name: '" . $name . "', Email: '" . $email . "', Subject: '" . $subject . "', Message: '" . $text . "'" );
	$content = "failure";
}