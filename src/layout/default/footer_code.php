<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;
$class_get    = new class_get();
$get_no_count = intval( $class_get->get( "no_count", 0 ) );
$ajax_info_js = "<script> var lang = { ajax_lang : \"" . $lang->lnbr . "\" }; var referer = { ajax_referer : \"" . $loc->get_referer() . "\" }; var page = { ajax_page : \"" . $v->get_page() . "\" }; var no_count = { ajax_no_count : \"" . $get_no_count . "\" }; </script>";

if ( ! isset( $footer_code ) ) {
	$footer_code = "";
}

if ( isset( $GLOBALS['foot'] ) && ! empty( $GLOBALS['foot'] ) ) {
	foreach ( $GLOBALS['foot'] as $value ) {
		$footer_code .= $value;
	}
}

$footer_code .= $GLOBALS["foot_bottom"];

// case, needed for admin preview to disable code
if ( ! isset( $do_userjs ) ) {
	$do_userjs = true;
}
if ( $do_userjs ) {
	if ( is_file( DIRROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "js/js_by_language.php" ) && defined( "LOAD_JS_LANGUAGE_FILES" ) && LOAD_JS_LANGUAGE_FILES ) {
		$footer_code .= "    <script src=\"/" . WEBROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "js/js_by_language.php?ldir=" . $lang->ldir . "\"></script>\n";
	}
	if ( is_file( DIRROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "js/js_visitor.php" ) && isset( $GLOBALS['load_visitor_code'] ) && $GLOBALS['load_visitor_code'] ) {
		$footer_code .= $ajax_info_js . "    <script src=\"/" . WEBROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "js/js_visitor.php?ldir=" . $lang->ldir . "\"></script>\n";
	}
	if ( is_file( DIRROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "js/user.js" ) ) {
		$footer_code .= "    <script src=\"/" . $loc->web_root . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "js/user.js\"></script>\n";
	}
	if ( is_file( DIRROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . $GLOBALS['sublayout'] . "js/user.js" ) ) {
		$footer_code .= "    <script src=\"/" . $loc->web_root . $GLOBALS["layout_d"] . $GLOBALS['theme'] . $GLOBALS['sublayout'] . "js/user.js\"></script>\n";
	}
}

$footer_code = "\n" . $footer_code . $GLOBALS["body_footer"] . "
    </body>
</html>";