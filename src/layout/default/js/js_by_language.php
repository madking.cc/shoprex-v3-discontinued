<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or define( "SECURITY_CHECK", "OK" );
define( "DO_DEBUG", 0 );
header( "content-type: application/x-javascript" );

$GLOBALS['fixed_lang_path'] = $_GET["ldir"];
$GLOBALS['fixed_lang_path'] = addslashes( $GLOBALS['fixed_lang_path'] );
$GLOBALS['fixed_lang_path'] = htmlspecialchars( $GLOBALS['fixed_lang_path'], ENT_QUOTES );
$GLOBALS['fixed_lang_path'] = str_replace( " ", "", $GLOBALS['fixed_lang_path'] );

$dir_root = realpath( __DIR__ ) . "/../../../";
defined( 'DIRROOT' ) or define( "DIRROOT", $dir_root );
$GLOBALS['do_visitor_store'] = false;
require_once( $dir_root . "__include/init.php" );

?>
	// User defined:

	$(function(){


	});
<?php
require_once( $dir_root . "__include/terminate.php" );
?>