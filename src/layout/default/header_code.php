<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$this_dir_root = realpath( __DIR__ ) . "/";
if ( ! isset( $GLOBALS["show_layout"] ) ) {
	$GLOBALS["show_layout"] = true;
}
if ( ! isset( $header_code ) ) {
	$header_code = "";
}
if ( ! isset( $title ) ) {
	$title = $GLOBALS['page_title'];
}
if ( ! isset( $subtitle ) ) {
	$subtitle = $GLOBALS['subtitle'];
}
if ( ! isset( $subtitle ) || empty( $subtitle ) || ! isset( $title ) || empty( $title ) ) {
	$separator = "";
}
if ( ! isset( $separator ) ) {
	$separator = "";
}
if ( isset( $GLOBALS['description'] ) && ! empty( $GLOBALS['description'] ) ) {
	$meta_description = $p->fix_html_attributes( $GLOBALS['description'] );
} elseif ( defined( "DESCRIPTION" . $lang->lnbr ) ) {
	$meta_description = $p->fix_html_attributes( constant( "DESCRIPTION" . $lang->lnbr ) );
} else {
	$meta_description = "";
}
if ( isset( $GLOBALS['keywords'] ) && ! empty( $GLOBALS['keywords'] ) ) {
	$meta_keywords = $p->fix_html_attributes( $GLOBALS['keywords'] );
} elseif ( defined( "KEYWORDS" . $lang->lnbr ) ) {
	$meta_keywords = $p->fix_html_attributes( constant( "KEYWORDS" . $lang->lnbr ) );
} else {
	$meta_keywords = "";
}
if ( ! is_null( $loc->parameter_line_casted ) && ! empty( $loc->parameter_line_casted ) ) {
	$parameter_suffix = "?";
} else {
	$parameter_suffix = "";
}
if ( ! isset( $hreflang_content ) ) {
	$hreflang_content = "";
}


$header_code .= "<!DOCTYPE html>
<html lang=\"" . $lang->lhcd . "\">
    <head>
	<meta charset=\"utf-8\">
	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
    <link rel=\"alternate\" hreflang=\"" . $lang->lhreflang . "\" href=\"" . $loc->httpauto_web_root . $lang->ldir . $loc->current . $parameter_suffix . $loc->parameter_line_casted . "\" >
    $hreflang_content
" . $GLOBALS['head_top'] . "
    <title>" . $subtitle . $separator . $title . "</title>

    <meta name=\"description\"
        content=\"" . $meta_description . "\" />
    <meta name=\"keywords\"
        content=\"" . $meta_keywords . "\" />
    <meta name=\"robots\"
        content=\"index, follow\" />\n";

if ( is_file( $this_dir_root . "thumbnail.png" ) ) {
	$header_code .= "    <link rel=\"image_src\"
        href=\"" . $loc->httpauto_web_root . $GLOBALS['layout_d'] . $GLOBALS['theme'] . "thumbnail.png\" />\n";
}
if ( isset( $GLOBALS['head'] ) && ! empty( $GLOBALS['head'] ) ) {
	foreach ( $GLOBALS['head'] as $value ) {
		$header_code .= $value;
	}
}

$header_code .= $GLOBALS["head_bottom"];

if ( $GLOBALS["show_layout"] ) {
	if ( is_file( DIRROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "screen.css" ) ) {
		$header_code .= "    <link rel=\"stylesheet\"
          href=\"/" . $loc->web_root . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "screen.css\" media=\"all\" />\n";
	}
	if ( is_file( DIRROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . $GLOBALS['sublayout'] . "screen.css" ) ) {
		$header_code .= "    <link rel=\"stylesheet\"
          href=\"/" . $loc->web_root . $GLOBALS["layout_d"] . $GLOBALS['theme'] . $GLOBALS['sublayout'] . "screen.css\" media=\"all\" />\n";
	}
	if ( is_file( DIRROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "screen_by_language.php" ) && defined( "LOAD_CSS_LANGUAGE_FILES" ) && LOAD_CSS_LANGUAGE_FILES ) {
		$header_code .= "    <link rel=\"stylesheet\"
          href=\"/" . $loc->web_root . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "screen_by_language.php?ldir=" . $lang->ldir . "\" media=\"all\" />\n";
	}
	if ( is_file( DIRROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "print.css" ) ) {
		$header_code .= "    <link rel=\"stylesheet\"
          href=\"/" . $loc->web_root . $GLOBALS["layout_d"] . $GLOBALS['theme'] . "print.css\" media=\"print\" />\n";
	}
	if ( is_file( DIRROOT . $GLOBALS["layout_d"] . $GLOBALS['theme'] . $GLOBALS['sublayout'] . "print.css" ) ) {
		$header_code .= "    <link rel=\"stylesheet\"
          href=\"/" . $loc->web_root . $GLOBALS["layout_d"] . $GLOBALS['theme'] . $GLOBALS['sublayout'] . "print.css\" media=\"print\" />\n";
	}
	if ( is_file( $this_dir_root . "favicon.ico" ) ) {
		$header_code .= "    <link rel=\"shortcut icon\"
          href=\"/" . $loc->web_root . $GLOBALS['layout_d'] . $GLOBALS['theme'] . "favicon.ico\" type=\"image/x-icon\" />\n";
	}
}

$header_code .= $GLOBALS["custom_css"] . "    </head>
    <body class=\"" . $GLOBALS["body_class"] . "\" style=''>\n";

if ( defined( 'PREVIEW_MODE' ) && PREVIEW_MODE ) {
	$header_code .= "\n<div id='show-preview-mode'><span class='first-info'>" . PREVIEW_VERSION . "</span>";
	if ( defined( 'PREVIEW_MODE_RESET_DB' ) && PREVIEW_MODE_RESET_DB ) {
		$header_code .= "<span class='second-info'>" . TIME_TILL_RESET . " " . PREVIEW_TIME_LEFT . " ";
		if ( PREVIEW_TIME_LEFT != 1 ) {
			$header_code .= MINUTES;
		} else {
			$header_code .= MINUTE;
		}
		$header_code .= "</span>";
	}
	$header_code .= "</div>\n";
}
$header_code .= $GLOBALS["body_header"];