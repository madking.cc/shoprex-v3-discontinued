<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$this_dir_root = realpath( __DIR__ ) . "/";
if ( ! isset( $header_code ) ) {
	$header_code = "";
}
if ( ! isset( $admin_title ) ) {
	$admin_title = $GLOBALS['page_title'];
}
if ( ! isset( $admin_subtitle ) ) {
	if ( ! isset( $GLOBALS['admin_subtitle'] ) ) {
		$admin_subtitle = $GLOBALS['subtitle'];
	} else {
		$admin_subtitle = $GLOBALS['admin_subtitle'];
	}
}

if ( ! isset( $admin_subtitle ) || empty( $admin_subtitle ) || ! isset( $admin_title ) || empty( $admin_title ) ) {
	$separator = "";
} else {
	$separator = " - ";
}
if ( ! isset( $admin_body_class ) ) {
	$admin_body_class = "";
}

$header_code .= "<!DOCTYPE html>
<html lang=\"" . $lang->lhcd . "\">
    <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
" . $GLOBALS['head_top'] . "
    <title>" . $admin_subtitle . $separator . $admin_title . "</title>

    <meta name=\"robots\"
        content=\"noindex, nofollow\" />\n";

if ( is_file( $this_dir_root . "thumbnail.png" ) ) {
	$header_code .= "    <link rel=\"image_src\"
        href=\"" . $loc->httpauto_web_root . ADMINDIR . "layout/thumbnail.png\" />\n";
}
if ( isset( $GLOBALS['head'] ) && ! empty( $GLOBALS['head'] ) ) {
	foreach ( $GLOBALS['head'] as $value ) {
		$header_code .= $value;
	}
}

$header_code .= $GLOBALS["head_bottom"];

$header_code .= "    <link rel=\"stylesheet\"
      href=\"/" . $loc->web_root . ADMINDIR . "layout/screen.css\" media=\"all\" />\n";

if ( ( isset( $GLOBALS['no_menu'] ) && $GLOBALS['no_menu'] ) ) {
	$header_code .= "    <link rel=\"stylesheet\"
          href=\"/" . $loc->web_root . ADMINDIR . "layout/screen_adjust.css\" media=\"all\" />\n";
}

$header_code .= "    <link rel=\"stylesheet\"
      href=\"/" . $loc->web_root . ADMINDIR . "layout/print.css\" media=\"print\" />\n";


$header_code .= "    <link rel=\"shortcut icon\"
      href=\"/" . $loc->web_root . ADMINDIR . "layout/favicon.ico\" type=\"image/x-icon\" />\n";


$header_code .= $GLOBALS["custom_css"] . "    </head>

    <body class=\"" . $admin_body_class . "\" style=''>
    <div id=\"please_wait\" style=\"position: absolute; top: 0; left: 0; height: 100%; width: 100%; padding-top: 25%; text-align: center; background-color: #ffffff; color: #000000;\">" . AL_PLEASE_WAIT . "</div>
    <div class=\"display_content\" style=\"display: none;\">\n";
$header_code .= $GLOBALS["body_header"];

if ( ! isset( $GLOBALS['no_menu'] ) || ( isset( $GLOBALS['no_menu'] ) && ! $GLOBALS['no_menu'] ) ) {
	require_once( $this_dir_root . "menu.php" );
}

$header_code .= "<div class='content_wrapper'>\n";
