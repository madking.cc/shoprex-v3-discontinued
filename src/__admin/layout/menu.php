<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$version_file = file( DIRROOT . "VERSION.TXT", FILE_SKIP_EMPTY_LINES );
if ( $version_file == false || sizeof( $version_file ) == 0 ) {
	$version_long  = "<p>" . AL_VERSION . ": " . AL_UNKNOWN . "</p>";
	$version_short = "";
} else {
	$version       = $version_file[0];
	$version_long  = "<p>" . AL_VERSION . ": $version</p>";
	$version_short = "v" . $version;
}

$sql              = "SELECT * FROM `" . TP . "language_admin_settings` WHERE `active` = '1' ORDER BY `pos`, `main` DESC";
$result           = $db->query( $sql, __FILE__ . ":" . __LINE__ );
$flag_array_admin = array();
$i                = 0;
while ( $row = $result->fetch_assoc() ) {
	$flag_array_admin[ $i ++ ] = $row;
}

$preview_text = " ";
if ( defined( 'PREVIEW_MODE' ) && PREVIEW_MODE ) {
	$preview_text .= "\n<span class='show-preview-mode'>" . AL_PREVIEW_VERSION . "</span>";

	if ( defined( 'PREVIEW_MODE_RESET_DB' ) && PREVIEW_MODE_RESET_DB ) {

		$preview_text .= "<span class='show-preview-mode'> (" . AL_TIME_TILL_RESET . " " . PREVIEW_TIME_LEFT . " ";
		if ( PREVIEW_TIME_LEFT != 1 ) {
			$preview_text .= AL_MINUTES;
		} else {
			$preview_text .= AL_MINUTE;
		}
		$preview_text .= ")</span>";
	}
}


$header_code .= "


<nav class=\"navbar navbar-default navbar-fixed-top\">
  <div class=\"container-fluid\">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class=\"navbar-header\">
      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">
        <span class=\"sr-only\">Toggle navigation</span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </button>
      <a class=\"navbar-brand\" href=\"http://shoprex.de/\" target=\"_blank\">Shoprex.de $version_short$preview_text</a>
    </div>
<div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">

        <ul id='main_top_menu' class='nav navbar-nav'>
            <li id=\"m_sta\" class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>" . AL_STATISTICS . " <b class='caret'></b></a>
                 <ul class='dropdown-menu'>
                    <li>" . $l->link( AL_PAGE_COUNTER, ADMINDIR . "statistics_page_counter.php" ) . "</li>
                    <li>" . $l->link( AL_CURRENT_VIEWS, ADMINDIR . "statistics_page_latest_access.php" ) . "</li>
                    <li>" . $l->link( AL_PAGEVIEWS, ADMINDIR . "statistics_page_visits.php" ) . "</li>
    				<li>" . $l->link( AL_DOWNLOADS, ADMINDIR . "statistics_downloads.php" ) . "</li>
                    <li>" . $l->link( AL_LINK_TO_SITE, ADMINDIR . "statistics_referer.php" ) . "</li>
                    <li>" . $l->link( AL_NOT_FOUND, ADMINDIR . "statistics_not_found.php" ) . "</li>
                    <li>" . $l->link( AL_DATABASE_STATISTICS_EXPORT, ADMINDIR . "statistics_export.php" ) . "</li>
                </ul>
            </li>
            <li id=\"m_adm\" class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>" . AL_LOG . " <b class='caret'></b></a>
                <ul class='dropdown-menu'>
                    <li>" . $l->link( AL_ERROR_LOG, ADMINDIR . "log_error.php" ) . "</li>
                    <li>" . $l->link( AL_EVENT_LOG, ADMINDIR . "log_event.php" ) . "</li>
                    <li>" . $l->link( AL_NOTICE_LOG, ADMINDIR . "log_notice.php" ) . "</li>
                    <li>" . $l->link( AL_MAIL_LOG, ADMINDIR . "log_mail.php" ) . "</li>";

//   <li>" . $l->link(AL_INCOMPATIBLE, ADMINDIR . "log_not_compatible.php") . "</li>
$header_code .= "                    <li>" . $l->link( AL_PHP_SYSTEM_LOG, ADMINDIR . "log_php.php" ) . "</li>
                </ul>
            </li>\n";
$header_code .= "
            <li id=\"m_abs\" class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>" . AL_CONTENT . " <b class='caret'></b></a>
                <ul class='dropdown-menu'>
                    <li>" . $l->link( AL_MENUS, ADMINDIR . "content_menus.php" ) . "</li>
                    <li>" . $l->link( AL_PAGES, ADMINDIR . "content_pages.php" ) . "</li>
                    <li>" . $l->link( AL_FILE_DOWNLOAD, ADMINDIR . "content_downloads.php" ) . "</li>
                    <li>" . $l->link( AL_ONE_LINERS, ADMINDIR . "content_single_text.php" ) . "</li>
                    <li>" . $l->link( AL_TEXT_BLOCKS, ADMINDIR . "content_block_text.php" ) . "</li>\n";

if ( isset( $GLOBALS["admin_menu_content"] ) && sizeof( $GLOBALS["admin_menu_content"] ) > 0 ) {
	foreach ( $GLOBALS["admin_menu_content"] as $text => $link ) {
		$header_code .= "<li>" . $l->link( $text, ADMINDIR . $link ) . "</li>\n";
	}
}

if ( defined( 'PREVIEW_MODE' ) && PREVIEW_MODE ) {
	$ghost_code = "";
} else {
	$ghost_code = "?no_count=" . NO_COUNT_CODE;
}


$header_code .= "
                </ul>
            </li>
            <li id=\"m_abs\" class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>" . AL_SETTINGS . " <b class='caret'></b></a>
                <ul class='dropdown-menu'>
                    <li>" . $l->link( AL_PAGE_INFORMATIONS, ADMINDIR . "settings_page_informations.php" ) . "</li>
                    <li>" . $l->link( AL_PAGE_SETTINGS, ADMINDIR . "settings_main.php" ) . "</li>
                    <li>" . $l->link( AL_GLOBAL_JAVASCRIPT, ADMINDIR . "settings_global_scripts.php" ) . "</li>
                    <li>" . $l->link( AL_SITEMAP_XML, ADMINDIR . "settings_sitemap.php" ) . "</li>
                    <li>" . $l->link( AL_SYSTEM_FILES, ADMINDIR . "settings_special_files.php" ) . "</li>
                    <li>" . $l->link( AL_ADMIN_LOGIN_DATA, ADMINDIR . "settings_admin_accounts.php" ) . "</li>
                    <li>" . $l->link( AL_MAINTENANCE, ADMINDIR . "settings_maintenance.php" ) . "</li>
                </ul>
            </li>
            <li id=\"m_adm\" class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>" . AL_LAYOUT . " <b class='caret'></b></a>
                <ul class='dropdown-menu'>
                    <li>" . $l->link( AL_MAIN_LAYOUTS, ADMINDIR . "layout_frontend.php" ) . "</li>
                    <li>" . $l->link( AL_SUB_LAYOUTS, ADMINDIR . "layout_frontend_sub.php" ) . "</li>
                </ul>
            <li id=\"m_adm\" class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>" . AL_LANGUAGE . " <b class='caret'></b></a>
                <ul class='dropdown-menu'>
                    <li>" . $l->link( AL_FRONTEND_LANGUAGE, ADMINDIR . "language_settings_frontend.php" ) . "</li>
                    <li>" . $l->link( AL_LANGUAGES_CONSTANTS, ADMINDIR . "language_constants_frontend.php" ) . "</li>
                    <li>" . $l->link( AL_ADMIN_LANGUAGE_SETTINGS, ADMINDIR . "language_settings_admin.php" ) . "</li>
                    <li>" . $l->link( AL_ADMIN_LANGUAGE_CONSTANTS, ADMINDIR . "language_constants_admin.php" ) . "</li>
                </ul>
            </li>

            <li id=\"m_inf\" class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'>" . AL_INFO . " <b class='caret'></b></a>
                <div class='dropdown-menu'>
                    <div class='wrapper-menu-content'>
                    <div class='text-center'><p class='color1 underline'>" . AL_INFORMATIONS . "</p></div>
                    <p>" . AL_AUTHOR . ": Andreas Rex<br />
                    (" . AL_PRIVATE_CITIZEN . ")<br />
                    Wissmannstr. 80<br />
                    DE-90411 Nürnberg<br />
                    " . $l->link( "post@shoprex.de", "mailto:post@shoprex.de" ) . "</p>
                    $version_long
                    <p>" . AL_HELP_UNDER . ": " . $l->link( "shoprex.de", "http://shoprex.de", "", "target=\"_blank\"" ) . "</p>
                    <p>" . $l->link( AL_SHOW_LICENSE, ADMINDIR . "show_licence.php" ) . "</p>
                    <p>" . $l->link( AL_SHOW_CHANGES, ADMINDIR . "show_changes.php" ) . "</p>
                    <p>" . $l->link( AL_PHP_INFO, ADMINDIR . "php_info.php", "", "target='_blank'" ) . "</p>
                    </div>
                </div>
            </li>
            <li id=\"m_tp\" class=''><a href='" . $loc->httpauto_web_root . $ghost_code . "' target='_blank'>" . AL_TO_PAGE . "</a></li>
            <li id=\"m_abs\" class=''><a href='logout.php'>" . AL_LOGOUT . "</a></li>
            \n";

$header_code .= "        </ul>\n";

if ( ! empty( $flag_array_admin ) ) {
	$header_code .= "
        <ul id='main_top_menu_right' class='nav navbar-nav navbar-right'>\n";
	foreach ( $flag_array_admin as $row ) {
		$parameter     = "";
		$admin_lang_id = "";

		if ( $row['id'] != 1 ) {
			$parameter     = "lang=_" . $row['id'];
			$admin_lang_id = "_" . $row['id'];
		}

		if ( ADMIN_LANGUAGE == $admin_lang_id ) {
			$active_class = "img nr active";
		} else {
			$active_class = "img nr inactive";
		}

		$header_code .= "            <li class='menu-flags'>" . $l->link( $l->img( ADMINDIR . "layout/flags/" . $row['img'], $row['language'], "", $active_class ), ADMINDIR . "language_admin_change.php", $parameter, "title='" . $row['link_title'] . "'" ) . "</li>\n";
	}
	$header_code .= "
        </ul>\n";
}

$header_code .= "            </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>\n";

