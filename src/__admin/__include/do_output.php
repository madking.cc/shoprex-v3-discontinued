<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$class_external                = class_load_external::getInstance();
$class_external->load_response = false;
require_once( DIRINCLUDE . "load_external_code.php" );

if ( ! ( isset( $pure_content ) && $pure_content ) ) {
	require_once( ADMINDIRROOT . "layout/header.php" );
	require_once( ADMINDIRROOT . "layout/footer.php" );
} else {
	$header_code = "";
	$footer_code = "";
}

$content = $header_code . $content . $footer_code;

if ( ! headers_sent() ) {
	header( 'X-UA-Compatible: IE=edge' );
	header( 'Content-type: text/html; charset=utf-8' );
} else {
	$log->notice( "headers", __FILE__ . ":" . __LINE__, "headers already sent!" );
}

echo $content;
$runlevel->set_level( 5 );