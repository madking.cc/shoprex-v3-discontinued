<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;


if ( ! isset( $GLOBALS['theme'] ) ) {
	if ( ! defined( "LAYOUT" ) ) {
		$GLOBALS['theme'] = "default/";
	} else {
		$GLOBALS['theme'] = LAYOUT . "/";
	}
}

$sub  = "";
$page = "";
if ( empty( $loc->uri_clean ) || $loc->uri_clean == "index.php" ) {
	if ( isset( $GLOBALS['start_sub'] ) && isset( $GLOBALS['start_page'] ) ) {
		$sub  = $GLOBALS['start_sub'];
		$page = $GLOBALS['start_page'];
	} else {
		$sub  = "statistics/";
		$page = "page_counter.php";
	}

} elseif ( is_file( ADMINDIRROOT . "pages/" . $loc->uri_clean ) ) {
	require_once( ADMINDIRROOT . "pages/" . $loc->uri_clean );
} else {
	if ( isset( $GLOBALS["admin_include_pages_path"] ) && sizeof( $GLOBALS["admin_include_pages_path"] ) > 0 ) {
		foreach ( $GLOBALS["admin_include_pages_path"] as $path ) {
			if ( is_file( DIRROOT . $path . $loc->uri_clean ) ) {
				require_once( DIRROOT . $path . $loc->uri_clean );
				break;
			}
		}
	}
}

//var_dump($loc->uri_inside);
$login_page = false;

if ( $loc->uri_clean != "pass_reset.php" ) {
	if ( isset( $admin_logged_in ) && ! $admin_logged_in ) {
		if ( isset( $ajax_submit ) && $ajax_submit ) {
			$ajax_not_logged_in = true;
		}
		$GLOBALS['no_menu'] = true;
		require_once( ADMINDIRROOT . "__functions/login.php" );
		$login_page = true;
	}
}
if ( ! $login_page ) {
	//var_dump($page, $sub, ADMINDIRROOT);
	//die();
	$found = false;
	if ( isset( $sub_direct ) && $sub_direct ) {
		if ( isset( $page ) && ! empty( $page ) && is_file( DIRROOT . $sub . $page ) ) {
			$found = true;
			require_once( DIRROOT . $sub . $page );
		}
	} elseif ( isset( $page ) && ! empty( $page ) && is_file( ADMINDIRROOT . "__functions/" . $sub . $page ) ) {
		$found = true;
		require_once( ADMINDIRROOT . "__functions/" . $sub . $page );
	}

	if ( ! $found ) {
		$loc->not_found = $loc->uri_inside;
		require_once( DIRROOT . "__functions/maintenance/maintenance.php" );
	}
}

$runlevel->set_level( 4 );