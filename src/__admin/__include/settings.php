<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['start_sub']                      = "statistics/";
$GLOBALS['start_page']                     = "statistics_page_counter.php";
$GLOBALS['box_width']                      = "95%";
$GLOBALS['box_height']                     = "95%";
$GLOBALS['box_slave_width']                = "1000";
$GLOBALS['box_slave_height']               = "900";
$GLOBALS['media_flags_dir']                = "__media/flags/";
$GLOBALS['flag_x_admin']                   = "22";
$GLOBALS['flag_y_admin']                   = "15";
$GLOBALS['layout_edit_files']              = array(
	"header_code.php",
	"footer_code.php",
	"screen.css",
	"print.css",
	"screen_by_language.php"
);
$GLOBALS['layout_edit_files_advanced']     = array( "init.php", "editor.css" );
$GLOBALS['layout_edit_files_js']           = array( "user.js", "bootstrap.js", "response.js", "js_by_language.php" );
$GLOBALS['layout_img_dir']                 = "img/";
$GLOBALS['layout_sub_edit_files']          = array( "header.php", "footer.php", "screen.css", "print.css" );
$GLOBALS['layout_sub_edit_files_advanced'] = array();
$GLOBALS['layout_sub_edit_files_js']       = array( "user.js" );
$GLOBALS['layout_sub_no_select']           = array( "download", "flags", "img", "js" );

