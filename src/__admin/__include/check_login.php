<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

if ( ! isset( $_SESSION['admin_user'] ) || ! isset( $_SESSION['admin_pass'] ) ) {
	$subtitle        = "Login";
	$admin_logged_in = false;
} else {
	$sql    = "SELECT * FROM `" . TP . "accounts_admin` WHERE `deleted` = '0' AND `name` LIKE BINARY '" . $_SESSION['admin_user'] . "' ORDER BY `id` DESC LIMIT 0,1";
	$result = $db->query( $sql, __FILE__ . ":" . __LINE__ );
	if ( $result->num_rows == 0 || $result->num_rows > 1 ) {
		$log->error( "sql", __FILE__ . ":" . __LINE__, "No admin account found. Num-Rows: '" . $result->num_rows . "', SQL: " . $sql );
	} else {
		$row = $result->fetch_assoc();
		if ( $_SESSION['admin_user'] == $row['name'] && $_SESSION['admin_pass'] == $row['pass'] ) {
			$admin_logged_in = true;
		} else {
			$subtitle        = "Login";
			$admin_logged_in = false;
		}
	}
}