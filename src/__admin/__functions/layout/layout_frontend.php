<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_LAYOUT;

class class_layout_frontend extends class_sys {
	public $content;

	public function __construct( $action = null ) {
		parent::__construct();

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );


		switch ( $action ) {
			case "select_main_layout":
				$this->select_main_layout();
				break;
			case "copy":
				$result = $this->layout_copy();
				if ( ! empty( $result ) ) {
					$this->content .= $result;
					$this->content .= $this->start();
				}
				break;
			case "delete":
				$this->layout_delete();
				break;
			case "edit":
				$this->content .= $this->edit();
				break;
			case "update_file":
				$result = $this->update_file();
				if ( ! empty( $result ) ) {
					$this->content .= $result;
					$this->content .= $this->edit();
				}
				break;
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";


		$content .= "<h3 class='underline'>" . AL_LAYOUT . "</h3>\n";

		$content .= $this->l->display_message_by_session( 'options_saved', AL_SETTINGS_SAVED );
		$content .= $this->l->display_message_by_session( 'layout_deleted', AL_LAYOUT_DELETED );

		$layout_copied = - 1;
		if ( isset( $_SESSION['layout_copied'] ) ) {
			$layout_copied = $_SESSION['layout_copied'];
			unset( $_SESSION['layout_copied'] );
		}
		if ( $layout_copied > 0 ) {
			$content .= $this->l->alert_text_dismiss( "success", AL_LAYOUT_COPIED_WITH_X_ELEMENTS_PART01 . " " . $layout_copied . " " . AL_LAYOUT_COPIED_WITH_X_ELEMENTS_PART02 );
		} elseif ( $layout_copied == 0 ) {
			$content .= $this->l->alert_text_dismiss( "success", AL_NO_ELEMENTS_FOR_LAYOUT_COPY );
		}

		$layouts = $this->p->get_dir_content( $GLOBALS['layout_d'] );

		if ( ! $layouts ) {
			$content .= "<p>" . AL_ERROR_CANT_FIND_DIRECTORY . "</p>";
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Error, can't determine directory content in '" . DIRROOT . $GLOBALS['layout_d'] . "'" );
		} else {

			$content .= $this->l->form_admin() . $this->l->hidden( "do", "select_main_layout" ) . $this->l->table() . "
    <tr><td>" . AL_TBL_SELECT_MAIN_LAYOUT . "</td><td>" . $this->l->select( "layout" );
			foreach ( $layouts as $value ) {
				$content .= "<option";
				if ( $value == LAYOUT ) {
					$content .= " selected='selected'";
				}
				$content .= ">$value</option>\n";
			}

			$content .= "</select></td></tr>
    <tr><td></td><td>" . $this->l->submit( AL_SAVE ) . "</td></tr>
    </table></form>
    ";

			$content .= "<h3 class='underline'>" . AL_COPY_LAYOUT . "</h3>\n";

			$content .= $this->l->form_admin() . $this->l->hidden( "do", "copy" ) . $this->l->table() . "
    <tr><td>" . AL_TBL_LAYOUT . "</td><td>" . $this->l->select( "layout_source" );
			foreach ( $layouts as $value ) {
				$content .= "<option";
				if ( $value == LAYOUT ) {
					$content .= " selected='selected'";
				}
				$content .= ">$value</option>\n";
			}

			$content .= "</select></td>
        <td>" . AL_TBL_COPY_TO . "</td>
        <td>" . $this->l->text( "layout_destination" ) . "</td>
        </tr>
    <tr><td></td><td colspan='3'>" . $this->l->button( AL_COPY, "onclick='copy_check(this.form)'" ) . " <span class='error' style='display:none;' id='copy_name_missing_message'>" . AL_ERROR_COPY_NAME_EMPTY . "</span></td></tr>
    </table></form>
    ";
			$GLOBALS['body_footer'] .= "<script>
function copy_check(formID)
{
    if(formID.layout_destination.value == '')
    {
        $(\"#copy_name_missing_message\").fadeIn();
    }
    else
    {
        formID.submit();
    }
}
</script>\n";

			if ( sizeof( $layouts ) > 1 ) {
				$enable_delete = true;
			} else {
				$enable_delete = false;
			}

			$content .= "<h3 class='underline'>" . AL_EDIT_LAYOUT . "</h3>\n";

			$content .= $this->l->table() .
			            "<tr><th>" . AL_TBL_LAYOUT . "</th><th>" . AL_TBL_SIZE . "</th><th>" . AL_TBL_ACTION . "</th></tr>\n";

			$entry_counter = 0;
			foreach ( $layouts as $value ) {
				$content .= "<tr><td>$value</td>";

				$dirsize = $this->p->get_dirsize( $GLOBALS['layout_d'] . $value . "/" );

				$content .= "<td>" . ( number_format( $dirsize, 2, ",", "" ) ) . " " . AL_MEGABYTE . "</td>";

				$content .= "<td>
" . $this->l->form_admin( EMPTY_FREE_TEXT, "layout_frontend_edit.php", "layout=$value" ) . $this->l->hidden( "do", "edit" ) . $this->l->submit( AL_EDIT ) . "</form> ";
				if ( $enable_delete ) {
					$content .= $this->l->form_admin( "id=\"form_admin_delete_content_$entry_counter\"" ) . $this->l->hidden( "do", "delete" ) . $this->l->hidden( "layout_to_delete", $value ) . $this->l->button( AL_DELETE, "id=\"button_delete_content_$entry_counter\"" ) . "</form>\n";
				}
				$content .= "</td></tr>\n";

				if ( $enable_delete ) {
					$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_LAYOUT_PART01 . " \"" . $value . "\" " . AL_DELETE_LAYOUT_PART02, "#button_delete_content_$entry_counter", "form_admin_delete_content_$entry_counter", "confirm_delete_$entry_counter" );
				}

				$entry_counter ++;
			}

			$content .= "</table>\n";
		}

		$this->l->display_preview();

		return $content;
	}

	public function select_main_layout() {


		$elements           = array();
		$elements['layout'] = $this->p->get( "layout" );

		foreach ( $elements as $key => $value ) {
			if ( ! $this->l->get_preview_status() ) {
				$this->db->upd( __FILE__ . ":" . __LINE__, "settings", array( "value" ), array( $value ), "UPPER(`key`) LIKE UPPER('$key')" );
			}
			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Settings updated, key: " . $key . ", value: " . $value );
		}

		$_SESSION['options_saved'] = 1;

		$this->l->reload_js();
	}

	public function layout_copy() {
		$content      = "";
		$copied_items = - 1;

		$layout_source      = $this->p->get( "layout_source" );
		$layout_destination = $this->p->get( "layout_destination", "", NOT_ESCAPED );

		$layout_destination = trim( $layout_destination );
		$layout_destination = $this->translate_special_characters( $layout_destination );
		$layout_destination = $this->db->escape( $layout_destination );
		if ( empty( $layout_destination ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME );

			return $content;
		} elseif ( is_dir( DIRROOT . $GLOBALS['layout_d'] . $layout_destination ) ) {
			$content .= $this->l->alert_text( "danger", AL_LAYOUT_NAME_ALREADY_TAKEN . " " . $layout_destination );

			return $content;
		} elseif ( ! $this->l->get_preview_status() ) {
			$result = $this->p->copy_recursive( $GLOBALS['layout_d'] . $layout_source, $GLOBALS['layout_d'] . $layout_destination, NO_WEBROOT );
			if ( $result === false ) {
				$content .= $this->l->alert_text( "danger", AL_CANT_COPY_LAYOUT );

				return $content;
			}
			$copied_items = $result;
		}

		$_SESSION['layout_copied'] = $copied_items;

		$this->l->reload_js();
	}

	public function layout_delete() {
		$layout_to_delete = $this->p->get( "layout_to_delete" );

		if ( $layout_to_delete == LAYOUT ) {
			$layouts = $this->p->get_dir_content( $GLOBALS['layout_d'] );
			foreach ( $layouts as $existing_layout ) {
				if ( $existing_layout != LAYOUT ) {
					if ( ! $this->l->get_preview_status() ) {
						$this->db->upd( __FILE__ . ":" . __LINE__, "settings", array( "value" ), array( $existing_layout ), "UPPER(`key`) LIKE UPPER('LAYOUT')" );
					}
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Due deletion of layout new layout set to '$existing_layout'" );
					break;
				}
			}
		}

		if ( ! $this->l->get_preview_status() ) {
			$this->p->delete_recursive( $GLOBALS['layout_d'] . $layout_to_delete . "/" );
		}

		$_SESSION['layout_deleted'] = 1;

		$this->l->reload_js();
	}

	public function edit() {
		$content = "";

		$layout_to_edit       = $this->p->get( "layout" );
		$file_select          = $this->p->get( "file_select" );
		$file_advanced_select = $this->p->get( "file_advanced_select" );
		$file_js_select       = $this->p->get( "file_js_select" );

		if ( empty( $file_select ) && empty( $file_advanced_select ) && empty( $file_js_select ) ) {
			$file_select = $GLOBALS['layout_edit_files'][0];
		}

		if ( ! empty( $file_select ) ) {
			$file_load           = $file_select;
			$file_load_parameter = "file_select=" . $file_load;
		} elseif ( ! empty( $file_advanced_select ) ) {
			$file_load           = $file_advanced_select;
			$file_load_parameter = "file_advanced_select=" . $file_load;
		} elseif ( ! empty( $file_js_select ) ) {
			$file_load           = $file_js_select;
			$file_load_parameter = "file_js_select=" . $file_load;
		}

		$input_hidden = $this->l->hidden( "file_select", $file_select ) .
		                $this->l->hidden( "file_advanced_select", $file_advanced_select ) . $this->l->hidden( "file_js_select", $file_js_select );
		$input_do     = $this->l->hidden( "do", "edit" );

		$content .= "<h3 class='underline'>$layout_to_edit " . AL_EDIT_LAYOUT_FILES . "</h3>\n" . $this->l->back_button( AL_BACK, "layout_frontend.php" ) . "<br /><br />\n";

		$content .= $this->l->table() .
		            "<tr>";

		if ( sizeof( $GLOBALS['layout_edit_files'] ) > 0 ) {
			$content .= "<th colspan='1'>" . AL_TBL_LAYOUT_FILES . "</th>";
		}
		if ( sizeof( $GLOBALS['layout_edit_files_advanced'] ) > 0 ) {
			$content .= "<th colspan='1'>" . AL_TBL_LAYOUT_FILES_ADVANCED . "</th>";
		}
		if ( sizeof( $GLOBALS['layout_edit_files_js'] ) > 0 ) {
			$content .= "<th colspan='1'>" . AL_TBL_LAYOUT_FILES_JS . "</th>";
		}

		$content .= "<th colspan='1'>" . AL_TBL_LAYOUT_PICTURES . "</th></tr>
            <tr>\n";

		if ( sizeof( $GLOBALS['layout_edit_files'] ) > 0 ) {
			$content .= $this->l->form( EMPTY_FREE_TEXT, DEFAULT_ACTION, "layout=" . $layout_to_edit ) . "<td>" . $input_do . $this->l->select( "file_select" );
			foreach ( $GLOBALS['layout_edit_files'] as $file ) {
				$content .= "<option value='$file'";
				if ( $file == $file_select ) {
					$content .= " selected='selected'";
				}
				$content .= ">$file</option>\n";
			}
			$content .= "</select> " . $this->l->submit( AL_EDIT ) . "</td></form>\n";
		}
		if ( sizeof( $GLOBALS['layout_edit_files_advanced'] ) > 0 ) {
			$content .= $this->l->form( EMPTY_FREE_TEXT, DEFAULT_ACTION, "layout=" . $layout_to_edit ) . "<td>" . $input_do . $this->l->select( "file_advanced_select" );
			foreach ( $GLOBALS['layout_edit_files_advanced'] as $file ) {
				$content .= "<option value='$file'";
				if ( $file == $file_advanced_select ) {
					$content .= " selected='selected'";
				}
				$content .= ">$file</option>\n";
			}
			$content .= "</select> " . $input_do . $this->l->submit( AL_EDIT ) . "</td></form>\n";
		}
		if ( sizeof( $GLOBALS['layout_edit_files_js'] ) > 0 ) {
			$content .= $this->l->form( EMPTY_FREE_TEXT, DEFAULT_ACTION, "layout=" . $layout_to_edit ) . "<td>" . $input_do . $this->l->select( "file_js_select" );
			foreach ( $GLOBALS['layout_edit_files_js'] as $file ) {
				$content .= "<option value='js/$file'";
				if ( ( "js/" . $file ) == $file_js_select ) {
					$content .= " selected='selected'";
				}
				$content .= ">$file</option>\n";
			}
			$content .= "</select> " . $input_do . $this->l->submit( AL_EDIT ) . "</td></form>\n";
		}
		$content .= "<td>" . $this->l->form() . $this->l->button( AL_PICTURE_LAYOUT_MANAGER, "id='picture_layout_manager'" ) . "</form>
        " . $this->l->form() . $this->l->button( AL_PICTURE_LAYOUT_UPLOAD, "id='picture_layout_upload'" ) . "</form></td>\n";

		$content .= "</tr>\n";

		$content .= "<tr><th colspan='4'>" . AL_TBL_FILE . " /" . $GLOBALS['layout_d'] . $layout_to_edit . "/" . $file_load . "</th></tr>\n";
		$content .= "</tr><tr>" . $this->l->form( EMPTY_FREE_TEXT, DEFAULT_ACTION, "layout=" . $layout_to_edit ) . "<td colspan='7'>\n";

		$file_content = $this->p->get_file_content( $GLOBALS['layout_d'] . $layout_to_edit . "/" . $file_load );

		$content .= $this->l->hidden( "do", "update_file" ) . $input_hidden . $this->l->submit( AL_SAVE ) . " " . $this->l->button( AL_RELOAD, "onclick='reload(this.form)'" ) . "\n";

		$file_type = $this->get_filetype( $file_load );

		$content .= $this->l->media_ta( "new_file_content", $file_content, UPLOADDIR, EDITOR_CODE, "800px", EMPTY_FREETEXT, "ta ta_media", NO_ADDITONAL_BUTTONS, $file_type );
		$content .= "</td></tr><tr><td colspan='4'>" . $this->l->submit( AL_SAVE ) . " " . $this->l->button( AL_RELOAD, "onclick='reload(this.form)'" );

		$content .= "</td></form></tr></table>\n";

		$uploaddir = urldecode( $GLOBALS['layout_d'] . $layout_to_edit . "/img/" );

		$GLOBALS['body_footer'] .= "<script>
    function reload(formID)
    {
        parent.location.href = '" . $this->loc->httpauto_web_admin_root . "layout_frontend_edit.php?layout=" . $layout_to_edit . "&" . $file_load_parameter . "';
    }

$(document).ready(function() {
    $('#picture_layout_manager').click(function() {
        $.colorbox({href:'/" . WEBROOT . ADMINDIR . "upload_manager.php?editor=new_file_content&textarea_type=" . EDITOR_CODE . "&save_delete=0&relative_insert=img/&uploaddir=$uploaddir', iframe:true, width:'" . $GLOBALS['box_width'] . "', height:'" . $GLOBALS['box_height'] . "' });
    });
    $('#picture_layout_upload').click(function() {
        $.colorbox({href:'/" . WEBROOT . ADMINDIR . "upload_picture.php?uploaddir=" . $uploaddir . "&target=new_file_content&textarea_type=" . EDITOR_CODE . "&use_orig_picture=0&keep_original_picture=0', iframe:true, width:'" . $GLOBALS['box_width'] . "', height:'" . $GLOBALS['box_height'] . "' });
    });
});

</script>\n";

		return $content;
	}

	public function update_file() {
		$content = "";

		$layout_to_update     = $this->p->get( "layout" );
		$file_select          = $this->p->get( "file_select" );
		$file_advanced_select = $this->p->get( "file_advanced_select" );
		$file_js_select       = $this->p->get( "file_js_select" );

		$new_file_content = $this->p->get( "new_file_content", false, NOT_ESCAPED );

		if ( ! empty( $file_select ) ) {
			$file_to_update      = $file_select;
			$file_load_parameter = "file_select=" . $file_to_update;
		} elseif ( ! empty( $file_advanced_select ) ) {
			$file_to_update      = $file_advanced_select;
			$file_load_parameter = "file_advanced_select=" . $file_to_update;
		} elseif ( ! empty( $file_js_select ) ) {
			$file_to_update      = $file_js_select;
			$file_load_parameter = "file_js_select=" . $file_to_update;
		} else {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Kein Dateiname zu ermitteln." );
			$content .= AL_MISSING_FILE;

			return $content;
		}

		if ( ! $this->l->get_preview_status() ) {
			$file         = DIRROOT . $GLOBALS['layout_d'] . $layout_to_update . "/" . $file_to_update;
			$file_handler = fopen( $file, "w" );

			if ( $file_handler === false ) {
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Konnte Datei nicht zum schreiben öffnen: '" . $file . "'" );
				$content .= AL_ERROR_WRITE_FILE;

				return $content;
			} else {
				$status = fwrite( $file_handler, $new_file_content );

				if ( $status === false ) {
					fclose( $file_handler );
					$this->log->error( "php", __FILE__ . ":" . __LINE__, "Konnte nicht in Datei schreiben: '" . $file . "'" );
					$content .= AL_ERROR_WRITE_FILE;

					return $content;
				} else {
					fclose( $file_handler );
					$this->log->event( "php", __FILE__ . ":" . __LINE__, "Inhalt in '" . $file . "' aktualisiert." );
					$_SESSION['content_updated'] = 1;
				}
			}
		}

		$GLOBALS['body_footer'] .= "<script>
        parent.location.href = '" . $this->loc->httpauto_web_admin_root . "layout_frontend_edit.php?layout=" . $layout_to_update . "&" . $file_load_parameter . "';
</script>\n";
	}

}

if ( ! isset( $action ) ) {
	$action = null;
}

$class_layout_frontend = new class_layout_frontend( $action );
$content .= $class_layout_frontend->get_content();