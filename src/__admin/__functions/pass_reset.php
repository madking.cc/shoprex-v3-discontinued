<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_PASSWORD_RESET;


class class_pass_reset extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();


		$action = $this->p->get( "do" );

		switch ( $action ) {
			case "update":
				$this->content .= $this->update();
				break;
			default:
				$this->content .= $this->start();
				break;
		}


	}

	public function get_content() {
		return $this->content;
	}

	public function start( $user = "", $error = "" ) {
		$content = "";


		if ( ! empty( $error ) ) {
			$content .= $this->l->alert_text( "danger", $error );
		}

		$id = $this->p->get( "id" );

		$content .= $this->l->form_admin( "onSubmit='return check_submit(this)'" ) . $this->l->hidden( "do", "update" ) . $this->l->hidden( "hash", $id ) . $this->l->table( "style='width:400px;height:200px;margin: 0 auto;margin-top: 10%;border:1px solid #dddddd !important;'" ) . "
        <tr><td colspan='2' align='center'><h4>" . AL_ADMIN_PASSWORT_RESET . "</h4></td></tr>
        <tr>
        <td>" . AL_NEW_PASSWORD . ":</td><td>" . $this->l->password( "password" ) . "</td>
        </tr>
        <tr>
        <td>" . AL_CONFIRM_PASSWORD . ":</td><td>" . $this->l->password( "password_check" ) . "</td>
        </tr>
        <tr>
         <td></td>
         <td>" . $this->l->submit( AL_SET_PASSWORD ) . "</td>
         </tr>
         </table></form>\n";

		$GLOBALS['body_footer'] .= "<script>
        function check_submit(formID)
        {
            if(formID.password.value != '' || formID.password_check.value != '')
            {
                if(formID.password.value != formID.password_check.value)
                {
                    " . $this->l->box_alert( "<H3 class='error underline'>" . AL_ERROR . "</H3><p>" . AL_PASSWORD_NOT_THE_SAME . "!</p>" ) . "
                    return false;
                }
            }
            return true;
        }
    </script>
    ";

		$this->l->display_preview();

		return $content;
	}

	public function update() {
		$content = "";

		if ( ! $this->l->get_preview_status() ) {

			$hash = $this->p->get( "hash" );

			$sql    = "SELECT * FROM `" . TP . "password_reset_admin` WHERE `hash` LIKE BINARY '$hash'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 1 ) {
				$this->log->error( "form", __FILE__ . ":" . __LINE__, "No hash found for password reset" );
				$GLOBALS['body_footer'] .= "<script>
            window.location.href = '" . $this->loc->http_domain . WEBROOT . ADMINDIR . "index.php';
        </script>
        ";

				return $content;
			}
			$row          = $result->fetch_assoc();
			$time_hash    = strtotime( $row['time_to_life'] );
			$time_now     = time();
			$time_to_life = 24 * 60 * 60; // 1 Tag

			if ( $time_hash > ( $time_now + $time_to_life ) ) {
				$this->log->error( "form", __FILE__ . ":" . __LINE__, "Time exceeded for password reset" );
				$GLOBALS['body_footer'] .= "<script>
            window.location.href = '" . $this->loc->http_domain . WEBROOT . ADMINDIR . "index.php';
        </script>
        ";

				return $content;
			}

			$password = $this->p->get( "password" );

			$crypt = new class_crypt();
			$hash  = $crypt->hash( $password );

			$this->db->upd( __FILE__ . ":" . __LINE__, "accounts_admin", array( "pass" ), array( $hash ), "`id` ='" . $row['to_id'] . "' AND `deleted` = '0'", "1" );
		}

		$GLOBALS['body_footer'] .= "<script>
            window.location.href = '" . $this->loc->httpauto_web_admin_root . "';
        </script>
        ";
	}
}

$class_pass_reset = new class_pass_reset ();
$content .= $class_pass_reset->get_content();