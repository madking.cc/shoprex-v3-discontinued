<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_PICTURE_UPLOAD;

class class_upload_picture extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		$action = $this->p->get( 'do', 'init' );

		switch ( $action ) {
			case "back";
				$this->delete();
			default:
			case "init":
				$this->content .= $this->init();
				break;
			case "picture_upload_1":
				$this->content .= $this->upload();
				break;
			case "picture_upload_2":
				$this->content .= $this->finished();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function init() {


		$content = "";

		$uploaddir   = $this->p->get( "uploaddir", UPLOADDIR, false );
		$target      = $this->p->get( "target" );
		$insert_type = $this->p->get( "insert_type" );

		if ( ( ! isset( $target ) || empty( $target ) ) && ( ! isset( $insert_type ) || empty( $insert_type ) ) ) {
			$GLOBALS['body_footer'] .= "
            <script>
            function window_close()
            {
                parent.$.fn.colorbox.close();
            }
            </script>\n";

			$content .= $this->l->alert_text( "danger", AL_NO_TARGET_DESTINATION . $this->l->button( AL_CLOSE_WINDOW, "onclick='window_close();'" ) . "" );
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "Missing Target" );

			return $content;
		}

		$element_is_image = $this->p->get( "element_is_image", 0 );
		$autoresize       = $this->p->get( "autoresize", 0 );
		$auto_max_x       = $this->p->get( "auto_max_x", 0 );
		$auto_max_y       = $this->p->get( "auto_max_y", 0 );
		$hint             = $this->p->get( "hint", "", false );
		$element_number   = $this->p->get( "element_number", 0 );
		$form_id          = $this->p->get( "form_id", "" );
		$do_submit        = $this->p->get( "do_submit", 0 );
		$resize           = $this->p->get( "resize", 1 );
		if ( $autoresize ) {
			$keep_ratio = 1;
		} else {
			$keep_ratio = $this->p->get( "keep_ratio", 0 );
		}
		$keep_original_picture = $this->p->get( "keep_original_picture", 1 );
		$one_step              = $this->p->get( "one_step", 0 );

		$imgupload  = new class_images();
		$init_array = array(
			"insert_type"           => $insert_type,
			"target"                => $target,
			"form_id"               => $form_id,
			"do_submit"             => $do_submit,
			"do_resize"             => $resize,
			"keep_ratio"            => $keep_ratio,
			"autoresize"            => $autoresize,
			"auto_max_x"            => $auto_max_x,
			"auto_max_y"            => $auto_max_y,
			"uploaddir"             => $uploaddir,
			"is_sub_window"         => true,
			"hint"                  => $hint,
			"do"                    => "picture_upload_1",
			"do_back"               => "back",
			"keep_original_picture" => $keep_original_picture,
			"uploadsubdir_orig"     => "orig/",
			"one_step"              => $one_step,
			"element_number"        => $element_number,
			"element_is_image"      => $element_is_image,
			"store_in_db"           => true
		);
		$imgupload->init( $init_array, MAIN_INIT );
		$content .= $imgupload->show_upload_page();

		return $content;
	}

	public function upload() {


		$imgupload = new class_images();
		$imgupload->init( array( "do" => "picture_upload_2" ), MAIN_INIT );
		$content = $imgupload->show_edit_page();

		return $content;
	}

	public function delete() {
		$uploaddir = $this->p->get( "uploaddir", "", false );

		if ( empty( $uploaddir ) ) {
			return;
		}

		$file_name_original_checked = $this->p->get( 'file_name_original_checked' );
		$file_name_checked          = $this->p->get( 'file_name_checked' );

		$this->img->delete_in_db_and_filesystem( $file_name_checked, $uploaddir, $file_name_original_checked );
	}

	public function finished() {
		$imgupload = new class_images();
		$content   = $imgupload->save();

		return $content;
	}

}

$class_upload_picture = new class_upload_picture ();
$content .= $class_upload_picture->get_content();
