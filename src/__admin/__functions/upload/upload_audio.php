<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_AUDIO_UPLOAD;

class class_upload_audio extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		$action = $this->p->get( 'do', 'init' );

		switch ( $action ) {
			default:
			case "init":
				$this->content .= $this->init();
				break;
			case "upload":
				$this->content .= $this->upload();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function init() {


		$content = "";

		$uploaddir     = $this->p->get( "uploaddir", null, false );
		$textarea_type = $this->p->get( "textarea_type", EDITOR_SIMPLE );
		$editor        = $this->p->get( "editor", null );

		if ( empty( $uploaddir ) ) {
			$GLOBALS['body_footer'] .= "
            <script>
            function window_close()
            {
                parent.$.fn.colorbox.close();
            }
            </script>\n";

			$content .= $this->l->alert_text( "danger", AL_NO_UPLOAD_DIR . $this->l->button( AL_CLOSE_WINDOW, "onclick='window_close();'" ) . "" );
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "Missing Name of upload directory" );

			return $content;
		}
		$uploaddir = urldecode( $uploaddir );

		if ( empty( $editor ) && empty( $target ) ) {
			$GLOBALS['body_footer'] .= "
            <script>
            function window_close()
            {
                parent.$.fn.colorbox.close();
            }
            </script>\n";

			$content .= $this->l->alert_text( "danger", AL_NO_DATA_DESTINATION . $this->l->button( AL_CLOSE_WINDOW, "onclick='window_close();'" ) . "" );
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "No destination given" );

			return $content;
		}

		$content .= $this->l->form_admin_file() . $this->l->hidden( "textarea_type", $textarea_type ) . $this->l->hidden( "editor", $editor ) . $this->l->hidden( "uploaddir", $uploaddir ) . $this->l->hidden( "do", "upload" ) . "
    <h3>" . AL_AUDIO_UPLOAD . "</h3>
    " . $this->l->table() . "
    <tr>
        <td>" . AL_TBL_MP3_FILE . "
        </td>
        <td>" . $this->l->file( "mp3_file", "300" ) . "
        </td>
    </tr>
    <tr>
        <td>" . AL_TBL_OGG_FILE . "
        </td>
        <td>" . $this->l->file( "ogg_file", "300" ) . "
        </td>
    </tr>
    <tr>
        <td>" . AL_TBL_WAV_FILE . "
        </td>
        <td>" . $this->l->file( "wav_file", "300" ) . "
        </td>
    </tr>
    <tr>
        <td>" . AL_DISPLAY_IN_PIXELS . "
        </td>
        <td>" . AL_TBL_WIDTH . " " . $this->l->text( "size_x", "300" ) . " " . AL_TBL_HEIGHT . " " . $this->l->text( "size_y", "50" ) . "
        </td>
    </tr>
    <tr>
        <td colspan=\"2\">" . AL_MAXIMUM_FILE_SIZE_TOTAL . " " . $this->p->get_max_upload() . " " . AL_MEGABYTE . "<br />
        <span class='info'>" . AL_SPECIFY_LEAST_ONE_AUDIO_FILE . "</span>
        </td>
    </tr>";

		$content .= "<tr>
        <td>
        </td>
        <td>" . $this->l->submit( AL_UPLOAD ) . "
        </td>
    </tr>
    </table>
    </form>";

		return $content;
	}

	public function upload() {

		$content = "";

		$uploaddir     = $this->p->get( "uploaddir", null, false );
		$textarea_type = $this->p->get( "textarea_type" );
		$editor        = $this->p->get( "editor", null );

		$size_x = $this->p->get( "size_x" );
		$size_y = $this->p->get( "size_y" );

		$mp4_file = null;
		if ( isset( $_FILES['mp3_file'] ) && ! empty( $_FILES['mp3_file']['name'] ) ) {
			if ( $_FILES['mp3_file']['error'] != 0 ) {
				return $this->p->file_upload_error( $_FILES['mp3_file']['error'], $_FILES['mp3_file']['name'] );
			}

			// Bestimme die Formatierung des Dateinamens:
			$tmp               = $_FILES['mp3_file']['name'];
			$file_name_checked = $this->p->check_filename( $uploaddir . $_FILES['mp3_file']['name'] );
			if ( $file_name_checked == false ) {
				$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $tmp . "'";

				$content .= $this->l->alert_text( "danger", $error, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$tmp'" );

				return $content;
			}

			// Bestimme Zieldatei mit Pfad:
			$upload_file_destination = $this->loc->dir_root . $uploaddir . $file_name_checked;

			// Die Original Datei hochladen:
			if ( move_uploaded_file( $_FILES['mp3_file']['tmp_name'], $upload_file_destination ) ) {
				$mp3_file = $file_name_checked;
				$this->log->event( "file", __FILE__ . ":" . __LINE__, "mp3 file uploaded: " . $upload_file_destination );
			} else {
				$error = AL_ERROR_WITH_FILE_UPLOAD_TOO_BIG . "<br/>
                " . AL_SOURCE_PATH . " " . $_FILES['mp3_file']['tmp_name'] . "<br />
                " . AL_DESTINATION_PATH . " " . $upload_file_destination;
				$content .= $this->l->alert_text( "danger", $error, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cannot upload mp3 file. Sourcepath: " . $_FILES['mp3_file']['tmp_name'] . ", destinationpath: " . $upload_file_destination );

				return $content;
			}
		}

		$ogg_file = null;
		if ( isset( $_FILES['ogg_file'] ) && ! empty( $_FILES['ogg_file']['name'] ) ) {
			if ( $_FILES['ogg_file']['error'] != 0 ) {
				return $this->p->file_upload_error( $_FILES['ogg_file']['error'], $_FILES['ogg_file']['name'] );
			}

			// Bestimme die Formatierung des Dateinamens:
			$tmp               = $_FILES['ogg_file']['name'];
			$file_name_checked = $this->p->check_filename( $uploaddir . $_FILES['ogg_file']['name'] );
			if ( $file_name_checked == false ) {
				$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $tmp . "''";

				$content .= $this->l->alert_text( "danger", $error, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$tmp'" );

				return $content;
			}

			// Bestimme Zieldatei mit Pfad:
			$upload_file_destination = $this->loc->dir_root . $uploaddir . $file_name_checked;

			// Die Original Datei hochladen:
			if ( move_uploaded_file( $_FILES['ogg_file']['tmp_name'], $upload_file_destination ) ) {
				$ogg_file = $file_name_checked;
				$this->log->event( "file", __FILE__ . ":" . __LINE__, "ogg Datei wurde hochgeladen: " . $upload_file_destination );
			} else {
				$error = AL_ERROR_WITH_FILE_UPLOAD_TOO_BIG . "<br/>
                " . AL_SOURCE_PATH . " " . $_FILES['ogg_file']['tmp_name'] . "<br />
                " . AL_DESTINATION_PATH . " " . $upload_file_destination;
				$content .= $this->l->alert_text( "danger", $error, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cannot upload ogg file. Sourcepath: " . $_FILES['ogg_file']['tmp_name'] . ", destinationpath: " . $upload_file_destination );

				return $content;
			}
		}

		$wav_file = null;
		if ( isset( $_FILES['wav_file'] ) && ! empty( $_FILES['wav_file']['name'] ) ) {
			if ( $_FILES['wav_file']['error'] != 0 ) {
				return $this->p->file_upload_error( $_FILES['wav_file']['error'], $_FILES['wav_file']['name'] );
			}

			// Bestimme die Formatierung des Dateinamens:
			$tmp               = $_FILES['wav_file']['name'];
			$file_name_checked = $this->p->check_filename( $uploaddir . $_FILES['wav_file']['name'] );
			if ( $file_name_checked == false ) {
				$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $tmp . "''";

				$content .= $this->l->alert_text( "danger", $error, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$tmp'" );

				return $content;
			}

			// Bestimme Zieldatei mit Pfad:
			$upload_file_destination = $this->loc->dir_root . $uploaddir . $file_name_checked;

			// Die Original Datei hochladen:
			if ( move_uploaded_file( $_FILES['wav_file']['tmp_name'], $upload_file_destination ) ) {
				$wav_file = $file_name_checked;
				$this->log->event( "file", __FILE__ . ":" . __LINE__, "wav Datei wurde hochgeladen: " . $upload_file_destination );
			} else {
				$error = AL_ERROR_WITH_FILE_UPLOAD_TOO_BIG . "<br/>
                " . AL_SOURCE_PATH . " " . $_FILES['wav_file']['tmp_name'] . "<br />
                " . AL_DESTINATION_PATH . " " . $upload_file_destination;
				$content .= $this->l->alert_text( "danger", $error, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cannot upload wav file. Sourcepath: " . $_FILES['wav_file']['tmp_name'] . ", destinationpath: " . $upload_file_destination );

				return $content;
			}
		}

		$text = "<span style='border: 1px solid #DDDDDD;'>";
		$text .= "<audio controls width=\"" . $size_x . "\" height=\"" . $size_y . "\">";
		if ( ! empty( $mp3_file ) ) {
			$text .= "<source src=\"" . $this->loc->web_root . $uploaddir . $mp3_file . "\" type=\"audio/mpeg\">";
		}
		if ( ! empty( $ogg_file ) ) {
			$text .= "<source src=\"" . $this->loc->web_root . $uploaddir . $ogg_file . "\" type=\"audio/ogg\">";
		}
		if ( ! empty( $wav_file ) ) {
			$text .= "<source src=\"" . $this->loc->web_root . $uploaddir . $wav_file . "\" type=\"audio/wav\">";
		}
		$found = false;
		if ( ! empty( $mp3_file ) ) {
			$text .= "<embed src=\"" . $this->loc->web_root . $uploaddir . $mp3_file . "\" width=\"" . $size_x . "\" height=\"" . $size_y . "\">";
			$found = true;
		}
		if ( ! empty( $ogg_file ) && ! $found ) {
			$text .= "<embed src=\"" . $this->loc->web_root . $uploaddir . $ogg_file . "\" width=\"" . $size_x . "\" height=\"" . $size_y . "\">";
			$found = true;
		}
		if ( ! empty( $wav_file ) && ! $found ) {
			$text .= "<embed src=\"" . $this->loc->web_root . $uploaddir . $wav_file . "\" width=\"" . $size_x . "\" height=\"" . $size_y . "\">";
		}
		$text .= "</audio></span>";

		switch ( $textarea_type ) {
			// EDITOR SIMPLE
			case 0:
				$this->p->paste_rangy( $editor, $text );
				break;
			// EDITOR ENHANCED
			case 1:
				$this->p->paste_tinymce( $editor, $text );
				break;
			// EDITOR CODE
			case 2:
				$this->p->paste_codemirror( $editor, $text );
				break;
			// UPLOAD ONLY
			case 99:
				break;
			default:
				$this->error      = true;
				$this->error_text = $this->l->alert_text_dismiss( "danger", $this->txt( 'ERROR_WRONG_INSERT_TYPE' ) );
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong insert type: '" . $textarea_type . "'" );

				return;
		}
	}

}

$class_upload_audio = new class_upload_audio ();
$content .= $class_upload_audio->get_content();
