<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

// TODO: Test Layout


$GLOBALS['admin_subtitle'] = AL_MEDIA_MANAGER;

class class_upload_manager extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		$action = $this->p->get( 'do', 'init' );
		$editor = $this->p->get( "editor", null );

		switch ( $action ) {
			case "init":
				$this->content .= $this->init( $editor );
				break;
			case "rename":
				$this->rename_file();
				$this->content .= $this->init( $editor );
				break;
			case "delete":
				$this->delete_file();
				$this->content .= $this->init( $editor );
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	function init( $editor ) {


		$content = "";
		$element = $this->p->get( "element", false );
		if ( ! $element ) {
			$element = $this->p->get( "target", false );
		}
		$element_is_image = $this->p->get( "element_is_image", false );
		$submit           = $this->p->get( "submit", false );
		$delete           = $this->p->get( "delete", true );
		$open             = $this->p->get( "open", true );
		$rename           = $this->p->get( "rename", true );
		$textarea_type    = $this->p->get( "textarea_type", EDITOR_SIMPLE );
		$uploaddir        = $this->p->get( "uploaddir", UPLOADDIR );
		$save_delete      = $this->p->get( "save_delete", 1 );
		$relative_insert  = $this->p->get( "relative_insert", 0 );

		if ( $this->l->get_preview_status() ) {
			$content .= "<p class='info'>" . AL_DELETING_RENAMING_DISABLED_PREVIEW . "</p>";
		}

		//$content .= $this->l->hidden("textarea_type", $textarea_type) . $this->l->hidden("editor", $editor) . $this->l->hidden("uploaddir", $uploaddir) . $this->l->hidden("do", "upload") . "

		$content .= "<h3>" . AL_MEDIA_MANAGER . "</h3>\n";

		$files      = $this->p->get_files( $uploaddir );
		$file_array = array();

		$i = 0;
		foreach ( $files as $filename ) {
			$create_date                     = $this->p->get_filecreatedate( $uploaddir . $filename, GET_UNIX_TIME );
			$file_array[ $i ]['create_date'] = $create_date;
			$file_array[ $i ]['filename']    = $filename;

			$size                             = @filesize( $this->loc->dir_root . $uploaddir . $filename );
			$file_array[ $i ]['size_display'] = $this->show_file_size_auto( $size );

			$file_array[ $i ]['size'] = $size;
			$file_type                = $this->get_filetype( $filename );
			switch ( $file_type ) {
				case "jpg":
				case "png":
				case "gif":
				case "jpeg":
					$file_array[ $i ]['type'] = $this->l->img( $uploaddir . $filename, "img", "width='40'" );
					break;
				default:
					$file_array[ $i ]['type'] = $file_type;
			}
			$i ++;
		}

		if ( isset( $file_array ) && sizeof( $file_array ) > 0 ) {
			$sort      = $this->p->get( "sort", "name" );
			$direction = $this->p->get( "direction", false );

			if ( empty( $direction ) ) {
				$direction_link = "up";
			} elseif ( $direction == "up" ) {
				$direction_link = "down";
			} else {
				$direction_link = "up";
			}

			$date_direction_link = "";
			$size_direction_link = "";
			$name_direction_link = "";
			$type_direction_link = "";
			$date_class          = "";
			$size_class          = "";
			$type_class          = "";
			$name_class          = "";
			switch ( $sort ) {
				case "date":
					$this->p->aasort( $file_array, "create_date" );
					$date_direction_link = "&direction=" . $direction_link;
					if ( $direction != "up" ) {
						$date_class = "selected_down";
					} else {
						$date_class = "selected_up";
					}
					break;
				case "size":
					$this->p->aasort( $file_array, "size" );
					$size_direction_link = "&direction=" . $direction_link;
					if ( $direction != "up" ) {
						$size_class = "selected_down";
					} else {
						$size_class = "selected_up";
					}
					break;
				case "type":
					$this->p->aasort( $file_array, "type" );
					$type_direction_link = "&direction=" . $direction_link;
					if ( $direction != "up" ) {
						$type_class = "selected_down";
					} else {
						$type_class = "selected_up";
					}
					break;
				case "name":
					$name_direction_link = "&direction=" . $direction_link;
					if ( $direction != "up" ) {
						$name_class = "selected_down";
					} else {
						$name_class = "selected_up";
					}
				default:
					$this->p->aasort( $file_array, "filename" );
					break;
			}

			$parameter = "&do=init&editor=$editor&textarea_type=$textarea_type&uploaddir=$uploaddir&save_delete=$save_delete&relative_insert=$relative_insert&element_is_image=$element_is_image&target=$element";

			$content .= $this->l->table() . "
        <tr>
            <th>" . $this->l->link( AL_TBL_TYPE, ADMINDIR . "upload_manager.php", "sort=type" . $type_direction_link . $parameter, EMPTY_FREE_TEXT, NO_ANKER, "link " . $type_class ) . "</th><th>" .
			            $this->l->link( AL_TBL_FILENAME, ADMINDIR . "upload_manager.php", "sort=name" . $name_direction_link . $parameter, EMPTY_FREE_TEXT, NO_ANKER, "link " . $name_class ) . "</th><th>
            " . $this->l->link( AL_TBL_CREATED_ON, ADMINDIR . "upload_manager.php", "sort=date" . $date_direction_link . $parameter, EMPTY_FREE_TEXT, NO_ANKER, "link " . $date_class ) . "</th><th>" .
			            $this->l->link( AL_TBL_SIZE, ADMINDIR . "upload_manager.php", "sort=size" . $size_direction_link . $parameter, EMPTY_FREE_TEXT, NO_ANKER, "link " . $size_class ) . "</th>";
			if ( $delete ) {
				$content .= "<th>" . AL_TBL_DELETE_QUE . "</th>";
			}
			$content .= "
        </tr>
        ";

			if ( $direction == "up" ) {
				$file_array = array_reverse( $file_array );
			}

			foreach ( $file_array AS $array ) {

				$content .= "<tr>
            <td align='center'>" . $array['type'] . "</td>
            <td>" . $this->l->form_admin() . $this->l->hidden( "do", "rename" ) . $this->l->hidden( "uploaddir", $uploaddir ) . $this->l->hidden( "save_delete", $save_delete ) . $this->l->hidden( "sort", $sort ) .
				            $this->l->hidden( "file", $array['filename'] ) . $this->l->text( "newfilename", $array['filename'], "400" );
				if ( $rename ) {
					$content .= $this->l->button( AL_RENAME, "onclick='confirmRename(this.form)'" );
				}
				if ( $open ) {
					$content .= $this->l->button( AL_OPEN, "onclick='openFile(this.form)'" );
				}

				if ( ! empty( $editor ) ) {
					$content .= $this->l->button( AL_INSERT, "onclick='insert(this.form, false)'" );

					if ( $relative_insert ) {
						$content .= $this->l->button( AL_RELATIVE_INSERT, "onclick='insert(this.form, \"$relative_insert\")'" );
					}
				}
				if ( ! empty( $element ) ) {
					$content .= $this->l->button( AL_ADD, "onclick='add_to_element(this.form, \"$element\", \"$submit\")'" );
				}

				$content .= "</form></td>
            <td>
                " . date( $GLOBALS['default_datetime_format']['datetime_seconds'], $array['create_date'] ) . "
            </td>
            <td align='right'>" . $array['size_display'] . "

            </td>";
				if ( $delete ) {
					$content .= "<td>" . $this->l->form_admin() . $this->l->hidden( "do", "delete" ) . $this->l->hidden( "uploaddir", $uploaddir ) . $this->l->hidden( "save_delete", $save_delete ) . $this->l->hidden( "sort", $sort ) . $this->l->hidden( "file", $array['filename'] ) . $this->l->button( AL_DELETE, "onclick='confirmDelete(this.form)'" ) . "</form></td>\n";
				}
				$content .= "
            </tr>
            ";
			}

			$content .= "</table>
        ";

			$GLOBALS['body_footer'] .= "
        <script>
            function openFile(formID)
            {
                http_root = '" . $this->loc->httpauto_web_root . "';
                sub_dirs = '" . $uploaddir . "';
                file = formID.file.value;

                window.open(http_root + sub_dirs + file);
            }

            function add_to_element(formID, parent_element, parent_submit_form)
            {
                    ";
			if ( ! $element_is_image ) {
				$GLOBALS['body_footer'] .= "
                    $('#'+parent_element, parent.document).val(formID.file.value);
                    $('#'+parent_submit_form, parent.document).submit();\n";
			} else {
				$GLOBALS['body_footer'] .= "
                $('#source_'+parent_element, parent.document).val(formID.file.value);
                $('#'+parent_element, parent.document).attr('src','/" . $this->loc->web_root . "files/'+formID.file.value);";
			}
			$GLOBALS['body_footer'] .= "
                    parent.$.fn.colorbox.close();
            }

            function insert(formID, relative)
            {
                    file = formID.file.value;
                    if(relative == false)
                    {
                        path = '" . $this->loc->httpauto_web_root . "' + '" . $uploaddir . "';
                    }
                    else
                    {
                        path = relative;
                    }
                    text = path + file;

                    ";

			$GLOBALS['body_footer'] .= $this->l->paste_textarea( $textarea_type, $editor, "text", JS_MODE );

			$GLOBALS['body_footer'] .= "
            }

            function confirmDelete(formID)
            {
                ";
			if ( $save_delete ) {
				$GLOBALS['body_footer'] .= "
                result = confirm('" . AL_DELETE_FILE_CAN_RESTORED . "');\n";
			} else {
				$GLOBALS['body_footer'] .= "
                result = confirm('" . AL_DELETE_FILE_CANNOT_RESTORED . "');\n";
			}

			$GLOBALS['body_footer'] .= "
            if(result)
                {
                    formID.submit();
                }

            }

            function confirmRename(formID)
            {
                oldFile = formID.file.value;
                newFile = formID.newfilename.value;

                result = confirm('" . AL_RENAME_FILE_PART01 . " \"' + oldFile + '\" in \"' + newFile + '\" " . AL_RENAME_FILE_PART02 . "');
                if(result)
                {
                    formID.submit();
                }

            }

        </script>
        ";
		} else {
			$content .= "<p class='wrapper'>" . AL_NO_FILES . "</p>";
		}

		return $content;
	}

	function rename_file() {


		$uploaddir = $this->p->get( "uploaddir", UPLOADDIR );

		$old_file_name = $this->p->get( "file" );
		$new_file_name = $this->p->get( "newfilename" );

		if ( empty( $new_file_name ) ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "New Filename is empty: '$new_file_name'" );

			return false;
		}

		$tmp           = $new_file_name;
		$new_file_name = $this->p->check_filename( $uploaddir . $new_file_name );
		if ( $new_file_name == false ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$tmp'" );

			return false;
		}

		if ( ! $this->l->get_preview_status() ) {
			$result = rename( $this->loc->dir_root . $uploaddir . $old_file_name, $this->loc->dir_root . $uploaddir . $new_file_name );

		} else {
			$result = false;
		}

		if ( $result ) {
			$this->img->rename_in_db( $old_file_name, $uploaddir, $new_file_name );
			$this->log->event( "file", __FILE__ . ":" . __LINE__, "File renamed, original: '" . $this->loc->dir_root . $uploaddir . $old_file_name . "', new: '" . $this->loc->dir_root . $new_file_name . "'" );
		} else {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "File cannot be renamed, original: '" . $this->loc->dir_root . $uploaddir . $old_file_name . "', new: '" . $this->loc->dir_root . $new_file_name . "'" );
		}
	}

	function delete_file() {

		$uploaddir   = $this->p->get( "uploaddir", UPLOADDIR );
		$save_delete = $this->p->get( "save_delete", 1 );

		$file_name = $this->p->get( "file" );

		if ( ! $this->l->get_preview_status() ) {

			if ( $save_delete ) {
				$result  = true;
				$result2 = true;
				$this->img->delete_in_db_and_filesystem( $file_name, $uploaddir );
			} else {
				$result = unlink( $this->loc->dir_root . $uploaddir . $file_name );
				if ( is_file( $this->loc->dir_root . $uploaddir . "orig/" . $file_name ) ) {
					$result2 = unlink( $this->loc->dir_root . $uploaddir . "orig/" . $file_name );
				} else {
					$result2 = true;
				}
			}
		} else {
			$result  = false;
			$result2 = true;
		}

		if ( $result && ! $save_delete ) {
			$this->log->event( "file", __FILE__ . ":" . __LINE__, "File deleted: '" . $this->loc->dir_root . $uploaddir . $file_name . "'" );

		} elseif ( ! $save_delete ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cannot delete file. '" . $this->loc->dir_root . $uploaddir . $file_name . "'" );
		}

		if ( ! $result2 && ! $save_delete ) {
			$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cannot delete file. '" . $this->loc->dir_root . $uploaddir . "orig/" . $file_name . "'" );
		}

	}
}

$class_upload_manager = new class_upload_manager ();
$content .= $class_upload_manager->get_content();