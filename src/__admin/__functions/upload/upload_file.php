<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_FILE_UPLOAD;

class class_upload_file extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		$action = $this->p->get( 'do', 'init' );

		switch ( $action ) {
			default:
			case "init":
				$this->content .= $this->init();
				break;
			case "file_upload_1":
				$this->content .= $this->upload();
				break;
			case "file_upload_2":
				$this->content .= $this->finished();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function init() {


		$content = "";

		$textarea_type = $this->p->get( "textarea_type", EDITOR_SIMPLE );
		$uploaddir     = $this->p->get( "uploaddir", null, NOT_ESCAPED );

		if ( empty( $uploaddir ) ) {
			$GLOBALS['body_footer'] .= "
            <script>
            function window_close()
            {
                parent.$.fn.colorbox.close();
            }
            </script>\n";

			$content .= $this->l->alert_text( "danger", AL_NO_UPLOAD_DIR . $this->l->button( AL_CLOSE_WINDOW, "onclick='window_close();'" ) . "" );

			$this->log->error( "value", __FILE__ . ":" . __LINE__, "Missing Name of upload directory" );

			return $content;
		}
		$uploaddir = urldecode( $uploaddir );

		$editor  = $this->p->get( "editor", null );
		$target  = $this->p->get( "target", null );
		$element = $this->p->get( "element", 0 );
		if ( empty( $editor ) && empty( $target ) ) {
			$GLOBALS['body_footer'] .= "
            <script>
            function window_close()
            {
                parent.$.fn.colorbox.close();
            }
            </script>\n";

			$content .= $this->l->alert_text( "danger", AL_NO_DATA_DESTINATION . $this->l->button( AL_CLOSE_WINDOW, "onclick='window_close();'" ) . "" );

			$this->log->error( "value", __FILE__ . ":" . __LINE__, "No destination given" );

			return $content;
		}

		$content .= $this->l->form_admin_file() . $this->l->hidden( "textarea_type", $textarea_type ) . $this->l->hidden( "uploaddir", $uploaddir ) . $this->l->hidden( "editor", $editor ) . $this->l->hidden( "do", "file_upload_1" ) . $this->l->hidden( "target", $target ) . $this->l->hidden( "element", $element ) . "
    <h3>" . AL_FILE_UPLOAD . "</h3>
    " . $this->l->table() . "
    <tr>
        <td>" . AL_TBL_SELECT_THE_FILE . "
        </td>
        <td>" . $this->l->file( "file_to_upload", "300" ) . "
        </td>
    </tr>
    <tr>
        <td colspan=\"2\">" . AL_MAXIMUM_FILE_SIZE . " " . $this->p->get_max_upload() . " " . AL_MEGABYTE . "
        </td>
    </tr>";
		$content .= "<tr>
        <td>
        </td>
        <td>" . $this->l->submit( AL_UPLOAD ) . "
        </td>
    </tr>
    </table>
    </form>";

		return $content;
	}

	public function upload() {


		$content = $this->l->show_file_upload();

		return $content;
	}

	public function finished() {

		$content = "";

		$content .= $this->l->save_file_upload();

		return $content;
	}

}

$class_upload_file = new class_upload_file ();
$content .= $class_upload_file->get_content();