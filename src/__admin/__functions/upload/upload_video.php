<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;
$GLOBALS['admin_subtitle'] = AL_VIDEO_UPLOAD;

class class_upload_video extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		$action = $this->p->get( 'do', 'init' );

		switch ( $action ) {
			case "init":
				$this->content .= $this->init();
				break;
			case "upload":
				$this->content .= $this->upload();
				break;
		}


	}

	public function get_content() {
		return $this->content;
	}

	public function init() {


		$content = "";

		$textarea_type = $this->p->get( "textarea_type", EDITOR_SIMPLE );
		$uploaddir     = $this->p->get( "uploaddir", null, NOT_ESCAPED );
		$editor        = $this->p->get( "editor", null );

		if ( empty( $uploaddir ) ) {
			$GLOBALS['body_footer'] .= "
            <script>
            function window_close()
            {
                parent.$.fn.colorbox.close();
            }
            </script>\n";

			$content .= $this->l->alert_text_dismiss( "danger", AL_NO_UPLOAD_DIR );
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "Missing Name of upload directory" );

			return $content;
		}
		$uploaddir = urldecode( $uploaddir );

		if ( empty( $editor ) && empty( $target ) ) {
			$GLOBALS['body_footer'] .= "
            <script>
            function window_close()
            {
                parent.$.fn.colorbox.close();
            }
            </script>\n";

			$content .= $this->l->alert_text_dismiss( "danger", AL_NO_DATA_DESTINATION );
			$this->log->error( "value", __FILE__ . ":" . __LINE__, "No destination given" );

			return $content;
		}

		$content .= $this->l->form_admin_file() . $this->l->hidden( "textarea_type", $textarea_type ) . $this->l->hidden( "editor", $editor ) . $this->l->hidden( "uploaddir", $uploaddir ) . $this->l->hidden( "do", "upload" ) . "
    <h3>" . AL_VIDEO_UPLOAD . "</h3>
    " . $this->l->table() . "
    <tr>
        <td>" . AL_TBL_FIRST_VIDEO_FILE_SELECT . "
        </td>
        <td>" . $this->l->file( "mp4_file", "300" ) . "
        </td>
    </tr>
    <tr>
        <td>" . AL_TBL_SECOND_VIDEO_FILE_SELECT . "
        </td>
        <td>" . $this->l->file( "ogg_file", "300" ) . "
        </td>
    </tr>
    <tr>
        <td>" . AL_DISPLAY_IN_PIXELS . "
        </td>
        <td>" . AL_TBL_WIDTH . " " . $this->l->text( "video_size_x", "640" ) . " " . AL_TBL_HEIGHT . " " . $this->l->text( "video_size_y", "390" ) . "
        </td>
    </tr>
    <tr>
        <td>" . AL_TBL_YOUTUBE_VIDEO_ID . "
        </td>
        <td>" . $this->l->text( "youtube_id" ) . "
        </td>
    </tr>
    <tr>
        <td>" . AL_DISPLAY_IN_PIXELS . "
        </td>
        <td>" . AL_TBL_WIDTH . " " . $this->l->text( "yt_size_x", "640" ) . " " . AL_TBL_HEIGHT . " " . $this->l->text( "yt_size_y", "390" ) . "
        </td>
    </tr>
    <tr>
        <td colspan=\"2\">" . AL_MAXIMUM_FILE_SIZE_TOTAL . " " . $this->p->get_max_upload() . " " . AL_MEGABYTE . "<br />

        <span class='info'>" . AL_SPECIFY_LEAST_ONE_AUDIO_FILE . "</span>

        " . $this->l->panel( AL_HELP_VIDEO_UPLOAD, "info", AL_INFORMATION ) . "
        </td>
    </tr>";

		$content .= "<tr>
        <td>
        </td>
        <td>" . $this->l->submit( AL_UPLOAD ) . "
        </td>
    </tr>
    </table>
    </form>";

		return $content;
	}

	public function upload() {


		$content = "";

		$uploaddir = $this->p->get( "uploaddir", null, NOT_ESCAPED );

		$textarea_type = $this->p->get( "textarea_type" );
		$editor        = $this->p->get( "editor", null );

		$video_size_x = $this->p->get( "video_size_x" );
		$video_size_y = $this->p->get( "video_size_y" );
		$yt_size_x    = $this->p->get( "yt_size_x" );
		$yt_size_y    = $this->p->get( "yt_size_y" );
		$youtube_id   = $this->p->get( "youtube_id" );

		$mp4_file = null;
		if ( isset( $_FILES['mp4_file'] ) && ! empty( $_FILES['mp4_file']['name'] ) ) {

			if ( $_FILES['mp4_file']['error'] != 0 ) {
				return $this->p->file_upload_error( $_FILES['mp4_file']['error'], $_FILES['mp4_file']['name'] );
			}

			// Bestimme die Formatierung des Dateinamens:
			$tmp               = $_FILES['mp4_file']['name'];
			$file_name_checked = $this->p->check_filename( $uploaddir . $_FILES['mp4_file']['name'] );
			if ( $file_name_checked == false ) {
				$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $tmp . "''";

				$content .= $this->l->alert_text( "danger", $error, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$tmp'" );

				return $content;
			}

			// Bestimme Zieldatei mit Pfad:
			$upload_file_destination = $this->loc->dir_root . $uploaddir . $file_name_checked;

			// Die Original Datei hochladen:
			if ( move_uploaded_file( $_FILES['mp4_file']['tmp_name'], $upload_file_destination ) ) {
				$mp4_file = $file_name_checked;
				$this->log->event( "file", __FILE__ . ":" . __LINE__, "mp4 file uploaded: " . $upload_file_destination );
			} else {

				$content .= $this->l->alert_text( "danger", AL_ERROR_WITH_FILE_UPLOAD_TOO_BIG . "<br/>
                " . AL_SOURCE_PATH . " " . $_FILES['mp4_file']['tmp_name'] . "<br />
                " . AL_DESTINATION_PATH . " " . $upload_file_destination, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cannot upload mp4 file. Sourcepath: " . $_FILES['mp4_file']['tmp_name'] . ", destinationpath: " . $upload_file_destination );

				return $content;
			}
		}

		$ogg_file = null;
		if ( isset( $_FILES['ogg_file'] ) && ! empty( $_FILES['ogg_file']['name'] ) ) {

			if ( $_FILES['ogg_file']['error'] != 0 ) {
				return $this->p->file_upload_error( $_FILES['ogg_file']['error'], $_FILES['ogg_file']['name'] );
			}

			// Bestimme die Formatierung des Dateinamens:
			$tmp               = $_FILES['ogg_file']['name'];
			$file_name_checked = $this->p->check_filename( $uploaddir . $_FILES['ogg_file']['name'] );
			if ( $file_name_checked == false ) {
				$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $tmp . "''";

				$content .= $this->l->alert_text( "danger", $error, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$tmp'" );

				return $content;
			}

			// Bestimme Zieldatei mit Pfad:
			$upload_file_destination = $this->loc->dir_root . $uploaddir . $file_name_checked;

			// Die Original Datei hochladen:
			if ( move_uploaded_file( $_FILES['ogg_file']['tmp_name'], $upload_file_destination ) ) {
				$ogg_file = $file_name_checked;
				$this->log->event( "file", __FILE__ . ":" . __LINE__, "ogg File uploaded: " . $upload_file_destination );
			} else {

				$content .= $this->l->alert_text( "danger", AL_ERROR_WITH_FILE_UPLOAD_TOO_BIG . "<br/>
                " . AL_SOURCE_PATH . " " . $_FILES['ogg_file']['tmp_name'] . "<br />
                " . AL_DESTINATION_PATH . " " . $upload_file_destination, DO_BACK );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "Cannot upload ogg file. Sourcepath: " . $_FILES['ogg_file']['tmp_name'] . ", destinationpath: " . $upload_file_destination );

				return $content;
			}
		}

		$text = "<span style='border: 1px solid #DDDDDD;'>";
		if ( ! empty( $mp4_file ) || ! empty( $ogg_file ) ) {
			$text .= "<video width=\"" . $video_size_x . "\" height=\"" . $video_size_y . "\" controls=\"controls\" autobuffer=\"autobuffer\">";
			if ( ! empty( $mp4_file ) ) {
				$text .= "<source src=\"/" . $this->loc->web_root . $uploaddir . $mp4_file . "\" type=\"video/mp4\">";
			}
			if ( ! empty( $ogg_file ) ) {
				$text .= "<source src=\"/" . $this->loc->web_root . $uploaddir . $ogg_file . "\" type=\"video/ogg\">";
			}
		}
		if ( ! empty( $mp4_file ) ) {
			$text .= "<object data=\"/" . $this->loc->web_root . $uploaddir . $mp4_file . "\" width=\"" . $video_size_x . "\" height=\"" . $video_size_y . "\">";
		}
		if ( ! empty( $youtube_id ) ) {
			$text .= "<iframe width=\"" . $yt_size_x . "\" height=\"" . $yt_size_y . "\" src=\"http://www.youtube.com/embed/" . $youtube_id . "?rel=0\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\">";
		}
		//if(!empty($youtube_id)) $text .= "<embed width=\\\"".$yt_size_x."\\\" height=\\\"".$yt_size_y."\\\" src=\\\"http://www.youtube.com/embed/".$youtube_id."?rel=0\\\"></embed>";
		//if(!empty($mp4_file)) $text .= "<a href=\\\"".$this->loc->web_root.$uploaddir.$mp4_file."\\\">".$mp4_file."</a>";
		//if(!empty($mp4_file) && !empty($ogg_file)) $text .= "<br />";
		//if(!empty($ogg_file)) $text .= "<a href=\\\"".$this->loc->web_root.$uploaddir.$ogg_file."\\\">".$ogg_file."</a>";
		if ( ! empty( $youtube_id ) ) {
			$text .= "</iframe>";
		}
		if ( ! empty( $mp4_file ) ) {
			$text .= "</object>";
		}
		if ( ! empty( $mp4_file ) || ! empty( $ogg_file ) ) {
			$text .= "</video>";
		}
		$text .= "</span>";

		switch ( $textarea_type ) {
			// EDITOR SIMPLE
			case 0:
				$this->p->paste_rangy( $editor, $text );
				break;
			// EDITOR ENHANCED
			case 1:
				$this->p->paste_tinymce( $editor, $text );
				break;
			// EDITOR CODE
			case 2:
				$this->p->paste_codemirror( $editor, $text );
				break;
			// UPLOAD ONLY
			case 99:
				break;
			default:
				$this->error      = true;
				$this->error_text = $this->l->alert_text_dismiss( "danger", $this->txt( 'ERROR_WRONG_INSERT_TYPE' ) );
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Wrong insert type: '" . $textarea_type . "'" );
		}

	}

}

$class_upload_video = new class_upload_video ();
$content .= $class_upload_video->get_content();

