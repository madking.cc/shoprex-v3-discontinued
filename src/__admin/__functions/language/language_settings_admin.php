<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_ADMIN_LANGUAGE;

class class_language_settings_admin extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		$action = $this->p->get( "do" );

		switch ( $action ) {

			case "update":
				$this->update();
				break;
			case "set_time":
				$this->content .= $this->set_time();
				break;
			case "update_time":
				$this->content .= $this->update_time();
				break;
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";

		$content .= $this->l->display_message_by_session( 'language_updated', AL_LANGUAGE_HAS_BEEN_UPDATED );

		$content .= "<h3 class='underline'>" . AL_ADMIN . " " . AL_LANGUAGE_SETTINGS . "</h3>";

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "update" ) . $this->l->table() . "<tr><th>" . AL_TBL_ID . "</th><th>" . AL_TBL_SORT_BY . "</th><th>" . AL_TBL_LANGUAGE . "</th><th>" . AL_TBL_HTML_CODE . "</th><th>" . AL_TBL_HREFLANG_CODE . "</th><th>" . AL_TBL_LINK_TITLE . "</th><th>" . AL_TBL_MAIN_LANGUAGE . "</th><th colspan='1'>" . AL_TBL_FLAG . "</th><th>" . AL_TBL_DATE_TIME . "</th><th>" . AL_TBL_ACTIVE . "</th></tr>\n";

		$sql    = "SELECT * FROM `" . TP . "language_admin_settings` ORDER BY `id`";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		$input_size           = "70";
		$media_button_counter = 0;
		while ( $row = $result->fetch_assoc() ) {
			if ( is_null( $row['img'] ) || empty( $row['img'] ) ) {
				$img_path = $GLOBALS['media_flags_dir'] . "empty.png";
			} else {
				$img_path = ADMINDIR . "layout/flags/" . $row['img'];
			}

			++ $media_button_counter;
			$content .= "<tr>
           <td>" . $row['id'] . $this->l->hidden( "id[]", $row['id'] ) . "</td>
           <td>" . $this->l->text( "pos[]", $row['pos'], intval( $input_size / 1.5 ) ) . "</td>
           <td>" . $this->l->text( "language[]", $row['language'], $input_size * 2 ) . "</td>

           <td>" . $this->l->text( "html_code[]", $row['html_code'], $input_size ) . "</td>
           <td>" . $this->l->text( "hreflang[]", $row['hreflang'], $input_size ) . "</td>
           <td>" . $this->l->text( "link_title[]", $row['link_title'], $input_size * 3 ) . "</td>
           <td>" . $this->l->radio( "main", $row['id'], "", $row['main'] ) . "</td>
           <td>" . $this->l->text( "source_img[]", $row['img'], $input_size * 2 ) . " " . $this->l->media_button( AL_SELECT, "img[]", "select_flag", "", "", ( $media_button_counter - 1 ) ) . " " . $this->l->img( $img_path, "", "name='img[]' style='max-width:" . FLAG_X . "px;max-height:" . FLAG_Y . "px;'" ) . "</td>
           <td>" . $this->l->button( AL_CONFIGURATION, "id=\"button_set_time_" . $row['id'] . "\"" ) . "</td>
           <td>" . $this->l->select_yesno( "active[]", $row['active'] ) . "</td>
           </tr>\n";

			$GLOBALS['body_footer'] .= "<script>
$(document).ready(function() {
$('#button_set_time_" . $row['id'] . "').click(function() {
    $.colorbox({href:'/" . WEBROOT . ADMINDIR . "language_settings_admin.php?do=set_time&id=" . $row['id'] . "', iframe:true, width:'" . $GLOBALS['box_width'] . "', height:'" . $GLOBALS['box_height'] . "' });
    });
});
</script>
";
		}

		$content .= "
    <tr><td colspan='9'>" . $this->l->submit( AL_SAVE ) . "</td></tr>
    </table></form>
    ";

		$this->l->display_preview();

		return $content;
	}

	public function update() {


		$id       = $this->p->get( "id", null, NOT_ESCAPED );
		$language = $this->p->get( "language", null, NOT_ESCAPED );

		$html_code  = $this->p->get( "html_code", null, NOT_ESCAPED );
		$hreflang   = $this->p->get( "hreflang", null, NOT_ESCAPED );
		$link_title = $this->p->get( "link_title", null, NOT_ESCAPED );
		$main       = $this->p->get( "main" );
		$img        = $this->p->get( "source_img", null, NOT_ESCAPED );
		$active     = $this->p->get( "active", null, NOT_ESCAPED );
		$pos        = $this->p->get( "pos", null, NOT_ESCAPED );

		if ( ! $this->l->get_preview_status() ) {
			foreach ( $pos as $key => $value ) {
				$pos[ $key ] = intval( $value );
			}

			foreach ( $id as $key => $row_id ) {
				if ( $main == $row_id ) {
					$set_main = true;
				} else {
					$set_main = false;
				}

				$language[ $key ]   = $this->db->escape( $language[ $key ] );
				$subdir_dummy       = "NULL";
				$html_code[ $key ]  = $this->db->escape( $html_code[ $key ] );
				$hreflang[ $key ]   = $this->db->escape( $hreflang[ $key ] );
				$link_title[ $key ] = $this->db->escape( $link_title[ $key ] );
				$img[ $key ]        = $this->db->escape( $img[ $key ] );
				$this->db->upd( __FILE__ . ":" . __LINE__, "language_admin_settings", array(
					"pos",
					"language",
					"subdir",
					"html_code",
					"hreflang",
					"link_title",
					"main",
					"img",
					"active"
				), array(
					$pos[ $key ],
					$language[ $key ],
					$subdir_dummy,
					$html_code[ $key ],
					$hreflang[ $key ],
					$link_title[ $key ],
					$set_main,
					$img[ $key ],
					$active[ $key ]
				), "`id` = '$row_id'" );
			}

			$this->update_files();
			$_SESSION['language_updated'] = 1;
		}
		$this->l->reload_js();
	}

	public function update_files( $flag_x = "", $flag_y = "" ) {
		$flags = $this->p->get_files( ADMINDIR . "layout/flags/" );

		if ( $flags !== false ) {
			foreach ( $flags as $flag ) {
				@$result = unlink( DIRROOT . ADMINDIR . "layout/flags/" . $flag );
				if ( $result == false ) {
					$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't delete file '" . DIRROOT . ADMINDIR . "layout/flags/" . $flag . "'" );
				}
			}
		}

		$sql    = "SELECT * FROM `" . TP . "language_admin_settings` WHERE `img` IS NOT NULL";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( empty( $flag_x ) ) {
			$flag_x = $GLOBALS['flag_x_admin'];
		}
		if ( empty( $flag_y ) ) {
			$flag_y = $GLOBALS['flag_y_admin'];
		}

		while ( $row = $result->fetch_assoc() ) {
			$this->img->resize( $row['img'], $GLOBALS['media_flags_dir'], $flag_x, $flag_y, DO_RATIO, NO_PREFIX, DIRECT_CALL, ADMINDIR . "layout/flags/", $row['img'] );
		}
	}

	public function set_time() {
		$GLOBALS['no_menu'] = true;

		$content = "";

		$id = $this->p->get( "id" );

		if ( ! isset( $id ) || empty( $id ) || ! is_numeric( $id ) || $id < 1 ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_TIME_SETTINGS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
		} else {
			$sql    = "SELECT * FROM `" . TP . "language_admin_settings` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows != 1 ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_TIME_SETTINGS );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "Can't find language entry, sql: '" . $sql . "', num_rows = '" . $result->num_rows . "'" );
			} else {
				$row = $result->fetch_assoc();

				if ( ! isset( $row['date_settings'] ) || empty( $row['date_settings'] ) ) {
					$date_settings = $GLOBALS['default_datetime_format'];
				} else {
					$date_settings = unserialize( $row['date_settings'] );
				}

				$select_timezone = $this->l->select( "date_settings[timezone]" );
				$select_timezone .= $this->l->select_timezone_options( $date_settings['timezone'] );
				$select_timezone .= "</select>";

				$content .= "<h3 class='underline'>" . AL_ADMIN . " " . AL_DATE_TIME_SETTINGS . "</h3>";

				$content .= $this->l->form_admin() . $this->l->hidden( "do", "update_time" ) . $this->l->hidden( "id", $id ) . $this->l->table() . "<tr><th>" . AL_TBL_TYPE . "</th><th>" . AL_TBL_EDIT . "</th><th>" . AL_TBL_TYPE . "</th><th>" . AL_TBL_EDIT . "</th></tr>\n";

				$content .= "<td>" . AL_TBL_DATE . "</td><td>" . $this->l->text( "date_settings[date]", $date_settings['date'] ) . "</td><td>" . AL_TBL_TIME . "</td><td>" . $this->l->text( "date_settings[time]", $date_settings['time'] ) . "</td></tr>\n";
				$content .= "<td>" . AL_TBL_DATE_TIME . "</td><td>" . $this->l->text( "date_settings[datetime]", $date_settings['datetime'] ) . "</td><td>" . AL_TBL_TIME_SECONDS . "</td><td>" . $this->l->text( "date_settings[time_seconds]", $date_settings['time_seconds'] ) . "</td></tr>\n";
				$content .= "<td>" . AL_TBL_DATE_TIME_SECONDS . "</td><td>" . $this->l->text( "date_settings[datetime_seconds]", $date_settings['datetime_seconds'] ) . "</td><td>" . AL_TBL_HOUR . "</td><td>" . $this->l->text( "date_settings[hour]", $date_settings['hour'] ) . "</td></tr>\n";
				$content .= "<td>" . AL_TBL_DAY_MONTH . "</td><td>" . $this->l->text( "date_settings[month_day]", $date_settings['month_day'] ) . "</td><td>" . AL_TBL_MONTH_YEAR . "</td><td>" . $this->l->text( "date_settings[month_year]", $date_settings['month_year'] ) . "</td></tr>\n";
				$content .= "<td>" . AL_TBL_YEAR . "</td><td>" . $this->l->text( "date_settings[year]", $date_settings['year'] ) . "</td><td>" . AL_TBL_TIMEZONE . "</td><td>" . $select_timezone . "</td></tr>\n";

				$content .= "<td colspan='4'>" . $this->l->submit( AL_SAVE ) . "</td></tr></table></form>\n";
			}
		}

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_DATE_TIME_SETTINGS, "info", AL_INFORMATION );

		return $content;
	}

	function update_time() {


		$GLOBALS['no_menu'] = true;

		$content = "";

		$id = $this->p->get( "id" );

		if ( ! isset( $id ) || empty( $id ) || ! is_numeric( $id ) || $id < 1 ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_UPDATE_TIME_SETTINGS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		} elseif ( ! $this->l->get_preview_status() ) {

			$date_settings = $this->p->get( "date_settings", false, NOT_ESCAPED );

			$date_settings = serialize( $date_settings );
			$date_settings = $this->db->escape( $date_settings );

			$this->db->upd( __FILE__ . ":" . __LINE__, "language_admin_settings", array( 'date_settings' ), array( $date_settings ), "`id` = '$id'", "1" );

			$GLOBALS['body_footer'] .= "<script>
        parent.$.fn.colorbox.close();
</script>\n";

			return "";
		}
	}

}

$class_language_settings_admin = new class_language_settings_admin ();
$content .= $class_language_settings_admin->get_content();