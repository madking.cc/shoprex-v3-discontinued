<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_ADMIN_LANGUAGE_CONSTANTS;

class class_language_constants_admin extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );

		switch ( $action ) {
			case "content_save":
				$this->content_save();
				$this->content .= $this->start();
				break;
			case "content_delete":
				$this->content .= $this->content_delete();
				$this->content .= $this->start();
				break;

			case "content_update":
				$this->content .= $this->content_update();
				$this->content .= $this->start();
				break;
			case "content_status":
				$this->content .= $this->content_status();
				$this->content .= $this->start();
				break;
			case "search":
			case "init":
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";


		$page_target = $this->p->get( "page_target" );

		$reset_search = $this->p->get( "reset_search", 0 );
		$search       = $this->p->get_session( "search", "", "text_search", $reset_search );

		$this->l->scroll_to_id( $page_target );

		$content .= $this->l->display_message_by_session( 'content_added', AL_TEXT_ADDED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_TEXTS_UPDATED );
		$content .= $this->l->display_message_by_session( 'content_deleted', AL_CONSTANT_DELETED );
		$content .= $this->l->display_message_by_session( 'content_status', AL_CONSTANT_STATUS_CHANGE );

		// Add lang constant
		$content .= "<h3 class='underline'>" . AL_ADMIN . " " . AL_ADD_LANG_CONSTANTS . "</h3>\n";
		$content .= $this->l->form_admin() . $this->l->hidden( "do", "content_save" ) . $this->l->table() . "<tr><th>" . AL_TBL_CONSTANT . "</th>";
		foreach ( $this->lang->languages_admin as $lang_array ) {
			$content .= "<th>" . $lang_array['text'] . ":</th>";
		}
		$content .= "</tr>\n<tr><td>" . $this->l->text( "constant" ) . "</td>";
		$delete_counter = 1;
		foreach ( $this->lang->languages_admin as $lang_array ) {
			$content .= "<td>" . $this->l->text( "text" . $lang_array['number'] ) . "</td>";
			$delete_counter ++;
		}
		$content .= "</tr>\n<tr><td colspan='$delete_counter'>" . $this->l->submit( AL_ADD_NEW_CONSTANT ) . "</td></tr></table></form>\n";

		// Show Header
		if ( empty( $search ) ) {
			$content .= "<h3 class='underline'>" . AL_EDIT_LANG_CONSTANTS . "</h3>\n";
		} else {
			$content .= "<h3 class='underline'>" . AL_LANG_CONSTANTS . " " . AL_HYPHEN . " " . AL_SEARCH_RESULT . "</h3>\n";
		}

		// Get constants
		if ( empty( $search ) ) {
			$sql    = "SELECT * FROM `" . TP . "language_admin` WHERE `deleted` = '0' ORDER BY `constant`";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$search_text = "";
			foreach ( $this->lang->languages_admin as $lang_array ) {
				$search_text .= " OR " . $this->db->search( "text" . $lang_array['number'], $search );
			}
			$sql    = "SELECT * FROM `" . TP . "language_admin` WHERE (" . $this->db->search( "constant", $search ) . $search_text . ") AND `deleted`=0 ORDER BY 'constant'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		}

		// Display search
		if ( $result->num_rows > 0 || ! empty( $search ) ) {

			// Search
			$content .= " " . $this->l->form_admin( "name='form_search'" ) . $this->l->text( "search", $search, "250", "id='search_text'" ) . $this->l->hidden( "do", "search" ) . " " . $this->l->submit( AL_SEARCH, "id='search'" ) . "
                <span id='message' class='error'>| " . AL_ERROR . ": " . AL_MISSING_SEARCH_TEXT . "</span>\n";
			$content .= "    </form>\n";

			// Search Reset
			if ( ! empty( $search ) ) {
				$content .= $this->l->form_admin() . $this->l->hidden( "reset_search", "1" ) . $this->l->submit( AL_RESET );
				$content .= "    </form>\n";
			}

			// Search Javascript
			$GLOBALS['body_footer'] .= "
        <script>
            $(document).ready(function () {
                $(\"#message\").hide();
            });
            $(\"form[name=form_search]\").submit(function( event ) {
                var val = $(\"input[name=search]\").val();
                if(val == '')
                {
                    $(\"#message\").fadeIn();
                    event.preventDefault();
                }
                else
                {
                    $(\"#message\").fadeOut();
                }
            });
             </script>\n";
		}

		// Display constants / search result
		if ( $result->num_rows != 0 ) {
			$array_head = array( AL_TBL_CONSTANT );
			if ( $this->lang->count_of_languages_admin > 1 ) {
				foreach ( $this->lang->languages_admin as $lang_array ) {
					$array_head[] = $lang_array['text'] . ":";
				}
			} else {
				$array_head[] = AL_TBL_TEXT;
			}
			$array_head[] = AL_TBL_DATE;
			$array_head[] = AL_TBL_ACTIVE;
			$array_head[] = array( AL_TBL_ACTION, "nosort" );

			$handler = $this->tbl->tbl_init( $array_head );

			// Get Data
			$delete_counter = 0;
			while ( $row = $result->fetch_assoc() ) {

				if ( $this->lang->count_of_languages_admin > 1 ) {
					foreach ( $this->lang->languages_admin as $lang_array ) {
						${"text" . $lang_array['number']} = htmlspecialchars( $row['text'], ENT_QUOTES );
					}
				} else {
					$text = $row['text'];
					$text = htmlspecialchars( $text, ENT_QUOTES );
				}

				// Get active status
				$active = $this->lang->answer( $row['active'] );
				if ( $row['active'] == "0" ) {
					$submit_status = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				} elseif ( $row['active'] == "1" ) {
					$submit_status = $this->l->submit( AL_DEACTIVATE );
				}

				// Get time
				if ( $row['changed'] == "0000-00-00 00:00:00" ) {
					$date = $this->dt->sqldatetime( $row['created'] );
				} else {
					$date = $this->dt->sqldatetime( $row['changed'] );
				}
				$timestamp = strtotime( $date );

				// Collect Data 1
				$array_load = array(
					$handler,
					$this->l->text( "constant", $row['constant'], "200", "id=\"constant_original_" . $row['id'] . "\"" )
				);
				$hidden     = "";
				if ( $this->lang->count_of_languages_admin > 1 ) {
					foreach ( $this->lang->languages_admin as $lang_array ) {
						$array_load[] = $this->l->text( "text" . $lang_array['number'], $row[ "text" . $lang_array['number'] ], "200", "id=\"text_original_" . $row['id'] . "_" . $lang_array['number'] . "\"" );
						$hidden .= $this->l->hidden( "text" . $lang_array['number'], $row[ "text" . $lang_array['number'] ], "id=\"text_" . $row['id'] . "_" . $lang_array['number'] . "\"" );
					}
				} else {
					$array_load[] = $this->l->text( "text", $text, "200", "id=\"text_original_" . $row['id'] . "\"" );
					$hidden       = $this->l->hidden( "text", $text, "id=\"text_" . $row['id'] . "\"" );
				}

				// Collect Data 3
				$array_load[] = array( $timestamp, "time", $date );
				$array_load[] = $active;
				// Define Buttons (Save, Status, Delete)
				$array_load[] = $this->l->form_admin( "id=\"form_" . $row['id'] . "\"" ) . $this->l->hidden( "do", "content_update" ) . $this->l->hidden( "id", $row['id'] ) . $this->l->hidden( "constant", $row['constant'], "id=\"constant_" . $row['id'] . "\"" ) . $hidden . $this->l->hidden( "page_target", "constant_original_" . $row['id'] ) . $this->l->button( AL_SAVE, "id=\"save_" . $row['id'] . "\"" ) . "</form>" . $this->l->form_admin() . $this->l->hidden( "page_target", "constant_original_" . $row['id'] ) . $this->l->hidden( "id", $row['id'] ) . $this->l->hidden( "do", "content_status" ) . $submit_status . "</form> " . $this->l->form_admin( "id=\"form_admin_delete_content_$delete_counter\"" ) . $this->l->hidden( "do", "content_delete" ) . $this->l->hidden( "id", $row["id"] ) . $this->l->button( AL_DELETE, "id=\"button_delete_content_$delete_counter\"" ) . "</form>";
				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_CONSTANT_PART01 . " \"" . $row['constant'] . "\" " . AL_DELETE_CONSTANT_PART02, "#button_delete_content_$delete_counter", "form_admin_delete_content_$delete_counter", "confirm_delete_$delete_counter" );
				// Put in table
				$this->tbl->tbl_load( $array_load );

				$GLOBALS['body_footer'] .= "
            <script>
            $(function(){
                $(document).on('click', '#save_" . $row['id'] . "', function(){
                    $('#constant_" . $row['id'] . "').val($('#constant_original_" . $row['id'] . "').val());
            ";
				if ( $this->lang->count_of_languages_admin > 1 ) {
					foreach ( $this->lang->languages_admin as $lang_array ) {
						$GLOBALS['body_footer'] .= "
                    $('#text_" . $row['id'] . "_" . $lang_array['number'] . "').val($('#text_original_" . $row['id'] . "_" . $lang_array['number'] . "').val());
                    ";
					}
				} else {
					$GLOBALS['body_footer'] .= "
                    $('#text_" . $row['id'] . "').val($('#text_original_" . $row['id'] . "').val());
                ";
				}
				$GLOBALS['body_footer'] .= "
                    $('#form_" . $row['id'] . "').submit();
                });
            });

            </script>
            ";

				$delete_counter ++;
			}
			// Output table
			$content .= $this->tbl->tbl_out( $handler, 0 );
		} else {
			if ( ! empty( $search ) ) {
				$content .= "<h3>" . AL_NO_MATCHES . "</h3>";
			}
		}

		return $content;
	}

	public function content_save() {


		$constant = $this->p->get( "constant" );

		$constant = $this->translate_special_characters( $constant, false, true );
		$constant = strtoupper( $constant );

		$array_keys   = array( "constant", "created" );
		$array_values = array( $constant, "NOW()" );

		foreach ( $this->lang->languages_admin as $lang_array ) {
			$array_keys[]   = "text" . $lang_array['number'];
			$array_values[] = $this->p->get( "text" . $lang_array['number'], "" );
		}

		$this->db->ins( __FILE__ . ":" . __LINE__, "language_admin", $array_keys, $array_values );

		$id = $this->db->get_insert_id();

		$this->log->event( "log", __FILE__ . ":" . __LINE__, "New language constant added, name: $constant, id: " . $id );

		$_SESSION['content_added'] = 1;
	}

	public function content_update() {
		$content = "";


		$constant = $this->p->get( "constant" );

		$constant = $this->translate_special_characters( $constant, false, true );
		$constant = strtoupper( $constant );
		$id       = $this->p->get( "id" );

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_UPDATE_CONSTANT );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= start();

			return $content;
		} else {
			$array_keys   = array( "constant" );
			$array_values = array( $constant );

			foreach ( $this->lang->languages_admin as $lang_array ) {
				$array_keys[]   = "text" . $lang_array['number'];
				$array_values[] = $this->p->get( "text" . $lang_array['number'], "" );
			}

			$this->db->upd( __FILE__ . ":" . __LINE__, "language_admin", $array_keys, $array_values, "`id` = '$id'" );

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "language constant changed, name: '$constant', id: " . $id );
			$_SESSION['content_updated'] = 1;

			return $content;
		}
	}

	public function content_delete() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {

			$this->db->upd( __FILE__ . ":" . __LINE__, "language_admin", array( "deleted" ), array( true ), "`id` = '$id'" );
			$this->log->event( "log", __FILE__ . ":" . __LINE__, "language frontend deleted, id: " . $id );
			$_SESSION['content_deleted'] = 1;

			return $content;
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_CONSTANT );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}
	}

	public function content_status() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT `active` FROM `" . TP . "language_admin` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();
				if ( $row['active'] == 0 ) {
					$active = 1;
				} else {
					$active = 0;
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "language_admin", array( "active" ), array( $active ), "`id` = '$id'" );
				$this->log->event( "log", __FILE__ . ":" . __LINE__, "text status changed, id: " . $id );
				$_SESSION['content_status'] = 1;

				return $content;
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_CONSTANT_STATUS );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_CONSTANT_STATUS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}
	}

}

$class_language_constants_admin = new class_language_constants_admin ();
$content .= $class_language_constants_admin->get_content();