<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_SITEMAPXML;

class class_settings_sitemap extends class_sys {
	public $content;

	public function __construct() {

		parent::__construct();

		$action = $this->p->get( "do" );

		switch ( $action ) {
			case "sitemap_update":
				$this->sitemap_update();
				break;
			case "sitemap_delete":
				$this->sitemap_delete();
				break;
			case "sitemap_show":
				$this->content .= $this->sitemap_show();
				break;
			case "update":
				$this->update();
				break;
			default:
				$this->content .= $this->start();
				break;
		}
	}

	public function get_content() {
		return $this->content;
	}


	public function start() {
		$content = "";


		$sitemap = $this->sitemap->sitemap_file;


		$sitemap_exists        = file_exists( DIRROOT . $sitemap );
		$sitemap_exists_answer = $this->lang->answer( $sitemap_exists );
		if ( $sitemap_exists ) {
			$sitemap_size = filesize( DIRROOT . $sitemap );
			$sitemap_date = filemtime( DIRROOT . $sitemap );
		} else {
			$sitemap_size = false;
			$sitemap_date = false;
		}

		$content .= $this->l->display_message_by_session( 'options_sitemap_deleted', AL_SITEMAPXML_DELETED );
		$content .= $this->l->display_message_by_session( 'options_sitemap_deleted_error', AL_ERROR_CANT_DELETE_SITEMAPXML );
		$content .= $this->l->display_message_by_session( 'options_saved', AL_SETTINGS_SAVED );

		$options_sitemap_updated = - 1;
		if ( isset( $_SESSION['options_sitemap_updated'] ) ) {
			$options_sitemap_updated = $_SESSION['options_sitemap_updated'];
			unset( $_SESSION['options_sitemap_updated'] );
		}
		if ( $options_sitemap_updated > 0 ) {

			$content .= $this->l->alert_text_dismiss( "success", AL_SITEMAP_UPDATED_WITH_X_ELEMENTS_PART01 . " " . $options_sitemap_updated . " " . AL_SITEMAP_UPDATED_WITH_X_ELEMENTS_PART02 );
		} elseif ( $options_sitemap_updated == 0 ) {
			$content .= $this->l->alert_text_dismiss( "success", NO_ELEMENTS_FOR_SITEMAP_UPDATE );
		}

		$options_sitemap_error = false;
		if ( isset( $_SESSION['options_sitemap_error'] ) ) {
			$options_sitemap_error = $_SESSION['options_sitemap_error'];
			unset( $_SESSION['options_sitemap_error'] );
		}
		if ( $options_sitemap_error ) {
			$content .= $this->l->alert_text_dismiss( "danger", $options_sitemap_error );
		}

		$content .= "<h3 class='underline'>" . AL_SITEMAPXML . "</h3>\n";

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "update" ) . $this->l->table() . "
    <tr><td>" . AL_TBL_SITEMAPXML_EXISTS . "</td><td>" . $sitemap_exists_answer . "</td></tr>\n";
		if ( $sitemap_exists ) {
			$content .= "<tr><td>" . AL_TBL_SIZE . "</td><td>" . $this->p->show_file_size( $sitemap_size ) . " " . AL_KILOBYTES_SHORT . "</td></tr>\n";
			$content .= "<tr><td>" . AL_TBL_LAST_CHANGE . "</td><td>" . date( $GLOBALS['default_datetime_format']['datetime_seconds'], $sitemap_date ) . "</td></tr>\n";
		}
		$content .= "<tr><td>" . AL_TBL_CREATE_SITEMAPXML_AUTMATICALLY . "</td><td>" . $this->l->select_yesno( "SITEMAP_AUTOMATIC", SITEMAP_AUTOMATIC ) . "</td></tr>
         <tr><td>" . AL_TBL_SITEMAPXML_HINTS_ENABLED . "</td><td>" . $this->l->select_yesno( "DO_SITEMAP_STATUS", DO_SITEMAP_STATUS ) . "</td></tr>
         <tr><td>" . AL_TBL_SITEMAP_HTTP_TYPE . "</td><td>" . $this->l->select( "SITEMAP_HTTP_TYPE", "", "120" ) . "<option value=\"http\"";
		if ( SITEMAP_HTTP_TYPE == "http" ) {
			$content .= " selected=selected";
		}
		$content .= ">http://</option>
            <option value=\"https\"";
		if ( SITEMAP_HTTP_TYPE == "https" ) {
			$content .= " selected=selected";
		}
		$content .= ">https://</option></select></td></tr>
    <tr><td></td><td>" . $this->l->submit( AL_SAVE ) . "</td></tr>
    </table></form>
    ";

		$content .= "<h3 class='underline'>" . AL_ACTIONS . "</h3>\n";

		$content .= $this->l->table() . "<tr><td>" . $this->l->form_admin() . $this->l->hidden( "do", "sitemap_update" ) . $this->l->submit( AL_UPDATE_SITEMAPXML ) . "</form>\n";
		if ( $sitemap_exists ) {
			$content .= " " . $this->l->form_admin( "id=\"form_admin_show_sitemap\"" ) . $this->l->button( AL_SHOW_SITEMAPXML, "id=\"button_show_sitemap\"" ) . "</form>\n";

			$GLOBALS['body_footer'] .= "<script>
$(document).ready(function() {
$('#button_show_sitemap').click(function() {
    $.colorbox({href:'/" . WEBROOT . ADMINDIR . "settings_sitemap.php?do=sitemap_show', iframe:true, width:'" . $GLOBALS['box_width'] . "', height:'" . $GLOBALS['box_height'] . "' });
    });
});
</script>
";
			$content .= " " . $this->l->form_admin( "id=\"form_admin_delete_sitemap\"" ) . $this->l->hidden( "do", "sitemap_delete" ) . $this->l->button( "Sitemap Löschen", "id=\"button_delete_sitemap\"", "btn btn-danger" ) . "</form></td>\n";
			$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_SITEMAP_XML_QUE, "#button_delete_sitemap", "form_admin_delete_sitemap", "dialog-confirm" );
		}

		$content .= "</tr></table>";

		return $content;
	}

	public function sitemap_show() {

		$sitemap = $this->sitemap->sitemap_file;


		$GLOBALS['no_menu'] = true;

		$content = "";

		$size = @filesize( DIRROOT . $sitemap );
		if ( file_exists( DIRROOT . $sitemap ) && ! empty( $size ) ) {
			$sitemap_content = $this->p->get_file_content( $sitemap );

			if ( $sitemap_content !== false ) {
				$content .= $this->parse->parse( "[code=xml]\n" . $sitemap_content . "\n[/code]" );
			} else {
				$content .= "<p>" . AL_ERROR_CANT_OPEN_FILE_FOR_READING . "</p>";
			}
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>";
		}

		return $content;
	}

	public function update() {


		$elements                      = array();
		$elements['SITEMAP_AUTOMATIC'] = $this->p->get( "SITEMAP_AUTOMATIC", 1 );
		$elements['DO_SITEMAP_STATUS'] = $this->p->get( "DO_SITEMAP_STATUS", 1 );
		$elements['SITEMAP_HTTP_TYPE'] = $this->p->get( "SITEMAP_HTTP_TYPE", "http" );

		foreach ( $elements as $key => $value ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "settings", array( "value" ), array( $value ), "UPPER(`key`) LIKE UPPER('$key')" );
			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Settings updated, key: " . $key . ", value: " . $value );
		}

		$_SESSION['options_saved'] = 1;

		$this->l->reload_js();
	}

	public function sitemap_delete() {


		$sitemap = $this->sitemap->sitemap_file;

		$status = file_exists( DIRROOT . $sitemap );

		if ( $status ) {
			$status = unlink( DIRROOT . $sitemap );
		}

		if ( $status === false ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "Datei sitemap.xml konnte nicht gelöscht werden." );
			$_SESSION['options_sitemap_deleted_error'] = 1;
			$this->l->reload_js();
		} else {
			$this->log->event( "php", __FILE__ . ":" . __LINE__, "Datei sitemap.xml wurde gelöscht." );
			$_SESSION['options_sitemap_deleted'] = 1;
		}

		$this->l->reload_js();
	}

	public function sitemap_update() {


		$sitemap_content = $this->sitemap->generate_sitemap_xml();

		if ( $sitemap_content['num_rows'] == 0 ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, AL_NO_ELEMENTS_FOR_SITEMAP_UPDATE );
			$_SESSION['options_sitemap_updated'] = 0;
		} else {
			$_SESSION['options_sitemap_updated'] = $sitemap_content['num_rows'];
		}

		$_SESSION['options_sitemap_error'] = $sitemap_content['error'];

		$this->l->reload_js();
	}

}

$class_settings_sitemap = new class_settings_sitemap ();
$content .= $class_settings_sitemap->get_content();