<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

define( "OPTION_ACCOUNT_UPDATED", 1 );
define( "OPTION_ACCOUNT_ADDED", 2 );
define( "OPTION_ACCOUNT_DELETED", 3 );
define( "OPTION_ERROR_CANT_CREATE_ACCOUNT", 4 );

$GLOBALS['admin_subtitle'] = AL_ADMIN_DATA;

class class_settings_admin_accounts extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		$action = $this->p->get( "do" );

		switch ( $action ) {
			case "update":
				$this->content .= $this->update();
				break;
			case "save":
				$this->content .= $this->save();
				break;
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";


		$content .= "<h3 class='underline'>" . AL_ADMIN_DATA . "</h3>\n";

		$options_saved = 0;
		if ( isset( $_SESSION['options_saved'] ) ) {
			$options_saved = $_SESSION['options_saved'];
			unset( $_SESSION['options_saved'] );
		}
		if ( $options_saved == OPTION_ACCOUNT_UPDATED ) {
			$content .= $this->l->alert_text_dismiss( "success", AL_ACCOUNT_UPDATED );
		} elseif ( $options_saved == OPTION_ACCOUNT_ADDED ) {
			$content .= $this->l->alert_text_dismiss( "success", AL_ACOUNT_ADDED );
		} elseif ( $options_saved == OPTION_ACCOUNT_DELETED ) {
			$content .= $this->l->alert_text_dismiss( "success", AL_ACCOUNT_DELETED );
		} elseif ( $options_saved == OPTION_ERROR_CANT_CREATE_ACCOUNT ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_ERROR_CANT_CREATE_ACCOUNT );
		}

		$sql    = "SELECT * FROM `" . TP . "accounts_admin` WHERE `deleted` = '0' ORDER BY `name`";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows == 0 ) {
			$content .= $this->l->alert_text( "danger", AL_NO_ADDMIN_ACCOUNT_FOUND, false );
			$this->log->error( "sql", __FILE__ . ":" . __LINE__, "No admin account found. SQL: " . $sql );

			return $content;
		}

		$count = $result->num_rows;
		if ( $count == 1 ) {
			$colspan = 4;
		} else {
			$colspan = 5;
		}

		while ( $row = $result->fetch_assoc() ) {
			$content .= $this->l->form_admin( "autocomplete='off'" ) . $this->l->hidden( "id", $row['id'] ) . $this->l->hidden( "do", "update" ) . $this->l->table() . "
        <tr><th>" . AL_TBL_LOGIN . "</th><th>" . AL_TBL_PASSWORD . "</th><th>" . AL_TBL_PASSWORD . "</th><th>" . AL_TBL_EMAIL . "</th>";
			if ( $count > 1 ) {
				$content .= "<th>" . AL_TBL_DELETE . "</th>";
			}
			$content .= "</tr>
        <tr>
        <td>" . $this->l->text( "name", $row['name'], "200" ) . "</td>
        <td>" . $this->l->password( "password", "", "200" ) . "</td>
        <td>" . $this->l->password( "password_check", "", "200" ) . "</td>
        <td>" . $this->l->text( "email", $row['email'], "200" ) . "</td>\n";
			if ( $count > 1 ) {
				$content .= "        <td>" . $this->l->checkbox( "delete", 1, false, 0, "", false ) . "</td>\n";
			}
			$content .= "        </tr>\n";
			$content .= "        <tr><td colspan='$colspan'>" . $this->l->button( AL_SAVE, "onclick='check_submit(this.form)'" ) . "</td></tr>
        </table></form>
        ";
		}

		$content .= "<H1>" . AL_NEW_ACCOUNT . "</H1>" . $this->l->form_admin( "autocomplete='off'" ) . $this->l->hidden( "do", "save" ) . $this->l->table() . "
        <tr><th>" . AL_TBL_LOGIN . "</th><th>" . AL_TBL_PASSWORD . "</th><th>" . AL_TBL_PASSWORD . "</th><th>E-" . AL_TBL_EMAIL . "</th></tr>
        <tr>
        <td>" . $this->l->text( "name", $row['name'], "200" ) . "</td>
        <td>" . $this->l->password( "password", "", "200" ) . "</td>
        <td>" . $this->l->password( "password_check", "", "200" ) . "</td>
        <td>" . $this->l->text( "email", $row['email'], "200" ) . "</td>
        </tr>
        <tr><td colspan='4'>" . $this->l->button( AL_ADD, "onclick='check_submit(this.form)'" ) . "</td></tr>
        </table></form>
        ";

		$GLOBALS['body_footer'] .= "<script>
    function check_submit(formID)
    {

        if(formID.name.value == '')
        {
            " . $this->l->box_alert( "<H3 class='error underline'>" . AL_ERROR . "</H3><p>" . AL_MUST_ENTER_NAME . "</p>" ) . "
            return;
        }
        if(formID.email.value == '')
        {
            " . $this->l->box_alert( "<H3 class='error underline'>" . AL_ERROR . "</H3><p>" . AL_MUST_ENTER_EMAIL . "</p>" ) . "
            return;
        }
        if(formID.password.value != '' || formID.password_check.value != '')
        {
            if(formID.password.value != formID.password_check.value)
            {
                " . $this->l->box_alert( "<H3 class='error underline'>" . AL_ERROR . "</H3><p>" . AL_PASSWORDS_NOT_THE_SAME . "</p>" ) . "
                return;
            }
        }
        formID.submit();
    }
    </script>
    ";

		$this->l->display_preview();

		return $content;
	}

	public function update() {
		$content = "";


		$name     = $this->p->get( "name" );
		$password = $this->p->get( "password", "", false );
		$email    = $this->p->get( "email" );
		$id       = $this->p->get( "id" );
		$delete   = $this->p->get( "delete", false );

		if ( ! ( is_numeric( $id ) && $id > 0 ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_UPDATE_USER, false );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= start();

			return $content;
		}

		if ( ! $this->l->get_preview_status() ) {
			if ( ! empty( $password ) ) {
				$crypt = new crypt();
				$hash  = $crypt->hash( $password );
				$hash  = $this->db->escape( $hash );

				$this->db->upd( __FILE__ . ":" . __LINE__, "accounts_admin", array(
					"name",
					"pass",
					"email",
					"deleted"
				), array( $name, $hash, $email, $delete ), "`id` ='$id'", "1" );

				if ( $delete ) {
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Admin Informations updated, new password set, account deleted" );
				} else {
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Admin Informations updated, new password set" );
				}
			} else {
				$this->db->upd( __FILE__ . ":" . __LINE__, "accounts_admin", array(
					"name",
					"email",
					"deleted"
				), array( $name, $email, $delete ), "`id` ='$id'", "1" );

				if ( $delete ) {
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Admin Informations updated, account deleted" );
				} else {
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Admin Informations updated" );
				}
			}
		}

		if ( $delete ) {
			$_SESSION['options_saved'] = OPTION_ACCOUNT_DELETED;
		} else {
			$_SESSION['options_saved'] = OPTION_ACCOUNT_UPDATED;
		}

		$this->l->reload_js();
	}

	public function save() {


		$name     = $this->p->get( "name" );
		$password = $this->p->get( "password", "", false );
		$email    = $this->p->get( "email" );

		if ( ! $this->l->get_preview_status() ) {
			if ( ! empty( $password ) ) {
				$crypt = new class_crypt();
				$hash  = $crypt->hash( $password );

				$hash = $this->db->escape( $hash );

				$this->db->ins( __FILE__ . ":" . __LINE__, "accounts_admin", array(
					"name",
					"pass",
					"email",
					"created"
				), array( $name, $hash, $email, "NOW()" ) );

				$this->log->event( "log", __FILE__ . ":" . __LINE__, "New Admin account added" );

				$_SESSION['options_saved'] = OPTION_ACCOUNT_ADDED;
			} else {
				$_SESSION['options_saved'] = OPTION_ERROR_CANT_CREATE_ACCOUNT;
			}
		}
		$this->l->reload_js();
	}

}

$class_settings_admin_accounts = new class_settings_admin_accounts ();
$content .= $class_settings_admin_accounts->get_content();