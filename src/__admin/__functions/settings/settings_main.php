<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_PAGE_SETTINGS;

class class_settings_main extends class_sys {
	public $content;

	public function __construct() {

		parent::__construct();


		$action = $this->p->get( "do" );

		switch ( $action ) {
			case "update_files":
				$this->update_files();
				break;
			case "update":
				$this->update();
				break;
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";

		$maintenance_styles = $this->get_dir_content( "__functions/maintenance/styles/" );

		$content .= $this->l->display_message_by_session( 'htaccess_saved', AL_HTACCESS_UPDATED );
		$content .= $this->l->display_message_by_session( 'settings_saved', AL_SETTINGS_SAVED );
		$content .= $this->l->display_message_by_session( 'debug_saved', AL_DEBUG_SAVED );
		$content .= $this->l->display_message_by_session( 'htaccess_error', AL_ERROR_HTACCESS_CANT_UPDATE, "danger" );
		$content .= $this->l->display_message_by_session( 'settings_error', AL_SETTINGS_COULD_NOT_BE_SAVED, "danger" );
		$content .= $this->l->display_message_by_session( 'debug_error', AL_DEBUG_COULD_NOT_BE_SAVED, "danger" );

		$content .= "<h3 class='underline'>" . AL_PAGE_SETTINGS . "</h3><p>" . AL_HINT_SAVED_IN_DATABASE . "</p>\n";

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "update" ) . $this->l->table();
		// <tr><td>" . AL_TBL_CHECK_INCOMPATIBLE_BROWSER . "</td><td>" . $this->l->select_yesno("CHECK_BROWSER", CHECK_BROWSER) . "</td></tr>
		$content .= "
        <tr><td>" . AL_TBL_PAGE_CREATED_ON . "</td><td>" . $this->l->datetime( "PAGE_CREATE_DATE", PAGE_CREATE_DATE ) . "</td></tr>
        <tr><td>" . AL_TBL_LOAD_CSS_LANGUAGE_FILES . "</td><td>" . $this->l->select_yesno( "LOAD_CSS_LANGUAGE_FILES", LOAD_CSS_LANGUAGE_FILES ) . " " . AL_HINT_LOAD_TIME . "</td></tr>
        <tr><td>" . AL_TBL_LOAD_JS_LANGUAGE_FILES . "</td><td>" . $this->l->select_yesno( "LOAD_JS_LANGUAGE_FILES", LOAD_JS_LANGUAGE_FILES ) . " " . AL_HINT_LOAD_TIME . "</td></tr>
    <tr><td></td><td>" . $this->l->submit( AL_SAVE ) . "</td></tr>
    </table></form>
    ";

		$content .= "<h3 class='underline'>" . AL_PAGE_SETTINGS . "</h3><p>" . AL_HINT_STORED_IN_FILES . "</p>\n";
		$content .= $this->l->form_admin() . $this->l->hidden( "do", "update_files" ) . $this->l->table() . "
    <tr><td></td><td>" . $this->l->submit( AL_SAVE ) . "</td></tr>
    <tr><td>" . AL_TBL_MAIL_BUGREPORT . "</td><td>" . $this->l->text( "ADMIN_MAIL", ADMIN_MAIL, "400" ) . "</td></tr>
    <tr><td>" . AL_TBL_DISPATCH_MAIL . "</td><td>" . $this->l->text( "MAIL_FROM", MAIL_FROM, "400" ) . "</td></tr>
    <tr><td>" . AL_TBL_DISPATCH_NAME . "</td><td>" . $this->l->text( "MAIL_FROM_NAME", MAIL_FROM_NAME, "400" ) . " " . AL_HINT_OPTIONAL . "</td></tr>
    <tr><td>" . AL_TBL_ALL_MAILS_BCC . "</td><td>" . $this->l->select_yesno( "SEND_BCC", SEND_BCC ) . "</td></tr>
    <tr><td>" . AL_TBL_BCC_RECIPIENT . "</td><td>" . $this->l->text( "MAIL_BCC", MAIL_BCC, "400" ) . "</td></tr>
    <tr><td>" . AL_TBL_BCC_RECIPIENT_NAME . "</td><td>" . $this->l->text( "MAIL_BCC_NAME", MAIL_BCC_NAME, "400" ) . " " . AL_HINT_OPTIONAL . "</td></tr>
    <tr><td>" . AL_TBL_WEBDIR . "</td><td>" . $this->l->text( "WEBROOT", WEBROOT, "400" ) . " " . AL_HINT_WITHOUT_LEADING_SLASH . " " . AL_HINT_ONLY_CHANGE_WHEN_YOU_ARE_BOFH . "</td></tr>
    <tr><td>" . AL_TBL_LOG_MAX_SIZE_MB . "</td><td>" . $this->l->text( "FILE_SIZE_FOR_ARCHIVE_IN_MB", FILE_SIZE_FOR_ARCHIVE_IN_MB, "80" ) . " " . AL_MEGABYTE . "</td></tr>
    <tr><td>" . AL_TBL_ADMINDIR . "</td><td>" . $this->l->text( "ADMINDIR", ADMINDIR, "400" ) . " " . AL_HINT_WITHOUT_LEADING_SLASH . "</td></tr>
    <tr><td>" . AL_TBL_UPLOAD_DIR . "</td><td>" . $this->l->text( "UPLOADDIR", UPLOADDIR, "400" ) . " " . AL_HINT_WITHOUT_LEADING_SLASH . "</td></tr>
    <tr><td>" . AL_TBL_DOWNLOAD_DIR . "</td><td>" . $this->l->text( "DOWNLOADDIR", DOWNLOADDIR, "400" ) . " " . AL_HINT_WITHOUT_LEADING_SLASH . "</td></tr>
    <tr><td>" . AL_TBL_GHOST_CODE . "</td><td>" . $this->l->text( "NO_COUNT_CODE", NO_COUNT_CODE, "400" ) . " " . AL_OPEN_PARENTHESIS . AL_USE_COLON . ": " . $this->loc->httpauto_web_root . "index.php?code=" . NO_COUNT_CODE . "" . AL_CLOSING_PARENTHESIS . "</td></tr>
    <tr><td>" . AL_TBL_ALLOW_ONLY_HTTPS . "</td><td>" . $this->l->select_yesno( "ONLY_HTTPS", ONLY_HTTPS ) . "</td></tr>
    <tr><td>" . AL_TBL_ENABLE_TAB_IN_TEXTAREAS . "</td><td>" . $this->l->select_yesno( "ENABLE_TAB_IN_TEXTAREAS", ENABLE_TAB_IN_TEXTAREAS ) . " " . AL_HINT_ONLY_IN_ADMIN_AREA . "</td></tr>
    <tr><td>" . AL_TBL_DEBUG_MODE . "</td><td>" . $this->l->select_yesno( "DO_DEBUG", DO_DEBUG ) . "</td></tr>
    <tr><td>" . AL_TBL_WRITE_LOG . "</td><td>" . $this->l->select_yesno( "WRITE_LOG", WRITE_LOG ) . "</td></tr>
    <tr><td>" . AL_TBL_WRITE_LOG_NOTICE . "</td><td>" . $this->l->select_yesno( "WRITE_LOG_NOTICE", WRITE_LOG_NOTICE ) . "</td></tr>
    <tr><td>" . AL_TBL_STOP_ON_ERROR . "</td><td>" . $this->l->select_yesno( "STOP_ON_ERROR", STOP_ON_ERROR ) . "</td></tr>


    <tr><td colspan='2'>" . AL_TBL_DEFAULT_PAGE_SETTINGS . "</td></tr>
        <tr><td>" . AL_TBL_MAINTENANCE_STYLE . "</td><td>" .
		            $this->l->select( "maintenance_style" );
		foreach ( $maintenance_styles as $maintenance_style ) {
			$content .= "<option value='" . $maintenance_style . "/'";
			if ( $maintenance_style . "/" == $GLOBALS['maintenance_style'] ) {
				$content .= " selected=selected";
			}
			$content .= ">" . $maintenance_style . "</option>\n";
		}

		$content .= "            </select></td></tr>
    <tr><td>" . AL_TBL_PHP_TIMEZONE . "</td><td>" .
		            $this->l->select( "php_timezone" ) .
		            $this->l->select_timezone_options( $GLOBALS["date_default_timezone"] ) .
		            "</select></td></tr>
    <tr><td>" . AL_TBL_BODY_CLASS . "</td><td>" . $this->l->text( "body_class", $GLOBALS['body_class'], "400" ) . "</td></tr>
    <tr><td>" . AL_TBL_PAGE_TITLE . "</td><td>" . $this->l->text( "page_title", $GLOBALS['page_title'], "400" ) . "</td></tr>
    <tr><td>" . AL_TBL_PAGE_SUBTITLE . "</td><td>" . $this->l->text( "page_subtitle", $GLOBALS['page_subtitle'], "400" ) . "</td></tr>
    <tr><td>" . AL_TBL_TITLE_DIVIDER . "</td><td>" . $this->l->text( "page_title_divider", $GLOBALS['page_title_divider'], "400" ) . "</td></tr>
    <tr><td>" . AL_TBL_DESCRIPTION . "</td><td>" . $this->l->ta_char_counter( "page_description", $GLOBALS['page_description'], "400", "100", EMPTY_FREE_TEXT, AUTO_ID, META_DESCRIPTION_MAX_CHARS ) . "</td></tr>
    <tr><td>" . AL_TBL_KEYWORDS . "</td><td>" . $this->l->text_char_counter( "page_keywords", $GLOBALS['page_keywords'], "400", EMPTY_FREE_TEXT, AUTO_ID, META_KEYWORDS_MAX_CHARS ) . "</td></tr>
    <tr><td>" . AL_TBL_DATE . "</td><td>" . $this->l->text( "default_datetime_format[date]", $GLOBALS['default_datetime_format_file']['date'], "200" ) . "</td></tr>
    <tr><td>" . AL_TBL_DATE_TIME . "</td><td>" . $this->l->text( "default_datetime_format[datetime]", $GLOBALS['default_datetime_format_file']['datetime'], "200" ) . "</td></tr>
    <tr><td>" . AL_TBL_DATE_TIME_SECONDS . "</td><td>" . $this->l->text( "default_datetime_format[datetime_seconds]", $GLOBALS['default_datetime_format_file']['datetime_seconds'], "200" ) . "</td></tr>
    <tr><td>" . AL_TBL_DAY_MONTH . "</td><td>" . $this->l->text( "default_datetime_format[month_day]", $GLOBALS['default_datetime_format_file']['month_day'], "200" ) . "</td></tr>
    <tr><td>" . AL_TBL_YEAR . "</td><td>" . $this->l->text( "default_datetime_format[year]", $GLOBALS['default_datetime_format_file']['year'], "200" ) . "</td></tr>
    <tr><td>" . AL_TBL_TIME . "</td><td>" . $this->l->text( "default_datetime_format[time]", $GLOBALS['default_datetime_format_file']['time'], "200" ) . "</td></tr>
    <tr><td>" . AL_TBL_TIME_SECONDS . "</td><td>" . $this->l->text( "default_datetime_format[time_seconds]", $GLOBALS['default_datetime_format_file']['time_seconds'], "200" ) . "</td></tr>
    <tr><td>" . AL_TBL_HOUR . "</td><td>" . $this->l->text( "default_datetime_format[hour]", $GLOBALS['default_datetime_format_file']['hour'], "200" ) . "</td></tr>
    <tr><td>" . AL_TBL_MONTH_YEAR . "</td><td>" . $this->l->text( "default_datetime_format[month_year]", $GLOBALS['default_datetime_format_file']['month_year'], "200" ) . "</td></tr>
    <tr><td></td><td>" . $this->l->submit( AL_SAVE ) . "</td></tr>
    </table></form>
    ";

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_DATE_TIME_SETTINGS, "info", AL_INFORMATION );

		$this->l->display_preview();

		return $content;
	}

	public function update() {


		if ( ! $this->l->get_preview_status() ) {
			$elements                            = array();
			$elements['CHECK_BROWSER']           = $this->p->get( "CHECK_BROWSER" );
			$elements['PAGE_CREATE_DATE']        = $this->p->get( "PAGE_CREATE_DATE" );
			$elements['LOAD_CSS_LANGUAGE_FILES'] = $this->p->get( "LOAD_CSS_LANGUAGE_FILES" );
			$elements['LOAD_JS_LANGUAGE_FILES']  = $this->p->get( "LOAD_JS_LANGUAGE_FILES" );


			foreach ( $elements as $key => $value ) {
				$this->db->upd( __FILE__ . ":" . __LINE__, "settings", array( "value" ), array( $value ), "UPPER(`key`) LIKE UPPER('$key')" );
				$this->log->event( "log", __FILE__ . ":" . __LINE__, "Settings updated, key: " . $key . ", value: " . $value );
			}
		}
		$_SESSION['options_saved'] = 1;

		$this->l->reload_js();
	}

	public function update_files() {


		if ( ! isset( $GLOBALS['page_is_online'] ) ) {
			;
		}
		$GLOBALS['page_is_online'] = 1;

		if ( ! $this->l->get_preview_status() ) {

			$admin_mail = $this->p->get( "ADMIN_MAIL", "", NOT_ESCAPED );
			$admin_mail = $this->p->fix_html_attributes( $admin_mail );

			$mail_from = $this->p->get( "MAIL_FROM", "", NOT_ESCAPED );
			$mail_from = $this->p->fix_html_attributes( $mail_from );

			$mail_from_name = $this->p->get( "MAIL_FROM_NAME", "", NOT_ESCAPED );
			$mail_from_name = $this->p->fix_html_attributes( $mail_from_name );

			$send_bcc = $this->p->get( "SEND_BCC", false );
			$send_bcc ? $send_bcc = "1" : $send_bcc = "0";

			$mail_bcc = $this->p->get( "MAIL_BCC", "", NOT_ESCAPED );
			$mail_bcc = $this->p->fix_html_attributes( $mail_bcc );

			$mail_bcc_name = $this->p->get( "MAIL_BCC_NAME", "", NOT_ESCAPED );
			$mail_bcc_name = $this->p->fix_html_attributes( $mail_bcc_name );

			$old_webroot = WEBROOT;
			$webroot     = $this->p->get( "WEBROOT", "", NOT_ESCAPED );
			$webroot     = $this->p->fix_html_attributes( $webroot );

			$file_size_for_archive_in_mb = $this->p->get( "FILE_SIZE_FOR_ARCHIVE_IN_MB", false );
			$file_size_for_archive_in_mb = intval( $file_size_for_archive_in_mb );
			//todo: check for value 0 in log class, should not create archive
			if ( ! $file_size_for_archive_in_mb ) {
				$file_size_for_archive_in_mb = 0.5;
			}

			$old_admindir = ADMINDIR;
			$admin_dir    = $this->p->get( "ADMINDIR", "", NOT_ESCAPED );
			$admin_dir    = $this->p->fix_html_attributes( $admin_dir );

			$uploaddir = $this->p->get( "UPLOADDIR", "", NOT_ESCAPED );
			$uploaddir = $this->p->fix_html_attributes( $uploaddir );

			$downloaddir = $this->p->get( "DOWNLOADDIR", "", NOT_ESCAPED );
			$downloaddir = $this->p->fix_html_attributes( $downloaddir );

			$no_count_code = $this->p->get( "NO_COUNT_CODE", "" );
			$no_count_code = $this->p->fix_html_attributes( $no_count_code );

			$only_https = $this->p->get( "ONLY_HTTPS", false );
			$only_https ? $only_https = "1" : $only_https = "0";

			$enable_tab_in_textareas = $this->p->get( "ENABLE_TAB_IN_TEXTAREAS", false );
			$enable_tab_in_textareas ? $enable_tab_in_textareas = "1" : $enable_tab_in_textareas = "0";

			$do_debug = $this->p->get( "DO_DEBUG", false );
			$do_debug ? $do_debug = "1" : $do_debug = "0";

			$write_log = $this->p->get( "WRITE_LOG", false );
			$write_log ? $write_log = "1" : $write_log = "0";

			$write_log_notice = $this->p->get( "WRITE_LOG_NOTICE", false );
			$write_log_notice ? $write_log_notice = "1" : $write_log_notice = "0";

			$stop_on_error = $this->p->get( "STOP_ON_ERROR", false );
			$stop_on_error ? $stop_on_error = "1" : $stop_on_error = "0";

			$maintenance_style = $this->p->get( "maintenance_style", "default/", NOT_ESCAPED );
			$maintenance_style = $this->p->fix_html_attributes( $maintenance_style );

			$php_timezone = $this->p->get( "php_timezone", "", NOT_ESCAPED );
			$php_timezone = $this->p->fix_html_attributes( $php_timezone );

			$admin_body_class = $this->p->get( "body_class", "", NOT_ESCAPED );
			$admin_body_class = $this->p->fix_html_attributes( $admin_body_class );

			$page_title = $this->p->get( "page_title", "", NOT_ESCAPED );
			$page_title = $this->p->fix_html_attributes( $page_title );

			$page_subtitle = $this->p->get( "page_subtitle", "", NOT_ESCAPED );
			$page_subtitle = $this->p->fix_html_attributes( $page_subtitle );

			$title_divider = $this->p->get( "page_title_divider", "", NOT_ESCAPED );
			$title_divider = $this->p->fix_html_attributes( $title_divider );

			$description = $this->p->get( "page_description", "", NOT_ESCAPED );
			$description = $this->p->fix_html_attributes( $description );

			$keywords = $this->p->get( "page_keywords", "", NOT_ESCAPED );
			$keywords = $this->p->fix_html_attributes( $keywords );

			$datetime_format = $this->p->get( "default_datetime_format", false, NOT_ESCAPED ); // Array

			$settings_file_content = "[date_default_timezone_set]
date_default_timezone_set = \"$php_timezone\"

[constants]
ADMIN_MAIL = \"$admin_mail\"
MAIL_FROM = \"$mail_from\"
MAIL_FROM_NAME = \"$mail_from_name\"
SEND_BCC = $send_bcc
MAIL_BCC = \"$mail_bcc\"
MAIL_BCC_NAME = \"$mail_bcc_name\"
WEBROOT = \"$webroot\"
FILE_SIZE_FOR_ARCHIVE_IN_MB = \"$file_size_for_archive_in_mb\"
ADMINDIR = \"$admin_dir\"
UPLOADDIR = \"$uploaddir\"
DOWNLOADDIR = \"$downloaddir\"
NO_COUNT_CODE = \"$no_count_code\"
ONLY_HTTPS = $only_https
ENABLE_TAB_IN_TEXTAREAS = $enable_tab_in_textareas
WRITE_LOG = $write_log
WRITE_LOG_NOTICE = $write_log_notice
STOP_ON_ERROR = $stop_on_error
    WAIT_TIME_FOR_REVISIT = " . WAIT_TIME_FOR_REVISIT . "
    HIDE_THIS_VARS_IN_LOG = \"" . HIDE_THIS_VARS_IN_LOG . "\"
    LOG_ADMIN_PREFIX = \"" . LOG_ADMIN_PREFIX . "\"
    PREVIEW_MODE = " . PREVIEW_MODE . "
    PREVIEW_MODE_RESET_DB = " . PREVIEW_MODE_RESET_DB . "
    PREVIEW_MODE_RESET_DB_TIME = \"" . PREVIEW_MODE_RESET_DB_TIME . "\"
    IP_SALT = \"" . IP_SALT . "\"

[global_variables]
maintenance_style = \"$maintenance_style\"
body_class = \"$admin_body_class\"
page_title = \"$page_title\"
page_subtitle = \"$page_subtitle\"
page_title_divider = \"$title_divider\"
page_description = \"$description\"
page_keywords = \"$keywords\"
default_datetime_format[date] = \"" . $datetime_format['date'] . "\"
default_datetime_format[datetime] = \"" . $datetime_format['datetime'] . "\"
default_datetime_format[datetime_seconds] = \"" . $datetime_format['datetime_seconds'] . "\"
default_datetime_format[month_day] = \"" . $datetime_format['month_day'] . "\"
default_datetime_format[year] = \"" . $datetime_format['year'] . "\"
default_datetime_format[time] = \"" . $datetime_format['time'] . "\"
default_datetime_format[time_seconds] = \"" . $datetime_format['time_seconds'] . "\"
default_datetime_format[hour] = \"" . $datetime_format['hour'] . "\"
default_datetime_format[month_year] = \"" . $datetime_format['month_year'] . "\"
    page_is_online = " . $GLOBALS['page_is_online'] . "";

			$debug_file_content = "DO_DEBUG = " . $do_debug;

			if ( $webroot != $old_webroot ) {
				$htaccess_file_content = "Errordocument 401 /" . $webroot . "__functions/maintenance/maintenance.php
Errordocument 404 /" . $webroot . "__functions/maintenance/maintenance.php
Errordocument 500 /" . $webroot . "__functions/maintenance/maintenance.php

SetEnv HTTP_MOD_REWRITE On
RewriteEngine On
RewriteBase /" . $webroot . "
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /" . $webroot . "index.php [L]";


				$htaccess_file = fopen( DIRROOT . ".htaccess", "w" );

				if ( $htaccess_file === false ) {
					$this->log->error( "php", __FILE__ . ":" . __LINE__, "Einstellungen in .htaccess konnten nicht aktualisiert werden. Konnte Datei nicht zum schreiben öffnen." );
					$_SESSION['htaccess_error'] = 1;
					$this->l->reload_js();
				} else {
					$status = fwrite( $htaccess_file, $htaccess_file_content );

					if ( $status === false ) {
						fclose( $htaccess_file );
						$this->log->error( "php", __FILE__ . ":" . __LINE__, "Einstellungen in .htaccess konnten nicht aktualisiert werden. Konnte nicht in Datei schreiben." );
						$_SESSION['htaccess_error'] = 1;
						$this->l->reload_js();
					} else {
						fclose( $htaccess_file );
						$this->log->event( "php", __FILE__ . ":" . __LINE__, "Einstellungen in .htaccess aktualisiert." );
						$_SESSION['htaccess_saved'] = 1;
					}
				}
			}

			if ( $webroot != $old_webroot || $admin_dir != $old_admindir ) {
				$htaccess_file_content = "Errordocument 401 /" . $webroot . "__functions/maintenance/maintenance.php
Errordocument 404 /" . $webroot . $admin_dir . "__functions/maintenance/maintenance.php
Errordocument 500 /" . $webroot . $admin_dir . "__functions/maintenance/maintenance.php

SetEnv HTTP_MOD_REWRITE On
RewriteEngine On
RewriteBase /" . $webroot . $admin_dir . "
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /" . $webroot . $admin_dir . "index.php [L]";


				$htaccess_file = fopen( ADMINDIRROOT . ".htaccess", "w" );

				if ( $htaccess_file === false ) {
					$this->log->error( "php", __FILE__ . ":" . __LINE__, "Einstellungen in admin .htaccess konnten nicht aktualisiert werden. Konnte Datei nicht zum schreiben öffnen." );
					$_SESSION['htaccess_error'] = 1;
					$this->l->reload_js();
				} else {
					$status = fwrite( $htaccess_file, $htaccess_file_content );

					if ( $status === false ) {
						fclose( $htaccess_file );
						$this->log->error( "php", __FILE__ . ":" . __LINE__, "Einstellungen in admin .htaccess konnten nicht aktualisiert werden. Konnte nicht in Datei schreiben." );
						$_SESSION['htaccess_error'] = 1;
						$this->l->reload_js();
					} else {
						fclose( $htaccess_file );
						$this->log->event( "php", __FILE__ . ":" . __LINE__, "Einstellungen in admin .htaccess aktualisiert." );
						$_SESSION['htaccess_saved'] = 1;
					}
				}
			}

			$settings_file = fopen( DIRROOT . "settings/settings_website.ini", "w" );

			if ( $settings_file === false ) {
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Einstellungen in __custom/settings.ini konnten nicht aktualisiert werden. Konnte Datei nicht zum schreiben öffnen." );
				$_SESSION['settings_error'] = 1;
				$this->l->reload_js();
			} else {
				$status = fwrite( $settings_file, $settings_file_content );

				if ( $status === false ) {
					fclose( $settings_file );
					$this->log->error( "php", __FILE__ . ":" . __LINE__, "Einstellungen in __custom/settings.ini konnten nicht aktualisiert werden. Konnte nicht in Datei schreiben." );
					$_SESSION['settings_error'] = 1;
					$this->l->reload_js();
				} else {
					fclose( $settings_file );
					$this->log->event( "php", __FILE__ . ":" . __LINE__, "Einstellungen in __custom/settings.ini aktualisiert." );
					$_SESSION['settings_saved'] = 1;
				}
			}

			$debug_file = fopen( DIRROOT . "settings/settings_debug.ini", "w" );

			if ( $debug_file === false ) {
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "Einstellungen in __custom/debug.ini konnten nicht aktualisiert werden. Konnte Datei nicht zum schreiben öffnen." );
				$_SESSION['debug_error'] = 1;
				$this->l->reload_js();
			} else {
				$status = fwrite( $debug_file, $debug_file_content );

				if ( $status === false ) {
					fclose( $debug_file );
					$this->log->error( "php", __FILE__ . ":" . __LINE__, "Einstellungen in __custom/debug.ini konnten nicht aktualisiert werden. Konnte nicht in Datei schreiben." );
					$_SESSION['debug_error'] = 1;
					$this->l->reload_js();
				} else {
					fclose( $debug_file );
					$this->log->event( "php", __FILE__ . ":" . __LINE__, "Einstellungen in __custom/debug.ini aktualisiert." );
					$_SESSION['debug_saved'] = 1;

					$this->l->reload_js(); // All Finished
				}
			}
		} else {
			$this->l->reload_js();
		}
	}

}

$class_settings_main = new class_settings_main ();
$content .= $class_settings_main->get_content();