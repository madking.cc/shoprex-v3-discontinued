<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_SYSTEM_FILES;

class class_settings_special_files extends class_sys {
	public $content;

	public function __construct() {

		parent::__construct();

		$action = $this->p->get( "do" );

		switch ( $action ) {
			case "update_robots":
				$this->update_robots();
				break;
			default:
				$this->content = $this->start();
				break;
		}
	}

	public function get_content() {
		return $this->content;
	}


	public function start() {
		$content = "";

		$content .= $this->l->display_message_by_session( 'options_robotstxt_saved', AL_ROBOTSTXT_UPDATED );
		$content .= $this->l->display_message_by_session( 'options_robotstxt_error', AL_ERROR_ROBOTSTXT_CANT_UPDATE, "danger" );

		$content .= "<h3 class='underline'>" . AL_ROBOTSTXT_COLON . "</h3>\n";

		$robotstxt_file    = $this->p->get_content( "robots.txt" );
		$robotstxt_content = "";
		if ( $robotstxt_file["exists"] == true ) {
			foreach ( $robotstxt_file["content"] as $zeile ) {
				$robotstxt_content .= $zeile;
			}
		}

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "update_robots" ) . $this->l->table() . "
    <tr><td>" . $this->l->ta( "robotstxt_content", $robotstxt_content, 500, 350 ) . "</td></tr>
    <tr><td>" . $this->l->submit( AL_SAVE ) . "</td></tr>
    </table></form>
    ";

		$this->l->display_preview();

		return $content;
	}

	public function update_robots() {


		if ( ! $this->l->get_preview_status() ) {
			$robotstxt_content = $this->p->get( "robotstxt_content", "", false );

			$content_file = $robotstxt_content;

			$datei = fopen( DIRROOT . "robots.txt", "wb" );

			if ( $datei === false ) {
				$this->log->error( "php", __FILE__ . ":" . __LINE__, "robots.txt konnte nicht aktualisiert werden. Konnte Datei nicht zum schreiben öffnen." );
				$_SESSION['options_robotstxt_error'] = 1;
				$this->l->reload_js();
			} else {
				$status = fwrite( $datei, $content_file );

				if ( $status === false ) {
					$this->log->error( "php", __FILE__ . ":" . __LINE__, "robots.txt konnte nicht aktualisiert werden. Konnte nicht in Datei schreiben." );
					$_SESSION['options_robotstxt_error'] = 1;
					$this->l->reload_js();
				} else {
					fclose( $datei );
					$this->log->event( "php", __FILE__ . ":" . __LINE__, "robots.txt aktualisiert." );
					$_SESSION['options_robotstxt_saved'] = 1;
					$this->l->reload_js();
				}
			}
		} else {
			$this->l->reload_js();
		}
	}

}

$class_settings_special_files = new class_settings_special_files();
$content .= $class_settings_special_files->get_content();