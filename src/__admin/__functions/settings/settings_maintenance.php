<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_MAINTENANCE;

class class_settings_maintenance extends class_sys {
	public $content;

	public function __construct() {

		parent::__construct();

		$action = $this->p->get( "do" );

		switch ( $action ) {
			case "reset_statistics":
				$this->reset_statistics(); //
				break;
			case "reset_log":
				$this->reset_log(); //
				break;
			case "clean_db":
				$this->clean_db(); //
				break;
			case "offline":
				$this->offline(); //
				break;
			case "clean_images_db":
				$this->clean_images_db(); //
				break;
			case "clean_images_filesystem":
				$this->clean_images_filesystem(); //
				break;
			case "delete_images":
				$this->clear_deleted_folder(); //
				break;
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function get_log_size() {


		$files = $this->p->get_files( $GLOBALS['log_d'] );

		$size = 0;
		if ( sizeof( $files ) > 0 ) {
			foreach ( $files as $file ) {
				$size += ( filesize( DIRROOT . $GLOBALS['log_d'] . $file ) / 1024 / 1024 );
			}
		}

		return $size;
	}

	public function clean_images_db_with_filesystem( $action = "get_count" ) {
		if ( $this->img->store_in_db ) {

			$images_counter_not_in_sync = 0;
			$images_counter_fixed       = 0;

			foreach ( $this->p->images_upload_dirs as $path ) {
				$sql    = "SELECT * FROM `" . TP . "images_resize_helper` WHERE `deleted`='0' AND `uploaddir` LIKE BINARY '$path'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				if ( $result->num_rows > 0 ) {
					while ( $row = $result->fetch_assoc() ) {
						// If File exists in db but not in filesystem
						if ( ! is_file( DIRROOT . $path . $row['filename_copy'] ) ) {
							$images_counter_not_in_sync ++;
							if ( $action == "delete" ) {
								$sql = "UPDATE `" . TP . "images_resize_helper` SET `deleted`='1' WHERE `id`='" . $row['id'] . "' LIMIT 1";
								$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
								$images_counter_fixed ++;
							}
						}
					}
				}
			}

			if ( $action == "delete" ) {
				return $images_counter_fixed;
			} else {
				return $images_counter_not_in_sync;
			}
		} else {
			return 0;
		}
	}

	public function clean_images_filesystem_with_db( $action = "get_count" ) {
		if ( $this->img->store_in_db ) {

			$images_counter_not_in_sync = 0;
			$images_counter_fixed       = 0;

			foreach ( $this->p->images_upload_dirs as $path ) {
				// Get all Files
				$files = $this->p->get_files( $path );
				if ( sizeof( $files ) > 0 ) {
					foreach ( $files as $file ) {
						$sql    = "SELECT * FROM `" . TP . "images_resize_helper` WHERE `deleted`='0' AND `uploaddir` LIKE BINARY '$path' AND `filename_copy` LIKE BINARY '$file'";
						$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

						// If File does not exist in db, delete it
						if ( $result->num_rows == 0 ) {
							$images_counter_not_in_sync ++;
							if ( $action == "delete" ) {
								if ( is_file( DIRROOT . $path . $file ) ) {
									$tmp            = $file;
									$file_name_move = $this->p->check_filename( $path . "delete/" . $file );
									if ( $file_name_move == false ) {
										$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$tmp'" );
									} else {

										$result = rename( DIRROOT . $path . $file, DIRROOT . $path . "delete/" . $file_name_move );
										if ( $result === false ) {
											$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file from: '" . DIRROOT . $path . $file . "' to '" . DIRROOT . $path . "delete/" . $file_name_move . "'" );
										} else {
											$images_counter_fixed ++;
										}
									}
								} else {
									$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't delete, because it's not a file: '" . DIRROOT . $path . $file . "'" );
								}
							}
						}
					}
				}

				// Get all Original Files
				$files = $this->p->get_files( $path . "orig/" );
				if ( sizeof( $files ) > 0 ) {
					foreach ( $files as $file ) {

						$sql    = "SELECT * FROM `" . TP . "images_resize_helper` WHERE `deleted`='0' AND `uploaddir` LIKE BINARY '$path' AND `filename_original` LIKE BINARY '$file'";
						$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

						// If File does not exist in db, delete it
						if ( $result->num_rows == 0 ) {
							$images_counter_not_in_sync ++;
							if ( $action == "delete" ) {
								if ( is_file( DIRROOT . $path . "orig/" . $file ) ) {
									$tmp            = $file;
									$file_name_move = $this->p->check_filename( $path . "orig/delete/" . $file );
									if ( $file_name_move == false ) {
										$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$tmp'" );
									} else {

										$result = rename( DIRROOT . $path . "orig/" . $file, DIRROOT . $path . "orig/delete/" . $file_name_move );
										if ( $result === false ) {
											$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file from: '" . DIRROOT . $path . "orig/" . $file . "' to '" . DIRROOT . $path . "orig/delete/" . $file_name_move . "'" );
										} else {
											$images_counter_fixed ++;
										}
									}
								} else {
									$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't delete, because it's not a file: '" . DIRROOT . $path . "orig/" . $file . "'" );
								}
							}
						}
					}
				}
			}
			if ( $action == "delete" ) {
				return $images_counter_fixed;
			} else {
				return $images_counter_not_in_sync;
			}
		} else {
			return 0;
		}
	}

	public function clear_delete_dirs( $action = "get_count" ) {


		$file_counter          = array();
		$file_counter['count'] = 0;
		$file_counter['size']  = 0;

		foreach ( $this->p->delete_dirs as $path ) {
			$files = $this->p->get_files( $path );

			if ( sizeof( $files ) > 0 ) {
				foreach ( $files as $file ) {
					$file_counter['count'] ++;
					$file_counter['size'] += ( filesize( DIRROOT . $path . $file ) / 1024 / 1024 );
					if ( $action == "delete" ) {
						if ( is_file( DIRROOT . $path . $file ) ) {
							$result = unlink( DIRROOT . $path . $file );
							if ( $result === false ) {
								$file_counter['count'] --;
								$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't delete file: '" . DIRROOT . $path . $file . "'" );
							}
						} else {
							$file_counter['count'] --;
							$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't delete, because it's not a file: '" . DIRROOT . $path . $file . "'" );
						}
					}
				}
			}
		}

		return $file_counter;
	}

	public function start() {
		$content = "";

		$content .= "<h3 class='underline'>" . AL_MAINTENANCE . "</h3>\n";

		$action_done = "";
		if ( isset( $_SESSION['actions_done'] ) ) {
			$action_done = $_SESSION['actions_done'];
			unset( $_SESSION['actions_done'] );
		}

		switch ( $action_done ) {
			case "log":
				$content .= $this->l->alert_text_dismiss( "success", AL_LOG_FILES_DELETED );
				break;
			case "statistics":
				$content .= $this->l->alert_text_dismiss( "success", AL_STATISTICS_RESET );
				break;
			case "statistics_files":
				$content .= $this->l->alert_text_dismiss( "success", AL_FILESTATISTICS_RESET );
				break;
			case "clean_db":
				$content .= $this->l->alert_text_dismiss( "success", AL_DATABASE_CLEANED_UP );
				break;
			case "offline":
				$content .= $this->l->alert_text_dismiss( "success", AL_OFFLINE_SETTINGS_SAVED );
				break;
			case "images":
				$content .= $this->l->alert_text_dismiss( "success", AL_IMAGES_ADJUSTED );
				break;
			case "delete_images":
				$content .= $this->l->alert_text_dismiss( "success", AL_IMAGES_FINALLY_DELETE );
				break;
			default:
				break;
		}

		$result          = $this->db->get_tables( __FILE__ . ":" . __LINE__ );
		$deleted_counter = 0;

		while ( $row = $result->fetch_assoc() ) {
			$table_has_deleted = $this->db->exists_column( $row[ "Tables_in_" . DB_TABLE . " (" . TP . "%)" ], "deleted" );

			if ( $table_has_deleted ) {
				$sql     = "SELECT id FROM " . $row[ "Tables_in_" . DB_TABLE . " (" . TP . "%)" ] . " WHERE `deleted` = '1'";
				$result2 = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				$deleted_counter += $result2->num_rows;
			}
		}

		$images_counter_exits_in_db_but_not_in_filesystem  = $this->clean_images_db_with_filesystem();
		$images_counter_exists_in_filesystem_but_not_in_db = $this->clean_images_filesystem_with_db();
		$images_counter_in_delete_folder                   = $this->clear_delete_dirs();
		$log_size                                          = $this->get_log_size();

		$content .= $this->l->table() . "
        <tr class='tr-v-align'>
        <td>" . AL_TBL_STATISTICS . "</td>
        <td>" . $this->l->form_admin( "id='form_statistics'" ) . $this->l->hidden( "do", "reset_statistics" ) . $this->l->button( AL_RESET_STATISTICS, "id='button_statistics'" ) . "</form></td><td>
        " . AL_STATISTICS_RECORDED_SINCE_PART01 . " " . STATISTICS_START_DATE . " " . AL_STATISTICS_RECORDED_SINCE_PART02 . "</td>
        </tr>
        <tr class='tr-v-align'>
        <td>" . AL_TBL_LOG_FILES . "</td><td colspan=\"1\">" . $this->l->form_admin( "id='form_log'" ) . $this->l->hidden( "do", "reset_log" ) . $this->l->button( AL_DELETE_ALL_LOG_FILES, "id='button_log'" ) . "</form></td><td>" . AL_TBL_SPACE_USED . " " . ( number_format( $log_size, 2, ",", "" ) ) . " " . AL_MEGABYTE . "</td>
        </tr>
";

		if ( $this->img->store_in_db ) {
			$content .= "
        <tr class='tr-v-align'>
            <td rowspan='2'>" . AL_TBL_DATABASE_PICTURES . "</td>
                <td>" . $this->l->form_admin( "id='form_images_clean_db'" ) .
			            $this->l->hidden( "do", "clean_images_db" ) .
			            $this->l->button( AL_CLEAN_IMAGES_IN_DATABASE, "id='button_images_clean_db'" ) . "</form></td><td>
                " . AL_TBL_NOT_LINKED_DATABASE_IMAGES_WITH_FILE_SYSTEM . " " . ( $images_counter_exits_in_db_but_not_in_filesystem ) . "</td></tr>
        <tr class='tr-v-align'>
            <td>" . $this->l->form_admin( "id='form_images_clean_filesystem'" ) .
			            $this->l->hidden( "do", "clean_images_filesystem" ) .
			            $this->l->button( AL_CLEAN_IMAGES_IN_FILESYSTEM_CAUTION_SIGN, "id='button_images_clean_filesystem'" ) . "</form></td><td>
        " . AL_TBL_NOT_LINKED_FILESYSTEM_IMAGES_WITH_DATABASE . " " . ( $images_counter_exists_in_filesystem_but_not_in_db ) . "</td></tr>\n";
		}

		$content .= "
        <tr class='tr-v-align'>
        <td>" . AL_TBL_DATABASE . "</td><td>" . $this->l->form_admin( "id='form_db_clean'" ) . $this->l->hidden( "do", "clean_db" ) . $this->l->button( AL_CLEAN_DATABASE, "id='button_db_clean'" ) . "</form></td><td>
        " . AL_TBL_ORPHANED_ENTRIES . " $deleted_counter</td>
        </tr>
        <tr class='tr-v-align'>
        <td>" . AL_TBL_PICTURES . "</td><td>" . $this->l->button( AL_MEDIA_MANAGER, "id='open-popup-manager'" ) . "</td><td>" . AL_HINT_PICTURES_FROM_TEXT_DELETE . "</td></tr>


        <tr class='tr-v-align'>
        <td>" . AL_TBL_FILES_DELETE_FOLDER . "</td><td>" . $this->l->form_admin( "id='form_images_delete'" ) . $this->l->hidden( "do", "delete_images" ) .
		            $this->l->button( AL_DELETE_FILES_PERMANENTLY, "id='button_delete_images'" ) . "</form></td><td>
        " . AL_TBL_NUMBER_OF_FILES . " " . $images_counter_in_delete_folder['count'] . ", " . AL_TBL_SPACE_USED . " " . ( number_format( $images_counter_in_delete_folder['size'], 2, ",", "" ) ) . " " . AL_MEGABYTE . "</td>
        </tr>
        </table>";

		$this->l->box_confirm( AL_RESET_QUE, AL_RESET_STATISTICS_QUE, "#button_statistics", "form_statistics", "dialog_statistics" );

		$this->l->box_confirm( AL_RESET_QUE, AL_RESET_FILE_STATISTICS_QUE, "#button_statistics_files", "form_statistics_files", "dialog_statistics_files" );

		$this->l->box_confirm( AL_DELETE_QUESTION, AL_REMOVE_ALL_LOG_FILES_QUE, "#button_log", "form_log", "dialog_log" );

		$this->l->box_confirm( AL_CLEAN_UP_QUE, AL_CLEAN_UP_DATABASE_QUE, "#button_db_clean", "form_db_clean", "dialog_db_clean" );

		$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_UNLINKED_DATABASE_IMAGES_QUE, "#button_images_clean_db", "form_images_clean_db", "dialog_images_clean_db" );
		$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_UNLINKED_LOCAL_IMAGES_QUE, "#button_images_clean_filesystem", "form_images_clean_filesystem", "dialog_images_clean_filesystem" );

		$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_PICTURES_IN_DELETE_FOLDERS_QUE, "#button_delete_images", "form_images_delete", "dialog_images_delete" );

		$GLOBALS['body_footer'] .= "<script>
    $('#open-popup-manager').click(function() {
        $.colorbox({href:'/" . WEBROOT . ADMINDIR . "upload_manager.php', iframe:true, width:'90%', height:'90%' });
    });
</script>
";

		$content .= "<h3 class='underline'>" . AL_SET_PAGE_OFFLINE . "</h3>
";

		$pages        = "";
		$sql          = "SELECT * FROM `" . TP . "pages` WHERE `deleted`='0' ORDER BY `name`";
		$result_pages = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result_pages->num_rows > 0 ) {
			$pages .= $this->l->select( "page_id" );

			$selected_page = OFFLINE_SHOW_PAGE;

			$found = false;
			while ( $row_pages = $result_pages->fetch_assoc() ) {
				$row_pages['active'] ? $active = AL_ACTIVE : $active = AL_INACTIVE;
				$pages .= "<option value='" . $row_pages['id'] . "'";
				$selected_page == $row_pages['id'] ? $pages .= " selected='selected'" : $pages .= "";
				$pages .= ">" . $row_pages['name'] . " - $active</option>\n";
				if ( $selected_page > 0 ) {
					if ( $selected_page == $row_pages['id'] ) {
						$found = true;
					}
				}
			}
			if ( ! $found && $selected_page ) {
				$pages .= "<option value='0' selected='selected'>" . AL_ENTRY_NO_LONGER_VALID . "</option>\n";
			}

			$pages .= "</select>";
		} else {
			$pages .= AL_NO_DEFINED_PAGED . $this->l->hidden( "page_id", "0" );
		}

		$content .= $this->l->form_admin( "id='form_statistics'" ) . $this->l->hidden( "do", "offline" ) . $this->l->table() . "
        <tr class='tr-v-align'>
        <td>" . AL_TBL_OFFLINE . "</td>
        <td>" . $this->l->select_yesno( "is_offline", PAGE_IS_OFFLINE ) . "
        </td>
        </tr>
        <tr class='tr-v-align'>
        <td>" . AL_TBL_SHOW_LAYOUT . "</td>
        <td>" . $this->l->select_yesno( "show_layout", OFFLINE_SHOW_LAYOUT ) . "
        </td>
        </tr>
        <tr class='tr-v-align'>
        <td>" . AL_TBL_SHOW_PAGE . "</td>
        <td>" . $pages . "</td>
        </tr>
        <tr>
        <td></td><td>" . $this->l->submit( AL_SAVE ) . "</td>
        </tr>
        </table></form>";

		$this->l->display_preview();

		return $content;
	}

	public function clean_images_db() //
	{


		$this->clean_images_db_with_filesystem( "delete" );
		$_SESSION['actions_done'] = "images";

		$this->l->reload_js();
	}

	public function clean_images_filesystem() //
	{


		$this->clean_images_filesystem_with_db( "delete" );

		$_SESSION['actions_done'] = "images";

		$this->l->reload_js();
	}

	public function clear_deleted_folder() //
	{


		$this->clear_delete_dirs( "delete" );

		$_SESSION['actions_done'] = "delete_images";

		$this->l->reload_js();
	}

	public function reset_statistics() //
	{


		if ( ! $this->l->get_preview_status() ) {
			$tables_to_clear = array(
				TP . "statistics_by_date",
				TP . "statistics_by_language",
				TP . "statistics_by_resolution",
				TP . "statistics_by_resolution_avail",
				TP . "statistics_by_system",
				TP . "statistics_by_windowsize",
				TP . "statistics_count",
				TP . "statistics_download",
				TP . "statistics_hits",
				TP . "statistics_not_compatible",
				TP . "statistics_not_found",
				TP . "statistics_page",
				TP . "statistics_referer",
				TP . "statistics_view_log",
				TP . "visitor_unique"
			);

			foreach ( $tables_to_clear as $value ) {
				$sql = "TRUNCATE TABLE `$value`";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			}

			$this->db->upd( __FILE__ . ":" . __LINE__, "file_download", array( "count" ), array( 0 ) );

			$date = date( "d.m.Y" );
			$this->db->upd( __FILE__ . ":" . __LINE__, "settings", array( "value" ), array( $date ), "`key` = 'statistics_start_date'" );
		}

		$_SESSION['actions_done'] = "statistics";

		$this->l->reload_js();
	}

	public function reset_log() //
	{


		if ( ! $this->l->get_preview_status() ) {
			$log_files = $this->p->get_files( $GLOBALS['log_d'], "log" );

			if ( is_array( $log_files ) && sizeof( $log_files ) > 0 ) {
				foreach ( $log_files as $value ) {
					unlink( DIRROOT . $GLOBALS['log_d'] . $value );
				}
			}

			$log_files = $this->p->get_files( $GLOBALS['log_d'], "zip" );

			if ( is_array( $log_files ) && sizeof( $log_files ) > 0 ) {
				foreach ( $log_files as $value ) {
					unlink( DIRROOT . $GLOBALS['log_d'] . $value );
				}
			}
		}

		$_SESSION['actions_done'] = "log";

		$this->l->reload_js();
	}

	public function clean_db() //
	{


		if ( ! $this->l->get_preview_status() ) {
			$result = $this->db->get_tables( __FILE__ . ":" . __LINE__ );

			while ( $row = $result->fetch_assoc() ) {
				$table_has_deleted = $this->db->exists_column( $row[ "Tables_in_" . DB_TABLE . " (" . TP . "%)" ], "deleted" );

				if ( $table_has_deleted ) {
					$sql = "DELETE FROM " . $row[ "Tables_in_" . DB_TABLE . " (" . TP . "%)" ] . " WHERE `deleted` = '1'";
					$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				}
			}
		}

		$_SESSION['actions_done'] = "clean_db";

		$this->l->reload_js();
	}

	public function offline() //
	{


		if ( ! $this->l->get_preview_status() ) {
			$elements                        = array();
			$elements['page_is_offline']     = $this->p->get( "is_offline" );
			$elements['offline_show_page']   = $this->p->get( "page_id" );
			$elements['offline_show_layout'] = $this->p->get( "show_layout" );

			foreach ( $elements as $key => $value ) {
				$this->db->upd( __FILE__ . ":" . __LINE__, "settings", array( "value" ), array( $value ), "UPPER(`key`) LIKE UPPER('$key')" );
				$this->log->event( "log", __FILE__ . ":" . __LINE__, "Settings updated, key: " . $key . ", value: " . $value );
			}
		}

		$_SESSION['actions_done'] = "offline";

		$this->l->reload_js();
	}

}

$class_settings_maintenance = new class_settings_maintenance ();
$content .= $class_settings_maintenance->get_content();