<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_SCRIPTS;

class class_settings_global_scripts extends class_sys {
	public $content;

	public function __construct( $action ) {

		parent::__construct();

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );

		switch ( $action ) {
			case "content_add":
				$this->content .= $this->content_add();
				break;
			case "content_save":
				$content_error = $this->content_save();
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add();
				}
				break;
			case "content_save_no_back":
				$content_error = $this->content_save( STAY_ON_PAGE );
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add();
				}

				break;
			case "content_delete":
				$this->content .= $this->content_delete();
				$this->content .= $this->start();
				break;
			case "content_edit":
				$this->content .= $this->content_add( EDIT );
				break;
			case "content_update":
				$content_error = $this->content_update();
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add( EDIT );
				}
				break;
			case "content_update_no_back":
				$content_error = $this->content_update( STAY_ON_PAGE );
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
				}
				$this->content .= $this->content_add( EDIT, SAME_PAGE );
				break;
			case "content_status":
				$this->content .= $this->content_status();
				$this->content .= $this->start();
				break;
			case "search":
			case "init":
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";


		$content .= $this->l->display_message_by_session( 'content_added', AL_SCRIPT_ADDED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_SCRIPT_UPDATED );

		$reset_search = $this->p->get( "reset_search", 0 );
		$search       = $this->p->get_session( "search", "", "text_search", $reset_search );
		$intext       = $this->p->get_session( "intext", 0, "text_intext", $reset_search );

		if ( empty( $search ) ) {
			$content .= "<h3 class='underline'>" . AL_SCRIPTS . "</h3>\n";
		} else {
			$content .= "<h3 class='underline'>" . AL_SCRIPTS . " " . AL_SEARCH_RESULT . "</h3>\n";
		}

		if ( empty( $search ) ) {
			$sql    = "SELECT * FROM `" . TP . "scripts` WHERE `deleted` = '0' ORDER BY `name`";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$search_text = "";
			if ( $intext ) {
				$search_text = "OR " . $this->db->search( "text", $search );
			}
			$sql    = "SELECT * FROM `" . TP . "scripts` WHERE (" . $this->db->search( "name", $search ) . " $search_text) AND `deleted` = '0' ORDER BY 'name'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		}

		$content .= $this->l->form_admin( "", "settings_global_scripts_add.php" ) . $this->l->submit( "Neues Script hinzufügen" ) . "</form>";

		if ( $result->num_rows > 0 || ! empty( $search ) ) {
			$content .= " " . $this->l->form_admin( "name='form_search'" ) . $this->l->text( "search", $search, "250", "id='search_text'" ) . $this->l->hidden( "do", "search" ) . " " . $this->l->submit( AL_SEARCH, "id='search'" ) . " <nobr>" . $this->l->checkbox( "intext", "1", $intext, "0", "style='display:inline'" ) . " " . AL_IN_TEXT . "</nobr>
                <span id='message' class='error'>| " . AL_ERROR . ": " . AL_MISSING_SEARCH_TEXT . "</span>\n";
			$content .= "    </form>\n";

			if ( ! empty( $search ) ) {
				$content .= $this->l->form_admin() . $this->l->hidden( "reset_search", "1" ) . $this->l->submit( AL_RESET );
				$content .= "    </form>\n";
			}

			$GLOBALS['body_footer'] .= "
        <script>

            $(document).ready(function () {
                $(\"#message\").hide();
            });
            $(\"form[name=form_search]\").submit(function( event ) {
                var val = $(\"input[name=search]\").val();
                if(val == '')
                {
                    $(\"#message\").fadeIn();
                    event.preventDefault();
                }
                else
                {
                    $(\"#message\").fadeOut();
                }
            });
             </script>\n";
		}

		if ( $result->num_rows != 0 ) {
			$handler = $this->tbl->tbl_init( "ID:", AL_TBL_NAME, AL_TBL_CONTENT, AL_TBL_CHANGED, AL_TBL_ACTIVE, array(
				AL_TBL_ACTION,
				"nosort"
			) );

			$entry_counter = 0;
			while ( $row = $result->fetch_assoc() ) {
				$text = $this->l->fix_ta( $row['text'] );
				$text = $this->l->ta( "text_" . $entry_counter, $text, "500", "180", "readonly=\"readonly\"" );

				$active = $this->lang->answer( $row['active'] );
				if ( $row['active'] == "0" ) {
					$submit_status = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				} elseif ( $row['active'] == "1" ) {
					$submit_status = $this->l->submit( AL_DEACTIVATE );
				}

				$date      = $this->dt->sqldatetime( $row['changed'] );
				$timestamp = strtotime( $date );

				$parameter = "id=" . $row["id"];

				$this->tbl->tbl_load( $handler, $row['id'], $row['name'], $text, array(
					$timestamp,
					"time",
					$date
				), $active, $this->l->form_admin( EMPTY_FREE_TEXT, "settings_global_scripts_edit.php", $parameter ) . $this->l->submit( AL_EDIT ) . "</form> " . $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "do", "content_status" ) . $submit_status . "</form> " . $this->l->form_admin( "id=\"form_admin_delete_content_$entry_counter\"" ) . $this->l->hidden( "do", "content_delete" ) . $this->l->hidden( "id", $row["id"] ) . $this->l->button( AL_DELETE, "id=\"button_delete_content_$entry_counter\"" ) . "</form>" );
				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_SCRIPT_PART01 . " \"" . $row['name'] . "\" " . AL_DELETE_SCRIPT_PART02, "#button_delete_content_$entry_counter", "form_admin_delete_content_$entry_counter", "confirm_delete_$entry_counter" );

				$entry_counter ++;
			}

			$content .= $this->tbl->tbl_out( $handler, 1 );
		} else {
			if ( ! empty( $search ) ) {
				$content .= "<h3>" . AL_NO_MATCHES . "</h3>";
			}
		}

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_SCRIPTS_START, "info", AL_INFORMATION );

		$this->l->display_preview();

		return $content;
	}

	public function content_add( $edit = false, $same_page = false ) {
		$content = "";


		if ( $edit ) {
			$GLOBALS['admin_subtitle'] = AL_EDIT_SCRIPT;
		} else {
			$GLOBALS['admin_subtitle'] = AL_ADD_SCRIPT;
		}

		$content .= $this->l->display_message_by_session( 'content_added', AL_SCRIPT_ADDED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_SCRIPT_UPDATED );

		if ( $edit ) {
			$content .= "<h3 class='underline'>" . AL_EDIT_SCRIPT . "</h3>\n";
		} else {
			$content .= "<h3 class='underline'>" . AL_ADD_SCRIPT . "</h3>\n";
		}

		if ( $edit ) {
			$id = $this->p->get( "id" );
			if ( empty( $id ) || ! is_numeric( $id ) ) {
				$content .= $this->l->alert_text( "danger", AL_ERROR_CANT_EDIT_SCRIPT );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
				$content .= start();

				return $content;
			} else {
				$sql    = "SELECT * FROM `" . TP . "scripts` WHERE `id` = '$id'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				if ( $result->num_rows != 1 ) {
					$content .= $this->l->alert_text( "danger", AL_ERROR_CANT_EDIT_SCRIPT );
					$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
					$content .= start();

					return $content;
				} else {
					$row            = $result->fetch_assoc();
					$row['changed'] = $this->dt->sqldatetime( $row['changed'] );
					$old_name       = $row['name'];
				}
			}
		} else {
			$old_name      = $this->p->get( "old_name", "" );
			$row['name']   = $this->p->get( "name", "" );
			$row['text']   = $this->p->get( "text", "", NOT_ESCAPED );
			$row['active'] = $this->p->get( "active", "1" );
			$id            = $this->p->get( "id", "" );
		}

		if ( $edit ) {
			$content .= $this->l->form_admin( "name='form_action'", "settings_global_scripts_edit.php", "id=$id" );
			$content .= $this->l->hidden( "do", "content_update" );
			$content .= $this->l->hidden( "do_no_back", "content_update_no_back" );
		} else {
			$content .= $this->l->form_admin( "name='form_action'", "settings_global_scripts_add.php" );
			$content .= $this->l->hidden( "do", "content_save" );
			$content .= $this->l->hidden( "do_no_back", "content_save_no_back" );
		}

		$content .= $this->l->hidden( "id", $id ) . $this->l->hidden( "old_name", $old_name ) . $this->l->table() . "
    <tr><td></td><td>" . $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='check_submit(this.form, false)'" ) . " ";
		if ( $edit ) {
			$content .= $this->l->reload_button( "settings_global_scripts_edit.php", "id=$id", AL_RELOAD ) . " ";
		} else {
			$content .= $this->l->reload_button( "settings_global_scripts_add.php", NO_PARAMETER, AL_RELOAD ) . " ";
		}

		$content .= $this->l->back_button( AL_BACK, "settings_global_scripts.php" ) . "</td></tr>\n";

		$content .= "
    <tr><td>" . AL_TBL_NAME . "</td><td>" . $this->l->text( "name", $row['name'] ) . "</td></tr>
    <tr><td>" . AL_TBL_TEXT . "</td><td>";

		$enhanced_editor = EDITOR_CODE;
		$rows            = "20";

		$ta_freetext = "";
		$ta_hint     = "";
		if ( $this->l->get_preview_status() ) {
			$ta_freetext = "readonly=\"readonly\"";
			$ta_hint     = "\n<br /><p class='info'>" . AL_JS_DISABLED_PREVIEW . ".</p>\n";
		}

		$content .= $this->l->media_ta( "text", $row['text'], UPLOADDIR, $enhanced_editor, $rows, $ta_freetext, "ta ta_media", NO_ADDITONAL_BUTTONS, "js" ) . $ta_hint . "</td></tr>
    <tr><td>" . AL_TBL_ACTIVE . "</td><td>" . $this->l->select_yesno( "active", $row['active'] ) . "</td></tr>

    <tr><td></td><td>" . $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " . $this->l->back_button( AL_BACK, "settings_global_scripts.php" ) . "</td></tr>
    </table></form>
    ";

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_SCRIPTS_EDIT, "info", AL_INFORMATION );

		$GLOBALS['body_footer'] .= "<script>
    function check_submit(formID, no_back)
    {
        str = formID.name.value;
        if(str == '')
        {
            " . $this->l->box_alert( "<H3 class='error underline'>" . AL_ERROR . "</H3><p>" . AL_MUST_ENTER_NAME . "</p>" ) . "
        }
        else
        {
            if(no_back)
            {
                formID.do.value = formID.do_no_back.value;
            }
            formID.submit();
        }
    }
    </script>
    ";

		return $content;
	}

	public function content_save( $stay_on_page = false ) {
		$content = "";


		$name   = $this->p->get( "name", "", NOT_ESCAPED );
		$text   = $this->p->get( "text", "", NOT_ESCAPED );
		$active = $this->p->get( "active", 1 );

		$parameter = "name=" . $name . "&text=" . $text . "&active=" . $active;

		$text = $this->db->escape( $text );

		$name = trim( $name );
		$name = $this->translate_special_characters( $name, false, true );
		$name = $this->db->escape( $name );
		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME, DO_BACK, $parameter, "settings_global_scripts_add.php" );

			return $content;
		}

		$sql    = "SELECT `id` FROM `" . TP . "scripts` WHERE `name` LIKE '$name'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 0 ) {
			$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02, DO_BACK, $parameter, "settings_global_scripts_add.php" );

			return $content;
		}

		if ( $this->l->get_preview_status() ) {
			$this->db->ins( __FILE__ . ":" . __LINE__, "scripts", array( "name", "active", "created" ), array(
				$name,
				$active,
				"NOW()"
			) );
		} else {
			$this->db->ins( __FILE__ . ":" . __LINE__, "scripts", array(
				"name",
				"text",
				"active",
				"created"
			), array( $name, $text, $active, "NOW()" ) );
		}

		$id = $this->db->get_insert_id();

		$this->log->event( "log", __FILE__ . ":" . __LINE__, "New content_text added, name: $name, id: " . $id );

		$_SESSION['content_added'] = 1;

		if ( $stay_on_page ) {
			$this->l->reload_js( "settings_global_scripts_edit.php", "id=$id" );
		} else {
			$this->l->reload_js( "settings_global_scripts.php" );
		}

		return $content;
	}

	public function content_update( $stay_on_page = false ) {
		$content = "";


		$id       = $this->p->get( "id" );
		$text     = $this->p->get( "text", "", false );
		$active   = $this->p->get( "active", 1 );
		$name     = $this->p->get( "name", "", false );
		$old_name = $this->p->get( "old_name" );

		$parameter = "id=" . $id . "&name=" . $name . "&text=" . $text . "&active=" . $active;

		$text = $this->db->escape( $text );

		$name = trim( $name );
		$name = $this->translate_special_characters( $name, false, true );
		$name = $this->db->escape( $name );
		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME, DO_BACK, $parameter, "settings_global_scripts_edit.php" );

			return $content;
		}
		if ( $old_name != $name ) {
			$sql    = "SELECT `id` FROM `" . TP . "scripts` WHERE `name` LIKE '$name'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 0 ) {
				$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02, DO_BACK, $parameter, "settings_global_scripts_edit.php" );

				return $content;
			}
		}
		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text( "danger", AL_ERROR_CANT_UPDATE_SCRIPT );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= start();

			return $content;
		} else {

			if ( $this->l->get_preview_status() ) {
				$this->db->upd( __FILE__ . ":" . __LINE__, "scripts", array( "name", "active" ), array(
					$name,
					$active
				), "`id` = '$id'" );
			} else {
				$this->db->upd( __FILE__ . ":" . __LINE__, "scripts", array( "name", "text", "active" ), array(
					$name,
					$text,
					$active
				), "`id` = '$id'" );
			}
			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content_text changed, name: $name, id: " . $id );

			$_SESSION['content_updated'] = 1;

			if ( ! $stay_on_page ) {
				$this->l->reload_js( "settings_global_scripts.php" );
			}

			return $content;
		}
	}

	public function content_delete() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "scripts", array( "deleted" ), array( true ), "`id` = '$id'" );
		} else {
			$content .= $this->l->alert_text( "danger", AL_ERROR_CANT_DELETE_SCRIPT );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_SCRIPT_DELETED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content_text deleted, id: " . $id );

		return $content;
	}

	public function content_status() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT `active` FROM `" . TP . "scripts` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();
				if ( $row['active'] == 0 ) {
					$active = 1;
				} else {
					$active = 0;
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "scripts", array( "active" ), array( $active ), "`id` = '$id'" );
			} else {
				$content .= $this->l->alert_text( "danger", AL_ERROR_CANT_CHANGE_STATUS_OF_SCRIPT );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_ERROR_CANT_CHANGE_STATUS_OF_SCRIPT );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_SCRIPT_STATUS_CHANGED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "text status changed, id: " . $id );

		return $content;
	}
}

if ( ! isset( $action ) ) {
	$action = null;
}

$class_settings_global_scripts = new class_settings_global_scripts ( $action );
$content .= $class_settings_global_scripts->get_content();