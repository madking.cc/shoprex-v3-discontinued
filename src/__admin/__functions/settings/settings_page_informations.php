<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_INFORMATIONS;

class class_settings_page_informations extends class_sys {
	public $content;

	public function __construct() {

		parent::__construct();

		$action = $this->p->get( "do" );

		switch ( $action ) {
			case "update":
				$this->update();
				break;
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";


		$content .= "<h3 class='underline'>" . AL_INFORMATIONS . "</h3>\n";

		$content .= $this->l->display_message_by_session( 'options_saved', AL_SETTINGS_SAVED );

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "update" ) . $this->l->table();

		foreach ( $this->lang->languages as $lang_array ) {
			$content .= "
    <tr><td>" . AL_PAGETITLE_IN . " " . $lang_array['text'] . "</td><td>" . $this->l->text( "title" . $lang_array['number'], constant( "TITLE" . $lang_array['number'] ), "800" ) . "</td></tr>
    <tr><td>" . AL_DESCRIPTION_IN . " " . $lang_array['text'] . "</td><td>" . $this->l->ta_char_counter( "description" . $lang_array['number'], constant( "DESCRIPTION" . $lang_array['number'] ), "800", "150", EMPTY_FREE_TEXT, AUTO_ID, META_DESCRIPTION_MAX_CHARS ) . "</td></tr>
    <tr><td>" . AL_KEYWORDS_IN . " " . $lang_array['text'] . "</td><td>" . $this->l->text_char_counter( "keywords" . $lang_array['number'], constant( "KEYWORDS" . $lang_array['number'] ), "800", EMPTY_FREE_TEXT, AUTO_ID, META_KEYWORDS_MAX_CHARS ) . "</td></tr>

        ";
		}
		$content .= "
    <tr><td></td><td>" . $this->l->submit( AL_SAVE ) . "</td></tr>
    </table></form>
    ";

		return $content;
	}

	public function update() {


		$elements = array();

		foreach ( $this->lang->languages as $lang_array ) {
			if ( is_null( $lang_array['number'] ) || empty( $lang_array['number'] ) ) {
				$key = "0";
			} else {
				$key = $lang_array['number'];
			}

			$elements[ $key ]['title']       = $this->p->get( "title" . $lang_array['number'] );
			$elements[ $key ]['description'] = $this->p->get( "description" . $lang_array['number'] );
			$elements[ $key ]['keywords']    = $this->p->get( "keywords" . $lang_array['number'] );
		}

		foreach ( $elements as $lang_key => $value_array ) {
			foreach ( $value_array as $key => $value ) {
				if ( empty( $lang_key ) ) {
					$lang_key = "";
				}
				$this->db->upd( __FILE__ . ":" . __LINE__, "settings_by_language", array( "value" . $lang_key ), array( $value ), "UPPER(`key`) LIKE UPPER('$key')" );
			}
		}
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "Settings updated, key" );

		$_SESSION['options_saved'] = 1;

		$this->l->reload_js();
	}

}

$class_settings_page_informations = new class_settings_page_informations ();
$content .= $class_settings_page_informations->get_content();