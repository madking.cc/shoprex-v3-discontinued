<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_LOGIN;

class class_login extends class_sys {
	public $content;

	public function __construct( $action ) {
		parent::__construct();
		$this->content = "";
		switch ( $action ) {
			case "adm_start_password_reset":
				$this->content .= $this->password_reset();
				$this->content .= $this->start();
				break;
			case "adm_start_check_login":
				$this->content .= $this->check();
				break;
			default:
				$this->content .= $this->start();
				break;
		}
	}

	public function get_content() {
		return $this->content;
	}

	public function start( $user = "", $error = "" ) {
		$content = "";

		if ( ! empty( $error ) ) {
			$content .= $this->l->alert_text( "danger", $error );
			$this->p->init_array( $GLOBALS['foot'], 90 );
			$GLOBALS['foot'][90] .= "   <script>
        $(function() {
            $( \"#login_form\" ).effect( \"shake\" );
        });
        </script>
";
			$GLOBALS['body_header'] .= "   <style>
        body { padding-top: 0; !important }
        </style>
";
		}

		$password = "";
		if ( $this->l->get_preview_status() ) {
			if ( empty( $user ) ) {
				$user     = "admin123";
				$password = "123";
			}
		}

		$content .= $this->l->form_admin( " onSubmit='return check_submit(this)'" ) .
		            $this->l->hidden( "do", "adm_start_check_login" ) .
		            $this->l->table( "id='login_form' style='width:400px;margin-left:-200px; position:absolute; top: 200px; left: 50%; border:1px solid #dddddd !important;'" ) . "
        <tr><td colspan='2' align='center'><h4>" . AL_ADMIN_LOGIN . "</h4></td></tr>
        <tr>
        <td>" . AL_LOGIN_NAME . ":</td><td>" . $this->l->text( "name", $user, "250", "autofocus" ) . "</td>
        </tr>
        <tr>
        <td>" . AL_PASSWORD . ":</td><td>" . $this->l->password( "pass", $password ) . "</td>
        </tr>
        <tr>
         <td></td>
         <td>" . $this->l->submit( AL_LOGIN ) . " " . $this->l->button( AL_PASSWORD_LOST, "onclick='password_reset(this.form)'" ) . "</td>
         </tr>
         </table></form>\n";

		$GLOBALS['body_footer'] .= "<script>
        function check_submit(formID)
        {
            return true;
        }
        function password_reset(formID)
        {
            if(formID.name.value == '')
            {
                " . $this->l->box_alert( "<H3 class='error underline'>" . AL_ERROR . "</H3><p>" . AL_MUST_ENTER_NAME . "!</p>" ) . "
                return;
            }
            else
            {
                formID.do.value = 'adm_start_password_reset';
                formID.submit();
            }
        }
    </script>
    ";

		return $content;
	}

	public function check() {
		$content = "";

		$sql    = "SELECT * FROM `" . TP . "accounts_admin` WHERE (`name` IS NOT NULL OR `name` NOT LIKE '') AND (`pass` IS NOT NULL OR `pass` NOT LIKE '') AND `deleted` = '0'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows == 0 ) {
			$content .= $this->l->alert_text( "danger", AL_NO_ADDMIN_ACCOUNT_FOUND, false );
			$this->log->error( "sql", __FILE__ . ":" . __LINE__, "No admin account found. SQL: " . $sql );

			return $content;
		}

		$name = $this->p->get( "name" );
		if ( empty( $name ) ) {
			$content .= $this->start( $name, ENTER_NAME );
			sleep( 2 );

			return $content;
		}

		$pass = $this->p->get( "pass" );
		if ( empty( $name ) ) {
			$content .= $this->start( $name, AL_ENTER_PASSWORD );
			sleep( 2 );

			return $content;
		}

		$name_found = false;
		while ( $row = $result->fetch_assoc() ) {
			if ( strcmp( $row['name'], $name ) === 0 ) {
				$name_found = true;
				break;
			}
		}
		if ( $name_found == false ) {
			$content .= $this->start( $name, AL_WRONG_USER_OR_PASSWORD );
			sleep( 2 );

			return $content;
		}

		$crypt       = new class_crypt();
		$pass_verify = $crypt->verify( $pass, $row['pass'] );

		if ( $pass_verify && $name_found ) {
			$_SESSION['admin_user'] = $name;
			$_SESSION['admin_pass'] = $row['pass'];

			$this->log->event( "security", __FILE__ . ":" . __LINE__, "Admin \"$name\" logged in" );
			sleep( 2 );
			$this->l->reload_js();
		} else {
			$this->log->event( "security", __FILE__ . ":" . __LINE__, "Wrong Admin Name: \"$name\" or Password: (hidden)", "Security" );
			$content .= $this->start( $name, "Falscher Username oder Passwort" );
			$this->p->mail_admin( __FILE__ . ":" . __LINE__, "<p>Wrong Admin Name: \"$name\" or Password: \"$pass\"</p>", "Security", false );
			sleep( 2 );
		}

		return $content;
	}

	public function password_reset() {
		$content = "";

		$GLOBALS['admin_subtitle'] = AL_PASSWORD_RESET;

		$name = $this->p->get( "name" );

		$sql    = "SELECT `id`,`email`,`name` FROM `" . TP . "accounts_admin` WHERE `name` LIKE BINARY '$name' AND `deleted` = '0'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows == 1 ) {
			$row = $result->fetch_assoc();

			$email = $row['email'];
			$hash  = $this->p->unique_id();

			$time_to_life = date( "Y-m-d H:i:s" );

			$this->db->ins( __FILE__ . ":" . __LINE__, "password_reset_admin", array(
				"to_id",
				"hash",
				"time_to_life",
				"created"
			), array( $row['id'], $hash, $time_to_life, "NOW()" ) );

			$message = "<H1>" . AL_PASSWORD_RESET . "</H1>
    <p>" . AL_REQUEST_PASSWORD_RESET_PART01 . " <strong>'" . $row['name'] . "'</strong> " . AL_REQUEST_PASSWORD_RESET_PART02 . "</p>
    <p> </p>
    <p><a href='" . $this->loc->httpauto_web_admin_root . "pass_reset.php?id=$hash'>" . $this->loc->httpauto_web_admin_root . "pass_reset.php?id=$hash</a></p>
    ";

			$subject = "Admin Passwort Reset";

			$result = $this->mail->send_short( __FILE__ . ":" . __LINE__, $email, "", $message, $subject );

			if ( $result ) {
				$this->log->event( "mail", __FILE__ . ":" . __LINE__, "Email für Passwort Reset des Users '" . $row['name'] . "' wurde gesendet" );
				$this->p->mail_admin( __FILE__ . ":" . __LINE__, "<p>" . AL_PASSWORD_RESET_MAIL_SENT_PART01 . " <strong>'" . $row['name'] . "'</strong> " . AL_PASSWORD_RESET_MAIL_SENT_PART02 . ".</p>", "Mail", false );
			} else {

				$this->log->error( "mail", __FILE__ . ":" . __LINE__, "Email für Passwort Reset des Users '" . $row['name'] . "' konnte nicht gesendet werden" );
				$this->p->mail_admin( __FILE__ . ":" . __LINE__, "<p>" . AL_PASSWORD_RESET_MAIL_NOT_SENT_PART01 . " <strong>'" . $row['name'] . "'</strong> " . AL_PASSWORD_RESET_MAIL_NOT_SENT_PART02 . ".</p>", "Mail", false );
			}
		} else {

			$this->log->error( "security", __FILE__ . ":" . __LINE__, "No admin account found for password reset. num_rows: '" . $result->num_rows . "', SQL: " . $sql );
			$this->p->mail_admin( __FILE__ . ":" . __LINE__, "<p>No admin account found for password reset. num_rows: '" . $result->num_rows . "', SQL: " . $sql . "</p>", "Security", false );
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_PROCESS_FINISHED );

		return $content;
	}
}


if ( isset( $ajax_not_logged_in ) && $ajax_not_logged_in ) {
	$this->log->error( "ajax", __FILE__ . ":" . __LINE__, "ajax request, but not logged in" );
	$content = false;
} else {
	$action = $p->get( "do" );
	$login  = new class_login( $action );
	$content .= $login->get_content();

}



 



    