<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_EVENT_LOG;

class class_event_log extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();
		$this->content = "";
		$this->content .= $this->start();

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {

		$content = "";

		if ( ! isset( $_SESSION['log_select'] ) ) {
			$_SESSION['log_select'] = "user";
		}
		$source                 = $this->p->get( "source", $_SESSION['log_select'] );
		$_SESSION['log_select'] = $source;

		switch ( $source ) {
			case "admin":
				$log = $this->loc->dir_root . $GLOBALS['log_d'] . $GLOBALS['logging_prefix'] . "events.log";
				break;
			case "user_bot":
				$log = $this->loc->dir_root . $GLOBALS['log_d'] . "events_bot.log";
				break;
			default:
				$log = $this->loc->dir_root . $GLOBALS['log_d'] . "events.log";
				break;
		}

		$size      = @filesize( $log );
		$count_all = 0;
		if ( file_exists( $log ) && ! empty( $size ) ) {

			$zeilen = array();
			$handle = fopen( $log, "r" );
			if ( $handle !== false ) {
				while ( ! feof( $handle ) ) {
					$zeilen[] = fgets( $handle );
				}
				fclose( $handle );
			}
			$types = array();

			if ( is_array( $zeilen ) && sizeof( $zeilen ) > 0 ) {
				foreach ( $zeilen AS $zeile ) {

					$tmp = unserialize( $zeile );
					if ( ! $zeile ) {
						continue;
					}
					$count_all ++;
					$tmp["type"] = strtolower( $tmp["type"] );
					if ( ! isset( $types[ $tmp["type"] ] ) ) {
						$types[ $tmp["type"] ] = 1;
					} else {
						$types[ $tmp["type"] ] ++;
					}
				}
			}

			if ( sizeof( $types ) == 0 ) {
				$types = false;
			}
		} else {
			$types = false;
		}

		if ( ! isset( $_SESSION['log_type'] ) ) {
			$_SESSION['log_type'] = "all";
		}
		$type                 = $this->p->get( "type", $_SESSION['log_type'] );
		$_SESSION['log_type'] = $type;
		$found                = false;
		if ( is_array( $types ) && sizeof( $types ) > 0 ) {
			foreach ( $types as $name => $count ) {
				if ( strcasecmp( $type, $name ) === 0 ) {
					$found = true;
					break;
				}
			}
		}
		if ( ! $found ) {
			$type = "all";
		}

		$content .= "<h3 class='underline'>" . AL_EVENT_LOG_COLON . "</h3>\n";

		$content .= $this->l->form_admin() . $this->l->table( "", "" ) . "
    <tr>
     <td>" . $this->l->select( "source", "", "", true ) . "
     <option value='user'";
		if ( $source == "user" ) {
			$content .= " selected=selected";
		}
		$content .= ">" . AL_USER_AREA . "</option>
     <option value='admin'";
		if ( $source == "admin" ) {
			$content .= " selected=selected";
		}
		$content .= ">" . AL_ADMIN_AREA . "</option>
     <option value='user_bot'";
		if ( $source == "user_bot" ) {
			$content .= " selected=selected";
		}
		$content .= ">" . AL_USER_AREA_BOTS . "</option>
     </select>
     </td>
     <td>" . $this->l->select( "type", "", "", true ) . "
     <option value='all'";

		if ( $type == "all" ) {
			$content .= " selected=selected";
			$content .= ">Alle ($count_all)</option>\n";
		} else {
			if ( ! empty( $types ) ) {
				$content .= ">Alle ($count_all)</option>\n";
			} else {
				$content .= ">Keine Daten</option>\n";
			}
		}

		if ( ! empty( $types ) ) {
			foreach ( $types as $name => $count ) {
				$content .= "<option value='$name'";
				if ( $type == $name ) {
					$content .= " selected=selected";
				}
				$content .= ">$name ($count)</option>\n";
			}
		}
		$content .= "     </select>
     </td>
    </tr>
    </table>
    </form>
    <br />
    ";

		$size = @filesize( $log );
		if ( file_exists( $log ) && ! empty( $size ) ) {

			$zeilen = array();
			$handle = fopen( $log, "r" );
			if ( $handle !== false ) {
				while ( ! feof( $handle ) ) {
					$zeilen[] = fgets( $handle );
				}
				fclose( $handle );
			}
			$log_content = array();

			if ( is_array( $zeilen ) && sizeof( $zeilen ) > 0 ) {
				foreach ( $zeilen AS $zeile ) {

					$tmp = unserialize( $zeile );
					if ( ! $zeile ) {
						continue;
					}

					$tmp["type"] = strtolower( $tmp["type"] );
					if ( $type != "all" && $tmp["type"] != $type ) {
						continue;
					}

					$date = strtotime( $tmp["date"] );
					$date = date( $GLOBALS['default_datetime_format']['datetime_seconds'], $date );

					$tmp2 = "";
					if ( ! empty( $tmp["location"] ) ) {
						$tmp2 = "<br />" . $tmp["location"];
					}

					$tmp3 = "";
					if ( isset( $tmp["user_agent"] ) && $tmp["user_agent"] ) {
						$tmp3 = $tmp["user_agent"] . "<br />";
					}

					$tmp2 .= " Runlevel: " . $tmp["runlevel"];

					$log_content[] =

						$date . " -> " . $tmp["ip"] . $tmp2 . "<br />

                <pre>" . $tmp["uri"] . "<br />" . $tmp3 . $tmp["text"] . "</pre>\n";
				}
			}

			if ( is_array( $log_content ) && sizeof( $log_content ) > 0 ) {
				$log_content = array_reverse( $log_content );
				$content .= $this->l->table();
				foreach ( $log_content as $data ) {
					$content .= "<tr><td>\n";
					$content .= $data;
					$content .= "\n</td></tr>\n";
				}
				$content .= "</table>\n";
			} else {
				$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>";
			}
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>";
		}

		return $content;
	}
}

$class_event_log = new class_event_log ();
$content .= $class_event_log->get_content();

