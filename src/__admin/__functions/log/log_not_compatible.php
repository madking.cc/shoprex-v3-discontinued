<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_INCOMPATIBLE;

class class_not_compatible extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();
		$this->content = "";
		$this->content .= $this->start();

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {

		$content = "";

		$content .= "<h3 class='underline'>" . AL_NOT_COMPATIBLE_LOG_COLON . "</h3>\n";

		$sql    = "SELECT * FROM `" . TP . "statistics_not_compatible` ORDER BY `changed` DESC";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {

			$sql     = "SELECT SUM(`count`) AS `sum_count` FROM `" . TP . "statistics_not_compatible`";
			$result2 = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			$row2    = $result2->fetch_assoc();
			$sum     = $row2['sum_count'];

			$handler = $this->tbl->tbl_init( AL_TBL_TYPE, AL_TBL_USER_AGENT, AL_TBL_VIEWS, AL_TBL_QUOTA, AL_TBL_LAST );

			while ( $row = $result->fetch_assoc() ) {
				if ( $row['type'] == 1 ) {
					$type = AL_HUMAN;
				} else {
					$type = AL_BOT;
				}

				$date = $row['changed'];
				if ( $date == "0000-00-00 00:00:00" ) {
					$date = $row['created'];
				}
				if ( $date == "0000-00-00 00:00:00" ) {
					$date = STATISTICS_START_DATE;
				} else {
					$date = $this->dt->sqldatetime( $date );
				}
				$timestamp = strtotime( $date );

				$percent = round( $row['count'] / ( $sum / 100 ), 1 );

				$this->tbl->tbl_load( $handler, $type, $row['user_agent'], $row['count'], array(
					$percent,
					"%"
				), array( $timestamp, "time", $date ) );
			}

			$content .= $this->tbl->tbl_out( $handler, 2 );
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>";
		}

		return $content;

	}
}

$class_not_compatible_log = new class_not_compatible ();
$content .= $class_not_compatible_log->get_content();
