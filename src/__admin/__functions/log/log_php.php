<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_PHP_LOG;


class class_log_php extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();
		$this->content = "";
		$this->content .= $this->start();

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {

		$content = "";

		$log = LOG_ERROR_FILE_PHP;

		$content .= "<h3 class='underline'>" . AL_PHP_LOG_COLON . "</h3>\n";

		$size = @filesize( $log );
		if ( file_exists( $log ) && ! empty( $size ) ) {

			$zeilen = array();
			$handle = fopen( $log, "r" );
			while ( ! feof( $handle ) ) {
				$zeilen[] = fgets( $handle );
			}
			fclose( $handle );

			$log_content = array();

			foreach ( $zeilen AS $zeile ) {
				if ( empty( $zeile ) ) {
					continue;
				}
				if ( stripos( $zeile, "fatal error" ) === false ) {
					$code_prefix = "";
					$code_suffix = "";
				} else {
					$code_prefix = "<span class='error'>";
					$code_suffix = "</span>";
				}

				$log_content[] = "<p>" . $code_prefix . $zeile . $code_suffix . "</p>";
			}

			if ( sizeof( $log_content ) > 0 ) {
				$log_content = array_reverse( $log_content );
				$content .= $this->l->table();
				foreach ( $log_content as $data ) {

					$content .= "<tr><td>\n";
					$content .= $data;
					$content .= "\n</td></tr>\n";
				}
				$content .= "</table>\n";
			} else {
				$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>";
			}
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>";
		}

		return $content;
	}
}

$class_log_php = new class_log_php ();
$content .= $class_log_php->get_content();

