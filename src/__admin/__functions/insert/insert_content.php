<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_ADD_TEXT;

class class_insert_content extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();
		$this->content = $this->start();

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {

		$content = "";

		$textarea_type = $this->p->get( "textarea_type", EDITOR_SIMPLE );
		$editor        = $this->p->get( "editor" );

		$spaces = "&#160;&#160;&#160;&#160;&#160;";

// Pages get
		$page_select_options        = "";
		$page_button_select_options = "";
		$sql                        = "SELECT `name`,`path`,`id`,`active` FROM `" . TP . "pages` WHERE `deleted` = '0' ORDER BY `name`,`id`,`path`";
		$result                     = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				if ( $row['active'] ) {
					$active = " " . AL_ACTIVE;
				} else {
					$active = " " . AL_INACTIVE;
				}
				$page_button_select_options .= "<option value='[page_link=" . $row['id'] . "]identifier=" . $row['name'] . "&text=&class=btn btn-primary&role=button[/page_link]'>" . $row['name'] . " " . $spaces . "(" . AL_TBL_ID . " " . $row['id'] . AL_COMMA . " " . AL_TBL_PATH . " " . $row[ 'path' . $this->lang->lnbrm ] . AL_COMMA . " " . AL_TBL_STATUS . " " . $active . ")</option>\n";
				$page_select_options .= "<option value='[page_link=" . $row['id'] . "]identifier=" . $row['name'] . "&text=&class=link&role=link[/page_link]'>" . $row['name'] . " " . $spaces . "(" . AL_TBL_ID . " " . $row['id'] . AL_COMMA . " " . AL_TBL_PATH . " " . $row[ 'path' . $this->lang->lnbrm ] . AL_COMMA . " " . AL_TBL_STATUS . " " . $active . ")</option>\n";
			}
		}

// Downloads get
		$file_download_select_options = "";
		$sql                          = "SELECT `name`,`filename`,`id`,`active` FROM `" . TP . "file_download` WHERE `deleted` = '0' ORDER BY `name`,`filename`,`id`";
		$result                       = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				if ( $row['active'] ) {
					$active = " " . AL_ACTIVE;
				} else {
					$active = " " . AL_INACTIVE;
				}
				if ( empty( $row['name'] ) ) {
					$row['name'] = $row['filename'];
				} else {
					$row['name'] = $row['name'] . AL_COMMA . " " . $row['filename'];
				}
				$file_download_select_options .= "<option value='[file=" . $row['filename'] . "]" . $row['id'] . "[/file]'>" . $row['name'] . " " . $spaces . "(" . AL_TBL_ID . " " . $row['id'] . AL_COMMA . " " . AL_TBL_STATUS . " " . $active . ")</option>\n";
			}
		}


// Text get
		$text_array = array();

		$sql    = "SELECT `name` FROM `" . TP . "text` WHERE `deleted` = '0' ";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				$text_array[ "[t]" . $row['name'] . "[/t]" ] = $row['name'];
			}
		}

		if ( sizeof( $text_array ) > 0 ) {
			asort( $text_array );
			$text_array = $this->p->array_split( $text_array );
		}

// Single-Text get
		$text_single_array = array();

		$sql    = "SELECT `name` FROM `" . TP . "text_single` WHERE `deleted` = '0' ";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				$text_single_array[ "[s]" . $row['name'] . "[/s]" ] = $row['name'];
			}
		}

		if ( sizeof( $text_single_array ) > 0 ) {
			asort( $text_single_array );
			$text_single_array = $this->p->array_split( $text_single_array );
		}

		$content .= "<div style='text-align:right'>" . $this->l->button_close() . "</div>\n";
		$content .= "<h3 class='underline'>" . AL_TEXT_HELP . "</h3>
";

		$content .= "<p> </p>
        " . $this->l->panel( "<p>" . AL_HELP_OVERVIEW_BINDABLE_VARS . "</p>
         ", "info", AL_INFORMATION );

// Display Uni-Code Chars
		$content .= "
<div class='row'><div class='col-sm-12'><h4>" . AL_UNICODE_CHARS . "</h4></div></div>
<div class='row'>\n";
		// todo: check suc duc
		$content .= "
    <div class='col-sm-6'>\n";
		$content .= $this->l->table();
		$content .= "<tr><td style='vertical-align:middle;'>" . AL_AMPERSAND . " &#38;</td><td style='vertical-align:middle;'>[suc]38[/suc]
    </td><td style='vertical-align:middle;'>" . $this->l->button_paste( $editor, $textarea_type, "[suc]38[/suc]" ) . "</td></tr>\n";
		$content .= "<tr><td style='vertical-align:middle;'>" . AL_LEFT_ANGLE_BRACKET . " &#60;</td><td style='vertical-align:middle;'>[suc]60[/suc]
    </td><td style='vertical-align:middle;'>" . $this->l->button_paste( $editor, $textarea_type, "[suc]60[/suc]" ) . "</td></tr>\n";
		$content .= "</table>";
		$content .= "
    </div>
    <div class='col-sm-6'>\n";
		$content .= $this->l->table();
		$content .= "<tr><td style='vertical-align:middle;'>" . AL_UNICODE . " &#38;#38;</td><td style='vertical-align:middle;'>[duc]38[/duc]
    </td><td style='vertical-align:middle;'>" . $this->l->button_paste( $editor, $textarea_type, "[duc]38[/duc]" ) . "</td></tr>\n";
		$content .= "<tr><td style='vertical-align:middle;'>" . AL_UNICODE . " &#38;#60;</td><td style='vertical-align:middle;'>[duc]60[/duc]
    </td><td style='vertical-align:middle;'>" . $this->l->button_paste( $editor, $textarea_type, "[duc]60[/duc]" ) . "</td></tr>\n";
		$content .= "</table>";
		$content .= "
    </div>\n";
		$content .= "
</div>
";

// Display text
		$content .= "
<div class='row'><div class='col-sm-12'><h4>" . AL_TEXT_BLOCKS . "</h4></div></div>
<div class='row'>\n";
		if ( sizeof( $text_array ) > 0 ) {

			$content .= "
    <div class='col-sm-6'>\n";
			if ( sizeof( $text_array['left'] > 0 ) ) {
				$content .= $this->l->table();
				foreach ( $text_array['left'] as $key => $value ) {
					$content .= "<tr><td style='vertical-align:middle;'>" . $key . "</td><td style='vertical-align:middle;'>" . $value . "</td><td style='vertical-align:middle;'>" . $this->l->button_paste( $editor, $textarea_type, $key ) . "</td></tr>\n";
				}
				$content .= "</table>";
			}
			$content .= "
    </div>
    <div class='col-sm-6'>\n";
			if ( sizeof( $text_array['right'] > 0 ) ) {
				$content .= $this->l->table();
				foreach ( $text_array['right'] as $key => $value ) {
					$content .= "<tr><td style='vertical-align:middle;'>" . $key . "</td><td style='vertical-align:middle;'>" . $value . "</td><td style='vertical-align:middle;'>" . $this->l->button_paste( $editor, $textarea_type, $key ) . "</td></tr>\n";
				}
				$content .= "</table>";
			}
			$content .= "
    </div>\n";
		} else {
			$content .= "<div class='col-sm-12'><p>" . AL_NO_CONTENT_FOUND . "</p></div>\n";
		}
		$content .= "
</div>
";


// Display Single-Text
		$content .= "
<div class='row'><div class='col-sm-12'><h4>" . AL_ONE_LINERS . "</h4></div></div>
<div class='row'>\n";
		if ( sizeof( $text_single_array ) > 0 ) {

			$content .= "<div class='col-sm-6'>\n";
			if ( sizeof( $text_single_array['left'] > 0 ) ) {
				$content .= $this->l->table();
				foreach ( $text_single_array['left'] as $key => $value ) {
					$content .= "<tr><td style='vertical-align:middle;'>" . $key . "</td><td style='vertical-align:middle;'>" . $value . "</td><td style='vertical-align:middle;'>" . $this->l->button_paste( $editor, $textarea_type, $key ) . "</td></tr>\n";
				}
				$content .= "</table>";
			}
			$content .= "
    </div>
    <div class='col-sm-6'>\n";
			if ( sizeof( $text_single_array['right'] > 0 ) ) {
				$content .= $this->l->table();
				foreach ( $text_single_array['right'] as $key => $value ) {
					$content .= "<tr><td style='vertical-align:middle;'>" . $key . "</td><td style='vertical-align:middle;'>" . $value . "</td><td style='vertical-align:middle;'>" . $this->l->button_paste( $editor, $textarea_type, $key ) . "</td></tr>\n";
				}
				$content .= "</table>";
			}
			$content .= "
    </div>\n";
		} else {
			$content .= "<div class='col-sm-12'><p>" . AL_NO_CONTENT_FOUND . "</p></div>\n";
		}
		$content .= "
</div>
";

		// Display File Download
		$content .= "
<div class='row'><div class='col-sm-12'><h4>" . AL_DOWNLOADS . "</h4></div></div>
<div class='row'>\n";
		if ( ! empty( $page_select_options ) > 0 ) {

			$content .= "<div class='col-sm-12'>\n";

			$content .= $this->l->table();

			$content .= "<tr><td>" . $this->l->form() . $this->l->select( "file_download_link", EMPTY_FREE_TEXT, AUTO_WIDTH ) . $file_download_select_options . "</select> " . $this->l->select_paste( $editor, $textarea_type, "#file_download_link", AL_INSERT_LINK ) . "</form></td></tr>";

			$content .= "</table>";

			$content .= "
    </div>\n";
		} else {
			$content .= "<div class='col-sm-12'><p>" . AL_NO_DOWNLOADS . "</p></div>\n";
		}
		$content .= "
</div>
";

// Display Pages
		$content .= "
<div class='row'><div class='col-sm-12'><h4>" . AL_LINK_TO_PAGE . "</h4></div></div>
<div class='row'>\n";
		if ( ! empty( $page_select_options ) > 0 ) {

			$content .= "<div class='col-sm-12'>\n";

			$content .= $this->l->table();

			$content .= "<tr><td>" . $this->l->form() . $this->l->select( "page_link_button", EMPTY_FREE_TEXT, AUTO_WIDTH ) . $page_button_select_options . "</select> " . $this->l->select_paste( $editor, $textarea_type, "#page_link_button", AL_INSERT_BUTTON ) . "</form></td></tr>";
			$content .= "<tr><td>" . $this->l->form() . $this->l->select( "page_link", EMPTY_FREE_TEXT, AUTO_WIDTH ) . $page_select_options . "</select> " . $this->l->select_paste( $editor, $textarea_type, "#page_link", AL_INSERT_LINK ) . "</form></td></tr>";

			$content .= "</table>";

			$content .= "
    </div>\n";
		} else {
			$content .= "<div class='col-sm-12'><p>" . AL_NO_PAGES . "</p></div>\n";
		}
		$content .= "
</div>
";

		if ( isset( $GLOBALS['plugins_insert_content_class'] ) && sizeof( $GLOBALS['plugins_insert_content_class'] ) > 0 ) {
			foreach ( $GLOBALS['plugins_insert_content_class'] as $plugin_class ) {
				$$plugin_class = new $plugin_class;
				$$plugin_class->generate_content( $spaces, $editor, $textarea_type );
				$content .= $$plugin_class->get_content();
			}
		}


		return $content;
	}
}

$class_insert_content = new class_insert_content ();
$content .= $class_insert_content->get_content();