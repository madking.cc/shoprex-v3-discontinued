<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_SELECT_FLAG;

class class_select_flag extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		$action = $this->p->get( 'do', 'init' );

		switch ( $action ) {
			case "init":
				$this->content .= $this->init();
				break;
			case "finish":
				$this->content .= $this->finished();
				break;
		}
	}

	public function get_content() {
		return $this->content;
	}

	public function init() {


		$content = "";

		$target  = $this->p->get( "target" );
		$element = $this->p->get( "element_number" );

		if ( is_null( $target ) || empty( $target ) ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "target missing." );
			$content .= "Target fehlt.";

			return $content;
		}
		if ( is_null( $element ) ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "element missing." );
			$content .= "Element fehlt.";

			return $content;
		}

		$flags = $this->p->get_files( $GLOBALS['media_flags_dir'] );

		$content .= $this->l->table();
		$i = 0;
		foreach ( $flags as $value ) {
			if ( $i == 0 ) {
				$content .= "<tr>\n";
			}

			$content .= "<td>" . $this->l->img( $GLOBALS['media_flags_dir'] . $value, EMPTY_ALT ) . " " . $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, NO_PARAMETER ) .
			            $this->l->hidden( "do", "finish" ) . $this->l->hidden( "target", $target ) . $this->l->hidden( "element", $element ) . $this->l->hidden( "text", $value ) . $this->l->submit( AL_SELECT ) . "</form></td>\n";

			if ( $i == 2 ) {
				$content .= "</tr>\n";
				$i = 0;
			} else {
				$i ++;
			}
		}
		if ( $i < 3 ) {
			if ( $i == 1 ) {
				$content .= "<td></td>\n";
			}
			if ( $i == 1 || $i == 2 ) {
				$content .= "<td></td>\n";
			}
			$content .= "</tr>\n";
		}

		$content .= "</table>\n";

		return $content;
	}

	public function finished() {


		$content = "";

		$target = $this->p->get( "target" );
		if ( is_null( $target ) || empty( $target ) ) {
			$this->log->error( "php", __FILE__ . ":" . __LINE__, "target missing." );
			$content .= "Target fehlt.";

			return $content;
		}
		$text    = $this->p->get( "text" );
		$element = $this->p->get( "element" );

		$this->p->paste_element( "/" . WEBROOT . $GLOBALS['media_flags_dir'] . $text, $target, $element, NO_SUBMIT_ID, AS_IMAGE, $text );

		return $content;
	}

}

$class_select_flag = new class_select_flag ();
$content .= $class_select_flag->get_content();