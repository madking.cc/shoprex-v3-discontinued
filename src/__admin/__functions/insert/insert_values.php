<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_ADD_TEMPLATES;

class class_insert_values extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();
		$this->content = $this->start();

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {

		$content = "";

		$textarea_type = $this->p->get( "textarea_type", EDITOR_SIMPLE );
		$editor        = $this->p->get( "editor" );

		$info_array = array(
			"[i]datetime[/i]"    => AL_CURRENT_DATE . " + " . AL_TIME_OF_DAY,
			"[i]date[/i]"        => AL_CURRENT_DATE,
			"[i]time[/i]"        => AL_CURRENT_TIME,
			"[i]ip[/i]"          => AL_IP_ADDRESS,
			"[i]ipsrv[/i]"       => AL_IP_SERVER_ADDRESS,
			"[i]domain[/i]"      => AL_DOMAIN,
			"[i]http_domain[/i]" => AL_DOMAIN . " (http/s)",
			"[i]admindir[/i]"    => AL_ADMIN_DIRECTORY,
			"[i]admin_mail[/i]"  => AL_ADMIN_MAIL,
			"[i]http_web[/i]"    => AL_DOMAIN . " (http/s) + Web Root",
			"[i]year[/i]"        => AL_CURRENT_YEAR

		);
		asort( $info_array );
		$info_array = $this->p->array_split( $info_array );

		$content .= "<div style='text-align:right'>" . $this->l->button_close() . "</div>\n";
		$content .= "<h3 class='underline'>" . AL_TEMPLATE_HELP . "</h3>
";

		$content .= "<p> </p>
        " . $this->l->panel( "<p>" . AL_HELP_OVERVIEW_BINDABLE_VARS . ".<br />
" . AL_HELP_VAR_CONSTRUCTED_AS_FOLLOWS . ": \$ins[" . AL_AREA_VARIABLE . "]</p>
         ", "info", AL_INFORMATION );

		$content .= "
<div class='row'><div class='col-sm-12'><h4>" . AL_VARIABLES . "</h4></div></div>
<div class='row'>\n";
		if ( sizeof( $info_array ) > 0 ) {

			$content .= "
    <div class='col-sm-6'>\n";
			if ( sizeof( $info_array['left'] > 0 ) ) {
				$content .= $this->l->table();
				foreach ( $info_array['left'] as $key => $value ) {
					$content .= "<tr><td style='vertical-align:middle;'>" . $key . "</td><td style='vertical-align:middle;'>" . $value . "</td><td style='vertical-align:middle;'>" .
					            $this->l->button_paste( $editor, $textarea_type, $key ) . "</td></tr>\n";
				}
				$content .= "</table>";
			}
			$content .= "
    </div>
    <div class='col-sm-6'>\n";
			if ( sizeof( $info_array['right'] > 0 ) ) {
				$content .= $this->l->table();
				foreach ( $info_array['right'] as $key => $value ) {
					$content .= "<tr><td style='vertical-align:middle;'>" . $key . "</td><td style='vertical-align:middle;'>" . $value . "</td><td style='vertical-align:middle;'>" .
					            $this->l->button_paste( $editor, $textarea_type, $key ) . "</td></tr>\n";
				}
				$content .= "</table>";
			}
			$content .= "
    </div>\n";
		} else {
			$content .= "<div class='col-sm-12'><p>" . NO_CONTENT_FOUND . "</p>\n";
		}
		$content .= "
</div>
";

		return $content;
	}
}

$class_insert_values = new class_insert_values ();
$content .= $class_insert_values->get_content();