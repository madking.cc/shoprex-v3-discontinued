<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_FILE_DOWNLOAD;

class class_content_downloads extends class_sys {
	public $content;

	public function __construct( $action ) {
		parent::__construct();

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );

		switch ( $action ) {
			case "add_file_from_upload_manager":
				$this->content .= $this->add_file_from_upload_manager();
				$this->content .= $this->start();
				break;
			case "add_all_files_from_download_dir":
				$this->content .= $this->add_all_files_from_download_dir();
				$this->content .= $this->start();
				break;
			case "delete_not_existend_files":
				$this->content .= $this->delete_not_existend_files();
				$this->content .= $this->start();
				break;


			case "content_add":
				$this->content .= $this->content_add();
				break;
			case "content_save":
				$content_error = $this->content_save();
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add();
				}
				break;
			case "content_save_no_back":
				$content_error = $this->content_save( STAY_ON_PAGE );
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
				}
				$this->content .= $this->content_add();
				break;
			case "content_delete":
				$this->content .= $this->content_delete();
				$this->content .= $this->start();
				break;
			case "content_edit":
				$this->content .= $this->content_add( EDIT );
				break;
			case "content_update":
				$content_error = $this->content_update();
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add( EDIT );
				}
				break;
			case "content_update_no_back":
				$content_error = $this->content_update( STAY_ON_PAGE );
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
				}
				$this->content .= $this->content_add( EDIT );
				break;
			case "content_status":
				$this->content .= $this->content_status();
				$this->content .= $this->start();
				break;
			case "options":
				$this->content .= $this->options();
				break;
			case "options_update":
				$this->options_update();
				break;
			case "options_update_and_back":
				$this->options_update( NO_RELOAD );
				$this->content .= $this->start();
				break;
			case "search":
			case "init":
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";


		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );
		$content .= $this->l->display_message_by_session( 'content_added', AL_FILE_ADDED );
		$content .= $this->l->display_message_by_session( 'content_not_added', AL_FILE_NOT_ADDED, "danger" );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_FILE_UPDATED );

		if ( isset( $_SESSION['files_auto_added'] ) ) {
			$content .= $this->l->alert_text_dismiss( "success", $_SESSION['files_auto_added'] . " " . AL_FILES_ADDED );
			unset( $_SESSION['files_auto_added'] );
		}

		if ( isset( $_SESSION['files_auto_deleted'] ) ) {
			$content .= $this->l->alert_text_dismiss( "success", $_SESSION['files_auto_deleted'] . " " . AL_FILES_DELETED );
			unset( $_SESSION['files_auto_deleted'] );
		}

		$content .= "<h3 class='underline'>" . AL_FILE_DOWNLOAD . "</h3>\n";

		$content .= $this->l->form_admin( "", "content_downloads_add.php" ) . $this->l->submit( AL_ADD_NEW_DOWNLOAD ) . "</form>";

		$content .= " " . $this->l->form_admin( "", "content_downloads_options.php" ) . $this->l->submit( AL_SETTINGS ) . "</form>";
		$content .= " " . $this->l->form() . $this->l->button( AL_FILE_DOWNLOAD_MANAGER, "id='file_download_manager'" ) . "</form>";
		$content .= " " . $this->l->form_admin() . $this->l->submit( AL_ADD_FILES_FROM_DOWNLOAD_AUTO ) . $this->l->hidden( "do", "add_all_files_from_download_dir" ) . "</form>";
		$content .= " " . $this->l->form_admin() . $this->l->submit( AL_DELETE_FILES_FROM_DOWNLOAD_AUTO ) . $this->l->hidden( "do", "delete_not_existend_files" ) . "</form>";

		$sql    = "SELECT * FROM `" . TP . "file_download` WHERE `deleted` = '0' ORDER BY `filename`";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows != 0 ) {
			$handler = $this->tbl->tbl_init( AL_TBL_ID, AL_TBL_DISPLAY_NAME, AL_TBL_FILENAME, AL_TBL_SIZE, AL_TBL_COUNTER, AL_TBL_DATE, AL_TBL_ACTIVE, array(
				AL_TBL_ACTION,
				"nosort"
			) );

			$entry_counter = 0;
			while ( $row = $result->fetch_assoc() ) {
				$filename = $row['filename'];
				$filename = htmlspecialchars( $filename, ENT_QUOTES );

				if ( $this->lang->count_of_languages <= 1 ) {
					if ( is_null( $row['name'] ) || empty( $row['name'] ) ) {
						$name = $filename;
					} else {
						$name = $row['name'];
						$name = htmlspecialchars( $name, ENT_QUOTES );
					}
				} else {
					$name = "";
					foreach ( $this->lang->languages as $lang_array ) {
						$name .= $lang_array['text'] . ": ";
						if ( is_null( $row[ 'name' . $lang_array['number'] ] ) || empty( $row[ 'name' . $lang_array['number'] ] ) ) {
							$name .= $filename;
						} else {
							$name_tmp = $row[ 'name' . $lang_array['number'] ];
							$name .= htmlspecialchars( $name_tmp, ENT_QUOTES );
						}
						$name .= "<br /><br />";
					}
				}

				$active = $this->lang->answer( $row['active'] );
				if ( $row['active'] == "0" ) {
					$submit_status = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				} elseif ( $row['active'] == "1" ) {
					$submit_status = $this->l->submit( AL_DEACTIVATE );
				}

				if ( is_null( $row['changed'] ) || $row['changed'] == "0000-00-00 00:00:00" ) {
					$date      = AL_NOT_SPECIFIED_SHORT;
					$timestamp = 0;
				} else {
					$date      = $this->dt->sqldatetime( $row['changed'] );
					$timestamp = strtotime( $date );
				}

				$parameter = "id=" . $row["id"];

				$file_size = @filesize( $this->loc->dir_root . DOWNLOADDIR . $filename );
				if ( $file_size > 0 ) {
					$file_size_display = $this->show_file_size_auto( $file_size );
				} else {
					$file_size_display = AL_FILE_DOES_NOT_EXIST;
				}

				$this->tbl->tbl_load( $handler, $row['id'], $name, $filename, array(
					$file_size,
					"file_size",
					$file_size_display
				), $row['count'], array( $timestamp, "time", $date ), $active,

					$this->l->form_submit_by_lang( INSERT_BREAKS, EMPTY_FREETEXT, "content_downloads_edit.php", $parameter ) . "<br />" .

					$this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "do", "content_status" ) . $submit_status . "</form> " .
					$this->l->form_admin( "id=\"form_admin_delete_content_$entry_counter\"" ) . $this->l->hidden( "do", "content_delete" ) . $this->l->hidden( "id", $row["id"] ) . $this->l->button( AL_DELETE, "id=\"button_delete_content_$entry_counter\"" ) . "</form>" );
				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_FILE_PART01 . " \"" . $row['filename'] . "\" " . AL_DELETE_FILE_PART02, "#button_delete_content_$entry_counter", "form_admin_delete_content_$entry_counter", "confirm_delete_$entry_counter" );

				$entry_counter ++;
			}

			$content .= $this->tbl->tbl_out( $handler, 1 );
		} else {
			$content .= "<h3>" . AL_NO_ENTRIES . "</h3>";
		}

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_FILE_START, "info", AL_INFORMATION );

		$uploaddir = urldecode(  DOWNLOADDIR );


		$content .= $this->l->form_admin( "id='upload_manager_form_submit'" ) . $this->l->hidden( "do", "add_file_from_upload_manager" ) . $this->l->hidden( "file_to_add", "", "id='file_to_add'" ) . "</form>";

		$GLOBALS['body_footer'] .= "<script>

$(document).ready(function() {
    $('#file_download_manager').click(function() {
        $.colorbox({href:'/" . WEBROOT . ADMINDIR . "upload_manager.php?element=file_to_add&submit=upload_manager_form_submit&delete=0&open=0&rename=0&uploaddir=$uploaddir', iframe:true, width:'" . $GLOBALS['box_width'] . "', height:'" . $GLOBALS['box_height'] . "' });
    });
});

</script>\n";

		return $content;
	}

	public function options() {
		$content = "";


		$GLOBALS['admin_subtitle'] = AL_FILE_DOWNLOAD_OPTIONS;

		$content .= "<h3 class='underline'>" . AL_GLOBAL_DOWNLOAD_OPTIONS . "</h3>\n";

		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "options_update" ) . $this->l->table() . "
    <tr><td></td><td>" . $this->l->submit( AL_SAVE ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='save_and_back(this.form);'" ) . " " . $this->l->back_button( AL_BACK, "content_downloads.php" ) . "</td></tr>
    <tr><td>" . AL_ALLOW_GLOBAL_DOWNLOAD . ":</td><td>" . $this->l->select_yesno( "content_download_available", CONTENT_DOWNLOAD_AVAILABLE ) . "</td></tr>
    </table></form>\n";

		$GLOBALS['body_footer'] .= "<script>
    function save_and_back(formID)
    {
        formID.do.value = 'options_update_and_back';
        formID.action = '/" . WEBROOT . ADMINDIR . "content_downloads.php';
        formID.submit();
    }
    </script>
    ";

		return $content;
	}

	public function options_update( $reload = true ) {


		$elements                               = array();
		$elements['content_download_available'] = $this->p->get( "content_download_available", "1" );

		foreach ( $elements as $key => $value ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "settings", array( "value" ), array( $value ), "UPPER(`key`) LIKE UPPER('$key')" );

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Settings updated, key: " . $key . ", value: " . $value );
		}

		$_SESSION['options_updated'] = 1;
		if ( $reload ) {
			$this->l->reload_js();
		}
	}

	public function content_add( $edit = false ) {
		$content = "";


		if ( $edit ) {
			$GLOBALS['admin_subtitle'] = AL_EDIT_FILE_DOWNLOAD;
		} else {
			$GLOBALS['admin_subtitle'] = AL_ADD_FILE_DOWNLOAD;
		}

		$content .= $this->l->display_message_by_session( 'content_added', AL_FILE_ADDED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_FILE_UPDATED );

		if ( $edit ) {
			$content .= "<h3 class='underline'>" . AL_EDIT_FILE_DOWNLOAD . "</h3>\n";
		} else {
			$content .= "<h3 class='underline'>" . AL_ADD_FILE_DOWNLOAD . "</h3>\n";
		}

		$lang_number        = $this->p->get( "lang" );
		$import_lang_number = $this->p->get( "import_lang" );

		$override = 0;

		if ( $edit ) {
			$id = $this->p->get( "id" );
			if ( empty( $id ) || ! is_numeric( $id ) ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_FILEDOWNLOAD );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
				$content .= start();

				return $content;
			} else {
				$sql    = "SELECT * FROM `" . TP . "file_download` WHERE `id` = '$id' AND `deleted` = '0'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				if ( $result->num_rows != 1 ) {
					$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_FILEDOWNLOAD );
					$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
					$content .= start();

					return $content;
				} else {
					$row            = $result->fetch_assoc();
					$row['changed'] = $this->dt->sqldatetime( $row['changed'] );
					$old_name       = $row['name'];
					$old_filename   = $row['filename'];
				}
			}
		} else {
			$old_name                     = $this->p->get( "old_name", "" );
			$old_filename                 = $this->p->get( "old_filename", "" );
			$row[ 'name' . $lang_number ] = $this->p->get( "name", "" );
			$row['filename']              = $this->p->get( "filename", "" );
			$row['active']                = $this->p->get( "active", "1" );
			$id                           = $this->p->get( "id", "" );
			$override                     = $this->p->get( "override", "0" );
		}

		$content .= $this->l->table();

		if ( $edit ) {
			if ( $this->lang->count_of_languages > 1 ) {
				$content .= "<tr>";
				$content .= "<td>" . AL_TBL_LANGUAGE_FUNCTIONS . "</td><td>";
				$content .= $this->l->language_import_fields( "content_downloads_edit.php", $id, $lang_number ) . $this->l->language_edit_fields( "content_downloads_edit.php", $id, $lang_number ) . "</td>
            </tr>";
			}

			$content .= "<tr><td></td><td>";
			$content .= $this->l->form_admin_file( "name='form_action'", "content_downloads_edit.php", "id=$id" );
			$content .= $this->l->hidden( "do", "content_update" );
			$content .= $this->l->hidden( "do_no_back", "content_update_no_back" ) . "";
		} else {
			$content .= "<tr><td></td><td>";
			$content .= $this->l->form_admin_file( "name='form_action'", "content_downloads_add.php" );
			$content .= $this->l->hidden( "do", "content_save" );
			$content .= $this->l->hidden( "do_no_back", "content_save_no_back" );
		}

		if ( ! is_null( $import_lang_number ) ) {
			if ( strcasecmp( $import_lang_number, "empty" ) === 0 ) {
				$import_lang_number = "";
			}
			$row[ 'name' . $lang_number ] = $row[ 'name' . $import_lang_number ];
		}

		$content .= $this->l->hidden( "id", $id ) . $this->l->hidden( "old_name", $old_name ) . $this->l->hidden( "old_filename", $old_filename ) . "
    " . $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='check_submit(this.form, false)'" ) . " ";

		if ( $edit ) {
			$content .= $this->l->reload_button( "content_downloads_edit.php", "id=$id&lang=$lang_number", AL_RELOAD ) . " ";
		} else {
			$content .= $this->l->reload_button( "content_downloads_add.php", NO_PARAMETER, AL_RELOAD ) . " ";
		}
		$content .= " " . $this->l->back_button( AL_BACK, "content_downloads.php" ) . "</td></tr>
    ";

		if ( $edit ) {
			$content .= "<tr><td>" . AL_TBL_ID . "</td><td>" . $id . "</td></tr>\n";
		}

		if ( $this->lang->count_of_languages > 1 ) {
			$content .= "<tr><td>" . AL_TBL_CURRENT_LANGUAGE . "</td><td><strong>" . $this->p->get_language_text( $lang_number ) . $this->l->hidden( "lang", $lang_number ) . "</strong></td></tr>\n";
		} else {
			$content .= $this->l->hidden( "lang", $this->lang->lnbrm );
		}

		$content .= "
    <tr><td>";

		if ( $edit ) {
			$content .= AL_TBL_OVERWRITE_ENTRY;
		} else {
			$content .= AL_TBL_ADD_FILE;
		}

		$content .= "</td><td>" . $this->l->file( "file_to_upload" ) . "</td></tr>
    <tr><td>" . AL_TBL_DISPLAY_NAME . $this->p->get_lhint() . "</td><td>" . $this->l->text( "name", $row[ 'name' . $lang_number ] ) . "</td></tr>\n";

		$content .= "<tr><td>" . AL_TBL_FILENAME . "</td><td>" . $this->l->text( "filename", $row['filename'] ) . "</td></tr>";

		$content .= "<tr><td>" . AL_TBL_OVERWRITE_EXISTING_FILE . "</td><td>" . $this->l->checkbox( "override", "1", $override ) . " </td></tr>\n";

		$content .= "
    <tr><td>" . AL_TBL_ACTIVE . "</td><td>" . $this->l->select_yesno( "active", $row['active'] ) . "</td></tr>

    <tr><td></td><td>" . $this->l->submit( AL_SAVE ) . " " . $this->l->back_button( AL_BACK, "content_downloads.php" ) . "</form></td></tr>
    </table>
    ";

		if ( $this->lang->count_of_languages > 1 ) {
			$language_hint = AL_HELP_LANGUAGE_HINT;
		} else {
			$language_hint = "";
		}

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_FILE_EDIT . $language_hint, "info", AL_INFORMATION );

		$GLOBALS['body_footer'] .= "<script>
    function check_submit(formID, no_back)
    {
        if(no_back)
        {
            formID.do.value = formID.do_no_back.value;
        }
        formID.submit();
    }
    </script>
    ";

		return $content;
	}

	public function content_save( $stay_on_page = false ) {
		$content = "";


		$lang_number = $this->p->get( "lang" );

		if ( ! $this->l->get_preview_status() ) {

			$name     = $this->p->get( "name", "" );
			$override = $this->p->get( "override", "0" );
			$active   = $this->p->get( "active", 1 );
			$filename = $this->p->get( "filename", "" );
			$filename = trim( $filename );

			$sql    = "SELECT `id` FROM `" . TP . "file_download` WHERE `name" . $lang_number . "` LIKE '$name' AND `deleted` = '0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows > 0 ) {
				$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02 );

				return $content;
			} elseif ( isset( $_FILES['file_to_upload'] ) && ! empty( $_FILES['file_to_upload']['name'] ) ) {

				if ( ! empty( $filename ) ) {
					$file_name_original = $filename;
				} else {
					$file_name_original = trim( $_FILES['file_to_upload']['name'] );
				}

				// Bestimme die Formatierung des Dateinamens:
				if ( ! $override ) {
					// Don't overwrite existing Files:
					$upload_file_destination = $this->p->check_filename( DOWNLOADDIR . $file_name_original );
					if ( $upload_file_destination == false ) {
						$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $file_name_original . "''";

						$content .= $this->l->alert_text( "danger", $error );
						$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$file_name_original'" );

						return $content;
					}
				} else {
					// Overwrite existing files:
					$upload_file_destination = $this->translate_special_characters( $file_name_original, IS_FILE, DO_UNDERLINE );
				}
				$upload_file_destination = $this->loc->dir_root . DOWNLOADDIR . $upload_file_destination;

				// Die Original Datei hochladen:
				if ( move_uploaded_file( $_FILES['file_to_upload']['tmp_name'], $upload_file_destination ) ) {
					$this->log->event( "file", __FILE__ . ":" . __LINE__, "Download File uploaded: " . $upload_file_destination );
				} else {

					$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_SOURCE_PATH . ": '" . $_FILES['file_to_upload']['tmp_name'] . "'<br />
                        " . AL_DESTINATION_PATH . ": '" . $upload_file_destination . "'";

					$content .= $this->l->alert_text( "danger", $error );
					$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file to his location. Sourcepath: '" . $_FILES['file_to_upload']['tmp_name'] . "', destinationpath: '" . $upload_file_destination . "'" );

					return $content;
				}
			} else {

				$error = AL_FILE_FORM_ERROR;

				$content .= $this->l->alert_text( "danger", $error );
				$this->log->error( "file", __FILE__ . ":" . __LINE__, "File not correct delivered from form." );

				return $content;
			}

			$file_name_db = $this->p->get_filename_and_type( $upload_file_destination );

			$this->db->ins( __FILE__ . ":" . __LINE__, "file_download", array(
				"name" . $lang_number,
				"filename",
				"active"
			), array( $name, $file_name_db, $active ) );
			$id = $this->db->get_insert_id();
			$this->log->event( "log", __FILE__ . ":" . __LINE__, "New filedownload added, filename: '$file_name_db', id: '" . $id . "'" );
		}

		$_SESSION['content_added'] = 1;

		if ( $stay_on_page ) {
			$this->l->reload_js( "content_downloads_edit.php", "id=$id" );
		} else {
			$this->l->reload_js( "content_downloads.php" );
		}

		return $content;
	}

	public function add_file_from_upload_manager( $stay_on_page = false ) {
		$content = "";


		$file_to_add = $this->p->get( "file_to_add" );

		if ( ! empty( $file_to_add ) ) {

			$this->db->ins( __FILE__ . ":" . __LINE__, "file_download", array(
				"filename",
				"active"
			), array( $file_to_add, true ) );
			$id = $this->db->get_insert_id();
			$this->log->event( "log", __FILE__ . ":" . __LINE__, "New filedownload added, filename: '$file_to_add', id: '" . $id . "'" );
			$_SESSION['content_added'] = 1;
		} else {
			$_SESSION['content_not_added'] = 1;
		}

		return $content;
	}


	public function content_update( $stay_on_page = false ) {
		$content = "";


		$lang_number = $this->p->get( "lang" );

		if ( ! $this->l->get_preview_status() ) {
			$id = $this->p->get( "id" );

			$name         = $this->p->get( "name", "" );
			$old_name     = $this->p->get( "old_name", "" );
			$filename     = $this->p->get( "filename", "" );
			$filename     = trim( $filename );
			$old_filename = $this->p->get( "old_filename", "" );
			$override     = $this->p->get( "override", "0" );
			$active       = $this->p->get( "active", 1 );

			if ( empty( $id ) || ! is_numeric( $id ) ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_DOWNLOAD_FILE_NOT_UPDATED );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: '" . $id . "'" );
				$content .= start();

				return $content;
			} elseif ( $name != $old_name ) {
				$sql    = "SELECT `id` FROM `" . TP . "file_download` WHERE `name" . $lang_number . "` LIKE '$name' AND `deleted` = '0'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				if ( $result->num_rows > 0 ) {
					$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02 );

					return $content;
				}
			}

			// If File Uploaded:
			if ( isset( $_FILES['file_to_upload'] ) && ! empty( $_FILES['file_to_upload']['name'] ) ) {

				if ( ! empty( $filename ) ) {
					$file_name_original = $filename;
				} else {
					$file_name_original = trim( $_FILES['file_to_upload']['name'] );
				}

				// Bestimme die Formatierung des Dateinamens:
				if ( ! $override ) {
					$upload_file_destination = $this->p->check_filename( DOWNLOADDIR . $file_name_original );
					if ( $upload_file_destination == false ) {
						$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $file_name_original . "''";

						$content .= $this->l->alert_text( "danger", $error );
						$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$file_name_original'" );

						return $content;
					}
				} else {
					$upload_file_destination = $this->translate_special_characters( $file_name_original, IS_FILE, DO_UNDERLINE );
				}
				$upload_file_destination = $this->loc->dir_root . DOWNLOADDIR . $upload_file_destination;

				// Die Original Datei hochladen:
				if ( move_uploaded_file( $_FILES['file_to_upload']['tmp_name'], $upload_file_destination ) ) {
					$this->log->event( "file", __FILE__ . ":" . __LINE__, "Download File uploaded: '" . $upload_file_destination . "''" );
				} else {

					$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_SOURCE_PATH . ": '" . $_FILES['file_to_upload']['tmp_name'] . "''<br />
                        " . AL_DESTINATION_PATH . ": '" . $upload_file_destination . "'";

					$content .= $this->l->alert_text( "danger", $error );
					$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't move file to his location. Sourcepath: '" . $_FILES['file_to_upload']['tmp_name'] . "', destinationpath: '" . $upload_file_destination . "'" );

					return $content;
				}
			} else {
				// Rename File
				if ( ! empty( $filename ) && $filename != $old_filename ) {
					if ( ! $override ) {
						// Don't overwrite existing Files
						$filename                = $this->translate_special_characters( $filename, IS_FILE, DO_UNDERLINE );
						$upload_file_destination = $this->p->check_filename( DOWNLOADDIR . $filename );
						if ( $upload_file_destination == false ) {
							$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $filename . "''";

							$content .= $this->l->alert_text( "danger", $error );
							$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$filename'" );

							return $content;
						} elseif ( $filename != $upload_file_destination ) {
							$error = AL_ERROR_WITH_FILE_UPLOAD . "<br/>
                        " . AL_NEW_FILENAME_ALREADY_EXIST . ": '" . $filename . "''";

							$content .= $this->l->alert_text( "danger", $error );
							$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename already exist, can't rename: '$filename'" );

							return $content;
						} else {
							$upload_file_destination = DIRROOT . DOWNLOADDIR . $filename;
						}
					} else {
						// Overwrite existing files
						$upload_file_destination = DIRROOT . DOWNLOADDIR . $filename;

						if ( is_file( $upload_file_destination ) ) {
							if ( ! @unlink( $upload_file_destination ) ) {
								$error = AL_ERROR_WITH_FILE_OVERWRITE . "<br/>
                        " . AL_EXISTING_FILE . " '" . $upload_file_destination . "'";

								$content .= $this->l->alert_text( "danger", $error );
								$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't overwrite existing file. File to unlink: '" . $upload_file_destination . "'" );

								return $content;
							}
						}
					}

					$old_filename = DIRROOT . DOWNLOADDIR . $old_filename;

					if ( ! @rename( $old_filename, $upload_file_destination ) ) {
						$error = AL_ERROR_CANT_RENAME_FILE . "<br/>
                        " . AL_OLD_FILENAME . ": '" . $old_filename . "'<br />
                        " . AL_DESIRED_NAME . ": '" . $upload_file_destination . "'";

						$content .= $this->l->alert_text( "danger", $error );
						$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't rename file. From: '" . $old_filename . "'', to: '" . $filename . "''" );

						return $content;
					}
				}
			}

			if ( empty( $filename ) ) {
				$filename = $old_filename;
			}
			$filename_db = $this->p->get_filename_and_type( $filename );

			$this->db->upd( __FILE__ . ":" . __LINE__, "file_download", array(
				"name" . $lang_number,
				"filename",
				"active"
			), array( $name, $filename_db, $active ), "`id` = '$id'" );

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Filedownload updated, filename: '$filename'', id: '" . $id . "''" );
		}

		$_SESSION['content_updated'] = 1;

		if ( ! $stay_on_page ) {
			$this->l->reload_js( "content_downloads.php" );
		}

		return $content;
	}

	public function delete_not_existend_files() {


		$sql          = "SELECT * FROM `" . TP . "file_download` WHERE `deleted` = '0'";
		$result       = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		$filenames_db = array();
		$files        = $this->get_files( DOWNLOADDIR );
		if ( ! isset( $files ) || ! is_array( $files ) ) {
			$files = array();
		}
		$_SESSION['files_auto_deleted'] = 0;
		while ( $row = $result->fetch_assoc() ) {
			if ( in_array( $row['filename'], $files ) ) {
				continue;
			} else {

				$this->db->upd( __FILE__ . ":" . __LINE__, "file_download", array( "deleted" ), array( true ), "`id` = '" . $row['id'] . "'" );
				$id = $this->db->get_insert_id();
				$this->log->event( "log", __FILE__ . ":" . __LINE__, "File deleted in db, filename: '" . $row['filename'] . "', id: '" . $id . "'" );
				$_SESSION['files_auto_deleted'] ++;
			}

		}


	}


	public function add_all_files_from_download_dir() {


		$sql          = "SELECT `filename` FROM `" . TP . "file_download` WHERE `deleted` = '0'";
		$result       = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		$filenames_db = array();
		while ( $row = $result->fetch_assoc() ) {
			$filenames_db[] = $row['filename'];
		}

		$files = $this->get_files( DOWNLOADDIR );

		$_SESSION['files_auto_added'] = 0;
		if ( isset( $files ) && is_array( $files ) && sizeof( $files ) > 0 ) {
			foreach ( $files as $filename ) {
				if ( in_array( $filename, $filenames_db ) ) {
					continue;
				} else {

					$this->db->ins( __FILE__ . ":" . __LINE__, "file_download", array(
						"filename",
						"active"
					), array( $filename, true ) );
					$id = $this->db->get_insert_id();
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "New filedownload added, filename: '$filename', id: '" . $id . "'" );
					$_SESSION['files_auto_added'] ++;
				}

			}
		}


	}

	public function content_delete() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT * FROM `" . TP . "file_download` WHERE `deleted` = '0' AND `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows == 1 ) {

				$row             = $result->fetch_assoc();
				$filename        = DIRROOT . DOWNLOADDIR . $row['filename'];
				$delete_filename = DOWNLOADDIR . "delete/" . $row['filename'];
				$tmp             = $delete_filename;
				$delete_filename = $this->p->check_filename( $delete_filename, NO_DIRROOT );
				if ( $delete_filename == false ) {
					$error = AL_ERROR_WITH_FILE_DELETE . "<br/>
                        " . AL_FILENAME_NOT_ALLOWED . ": '" . $tmp . "''";

					$content .= $this->l->alert_text( "danger", $error );
					$this->log->error( "file", __FILE__ . ":" . __LINE__, "Filename not allowed: '$tmp'" );

					return $content;
				} else {
					if ( ! @rename( $filename, DIRROOT . DOWNLOADDIR . "delete/" . $delete_filename ) ) {
						$error = AL_ERROR_WITH_FILE_MOVE . "<br/>
                        " . AL_SOURCE_PATH . ": '" . $filename . "'<br />
                        " . AL_DESTINATION_PATH . ": '" . DIRROOT . DOWNLOADDIR . "delete/" . $delete_filename . "'";

						$content .= $this->l->alert_text( "danger", $error );
						$this->log->error( "file", __FILE__ . ":" . __LINE__, "Can't rename file. From: '" . $filename . "'', to: '" . DOWNLOADDIR . "delete/" . $delete_filename . "''" );
					} else {
						$content .= $this->l->alert_text_dismiss( "success", AL_FILE_DELETED_DISK );
						$this->log->event( "file", __FILE__ . ":" . __LINE__, "File renamed from: '" . $filename . "'', to: '" . DOWNLOADDIR . "delete/" . $delete_filename . "''" );
					}

					$this->db->upd( __FILE__ . ":" . __LINE__, "file_download", array( "deleted" ), array( true ), "`id` = '$id'" );
					$content .= $this->l->alert_text_dismiss( "success", AL_FILE_DELETED_DB );
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "file marked as deleted in database, id: '" . $id . "'. File '$filename' moved to '" . DOWNLOADDIR . "delete/" . "$delete_filename'" );

					return $content;
				}
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_ERROR_WITH_FILE_DELETE );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}
	}

	public function content_status() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT `active` FROM `" . TP . "file_download` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();
				if ( $row['active'] == 0 ) {
					$active = 1;
				} else {
					$active = 0;
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "file_download", array( "active" ), array( $active ), "`id` = '$id'" );

				$content .= $this->l->alert_text_dismiss( "success", AL_FILE_STATUS_CHANGED );
				$this->log->event( "log", __FILE__ . ":" . __LINE__, "Filedownload status changed, id: " . $id );

				return $content;
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_FILE_STATUS );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_FILE_STATUS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}
	}

}

if ( ! isset( $action ) ) {
	$action = null;
}

$class_content_downloads = new class_content_downloads( $action );
$content .= $class_content_downloads->get_content();