<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

class class_content_single_text extends class_sys {
	public $content;

	public function __construct( $action ) {
		parent::__construct();

		$GLOBALS['admin_subtitle'] = AL_ONELINER;

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );

		switch ( $action ) {
			case "content_add":
				$this->content .= $this->content_add();
				break;
			case "content_save":
				$content_error = $this->content_save();
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add();
				}
				break;
			case "content_save_no_back":
				$content_error = $this->content_save( STAY_ON_PAGE );
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add();
				}
				break;
			case "content_delete":
				$this->content .= $this->content_delete();
				$this->content .= $this->start();
				break;
			case "content_edit":
				$this->content .= $this->content_add( EDIT );
				break;
			case "content_update":
				$content_error = $this->content_update();
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add( EDIT );
				}
				break;
			case "content_update_no_back":
				$content_error = $this->content_update( STAY_ON_PAGE );
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
				}
				$this->content .= $this->content_add( EDIT );
				break;
			case "content_status":
				$this->content .= $this->content_status();
				$this->content .= $this->start();
				break;
			case "search":
			case "init":
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";


		$content .= $this->l->display_message_by_session( 'content_added', AL_ONELINER_ADDED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_ONELINER_UPDATED );

		$reset_search = $this->p->get( "reset_search", 0 );
		$search       = $this->p->get_session( "search", "", "text_search", $reset_search );
		$intext       = $this->p->get_session( "intext", 0, "text_intext", $reset_search );

		if ( empty( $search ) ) {
			$content .= "<h3 class='underline'>" . AL_ONELINER . "</h3>\n";
		} else {
			$content .= "<h3 class='underline'>" . AL_ONELINER . " " . AL_SEARCH_RESULT . "</h3>\n";
		}

		if ( empty( $search ) ) {
			$sql    = "SELECT * FROM `" . TP . "text_single` WHERE `deleted` = '0' ORDER BY `name`";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$search_text = "";
			if ( $intext ) {
				$search_text = "OR " . $this->db->search( "text", $search );
			}
			$sql    = "SELECT * FROM `" . TP . "text_single` WHERE (" . $this->db->search( "name", $search ) . " $search_text) AND `deleted`=0 ORDER BY 'name'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		}

		$content .= $this->l->form_admin( "", "content_single_text_add.php" ) . $this->l->submit( AL_ADD_NEW_ONELINER ) . "</form>";

		if ( $result->num_rows > 0 || ! empty( $search ) ) {

			$content .= " " . $this->l->form_admin( "name='form_search'" ) . $this->l->text( "search", $search, "250", "id='search_text'" ) . $this->l->hidden( "do", "search" ) . " " . $this->l->submit( AL_SEARCH, "id='search'" ) . " <nobr>" . $this->l->checkbox( "intext", "1", $intext, "0", "style='display:inline'" ) . " " . AL_IN_TEXT . "</nobr>
                <span id='message' class='error'>| " . AL_ERROR . ": " . AL_MISSING_SEARCH_TEXT . "</span>\n";
			$content .= "    </form>\n";

			if ( ! empty( $search ) ) {
				$content .= $this->l->form_admin() . $this->l->hidden( "reset_search", "1" ) . $this->l->submit( AL_RESET );
				$content .= "    </form>\n";
			}

			$GLOBALS['body_footer'] .= "
        <script>

            $(document).ready(function () {
                $(\"#message\").hide();
            });
            $(\"form[name=form_search]\").submit(function( event ) {
                var val = $(\"input[name=search]\").val();
                if(val == '')
                {
                    $(\"#message\").fadeIn();
                    event.preventDefault();
                }
                else
                {
                    $(\"#message\").fadeOut();
                }
            });
             </script>\n";
		}

		if ( $result->num_rows != 0 ) {
			$handler = $this->tbl->tbl_init( "ID:", AL_TBL_NAME, AL_TBL_CONTENT, AL_TBL_DATE, array(
				AL_TBL_CHARS,
				"nosort"
			), AL_TBL_ACTIVE, array( AL_TBL_ACTION, "nosort" ) );

			$entry_counter = 0;
			while ( $row = $result->fetch_assoc() ) {
				$text = $this->l->fix_ta( $row[ 'text' . $this->lang->lnbrm ] );
				$text = $this->l->ta( "text_" . $entry_counter, $text, "500", "90", "readonly=\"readonly\"" );

				$active = $this->lang->answer( $row['active'] );
				if ( $row['active'] == "0" ) {
					$submit_status = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				} elseif ( $row['active'] == "1" ) {
					$submit_status = $this->l->submit( AL_DEACTIVATE );
				}

				if ( $row['changed'] == "0000-00-00 00:00:00" ) {
					$date = $this->dt->sqldatetime( $row['created'] );
				} else {
					$date = $this->dt->sqldatetime( $row['changed'] );
				}
				$timestamp = strtotime( $date );

				$parameter = "id=" . $row["id"];

				$chars = $this->p->strlen_language_all( "text_single", "text", $row['id'] );

				$this->tbl->tbl_load( $handler, $row['id'], $row['name'], $text, array(
					$timestamp,
					"time",
					$date
				), $chars, $active,

					$this->l->form_submit_by_lang( INSERT_BREAKS, EMPTY_FREETEXT, "content_single_text_edit.php", $parameter ) . "<br />" .

					$this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "do", "content_status" ) . $submit_status . "</form> " .
					$this->l->form_admin( "id=\"form_admin_delete_content_$entry_counter\"" ) . $this->l->hidden( "do", "content_delete" ) . $this->l->hidden( "id", $row["id"] ) . $this->l->button( AL_DELETE, "id=\"button_delete_content_$entry_counter\"" ) . "</form>" );
				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_ONELINER_PART01 . " \"" . $row['name'] . "\" " . AL_DELETE_ONELINER_PART02, "#button_delete_content_$entry_counter", "form_admin_delete_content_$entry_counter", "confirm_delete_$entry_counter" );

				$entry_counter ++;
			}

			$content .= $this->tbl->tbl_out( $handler, 1 );
		} else {
			if ( ! empty( $search ) ) {
				$content .= "<h3>" . AL_NO_MATCHES . "</h3>";
			} else {
				$content .= "<h3>" . AL_NO_ENTRIES . "</h3>";
			}
		}

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_ONELINER_START, "info", AL_INFORMATION );

		return $content;
	}

	public function content_add( $edit = false ) {
		$content = "";


		if ( $edit ) {
			$GLOBALS['admin_subtitle'] = AL_EDIT_ONELINER;
		} else {
			$GLOBALS['admin_subtitle'] = AL_ADD_ONELINER;
		}

		$content .= $this->l->display_message_by_session( 'content_added', AL_ONELINER_ADDED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_ONELINER_UPDATED );

		if ( $edit ) {
			$content .= "<h3 class='underline'>" . AL_EDIT_ONELINER . "</h3>\n";
		} else {
			$content .= "<h3 class='underline'>" . AL_ADD_ONELINER . "</h3>\n";
		}

		$lang_number        = $this->p->get( "lang" );
		$import_lang_number = $this->p->get( "import_lang" );

		if ( $edit ) {
			$id = $this->p->get( "id" );
			if ( empty( $id ) || ! is_numeric( $id ) ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_ONELINER );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: '" . $id . "'" );
				$content .= start();

				return $content;
			} else {
				$sql    = "SELECT * FROM `" . TP . "text_single` WHERE `id` = '$id'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				if ( $result->num_rows != 1 ) {
					$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_ONELINER );
					$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: '" . $id . "'', SQL: '" . $sql . "'" );
					$content .= start();

					return $content;
				} else {
					$row      = $result->fetch_assoc();
					$old_name = $row['name'];
				}
			}
		} else {
			$old_name                     = $this->p->get( "old_name", "" );
			$row['name']                  = $this->p->get( "name", "" );
			$row[ 'text' . $lang_number ] = $this->p->get( "text", "", NOT_ESCAPED );
			$row['active']                = $this->p->get( "active", "1" );
			$id                           = $this->p->get( "id", "" );
		}

		if ( ! is_null( $import_lang_number ) ) {
			if ( strcasecmp( $import_lang_number, "empty" ) === 0 ) {
				$import_lang_number = "";
			}
			$row[ 'text' . $lang_number ] = $row[ 'text' . $import_lang_number ];
		}

		$content .= $this->l->table();

		if ( $edit ) {
			if ( $this->lang->count_of_languages > 1 ) {
				$content .= "<tr>";
				$content .= "<td>" . AL_TBL_LANGUAGE_FUNCTIONS . "</td><td>";
				$content .= $this->l->language_import_fields( "content_single_text_edit.php", $id, $lang_number ) . $this->l->language_edit_fields( "content_single_text_edit.php", $id, $lang_number ) . "</td>
            </tr>";
			}

			$content .= "<tr><td></td><td>";
			$content .= $this->l->form_admin( "name='form_action'", "content_single_text_edit.php", "id=$id" );
			$content .= $this->l->hidden( "do", "content_update" );
			$content .= $this->l->hidden( "do_no_back", "content_update_no_back" );
		} else {
			$content .= "<tr><td></td><td>";
			$content .= $this->l->form_admin( "name='form_action'", "content_single_text_add.php" );
			$content .= $this->l->hidden( "do", "content_save" );
			$content .= $this->l->hidden( "do_no_back", "content_save_no_back" );
		}

		$content .= $this->l->hidden( "id", $id ) . $this->l->hidden( "old_name", $old_name ) . $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='check_submit(this.form, false)'" ) . " ";
		if ( $edit ) {
			$content .= $this->l->reload_button( "content_single_text_edit.php", "id=$id&lang=$lang_number", AL_RELOAD ) . " ";
		} else {
			$content .= $this->l->reload_button( "content_single_text_add.php", NO_PARAMETER, AL_RELOAD ) . " ";
		}

		$content .= $this->l->back_button( AL_BACK, "content_single_text.php" ) . "</td></tr>
    <tr><td>" . AL_TBL_NAME . "</td><td>" . $this->l->text( "name", $row['name'] ) . "</td></tr>\n";

		if ( $edit ) {
			$content .= "<tr><td>" . AL_TBL_ID . "<td>" . $id . "</td></tr>";
		}

		if ( $this->lang->count_of_languages > 1 ) {
			$content .= "<tr><td>" . AL_TBL_CURRENT_LANGUAGE . "</td><td><strong>" . $this->p->get_language_text( $lang_number ) . $this->l->hidden( "lang", $lang_number ) . "</strong></td></tr>\n";
		} else {
			$content .= $this->l->hidden( "lang", $this->lang->lnbrm );
		}

		$content .= "
    <tr><td>" . AL_TBL_TEXT . $this->p->get_lhint() . "</td><td>" . $this->l->text( "text", $row[ 'text' . $lang_number ], "700" ) . "</td></tr>
    <tr><td>" . AL_TBL_ACTIVE . "</td><td>" . $this->l->select_yesno( "active", $row['active'] ) . "</td></tr>

    <tr><td></td><td>" . $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " . $this->l->back_button( AL_BACK, "content_single_text.php" ) . "</td></tr>
    </table></form>
    ";

		$GLOBALS['body_footer'] .= "<script>
    function check_submit(formID, no_back)
    {
        str = formID.name.value;
        if(str == '')
        {
            " . $this->l->box_alert( "<H3 class='error underline'>" . AL_ERROR . "</H3><p>" . AL_MUST_ENTER_NAME . "</p>" ) . "
        }
        else
        {
            if(no_back)
            {
                formID.do.value = formID.do_no_back.value;
            }
            formID.submit();
        }
    }
    </script>
    ";

		if ( $this->lang->count_of_languages > 1 ) {
			$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_LANGUAGE_HINT, "info", AL_INFORMATION );
		}

		return $content;
	}

	public function content_save( $stay_on_page = false ) {
		$content = "";


		$lang_number = $this->p->get( "lang" );

		$name   = $this->p->get( "name", "", NOT_ESCAPED );
		$text   = $this->p->get( "text" );
		$active = $this->p->get( "active", 1 );

		$name = trim( $name );
		$name = $this->translate_special_characters( $name, NOT_A_FILE, NO_UNDERLINE );
		$name = $this->db->escape( $name );
		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME, HISTORY_BACK );

			return $content;
		}

		$sql    = "SELECT `id` FROM `" . TP . "text_single` WHERE `name` LIKE '$name'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02, HISTORY_BACK );

			return $content;
		}

		$this->db->ins( __FILE__ . ":" . __LINE__, "text_single", array(
			"name",
			"text" . $lang_number,
			"active",
			"created"
		), array( $name, $text, $active, "NOW()" ) );

		$id = $this->db->get_insert_id();

		$this->log->event( "log", __FILE__ . ":" . __LINE__, "New content_single_text added, name: '$name', id: '" . $id . "'" );

		$_SESSION['content_added'] = 1;

		if ( $stay_on_page ) {
			$this->l->reload_js( "content_single_text_edit.php", "id=$id" );
		} else {
			$this->l->reload_js( "content_single_text.php" );
		}

		return $content;
	}

	public function content_update( $stay_on_page = false ) {
		$content = "";


		$lang_number = $this->p->get( "lang" );

		$id       = $this->p->get( "id" );
		$text     = $this->p->get( "text", "" );
		$active   = $this->p->get( "active", 1 );
		$name     = $this->p->get( "name", "", NOT_ESCAPED );
		$old_name = $this->p->get( "old_name" );

		$name = trim( $name );
		$name = $this->translate_special_characters( $name, NOT_A_FILE, NO_UNDERLINE );
		$name = $this->db->escape( $name );
		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME, HISTORY_BACK );

			return $content;
		} elseif ( $old_name != $name ) {
			$sql    = "SELECT `id` FROM `" . TP . "text_single` WHERE `name` LIKE '$name'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 0 ) {
				$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02, HISTORY_BACK );

				return $content;
			}
		}

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_UPDATE_ONELINER );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: '" . $id . "'" );
			$content .= start();

			return $content;
		} else {
			$this->db->upd( __FILE__ . ":" . __LINE__, "text_single", array(
				"name",
				"text" . $lang_number,
				"active"
			), array( $name, $text, $active ), "`id` = '$id'" );

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content_text changed, name: '$name', id: '" . $id . "'" );

			$_SESSION['content_updated'] = 1;

			if ( ! $stay_on_page ) {
				$this->l->reload_js( "content_single_text.php" );
			}

			return $content;
		}
	}

	public function content_delete() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "text_single", array( "deleted" ), array( true ), "`id` = '$id'" );

			$content .= $this->l->alert_text_dismiss( "success", AL_ONELINER_DELETED );
			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content_text deleted, id: " . $id );

			return $content;
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_ONELINER );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: '" . $id . "'" );

			return $content;
		}
	}

	public function content_status() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT `active` FROM `" . TP . "text_single` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();
				if ( $row['active'] == 0 ) {
					$active = 1;
				} else {
					$active = 0;
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "text_single", array( "active" ), array( $active ), "`id` = '$id'" );

				$content .= $this->l->alert_text_dismiss( "success", AL_ONELINER_STATUS_CHANGE );
				$this->log->event( "log", __FILE__ . ":" . __LINE__, "text status changed, id: " . $id );

				return $content;
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_ONELINER_STATUS );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_ONELINER_STATUS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}
	}

}

if ( ! isset( $action ) ) {
	$action = null;
}

$class_content_single_text = new class_content_single_text( $action );
$content .= $class_content_single_text->get_content();