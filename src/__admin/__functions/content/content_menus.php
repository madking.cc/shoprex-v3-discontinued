<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_MENUS;

class class_content_menus extends class_sys {
	public $content;

	public function __construct( $action ) {
		parent::__construct();

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );

		switch ( $action ) {
			case "menus_add_root":
				$this->content .= $this->menus_add_root();
				$this->content .= $this->start();
				break;
			case "search":
			case "menu_edit":
				$this->content .= $this->menu_edit();
				break;
			case "menu_options":
				$this->content .= $this->menu_options();
				break;
			case "menu_options_update":
				$this->content .= $this->menu_options_update();
				break;
			case "menu_options_update_and_back":
				$this->content .= $this->menu_options_update( NO_RELOAD );
				$this->content .= $this->start();
				break;
			case "menu_delete":
				$this->content .= $this->menu_delete();
				$this->content .= $this->start();
				break;
			case "menu_status":
				$this->content .= $this->menu_status();
				$this->content .= $this->start();
				break;
			case "content_add":
				$this->content .= $this->content_db();
				$this->content .= $this->menu_edit();
				break;
			case "change_pos":
				$this->content .= $this->change_pos();
				$this->content .= $this->menu_edit();
				break;
			case "content_status":
				$this->content .= $this->content_status();
				$this->content .= $this->menu_edit();
				break;
			case "content_delete":
				$this->content .= $this->delete_content();
				$this->content .= $this->menu_edit();
				break;
			case "content_edit":
				$this->content .= $this->content_edit();
				break;
			case "content_update":
				$this->content .= $this->content_db( DO_UPDATE );
				break;
			case "init":
			default:
				$this->content .= $this->start();
				break;
		}


	}

	public function get_content() {
		return $this->content;
	}


	public function start() {
		$content = "";

		$m = new class_menu();

		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );

		$sql    = "SELECT *, `" . TP . "menus`.`active` AS `menus_active` FROM `" . TP . "menus` WHERE `deleted` = '0' AND `to_id` = '0' ORDER BY `name`";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			$content .= "<h3 class='underline'>" . AL_EXISTING_MENU_BLOCKS . "</h3>\n";

			$handler = $this->tbl->tbl_init( AL_TBL_NAME, AL_TBL_TYPE, AL_TBL_MENU_ITEMS, AL_TBL_DATE, AL_TBL_ID, AL_TBL_ACTIVE, array(
				AL_TBL_ACTION,
				"nosort"
			) );

			$entry_counter = 0;
			while ( $row = $result->fetch_assoc() ) {

				$sql2        = "SELECT `id` FROM `" . TP . "menus` WHERE `to_id` = '" . $row['id'] . "' AND `deleted` = '0'";
				$result2     = $this->db->query( $sql2, __FILE__ . ":" . __LINE__ );
				$content_sum = $result2->num_rows;
				if ( ! isset( $content_sum ) ) {
					$content_sum = 0;
				}

				$date = $row['changed'];
				if ( $row['changed'] == "0000-00-00 00:00:00" ) {
					$date = $row['created'];
				}
				$date      = $this->dt->sqldatetime( $date );
				$timestamp = strtotime( $date );

				$parameter        = "id=" . $row["id"];
				$parameter_delete = "";

				$type = $m->display_menu_type( $row['id_type'] );

				if ( $row['menus_active'] == "0" ) {
					$submit_status = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				} elseif ( $row['menus_active'] == "1" ) {
					$submit_status = $this->l->submit( AL_DEACTIVATE );
				}

				$active = $this->lang->answer( $row['menus_active'] );

				$this->tbl->tbl_load( $handler, $row['name'], $type, $content_sum, array(
					$timestamp,
					"time",
					$date
				), $row['id'], $active, $this->l->form_admin( "", "content_menus_content.php", $parameter ) . $this->l->submit( AL_TO_CONTENT ) . "</form> " . $this->l->form_admin( "", "content_menus_options.php", $parameter ) . $this->l->submit( AL_OPTIONS ) . "</form> " . $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter_delete ) . $this->l->hidden( "id", $row["id"] ) . $this->l->hidden( "do", "menu_status" ) . $submit_status . "</form> " . $this->l->form_admin( "id=\"form_admin_delete_menu_$entry_counter\"", "", $parameter_delete ) . $this->l->hidden( "do", "menu_delete" ) . $this->l->hidden( "id", $row["id"] ) . $this->l->button( AL_DELETE, "id=\"button_delete_menu_$entry_counter\"" ) . "</form>" );
				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_MENU_PART01 . " \"" . $row['name'] . "\" " . AL_DELETE_MENU_PART02, "#button_delete_menu_$entry_counter", "form_admin_delete_menu_$entry_counter", "confirm_delete_$entry_counter" );
				$entry_counter ++;
			}
			$content .= $this->tbl->tbl_out( $handler, 0 );
		}

		$content .= "<h3 class='underline'>" . AL_ADD_NEW_MENU . "</h3>\n";

		$new_menu_name = "";
		$new_menu_type = 1;

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "menus_add_root" ) . $this->l->table() .
		            $m->get_root_menu_options( "type", $new_menu_type );

		$content .= "\n<tr><td></td><td>" . $this->l->submit( AL_ADD_NEW_MENU ) . "</td></tr></table></form>\n";

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_MENU_START, "info", AL_INFORMATION );

		return $content;
	}

	public function menus_add_root() {
		$content = "";


		$name = $this->p->get( "name", "", NOT_ESCAPED );
		$type = $this->p->get( "type", 1 );

		$nav_type            = $this->p->get( "options_nav_type", 0 );
		$nav_directions_type = $this->p->get( "options_nav_direction_type", 0 );
		$nav_justified       = $this->p->get( "options_nav_justified", 0 );
		$navbar_type         = $this->p->get( "options_navbar_type", 0 );
		$navbar_options_1    = $this->p->get( "options_navbar_1", 0 );
		$navbar_options_2    = $this->p->get( "options_navbar_2", 0 );
		$navbar_brand        = $this->p->get( "brand", "" );
		$dropdown_right      = $this->p->get( "options_dropdown_right", 0 );
		$css_id              = $this->p->get( "options_css_id", "" );
		$css_class           = $this->p->get( "options_css_class", "" );
		$content_text        = $this->p->get( "content_text", "" );
		$with_menu           = $this->p->get( "with_menu" );

		$name = trim( $name );
		$name = $this->translate_special_characters( $name, NOT_A_FILE, NO_UNDERLINE );
		$name = $this->db->escape( $name );

		$css_id    = $this->translate_special_characters( $css_id, NOT_A_FILE, NO_UNDERLINE );
		$css_class = $this->translate_special_characters( $css_class, NOT_A_FILE, NO_UNDERLINE );

		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME );

			return $content;
		}

		$sql    = "SELECT `id` FROM `" . TP . "menus` WHERE `name` LIKE '$name' AND `deleted` = '0' ";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 0 ) {
			$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02 );

			return $content;
		}

		switch ( $type ) {
			case MENU_TYPE_NAV:
				$this->db->ins( __FILE__ . ":" . __LINE__, "menus", array(
					"name",
					"id_type",
					"enhanced_editor",
					"id_option0",
					"id_option1",
					"id_option2",
					"created",
					"css_id",
					"css_class",
					"with_menu"
				), array(
					$name,
					$type,
					"NULL",
					$nav_type,
					$nav_justified,
					$nav_directions_type,
					"NOW()",
					$css_id,
					$css_class,
					$with_menu
				) );
				$id = $this->db->get_insert_id();
				break;
			case MENU_TYPE_NAVBAR:
				$this->db->ins( __FILE__ . ":" . __LINE__, "menus", array(
					"name",
					"id_type",
					"enhanced_editor",
					"id_option0",
					"id_option1",
					"id_option2",
					"content",
					"created",
					"css_id",
					"css_class",
					"with_menu"
				), array(
					$name,
					$type,
					"NULL",
					$navbar_type,
					$navbar_options_1,
					$navbar_options_2,
					$navbar_brand,
					"NOW()",
					$css_id,
					$css_class,
					$with_menu
				) );
				$id = $this->db->get_insert_id();
				break;
			case MENU_TYPE_DROPDOWN:
				$this->db->ins( __FILE__ . ":" . __LINE__, "menus", array(
					"name",
					"id_type",
					"enhanced_editor",
					"id_option0",
					"created",
					"css_id",
					"css_class",
					"content",
					"with_menu"
				), array(
					$name,
					$type,
					"NULL",
					$dropdown_right,
					"NOW()",
					$css_id,
					$css_class,
					$content_text,
					$with_menu
				) );
				$id = $this->db->get_insert_id();
				break;
			case MENU_TYPE_ULLI:
				$this->db->ins( __FILE__ . ":" . __LINE__, "menus", array(
					"name",
					"id_type",
					"enhanced_editor",
					"created",
					"css_id",
					"css_class",
					"with_menu"
				), array( $name, $type, "NULL", "NOW()", $css_id, $css_class, $with_menu ) );
				$id = $this->db->get_insert_id();
				break;
			default:
				$content .= $this->l->alert_text( "danger", AL_ERROR_CANT_ADD_MENU_UNKOWN_TYPE );
				$this->log->error( "form", __FILE__ . ":" . __LINE__, "Der Menüblock konnte nicht eingetragen werden, da der Typ unbekannt ist. Name: " . $name . ", Typ: " . $type );

				return $content;
				break;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_MENU_ADDED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "New menublock saved, name: " . $name . ", Id: " . $id );

		return $content;
	}

	public function menu_options_update( $reload = true ) {
		$content = "";


		$id = $this->p->get( "id", "" );

		if ( ! is_numeric( $id ) || $id < 1 ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_UPDATE_MENU );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= $this->start();

			return $content;
		}

		$name                = $this->p->get( "name", "", NOT_ESCAPED );
		$old_name            = $this->p->get( "old_name", "" );
		$type                = $this->p->get( "type", 1 );
		$nav_type            = $this->p->get( "options_nav_type", 0 );
		$nav_directions_type = $this->p->get( "options_nav_direction_type", 0 );
		$nav_justified       = $this->p->get( "options_nav_justified", 0 );
		$navbar_type         = $this->p->get( "options_navbar_type", 0 );
		$navbar_options_1    = $this->p->get( "options_navbar_1", 0 );
		$navbar_options_2    = $this->p->get( "options_navbar_2", 0 );
		$navbar_brand        = $this->p->get( "brand", "" );
		$dropdown_right      = $this->p->get( "options_dropdown_right", 0 );
		$enable_sitemap      = $this->p->get( "options_enable_sitemap", 0 );
		$css_id              = $this->p->get( "options_css_id", "" );
		$css_class           = $this->p->get( "options_css_class", "" );
		$content_text        = $this->p->get( "content_text", "" );
		$with_menu           = $this->p->get( "with_menu" );

		$name = trim( $name );
		$name = $this->translate_special_characters( $name, NOT_A_FILE, NO_UNDERLINE );
		$name = $this->db->escape( $name );

		$css_id    = $this->translate_special_characters( $css_id, NOT_A_FILE, NO_UNDERLINE );
		$css_class = $this->translate_special_characters( $css_class, NOT_A_FILE, NO_UNDERLINE );

		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME );

			return $content;
		}

		if ( $old_name != $name ) {
			$sql    = "SELECT `id` FROM `" . TP . "menus` WHERE `name` LIKE '$name' AND `deleted` = '0' ";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 0 ) {
				$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02, true );

				return $content;
			}
		}

		switch ( $type ) {
			case MENU_TYPE_NAV:
				$this->db->upd( __FILE__ . ":" . __LINE__, "menus", array(
					"name",
					"enhanced_editor",
					"id_option0",
					"id_option1",
					"id_option2",
					"css_id",
					"css_class",
					"enable_sitemap",
					"with_menu"
				), array(
					$name,
					"NULL",
					$nav_type,
					$nav_justified,
					$nav_directions_type,
					$css_id,
					$css_class,
					$enable_sitemap,
					$with_menu
				), "`id` = '$id'" );
				break;
			case MENU_TYPE_NAVBAR:
				$this->db->upd( __FILE__ . ":" . __LINE__, "menus", array(
					"name",
					"enhanced_editor",
					"id_option0",
					"id_option1",
					"id_option2",
					"content",
					"css_id",
					"css_class",
					"enable_sitemap",
					"with_menu"
				), array(
					$name,
					"NULL",
					$navbar_type,
					$navbar_options_1,
					$navbar_options_2,
					$navbar_brand,
					$css_id,
					$css_class,
					$enable_sitemap,
					$with_menu
				), "`id` = '$id'" );
				break;
			case MENU_TYPE_DROPDOWN:
				$this->db->upd( __FILE__ . ":" . __LINE__, "menus", array(
					"name",
					"enhanced_editor",
					"id_option0",
					"css_id",
					"css_class",
					"content",
					"enable_sitemap",
					"with_menu"
				), array(
					$name,
					"NULL",
					$dropdown_right,
					$css_id,
					$css_class,
					$content_text,
					$enable_sitemap,
					$with_menu
				), "`id` = '$id'" );
				break;
			case MENU_TYPE_ULLI:
				$this->db->upd( __FILE__ . ":" . __LINE__, "menus", array(
					"name",
					"enhanced_editor",
					"css_id",
					"css_class",
					"enable_sitemap",
					"with_menu"
				), array( $name, "NULL", $css_id, $css_class, $enable_sitemap, $with_menu ), "`id` = '$id'" );
				break;
			default:
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_TYPE_CANT_UPDATE_MENU );
				$this->log->error( "form", __FILE__ . ":" . __LINE__, "Der Menüblock konnte nicht aktualisiert werden, da der Typ unbekannt ist. Name: " . $name . ", Typ: " . $type );

				return $content;
				break;
		}

		$this->log->event( "log", __FILE__ . ":" . __LINE__, "Menublock updated, name: " . $name . ", Id: " . $id );

		$_SESSION['options_updated'] = 1;
		if ( $reload ) {
			$this->l->reload_js( "", "id=$id" );
		}

		return $content;
	}

	public function rekursive_delete_menu( $id ) {


		$sql    = "SELECT `id` FROM `" . TP . "menus` WHERE `to_id`=" . $id;
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows > 0 ) {
			while ( $row = $result->fetch_assoc() ) {
				$sql = "UPDATE `" . TP . "menus` SET `deleted` = '1' WHERE `id`=" . $row['id'];
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				$this->rekursive_delete_menu( $row['id'] );
			}
		}
	}

	public function change_pos() {
		$content = "";


		$id = $this->p->get( "content_id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT * FROM `" . TP . "menus` WHERE `id`='" . $id . "' AND `deleted` = '0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows == 1 ) {
				$row     = $result->fetch_assoc();
				$old_pos = $row['pos'];
				$to_id   = $row['to_id'];

				$sql     = "SELECT * FROM `" . TP . "menus` WHERE `to_id`='" . $to_id . "' AND `deleted` = '0'";
				$result  = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				$max_pos = $result->num_rows - 1;

				$direction = $this->p->get( "direction" );
				if ( ! empty( $direction ) ) {
					if ( $direction == "up" ) {
						if ( $old_pos == $max_pos ) {
							$new_pos = 0;
							$sql     = "UPDATE `" . TP . "menus` SET `pos` = `pos` +1 WHERE `to_id`='" . $to_id . "'";
							$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
							$sql = "UPDATE `" . TP . "menus` SET `pos` = '$new_pos' WHERE `id`='" . $id . "'";
							$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						} else {
							$new_pos = $old_pos + 1;
							$sql     = "UPDATE `" . TP . "menus` SET `pos` = `pos` -1 WHERE `pos` > '" . $old_pos . "' AND `pos` <= '$new_pos' AND `to_id`='" . $to_id . "'";
							$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
							$sql = "UPDATE `" . TP . "menus` SET `pos` = '$new_pos' WHERE `id`='" . $id . "'";
							$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						}
					} elseif ( $direction == "down" ) {
						if ( $old_pos == "0" ) {
							$new_pos = $max_pos;
							$sql     = "UPDATE `" . TP . "menus` SET `pos` = `pos` -1 WHERE `to_id`='" . $to_id . "'";
							$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
							$sql = "UPDATE `" . TP . "menus` SET `pos` = '$new_pos' WHERE `id`='" . $id . "'";
							$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						} else {
							$new_pos = $old_pos - 1;
							$sql     = "UPDATE `" . TP . "menus` SET `pos` = `pos` +1 WHERE `pos` < '" . $old_pos . "' AND `pos` >= '$new_pos' AND `to_id`='" . $to_id . "'";
							$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
							$sql = "UPDATE `" . TP . "menus` SET `pos` = '$new_pos' WHERE `id`='" . $id . "'";
							$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						}
					} else {
						$content .= $this->l->alert_text( "danger", UNKNOWN_ERROR_CANT_MOVE_MENUPOINT );
						$this->log->error( "form", __FILE__ . ":" . __LINE__, "direction invalid, id: " . $id . ", direction: " . $direction );

						return $content;
					}
				} else {
					$content .= $this->l->alert_text( "danger", UNKNOWN_ERROR_CANT_MOVE_MENUPOINT );
					$this->log->error( "form", __FILE__ . ":" . __LINE__, "direction invalid, id: " . $id . ", direction: " . $direction );

					return $content;
				}
			} else {
				$content .= $this->l->alert_text( "danger", UNKNOWN_ERROR_CANT_MOVE_MENUPOINT );
				$this->log->error( "form", __FILE__ . ":" . __LINE__, "menupoint not found, id: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", UNKNOWN_ERROR_CANT_MOVE_MENUPOINT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_MENUPOINT_MOVED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "Menupoint moved, id: " . $id . ", direction: " . $direction );

		return $content;
	}

	public function menu_delete() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql = "UPDATE `" . TP . "menus` SET `deleted` = '1' WHERE `id`=" . $id;
			$this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			$this->rekursive_delete_menu( $id );
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_MENU );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_MENU_DELETED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "menu deleted, id: " . $id );

		return $content;
	}

	public function menu_options() {
		$content = "";


		$m = new class_menu();

		$GLOBALS['admin_subtitle'] = AL_MENU_OPTIONS;

		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );

		$id = $this->p->get( "id", "" );

		$content .= "<h3 class='underline'>" . AL_MENU_OPTIONS . "</h3>\n";

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT * FROM `" . TP . "menus` WHERE `id`='" . $id . "' AND `deleted` = '0' ";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_MENU );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= $this->start();

			return $content;
		}

		if ( $result->num_rows == 0 ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_FIND_MENU );
			$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
			$content .= $this->start();

			return $content;
		}

		$row = $result->fetch_assoc();

		$content .= $this->l->form_admin() .
		            $this->l->hidden( "old_name", $row['name'] ) . $this->l->hidden( "do", "menu_options_update" ) .
		            $this->l->hidden( "id", $id ) . $this->l->hidden( "type", $row['id_type'] ) .
		            $this->l->table() .

		            $m->get_root_menu_options( NO_NAME, $row, NO_SELECTION );

		$content .= "\n<tr><td></td><td>" . $this->l->submit( AL_SAVE ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='save_and_back(this.form);'" ) . " " . $this->l->reload_button( "content_menus_options.php", "id=$id", AL_RELOAD ) . " " . $this->l->back_button( AL_BACK, "content_menus.php" ) . "</td></tr></table></form>\n";

		$GLOBALS['body_footer'] .= "<script>
    function save_and_back(formID)
    {
        formID.do.value = 'menu_options_update_and_back';
        formID.action = '/" . WEBROOT . ADMINDIR . "content_menus.php';
        formID.submit();
    }
    </script>
    ";

		return $content;
	}

	public function menu_edit() {
		$content = "";


		$m = new class_menu();

		$GLOBALS['admin_subtitle'] = AL_EDIT_MENU;

		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT * FROM `" . TP . "menus` WHERE `id`='" . $id . "' AND `deleted` = '0' ";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_MENU );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= $this->start();

			return $content;
		}

		if ( $result->num_rows == 0 ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_FIND_MENU );
			$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
			$content .= $this->start();

			return $content;
		}

		$content .= $this->l->display_message_by_session( 'content_updated', AL_MENUITEM_UPDATED );

		$row       = $result->fetch_assoc();
		$parameter = "id=" . $row["id"];

		global $submenus_id;
		global $submenus_name;
		$submenus_id   = array();
		$submenus_name = array();

		$row_main = $this->get_main_menu( $row );

		if ( sizeof( $submenus_name ) > 1 ) {
			$submenus_id   = array_reverse( $submenus_id );
			$submenus_name = array_reverse( $submenus_name );
		}

		$type = $m->display_menu_type( $row_main['id_type'] );

		$content .= "<h3 class='underline'>" . AL_EDITING_MENU . " " . $this->l->link( $row_main['name'], ADMINDIR . "content_menus_content.php?id=" . $row_main['id'] );

		$is_sub = 0;
		if ( sizeof( $submenus_name ) > 0 ) {
			$is_sub = 1;
			foreach ( $submenus_name AS $key => $value ) {
				$content .= " &#8594; " . $this->l->link( $value, ADMINDIR . "content_menus_content.php?id=" . $submenus_id[ $key ] );
			}
		}

		$content .= ", Typ: $type</h3>\n";

		$sql     = "SELECT * FROM `" . TP . "menus` WHERE `to_id`='" . $id . "' AND `deleted` = '0' ORDER BY `pos`";
		$result2 = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result2->num_rows == 0 ) {
			$content .= "<p>Menü ist leer</p>\n";
		} else {
			$content .= $this->l->table() . "<tr>
        <td>" . AL_TBL_POS . "</td><td>" . AL_TBL_CONTENT . "</td><td>" . AL_TBL_TYPE . "</td><td>" . AL_TBL_DATE . "</td><td>" . AL_TBL_ID . "</td><td>" . AL_TBL_STATUS . "</td><td>" . AL_TBL_ACTION . "</td></tr>\n";

			$entry_counter = 0;
			while ( $row2 = $result2->fetch_assoc() ) {

				$content .= "<tr><td>";

				$content .= $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "do", "change_pos" ) . $this->l->hidden( "content_id", $row2['id'] ) .
				            $this->l->hidden( "direction", "down" ) . $this->l->submit( "&#8593;" ) . "</form> " . ( $row2['pos'] + 1 ) . " " .
				            $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "do", "change_pos" ) . $this->l->hidden( "content_id", $row2['id'] ) . $this->l->hidden( "direction", "up" ) .
				            $this->l->submit( "&#8595;" ) . "</form>";

				$content .= "</td><td>";

				$content .= $m->get_content_type( $row['id_type'], $row2, "full" );

				$content .= "</td><td>";

				$content .= $m->get_content_type( $row['id_type'], $row2, "text" );

				$content .= "</td><td>";

				if ( is_null( $row['changed'] ) || $row['changed'] == "0000-00-00 00:00:00" ) {
					$date = AL_NOT_SPECIFIED_SHORT;
				} else {
					$date = $this->dt->sqldatetime( $row['changed'] );
				}

				$content .= $date;

				$content .= "</td><td>";

				$content .= $row2['id'];

				$content .= "</td><td>";

				$content .= $m->get_menupoint_status_text( $row['id_type'], $row2 );

				$row2['active'] ? $content .= AL_ACTIVE : $content .= AL_INACTIVE;

				$content .= "</td><td>";

				$this->l->box_js( "#edit_" . $row2['id'], "content_menus_content_edit.php", "id=" . $row2['id'] . "&is_sub=" . $is_sub );

				$content .= $this->l->form_admin() . $this->l->button( AL_EDIT, "id=\"edit_" . $row2['id'] . "\"" ) . "</form> ";

				$content .= $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "do", "content_status" ) . $this->l->hidden( "content_id", $row2['id'] );

				if ( $row2['active'] ) {
					$content .= $this->l->submit( AL_DEACTIVATE );
				} else {
					$content .= $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				}

				$content .= "</form> " . $this->l->form_admin( "id='form_admin_delete_menu_$entry_counter'", "", $parameter ) .
				            $this->l->hidden( "do", "content_delete" ) . $this->l->hidden( "content_id", $row2['id'] ) . $this->l->button( AL_DELETE, "id=\"button_delete_menu_$entry_counter\"" ) . "</form>";

				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_MENUITEM_PART01 . " " . ( $row2['pos'] + 1 ) . " " . AL_DELETE_MENUITEM_PART02, "#button_delete_menu_$entry_counter", "form_admin_delete_menu_$entry_counter", "confirm_delete_$entry_counter" );
				$entry_counter ++;

				$content .= "</td></tr>\n";
			}

			$content .= "</table>\n";
		}

		$content .= "<h3>" . AL_PREVIEW . AL_TBL_COLON . "</h3>\n";

		$tmp = $this->l->show_menu( $row_main['name'], MENU_PREVIEW_MODE );
		$content .= $tmp;

		$content .= "\n<br /><br />\n";

		$content .= $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->back_button( AL_TO_OVERVIEW, "content_menus.php" ) . "</form><br /><br /><h4 class='underline'>" . AL_ADD_MENUITEM . AL_TBL_COLON . "</h4>\n";

		$content .= $this->menu_point( $row, $parameter, "content_add", $is_sub );

		$content .= $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->back_button( AL_TO_OVERVIEW, "content_menus.php" ) . "</form>";

		$content .= "\n<br /><br />\n";

		return $content;
	}

	public function get_main_menu( $row ) {


		global $submenus_id;
		global $submenus_name;

		if ( empty( $row['to_id'] ) ) {
			return $row;
		} else {
			array_push( $submenus_id, $row['id'] );
			array_push( $submenus_name, $row['content'] );
			$sql    = "SELECT * FROM `" . TP . "menus` WHERE `id`='" . $row['to_id'] . "'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();

				$row = $this->get_main_menu( $row );
			} else {
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "Kann Hauptmenü nicht ermitteln. Suche ID: " . $row['to_id'] . ", Treffer: " . $result->num_rows );
			}
		}

		return $row;
	}

	public function get_page_select( $row, $select_name = "page_id" ) {


		$content = "";

		$sql          = "SELECT * FROM `" . TP . "pages` WHERE `deleted`='0' ORDER BY `name`";
		$result_pages = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result_pages->num_rows > 0 ) {
			$content .= $this->l->select( $select_name );

			if ( ! is_numeric( $row[ MENU_DB_LINK_TO_PAGE ] ) ) {
				$selected_page = "0";
			} else {
				$selected_page = $row['content'];
			}

			$found = false;
			while ( $row_pages = $result_pages->fetch_assoc() ) {
				$row_pages['active'] ? $active = AL_ACTIVE : $active = AL_INACTIVE;
				$content .= "<option value='" . $row_pages['id'] . "'";
				if ( $selected_page > 0 ) {
					if ( $selected_page == $row_pages['id'] ) {
						$found = true;
						$content .= " selected='selected'";
					}
				}
				$content .= ">" . $row_pages['name'] . " - $active</option>\n";
			}
			if ( ! $found && $selected_page ) {
				$content .= "<option value='0' selected='selected'>" . AL_ENTRY_NO_LONGER_VALID . "</option>\n";
			}

			$content .= "</select>";
		} else {
			$content .= AL_NO_DEFINED_PAGED . $this->l->hidden( "page_id", "0" );
		}

		return $content;
	}

	public function menu_point( $row, $parameter, $action = "content_add", $is_sub = false ) {


		$content = "";

		if ( $this->lang->count_of_languages <= 1 ) {
			$single_language = true;
		} else {
			$single_language = false;
		}

		if ( $action == "content_add" ) {
			$row['id_option0']      = "0";
			$row['id_option1']      = "0";
			$row['id_option2']      = "0";
			$row['id_option3']      = "0";
			$row['content_option0'] = "0";
			$row['content_option1'] = "0";
			$row['content_option2'] = "0";
			$row['content_option3'] = "0";
			$row['content_option4'] = "0";
			$row['pos']             = "";
			$row['css_id']          = "";
			$row['css_class']       = "";
			$row['active']          = "1";
			$row['to_id']           = $row['id'];
			$content_type           = "0";
			$submit_text            = AL_ADD;

			if ( $single_language ) {
				$row['name']    = "";
				$row['content'] = "";
				$content_form   = "<form class='navbar-form navbar-left'>\r\n\r\n</form>";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$row[ 'name' . $lang_array['number'] ]    = "";
					$row[ 'content' . $lang_array['number'] ] = "";
					${"content_form" . $lang_array['number']} = "<form class='navbar-form navbar-left'>\r\n\r\n</form>";
				}
			}
		} else {
			$content_type = $row['content_type'];
			$submit_text  = AL_SAVE;
			$row['pos']   = $row['pos'] + 1;
			if ( $single_language ) {
				$content_form = $row[ 'content' . $this->lang->lnbrm ];
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					${"content_form" . $lang_array['number']} = $row[ 'content' . $lang_array['number'] ];
				}
			}

			$content .= $this->l->form_admin() . $this->l->button( AL_CLOSE_WINDOW, "id='close_window'" ) . "</form><br /><br />\n";

			$GLOBALS['body_footer'] .= "<script>
               $(\"#close_window\").click(function() {
                    parent.$.fn.colorbox.close();
                });
            </script>\n";
		}

		$default_form_start = $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) .
		                      $this->l->hidden( "id_type", $row['id_type'] ) .
		                      $this->l->hidden( "to_id", $row['to_id'] ) .
		                      $this->l->hidden( "id", $row['id'] ) .
		                      $this->l->hidden( "do", $action );

		if ( $single_language ) {
			$name = "<tr class='content_name'>\n";
			$name .= "<td>" . AL_TBL_DISPLAY_TEXT . "</td><td>" . $this->l->text( "content_name", $row[ MENU_DB_OPTIONS_NAME . $this->lang->lnbrm ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
			$name .= "</tr>\n";
		} else {
			$name = "";
			foreach ( $this->lang->languages as $lang_array ) {
				$name .= "<tr class='content_name'>\n";
				$name .= "<td>" . AL_DISPLAY_TEXT_IN . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_name" . $lang_array['number'], $row[ MENU_DB_OPTIONS_NAME . $lang_array['number'] ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$name .= "</tr>\n";
			}
		}

		$default_form_end = $name . "<tr class='disabled'><td>" . AL_DISABLED_QUE . "</td><td>" . $this->l->select_yesno( "disabled", $row[ MENU_DB_CSS_STATUS ] ) . "</td></tr>
            <tr class='link_target'><td>" . AL_LINK_IN_NEW_WINDOW_QUES . "</td><td>" . $this->l->select_yesno( "target_blank", $row[ MENU_DB_OPEN_NEW_WINDOW ] ) . "</td></tr>
            <tr><td>" . AL_TBL_POS . "</td><td>" . $this->l->text( "pos", $row['pos'] ) . " " . AL_HINT_EMPTY_LAST_POSITION . "</td></tr>
            <tr><td>" . AL_TBL_CSS_ID . "</td><td>" . $this->l->text( "css_id", $row[ MENU_DB_OPTIONS_CSS_ID ] ) . " " . AL_HINT_OPTIONAL . "</td></tr>
            <tr><td>" . AL_TBL_CSS_CLASS . "</td><td>" . $this->l->text( "css_class", $row[ MENU_DB_OPTIONS_CSS_CLASS ] ) . " " . AL_HINT_OPTIONAL . "</td></tr>
            <tr><td></td><td>" . $this->l->submit( $submit_text ) . "</td></tr>
            </table>
            </form>
            ";

		// NAV
		if ( $row['id_type'] == MENU_TYPE_NAV ) {
			$content .= $default_form_start;
			$content .= $this->l->table() . "

            <tr><td>" . AL_TBL_TYPE . "</td>";
			// Select Type
			$content .= "
            <td>" . $this->l->select( "content_type", "id='content_type'" ) . "
                <option value='" . MENU_NAV_MENU_POINT_IS_LINK . "'";
			$content_type == MENU_NAV_MENU_POINT_IS_LINK ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_SIMPLE_MENU_LINK . "</option>\n";
			if ( ! $is_sub ) {
				$content .= "<option value='" . MENU_NAV_MENU_POINT_IS_DROPDOWN . "'";
				$content_type == MENU_NAV_MENU_POINT_IS_DROPDOWN ? $content .= " selected='selected'" : $content .= "";
				$content .= ">" . AL_DROPDOWN_LINK_SUBMENU . "</option>\n";
			} else {
				$content .= "<option value='" . MENU_NAV_SUB_MENU_POINT_IS_DIVIDER . "'";
				$content_type == MENU_NAV_SUB_MENU_POINT_IS_DIVIDER ? $content .= " selected='selected'" : $content .= "";
				$content .= ">" . AL_DIVIDER . "</option>\n";
			}
			$content .= "<option value='" . MENU_NAV_MENU_POINT_IS_FREE_TEXT . "'";
			$content_type == MENU_NAV_MENU_POINT_IS_FREE_TEXT ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_FREE_TEXT . "</option>
            <option value='" . MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE . "'";
			$content_type == MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_LINK_TO_PAGE . "</option>
            </select>
            </td></tr>\n";

			// Display Content to Selection

			// Simple Link
			$content .= "<tr class='content_" . MENU_NAV_MENU_POINT_IS_LINK . "'>
                <td>" . AL_TBL_LINK . "</td><td>" . $this->l->text( "content_link", $row[ MENU_DB_LINK ] ) . " " . AL_HINT_LINK_EXAMPLE . "</td>
            </tr>\n";

			// Dropdown
			if ( $single_language ) {
				$content .= "<tr class='content_" . MENU_NAV_MENU_POINT_IS_DROPDOWN . "'>";
				$content .= "<td>" . AL_TBL_DROPDOWN_TEXT . "</td><td>" . $this->l->text( "content_dropdown", $row[ MENU_DB_DROPDOWN_TEXT . $this->lang->lnbrm ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$content .= "</tr>\n";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . MENU_NAV_MENU_POINT_IS_DROPDOWN . "'>";
					$content .= "<td>" . AL_DROPDOWN_TEXT_IN . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_dropdown" . $lang_array['number'], $row[ MENU_DB_DROPDOWN_TEXT . $lang_array['number'] ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
					$content .= "</tr>\n";
				}
			}

			// Free Text
			if ( $single_language ) {
				$content .= "<tr class='content_" . MENU_NAV_MENU_POINT_IS_FREE_TEXT . "'>";
				$content .= "<td>" . AL_TBL_TEXT . "</td><td>" . $this->l->text( "content_text", $row[ MENU_DB_FREE_TEXT . $this->lang->lnbrm ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$content .= "</tr>\n";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . MENU_NAV_MENU_POINT_IS_FREE_TEXT . "'>";
					$content .= "<td>" . AL_TEXT_IN . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_text" . $lang_array['number'], $row[ MENU_DB_FREE_TEXT . $lang_array['number'] ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
					$content .= "</tr>\n";
				}
			}

			// Link to Page
			$content .= "<tr class='content_" . MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE . "'>";
			$content .= "<td>" . AL_TBL_PAGE . "</td><td>";
			$content .= $this->get_page_select( $row );
			$content .= "</td></tr>\n";

			$content .= $default_form_end;

			// Javscript
			$this->p->init_array( $GLOBALS['foot'], 30 );
			$GLOBALS['foot'][30] .= "<script>

    $( function() {
        change_content($( \"#content_type option:selected\" ).val());
        $(\"#content_type\").change(function() {
            change_content($( \"#content_type option:selected\" ).val());
        })
    });

    function change_content(id)
    {
        $(\".content_" . MENU_NAV_MENU_POINT_IS_LINK . "\").hide();
        $(\".content_" . MENU_NAV_MENU_POINT_IS_DROPDOWN . "\").hide();
        $(\".content_" . MENU_NAV_MENU_POINT_IS_FREE_TEXT . "\").hide();
        $(\".content_" . MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE . "\").hide();
        $(\".disabled\").hide();
        $(\".link_target\").hide();
        $(\".content_name\").hide();

        if(id != " . MENU_NAV_SUB_MENU_POINT_IS_DIVIDER . ")
            $(\".content_\" + id).show();

        if(id==" . MENU_NAV_MENU_POINT_IS_LINK . " || id==" . MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE . ")
        {
            $(\".link_target\").show();
            $(\".content_name\").show();
        }

        if(id==" . MENU_NAV_MENU_POINT_IS_LINK . " || id==" . MENU_NAV_MENU_POINT_IS_DROPDOWN . " || id==" . MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE . ")
            $(\".disabled\").show();
    }
</script>\n";
		} // NAVBAR
		elseif ( $row['id_type'] == MENU_TYPE_NAVBAR ) {
			$content .= $default_form_start;
			$content .= $this->l->table() . "

            <tr><td>" . AL_TBL_TYPE . "</td><td>";
			// Select Type

			$content .= $this->l->select( "content_type", "id='content_type'" ) . "
                <option value='" . NAVBAR_SIDE_MENU_POINT_IS_LINK . "'";
			$content_type == NAVBAR_SIDE_MENU_POINT_IS_LINK ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_SIMPLE_MENU_LINK . "</option>\n";
			if ( ! $is_sub ) {
				$content .= "<option value='" . NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN . "'";
				$content_type == NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN ? $content .= " selected='selected'" : $content .= "";
				$content .= ">" . AL_DROPDOWN_LINK_SUBMENU . "</option>\n";
			}
			$content .= "<option value='" . NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT . "'";
			$content_type == NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_FREE_TEXT . "</option>\n";
			if ( ! $is_sub ) {
				$content .= "<option value='" . NAVBAR_SIDE_MENU_POINT_IS_BUTTON . "'";
				$content_type == NAVBAR_SIDE_MENU_POINT_IS_BUTTON ? $content .= " selected='selected'" : $content .= "";
				$content .= ">" . AL_BUTTON . "</option>
                <option value='" . NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS . "'";
				$content_type == NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS ? $content .= " selected='selected'" : $content .= "";
				$content .= ">" . AL_TEXT_WITH_FORM . "</option>\n";
			} else {
				$content .= "<option value='" . NAVBAR_SUB_POINT_IS_DIVIDER . "'";
				$content_type == NAVBAR_SUB_POINT_IS_DIVIDER ? $content .= " selected='selected'" : $content .= "";
				$content .= ">" . AL_DIVIDER . "</option>\n";
			}

			$content .= "<option value='" . NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE . "'";
			$content_type == NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_LINK_TO_PAGE . "</option>
            </select>
            </td></tr>";

			// Options
			if ( ! $is_sub ) {
				$content .= "
            <tr>
             <td>" . AL_TBL_FLUSH_RIGHT . "</td>
             <td>" . $this->l->select_yesno( "align_right", $row[ MENU_DB_SIDE ] ) . "</td>
            </tr>";
			}

			// Simple Link
			$content .= "
            <tr class='content_" . NAVBAR_SIDE_MENU_POINT_IS_LINK . "'>
                <td>" . AL_TBL_LINK . "</td><td>" . $this->l->text( "content_link", $row[ MENU_DB_LINK ] ) . " " . AL_HINT_LINK_EXAMPLE . "</td>
            </tr>";

			// Dropdown
			if ( $single_language ) {
				$content .= "<tr class='content_" . NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN . "'>";
				$content .= "<td>" . AL_TBL_DROPDOWN_TEXT . "</td><td>" . $this->l->text( "content_dropdown", $row[ MENU_DB_DROPDOWN_TEXT . $this->lang->lnbrm ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$content .= "</tr>\n";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN . "'>";
					$content .= "<td>";
					$content .= AL_DROPDOWN_TEXT_IN . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_dropdown" . $lang_array['number'], $row[ MENU_DB_DROPDOWN_TEXT . $lang_array['number'] ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "<br />\n";
					$content .= "</td>\n";
					$content .= "</tr>\n";
				}
			}

			// Freetext
			if ( $single_language ) {
				$content .= "<tr class='content_" . NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT . "'>";
				$content .= "<td>" . AL_TBL_TEXT . "</td><td>" . $this->l->text( "content_text", $row[ MENU_DB_FREE_TEXT . $this->lang->lnbrm ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$content .= "</tr>\n";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT . "'>";
					$content .= "<td>";
					$content .= AL_TEXT_IN . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_text" . $lang_array['number'], $row[ MENU_DB_FREE_TEXT . $lang_array['number'] ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "<br />\n";
					$content .= "</td>\n";
					$content .= "</tr>\n";
				}
			}

			// Button
			if ( $single_language ) {
				$content .= "<tr class='content_" . NAVBAR_SIDE_MENU_POINT_IS_BUTTON . "'>";
				$content .= "<td>" . AL_TBL_TEXT . "</td><td>" . $this->l->text( "content_button", $row[ MENU_DB_BUTTON_TEXT . $this->lang->lnbrm ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$content .= "</tr>\n";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . NAVBAR_SIDE_MENU_POINT_IS_BUTTON . "'>";
					$content .= "<td>";
					$content .= AL_TEXT_IN . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_button" . $lang_array['number'], $row[ MENU_DB_BUTTON_TEXT . $lang_array['number'] ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "<br />\n";
					$content .= "</td>\n";
					$content .= "</tr>\n";
				}
			}

			// Form Elements
			if ( $single_language ) {
				$content .= "<tr class='content_" . NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS . "'>";
				$content .= "<td>" . AL_TBL_TEXT . "</td><td>" . $this->l->text( "content_form", $content_form ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$content .= "</tr>\n";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS . "'>";
					$content .= "<td>";
					$content .= AL_TEXT_IN . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_form" . $lang_array['number'], ${"content_form" . $lang_array['number']} ) . " " . AL_HINT_HTML_CODE_ALLOWED . "<br />\n";
					$content .= "</td>\n";
					$content .= "</tr>\n";
				}
			}

			// Link to Page
			$content .= "<tr class='content_" . NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE . "'>";
			$content .= "<td>" . AL_TBL_PAGE . "</td><td>";
			$content .= $this->get_page_select( $row );
			$content .= "</td></tr>\n";

			// More Options

			// Link Color
			$content .= "<tr class='freetext_color'>
                <td>" . AL_TBL_TEXTCOLOR_LIKE_LINKS . "</td><td>" . $this->l->select_yesno( "freetext_color", $row[ MENU_DB_TEXT_COLOR_UNIFORM ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>
            </tr>\n";

			// Button Type (Color)
			$content .= "<tr class='button_color'>
                <td>" . AL_TBL_BUTTONTYPE_COLOR . "</td><td>" . $this->l->select( "button_type" );

			foreach ( $this->l->button_types as $key => $value ) {
				$content .= "<option value='" . $key . "'";
				if ( $value == $row[ MENU_DB_BUTTON_TYPE ] ) {
					$content .= " selected='selected'";
				}
				$content .= ">$value</option>\n";
			}
			$content .= "</select></td>
            </tr>\n";

			// Button Size
			$content .= "<tr class='button_size'>
                <td>" . AL_TBL_BUTTONSIZE . "</td><td>" . $this->l->select( "button_size" );

			foreach ( $this->l->button_sizes as $key => $value ) {
				$content .= "<option value='" . $key . "'";
				if ( $value == $row[ MENU_DB_BUTTON_SIZE ] ) {
					$content .= " selected='selected'";
				}
				$content .= ">$value</option>\n";
			}
			$content .= "</select></td>
            </tr>\n";

			$content .= $default_form_end;

			// Javascript
			$this->p->init_array( $GLOBALS['foot'], 30 );
			$GLOBALS['foot'][30] .= "<script>
    $( function() {
        change_content($( \"#content_type option:selected\" ).val());
        $(\"#content_type\").change(function() {
            change_content($( \"#content_type option:selected\" ).val());
        })
    });

    function change_content(id)
    {
        $(\".content_" . NAVBAR_SIDE_MENU_POINT_IS_LINK . "\").hide();
        $(\".content_" . NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN . "\").hide();
        $(\".content_" . NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT . "\").hide();
        $(\".content_" . NAVBAR_SIDE_MENU_POINT_IS_BUTTON . "\").hide();
        $(\".content_" . NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS . "\").hide();
        $(\".content_" . NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE . "\").hide();
        $(\".disabled\").hide();
        $(\".link_target\").hide();
        $(\".content_name\").hide();
        $(\".freetext_color\").hide();
        $(\".button_color\").hide();
        $(\".button_size\").hide();

        $(\".content_\" + id).show();

        if(id == " . NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT . ")
            $(\".freetext_color\").show();

        if(id == " . NAVBAR_SIDE_MENU_POINT_IS_BUTTON . ")
        {
            $(\".button_color\").show();
            $(\".button_size\").show();
        }

        if(id==" . NAVBAR_SIDE_MENU_POINT_IS_LINK . " || id==" . NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE . ")
        {
            $(\".link_target\").show();
            $(\".content_name\").show();
        }
        if(id==" . NAVBAR_SIDE_MENU_POINT_IS_LINK . " || id==" . NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN . " || id==" . NAVBAR_SIDE_MENU_POINT_IS_BUTTON . " || id==" . NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE . ")
            $(\".disabled\").show();
    }
</script>
    ";
		} // Dropdown
		elseif ( $row['id_type'] == MENU_TYPE_DROPDOWN ) {

			$content .= $default_form_start;
			$content .= $this->l->table();

			// Select Type
			$content .= "<tr><td>" . AL_TBL_TYPE . "</td><td>" . $this->l->select( "content_type", "id='content_type'" ) . "
                <option value='" . DROPDOWN_MENU_POINT_IS_LINK . "'";
			$content_type == DROPDOWN_MENU_POINT_IS_LINK ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_SIMPLE_MENU_LINK . "</option>
                <option value='" . DROPDOWN_MENU_POINT_IS_FREE_TEXT . "'";
			$content_type == DROPDOWN_MENU_POINT_IS_FREE_TEXT ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_FREE_TEXT . "</option>
                <option value='" . DROPDOWN_MENU_POINT_IS_DIVIDER . "'";
			$content_type == DROPDOWN_MENU_POINT_IS_DIVIDER ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_DIVIDER . "</option>
                <option value='" . DROPDOWN_MENU_POINT_IS_HEADER . "'";
			$content_type == DROPDOWN_MENU_POINT_IS_HEADER ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_HEADER . "</option>
                <option value='" . DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE . "'";
			$content_type == DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_LINK_TO_PAGE . "</option>
            </select>
            </td></tr>\n";

			// Simple Link
			$content .= "<tr class='content_" . DROPDOWN_MENU_POINT_IS_LINK . "'>
                <td>" . AL_TBL_LINK . "</td><td>" . $this->l->text( "content_link", $row[ MENU_DB_LINK ] ) . " " . AL_HINT_LINK_EXAMPLE . "</td>
            </tr>\n";

			// Free Text
			if ( $single_language ) {
				$content .= "<tr class='content_" . DROPDOWN_MENU_POINT_IS_FREE_TEXT . "'>";
				$content .= "<td>" . AL_TBL_TEXT . "</td><td>" . $this->l->text( "content_text", $row[ MENU_DB_FREE_TEXT . $this->lang->lnbrm ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$content .= "</tr>\n";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . DROPDOWN_MENU_POINT_IS_FREE_TEXT . "'>";
					$content .= "<td>";
					$content .= AL_TEXT_IN . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_text" . $lang_array['number'], $row[ MENU_DB_FREE_TEXT . $lang_array['number'] ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "<br />\n";
					$content .= "</td>\n";
					$content .= "</tr>\n";
				}
			}

			// Header
			if ( $single_language ) {
				$content .= "<tr class='content_" . DROPDOWN_MENU_POINT_IS_HEADER . "'>";
				$content .= "<td>" . AL_TBL_TEXT . "</td><td>" . $this->l->text( "content_header", $row[ MENU_DB_HEADER_TEXT . $this->lang->lnbrm ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$content .= "</tr>\n";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . DROPDOWN_MENU_POINT_IS_HEADER . "'>";
					$content .= "<td>";
					$content .= AL_TEXT_IN . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_header" . $lang_array['number'], $row[ MENU_DB_HEADER_TEXT . $lang_array['number'] ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "<br />\n";
					$content .= "</td>\n";
					$content .= "</tr>\n";
				}
			}

			// Link to Page
			$content .= "<tr class='content_" . DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE . "'>";
			$content .= "<td>" . AL_TBL_PAGE . "</td><td>";
			$content .= $this->get_page_select( $row );
			$content .= "</td></tr>\n";

			$content .= $default_form_end;

			// Javascript
			$this->p->init_array( $GLOBALS['foot'], 30 );
			$GLOBALS['foot'][30] .= "<script>
    $( function() {
        change_content($( \"#content_type option:selected\" ).val());
        $(\"#content_type\").change(function() {
            change_content($( \"#content_type option:selected\" ).val());
        })
    });

    function change_content(id)
    {
        $(\".content_" . DROPDOWN_MENU_POINT_IS_LINK . "\").hide();
        $(\".content_" . DROPDOWN_MENU_POINT_IS_FREE_TEXT . "\").hide();
        $(\".content_" . DROPDOWN_MENU_POINT_IS_HEADER . "\").hide();
        $(\".content_" . DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE . "\").hide();
        $(\".disabled\").hide();
        $(\".link_target\").hide();
        $(\".content_name\").hide();

        if(id != " . DROPDOWN_MENU_POINT_IS_DIVIDER . ")
            $(\".content_\" + id).show();

        if(id==" . DROPDOWN_MENU_POINT_IS_LINK . " || id==" . DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE . ")
            $(\".link_target\").show();
        if(id==" . DROPDOWN_MENU_POINT_IS_LINK . " || id==" . DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE . ")
        {
            $(\".disabled\").show();
            $(\".content_name\").show();
        }
    }

</script>
    ";
		} // ULLI
		elseif ( $row['id_type'] == MENU_TYPE_ULLI ) {

			$content .= $default_form_start;
			$content .= $this->l->table();

			// Select Type
			$content .= "<tr><td>" . AL_TBL_TYPE . "</td><td>" . $this->l->select( "content_type", "id='content_type'" ) . "
                <option value='" . ULLI_MENU_POINT_IS_LINK . "'";
			$content_type == ULLI_MENU_POINT_IS_LINK ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_SIMPLE_MENU_LINK . "</option>
                <option value='" . ULLI_MENU_POINT_IS_FREE_TEXT . "'";
			$content_type == ULLI_MENU_POINT_IS_FREE_TEXT ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_FREE_TEXT . "</option>
                <option value='" . ULLI_MENU_POINT_IS_DIVIDER . "'";
			$content_type == ULLI_MENU_POINT_IS_DIVIDER ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_DIVIDER . "</option>
                <option value='" . ULLI_MENU_POINT_IS_DROPDOWN . "'";
			$content_type == ULLI_MENU_POINT_IS_DROPDOWN ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_DROPDOWN_LINK_SUBMENU . "</option>
                <option value='" . ULLI_MENU_POINT_IS_LINK_TO_PAGE . "'";
			$content_type == ULLI_MENU_POINT_IS_LINK_TO_PAGE ? $content .= " selected='selected'" : $content .= "";
			$content .= ">" . AL_LINK_TO_PAGE . "</option>
            </select>
            </td></tr>\n";

			// Simple Link
			$content .= "<tr class='content_" . ULLI_MENU_POINT_IS_LINK . "'>
                <td>" . AL_TBL_LINK . "</td><td>" . $this->l->text( "content_link", $row[ MENU_DB_LINK ] ) . " " . AL_HINT_LINK_EXAMPLE . "</td>
                </tr>\n";

			// Free Text
			if ( $single_language ) {
				$content .= "<tr class='content_" . ULLI_MENU_POINT_IS_FREE_TEXT . "'>";
				$content .= "<td>" . AL_TBL_TEXT . "</td><td>" . $this->l->text( "content_text", $row[ MENU_DB_FREE_TEXT . $this->lang->lnbrm ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$content .= "</tr>\n";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . ULLI_MENU_POINT_IS_FREE_TEXT . "'>";
					$content .= "<td>";
					$content .= AL_TEXT_IN . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_text" . $lang_array['number'], $row[ MENU_DB_FREE_TEXT . $lang_array['number'] ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "<br />\n";
					$content .= "</td>\n";
					$content .= "</tr>\n";
				}
			}

			// Dropdown
			if ( $single_language ) {
				$content .= "<tr class='content_" . ULLI_MENU_POINT_IS_DROPDOWN . "'>";
				$content .= "<td>" . AL_TBL_DROPDOWN_TEXT . "</td><td>" . $this->l->text( "content_dropdown", $row[ MENU_DB_DROPDOWN_TEXT . $this->lang->lnbrm ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "</td>\n";
				$content .= "</tr>\n";
				$content .= "<tr class='content_" . ULLI_MENU_POINT_IS_DROPDOWN . "'>";
				$content .= "<td>" . AL_TBL_LINK . "</td><td>" . $this->l->text( "content_link", $row[ MENU_DB_LINK_ULLI . $this->lang->lnbrm ] ) . " " . AL_HINT_LINK_EXAMPLE . "</td>\n";
				$content .= "</tr>\n";
			} else {
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . ULLI_MENU_POINT_IS_DROPDOWN . "'>";
					$content .= "<td>";
					$content .= AL_DROPDOWN_TEXT_IN . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_dropdown" . $lang_array['number'], $row[ MENU_DB_DROPDOWN_TEXT . $lang_array['number'] ] ) . " " . AL_HINT_HTML_CODE_ALLOWED . "<br />\n";
					$content .= "</td>\n";
					$content .= "</tr>\n";
				}
				foreach ( $this->lang->languages as $lang_array ) {
					$content .= "<tr class='content_" . ULLI_MENU_POINT_IS_DROPDOWN . "'>";
					$content .= "<td>";
					$content .= AL_LINK . " " . $lang_array['text'] . AL_TBL_COLON . "</td><td>" . $this->l->text( "content_link" . $lang_array['number'], $row[ MENU_DB_LINK_ULLI . $lang_array['number'] ] ) . " " . AL_HINT_LINK_EXAMPLE;
					$content .= "</td>\n";
					$content .= "</tr>\n";
				}
			}


			// Link to Page
			$content .= "<tr class='content_" . ULLI_MENU_POINT_IS_LINK_TO_PAGE . "'>";
			$content .= "<td>" . AL_TBL_PAGE . "</td><td>";
			$content .= $this->get_page_select( $row );
			$content .= "</td></tr>\n";

			$content .= $default_form_end;

			// Javascript
			$this->p->init_array( $GLOBALS['foot'], 30 );
			$GLOBALS['foot'][30] .= "<script>
    $( function() {
        change_content($( \"#content_type option:selected\" ).val());
        $(\"#content_type\").change(function() {
            change_content($( \"#content_type option:selected\" ).val());
        })
    });

    function change_content(id)
    {
        $(\".content_" . ULLI_MENU_POINT_IS_LINK . "\").hide();
        $(\".content_" . ULLI_MENU_POINT_IS_FREE_TEXT . "\").hide();
        $(\".content_" . ULLI_MENU_POINT_IS_DROPDOWN . "\").hide();
        $(\".content_" . ULLI_MENU_POINT_IS_LINK_TO_PAGE . "\").hide();
        $(\".disabled\").hide();
        $(\".link_target\").hide();
        $(\".content_name\").hide();

        if(id != " . ULLI_MENU_POINT_IS_DIVIDER . ")
            $(\".content_\" + id).show();

        if(id==" . ULLI_MENU_POINT_IS_LINK . " || id==" . ULLI_MENU_POINT_IS_LINK_TO_PAGE . ")
        {
            $(\".link_target\").show();
            $(\".content_name\").show();

        }
        if(id==" . ULLI_MENU_POINT_IS_LINK . " || id==" . ULLI_MENU_POINT_IS_DROPDOWN . " || id==" . ULLI_MENU_POINT_IS_LINK_TO_PAGE . ")
        {
            $(\".disabled\").show();

        }
    }
</script>
    ";
		} else {
			$this->log->error( __FILE__ . ":" . __LINE__, "php", "Can't find type for editing content of menu, type: " . $row['id_type'] . ", id: " . $row['id'] );
			$content .= "<H1>" . AL_ERROR_TYPE_NOT_FOUND . "</H1>\n";
		}

		return $content;
	}

	public function content_db( $update = false ) {
		$content = "";


		if ( $update ) {

			$GLOBALS['no_menu'] = true;

			$id = $this->p->get( "id", "" );
			if ( empty( $id ) || ! is_numeric( $id ) ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_MENU );
				$this->log->error( __FILE__ . ":" . __LINE__, "php", "Wrong or Missing ID of Sub-Menu, id: " . $id );

				return $content;
			}
		}

		$id_type      = $this->p->get( "id_type", "" );
		$content_type = $this->p->get( "content_type", "" );
		$to_id        = $this->p->get( "to_id", "" );

		if ( empty( $to_id ) || ! is_numeric( $to_id ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_MENU );
			$this->log->error( __FILE__ . ":" . __LINE__, "php", "Wrong or Missing ID of Menu, to_id: " . $to_id );

			return $content;
		}

		$id_option0   = "0";
		$id_option1   = "0";
		$id_option2   = "0";
		$id_option3   = "0";
		$menu_content = null;

		if ( $this->lang->count_of_languages <= 1 ) {
			$single_language = true;
		} else {
			$single_language = false;
		}

		if ( $single_language ) {
			$name = $this->p->get( "content_name", null );
		} else {
			foreach ( $this->lang->languages as $lang_array ) {
				${"name" . $lang_array['number']} = $this->p->get( "content_name" . $lang_array['number'] );
			}
		}

		$content_option0 = $this->p->get( "disabled", "0" );
		$content_option1 = $this->p->get( "target_blank", "0" );
		$content_option2 = "0";
		$content_option3 = "0";
		$content_option4 = "0";
		$pos             = $this->p->get( "pos", "0" );
		$css_id          = $this->p->get( "css_id", null );
		$css_class       = $this->p->get( "css_class", null );
		if ( ! is_null( $css_id ) ) {
			$css_id = $this->translate_special_characters( $css_id, NOT_A_FILE, NO_UNDERLINE );
		}
		if ( ! is_null( $css_class ) ) {
			$css_class = $this->translate_special_characters( $css_class, NOT_A_FILE, NO_UNDERLINE );
		}
		$active = $this->p->get( "active", "1" );

		$sql     = "SELECT `id` FROM `" . TP . "menus` WHERE `to_id` = '$to_id'";
		$result  = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		$max_pos = $result->num_rows;

		if ( ! empty( $pos ) ) {
			$pos = intval( $pos );
		}
		if ( ! $update ) {
			if ( empty( $pos ) || $pos >= $max_pos ) {
				$pos = $max_pos;
			} else {
				$pos    = $pos - 1;
				$sql    = "SELECT `id` FROM `" . TP . "menus` WHERE `to_id` = '$to_id' AND `pos` >= '" . $pos . "'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				if ( $result->num_rows > 0 ) {
					while ( $row = $result->fetch_assoc() ) {
						$sql = "UPDATE `" . TP . "menus` SET `pos` = (`pos` + 1) WHERE `id`='" . $row['id'] . "'";
						$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
					}
				}
			}
		} else {
			$sql     = "SELECT `pos` FROM `" . TP . "menus` WHERE `id` = '$id'";
			$result  = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			$row_old = $result->fetch_assoc();
			$old_pos = $row_old['pos'];

			if ( ( $pos - 1 ) != $old_pos ) {
				if ( empty( $pos ) || $pos >= $max_pos ) {
					$pos    = $max_pos - 1;
					$sql    = "SELECT `id` FROM `" . TP . "menus` WHERE `to_id` = '$to_id' AND `pos` >= '" . $old_pos . "'";
					$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
					if ( $result->num_rows > 0 ) {
						while ( $row = $result->fetch_assoc() ) {
							$sql = "UPDATE `" . TP . "menus` SET `pos` = (`pos` - 1) WHERE `id`='" . $row['id'] . "'";
							$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						}
					}
				} else {
					$pos = $pos - 1;
					if ( $pos < $old_pos ) {
						$sql    = "SELECT `id` FROM `" . TP . "menus` WHERE `to_id` = '$to_id' AND `pos` >= '" . $pos . "' AND `pos` < '" . $old_pos . "'";
						$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						if ( $result->num_rows > 0 ) {
							while ( $row = $result->fetch_assoc() ) {
								$sql = "UPDATE `" . TP . "menus` SET `pos` = (`pos` + 1) WHERE `id`='" . $row['id'] . "'";
								$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
							}
						}
					} elseif ( $pos > $old_pos ) {
						$sql    = "SELECT `id` FROM `" . TP . "menus` WHERE `to_id` = '$to_id' AND `pos` <= '" . $pos . "' AND `pos` > '" . $old_pos . "'";
						$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
						if ( $result->num_rows > 0 ) {
							while ( $row = $result->fetch_assoc() ) {
								$sql = "UPDATE `" . TP . "menus` SET `pos` = (`pos` - 1) WHERE `id`='" . $row['id'] . "'";
								$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
							}
						}
					}
				}
			} else {
				$pos = $pos - 1;
			}
		}

		switch ( $id_type ) {
			// NAV
			case MENU_TYPE_NAV:
				switch ( $content_type ) {
					// LINK
					case MENU_NAV_MENU_POINT_IS_LINK:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_link" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_link" );
							}
						}
						break;
					// Dropdown
					case MENU_NAV_MENU_POINT_IS_DROPDOWN:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_dropdown" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_dropdown" . $lang_array['number'] );
							}
						}
						break;
					// Custom Text
					case MENU_NAV_MENU_POINT_IS_FREE_TEXT:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_text" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_text" . $lang_array['number'] );
							}
						}
						break;
					// Link to page
					case MENU_NAV_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $single_language ) {
							$menu_content = $this->p->get( "page_id", 0 );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "page_id", 0 );
							}
						}
						break;
					// Divider
					case MENU_NAV_SUB_MENU_POINT_IS_DIVIDER:
						break;

					default:
						$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_MENU );
						$this->log->error( __FILE__ . ":" . __LINE__, "php", "Type of content is incorrect. content_type: $content_type, id_type: " . $id_type );

						return $content;
						break;
				}
				break;

			// NAVBAR
			case MENU_TYPE_NAVBAR:
				$id_option0      = $this->p->get( "align_right", "0" );
				$content_option2 = $this->p->get( "freetext_color", "0" );
				switch ( $content_type ) {
					// Link
					case NAVBAR_SIDE_MENU_POINT_IS_LINK:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_link" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_link" );
							}
						}
						break;
					// Dropdown
					case NAVBAR_SIDE_MENU_POINT_IS_DROPDOWN:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_dropdown" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_dropdown" . $lang_array['number'] );
							}
						}
						break;
					// Custom TExt
					case NAVBAR_SIDE_MENU_POINT_IS_FREE_TEXT:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_text" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_text" . $lang_array['number'] );
							}
						}
						break;
					// Button
					case NAVBAR_SIDE_MENU_POINT_IS_BUTTON:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_button" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_button" . $lang_array['number'] );
							}
						}
						$content_option3 = $this->p->get( "button_type" );
						$content_option4 = $this->p->get( "button_size", "" );
						break;
					// Text with form elements
					case NAVBAR_SIDE_MENU_POINT_IS_FORM_ELEMENTS:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_form" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_form" . $lang_array['number'] );
							}
						}
						break;
					// Link to page
					case NAVBAR_SIDE_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $single_language ) {
							$menu_content = $this->p->get( "page_id", 0 );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "page_id", 0 );
							}
						}
						break;
					// Divider
					case NAVBAR_SUB_POINT_IS_DIVIDER:
						break;

					default:
						$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_MENU );
						$this->log->error( __FILE__ . ":" . __LINE__, "php", "Type of content is incorrect. content_type: $content_type, id_type: " . $id_type );

						return $content;
						break;
				}
				break;

			// Dropdowns
			case MENU_TYPE_DROPDOWN:
				$id_option0 = $this->p->get( "align_right", "0" );
				switch ( $content_type ) {
					// Link
					case DROPDOWN_MENU_POINT_IS_LINK:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_link" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_link" );
							}
						}
						break;
					// Custom Text
					case DROPDOWN_MENU_POINT_IS_FREE_TEXT:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_text" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_text" . $lang_array['number'] );
							}
						}
						break;
					// Divider
					case DROPDOWN_MENU_POINT_IS_DIVIDER:
						break;
					// Header
					case DROPDOWN_MENU_POINT_IS_HEADER:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_header" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_header" . $lang_array['number'] );
							}
						}
						break;
					// Link to page
					case DROPDOWN_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $single_language ) {
							$menu_content = $this->p->get( "page_id", 0 );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "page_id", 0 );
							}
						}
						break;

					default:
						$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_MENU );
						$this->log->error( __FILE__ . ":" . __LINE__, "php", "Type of content is incorrect. content_type: $content_type, id_type: " . $id_type );

						return $content;
						break;
				}
				break;

			// ULLI
			case MENU_TYPE_ULLI:
				switch ( $content_type ) {
					// Link
					case ULLI_MENU_POINT_IS_LINK:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_link" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_link" );
							}
						}
						break;
					// Custom Text
					case ULLI_MENU_POINT_IS_FREE_TEXT:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_text" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_text" . $lang_array['number'] );
							}
						}
						break;
					// Divider
					case ULLI_MENU_POINT_IS_DIVIDER:

						break;
					// Dropdown
					case ULLI_MENU_POINT_IS_DROPDOWN:
						if ( $single_language ) {
							$menu_content = $this->p->get( "content_dropdown" );
							$name         = $this->p->get( "content_link" );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "content_dropdown" . $lang_array['number'] );
								${"name" . $lang_array['number']}         = $this->p->get( "content_link" . $lang_array['number'] );
							}
						}
						break;
					// Link to page
					case ULLI_MENU_POINT_IS_LINK_TO_PAGE:
						if ( $single_language ) {
							$menu_content = $this->p->get( "page_id", 0 );
						} else {
							foreach ( $this->lang->languages as $lang_array ) {
								${"menu_content" . $lang_array['number']} = $this->p->get( "page_id", 0 );
							}
						}
						break;

					default:
						$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_MENU );
						$this->log->error( __FILE__ . ":" . __LINE__, "php", "Type of content is incorrect. content_type: $content_type, id_type: " . $id_type );

						return $content;
						break;
				}
				break;

			default:
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_MENU );
				$this->log->error( __FILE__ . ":" . __LINE__, "php", "Type of menu is incorrect. id_type: " . $id_type );

				return $content;
				break;
		}

		if ( ! $update ) {
			if ( $single_language ) {
				$this->db->ins( __FILE__ . ":" . __LINE__, "menus", array(
					"id_type",
					"to_id",
					"id_option0",
					"id_option1",
					"id_option2",
					"id_option3",
					"content" . $this->lang->lnbrm,
					"content_type",
					"content_option0",
					"content_option1",
					"content_option2",
					"content_option3",
					"content_option4",
					"pos",
					"created",
					"css_id",
					"css_class",
					"name" . $this->lang->lnbrm
				), array(
					$id_type,
					$to_id,
					$id_option0,
					$id_option1,
					$id_option2,
					$id_option3,
					$menu_content,
					$content_type,
					$content_option0,
					$content_option1,
					$content_option2,
					$content_option3,
					$content_option4,
					$pos,
					"NOW()",
					$css_id,
					$css_class,
					$name
				) );
			} else {
				$array_keys   = array(
					"id_type",
					"to_id",
					"id_option0",
					"id_option1",
					"id_option2",
					"id_option3",
					"content_type",
					"content_option0",
					"content_option1",
					"content_option2",
					"content_option3",
					"content_option4",
					"pos",
					"created",
					"css_id",
					"css_class"
				);
				$array_values = array(
					$id_type,
					$to_id,
					$id_option0,
					$id_option1,
					$id_option2,
					$id_option3,
					$content_type,
					$content_option0,
					$content_option1,
					$content_option2,
					$content_option3,
					$content_option4,
					$pos,
					"NOW()",
					$css_id,
					$css_class
				);

				foreach ( $this->lang->languages as $lang_array ) {
					$array_keys[]   = "content" . $lang_array['number'];
					$array_keys[]   = "name" . $lang_array['number'];
					$array_values[] = ${"menu_content" . $lang_array['number']};
					$array_values[] = ${"name" . $lang_array['number']};
				}

				$this->db->ins( __FILE__ . ":" . __LINE__, "menus", $array_keys, $array_values );
			}
			$id = $this->db->get_insert_id();

			$content .= $this->l->alert_text_dismiss( "success", AL_MENUITEM_ADDED );
			$this->log->event( "log", __FILE__ . ":" . __LINE__, "New menucontent saved, id: " . $id );
		} else {

			if ( $single_language ) {
				$this->db->upd( __FILE__ . ":" . __LINE__, "menus", array(
					"id_type",
					"to_id",
					"id_option0",
					"id_option1",
					"id_option2",
					"id_option3",
					"content" . $this->lang->lnbrm,
					"content_type",
					"content_option0",
					"content_option1",
					"content_option2",
					"content_option3",
					"content_option4",
					"pos",
					"created",
					"css_id",
					"css_class",
					"name" . $this->lang->lnbrm
				), array(
					$id_type,
					$to_id,
					$id_option0,
					$id_option1,
					$id_option2,
					$id_option3,
					$menu_content,
					$content_type,
					$content_option0,
					$content_option1,
					$content_option2,
					$content_option3,
					$content_option4,
					$pos,
					"NOW()",
					$css_id,
					$css_class,
					$name
				), "`id`='$id'" );
			} else {
				$array_keys   = array(
					"id_type",
					"to_id",
					"id_option0",
					"id_option1",
					"id_option2",
					"id_option3",
					"content_type",
					"content_option0",
					"content_option1",
					"content_option2",
					"content_option3",
					"content_option4",
					"pos",
					"created",
					"css_id",
					"css_class"
				);
				$array_values = array(
					$id_type,
					$to_id,
					$id_option0,
					$id_option1,
					$id_option2,
					$id_option3,
					$content_type,
					$content_option0,
					$content_option1,
					$content_option2,
					$content_option3,
					$content_option4,
					$pos,
					"NOW()",
					$css_id,
					$css_class
				);

				foreach ( $this->lang->languages as $lang_array ) {
					$array_keys[]   = "content" . $lang_array['number'];
					$array_keys[]   = "name" . $lang_array['number'];
					$array_values[] = ${"menu_content" . $lang_array['number']};
					$array_values[] = ${"name" . $lang_array['number']};
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "menus", $array_keys, $array_values, "`id`='$id'" );
			}

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "New menucontent updated, id: " . $id );

			$GLOBALS['body_footer'] .= "<script>
        parent.$.fn.colorbox.close();
</script>\n";
			$_SESSION['content_update'] = 1;
			$this->l->reload_js( "content_menus_content.php", "id=$to_id", true );
		}

		return $content;
	}

	public function content_edit() {
		$content = "";


		$GLOBALS['admin_subtitle'] = AL_EDIT_MENU_CONTENT;

		$GLOBALS['no_menu'] = true;

		$id     = $this->p->get( "id" );
		$is_sub = $this->p->get( "is_sub", "0" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT * FROM `" . TP . "menus` WHERE `id`='" . $id . "' AND `deleted` = '0' ";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows == 0 ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CONTENT_NOT_FOUND );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: " . $id . ", SQL: " . $sql );
				$content .= $this->menu_edit();

				return $content;
			} else {
				$row = $result->fetch_assoc();

				$content .= $this->menu_point( $row, EMPTY_PARAMETER, "content_update", $is_sub );
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_CONTENT );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= $this->menu_edit();

			return $content;
		}

		return $content;
	}

	public function delete_content() {


		$content = "";

		$id = $this->p->get( "content_id" );

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_MENU );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or incorrect, id: " . $id );

			return $content;
		} else {
			$sql    = "SELECT `pos`,`to_id` FROM `" . TP . "menus` WHERE id = '$id' AND `deleted` = '0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows == 0 ) {
				$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_CONTENT );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "Content ID $id not found" );

				return $content;
			} else {
				$row = $result->fetch_assoc();
				$sql = "UPDATE `" . TP . "menus` SET `pos` = `pos` -1 WHERE `to_id` = '" . $row['to_id'] . "' AND `pos` > " . $row['pos'];
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				$this->rekursive_delete_menu( $id );

				$sql = "UPDATE `" . TP . "menus` SET `deleted` = '1' WHERE `id` = '$id'";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );

				$content .= $this->l->alert_text_dismiss( "success", AL_CONTENT_DELETED );
				$this->log->event( "log", __FILE__ . ":" . __LINE__, "Menuepoint deleted, id: " . $id );

				return $content;
			}
		}
	}

	public function content_status() {


		$content = "";

		$id = $this->p->get( "content_id" );

		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text_dismiss( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_MENUITEM_STATUS );
			$this->log->error( "form", __FILE__ . ":" . __LINE__, "ID value is empty or incorrect, id: " . $id );

			return $content;
		} else {
			$sql    = "SELECT `active` FROM `" . TP . "menus` WHERE `id` = '$id' AND `deleted` = '0'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows == 0 ) {
				$content .= $this->l->alert_text_dismiss( "danger", "Der Inhalt konnte nicht bearbeitet werden" );
				$this->log->error( "sql", __FILE__ . ":" . __LINE__, "Content ID $id not found" );

				return $content;
			} else {
				$row = $result->fetch_assoc();

				if ( $row['active'] ) {
					$sql = "UPDATE `" . TP . "menus` SET `active` = 0 WHERE `id` = '" . $id . "'";
					$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content status in menupoint changed, id: " . $id . ", new status: inactive" );
				} else {
					$sql = "UPDATE `" . TP . "menus` SET `active` = 1 WHERE `id` = '" . $id . "'";
					$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
					$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content status in menupoint changed, id: " . $id . ", new status: active" );
				}

				$content .= $this->l->alert_text_dismiss( "success", AL_STATUS_CHANGED );

				return $content;
			}
		}
	}

	public function menu_status() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT `active` FROM `" . TP . "menus` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();
				if ( $row['active'] == 0 ) {
					$active = 1;
				} else {
					$active = 0;
				}

				$sql = "UPDATE `" . TP . "menus` SET `active` = '$active' WHERE `id` = '$id'";
				$this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_MENU_STATUS );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_MENU_STATUS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_MENU_STATUS_CHANGE );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "menu status changed, id: " . $id );

		return $content;
	}
}

if ( ! isset( $action ) ) {
	$action = null;
}

$class_content_menus = new class_content_menus( $action );
$content .= $class_content_menus->get_content();
