<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_PAGES;

class class_content_pages extends class_sys {
	public $content;

	public function __construct( $action ) {
		parent::__construct();

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );

		switch ( $action ) {
			case "content_add":
				$this->content .= $this->content_add();
				break;
			case "content_save":
				$content_error = $this->content_save();
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add();
				}
				break;
			case "content_save_no_back":
				$content_error = $this->content_save( STAY_ON_PAGE );
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add();
				}
				break;
			case "content_delete":
				$this->content .= $this->content_delete();
				$this->content .= $this->start();
				break;
			case "content_edit":
				$this->content .= $this->content_add( EDIT );
				break;
			case "content_update":
				$content_error = $this->content_update();
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
					$this->content .= $this->content_add( EDIT );
				}
				break;
			case "content_update_no_back":
				$content_error = $this->content_update( STAY_ON_PAGE );
				if ( ! empty( $content_error ) ) {
					$this->content .= $content_error;
				}
				$this->content .= $this->content_add( EDIT );
				break;
			case "content_status":
				$this->content .= $this->content_status();
				$this->content .= $this->start();
				break;
			case "options":
				$this->content .= $this->options();
				break;
			case "options_update":
				$this->options_update();
				break;
			case "options_update_and_back":
				$this->options_update( NO_RELOAD );
				$this->content .= $this->start();
				break;
			case "search":
			case "init":
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";


		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );
		$content .= $this->l->display_message_by_session( 'content_added', AL_PAGE_ADDED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_PAGE_UPDATED );
		$content .= $this->sitemap->display_sitemap_message();

		$reset_search = $this->p->get( "reset_search", 0 );
		$search       = $this->p->get_session( "search", "", "text_search", $reset_search );
		$intext       = $this->p->get_session( "intext", 0, "text_intext", $reset_search );

		if ( empty( $search ) ) {
			$content .= "<h3 class='underline'>" . AL_PAGES . "</h3>\n";
		} else {
			$content .= "<h3 class='underline'>" . AL_PAGES . " " . AL_SEARCH_RESULT . "</h3>\n";
		}

		if ( empty( $search ) ) {
			$sql    = "SELECT * FROM `" . TP . "pages` WHERE `deleted` = '0' ORDER BY `name`";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		} else {
			$search_text = "";
			if ( $intext ) {
				$search_text = "OR " . $this->db->search( "text", $search );
			}
			$sql    = "SELECT * FROM `" . TP . "pages` WHERE (" . $this->db->search( "header", $search ) . " OR " . $this->db->search( "name", $search ) . " $search_text) AND `deleted`=0 ORDER BY 'name'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		}

		$content .= $this->l->form_admin( "", "content_pages_add.php" ) . $this->l->submit( AL_ADD_NEW_PAGE ) . "</form>";

		$content .= " " . $this->l->form_admin( "", "content_pages_options.php" ) . $this->l->submit( AL_SETTINGS ) . "</form>";

		if ( $result->num_rows > 0 || ! empty( $search ) ) {

			$content .= " " . $this->l->form_admin( "name='form_search'" ) . $this->l->text( "search", $search, "250", "id='search_text'" ) . $this->l->hidden( "do", "search" ) . " " . $this->l->submit( AL_SEARCH, "id='search'" ) . " <nobr>" . $this->l->checkbox( "intext", "1", $intext, "0", "style='display:inline'" ) . " " . AL_IN_TEXT . "</nobr>
                <span id='message' class='error'>| " . AL_ERROR . ": " . AL_MISSING_SEARCH_TEXT . "</span>\n";
			$content .= "    </form>\n";

			if ( ! empty( $search ) ) {
				$content .= $this->l->form_admin() . $this->l->hidden( "reset_search", "1" ) . $this->l->submit( AL_RESET );
				$content .= "    </form>\n";
			}

			$GLOBALS['body_footer'] .= "
        <script>

            $(document).ready(function () {
                $(\"#message\").hide();
            });
            $(\"form[name=form_search]\").submit(function( event ) {
                var val = $(\"input[name=search]\").val();
                if(val == '')
                {
                    $(\"#message\").fadeIn();
                    event.preventDefault();
                }
                else
                {
                    $(\"#message\").fadeOut();
                }
            });
             </script>\n";
		}

		if ( $result->num_rows != 0 ) {
			$handler = $this->tbl->tbl_init( AL_TBL_NAME, AL_TBL_HEADER, AL_TBL_CONTENT, AL_TBL_DATE, array(
				AL_TBL_CHARS,
				"nosort"
			), AL_TBL_ACTIVE, array( AL_TBL_ACTION, "nosort" ) );

			$entry_counter = 0;
			while ( $row = $result->fetch_assoc() ) {
				$text = $this->l->fix_ta( $row[ 'text' . $this->lang->lnbrm ] );
				$text = $this->l->ta( "text_" . $entry_counter, $text, "500", "180", "readonly=\"readonly\"" );

				$text .= "<p>" . AL_PATH_COLON . " " . $row['path'] . "</p>";

				$active = $this->lang->answer( $row['active'] );
				if ( $row['active'] == "0" ) {
					$submit_status = $this->l->submit( AL_ACTIVATE, EMPTY_FREE_TEXT, "btn btn-info" );
				} elseif ( $row['active'] == "1" ) {
					$submit_status = $this->l->submit( AL_DEACTIVATE );
				}

				if ( is_null( $row['changed'] ) || $row['changed'] == "0000-00-00 00:00:00" ) {
					$date      = AL_NOT_SPECIFIED_SHORT;
					$timestamp = 0;
				} else {
					$date      = $this->dt->sqldatetime( $row['changed'] );
					$timestamp = strtotime( $date );
				}

				$parameter = "id=" . $row["id"];

				$chars = $this->p->strlen_language_all( "pages", "text", $row['id'] );

				$this->tbl->tbl_load( $handler, $row['name'], $row['header'], $text, array(
					$timestamp,
					"time",
					$date
				), $chars, $active,
					$this->l->form_submit_by_lang( INSERT_BREAKS, EMPTY_FREETEXT, "content_pages_edit.php", $parameter ) . "<br />" . "</form> " . $this->l->form_admin( EMPTY_FREE_TEXT, DEFAULT_ACTION, $parameter ) . $this->l->hidden( "do", "content_status" ) . $submit_status . "</form> " .
					$this->l->form_admin( "id=\"form_admin_delete_content_$entry_counter\"" ) . $this->l->hidden( "do", "content_delete" ) . $this->l->hidden( "id", $row["id"] ) . $this->l->button( AL_DELETE, "id=\"button_delete_content_$entry_counter\"" ) . "</form>" );
				$this->l->box_confirm( AL_DELETE_QUESTION, AL_DELETE_PAGE_PART01 . " \"" . $row['name'] . "\" " . AL_DELETE_PAGE_PART02, "#button_delete_content_$entry_counter", "form_admin_delete_content_$entry_counter", "confirm_delete_$entry_counter" );

				$entry_counter ++;
			}

			$content .= $this->tbl->tbl_out( $handler, 0 );
		} else {
			if ( ! empty( $search ) ) {
				$content .= "<h3>" . AL_NO_MATCHES . "</h3>";
			} else {
				$content .= "<h3>" . AL_NO_ENTRIES . "</h3>";
			}
		}

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_PAGE_START, "info", AL_INFORMATION );

		return $content;
	}

	public function options() {
		$content = "";


		$GLOBALS['admin_subtitle'] = AL_PAGE_OPTIONS;

		$content .= $this->l->display_message_by_session( 'options_updated', AL_SETTINGS_SAVED );

		$content .= "<h3 class='underline'>" . AL_GLOBAL_PAGE_OPTIONS . "</h3>\n";

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "options_update" ) . $this->l->table() . "
    <tr><td></td><td>" . $this->l->submit( AL_SAVE ) . " " . $this->l->button( AL_SAVE_AND_BACK, "onclick='save_and_back(this.form);'" ) . " " . $this->l->back_button( AL_BACK, "content_pages.php" ) . "</td></tr>
    <tr><td>" . AL_USE_ENHANCED_EDITOR . ":</td><td>" . $this->l->select_yesno( "content_page_enhanced_editor", CONTENT_PAGE_ENHANCED_EDITOR ) . "</td></tr>
    <tr><td>Zeige Überschrift:</td><td>" . $this->l->select_yesno( "content_page_show_header", CONTENT_PAGE_SHOW_HEADER ) . "</td></tr>
    <tr><td>Zeige Datum:</td><td>" . $this->l->select_yesno( "content_page_show_date", CONTENT_PAGE_SHOW_DATE ) . "</td></tr>
    </table></form>\n";

		$content .= $this->l->panel( AL_HELP_ENHANCED_EDITOR_ADVANTAGES, "info", AL_INFORMATION );

		$GLOBALS['body_footer'] .= "<script>
    function save_and_back(formID)
    {
        formID.do.value = 'options_update_and_back';
        formID.action = '/" . WEBROOT . ADMINDIR . "content_pages.php';
        formID.submit();
    }
    </script>
    ";

		return $content;
	}

	public function options_update( $reload = true ) {


		$elements                                 = array();
		$elements['content_page_enhanced_editor'] = $this->p->get( "content_page_enhanced_editor", "1" );
		$elements['content_page_show_header']     = $this->p->get( "content_page_show_header", "1" );
		$elements['content_page_show_date']       = $this->p->get( "content_page_show_date", "1" );

		foreach ( $elements as $key => $value ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "settings", array( "value" ), array( $value ), "UPPER(`key`) LIKE UPPER('$key')" );

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Settings updated, key: " . $key . ", value: " . $value );
		}

		$_SESSION['options_updated'] = 1;

		if ( $reload ) {
			$this->l->reload_js();
		}
	}

	public function content_add( $edit = false ) {
		$content = "";


		if ( $edit ) {
			$GLOBALS['admin_subtitle'] = AL_EDIT_PAGE;
		} else {
			$GLOBALS['admin_subtitle'] = AL_ADD_PAGE;
		}

		$content .= $this->l->display_message_by_session( 'content_added', AL_PAGE_ADDED );
		$content .= $this->l->display_message_by_session( 'content_updated', AL_PAGE_UPDATED );
		$content .= $this->sitemap->display_sitemap_message();

		if ( $edit ) {
			$content .= "<h3 class='underline'>" . AL_EDIT_PAGE . "</h3>\n";
		} else {
			$content .= "<h3 class='underline'>" . AL_ADD_PAGE . "</h3>\n";
		}

		$lang_number        = $this->p->get( "lang" );
		$import_lang_number = $this->p->get( "import_lang" );
		$change_editortype  = $this->p->get( "change_editortype", - 1 );

		//var_dump($edit, $change_in_editortype);
		if ( $edit && $change_editortype == "-1" ) {
			$id = $this->p->get( "id" );
			if ( empty( $id ) || ! is_numeric( $id ) ) {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_PAGE );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: '" . $id . "'" );
				$content .= $this->start();

				return $content;
			} else {
				$sql    = "SELECT * FROM `" . TP . "pages` WHERE `id` = '$id'";
				$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				if ( $result->num_rows == 0 ) {
					$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_EDIT_PAGE );
					$this->log->error( "sql", __FILE__ . ":" . __LINE__, "ID not found: '" . $id . "', SQL: '" . $sql . "'" );
					$content .= $this->start();

					return $content;
				} else {
					$row             = $result->fetch_assoc();
					$row['datetime'] = $this->dt->sqldatetime( $row['datetime'] );
					$old_name        = $row['name'];
					if ( is_null( $row['sub_layout'] ) ) {
						$row['sub_layout'] = "main";
					}
					$hash = $this->p->unique_id( BIG );
				}
			}
		} else {
			$hash                                     = $this->p->get( "hash", $this->p->unique_id( BIG ) );
			$old_name                                 = $this->p->get( "old_name", "", NOT_ESCAPED );
			$row['name']                              = $this->p->get( "name", "", NOT_ESCAPED );
			$row[ 'header' . $lang_number ]           = $this->p->get( "header", "", NOT_ESCAPED );
			$row[ 'text' . $lang_number ]             = $this->p->get( "text", "", NOT_ESCAPED );
			$row[ 'js' . $lang_number ]               = $this->p->get( "js", "", NOT_ESCAPED );
			$row[ 'css' . $lang_number ]              = $this->p->get( "css", "", NOT_ESCAPED );
			$row['active']                            = $this->p->get( "active", "1" );
			$row['datetime']                          = $this->p->get( "datetime", "" );
			$row['enhanced_editor']                   = $this->p->get( "enhanced_editor", CONTENT_PAGE_ENHANCED_EDITOR );
			$row['class_body']                        = $this->p->get( "class_body", "", NOT_ESCAPED );
			$row[ 'subtitle' . $lang_number ]         = $this->p->get( "subtitle", "", NOT_ESCAPED );
			$row[ 'path' . $lang_number ]             = $this->p->get( "path", "", NOT_ESCAPED );
			$row[ 'sub_layout' . $lang_number ]       = $this->p->get( "sub_layout", "main" );
			$row['show_layout']                       = $this->p->get( "show_layout", true );
			$row['in_sitemap']                        = $this->p->get( "in_sitemap", true );
			$row['parse_php']                         = $this->p->get( "parse_php", false );
			$row[ 'meta_description' . $lang_number ] = $this->p->get( "meta_description", "", NOT_ESCAPED );
			$row[ 'meta_keywords' . $lang_number ]    = $this->p->get( "meta_keywords", "", NOT_ESCAPED );
			$row['password_protection']               = $this->p->get( "password_protection", 0, NOT_ESCAPED );
			$row['password']                          = $this->p->get( "password", "", NOT_ESCAPED );
			$row['username']                          = $this->p->get( "username", "", NOT_ESCAPED );
			$row[ 'login_text' . $lang_number ]       = $this->p->get( "login_text", "", NOT_ESCAPED );
			$id                                       = $this->p->get( "id", "" );
		}

		if ( ! is_null( $import_lang_number ) ) {
			if ( strcasecmp( $import_lang_number, "empty" ) === 0 ) {
				$import_lang_number = "";
			}
			$row[ 'js' . $lang_number ]               = $row[ 'js' . $import_lang_number ];
			$row[ 'css' . $lang_number ]              = $row[ 'css' . $import_lang_number ];
			$row[ 'text' . $lang_number ]             = $row[ 'text' . $import_lang_number ];
			$row[ 'header' . $lang_number ]           = $row[ 'header' . $import_lang_number ];
			$row[ 'subtitle' . $lang_number ]         = $row[ 'subtitle' . $import_lang_number ];
			$row[ 'sub_layout' . $lang_number ]       = $row[ 'sub_layout' . $import_lang_number ];
			$row[ 'path' . $lang_number ]             = $row[ 'path' . $import_lang_number ];
			$row[ 'meta_description' . $lang_number ] = $row[ 'meta_description' . $import_lang_number ];
			$row[ 'meta_keywords' . $lang_number ]    = $row[ 'meta_keywords' . $import_lang_number ];
			$row[ 'login_text' . $lang_number ]       = $row[ 'login_text' . $import_lang_number ];
		}

		$content .= $this->l->table();

		if ( $edit ) {
			if ( $this->lang->count_of_languages > 1 ) {
				$content .= "<tr>";
				$content .= "<td>" . AL_TBL_LANGUAGE_FUNCTIONS . "</td><td>";
				$content .= $this->l->language_import_fields( "content_pages_edit.php", $id, $lang_number ) . $this->l->language_edit_fields( "content_pages_edit.php", $id, $lang_number ) . "</td>
            </tr>";
			}

			$content .= "<tr><td></td><td>";

			$content .= $this->l->form_admin( "name='form_action'", "content_pages_edit.php", "id=$id" );
			$content .= $this->l->hidden( "do", "content_update" );
			$content .= $this->l->hidden( "do_no_back", "content_update_no_back" );
		} else {
			$content .= "<tr><td></td><td>";
			$content .= $this->l->form_admin( "name='form_action'", "content_pages_add.php" );
			$content .= $this->l->hidden( "do", "content_save" );
			$content .= $this->l->hidden( "do_no_back", "content_save_no_back" );
		}

		$GLOBALS['body_footer'] .= "<script>
    function check_submit(formID, no_back)
    {
        str = formID.name.value;
        if(str == '')
        {
            " . $this->l->box_alert( "<H3 class='error underline'>" . AL_ERROR . "</H3><p>" . AL_MUST_ENTER_NAME . "</p>" ) . "
        }
        else
        {
            if(no_back)
            {
                formID.do.value = formID.do_no_back.value;
            }
            formID.submit();
        }
    }
    </script>
    ";

		$content .= $this->l->hidden( "hash", $hash ) . $this->l->hidden( "old_name", $old_name ) . $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " .
		            $this->l->button( AL_SAVE_AND_BACK, "onclick='check_submit(this.form, false)'" ) . " ";

		if ( $edit ) {
			$content .= $this->l->reload_button( "content_pages_edit.php", "id=$id&lang=$lang_number", AL_RELOAD ) . " ";
		} else {
			$content .= $this->l->reload_button( "content_pages_add.php", NO_PARAMETER, AL_RELOAD ) . " ";
		}

		$content .= $this->l->back_button( AL_BACK, "content_pages.php" ) . "</td></tr>\n";

		if ( $edit ) {
			$content .= "<tr><td>" . AL_TBL_ID . "</td><td>" . $id . "</td></tr>\n";
		}

		if ( $this->lang->count_of_languages > 1 ) {
			$content .= "<tr><td>" . AL_TBL_CURRENT_LANGUAGE . "</td><td><strong>" . $this->p->get_language_text( $lang_number ) . $this->l->hidden( "lang", $lang_number ) . "</strong></td></tr>\n";
		} else {
			$content .= $this->l->hidden( "lang", $this->lang->lnbrm );
		}

		$content .= "
    <tr><td>" . AL_TBL_NAME . "</td><td>" . $this->l->text( "name", $row['name'] ) . "</td></tr>
    <tr><td>" . AL_TBL_SUBTITLE . $this->p->get_lhint() . "</td><td>" . $this->l->text( "subtitle", $row[ 'subtitle' . $lang_number ] ) . "</td></tr>
    <tr><td>" . AL_TBL_PATH_FILENAME . $this->p->get_lhint() . "</td><td>" . $this->l->text( "path", $row[ 'path' . $lang_number ] ) . " " . AL_HINT_WITHOUT_LEADING_SLASH . "</td></tr>
    <tr><td>" . AL_TBL_TEXT . $this->p->get_lhint() . "</td><td>";

		$rows = "";
		if ( CONTENT_PAGE_ENHANCED_EDITOR ) {
			if ( $change_editortype == "-1" ) {
				if ( $row['enhanced_editor'] ) {
					$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
					$enhanced_editor = EDITOR_ENHANCED;
				} else {
					$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
					$rows            = "20";
					$enhanced_editor = EDITOR_SIMPLE;
				}
			} elseif ( $change_editortype ) {
				$content .= $this->l->button( AL_DEACTIVATE_ENHANCED_EDITOR, "id='change_editor_simple'" );
				$enhanced_editor = EDITOR_ENHANCED;
			} else {
				$content .= $this->l->button( AL_ACTIVATE_ENHANCED_EDITOR, "id='change_editor_enhanced'" );
				$rows            = "20";
				$enhanced_editor = EDITOR_SIMPLE;
			}

			$content .= $this->l->hidden( "enhanced_editor", $enhanced_editor ) . $this->l->hidden( "change_editortype", "-1" );

			if ( $edit ) {
				$action_js = "content_edit";
			} else {
				$action_js = "content_add";
			}

			$GLOBALS['body_footer'] .= "
<script>
    $(\"#change_editor_enhanced\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('1');
        $(\"form[name=form_action]\").submit();
    });
    $(\"#change_editor_simple\").click(function() {
        $(\"input[name=do]\").val('$action_js');
        $(\"input[name=change_editortype]\").val('0');
        $(\"form[name=form_action]\").submit();
    });

</script>
";
		} else {
			$enhanced_editor = EDITOR_SIMPLE;
			$rows            = "20";
			$content .= $this->l->hidden( "enhanced_editor", $row['enhanced_editor'] );
		}

		$ta_freetext = "";
		$ta_hint     = "";
		if ( $this->l->get_preview_status() ) {
			$ta_freetext = "readonly=\"readonly\"";
			$ta_hint     = "\n<br /><p class='info'>" . AL_JS_DISABLED_PREVIEW . ".</p>\n";
		}

		$sub_layouts   = $this->p->get_dir_content( $GLOBALS['layout_d'] . $GLOBALS['theme'] );
		$dont_list_sub = array( "img", "js", "flags" );
		foreach ( $sub_layouts as $key => $dir ) {
			if ( in_array( $dir, $dont_list_sub ) ) {
				unset( $sub_layouts[ $key ] );
			}
		}

		$content .= $this->l->media_ta( "text", $row[ 'text' . $lang_number ], UPLOADDIR, $enhanced_editor, $rows ) . "</td></tr>
    <tr><td>" . AL_TBL_JS . $this->p->get_lhint() . "</td><td>" . $this->l->media_ta( "js", $row[ 'js' . $lang_number ], UPLOADDIR, EDITOR_CODE, "100%", $ta_freetext, "ta ta_media", NO_ADDITONAL_BUTTONS, "js" ) . $ta_hint . "</td></tr>
    <tr><td>" . AL_TBL_CSS . $this->p->get_lhint() . "</td><td>" . $this->l->media_ta( "css", $row[ 'css' . $lang_number ], UPLOADDIR, EDITOR_CODE, "100%", EMPTY_FREETEXT, "ta ta_media", NO_ADDITONAL_BUTTONS, "css" ) . "</td></tr>
    <tr><td>" . AL_TBL_ACTIVE . "</td><td>" . $this->l->select_yesno( "active", $row['active'] ) . "</td></tr>
    <tr><td>" . AL_TBL_DATE . "</td><td>" . $this->l->datetime( "datetime", $row['datetime'] ) . "</td></tr>
    <tr><td>" . AL_TBL_HEADER . $this->p->get_lhint() . "</td><td>" . $this->l->text( "header", $row[ 'header' . $lang_number ] ) . "</td></tr>
    <tr><td>" . AL_TBL_USE_SITEMAP . "</td><td>" . $this->l->select_yesno( "in_sitemap", $row['in_sitemap'] ) . "</td></tr>
    <tr><td>" . AL_TBL_USE_LAYOUT . "</td><td>" . $this->l->select_yesno( "show_layout", $row['show_layout'] ) . "</td></tr>
    <tr><td>" . AL_TBL_PARSE_PHP . "</td><td>" . $this->l->select_yesno( "parse_php", $row['parse_php'] ) . " " . AL_HELP_PARSE_PHP . "</td></tr>
    <tr><td>" . AL_TBL_SUB_LAYOUT . $this->p->get_lhint() . "</td><td>" . $this->l->select( "sub_layout" );


		if ( ! is_null( $row[ 'sub_layout' . $lang_number ] ) ) {
			$sub_layout_select_field = $row[ 'sub_layout' . $lang_number ];
		} else {
			$sub_layout_select_field = $row[ 'sub_layout' . $this->lang->lnbrm ];
		}

		foreach ( $sub_layouts as $dir ) {
			$content .= "<option";
			if ( strcasecmp( $dir, $sub_layout_select_field ) === 0 ) {
				$content .= " selected=selected";
			}
			$content .= ">$dir</option>\n";
		}

		$content .= "</select></td></tr>
    <tr><td>" . AL_TBL_BODY_CLASS . "</td><td>" . $this->l->text( "class_body", $row['class_body'] ) . "</td></tr>

    <tr><td>" . AL_TBL_META_DESCRIPTION . $this->p->get_lhint() . "</td><td>" . $this->l->ta_char_counter( "meta_description", $row[ 'meta_description' . $lang_number ], "600", "150", EMPTY_FREE_TEXT, AUTO_ID, 160 ) . "</td></tr>
    <tr><td>" . AL_TBL_META_KEYWORDS . $this->p->get_lhint() . "</td><td>" . $this->l->ta_char_counter( "meta_keywords", $row[ 'meta_keywords' . $lang_number ], "600", "150", EMPTY_FREE_TEXT, AUTO_ID, 200 ) . "</td></tr>

    <tr><td></td><td>" . $this->l->button( AL_SAVE, "onclick='check_submit(this.form, true)'" ) . " " . $this->l->back_button( AL_BACK, "content_pages.php" ) . "</td></tr>

    <tr><td>" . AL_TBL_USE_PASSWORD_PROTECTION . "</td><td>" . $this->l->select_yesno( "password_protection", $row['password_protection'] ) . "</td></tr>
    <tr><td>" . AL_TBL_USERNAME . "</td><td>" . $this->l->text( "username", $row['username'] ) . "</td></tr>
    <tr><td>" . AL_TBL_PASSWORD . "</td><td>" . $this->l->text( "password", $row['password'] ) . "</td></tr>
    <tr><td>" . AL_TBL_LOGIN_TEXT . $this->p->get_lhint() . "</td><td>" . $this->l->text( "login_text", $row[ 'login_text' . $lang_number ] ) . "</td></tr>

    </table></form>
    ";

		if ( $this->lang->count_of_languages > 1 ) {
			$language_hint = AL_HELP_LANGUAGE_HINT;
		} else {
			$language_hint = "";
		}

		$content .= "<p> </p>
        " . $this->l->panel( AL_HELP_PAGE_EDIT . AL_HELP_ENHANCED_EDITOR_ADVANTAGES . AL_HELP_JAVASCRIPT_TAGS_NEEDED . AL_HELP_CSS_TAGS_NEEDED . AL_HELP_TEXT_MODS . $language_hint, "info", AL_INFORMATION );

		return $content;
	}

	public function content_save( $stay_on_page = false ) {
		$content = "";


		$lang_number = $this->p->get( "lang" );

		$name                      = $this->p->get( "name", "", NOT_ESCAPED );
		$header                    = $this->p->get( "header", "" );
		$password_protection       = $this->p->get( "password_protection", 0 );
		$username                  = $this->p->get( "username", "" );
		$password                  = $this->p->get( "password", "" );
		$login_text                = $this->p->get( "login_text", "" );
		$text                      = $this->p->get( "text", "" );
		$js                        = $this->p->get( "js", "" );
		$css                       = $this->p->get( "css", "" );
		$active                    = $this->p->get( "active", 1 );
		$datetime                  = $this->p->get( "datetime", "" );
		$enhanced_editor           = $this->p->get( "enhanced_editor", 1 );
		$class_body                = $this->p->get( "class_body", "" );
		$GLOBALS['admin_subtitle'] = $this->p->get( "subtitle", "" );
		$path                      = $this->p->get( "path", "" );
		$show_layout               = $this->p->get( "show_layout", 1 );
		$sub_layout                = $this->p->get( "sub_layout", null );
		$in_sitemap                = $this->p->get( "in_sitemap", 1 );
		$parse_php                 = $this->p->get( "parse_php", 0 );
		$meta_description          = $this->p->get( "meta_description", "" );
		$meta_keywords             = $this->p->get( "meta_keywords", "" );

		if ( empty( $datetime ) ) {
			//$datetime = date("Y-m-d H:i:s");
		} else {
			$datetime = $this->dt->sqldatetime( $datetime, "out" );
		}

		$name = trim( $name );
		$name = $this->translate_special_characters( $name, NOT_A_FILE, NO_UNDERLINE );
		$name = $this->db->escape( $name );
		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME, true, "", "content_pages_add.php" );

			return $content;
		}

		$sql    = "SELECT `id` FROM `" . TP . "pages` WHERE `name` LIKE '$name'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows != 0 ) {
			$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02, true, "", "content_pages_add.php" );

			return $content;
		}

		if ( $this->l->get_preview_status() ) {
			$this->db->ins( __FILE__ . ":" . __LINE__, "pages", array(
				"name",
				"header" . $lang_number,
				"text" . $lang_number,
				"active",
				"datetime",
				"created",
				"enhanced_editor",
				"class_body",
				"subtitle" . $lang_number,
				"path" . $lang_number,
				"in_sitemap",
				"css" . $lang_number,
				"parse_php",
				"meta_description" . $lang_number,
				"meta_keywords" . $lang_number
			), array(
				$name,
				$header,
				$text,
				$active,
				$datetime,
				"NOW()",
				$enhanced_editor,
				$class_body,
				$GLOBALS['admin_subtitle'],
				$path,
				$in_sitemap,
				$css,
				false,
				$meta_description,
				$meta_keywords
			) );
		} else {
			$this->db->ins( __FILE__ . ":" . __LINE__, "pages", array(
				"name",
				"header" . $lang_number,
				"text" . $lang_number,
				"js" . $lang_number,
				"active",
				"datetime",
				"created",
				"enhanced_editor",
				"class_body",
				"subtitle" . $lang_number,
				"path" . $lang_number,
				"show_layout",
				"sub_layout" . $lang_number,
				"in_sitemap",
				"css" . $lang_number,
				"parse_php",
				"meta_description" . $lang_number,
				"meta_keywords" . $lang_number,
				"password_protection",
				"password",
				"username",
				"login_text" . $lang_number
			),
				array(
					$name,
					$header,
					$text,
					$js,
					$active,
					$datetime,
					"NOW()",
					$enhanced_editor,
					$class_body,
					$GLOBALS['admin_subtitle'],
					$path,
					$show_layout,
					$sub_layout,
					$in_sitemap,
					$css,
					$parse_php,
					$meta_description,
					$meta_keywords,
					$password_protection,
					$password,
					$username,
					$login_text
				) );
		}

		$id = $this->db->get_insert_id();

		$this->log->event( "log", __FILE__ . ":" . __LINE__, "New content_text added, name: $name, id: " . $id );

		$_SESSION['content_added'] = 1;
		$this->sitemap->update_sitemap_automatic();

		if ( $stay_on_page ) {
			$this->l->reload_js( "content_pages_edit.php", "id=$id" );
		} else {
			$this->l->reload_js( "content_pages.php" );
		}

		return $content;
	}

	public function content_update( $stay_on_page = false ) {
		$content = "";


		$lang_number = $this->p->get( "lang" );

		$id                        = $this->p->get( "id" );
		$enhanced_editor           = $this->p->get( "enhanced_editor", 1 );
		$header                    = $this->p->get( "header", "" );
		$text                      = $this->p->get( "text", "" );
		$js                        = $this->p->get( "js", "" );
		$css                       = $this->p->get( "css", "" );
		$active                    = $this->p->get( "active", 1 );
		$datetime                  = $this->p->get( "datetime", "" );
		$name                      = $this->p->get( "name", "", NOT_ESCAPED );
		$password_protection       = $this->p->get( "password_protection", 0 );
		$username                  = $this->p->get( "username", "" );
		$password                  = $this->p->get( "password", "" );
		$login_text                = $this->p->get( "login_text", "" );
		$old_name                  = $this->p->get( "old_name" );
		$GLOBALS['admin_subtitle'] = $this->p->get( "subtitle", "" );
		$class_body                = $this->p->get( "class_body" );
		$path                      = $this->p->get( "path", "" );
		$show_layout               = $this->p->get( "show_layout", 1 );
		$sub_layout                = $this->p->get( "sub_layout", null );
		$in_sitemap                = $this->p->get( "in_sitemap", 1 );
		$parse_php                 = $this->p->get( "parse_php", 0 );
		$meta_description          = $this->p->get( "meta_description", "" );
		$meta_keywords             = $this->p->get( "meta_keywords", "" );

		if ( empty( $datetime ) ) {
			//$datetime = date("Y-m-d H:i:s");
		} else {
			$datetime = $this->dt->sqldatetime( $datetime, "out" );
		}

		$name = trim( $name );
		$name = $this->translate_special_characters( $name, NOT_A_FILE, NO_UNDERLINE );
		$name = $this->db->escape( $name );
		if ( empty( $name ) ) {
			$content .= $this->l->alert_text( "danger", AL_ENTER_VALID_NAME, true, "", "content_pages_edit.php" );

			return $content;
		}
		if ( $old_name != $name ) {
			$sql    = "SELECT `id` FROM `" . TP . "pages` WHERE `name` LIKE '$name'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows != 0 ) {
				$content .= $this->l->alert_text( "danger", AL_NAME_ALREADY_TAKEN_PART01 . " \"$name\" " . AL_NAME_ALREADY_TAKEN_PART02, HISTORY_BACK, NO_PARAMETER, "content_pages_edit.php" );

				return $content;
			}
		}
		if ( empty( $id ) || ! is_numeric( $id ) ) {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_UPDATE_PAGE );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );
			$content .= $this->start();

			return $content;
		} else {
			if ( $this->l->get_preview_status() ) {
				$this->db->upd( __FILE__ . ":" . __LINE__, "pages", array(
					"name",
					"header" . $lang_number,
					"text" . $lang_number,
					"active",
					"datetime",
					"enhanced_editor",
					"subtitle" . $lang_number,
					"class_body",
					"path" . $lang_number,
					"in_sitemap",
					"css" . $lang_number,
					"meta_description" . $lang_number,
					"meta_keywords" . $lang_number
				), array(
					$name,
					$header,
					$text,
					$active,
					$datetime,
					$enhanced_editor,
					$GLOBALS['admin_subtitle'],
					$class_body,
					$path,
					$in_sitemap,
					$css,
					$meta_description,
					$meta_keywords
				), "`id` = '$id'" );
			} else {
				$this->db->upd( __FILE__ . ":" . __LINE__, "pages", array(
					"name",
					"header" . $lang_number,
					"text" . $lang_number,
					"js" . $lang_number,
					"active",
					"datetime",
					"enhanced_editor",
					"subtitle" . $lang_number,
					"class_body",
					"path" . $lang_number,
					"show_layout",
					"sub_layout" . $lang_number,
					"in_sitemap",
					"css" . $lang_number,
					"parse_php",
					"meta_description" . $lang_number,
					"meta_keywords" . $lang_number,
					"password_protection",
					"password",
					"username",
					"login_text" . $lang_number
				), array(
					$name,
					$header,
					$text,
					$js,
					$active,
					$datetime,
					$enhanced_editor,
					$GLOBALS['admin_subtitle'],
					$class_body,
					$path,
					$show_layout,
					$sub_layout,
					$in_sitemap,
					$css,
					$parse_php,
					$meta_description,
					$meta_keywords,
					$password_protection,
					$password,
					$username,
					$login_text
				), "`id` = '$id'" );
			}

			$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content_text changed, name: $name, id: " . $id );

			$_SESSION['content_updated'] = 1;
			$this->sitemap->update_sitemap_automatic();

			if ( ! $stay_on_page ) {
				$this->l->reload_js( "content_pages.php" );
			}

			return $content;
		}
	}

	public function content_delete() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$this->db->upd( __FILE__ . ":" . __LINE__, "pages", array( "deleted" ), array( true ), "`id` = '$id'" );
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_DELETE_PAGE );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_PAGE_DELETED );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "Content_text deleted, id: " . $id );

		$this->sitemap->update_sitemap_automatic();

		return $content;
	}

	public function content_status() {
		$content = "";


		$id = $this->p->get( "id" );

		if ( is_numeric( $id ) && $id > 0 ) {
			$sql    = "SELECT `active` FROM `" . TP . "pages` WHERE `id` = '$id'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();
				if ( $row['active'] == 0 ) {
					$active = 1;
				} else {
					$active = 0;
				}

				$this->db->upd( __FILE__ . ":" . __LINE__, "pages", array( "active" ), array( $active ), "`id` = '$id'" );
			} else {
				$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_PAGE_STATUS );
				$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

				return $content;
			}
		} else {
			$content .= $this->l->alert_text( "danger", AL_UNKNOWN_ERROR_CANT_CHANGE_PAGE_STATUS );
			$this->log->error( "form_admin", __FILE__ . ":" . __LINE__, "ID value is empty or not numeric: " . $id );

			return $content;
		}

		$content .= $this->l->alert_text_dismiss( "success", AL_PAGE_STATUS_CHANGE );
		$this->log->event( "log", __FILE__ . ":" . __LINE__, "text status changed, id: " . $id );

		$this->sitemap->update_sitemap_automatic();

		return $content;
	}

}

if ( ! isset( $action ) ) {
	$action = null;
}

$class_content_pages = new class_content_pages( $action );
$content .= $class_content_pages->get_content();