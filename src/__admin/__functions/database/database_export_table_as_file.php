<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_ADMIN_EXPORT;

class class_statistics_export extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();


		$action = $this->p->get( "do", "init" );

		switch ( $action ) {
			case "output_csv":
				$this->content .= $this->output( "csv" );
				break;
			case "init":
			default:
				$this->content .= AL_ERROR_NO_ACTION;
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function output( $output_type ) {


		$table = $this->p->get( "table" );

		if ( ! isset( $table ) || empty( $table ) ) {
			return AL_NO_TABLE_NAME_GIVEN;
		}

		$table_escaped = $this->db->escape( $table );

		$sql    = "SELECT * FROM `" . TP . $table_escaped . "` ORDER BY `id`";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( empty( $result->num_rows ) ) {
			return AL_TABLE_EMPTY;
		}

		$got_header     = false;
		$table_contents = array();
		$table_headers  = array();
		while ( $row = $result->fetch_assoc() ) {
			$tmp_table_contents = array();

			if ( $got_header ) {
				foreach ( $row as $table_content ) {
					$tmp_table_contents[] = $table_content;
				}
				$table_contents[] = $tmp_table_contents;
			} else {
				$tmp_table_headers = array();
				foreach ( $row as $table_row => $table_content ) {
					$tmp_table_contents[] = $table_content;
					$tmp_table_headers[]  = $table_row;
				}
				$table_contents[] = $tmp_table_contents;
				$table_headers    = $tmp_table_headers;
				$got_header       = true;
			}
		}

		if ( $output_type == "csv" ) {
			$table_filename = $this->translate_special_characters( $table, true, true );

			// output headers so that the file is downloaded rather than displayed
			header( 'Content-Type: text/csv; charset=utf-8' );
			header( 'Content-Disposition: attachment; filename=' . $table_filename . '.csv' );

			// create a file pointer connected to the output stream
			$output = fopen( 'php://output', 'w' );

			// output the column headings
			fputcsv( $output, $table_headers );

			// loop over the rows, outputting them
			foreach ( $table_contents as $row ) {
				fputcsv( $output, $row );
			}
		} else {
			$content = AL_WRONG_OUTPUTTYPE . ": '" . $output_type . "''";

			return $content;
		}
	}

}

$class_statistics_export = new class_statistics_export();
$content .= $class_statistics_export->get_content();
