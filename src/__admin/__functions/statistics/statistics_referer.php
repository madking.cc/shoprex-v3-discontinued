<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_REFERER_LOG;

class class_statistics_referer extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		if ( ! isset( $action ) ) {
			$action = "init";
		}
		$action = $this->p->get( "do", $action );

		switch ( $action ) {
			case "exclude_domain_del":
				$this->content .= $this->exclude_domain_del();
				$this->content .= $this->start();
				break;
			case "exclude_domain_add":
				$this->content .= $this->exclude_domain_add();
				$this->content .= $this->start();
				break;
			case "init":
			default:
				$this->content .= $this->start();
				break;
		}
	}

	public function get_content() {
		return $this->content;
	}

	public function exclude_domain_add() {
		$excludedomain = trim( $this->p->get( "excludedomain" ) );

		if ( empty( $excludedomain ) ) {
			$_SESSION['exclude_domain_empty'] = 1;

			return false;
		}

		$result                           = $this->db->ins( __FILE__ . ":" . __LINE__, "statistics_referer_exclude", array(
			"domain",
			"created"
		), array( $excludedomain, "NOW()" ) );
		$_SESSION['exclude_domain_added'] = 1;

		return $result;

	}

	public function exclude_domain_del() {
		$del_domain = trim( $this->p->get( "del_domain" ) );

		$result                             = $this->db->upd( __FILE__ . ":" . __LINE__, "statistics_referer_exclude", array( "deleted" ), array( 1 ), "`domain` LIKE '$del_domain'" );
		$_SESSION['exclude_domain_deleted'] = 1;

		return $result;

	}

	public function start() {
		$content = "";

		$content .= $this->l->display_message_by_session( 'exclude_domain_added', AL_EXCLUDE_DOMAIN_ADDED );
		$content .= $this->l->display_message_by_session( 'exclude_domain_deleted', AL_EXCLUDE_DOMAIN_DELETED );
		$content .= $this->l->display_message_by_session( 'exclude_domain_empty', AL_EXCLUDE_DOMAIN_EMPTY, "danger" );


		$content .= "<h3 class='underline'>" . AL_DEFINE_DOMAIN_EXCLUSION . "</h3>";

		$content .= $this->l->form_admin() . $this->l->hidden( "do", "exclude_domain_add" ) . $this->l->table() . "
        <tr><td>" . AL_TBL_DOMAINNAME_TO_EXCLUDE . "</td><td>" . $this->l->text( "excludedomain" ) . "</td><td>" . $this->l->submit() . "</td></tr>
        </table></form>";

		$sql    = "SELECT * FROM `" . TP . "statistics_referer_exclude` WHERE `deleted` =  '0' ORDER BY `domain`";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			$content .= "<h4 class='underline'>" . AL_DEFINED_DOMAIN_EXCLUSION . "</h4>";
			$domain_exclusion_ar = array();
			while ( $row = $result->fetch_assoc() ) {
				$domain_exclusion_ar[] = $row['domain'];
			}
			$content .= $this->l->table();

			$i = 0;
			foreach ( $domain_exclusion_ar as $domain ) {
				if ( $i == 0 ) {
					$content .= "<tr>";
				}

				$content .= "<td>" . $this->l->text( $this->p->unique_id( false ), $domain, "250", "readonly=readonly" ) . " " . $this->l->form_admin() . $this->l->hidden( "do", "exclude_domain_del" ) . $this->l->hidden( "del_domain", $domain ) . $this->l->submit( AL_DELETE ) . "</form></td>";

				$i ++;
				if ( $i == 3 ) {
					$content .= "</tr>\n";
					$i = 0;
				}
			}
			if ( $i > 0 && $i < 3 ) {
				for ( ; $i < 3; $i ++ ) {
					$content .= "<td></td>";
				}
				$content .= "</tr>\n";
			}
			$content .= "</table>\n";
		}


		$content .= "<h3 class='underline'>" . AL_EXTERNAL_LINKS_TO_PAGE_COLON . "</h3>";


		$sql    = "SELECT * FROM `" . TP . "statistics_referer` WHERE `type` =  '1' ";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {

			$ref = array();
			$sum = 0;

			while ( $row = $result->fetch_assoc() ) {
				if ( empty( $row['referer'] ) ) {
					continue;
				}

				$found = false;
				foreach ( $domain_exclusion_ar as $exclude_domain ) {
					if ( strpos( $row['referer'], "//" . $exclude_domain ) !== false ) {
						$found = true;
						break;
					}
					if ( strpos( $row['referer'], "//www." . $exclude_domain ) !== false ) {
						$found = true;
						continue;
					}
				}
				if ( $found ) {
					continue;
				}

				$_SESSION['referer_check'] = "asgkj5345kg342lglpo";

				$text_link = wordwrap( urldecode( $row['referer'] ), 100, "<br>", 1 );

				$date = $row['changed'];
				if ( $date == "0000-00-00 00:00:00" ) {
					$date = $row['created'];
				}
				if ( $date == "0000-00-00 00:00:00" ) {
					$date = STATISTICS_START_DATE;
				} else {
					$date = $this->dt->sqldatetime( $date );
				}
				$timestamp = strtotime( $date );

				if ( ! isset( $ref[ $text_link ] ) ) {
					$link_wo_sid = $this->loc->remove_sid( $row['referer'] );

					$ref[ $text_link ] = array(
						"referer" => $row['referer'],
						"link"    => $this->l->link( $text_link, "referer.php", "link=" . $link_wo_sid, "target=\"_blank\"" ),
						"used"    => $row['count'],
						"changed" => array( $timestamp, "time", $date )
					);
				} else {
					$ref[ $text_link ]["used"] += $row['count'];
					if ( $ref[ $text_link ]["changed"][0] < $timestamp ) {
						$ref[ $text_link ]["changed"] = array( $timestamp, "time", $date );
					}
				}
				$sum += $row['count'];
			}

			$handler = $this->tbl->tbl_init( AL_TBL_LINK, AL_TBL_USED, AL_TBL_QUOTA, AL_TBL_LAST );

			foreach ( $ref as $array ) {

				$percent = round( $array['used'] / ( $sum / 100 ), 1 );

				$this->tbl->tbl_load( $handler, $array['link'], $array['used'], array(
					$percent,
					"%"
				), $array['changed'] );

			}
			$content .= $this->tbl->tbl_out( $handler, 1 );
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
		}


		return $content;
	}
}

$class_statistics_referer = new class_statistics_referer();
$content .= $class_statistics_referer->get_content();


