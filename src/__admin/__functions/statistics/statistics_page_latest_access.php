<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_PAGE_LOG;

class class_page_latest_access extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();
		$this->content = "";
		$this->content .= $this->start();

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";
		$content .= "<h3 class='underline'>" . AL_CURRENT_PAGE_VIEWS_COLON . "</h3>";

		$sql = "SELECT * FROM `" . TP . "statistics_view_log` WHERE `type` =  '1' ORDER BY `created` DESC LIMIT 0, 250";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {

			$handler = $this->tbl->tbl_init( AL_TBL_PAGE, AL_TBL_COUNTRY, "<nobr>" . AL_TBL_SYSTEM_ID . "</nobr>", AL_TBL_ACCESS );

			while ( $row = $result->fetch_assoc() ) {
				$date = $row['created'];
				if ( $date == "0000-00-00 00:00:00" ) {
					$date = STATISTICS_START_DATE;
				} else {
					$date = $this->dt->sqldatetime( $date );
				}
				$timestamp = strtotime( $date );

				$page = str_replace( "&", "&#38;", $row['page'] );

				if ( is_null( $row['country'] ) ) {
					$country = DISPLAY_EMPTY_VALUE;
				} else {
					$country = $this->country->get_country_name( $row['country'] );
				}

				$this->tbl->tbl_load( $handler, $page, $country, $row['by_system_id'], array(
					$timestamp,
					"time",
					$date
				) );
			}
			$content .= $this->tbl->tbl_out( $handler, 3 );
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
		}

		return $content;
	}
}

$class_page_latest_access = new class_page_latest_access();
$content .= $class_page_latest_access->get_content();

