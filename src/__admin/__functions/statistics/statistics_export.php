<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_ADMIN_EXPORT;

class class_statistics_export extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();

		$action = $this->p->get( "do", "init" );

		switch ( $action ) {
			case "init":
			default:
				$this->content .= $this->start();
				break;
		}

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";


		//$tables = array("statistics_by_date" => AL_STATISTICS_BY_DATE, "statistics_by_resolution" => AL_STATISTICS_BY_RESOLUTION, "statistics_by_resolution_avail" => AL_STATISTICS_BY_RESOLUTION_AVAILABLE, "statistics_by_system" => AL_STATISTICS_BY_SYSTEM, "statistics_by_windowsize" => AL_STATISTICS_BY_WINDOWSIZE, "statistics_count" => AL_STATISTICS_BY_COUNT, "statistics_download" => AL_STATISTICS_BY_DOWNLOAD, "file_download" => AL_DOWNLOAD_TABLE, "statistics_not_compatible" => AL_STATISTICS_BY_NOT_COMPATIBLE_BROWSER, "statistics_not_found" => AL_STATISTICS_BY_NOT_FOUND, "statistics_page" => AL_STATISTICS_BY_PAGE, "statistics_referer" => AL_STATISTICS_BY_REFERER, "statistics_view_log" => AL_STATISTICS_BY_VIEW_LOG);
		$tables = array( "statistics_by_date"   => AL_STATISTICS_BY_DATE,
		                 "statistics_by_system" => AL_STATISTICS_BY_SYSTEM,
		                 "statistics_count"     => AL_STATISTICS_BY_COUNT,
		                 "statistics_download"  => AL_STATISTICS_BY_DOWNLOAD,
		                 "file_download"        => AL_DOWNLOAD_TABLE,
		                 "statistics_not_found" => AL_STATISTICS_BY_NOT_FOUND,
		                 "statistics_page"      => AL_STATISTICS_BY_PAGE,
		                 "statistics_referer"   => AL_STATISTICS_BY_REFERER,
		                 "statistics_view_log"  => AL_STATISTICS_BY_VIEW_LOG
		);

		asort( $tables );

		$content .= "<h3 class='underline'>" . AL_DATABASE_STATISTICS_EXPORT . "</h3>\n";

		$content .=

			$this->l->table() . "<tr><th>" . AL_TBL_TABLE . "</th><th>" . AL_TBL_ACTION . "</th></tr>";

		foreach ( $tables as $table => $title ) {
			$content .= "<tr><td>" . $title . "</td><td>";

			$table_escaped = $this->db->escape( $table );
			$sql           = "SELECT * FROM `" . TP . $table_escaped . "` ORDER BY `id`";
			$result        = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( empty( $result->num_rows ) ) {
				$content .= $this->l->link_button( AL_NO_DATA, "#" ) . "</td></tr>\n";
			} else {
				$content .= $this->l->form_admin( "", "database_export_table_as_file.php" ) . $this->l->hidden( "do", "output_csv" ) . $this->l->hidden( "table", $table ) . $this->l->submit( AL_EXPORT_CSV_FILE ) . "</form></td></tr>\n";
			}
		}
		$content .= "</table>\n";

		return $content;
	}

}

$class_statistics_export = new class_statistics_export();
$content .= $class_statistics_export->get_content();


