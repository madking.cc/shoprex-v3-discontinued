<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_NOT_FOUND_LOG;

class class_statistics_not_found extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();
		$this->content = "";
		$this->content .= $this->start();

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {

		$content = "";

		$source = $this->p->get( "source", "user" );

		switch ( $source ) {
			case "bot":
				$type = 2;
				break;
			default:
				$type = 1;
				break;
		}

		$content .= "<h3 class='underline'>" . AL_PAGES_NOT_FOUND_COLON . "</h3>\n";

		$content .= $this->l->form_admin() . $this->l->table( "", "" ) . "
        <tr>
     <td>" . $this->l->select( "source", "", "", true ) . "
        <option value='user'";
		if ( $source == "user" ) {
			$content .= " selected=selected";
		}
		$content .= ">" . AL_USER_HITS . "</option>
        <option value='bot'";
		if ( $source == "bot" ) {
			$content .= " selected=selected";
		}
		$content .= ">" . AL_BOT_HITS . "</option>
     </select>
     </td>
    </tr>
    </table>
    </form>
    <br />
    ";

		$sql    = "SELECT * FROM `" . TP . "statistics_not_found` WHERE `type`='$type' ORDER BY `changed` DESC";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {
			$sql     = "SELECT SUM(`count`) AS `sum_count` FROM `" . TP . "statistics_not_found` WHERE `type`='$type'";
			$result2 = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			$row2    = $result2->fetch_assoc();
			$sum     = $row2['sum_count'];


			$handler = $this->tbl->tbl_init( AL_TBL_LINK, AL_TBL_REFERER, AL_TBL_VIEWS, AL_TBL_QUOTA, AL_TBL_LAST );


			$i = 1;
			while ( $row = $result->fetch_assoc() ) {
				$link = wordwrap( $row['link'], 100, "<br>", 1 );
				if ( isset( $row['referer'] ) ) {
					$referer_wo_sid = $this->loc->remove_sid( $row['referer'] );
					$referer_text   = wordwrap( $referer_wo_sid, 100, "<br>", 1 );
					$referer        = $this->l->link( $referer_text, "referer.php", "link=" . $referer_wo_sid, "target=\"_blank\"" );
				} else {
					$referer = "";
				}
				$changed = $this->dt->sqldatetime( $row['changed'] );

				$date = $row['changed'];
				if ( $date == "0000-00-00 00:00:00" ) {
					$date = $row['created'];
				}
				if ( $date == "0000-00-00 00:00:00" ) {
					$date = STATISTICS_START_DATE;
				} else {
					$date = $this->dt->sqldatetime( $date );
				}
				$timestamp = strtotime( $date );

				$percent = round( $row['count'] / ( $sum / 100 ), 1 );

				$this->tbl->tbl_load( $handler, $this->l->link( $link, $row['link'], "", "target=\"_blank\"" ), $referer, $row['count'], array(
					$percent,
					"%"
				), array( $timestamp, "time", $date ) );

			}
			$content .= $this->tbl->tbl_out( $handler, 4 );
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
		}


		return $content;
	}
}

$class_statistics_not_found = new class_statistics_not_found();
$content .= $class_statistics_not_found->get_content();