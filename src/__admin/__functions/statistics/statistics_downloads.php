<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_DOWNLOAD_LOG;


class class_downloads extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();
		$this->content = "";
		$this->content .= $this->start();

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {

		$content = "";

		$content .= $this->l->link_button( AL_NOT_SPLIT_STATISTICS_BY_COUNTRY, "content_downloads.php" );

		$content .= "<h3 class='underline'>" . AL_DOWNLOAD_STATISTICS_COLON . "</h3>";

		$sql = "SELECT *, `" . TP . "file_download`.`id` as `download_id`, `" . TP . "statistics_download`.`id` as `statistics_id`,
            `" . TP . "statistics_download`.`count` as `statistics_count`, `" . TP . "statistics_download`.`changed` as `statistics_changed`,
            `" . TP . "file_download`.`count` as `download_count`, `" . TP . "file_download`.`changed` as `download_changed`
            FROM `" . TP . "statistics_download`
            LEFT JOIN `" . TP . "file_download` ON (`" . TP . "file_download`.`id` = `" . TP . "statistics_download`.`to_id`)
            WHERE `" . TP . "statistics_download`.`type` =  '1'
            ORDER BY `" . TP . "statistics_download`.`count` DESC, `to_id`";

		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {

			$handler = $this->tbl->tbl_init( AL_TBL_FILENAME, AL_TBL_DOWNLOAD, AL_TBL_QUOTA, AL_TBL_COUNTRY, AL_TBL_IP, AL_TBL_LAST, AL_TBL_ACTIVE, AL_TBL_DELETED );

			$sql2    = "SELECT SUM(`count`) AS `sum_count`
            FROM `" . TP . "statistics_download`";
			$result2 = $this->db->query( $sql2, __FILE__ . ":" . __LINE__ );
			$row2    = $result2->fetch_assoc();
			$sum     = $row2['sum_count'];

			while ( $row = $result->fetch_assoc() ) {

				$date      = $row['statistics_changed'];
				$timestamp = strtotime( $date );
				$date      = $this->dt->sqldatetime( $date );
				$percent   = round( $row['statistics_count'] / ( $sum / 100 ), 1 );

				$active  = $this->lang->answer( $row['active'] );
				$deleted = $this->lang->answer( $row['deleted'] );

				if ( is_null( $row['country'] ) ) {
					$country = DISPLAY_EMPTY_VALUE;
				} else {
					$country = $this->country->get_country_name( $row['country'] );
				}

				$this->tbl->tbl_load( $handler, $row['filename'], $row['statistics_count'], array(
					$percent,
					"%"
				), $country, $row['ip'], array( $timestamp, "time", $date ), $active, $deleted );
			}
			$content .= $this->tbl->tbl_out( $handler, 5 );
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
		}

		return $content;
	}
}

$class_downloads = new class_downloads();
$content .= $class_downloads->get_content();

