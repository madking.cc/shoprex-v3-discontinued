<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$GLOBALS['admin_subtitle'] = AL_PAGE_COUNTER;

class class_page_counter extends class_sys {
	public $content;

	public function __construct() {
		parent::__construct();
		$this->content = "";
		$this->content .= $this->start();

	}

	public function get_content() {
		return $this->content;
	}

	public function start() {
		$content = "";
		$content .= $this->views_visitors();
		$content .= $this->monthly_statistics();
		$content .= $this->devices();
		$content .= $this->hits_visits_by_language();

		return $content;
	}

	public function views_visitors() {
		/* Seitenaufrufe und Besucher */

		$content = "";

		$sql    = "SELECT sum(`count`) AS `summe` FROM `" . TP . "statistics_count` WHERE `type` =  '1'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		$row    = $result->fetch_assoc();

		if ( ! isset( $row['summe'] ) ) {
			$row['summe'] = 0;
		}

		$time     = strtotime( STATISTICS_START_DATE );
		$time     = $time - 24 * 60 * 60;
		$time_now = strtotime( date( "d.m.Y" ) );
		$time     = $time_now - $time;

		$time = $time / 60 / 60 / 24;

		$day_hits = ceil( $row['summe'] / $time );

		$visits_sum = $row['summe'];

		$sql     = "SELECT `hits` FROM `" . TP . "statistics_hits` WHERE `type` =  '1'";
		$result2 = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		$row2    = $result2->fetch_assoc();

		$time2     = strtotime( STATISTICS_START_DATE );
		$time2     = $time2 - 24 * 60 * 60;
		$time_now2 = strtotime( date( "d.m.Y" ) );
		$time2     = $time_now2 - $time2;

		$time2 = $time2 / 60 / 60 / 24;

		$day_hits2 = ceil( $row2['hits'] / $time2 );
		$hits_sum  = $row2['hits'];


		$content .= "<H3 class='underline'>" . AL_PAGE_COUNTER_COLON . "</H3>
" . $this->l->table() . "<tr><td>
<p>" . $visits_sum . " " . AL_UNIQUE_REQUESTS_SINCE . " " . STATISTICS_START_DATE . "<br />
" . AL_THIS_CORRESPONDS_TO_AN_AVERAGE_OF_X_DAILY_PART01 . " $day_hits " . AL_THIS_CORRESPONDS_TO_AN_AVERAGE_OF_X_DAILY_PART02 . "
</p>
<p>
" . $hits_sum . " " . AL_OVERALL_PAGE_VIEWS_SINCE . " " . STATISTICS_START_DATE . "<br />
" . AL_THIS_CORRESPONDS_TO_AN_AVERAGE_OF_X_DAILY_PART01 . " $day_hits2 " . AL_THIS_CORRESPONDS_TO_AN_AVERAGE_OF_X_DAILY_PART02 . "
</p>";
		$content .= "</td>";

		$content .= "</tr>
</table>
";

		return $content;
	}

	public function monthly_statistics() {

		/* Monatliche Statistik */

		$content = "";


		$content .= "<H3 class='underline'>" . AL_MONTHLY_STATS_COLON . "</H3>";

		$show_time = $this->p->get( "show_time" );

		$sql    = "SELECT * FROM `" . TP . "statistics_by_date` WHERE `country` IS NULL AND `type` =  '1' ORDER BY `id` DESC";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		if ( $result->num_rows > 0 ) {

			$show_by_time = array();

			$page_create_date_timestamp = strtotime( STATISTICS_START_DATE );
			$page_create_date_getdate   = getdate( $page_create_date_timestamp );
			$page_create_date_day       = $page_create_date_getdate["mday"];
			$page_create_date_month     = $page_create_date_getdate["mon"];
			$page_create_date_year      = $page_create_date_getdate["year"];

			$this_date_timestamp = time();
			$this_date_getdate   = getdate( $this_date_timestamp );
			$this_date_day       = $this_date_getdate["mday"];
			$this_date_month     = $this_date_getdate["mon"];
			$this_date_year      = $this_date_getdate["year"];

			/*
				while($row = $result->fetch_assoc())
				{
					$year = substr($row['date'], 0, 4);
					$month = substr($row['date'], 5, 2);
					$show_by_time[$month.$year] = substr($row['date'],0,7);
				}
			*/

			for ( $i = $page_create_date_year; $i <= $this_date_year; $i ++ ) {
				if ( $i == $this_date_year || $page_create_date_year == $this_date_year ) {
					$last_month = $this_date_month;
				} else {
					$last_month = 12;
				}
				if ( $i == $page_create_date_year ) {
					$first_month = $page_create_date_month;
				} else {
					$first_month = 1;
				}

				for ( $j = $first_month; $j <= $last_month; $j ++ ) {
					$month = str_pad( $j, 2, '0', STR_PAD_LEFT );
					$year  = $i;

					$show_by_time[ $month . "." . $year ] = $year . "-" . $month;
				}
			}

			$show_by_time = array_reverse( $show_by_time );

			$content .= $this->l->img( ADMINDIR . "layout/img/blue.jpg", "", "height='5' width='5'" ) . " = " . AL_VIEWS . "
    " . $this->l->img( ADMINDIR . "layout/img/green.jpg", "", "height='5' width='5'" ) . " = " . AL_VISITORS . "
    <br />
    <br />
    " . $this->l->form_admin() . $this->l->select( "show_time", "onchange='this.form.submit()'" );

			foreach ( $show_by_time AS $key => $value ) {
				$content .= "<option value='$value'";
				if ( $show_time == $value ) {
					$content .= " selected='selected'";
				}
				$content .= ">$key</option>\n";
			}
			$content .= "</select></form>";
			if ( ! empty( $show_time ) ) {
				$select = $show_time;
			} else {
				foreach ( $show_by_time AS $key => $value ) {
					$select = $value;
					break;
				}
			}

			$content .= "<br /><br />";

			$values = array();

			$select_date_timestamp = strtotime( $select );
			$select_date_getdate   = getdate( $select_date_timestamp );
			$select_date_month     = $select_date_getdate["mon"];
			$select_date_year      = $select_date_getdate["year"];

			//var_dump($page_create_date_month, $select_date_month);
			if ( $page_create_date_year != $select_date_year || $page_create_date_month != $select_date_month ) {
				$first_day = 1;
				//var_dump($first_day);
				if ( $select_date_year < $this_date_year || $select_date_month < $this_date_month ) {
					$last_day = $this->dt->get_last_day( $select_date_month, $select_date_year );
					//var_dump($last_day);
				} else {
					$last_day = $this_date_day;
				}
			} else {
				$first_day = $page_create_date_day;
				if ( $page_create_date_year == $this_date_year && $page_create_date_month == $this_date_month ) {
					$last_day = $this_date_day;
				} else {
					$last_day = $this->dt->get_last_day( $page_create_date_month, $page_create_date_year );
				}
				//var_dump($first_day, $last_day);
			}

			for ( $i = $first_day; $i <= $last_day; $i ++ ) {
				$day                      = str_pad( $i, 2, '0', STR_PAD_LEFT );
				$values[ $day ]['visits'] = 0;
				$values[ $day ]['hits']   = 0;
			}

			$sql    = "SELECT * FROM `" . TP . "statistics_by_date` WHERE `date` LIKE '" . $select . "%' AND `country` IS NULL AND `type` =  '1' ORDER BY `id` ASC";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

			$sum_hits       = 0;
			$sum_visits     = 0;
			$highest_hits   = 0;
			$highest_visits = 0;

			while ( $row = $result->fetch_assoc() ) {
				$day                    = substr( $row['date'], 8, 2 );
				$values[ $day ]['hits'] = $row['hits'];
				$sum_hits += $row['hits'];
				if ( $row['hits'] > $highest_hits ) {
					$highest_hits = $row['hits'];
				}
				$values[ $day ]['visits'] = $row['visits'];
				$sum_visits += $row['visits'];
				if ( $row['visits'] > $highest_visits ) {
					$highest_visits = $row['visits'];
				}
			}
			$count = sizeof( $values );
			//var_dump($values);
			/*
				if($count > 2)
				{
					$first_element = array_slice($values, 0, 1, true);
					$last_key = date("d");

					foreach($first_element AS $key => $value)
					{
						$first_key = $key;
					}
					$tmp = array();
					for($i=$first_key; $i <= $last_key; $i++)
					{
						if(isset($values[$i]))
						{
							$tmp[$i]['hits'] = $values[$i]['hits'];
							$tmp[$i]['visits'] = $values[$i]['visits'];
						}
						else
						{
							$tmp[$i]['hits'] = 0;
							$tmp[$i]['visits'] = 0;
						}
					}
					$values = $tmp;

				}


			*/
			$content .= $this->l->table( "", "month_table" ) . "<tr>\n";

			if ( $highest_hits >= $highest_visits ) {
				$highest_value = $highest_hits;
			} else {
				$highest_value = $highest_visits;
			}

			foreach ( $values AS $key => $value ) {
				if ( $highest_hits > 0 ) {
					$percent_hit = ceil( $value['hits'] * 100 / $highest_hits );
				} else {
					$percent_hit = 0;
				}
				if ( $highest_visits > 0 ) {
					$percent_visit = ceil( $value['visits'] * 100 / $highest_visits );
				} else {
					$percent_visit = 0;
				}
				$content .= "<td align='right' valign='bottom'>" . $this->l->img( ADMINDIR . "layout/img/blue.jpg", "", "height='" . ( $percent_hit * 1.5 ) . "' width='4'" ) . "</td><td valign='bottom' class=''>" . $this->l->img( ADMINDIR . "layout/img/green.jpg", "", "height='" . ( $percent_visit * 1.5 ) . "' width='4'", "img diag-green" ) . "</td><td> </td>\n";
			}

			$content .= "</tr>";

			$content .= "<tr id='values'>\n";

			foreach ( $values AS $key => $value ) {
				$content .= "<td style='border-right: 1px solid #666'>" . $value['hits'] . "</td><td>" . $value['visits'] . "</td><td> </td>\n";
			}

			$content .= "</tr>";

			$content .= "<tr>\n";

			foreach ( $values AS $key => $value ) {
				$timestamp = strtotime( $select . "-" . $key );
				$day_text  = getdate( $timestamp );
				$day_text  = $day_text["wday"];

				switch ( $day_text ) {
					case "0":
						$day_text = AL_SUNDAY_SHORT;
						break;
					case "1":
						$day_text = "<span style=''>" . AL_MONDAY_SHORT . "</span>";
						break;
					case "2":
						$day_text = "<span style=''>" . AL_TUESDAY_SHORT . "</span>";
						break;
					case "3":
						$day_text = "<span style=''>" . AL_WEDNESDAY_SHORT . "</span>";
						break;
					case "4":
						$day_text = "<span style=''>" . AL_THURSDAY_SHORT . "</span>";
						break;
					case "5":
						$day_text = "<span style=''>" . AL_FRIDAY_SHORT . "</span>";
						break;
					case "6":
						$day_text = AL_SATURDAY_SHORT;
						break;
				}

				$content .= "<td colspan='2' align='center'>" . $key . " " . $day_text . "</td><td> </td>\n";
			}

			$content .= "</tr>";

			$content .= "</table>";
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
		}

		return $content;
	}


	public function devices() {
		/* Geräte */

		$content = "";

		$sql    = "SELECT SUM(`visitors`) AS `sum_tablet` FROM `" . TP . "statistics_by_system` WHERE `tablet` = '1' AND `type` =  '1'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		$row = $result->fetch_assoc();
		if ( ! empty( $row['sum_tablet'] ) ) {
			$sum_tablet = $row['sum_tablet'];

			$sql         = "SELECT `changed` FROM `" . TP . "statistics_by_system` WHERE `tablet` = '1' AND `type` =  '1' ORDER BY `changed` DESC LIMIT 0,1";
			$result      = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			$row         = $result->fetch_assoc();
			$date_tablet = $row['changed'];
			if ( $date_tablet == "0000-00-00 00:00:00" ) {
				$sql         = "SELECT `created` FROM `" . TP . "statistics_by_system` WHERE `tablet` = '1' AND `type` =  '1' ORDER BY `created` DESC LIMIT 0,1";
				$result      = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				$row         = $result->fetch_assoc();
				$date_tablet = $row['created'];
			}
			if ( $date_tablet == "0000-00-00 00:00:00" ) {
				$date_tablet = STATISTICS_START_DATE;
			} else {
				$date_tablet = $this->dt->sqldatetime( $date_tablet );
			}
		} else {
			$sum_tablet  = 0;
			$date_tablet = "";
		}

		$sql    = "SELECT SUM(`visitors`) AS `sum_phone` FROM `" . TP . "statistics_by_system` WHERE `phone` = '1' AND `type` =  '1'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );

		$row = $result->fetch_assoc();
		if ( ! empty( $row['sum_phone'] ) ) {
			$sum_phone = $row['sum_phone'];

			$sql        = "SELECT `changed` FROM `" . TP . "statistics_by_system` WHERE `phone` = '1' AND `type` =  '1' ORDER BY `changed` DESC LIMIT 0,1";
			$result     = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			$row        = $result->fetch_assoc();
			$date_phone = $row['changed'];
			if ( $date_phone == "0000-00-00 00:00:00" ) {
				$sql        = "SELECT `created` FROM `" . TP . "statistics_by_system` WHERE `phone` = '1' AND `type` =  '1' ORDER BY `created` DESC LIMIT 0,1";
				$result     = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				$row        = $result->fetch_assoc();
				$date_phone = $row['created'];
			}
			if ( $date_phone == "0000-00-00 00:00:00" ) {
				$date_phone = STATISTICS_START_DATE;
			} else {
				$date_phone = $this->dt->sqldatetime( $date_phone );
			}
		} else {
			$sum_phone  = 0;
			$date_phone = "";
		}

		$sql    = "SELECT SUM(`visitors`) AS `sum_pc` FROM `" . TP . "statistics_by_system` WHERE `tablet` = '0' AND `phone` = '0' AND `type` =  '1'";
		$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
		$row    = $result->fetch_assoc();
		if ( ! empty( $row['sum_pc'] ) ) {
			$sum_pc = $row['sum_pc'];

			$sql     = "SELECT `changed` FROM `" . TP . "statistics_by_system` WHERE `tablet` = '0' AND `phone` = '0' AND `type` =  '1' ORDER BY `changed` DESC LIMIT 0,1";
			$result  = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			$row     = $result->fetch_assoc();
			$date_pc = $row['changed'];
			if ( $date_pc == "0000-00-00 00:00:00" ) {
				$sql     = "SELECT `created` FROM `" . TP . "statistics_by_system` WHERE `tablet` = '0' AND `phone` = '0' AND `type` =  '1' ORDER BY `created` DESC LIMIT 0,1";
				$result  = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
				$row     = $result->fetch_assoc();
				$date_pc = $row['created'];
			}
			if ( $date_pc == "0000-00-00 00:00:00" ) {
				$date_pc = STATISTICS_START_DATE;
			} else {
				$date_pc = $this->dt->sqldatetime( $date_pc );
			}
		} else {
			$sum_pc  = 0;
			$date_pc = "";
		}

		$sum_all = $sum_pc + $sum_tablet + $sum_phone;

		$content .= "

    <H3 class='underline'>" . AL_DEVICES_USED_COLON . "</H3>";

		if ( $sum_all > 0 ) {
			$sum_pc_percent     = round( $sum_pc / ( $sum_all / 100 ), 1 );
			$sum_phone_percent  = round( $sum_phone / ( $sum_all / 100 ), 1 );
			$sum_tablet_percent = round( $sum_tablet / ( $sum_all / 100 ), 1 );

			$tbl_class = new class_table();
			$handler   = $tbl_class->tbl_init( AL_TBL_TYPE, AL_TBL_VIEWS, AL_TBL_QUOTA, AL_TBL_LAST );
			$timestamp = strtotime( $date_pc );
			$tbl_class->tbl_load( $handler, AL_PC, $sum_pc, array( $sum_pc_percent, "%" ), array(
				$timestamp,
				"time",
				$date_pc
			) );
			$timestamp = strtotime( $date_phone );
			$tbl_class->tbl_load( $handler, AL_SMARTPHONE, $sum_phone, array(
				$sum_phone_percent,
				"%"
			), array( $timestamp, "time", $date_phone ) );
			$timestamp = strtotime( $date_tablet );
			$tbl_class->tbl_load( $handler, AL_TABLET, $sum_tablet, array(
				$sum_tablet_percent,
				"%"
			), array( $timestamp, "time", $date_tablet ) );
			$content .= $tbl_class->tbl_out( $handler, 1 );
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
		}

		return $content;

	}


	public function hits_visits_by_language() {

// Hits and visits by language

		$content = "";

		$country_sum            = array();
		$country_sum_all_visits = 0;
		$country_sum_all_hits   = 0;
		$sqlc                   = "SELECT DISTINCT `country`, sum(`count`) AS `summe`, sum(`hits`) AS `summe_hits` FROM `" . TP . "statistics_count` WHERE `type` =  '1'  GROUP BY `country` ORDER BY `summe_hits` DESC";
		$resultc                = $this->db->query( $sqlc, __FILE__ . ":" . __LINE__ );
		while ( $rowc = $resultc->fetch_assoc() ) {
			if ( is_null( $rowc['country'] ) ) {
				$rowc['country'] = 0;
			}
			$country_sum[ $rowc['country'] ] = array( $rowc['summe'], $rowc['summe_hits'] );
			$country_sum_all_visits += $rowc['summe'];
			$country_sum_all_hits += $rowc['summe_hits'];
		}

		$country_languages_found     = false;
		$countries_by_language_ar    = array();
		$country_language_sum_hits   = 0;
		$country_language_sum_visits = 0;
		if ( true ) {
			$sql    = "SELECT `created` FROM `" . TP . "statistics_by_language`  WHERE `type` =  '1' ORDER BY `created` LIMIT 0,1";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows == 1 ) {
				$row                         = $result->fetch_assoc();
				$countries_by_language_start = $this->dt->sqldate( $row['created'] );
			} else {
				$countries_by_language_start = $this->dt->sqldate( date( "Y-m-d", time() ) );
			}

			$sql    = "SELECT DISTINCT `country` FROM `" . TP . "statistics_by_language` WHERE `type` =  '1'";
			$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );
			if ( $result->num_rows > 0 ) {
				$countries_by_language_tmp_ar = array();
				$countries_by_language_ar     = array();
				while ( $row = $result->fetch_assoc() ) {
					if ( is_null( $row['country'] ) ) {
						continue;
					}
					$country_languages_found        = true;
					$countries_by_language_tmp_ar[] = $row['country'];
				}


				foreach ( $countries_by_language_tmp_ar as $country ) {
					$sql    = "SELECT * FROM `" . TP . "statistics_by_language` WHERE `country` LIKE BINARY '$country' AND `type` =  '1'";
					$result = $this->db->query( $sql, __FILE__ . ":" . __LINE__ );


					while ( $row = $result->fetch_assoc() ) {
						if ( is_null( $row['lang'] ) ) {
							$lang = "0";
						} else {
							$lang = $row['lang'];
						}
						if ( ! isset( $countries_by_language_ar[ AL_ALL ][ $lang ]['hits'] ) ) {
							$countries_by_language_ar[ AL_ALL ][ $lang ]['hits'] = 0;
						}
						if ( ! isset( $countries_by_language_ar[ AL_ALL ][ $lang ]['visits'] ) ) {
							$countries_by_language_ar[ AL_ALL ][ $lang ]['visits'] = 0;
						}

						if ( ! empty( $row['hits'] ) ) {
							if ( ! isset( $countries_by_language_ar[ $country ][ $lang ]['hits'] ) ) {
								$countries_by_language_ar[ $country ][ $lang ]['hits'] = 0;
							}
							//$countries_by_language_ar[ $country ][ $lang ]['hits'] += $row['hits'];
							$countries_by_language_ar[ AL_ALL ][ $lang ]['hits'] += $row['hits'];
							$country_language_sum_hits += $row['hits'];
						}
						if ( ! empty( $row['visits'] ) ) {
							if ( ! isset( $countries_by_language_ar[ $country ][ $lang ]['visits'] ) ) {
								$countries_by_language_ar[ $country ][ $lang ]['visits'] = 0;
							}
							//$countries_by_language_ar[ $country ][ $lang ]['visits'] += $row['visits'];
							$countries_by_language_ar[ AL_ALL ][ $lang ]['visits'] += $row['visits'];
							$country_language_sum_visits += $row['visits'];
						}
					}
				}
			}
		}

		$content .= "<H3 class='underline'>" . AL_COUNTRIES_COLON . "</H3>";

		if ( sizeof( $country_sum ) > 0 ) {


			$handler = $this->tbl->tbl_init( AL_TBL_COUNTRY, AL_TBL_VIEWS, AL_TBL_QUOTA, AL_TBL_VSITORS, AL_TBL_QUOTA );
			foreach ( $country_sum as $country => $sumc ) {
				if ( empty( $country ) ) {
					$country = AL_UNKNOWN;
				} else {
					$country = $this->country->get_country_name( $country );
				}

				$percent_visits = number_format( ( $sumc[0] * 100 / $country_sum_all_visits ), 1 );
				$percent_hits   = number_format( ( $sumc[1] * 100 / $country_sum_all_hits ), 1 );
				if ( $percent_hits < 1 && $percent_visits < 1 ) {
					continue;
				}
				$this->tbl->tbl_load( $handler, $country, $sumc[1], array(
					$percent_hits,
					"%"
				), $sumc[0], array( $percent_visits, "%" ) );
			}
			$content .= $this->tbl->tbl_out( $handler, 1 );
		} else {
			$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
		}

		if ( $this->lang->count_of_languages > 1 ) {

			$content .= "<H3 class='underline'>" . AL_VISITORS_BY_PAGE_LANGUAGE_SINCE . " $countries_by_language_start:</H3>";

			if ( $country_languages_found ) {


				//AL_TBL_COUNTRY
				$handler                            = $this->tbl->tbl_init( AL_TBL_LANGUAGE_OF_PAGE, AL_TBL_VSITORS, AL_TBL_QUOTA );
				$country_page_language_visits_found = false;
				foreach ( $countries_by_language_ar as $country => $country_array ) {
					if ( empty( $country ) ) {
						$country = AL_UNKNOWN;
					} else {
						$country = $this->country->get_country_name( $country );
					}

					foreach ( $country_array as $language => $type_array ) {
						if ( empty( $type_array['visits'] ) ) {
							continue;
						}
						$language       = $this->p->get_language_text( $language );
						$percent_visits = number_format( ( $type_array['visits'] * 100 / $country_language_sum_visits ), 1 );

						$this->tbl->tbl_load( $handler, $language, $type_array['visits'], array(
							$percent_visits,
							"%"
						) );
						$country_page_language_visits_found = true;
					}
				}
				if ( $country_page_language_visits_found ) {
					$content .= $this->tbl->tbl_out( $handler, 1 );
				} else {
					$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
				}
			} else {
				$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
			}

			$content .= "<H3 class='underline'>" . AL_HITS_BY_PAGE_LANGUAGE_SINCE . " $countries_by_language_start:</H3>";

			if ( $country_languages_found ) {

				//AL_TBL_COUNTRY
				$handler                          = $this->tbl->tbl_init( AL_TBL_LANGUAGE_OF_PAGE, AL_TBL_VIEWS, AL_TBL_QUOTA );
				$country_page_language_hits_found = false;
				foreach ( $countries_by_language_ar as $country => $country_array ) {
					if ( empty( $country ) ) {
						$country = AL_UNKNOWN;
					} else {
						$country = $this->country->get_country_name( $country );
					}

					foreach ( $country_array as $language => $type_array ) {
						if ( empty( $type_array['hits'] ) ) {
							continue;
						}
						$language     = $this->p->get_language_text( $language );
						$percent_hits = number_format( ( $type_array['hits'] * 100 / $country_language_sum_hits ), 1 );
						$this->tbl->tbl_load( $handler, $language, $type_array['hits'], array( $percent_hits, "%" ) );
						$country_page_language_hits_found = true;
					}
				}
				if ( $country_page_language_hits_found ) {
					$content .= $this->tbl->tbl_out( $handler, 1 );
				} else {
					$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
				}
			} else {
				$content .= "<p>" . AL_NO_DATA_AVAILABLE . "</p>\n";
			}


		}
		return $content;
	}

}


$class_page_counter = new class_page_counter();
$content .= $class_page_counter->get_content();
