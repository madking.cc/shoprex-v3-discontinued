<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or define( "SECURITY_CHECK", "OK" );

if ( session_status() !== PHP_SESSION_ACTIVE ) {
	session_start();
}

define( "DIRROOT", realpath( __DIR__ ) . "/../" );
define( "DIRINCLUDE", DIRROOT . "__include/" );
define( "ADMINDIRROOT", realpath( __DIR__ ) . "/" );
define( "ADMINDIRINCLUDE", ADMINDIRROOT . "__include/" );
$GLOBALS['is_admin']         = true;
$GLOBALS['do_visitor_store'] = false;
$GLOBALS['logging_prefix']   = "admin_";

require_once( DIRINCLUDE . "init.php" );
require_once( ADMINDIRINCLUDE . "settings.php" );
require_once( ADMINDIRINCLUDE . "check_login.php" );
require_once( ADMINDIRINCLUDE . "get_content.php" );
require_once( ADMINDIRINCLUDE . "do_output.php" );
require_once( ADMINDIRINCLUDE . "terminate.php" );
