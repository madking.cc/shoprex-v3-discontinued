<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

if ( ! isset( $GLOBALS['is_admin'] ) ) {
	$GLOBALS['is_admin'] = false;
}
if ( ! isset( $GLOBALS['do_visitor_store'] ) ) {
	$GLOBALS['do_visitor_store'] = false;
}

define( "DIRSETTINGS", DIRROOT . "__functions/settings/" );

// Lokal Settings
require_once( DIRSETTINGS . "load_settings_local.php" );
$default_datetime_format_file = $GLOBALS['default_datetime_format'];
// Load runlevel 0 Classes Code
require_once( DIRCLASSES . "load_runlevel_0_code.php" );
// Set runlevel 0
$runlevel = class_runlevel::getInstance();
$runlevel->set_level( 0 );

// Load runlevel 1 Classes Code
require_once( DIRCLASSES . "load_runlevel_1_code.php" );
$log = class_logging::getInstance();
//$mail = new class_mail();
$db = new class_database();
require_once( DIRSETTINGS . "load_settings_database.php" );
$runlevel->set_level( 1 );

require_once( DIRCLASSES . "load_runlevel_2_code.php" );
if ( ! isset( $GLOBALS['fixed_lang_path'] ) ) {
	$GLOBALS['fixed_lang_path'] = null;
}
$lang = class_language::getInstance();
$loc  = class_location::getInstance();
$lang->set_lang( $loc->lang_path );
require_once( DIRSETTINGS . "load_settings_database_by_language.php" );
if ( $GLOBALS['is_admin'] ) {
	require_once( DIRFUNCTIONS . "language/define_language_admin.php" );
	require_once( DIRFUNCTIONS . "date_time_locale/set_date_time_format_admin.php" );
	require_once( DIRFUNCTIONS . "date_time_locale/set_locale_admin.php" );
	require_once( DIRFUNCTIONS . "language/load_language_constants_admin.php" );
}
require_once( DIRFUNCTIONS . "language/load_language_constants.php" );
$runlevel->set_level( 2 );

require_once( DIRCLASSES . "load_runlevel_3_code.php" );
$v = class_visitor::getInstance();
$p = new class_page();
$p->keep_session_alive();
$l = new class_layout();
$runlevel->set_level( 3 );

if ( $GLOBALS['is_admin'] ) {
	require_once( DIRCLASSES . "class_sys.php" );
	//$sys = new class_sys();
	$runlevel->set_level( 4 );
}


if ( ! $GLOBALS['do_visitor_store'] || $GLOBALS['is_admin'] ) {
	if ( defined( "PREVIEW_MODE" ) && PREVIEW_MODE ) {
		require_once( DIRFUNCTIONS . "preview/reset_db.php" );
	}

	$plugin_loader = $p->get_files( "plugins/" );
	if ( is_array( $plugin_loader ) && sizeof( $plugin_loader ) > 0 ) {
		foreach ( $plugin_loader as $plugin_load ) {
			if ( ! $GLOBALS['is_admin'] ) {
				if ( strpos( $plugin_load, "_admin" ) !== false ) {
					continue;
				}
			}
			require_once( DIRROOT . "plugins/" . $plugin_load );
		}
	}
}
