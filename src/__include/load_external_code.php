<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

$class_external = class_load_external::getInstance();

if ( $class_external->load_external_code ) {


	if ( ! DO_DEBUG ) {
		$min  = ".min";
		$min2 = "-min";
	} else {
		$min  = "";
		$min2 = "";
	}

	// jQuery
	if ( $class_external->load_jquery ) {
		$p->init_array( $GLOBALS['head'], 10 );
		$GLOBALS['head'][10] .= "      <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "jquery/ui/jquery-ui" . $min . ".css\" rel=\"stylesheet\" media=\"all\" />\n";
		$GLOBALS['head'][10] .= "      <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "jquery/ui/jquery-ui.structure" . $min . ".css\" rel=\"stylesheet\" media=\"all\" />\n";
		$GLOBALS['head'][10] .= "      <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "jquery/ui/jquery-ui.theme" . $min . ".css\" rel=\"stylesheet\" media=\"all\" />\n";
		$p->init_array( $GLOBALS['foot'], 20 );
		$GLOBALS['foot'][20] .= "      <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/jquery" . $min . ".js\"></script>\n";
		$GLOBALS['foot'][20] .= "      <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/ui/jquery-ui" . $min . ".js\"></script>\n";
	}
	// jQuery Plugin Cookie
	if ( $class_external->load_jquery && $class_external->load_cookie ) {
		$GLOBALS['foot'][20] .= "      <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/cookie/jquery.cookie.js\"></script>\n";
	}
	// Bootstrap
	if ( $class_external->load_bootstrap ) {
		$GLOBALS['head_top'] .= "      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n";
		$p->init_array( $GLOBALS['head'], 15 );
		$GLOBALS['head'][15] .= "      <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "bootstrap/css/bootstrap" . $min . ".css\" rel=\"stylesheet\" media=\"all\" />\n";
		$p->init_array( $GLOBALS['foot'], 0 );
		$GLOBALS['foot'][0] .= "       <!--[if lt IE 9]>\n";
		$GLOBALS['foot'][0] .= "           <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "bootstrap/assets/js/html5shiv" . $min . ".js\"></script>\n";
		//$GLOBALS['foot'][0] .= "           <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "bootstrap/assets/js/html5shiv-printshiv".$min.".js\"></script>\n";
		$GLOBALS['foot'][0] .= "           <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "bootstrap/assets/js/respond" . $min . ".js\"></script>\n";
		$GLOBALS['foot'][0] .= "       <![endif]-->\n";
		$p->init_array( $GLOBALS['foot'], 30 );
		$GLOBALS['foot'][30] .= "      <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "bootstrap/js/bootstrap" . $min . ".js\"></script>\n";
		if ( is_file( DIRROOT . $GLOBALS['layout_d'] . $GLOBALS['theme'] . "js/bootstrap.js" ) ) {
			$p->init_array( $GLOBALS['foot'], 100 );
			$GLOBALS['foot'][100] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['layout_d'] . $GLOBALS['theme'] . "js/bootstrap.js\"></script>\n";
		}
		if ( $class_external->load_response && is_file( DIRROOT . $GLOBALS['layout_d'] . $GLOBALS['theme'] . "js/response.js" ) ) {
			$p->init_array( $GLOBALS['foot'], 100 );
			$GLOBALS['foot'][100] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['layout_d'] . $GLOBALS['theme'] . "js/response.js\"></script>\n";
		}
	}
	// jQuery Plugin Sticky
	if ( $class_external->load_jquery && $class_external->load_sticky ) {
		$p->init_array( $GLOBALS['foot'], 22 );
		$GLOBALS['foot'][20] .= "         <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/garand-sticky/jquery.sticky.js\"></script>\n";
	}
	// jQuery Plugin Colorbox
	if ( $class_external->load_jquery && $class_external->load_colorbox ) {
		$p->init_array( $GLOBALS['foot'], 25 );
		$GLOBALS['foot'][25] .= "         <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/colorbox-master/jquery.colorbox" . $min2 . ".js\"></script>\n";
		if ( is_file( DIRROOT . $GLOBALS['external_d'] . "jquery/plugins/colorbox-master/i18n/jquery.colorbox-" . $lang->lhcd . ".js" ) ) {
			$GLOBALS['foot'][25] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/colorbox-master/i18n/jquery.colorbox-" . $lang->lhcd . ".js\"></script>\n";
		} else {
			$GLOBALS['foot'][25] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/colorbox-master/i18n/jquery.colorbox-de.js\"></script>\n";
		}
		$p->init_array( $GLOBALS['head'], 12 );
		$GLOBALS['head'][12] .= "        <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "jquery/plugins/colorbox-master/colorbox.css\" rel=\"stylesheet\" media=\"all\" />\n";
	}
	// Editor
	if ( $class_external->load_jquery && $class_external->load_ta_enhanced ) {
		$p->init_array( $GLOBALS['foot'], 22 );
		$GLOBALS['foot'][22] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "tinymce/tinymce.min.js\"></script>\n";
		$GLOBALS['foot'][22] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "tinymce/tinymce.min.js\"></script>\n";
	}
	// jQuery Plugin Timepicker
	if ( $class_external->load_jquery && $class_external->load_timepicker ) {
		$p->init_array( $GLOBALS['foot'], 35 );
		$GLOBALS['foot'][35] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/timepicker/jquery-ui-timepicker-addon" . $min . ".js\"></script>\n";
		if ( is_file( DIRROOT . $GLOBALS['external_d'] . "jquery/plugins/timepicker/i18n/jquery-ui-timepicker-" . $lang->lhcd . ".js" ) ) {
			$GLOBALS['foot'][35] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/timepicker/i18n/jquery-ui-timepicker-" . $lang->lhcd . ".js\"></script>\n";
		} else {
			$GLOBALS['foot'][35] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/timepicker/i18n/jquery-ui-timepicker-de.js\"></script>\n";
		}
		$p->init_array( $GLOBALS['head'], 32 );
		$GLOBALS['head'][32] .= "    <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "jquery/plugins/timepicker/jquery-ui-timepicker-addon" . $min . ".css\" rel=\"stylesheet\" media=\"all\" />\n";
	}
	// jQuery Plugin Colorpicker
	if ( $class_external->load_jquery && $class_external->load_colorpicker ) {
		$p->init_array( $GLOBALS['foot'], 40 );
		$GLOBALS['foot'][40] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/spectrum/spectrum.js\"></script>\n";
		if ( is_file( DIRROOT . $GLOBALS['external_d'] . "jquery/plugins/spectrum/i18n/jquery.spectrum-" . $lang->lhcd . ".js" ) ) {
			$GLOBALS['foot'][40] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/spectrum/i18n/jquery.spectrum-" . $lang->lhcd . ".js\"></script>\n";
		} else {
			$GLOBALS['foot'][40] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/spectrum/i18n/jquery.spectrum-de.js\"></script>\n";
		}
		$p->init_array( $GLOBALS['head'], 40 );
		$GLOBALS['head'][40] .= "    <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "jquery/plugins/spectrum/spectrum.css\" rel=\"stylesheet\" media=\"all\" />\n";
	}
	// Rangy
	if ( $class_external->load_jquery && $class_external->load_rangy ) {
		$p->init_array( $GLOBALS['foot'], 35 );
		$GLOBALS['foot'][35] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "jquery/plugins/rangy/rangyinputs-jquery.js\"></script>\n";
	}
	// Codemirror
	if ( $class_external->load_ta_highlighter ) {
		$p->init_array( $GLOBALS['foot'], 55 );
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "codemirror/lib/codemirror.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "codemirror/addon/selection/selection-pointer.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "codemirror/mode/clike/clike.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "codemirror/mode/javascript/javascript.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "codemirror/mode/css/css.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "codemirror/mode/xml/xml.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "codemirror/mode/htmlmixed/htmlmixed.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "codemirror/mode/php/php.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "codemirror/addon/display/autorefresh.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "codemirror/addon/edit/matchbrackets.js\"></script>\n";

		$p->init_array( $GLOBALS['head'], 55 );
		$GLOBALS['head'][55] .= "    <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "codemirror/lib/codemirror.css\" rel=\"stylesheet\" media=\"all\" />\n";
		//$GLOBALS['head'][55] .= "    <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "codemirror/theme/elegant.css\" rel=\"stylesheet\" media=\"all\" />\n";
	}

	// Syntaxhighlighter
	if ( $class_external->load_codehl ) {
		$p->init_array( $GLOBALS['foot'], 55 );
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "syntaxhighlighter/scripts/shCore.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "syntaxhighlighter/scripts/shBrushXml.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "syntaxhighlighter/scripts/shBrushBash.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "syntaxhighlighter/scripts/shBrushCpp.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "syntaxhighlighter/scripts/shBrushCSharp.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "syntaxhighlighter/scripts/shBrushCss.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "syntaxhighlighter/scripts/shBrushJScript.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "syntaxhighlighter/scripts/shBrushPhp.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "syntaxhighlighter/scripts/shBrushPlain.js\"></script>\n";
		$GLOBALS['foot'][55] .= "    <script src=\"" . $loc->httpauto_web_root . $GLOBALS['external_d'] . "syntaxhighlighter/scripts/shBrushSql.js\"></script>\n";
		$p->init_array( $GLOBALS['head'], 55 );
		$GLOBALS['head'][55] .= "    <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "syntaxhighlighter/styles/shCore.css\" rel=\"stylesheet\" media=\"all\" />\n";
		$GLOBALS['head'][55] .= "    <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "syntaxhighlighter/styles/shCoreDefault.css\" rel=\"stylesheet\" media=\"all\" />\n";
		$GLOBALS['head'][55] .= "    <link href=\"/" . $loc->web_root . $GLOBALS['external_d'] . "syntaxhighlighter/styles/shThemeDefault.css\" rel=\"stylesheet\" media=\"all\" />\n";
		$GLOBALS['head'][55] .= "    <style>\n";
		$GLOBALS['head'][55] .= "          div.syntaxhighlighter .container:before,\n";
		$GLOBALS['head'][55] .= "          div.syntaxhighlighter .container:after\n";
		$GLOBALS['head'][55] .= "          {\n";
		$GLOBALS['head'][55] .= "              content:none;\n";
		$GLOBALS['head'][55] .= "          }\n";
		$GLOBALS['head'][55] .= "    </style>\n";
		$p->init_array( $GLOBALS['foot'], 55 );
		$GLOBALS['foot'][55] .= "    <script>\n";
		$GLOBALS['foot'][55] .= "          SyntaxHighlighter.defaults['html-script'] = false;\n";
		$GLOBALS['foot'][55] .= "          SyntaxHighlighter.all();\n";
		$GLOBALS['foot'][55] .= "    </script>\n";
	}
	// Own Table Sort Javascript
	if ( $class_external->load_js_tbl ) {
		$p->init_array( $GLOBALS['foot'], 200 );
		$GLOBALS['foot'][200] .= "    <script>
            jQuery.fn.outerHTML = function() {
                return (this[0]) ? this[0].outerHTML : '';
            };
        
            function change_sort(row, id)
            {
                var p = window['arg_size_' + id];
                for(var i=0; i < p; i++)
                {
                    if(i==row) continue;
                    if(window['sort_' + row + '_' + id] != 'undefined')
                        delete window['sort_' + i + '_' + id];
                }
        
                if(window['sort_' + row + '_' + id] == 'undefined')
                {
                    window['sort_' + row + '_' + id] = 0;
                }
                else if(window['sort_' + row + '_' + id] == 0)
                {
                    window['sort_' + row + '_' + id] = 1;
                }
                else
                {
                    window['sort_' + row + '_' + id] = 0;
                }
                sort = window['sort_' + row + '_' + id];
                update_tbl(id, row, sort);
            }
        
            function update_tbl(id, row, sort)
            {
                var content = '';
                $.each($('table#' + id + ' thead a'), function()
                {
                    $(this).removeClass('selected_up');
                    $(this).removeClass('selected_down');
                });
                $.cookie('initSort', row, { expires: 30 });
                if(sort == 0)
                {
                    $('#tbl_head_sort_' + row + '_' + id).addClass('selected_down');
                    $.cookie('initDirection', 0, { expires: 30 });
                }
                else
                {
                    $('#tbl_head_sort_' + row + '_' + id).addClass('selected_up');
                    $.cookie('initDirection', 1, { expires: 30 });
                }
        
                row = row * 3;
                window['array_' + id] = sort_js_tbl(window['array_' + id], row, sort);
        
                content += '<tbody>';
                for ( var i = 0; i < window['array_' + id].length; i = i + 1 )
                {
                    content += '<tr><td>' + (i+1) + '</td>';
                    for ( var j = 0; j < window['array_' + id][i].length; j = j + 3 )
                    {
                        if(window['array_' + id][i][j+1] == 'time' || window['array_' + id][i][j+1] == 'file_size' )
                        {
        
                            content += '<td>' +  window['array_' + id][i][j+2] + '</td>';
                        }
                        else
                        {
                            content += '<td>' +  window['array_' + id][i][j] +  window['array_' + id][i][j+1] + '</td>';
                        }
        
                    }
                    content += '</tr>';
                }
                content += '</tbody>';
                $(content).replaceAll('#' + id + ' > tbody');
        
            }
        
            function sort_js_tbl(array, row, sort)
            {
        
                array.sort((function(index){
                    return function(a, b){
                        return (strcmp(a[index],b[index], sort));
                    };
                })(row));
                return array;
            }
        
            function strcmp(a, b, sort) {
        
                if(!isNaN(a) && !isNaN(b))
                {
                    a = parseFloat(a);
                    b = parseFloat(b);
                    if (a === b) return 0;
                    if(sort == 0)
                        return a > b ? -1 : 1;
                    else
                        return a < b ? -1 : 1;
        
                }
                if (a === b) return 0;
                if(sort == 0)
                    return a < b ? -1 : 1;
                else
                    return a > b ? -1 : 1;
            }
            </script>
        ";
	}

	ksort( $GLOBALS['head'] );
	ksort( $GLOBALS['foot'] );
}
