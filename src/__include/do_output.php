<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

if ( ! isset( $GLOBALS["show_layout"] ) ) {
	$GLOBALS["show_layout"] = true;
}
if ( ! isset( $GLOBALS['theme'] ) ) {
	if ( ! defined( "LAYOUT" ) ) {
		$GLOBALS['theme'] = "default/";
	} else {
		$GLOBALS['theme'] = LAYOUT . "/";
	}
}
if ( ! isset( $GLOBALS['sublayout'] ) || empty( $GLOBALS['sublayout'] ) ) {
	$GLOBALS['sublayout'] = "main/";
}


if ( is_file( LAYOUTDIR . $GLOBALS['theme'] . "init.php" ) ) {
	require_once( LAYOUTDIR . $GLOBALS['theme'] . "init.php" );
}
if ( is_file( LAYOUTDIR . $GLOBALS['theme'] . $GLOBALS['sublayout'] . "init.php" ) ) {
	require_once( LAYOUTDIR . $GLOBALS['theme'] . $GLOBALS['sublayout'] . "init.php" );
}

if ( $GLOBALS['show_layout'] && ! ( isset( $pure_content ) && $pure_content ) ) {
	require_once( LAYOUTDIR . $GLOBALS['theme'] . $GLOBALS['sublayout'] . "header.php" );
	require_once( LAYOUTDIR . $GLOBALS['theme'] . $GLOBALS['sublayout'] . "footer.php" );
	$content = $header . $footer;
}

$parse_class = class_parse_content::getInstance();
$content     = $parse_class->parse( $content );
// Disable PHP and show as code
$content = str_replace( "<?", "&#60;?", $content );

// Flags and alternate links Start
if ( LACTIVE ) {
	$hreflang_content = "";
	$flags            = array();
	$flag_links       = "";
	if ( stripos( $content, "#[flags]#" ) !== false ) {
		if ( isset( $lang->count_of_languages ) && $lang->count_of_languages > 1 ) {
			if ( sizeof( $lang->lang_path ) > 0 ) {
				// Get last "Language Path" (Last has the highest priority and it is an array)
				$lang_path = array_pop( $lang->lang_path );
				foreach ( $lang_path as $lang_key => $hreflang_path ) {
					$hreflang_path = $p->fix_link( $hreflang_path );
					$flags[ $lang_key ] = array(
						"title" => $lang->languages[ $lang_key ]['link_title'],
						"img"   => $lang->languages[ $lang_key ]['img'],
						"path"  => $loc->httpauto_root . $loc->web_root . $hreflang_path
					);
					if ( $lang_key == $lang->lkey ) {
						continue;
					}
					$hreflang_content .= "<link rel=\"alternate\" hreflang=\"" . $lang->languages[ $lang_key ]['hreflang'] . "\" href=\"" . $loc->httpauto_web_root . $hreflang_path . "\" >\n";
				}
			}
			$flag_links = "";
			if ( sizeof( $flags ) > 0 ) {
				$flag_links .= "<ul class='flags'>\n";
				foreach ( $flags as $lang_key => $flag_array ) {
					if ( ! is_null( $flag_array['title'] ) && ! empty( $flag_array['title'] ) ) {
						$flag_title = "title=\"" . $flag_array['title'] . "\"";
					} else {
						$flag_title = "";
					}
					if ( $lang->languages[ $lang_key ]['number'] == $lang->lnbr ) {
						$class = "img active";
					} else {
						$class = "img inactive";
					}
					$flag_links .= " <li>" . $l->link( $l->img( $GLOBALS['layout_d'] . $GLOBALS['theme'] . "flags/" . $flag_array['img'], "", "", $class ), $flag_array['path'], "", $flag_title, "" ) . "</li>\n";
				}
				$flag_links .= "</ul>\n";
			}
		}
		$content = str_replace( "#[flags]#", $flag_links, $content );
	}
} else {
	$content = str_replace( "#[flags]#", "", $content );
}

// Title
if ( ! defined( "SEPARATOR" ) ) {
	$separator = " ";
} else {
	$separator = SEPARATOR;
}

if ( isset( $l->subtitle ) && ! empty( $l->subtitle ) ) {
	$subtitle .= $l->subtitle;
} elseif ( ! isset( $subtitle ) || empty( $subtitle ) ) {
	$separator = "";
	$subtitle  = "";
}

if ( ! ( isset( $pure_content ) && $pure_content ) ) {
	$this_dir_root = realpath( __DIR__ ) . "/";
	require_once( $this_dir_root . "load_external_code.php" );

	require_once( LAYOUTDIR . $GLOBALS['theme'] . "header_code.php" );
	require_once( LAYOUTDIR . $GLOBALS['theme'] . "footer_code.php" );
	$content = $header_code . $content . $footer_code;
}

require_once( DIRROOT . "__functions/security/set_headers.php" );

// Do Output
echo $content;
$runlevel->set_level( 5 );