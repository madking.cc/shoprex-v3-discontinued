<?php /*
 ////////////////////////////////////////////////////////////////////////

 Content Management and OnlineShop Software "shoprex"
 Copyright (C) {2010-2016}  Andreas Rex

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 Contact:

 Andreas Rex
 Wismmannstr. 80
 DE-90411 Nuremberg

 E-Mail: post@shoprex.de

 ////////////////////////////////////////////////////////////////////////
*/ ?><?php defined( 'SECURITY_CHECK' ) or die;

require_once( DIRFUNCTIONS . "content/load_language_text_constants.php" );
require_once( DIRFUNCTIONS . "content/get_content_functions.php" );
require_once( DIRFUNCTIONS . "content/check_browser_compatibility.php" );
require_once( DIRFUNCTIONS . "content/cookie_hint.php" );
require_once( DIRFUNCTIONS . "content/load_global_javascript.php" );

if ( ! isset( $GLOBALS["show_layout"] ) ) {
	$GLOBALS["show_layout"] = true;
}

if ( isset( $page_function ) ) {
	if ( is_file( DIRROOT . $page_function ) ) {
		require_once( DIRROOT . $page_function );
	} else {
		$log->error( "php", __FILE__ . ":" . __LINE__, "Page function file '" . DIRROOT . $page_function . "' not found." );
		$loc->not_found = $loc->uri_inside;
		require_once( DIRROOT . "__functions/maintenance/maintenance.php" );
	}
} else {
	// Display correct page, when site is offline
	if ( PAGE_IS_OFFLINE ) {
		$GLOBALS["show_layout"] = OFFLINE_SHOW_LAYOUT;


		if ( OFFLINE_SHOW_PAGE > 0 ) {
			$sql    = "SELECT * FROM `" . TP . "pages` WHERE `id` = '" . OFFLINE_SHOW_PAGE . "' AND `deleted`='0' AND `active`='1' LIMIT 0,1; ";
			$result = $db->query( $sql, __FILE__ . ":" . __LINE__ );

			if ( $result->num_rows == 1 ) {
				$row = $result->fetch_assoc();

				set_subtitle( $row );
				set_body_class( $row );
				set_meta( $row );
				set_js( $row );
				set_css( $row );
				set_layout( $row );
				set_lang_array( $row );

				$content .= get_text( $row );
			} else {
				$content .= no_content();
			}
		} else {
			$content .= no_content();
		}
	} else // Find Content in Database:
	{
		// Find Page in case of language
		if ( empty( $loc->uri_clean ) || strcasecmp( $loc->uri_clean, "index.php" ) == 0 ) // If index file
		{
			$sql = "SELECT * FROM `" . TP . "pages` WHERE `path" . $lang->lnbr . "` IS NULL AND `deleted`='0' AND `active`='1' LIMIT 0,1; ";
		} else // All other Sites
		{
			$sql = "SELECT * FROM `" . TP . "pages` WHERE `path" . $lang->lnbr . "` LIKE BINARY '" . $loc->uri_clean . "' AND `deleted`='0' AND `active`='1' LIMIT 0,1; ";
		}

		$result = $db->query( $sql, __FILE__ . ":" . __LINE__ );
		if ( $result->num_rows == 0 ) {
			// Try to find page with main language
			if ( empty( $loc->uri_clean ) || strcasecmp( $loc->uri_clean, "index.php" ) == 0 ) {
				$sql = "SELECT * FROM `" . TP . "pages` WHERE `path" . $lang->lnbrm . "` IS NULL AND `deleted`='0' AND `active`='1' LIMIT 0,1; ";
			} else {
				$sql = "SELECT * FROM `" . TP . "pages` WHERE `path" . $lang->lnbrm . "` LIKE BINARY '" . $loc->uri_clean . "' AND `deleted`='0' AND `active`='1' LIMIT 0,1; ";
			}

			$result = $db->query( $sql, __FILE__ . ":" . __LINE__ );
		}

		// Page in Database found
		if ( $result->num_rows == 1 ) {

			$row = $result->fetch_assoc();

			check_password_protection( $row );

			set_subtitle( $row );
			set_body_class( $row );
			set_meta( $row );
			set_js( $row );
			set_css( $row );
			set_layout( $row );
			set_lang_array( $row );

			$content .= get_text( $row );
		} else {
			// Try to find local file

			if ( is_file( DIRFUNCTIONS . "pages/" . $loc->uri_clean ) ) {
				set_lang_array( $loc->current );
				require_once( DIRFUNCTIONS . "pages/" . $loc->uri_clean );
			} elseif ( is_file( $loc->dir_root . "pages/" . $loc->lang_path . "/" . $loc->uri_clean ) ) {
				set_lang_array( $loc->current );
				require_once( $loc->dir_root . "pages/" . $loc->lang_path . "/" . $loc->uri_clean );
			} else {
				// Falls keine $page angegeben wurde
				if ( ! isset( $page ) && empty( $page ) ) {
					$loc->not_found = $loc->uri;
					$v->store_not_found( $loc->not_found );
					require_once( DIRROOT . "__functions/maintenance/maintenance.php" );
				} else // Wenn $page nicht leer
				{
					set_lang_array( $page );
					require_once( DIRROOT . "pages/" . $page );
				}
			}
		}
	}
}

$runlevel->set_level( 4 );