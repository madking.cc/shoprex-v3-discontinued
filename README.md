Who should use this software aimed at?

This software is intended for web administrators, who want to develop smaller sites for relatives, friends, acquaintances or small business users.
The advantages of this are:

    A page is created quickly, it has to be taken care of just about the layout and content.
    All technical details, like a proper HTML document or the sitemap will be generated automatically.
    Content and layout are quickly remotely through the admin panel for the Web Admin and easily changeable.

As solid plugins moment include: Blog System, news and pictures slideshows.
The online store feature is still in development. The end of 2016 is expected to be completed.

For live preview see: http://www.shoprex.de/en/#shoprexFunctions

This Software is licensed with GNU GPL v2.0.

Functions:

Multilanguage (up to 10); Responsive; Many integrated libraries, like jQuery and Bootstrap; Easy to use admin panel; Editable Menus, Pages and more; With Blog, News, Picture-Carousel, File Download; Effective Login evaluation online; Meaningful statistics; Automatic Sitemap Generator; High security, since all data received will be examined; Full UTF-8 support; Many layout possibilities; Upload pictures editable after upload; Videos and files be uploaded; WYSIWYG editor (individually switched off); Good Maintenance Functions; Preview function; Private Javascript and CSS code addable to each side;

In development:

Shop functions;